﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Data.SqlClient;
using System.Security.Claims;
using System.IO;
using System.Text.Json;
using NawaDataBLL;
using NawaDataDAL;
using NawaDataDAL.Models;

namespace NawaDataAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomController : ControllerBase
    {
        private CustomBLL GetBLL()
        {
            return new CustomBLL();
        }
        private CustomBLL GetBLL(int moduleID)
        {
            return new CustomBLL(moduleID);
        }
        private CustomBLL GetBLL(long moduleApprovalID)
        {
            return new CustomBLL(moduleApprovalID);
        }

        [Route("ExportTemplateData")]
        [HttpGet]
        [Authorize]
        public IActionResult ExportModuleData(int ModuleID)
        {
            ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
            string userID = identity.FindFirst("UserID").Value;
            try
            {
                if (CommonBLL.CheckMenuAccess(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), ModuleID, EActionType.Import))
                {
                    MemoryStream ms = GetBLL(ModuleID).ExportTemplateData(ModuleID, identity.FindFirst("UserID").Value, Convert.ToInt32(identity.FindFirst("PK_MUser_ID").Value), Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value));
                    ms.Position = 0;

                    return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                }
                else
                {
                    return StatusCode(500, "You don't have authorization to access this menu.");
                }

            }
            catch (Exception ex)
            {
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("SaveParameterUpload")]
        [Authorize]
        [HttpPost]
        public ActionResult SaveParameterUpload([FromForm] CParameterUpload parameterUpload)
        {
            ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
            string userID = identity.FindFirst("UserID").Value;
            try
            {
                if (CommonBLL.CheckMenuAccess(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), parameterUpload.ModuleID, EActionType.Import))
                {
                    GetBLL(parameterUpload.ModuleID).SaveParameterUpload(parameterUpload.ModuleID, parameterUpload.body.OpenReadStream(), identity.FindFirst("UserID").Value);

                    return Ok("OK");
                }
                else
                {
                    return StatusCode(500, "You don't have authorization to access this menu.");
                }
            }
            catch (Exception ex)
            {
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("CommitModuleUpload")]
        [Authorize]
        [HttpGet]
        public ActionResult CommitModuleUpload(int ModuleID)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;

                if (CommonBLL.CheckMenuAccess(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), ModuleID, EActionType.Import))
                {
                    GetBLL(ModuleID).CommitParameterUpload(ModuleID, identity.FindFirst("UserID").Value, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value));

                    return Ok("OK");
                }
                else
                {
                    return StatusCode(500, "You don't have authorization to access this menu.");
                }
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }


        [Route("GetValidModuleDetail")]
        [Authorize]
        [HttpGet]
        public ActionResult GetValidModuleDetail(int ModuleID)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;

                if (CommonBLL.CheckMenuAccess(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), ModuleID, EActionType.Import))
                {
                    ModuleView moduleView = GetBLL(ModuleID).GetValidModuleDetail(ModuleID);

                    return Ok(moduleView);
                }
                else
                {
                    return StatusCode(500, "You don't have authorization to access this menu.");
                }
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500);
            }
        }
        [Route("GetValidModuleData")]
        [Authorize]
        [HttpGet]
        public ActionResult GetValidModuleData(int ModuleID, int page, string orderBy, string order, string search, string filter)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;

                if (CommonBLL.CheckMenuAccess(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), ModuleID, EActionType.Import))
                {
                    int rowCount = 0;
                    List<NawaDataDAL.DynamicDictionary> moduleData = GetBLL(ModuleID).GetValidModuleData(ModuleID, identity.FindFirst("UserID").Value, page, orderBy, order, search, filter, out rowCount);

                    return Ok(new { count = rowCount, rows = moduleData });
                }
                else
                {
                    return StatusCode(500, "You don't have authorization to access this menu.");
                }
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500);
            }
        }

        [Route("GetInvalidModuleDetail")]
        [Authorize]
        [HttpGet]
        public ActionResult GetInvalidModuleDetail(int ModuleID)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;

                if (CommonBLL.CheckMenuAccess(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), ModuleID, EActionType.Import))
                {
                    ModuleView moduleView = GetBLL(ModuleID).GetInvalidModuleDetail(ModuleID);

                    return Ok(moduleView);
                }
                else
                {
                    return StatusCode(500, "You don't have authorization to access this menu.");
                }
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500);
            }
        }

        [Route("GetInvalidModuleData")]
        [Authorize]
        [HttpGet]
        public ActionResult GetInvalidModuleData(int ModuleID, int page, string orderBy, string order, string search, string filter)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;

                if (CommonBLL.CheckMenuAccess(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), ModuleID, EActionType.Import))
                {
                    int rowCount = 0;
                    List<NawaDataDAL.DynamicDictionary> moduleData = GetBLL(ModuleID).GetInvalidModuleData(ModuleID, identity.FindFirst("UserID").Value, page, orderBy, order, search, filter, out rowCount);

                    return Ok(new { count = rowCount, rows = moduleData });
                }
                else
                {
                    return StatusCode(500, "You don't have authorization to access this menu.");
                }
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500);
            }
        }

        [Route("GetModuleDetail")]
        [Authorize]
        [HttpGet]
        public ActionResult GetModuleDetail(int ModuleID, string ID = "", long PK_ModuleApproval_ID = 0)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;

                if (CommonBLL.CheckMenuAccess(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), ModuleID, EActionType.View)
                    || CommonBLL.CheckMenuAccess(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), ModuleID, EActionType.Insert)
                    || CommonBLL.CheckMenuAccess(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), ModuleID, EActionType.Import)
                    || CommonBLL.CheckMenuAccess(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), ModuleID, EActionType.Detail))
                {
                    ModuleView moduleView = GetBLL(ModuleID).GetModuleDetail(ModuleID, ID, PK_ModuleApproval_ID, identity.FindFirst("UserID").Value);

                    return Ok(moduleView);
                }
                else
                {
                    return StatusCode(500, "You don't have authorization to access this menu.");
                }
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetModuleDetailDetail")]
        [Authorize]
        [HttpGet]
        public ActionResult GetModuleDetailDetail(int ModuleDetailID, string ID = "", long PK_ModuleApproval_ID = 0)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;

                if (CommonBLL.CheckMenuAccessModuleDetail(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), ModuleDetailID, EActionType.View))
                {
                    ModuleDetailView moduleView = GetBLL(PK_ModuleApproval_ID).GetModuleDetailDetail(ModuleDetailID, ID, PK_ModuleApproval_ID, identity.FindFirst("UserID").Value);

                    return Ok(moduleView);
                }
                else
                {
                    return StatusCode(500, "You don't have authorization to access this menu.");
                }
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500);
            }
        }

        [Route("GetModuleData")]
        [Authorize]
        [HttpGet]
        public ActionResult GetModuleData(int ModuleID, int page, string orderBy, string order, string search, string filter, bool isAdv)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                int rowCount = 0;

                if (!CommonBLL.CheckMenuAccess(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), ModuleID, EActionType.View))
                    return StatusCode(500, "You don't have authorization to access this menu.");

                List<NawaDataDAL.DynamicDictionary> moduleData = GetBLL(ModuleID).GetModuleData(ModuleID, page, orderBy, order, search, filter, isAdv, out rowCount, identity.FindFirst("FK_MRole_ID").Value, identity.FindFirst("UserID").Value);

                return Ok(new { count = rowCount, rows = moduleData });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        //[Route("GetModuleDetailDataView")]
        //[Authorize]
        //[HttpGet]
        //public ActionResult GetModuleDetailDataView(int ModuleDetailID, int id)
        //{
        //    try
        //    {
        //        ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;

        //        if (CommonBLL.CheckMenuAccessModuleDetail(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), ModuleDetailID, EActionType.Insert))
        //        {
        //            int rowCount = 0;
        //            List<NawaDataDAL.DynamicDictionary> moduleData = GetBLL().GetModuleDetailDataByFkId(ModuleDetailID, id, out rowCount);

        //            return Ok(new { count = rowCount, rows = moduleData });
        //        }
        //        else
        //        {
        //            return StatusCode(500, "You don't have authorization to access this menu.");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ElmahErrorBLL.Log(ex);
        //        return StatusCode(500);
        //    }
        //}

        [Route("GetModuleApproval")]
        [Authorize]
        [HttpGet]
        public ActionResult GetModuleApproval(int ModuleID, int page, string orderBy, string order, string search, string filterKey, string filterValue)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MUser currentUser = new MUser()
                {
                    UserID = identity.FindFirst("UserID").Value,
                    FK_MRole_ID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value),
                    FK_MGroupMenu_ID = Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value)
                };

                if (!CommonBLL.CheckMenuAccess(currentUser.FK_MGroupMenu_ID, ModuleID, EActionType.Approval))
                    return StatusCode(500, "You don't have authorization to access this menu.");

                int rowCount = 0;
                List<ModuleApprovalView> moduleApproval = GetBLL(ModuleID).GetModuleApproval(ModuleID, identity.FindFirst("UserID").Value, page, orderBy, order, search, filterKey, filterValue, out rowCount);

                return Ok(new { count = rowCount, rows = moduleApproval });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500);
            }
        }

        //[Route("GetModuleUploadDetail")]
        //[Authorize]
        //[HttpGet]
        //public ActionResult GetModuleUploadDetailByID(int ModuleID, int RecordID)
        //{
        //    try
        //    {
        //        ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;

        //        if (CommonBLL.CheckMenuAccess(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), ModuleID, EActionType.Import)
        //            || CommonBLL.CheckMenuAccess(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), ModuleID, EActionType.Approval))
        //        {
        //            List<ModuleUploadDetailView> uploadDetail = GetBLL(ModuleID).GetModuleUploadDetail(ModuleID, RecordID, identity.FindFirst("UserID").Value);

        //            return Ok(uploadDetail);
        //        }
        //        else
        //        {
        //            return StatusCode(500, "You don't have authorization to access this menu.");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ElmahErrorBLL.Log(ex);
        //        return StatusCode(500, ex.Message);
        //    }
        //}

        [Route("GetModuleApprovalByID")]
        [Authorize]
        [HttpGet]
        public ActionResult GetModuleApprovalByID(long PK_ModuleApproval_ID)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MUser currentUser = new MUser()
                {
                    UserID = identity.FindFirst("UserID").Value,
                    FK_MRole_ID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value),
                    FK_MGroupMenu_ID = Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value)
                };


                if (!CommonBLL.CheckMenuAccessByApprovalID(currentUser.FK_MGroupMenu_ID, Convert.ToInt32(PK_ModuleApproval_ID), EActionType.Approval))
                    return StatusCode(500, "You don't have authorization to access this menu.");

                ModuleApprovalView moduleApproval = GetBLL(PK_ModuleApproval_ID).GetModuleApprovalByID(PK_ModuleApproval_ID);

                return Ok(moduleApproval);

            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500);
            }
        }

        //[Route("GetModuleDetailApprovalByFkID")]
        //[Authorize]
        //[HttpGet]
        //public ActionResult GetModuleDetailApprovalByFkID(long PK_ModuleApproval_ID, long ModuleDetailID)
        //{
        //    try
        //    {
        //        ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
        //        MUser currentUser = new MUser()
        //        {
        //            UserID = identity.FindFirst("UserID").Value,
        //            FK_MRole_ID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value),
        //            FK_MGroupMenu_ID = Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value)
        //        };


        //        if (!CommonBLL.CheckMenuAccessByApprovalID(currentUser.FK_MGroupMenu_ID, Convert.ToInt32(PK_ModuleApproval_ID), EActionType.Approval))
        //            return StatusCode(500, "You don't have authorization to access this menu.");

        //        List<ModuleDetailApprovalView> moduleDetailApproval = GetBLL(PK_ModuleApproval_ID).GetModuleDetailApprovalByFkID(PK_ModuleApproval_ID, ModuleDetailID);

        //        return Ok(moduleDetailApproval);
        //    }
        //    catch (Exception ex)
        //    {
        //        ElmahErrorBLL.Log(ex);
        //        return StatusCode(500);
        //    }
        //}


        [Route("GetModuleWorkflowHistory")]
        [Authorize]
        [HttpGet]
        public ActionResult GetModuleWorkflowHistory(long PK_ModuleApproval_ID, int page, string orderBy, string order, string search)
        {
            try
            {
                int rowCount = 0;
                List<MWorkFlow_HistoryView> workflowHistories = GetBLL(PK_ModuleApproval_ID).GetModuleWorkflowHistory(PK_ModuleApproval_ID, page, orderBy, order, search, out rowCount);

                return Ok(new { count = rowCount, rows = workflowHistories });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500);
            }
        }


        [Route("ActionModuleApproval")]
        [Authorize]
        [HttpPost]
        public ActionResult ActionModuleApproval(CParameterApproval parameterApproval)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MUser currentUser = new MUser()
                {
                    UserID = identity.FindFirst("UserID").Value,
                    FK_MRole_ID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value),
                    FK_MGroupMenu_ID = Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value)
                };

                if (!CommonBLL.CheckMenuAccessByApprovalID(currentUser.FK_MGroupMenu_ID, Convert.ToInt32(parameterApproval.PK_ModuleApproval_ID), EActionType.Approval))
                    return StatusCode(500, "You don't have authorization to access this menu.");

                if (parameterApproval.ActionApproval == EActionApprovalType.Approve)
                {
                    GetBLL(parameterApproval.PK_ModuleApproval_ID).ApproveModuleApproval(parameterApproval.PK_ModuleApproval_ID, identity.FindFirst("UserID").Value, Convert.ToInt32(identity.FindFirst("PK_MUser_ID").Value), parameterApproval.Review);
                }
                else if (parameterApproval.ActionApproval == EActionApprovalType.Reject)
                {
                    GetBLL(parameterApproval.PK_ModuleApproval_ID).RejectModuleApproval(parameterApproval.PK_ModuleApproval_ID, Convert.ToInt32(identity.FindFirst("PK_MUser_ID").Value), parameterApproval.Review, identity.FindFirst("UserID").Value);
                }
                else
                {
                    GetBLL(parameterApproval.PK_ModuleApproval_ID).ReviseModuleApproval(parameterApproval.PK_ModuleApproval_ID, Convert.ToInt32(identity.FindFirst("PK_MUser_ID").Value), parameterApproval.Review);
                }
                return Ok();
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500);
            }
        }
        [Route("ExportModuleData")]
        [HttpGet]
        [Authorize]
        public IActionResult ExportModuleData(int ModuleID, int page, string orderBy, string order, string search, string filter, bool isAdv)
        {
            ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;

            if (!CommonBLL.CheckMenuAccess(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), ModuleID, EActionType.View))
                return StatusCode(500, "You don't have authorization to access this menu.");

            MemoryStream ms = GetBLL(ModuleID).ExportModuleData(ModuleID, page, orderBy, order, search, filter, isAdv, identity.FindFirst("UserID").Value, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value));
            ms.Position = 0;

            return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Data.xlsx");
        }

        [Route("ExportModuleDataAll")]
        [HttpGet]
        [Authorize]
        public IActionResult ExportModuleDataAll(int ModuleID, string orderBy, string order, string search, string filter, bool isAdv)
        {
            ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;

            if (!CommonBLL.CheckMenuAccess(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), ModuleID, EActionType.View))
                return StatusCode(500, "You don't have authorization to access this menu.");

            MemoryStream ms = GetBLL(ModuleID).ExportModuleDataAll(ModuleID, orderBy, order, search, filter, isAdv, identity.FindFirst("UserID").Value, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value));
            ms.Position = 0;

            return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Data.xlsx");
        }

        [Route("ExportModuleDataSelected")]
        [HttpPost]
        [Authorize]
        public IActionResult ExportModuleDataSelected(CSelectedData selectedData)
        {
            ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;

            if (!CommonBLL.CheckMenuAccess(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), selectedData.ModuleID, EActionType.View))
                return StatusCode(500, "You don't have authorization to access this menu.");

            MemoryStream ms = GetBLL(selectedData.ModuleID).ExportModuleDataSelected(selectedData.ModuleID, selectedData.SelectedData, selectedData.orderBy, selectedData.order);
            ms.Position = 0;

            return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Data.xlsx");
        }

        // 30/9/2020 hadi, to receive RNfetchblob body type
        [Route("ExportModuleDataSelectedMobile")]
        [HttpPost]
        [Authorize]
        public IActionResult ExportUploadModuleDataSelectedMobile([FromBody] string data)
        {
            var selectedData = JsonSerializer.Deserialize<CSelectedData>(data);

            ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
            MemoryStream ms = GetBLL(selectedData.ModuleID).ExportUploadModuleDataSelected(selectedData.ModuleID, selectedData.SelectedData, selectedData.orderBy, selectedData.order, identity.FindFirst("UserID").Value);

            ms.Position = 0;
            return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Data.xlsx");
        }
        // --- end from hadi ---

        [Route("GetModuleDataByID")]
        [Authorize]
        [HttpGet]
        public ActionResult GetModuleDataByID(int ModuleID, int id)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;

                if (CommonBLL.CheckMenuAccess(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), ModuleID, EActionType.Insert)
                    || CommonBLL.CheckMenuAccess(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), ModuleID, EActionType.Detail))
                {
                    NawaDataDAL.Models.CDataWithValidation moduleData = GetBLL(ModuleID).GetModuleData(ModuleID, id);

                    return Ok(moduleData);
                }
                else
                {
                    return StatusCode(500, "You don't have authorization to access this menu.");
                }
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);

                return StatusCode(500);
            }
        }

        [Route("GetListOptions")]
        [Authorize]
        [HttpGet]
        public ActionResult GetListOptions(long PK_ModuleField_ID, string ParentValue, string search = "")
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                List<COption> listOptions = GetBLL().GetListOptions(PK_ModuleField_ID, ParentValue, identity.FindFirst("UserID").Value, search);

                return Ok(listOptions);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500);
            }
        }

        //[Route("GetListOptionsModuleDetail")]
        //[Authorize]
        //[HttpGet]
        //public ActionResult GetListOptionsModuleDetail(long PK_ModuleDetailField_ID, string ParentValue)
        //{
        //    try
        //    {
        //        List<COption> listOptions = GetBLL(ModuleID).GetListOptionsModuleDetail(PK_ModuleDetailField_ID, ParentValue);

        //        return Ok(listOptions);
        //    }
        //    catch (Exception ex)
        //    {
        //        ElmahErrorBLL.Log(ex);
        //        return StatusCode(500);
        //    }
        //}


        [Route("SaveParameterInput")]
        [Authorize]
        [HttpPost]
        public ActionResult SaveParameterInput(CParameterInput parameterInput)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;

                if (CommonBLL.CheckMenuAccess(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), parameterInput.ModuleID, parameterInput.ActionType))
                {
                    List<KeyValuePair<long, string>> validationMessage = GetBLL(parameterInput.ModuleID).SaveData(parameterInput, identity.FindFirst("UserID").Value, Convert.ToInt32(identity.FindFirst("PK_MUser_ID").Value), Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value));

                    if (validationMessage.Count > 0)
                    {
                        return StatusCode(400, validationMessage);
                    }
                    else
                    {
                        return Ok();
                    }
                }
                else
                {
                    return StatusCode(500, "You don't have authorization to access this menu.");
                }
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }


        [Route("SaveParameterDetail")]
        [Authorize]
        [HttpPost]
        public ActionResult SaveParameterDetail(CParameterDetail parameterDetail)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;

                if (CommonBLL.CheckMenuAccess(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), parameterDetail.ModuleID, parameterDetail.ActionType))
                {
                    GetBLL(parameterDetail.ModuleID).SaveData(parameterDetail, identity.FindFirst("UserID").Value, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value));

                    return Ok();
                }
                else
                {
                    return StatusCode(500, "You don't have authorization to access this menu.");
                }
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("CustomSupportSP")]
        [Authorize]
        [HttpPost]
        public ActionResult CustomSupportSP(CustomSupportSPRequest request)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;

                if (!CommonBLL.CheckMenuAccess(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), request.ModuleID, EActionType.View))
                    return StatusCode(500, "You don't have authorization to access this menu.");

                GetBLL(request.ModuleID).CustomSupportSP(request);

                return Ok("Action Complete");
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }
        [Authorize]
        [Route("getSysParam")]
        [HttpGet]
        public ActionResult GetSysParam(int SysParamID)
        {
            try
            {
                AppSystemParameter sysParam = SystemParameterBLL.GetAppSystemParameter(SysParamID);
                return Ok(sysParam);
            }
            catch
            {
                return StatusCode(500);
            }
        }

        [Route("GetImportApprovalByID")]
        [Authorize]
        [HttpGet]
        public ActionResult GetImportApprovalByID(long PK_ModuleApproval_ID, int page, string orderBy, string order, string search, string filter)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MUser currentUser = new MUser()
                {
                    UserID = identity.FindFirst("UserID").Value,
                    FK_MRole_ID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value),
                    FK_MGroupMenu_ID = Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value)
                };


                if (!CommonBLL.CheckMenuAccessByApprovalID(currentUser.FK_MGroupMenu_ID, Convert.ToInt32(PK_ModuleApproval_ID), EActionType.Approval))
                    return StatusCode(500, "You don't have authorization to access this menu.");

                int rowCount = 0;

                List<NawaDataDAL.DynamicDictionary> moduleData = GetBLL(PK_ModuleApproval_ID).GetImportApprovalByID(PK_ModuleApproval_ID, page, orderBy, order, search, filter, out rowCount);

                return Ok(new { count = rowCount, rows = moduleData });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500);
            }
        }


    }
}
