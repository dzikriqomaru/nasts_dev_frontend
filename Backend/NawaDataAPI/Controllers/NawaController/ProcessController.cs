﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NawaDataBLL;
using NawaDataDAL.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace NawaDataAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProcessController : Controller
    {
        [Route("GetLogProcessData")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetLogProcessData(int ModuleID, int page, string orderBy, string order, string search, string filter, bool isAdv)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MUser currentUser = new MUser()
                {
                    UserID = identity.FindFirst("UserID").Value,
                    FK_MRole_ID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value),
                    FK_MGroupMenu_ID = Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value)
                };

                if (!CommonBLL.CheckMenuAccess(currentUser.FK_MGroupMenu_ID, ModuleID, EActionType.View))
                    return StatusCode(500, "You don't have authorization to access this menu.");

                int rowCount = 0;
                List<LogProcess> logProcesses = ProcessBLL.GetLogProcess(
                    ModuleID, 
                    identity.FindFirst("UserID").Value, 
                    page, 
                    orderBy, 
                    order, 
                    search, 
                    filter,
                    isAdv,
                    currentUser.FK_MRole_ID,
                    null,
                    out rowCount, 
                    null
                );

                return Ok(new { count = rowCount, rows = logProcesses });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetLogProcessData")]
        [Authorize(Policy="ApplicationLock")]
        [HttpPost]
        public ActionResult GetLogProcessData([FromBody] GetModuleDataPostBody body)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MUser currentUser = new MUser()
                {
                    UserID = identity.FindFirst("UserID").Value,
                    FK_MRole_ID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value),
                    FK_MGroupMenu_ID = Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value)
                };

                if (!CommonBLL.CheckMenuAccess(currentUser.FK_MGroupMenu_ID, body.ModuleID, EActionType.View))
                    return StatusCode(500, "You don't have authorization to access this menu.");

                int rowCount = 0;
                List<LogProcess> logProcesses = ProcessBLL.GetLogProcess(
                    body.ModuleID, 
                    identity.FindFirst("UserID").Value, 
                    body.page, body.orderBy, 
                    body.order, 
                    body.search, 
                    body.filter,
                    body.isAdv,
                    currentUser.FK_MRole_ID,
                    body.fields,
                    out rowCount, 
                    body.jsonFilter
                );

                return Ok(new { count = rowCount, rows = logProcesses });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ExportCurrentPageData")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public IActionResult ExportCurrentPageData(int ModuleID, int page, string orderBy, string order, string search, string filter, bool isAdv, string exportAs)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MUser currentUser = new MUser()
                {
                    UserID = identity.FindFirst("UserID").Value,
                    FK_MRole_ID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value),
                    FK_MGroupMenu_ID = Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value)
                };

                if (!CommonBLL.CheckMenuAccess(currentUser.FK_MGroupMenu_ID, ModuleID, EActionType.View))
                    return StatusCode(500, "You don't have authorization to access this menu.");

                MemoryStream ms = ProcessBLL.ExportCurrentPage(
                    page, orderBy, order, search, filter, isAdv, exportAs, currentUser.UserID, currentUser.FK_MRole_ID, null, null);

                ms.Position = 0;

                string mediaType;

                if (exportAs == "Text")
                {
                    mediaType = "text/plain";
                }
                else if (exportAs == "CSV")
                {
                    mediaType = "text/csv";
                }
                else
                {
                    mediaType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                }

                return File(ms, mediaType);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ExportCurrentPageData")]
        [Authorize(Policy="ApplicationLock")]
        [HttpPost]
        public IActionResult ExportCurrentPageData([FromBody] GetModuleDataPostBody body)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MUser currentUser = new MUser()
                {
                    UserID = identity.FindFirst("UserID").Value,
                    FK_MRole_ID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value),
                    FK_MGroupMenu_ID = Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value)
                };

                if (!CommonBLL.CheckMenuAccess(currentUser.FK_MGroupMenu_ID, body.ModuleID, EActionType.View))
                    return StatusCode(500, "You don't have authorization to access this menu.");

                MemoryStream ms = ProcessBLL.ExportCurrentPage(
                    body.page, body.orderBy, body.order, body.search, body.filter, body.isAdv, body.exportAs,currentUser.UserID, currentUser.FK_MRole_ID, body.fields, body.jsonFilter);
                ms.Position = 0;

                return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Data.xlsx");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ExportAllPageData")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public IActionResult ExportAllPageData(int ModuleID, int page, string orderBy, string order, string search, string filter, bool isAdv, string exportAs)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MUser currentUser = new MUser()
                {
                    UserID = identity.FindFirst("UserID").Value,
                    FK_MRole_ID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value),
                    FK_MGroupMenu_ID = Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value)
                };

                if (!CommonBLL.CheckMenuAccess(currentUser.FK_MGroupMenu_ID, ModuleID, EActionType.View))
                    return StatusCode(500, "You don't have authorization to access this menu.");


                MemoryStream ms = ProcessBLL.ExportAllPage(
                    page, orderBy, order, search, filter, isAdv, exportAs, currentUser.UserID, currentUser.FK_MRole_ID, null, null);

                ms.Position = 0;

                string mediaType;

                if (exportAs == "Text")
                {
                    mediaType = "text/plain";
                }
                else if (exportAs == "CSV")
                {
                    mediaType = "text/csv";
                }
                else
                {
                    mediaType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                }

                return File(ms, mediaType);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ExportAllPageData")]
        [Authorize(Policy="ApplicationLock")]
        [HttpPost]
        public IActionResult ExportAllPageData([FromBody] GetModuleDataPostBody body)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MUser currentUser = new MUser()
                {
                    UserID = identity.FindFirst("UserID").Value,
                    FK_MRole_ID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value),
                    FK_MGroupMenu_ID = Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value)
                };

                if (!CommonBLL.CheckMenuAccess(currentUser.FK_MGroupMenu_ID, body.ModuleID, EActionType.View))
                    return StatusCode(500, "You don't have authorization to access this menu.");


                MemoryStream ms = ProcessBLL.ExportAllPage(
                    body.page, body.orderBy, body.order, body.search, body.filter, body.isAdv, body.exportAs, currentUser.UserID, currentUser.FK_MRole_ID, body.fields, body.jsonFilter);
                ms.Position = 0;

                return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Data.xlsx");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [Route("CancelTask")]
        [Authorize(Policy="ApplicationLock")]
        [HttpPost]
        public IActionResult CancelTask(int PK_EODSchedulerLog_ID)
        {
            try

            {
                ProcessBLL.CancelTask(PK_EODSchedulerLog_ID);

                return Ok();

            }
            catch (Exception ex)
            {
               return StatusCode(500, ex.Message);
            }
        }

        [Route("ViewDetailProcess")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetDetailData(int ModuleID, int page, string orderBy, string order, string search, string filter, bool isAdv)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MUser currentUser = new MUser()
                {
                    UserID = identity.FindFirst("UserID").Value,
                    FK_MRole_ID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value),
                    FK_MGroupMenu_ID = Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value)
                };

                if (!CommonBLL.CheckMenuAccess(currentUser.FK_MGroupMenu_ID, ModuleID, EActionType.View))
                    return StatusCode(500, "You don't have authorization to access this menu.");

                int rowCount = 0;
                List<LogProcess> logProcesses = ProcessBLL.GetLogProcess(
                    ModuleID,
                    identity.FindFirst("UserID").Value,
                    page,
                    orderBy,
                    order,
                    search,
                    filter,
                    isAdv,
                    currentUser.FK_MRole_ID,
                    null,
                    out rowCount,
                    null
                );

                return Ok(new { count = rowCount, rows = logProcesses });
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }




    }

}
