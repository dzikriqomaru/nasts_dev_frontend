﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NawaDataAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class KubernetesController : ControllerBase
    {
        [Route("GetClusters")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetClusters()
        {
            try
            {
                List<dynamic> allClusters = NawaDataBLL.KubernetesBLL.getClusters();
                return Ok(new { clusters = allClusters });
            }
            catch (Exception)
            {
                return StatusCode(500);
            }
        }

        [Route("GetContainers")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetContainers(string endPoint)
        {
            try
            {
                string containers = NawaDataBLL.KubernetesBLL.getContainers(endPoint);
                return Ok(containers);
            }
            catch (Exception)
            {
                return StatusCode(500);
            }
        }

        [Route("GetPods")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetPods(string endPoint)
        {
            try
            {
                string pods = NawaDataBLL.KubernetesBLL.getPods(endPoint);
                return Ok(pods);
            }
            catch (Exception)
            {
                return StatusCode(500);
            }
        }

        [Route("GetStatefulsets")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetStatefulsets(string endPoint)
        {
            try
            {
                string statefulsets = NawaDataBLL.KubernetesBLL.getStatefulsets(endPoint);
                return Ok(statefulsets);
            }
            catch (Exception)
            {
                return StatusCode(500);
            }
        }

        [Route("GetScale")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetScale(string endPoint, string deploymentName)
        {
            try
            {
                string scale = NawaDataBLL.KubernetesBLL.getScale(endPoint, deploymentName);
                return Ok(scale);
            }
            catch (Exception)
            {
                return StatusCode(500);
            }
        }

        [Route("PostScale")]
        [Authorize(Policy="ApplicationLock")]
        [HttpPost]
        public ActionResult PostScale(string endPoint, string deploymentName, string parameter)
        {
            try
            {
                string scale = NawaDataBLL.KubernetesBLL.putScale(endPoint, deploymentName, parameter);
                return Ok(scale);
            }
            catch (Exception)
            {
                return StatusCode(500);
            }
        }
    }
}
