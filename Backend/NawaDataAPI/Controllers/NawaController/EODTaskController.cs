﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using NawaDataBLL;
using NawaDataDAL;
using NawaDataDAL.Models;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using System.Diagnostics.Tracing;
using System.IO;
using Microsoft.AspNetCore.Hosting;

namespace NawaDataAPI.Controllers
{
    
    [Route("api/[controller]")]
    [ApiController]
    public class EODTaskController : ControllerBase
    {
        private IHostingEnvironment _env;
        public EODTaskController(IHostingEnvironment env)
        {
            _env = env;
        }
        #region Master EODTask

        #region Get Module Data EOD Task
        [Route("GetModuleData")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetModuleData(int page, string orderBy, string order, string search, string filter, bool isAdv)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                int rowCount = 0;
                List<NawaDataDAL.DynamicDictionary> moduleData = NawaDataBLL.EODTaskBLL.GetModuleData(
                    page, orderBy, order, search, filter, isAdv, out rowCount, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value, null);

                return Ok(new { count = rowCount, rows = moduleData });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetModuleData")]
        [Authorize(Policy="ApplicationLock")]
        [HttpPost]
        public ActionResult GetModuleData([FromBody] GenericPagingFilterPostBody body)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                int rowCount = 0;
                List<NawaDataDAL.DynamicDictionary> moduleData = NawaDataBLL.EODTaskBLL.GetModuleData(
                    body.page, body.orderBy, body.order, body.search, body.filter, body.isAdv, out rowCount, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value, body.jsonFilter);

                return Ok(new { count = rowCount, rows = moduleData });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }
        #endregion

        #region GET DETAIL DATA
        // get detail type
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        [Route("GetEODTaskDetailType")]
        public ActionResult GetEODTaskDetailType()
        {
            try
            {
                List<EODDetailType> TaskDetailType = EODTaskBLL.GetEODTaskDetailType();
                return Ok(TaskDetailType);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }
        #endregion

        #region GET EOD EDIT TASK DATA
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        [Route("GetDataEditTask")]
        public ActionResult GetDataEditTask(int taskID)
        {
            try
            {
                EODTask objTask = EODTaskBLL.getTaskByID(taskID);

                if (objTask != null)
                {
                    objTask.ListField = EODTaskBLL.getTaskDetail(taskID);
                }
                else
                {
                    throw new Exception("Task not found");
                }

                return Ok(objTask);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }
        #endregion

        #region Save Task
        [Authorize(Policy="ApplicationLock")]
        [HttpPost]
        [Route("SaveEodTask")]
        public ActionResult SaveEodTask([FromForm]EODTaskParam objParam)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string errorMsg = "";

                MUser currentUser = new MUser()
                {
                    UserID = identity.FindFirst("UserID").Value,
                    PK_MUser_ID = Convert.ToInt32(identity.FindFirst("PK_MUser_ID").Value),
                    FK_MRole_ID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value),
                    FK_MGroupMenu_ID = Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value),
                };
                EODTask dataTask = JsonConvert.DeserializeObject<EODTask>(objParam.cDataTask);
                List<EODTaskDetailView> listData = dataTask.ListField;

                bool CheckApprovalRow = EODTaskBLL.CheckApprovalRow(dataTask.PK_EODTask_ID);
                if (CheckApprovalRow == false)
                {
                    return StatusCode(500, "EOD Task Already Exists in Pending Approval");
                }

                if (currentUser.UserID != null)
                {

                    int counter = 0;
                    for (int i = 0; i < listData.Count; i++)
                    {
                        if ((listData[i].FK_EODTaskDetailType_ID == 1 || listData[i].FK_EODTaskDetailType_ID == 3 || listData[i].FK_EODTaskDetailType_ID == 6) && listData[i].IsEdited)
                        {
                            listData[i].SSISFile = objParam.body[counter++];
                            MemoryStream ms = new MemoryStream();
                            if (listData[i].SSISFile != null)
                            {
                                listData[i].SSISFile.OpenReadStream().CopyTo(ms);
                                listData[i].SSISFIle = ms.ToArray();
                                listData[i].SSISFile = null;
                            }
                        }
                    }

                    if (currentUser.FK_MRole_ID == 1)
                    {
                        EODTaskBLL.SaveTask(objParam.InputMode, dataTask, currentUser.UserID, false, "",_env);
                        errorMsg = "EODTask " + dataTask.EODTaskName + " is saved into Database successfully.";

                        return Ok(errorMsg);
                    }
                    else
                    {
                        errorMsg = EODTaskBLL.SaveTaskWithApproval(objParam.InputMode, dataTask, currentUser.UserID, currentUser.PK_MUser_ID, currentUser.FK_MRole_ID);
                        if (string.IsNullOrEmpty(errorMsg))
                        {
                            return Ok("Pending To Approval");
                        }
                        else
                        {
                            return StatusCode(400, errorMsg);
                        }
                    }
                    
                }
                return Ok(errorMsg);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        #endregion

        #region Action Task Approval
        [Route("ActionTaskApproval")]
        [HttpPost]
        [Authorize(Policy="ApplicationLock")]
        public ActionResult ActionTaskApproval(CParameterApproval parameterApproval)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string CurrentUser = identity.FindFirst("UserID").Value;
                int pk_MUser_ID = Convert.ToInt32(identity.FindFirst("PK_MUser_ID").Value);
                if (parameterApproval.PK_ModuleApproval_IDSelected != null && parameterApproval.PK_ModuleApproval_IDSelected.Length > 0)
                {
                    foreach (long pk in parameterApproval.PK_ModuleApproval_IDSelected)
                    {
                        CParameterApproval approvalData = new CParameterApproval
                        {
                            PK_ModuleApproval_ID = pk,
                            ActionApproval = parameterApproval.ActionApproval
                        };
                        EODTaskBLL.ActionTaskApproval(approvalData, CurrentUser, pk_MUser_ID, _env);
                    }
                }
                else
                {
                    EODTaskBLL.ActionTaskApproval(parameterApproval, CurrentUser, pk_MUser_ID, _env);
                }
                return Ok("Success");
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }
        #endregion

        #region Delete Task
        [Authorize(Policy="ApplicationLock")]
        [HttpPost]
        [Route("deleteEODTask")]
        public ActionResult deleteEODTask([FromForm] EODTaskParam objParam)
        {
            try
            {
                EODTask dataTask = JsonConvert.DeserializeObject<EODTask>(objParam.cDataTask);
                //EODTask objDeleteTask = EODTaskBLL.getTaskByID(objParam.TaskID);

                /* --- Cek hak akses user --- */
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MUser currentUser = new MUser()
                {
                    UserID = identity.FindFirst("UserID").Value,
                    PK_MUser_ID = Convert.ToInt32(identity.FindFirst("PK_MUser_ID").Value),
                    FK_MRole_ID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value),
                };
                string errorMsg = "";

                List<EODTaskDetailView> listData = dataTask.ListField;

                bool CheckApprovalRow = EODTaskBLL.CheckApprovalRow(dataTask.PK_EODTask_ID);
                if (CheckApprovalRow == false)
                {
                    return StatusCode(500, "EOD Task Already Exists in Pending Approval");
                }

                int counter = 0;
                for (int i = 0; i < listData.Count; i++)
                {
                    if ((listData[i].FK_EODTaskDetailType_ID == 1 || listData[i].FK_EODTaskDetailType_ID == 3) && listData[i].IsEdited)
                    {
                        listData[i].SSISFile = objParam.body[counter++];
                        MemoryStream ms = new MemoryStream();
                        if (listData[i].SSISFile != null)
                        {
                            listData[i].SSISFile.OpenReadStream().CopyTo(ms);
                            listData[i].SSISFIle = ms.ToArray();
                            listData[i].SSISFile = null;
                        }
                    }
                }

                if (currentUser.FK_MRole_ID == 1)
                {
                    errorMsg = EODTaskBLL.deleteTask(dataTask,  currentUser.UserID, false, "");
                    if (!string.IsNullOrEmpty(errorMsg))
                    {
                        return StatusCode(400, errorMsg);
                    }
                    else
                    {
                        errorMsg = "EODTask " + dataTask.EODTaskName + " is deleted successfully.";
                    }
                    return Ok(errorMsg);
                }
                else
                {
                    errorMsg = EODTaskBLL.SaveTaskWithApproval(objParam.InputMode, dataTask, currentUser.UserID, currentUser.PK_MUser_ID, currentUser.FK_MRole_ID);
                    if (string.IsNullOrEmpty(errorMsg))
                    {
                        return Ok("Pending To Approval");
                    }
                    else
                    {
                        return StatusCode(400, errorMsg);
                    }
                }
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        #endregion

        #region Activate Task
        [Authorize(Policy="ApplicationLock")]
        [HttpPost]
        [Route("activateEODTask")]
        public ActionResult activateEODTask([FromForm] EODTaskParam objParam)
        {
            try
            {
                EODTask dataTask = JsonConvert.DeserializeObject<EODTask>(objParam.cDataTask);

                /* --- Cek hak akses user --- */
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MUser currentUser = new MUser()
                {
                    UserID = identity.FindFirst("UserID").Value,
                    PK_MUser_ID = Convert.ToInt32(identity.FindFirst("PK_MUser_ID").Value),
                    FK_MRole_ID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value),
                };

                string errorMsg = "";

                EODTask objActivateTask = EODTaskBLL.getTaskByID(dataTask.PK_EODTask_ID);
                string activateStr = objActivateTask.Active ? "deactivated" : "activated";

                List<EODTaskDetailView> listData = dataTask.ListField;

                bool CheckApprovalRow = EODTaskBLL.CheckApprovalRow(dataTask.PK_EODTask_ID);
                if (CheckApprovalRow == false)
                {
                    return StatusCode(500, "EOD Task Already Exists in Pending Approval");
                }

                int counter = 0;
                for (int i = 0; i < listData.Count; i++)
                {
                    if ((listData[i].FK_EODTaskDetailType_ID == 1 || listData[i].FK_EODTaskDetailType_ID == 3) && listData[i].IsEdited)
                    {
                        listData[i].SSISFile = objParam.body[counter++];
                        MemoryStream ms = new MemoryStream();
                        if (listData[i].SSISFile != null)
                        {
                            listData[i].SSISFile.OpenReadStream().CopyTo(ms);
                            listData[i].SSISFIle = ms.ToArray();
                            listData[i].SSISFile = null;
                        }
                    }
                }

                if (currentUser.FK_MRole_ID == 1)
                {
                    dataTask.Active = !dataTask.Active;
                    EODTaskBLL.activateTask(dataTask, currentUser.UserID, false, "");
                    errorMsg = "EODTask " + dataTask.EODTaskName + " is " + activateStr + " successfully.";

                    return Ok(errorMsg);
                }
                else
                {
                    dataTask.Active = !dataTask.Active;
                    errorMsg = EODTaskBLL.SaveTaskWithApproval(objParam.InputMode, dataTask, currentUser.UserID, currentUser.PK_MUser_ID, currentUser.FK_MRole_ID);
                    if (string.IsNullOrEmpty(errorMsg))
                    {
                        return Ok("Pending To Approval");
                    }
                    else
                    {
                        return StatusCode(400, errorMsg);
                    }
                }
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        #endregion

        #region Export Module Task Data
        [Route("ExportModuleData")]
        [HttpGet]
        [Authorize(Policy="ApplicationLock")]
        public IActionResult ExportModuleData(int page, string orderBy, string order, string search, string filter)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MemoryStream ms = NawaDataBLL.EODTaskBLL.ExportModuleData(
                    page, orderBy, order, search, filter, false, "", null, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value, null);

                ms.Position = 0;
                return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Data.xlsx");
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ExportModuleData")]
        [HttpPost]
        [Authorize(Policy="ApplicationLock")]
        public IActionResult ExportModuleData([FromBody] GenericPagingFilterPostBody body)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MemoryStream ms = NawaDataBLL.EODTaskBLL.ExportModuleData(
                    body.page, body.orderBy, body.order, body.search, body.filter, body.isAdv, body.exportAs, body.fields, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value, body.jsonFilter);

                ms.Position = 0;
                return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Data.xlsx");
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }
        #endregion

        #region Export Module All Task Data
        [Route("ExportModuleDataAll")]
        [HttpGet]
        [Authorize(Policy="ApplicationLock")]
        public IActionResult ExportModuleDataAll(string orderBy, string order, string search, string filter)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MemoryStream ms = NawaDataBLL.EODTaskBLL.ExportModuleDataAll(
                    orderBy, order, search, filter, false, "", null, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value, null);

                ms.Position = 0;
                return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Data.xlsx");
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ExportModuleDataAll")]
        [HttpPost]
        [Authorize(Policy="ApplicationLock")]
        public IActionResult ExportModuleDataAll([FromBody] GenericPagingFilterPostBody body)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MemoryStream ms = NawaDataBLL.EODTaskBLL.ExportModuleDataAll(
                    body.orderBy, body.order, body.search, body.filter, body.isAdv, body.exportAs, body.fields, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value, body.jsonFilter);

                ms.Position = 0;
                return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Data.xlsx");
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }
        #endregion

        #region Export Module Selected Task Data
        [Route("ExportModuleDataSelected")]
        [HttpPost]
        [Authorize(Policy="ApplicationLock")]
        public IActionResult ExportModuleDataSelected(NawaDataDAL.Models.TaskSelectedData selectedData)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MemoryStream ms = NawaDataBLL.EODTaskBLL.ExportModuleDataSelected(selectedData.SelectedData, selectedData.orderBy, selectedData.order, selectedData.exportAs, selectedData.fields, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value));

                ms.Position = 0;
                return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Data.xlsx");
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }
        #endregion

        #region FUNCTION PARAMETER
        public class EODTaskParam
        {
            public EActionType InputMode { get; set; }
            public string cDataTask { get; set; }
            public int TaskID { get; set; }
            public List<IFormFile> body { get; set; }
        }
        #endregion

        #region Check PK_EODTaskDetailLog_ID
        [Route("Check_PK_EODTaskDetailLog_ID")]
        [HttpPost]
        [Authorize(Policy="ApplicationLock")]
        public string PK_EODTaskDetailLog_ID(string SPName, bool IsUseParameterBranch, bool IsUseParameterProcessDate)
        {
            string res = NawaDataBLL.EODTaskBLL.Check_PK_EODTaskDetailLog_ID(SPName, IsUseParameterBranch, IsUseParameterProcessDate);

            return res;
        }
        #endregion

        #region Get Module Access
        [Route("GetModuleAccess")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetModuleAccess(int moduleID)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MGroupMenuAccess menuAccess = CommonBLL.GetModuleAccess(moduleID, identity.FindFirst("FK_MGroupMenu_ID").Value);

                return Ok(menuAccess);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }
        #endregion

        #endregion
    }
}
