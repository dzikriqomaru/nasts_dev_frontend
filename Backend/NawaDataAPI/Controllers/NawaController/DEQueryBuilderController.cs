﻿using DevExpress.AspNetCore.Reporting.QueryBuilder;
using DevExpress.AspNetCore.Reporting.QueryBuilder.Native.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NawaDataAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DEQueryBuilderController : QueryBuilderController
    {
        public DEQueryBuilderController(IQueryBuilderMvcControllerService controllerService) : base(controllerService) { }
        public IActionResult Index()
        {
            return View();
        }

        [Route("DXXQB")]
        [HttpPost]
        public override Task<IActionResult> Invoke()
        {
            return base.Invoke();
        }
    }
}
