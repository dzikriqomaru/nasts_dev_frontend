﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NawaDataBLL;
using NawaDataDAL;
using NawaDataDAL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Security.Claims;

namespace NawaDataAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MGroupAccessController : ControllerBase
    {
        #region Get Group Menu
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        [Route("getNewGroupMenuForAccess")]
        public ActionResult GetNewGroupMenuForAccess()
        {
            try
            {
                List<MGroupMenu> listGroupMenu = MGroupAccessBLL.GetNewGroupMenuForAccess();
                return Ok(listGroupMenu);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        [Route("getGroupMenuByID")]
        public ActionResult GetGroupMenuByID(int groupMenuID)
        {
            try
            {
                MGroupMenu objGroupMenu = MGroupAccessBLL.GetGroupMenuByID(groupMenuID);
                return Ok(objGroupMenu);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        [Route("getGroupMenuByName")]
        public ActionResult GetGroupMenuByName(string groupMenuName, int page, string orderBy, string order)
        {
            try
            {
                int rowCount = 0;
                List<DynamicDictionary> listGroupMenu = MGroupAccessBLL.GetGroupMenuByName(groupMenuName, page, orderBy, order, out rowCount);
                return Ok(new { TotalRow = rowCount, ListData = listGroupMenu });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }
        #endregion

        #region Get Group Menu Access
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        [Route("getGroupMenuAccessFieldByNameAndLabel")]
        public ActionResult GetMGroupMenuAccessFieldByNameAndLabel(string groupMenuName, string moduleLabel, int page, string orderBy, string order)
        {
            try
            {
                int rowCount = 0;
                List<DynamicDictionary> listGroupMenuAccess = MGroupAccessBLL.GetMGroupMenuAccessFieldByNameAndLabel(groupMenuName, moduleLabel, page, orderBy, order, out rowCount);
                return Ok(new { TotalRow = rowCount, ListData = listGroupMenuAccess });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        [Route("getGroupMenuAccess")]
        public ActionResult GetGroupMenuAccess(int groupMenuID)
        {
            try
            {
                if (groupMenuID == 0)
                {
                    ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                    groupMenuID = Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value);
                }
                List<MGroupMenuAccess> listMenuAccess = MGroupAccessBLL.GetMenuAccessByGroupMenu(groupMenuID);
                return Ok(listMenuAccess);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        [Route("getGroupMenuAccessByPaging")]
        public ActionResult GetGroupMenuAccess(int groupMenuID, int pageIndex, int pageSize, string filters, string sorts)
        {
            try
            {
                string fields = "*";
                string table = "(SELECT " +
                                "   c.PK_MGroupMenuAcess_ID, xxx.PK_MGroupMenu_ID FK_GroupMenu_ID, xxx.GroupMenuName, xxx.PK_Module_ID FK_Module_ID, xxx.ModuleLabel, " +
                                "   CASE WHEN isnull(m.IsSupportAdd, 0) = 1 THEN CASE WHEN isnull(c.bAdd, 0)= 'true' THEN CONVERT(BIT,1) ELSE CONVERT(BIT,0) END ELSE CONVERT(BIT, 0) END bAdd, " +
                                "   CASE WHEN isnull(m.IsSupportEdit,0)= 1 THEN CASE WHEN isnull(c.bEdit,0)= 'true' THEN CONVERT(BIT,1) ELSE CONVERT(BIT,0) END ELSE CONVERT(BIT, 0) END bEdit, " +
                                "   CASE WHEN isnull(m.IsSupportDetail,0)= 1 THEN CASE WHEN isnull(c.bDetail,0)= 'true' THEN CONVERT(BIT,1) ELSE CONVERT(BIT,0) END ELSE CONVERT(BIT, 0) END bDetail, " +
                                "   CASE WHEN isnull(m.IsSupportDelete,0)= 1 THEN CASE WHEN isnull(c.bDelete,0)= 'true' THEN CONVERT(BIT,1) ELSE CONVERT(BIT,0) END ELSE CONVERT(BIT, 0) END bDelete, " +
                                "   CASE WHEN isnull(m.IsSupportActivation,0)= 1 THEN CASE WHEN isnull(c.bActivation,0)= 'true' THEN CONVERT(BIT,1) ELSE CONVERT(BIT,0) END ELSE CONVERT(BIT, 0) END bActivation, " +
                                "   CASE WHEN isnull(m.IsSupportView,0)= 1 THEN CASE WHEN isnull(c.bView,0) = 'true' THEN CONVERT(BIT,1) ELSE CONVERT(BIT,0) END ELSE CONVERT(BIT, 0) END bView, " +
                                "   CASE WHEN isnull(m.IsUseApproval,0)= 1 THEN CASE WHEN isnull(c.bApproval,0)= 'true' THEN CONVERT(BIT,1) ELSE CONVERT(BIT,0) END ELSE CONVERT(BIT, 0) END bApproval, " +
                                "   CASE WHEN isnull(m.IsSupportUpload,0)= 1 THEN CASE WHEN isnull(c.bUpload,0)= 'true' THEN CONVERT(BIT,1) ELSE CONVERT(BIT,0) END ELSE CONVERT(BIT, 0) END bUpload, " +
                                "   m.IsSupportAdd, m.IsSupportEdit, m.IsSupportDelete, m.IsSupportActivation, " +
                                "   m.IsSupportView, m.IsSupportUpload, m.IsSupportDetail,m.IsUseApproval" +
                                "   FROM (  SELECT m.PK_Module_ID, b.PK_MGroupMenu_ID, b.GroupMenuName, m.ModuleLabel " +
                                "           FROM Module m CROSS JOIN MGroupMenu b " +
                                "       ) xxx " +
                                "   LEFT JOIN MGroupMenuAccess c ON xxx.PK_MGroupMenu_ID = c.FK_GroupMenu_ID " +
                                "     AND xxx.PK_Module_ID = c.FK_Module_ID " +
                                "   LEFT JOIN Module m ON m.PK_Module_ID = xxx.PK_Module_ID" +
                                ") GMA ";

                string whereCond = " FK_GroupMenu_ID = " + groupMenuID;
                if (!string.IsNullOrEmpty(filters)) whereCond += " AND " + filters;

                string sortCond = " ModuleLabel";
                if (!string.IsNullOrEmpty(sorts)) sortCond += ", " + sorts;

                int totalRow = 0;
                DataTable dtResult = CommonBLL.ExecuteTablePaging(table, fields, whereCond, sortCond, pageIndex, pageSize, out totalRow);
                List<MGroupMenuAccess> result = NawaDAO.ConvertDataTable<MGroupMenuAccess>(dtResult);

                return Ok(new { TotalRow = totalRow, ListData = result });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }
        
        #endregion

        #region Get Group Menu Setting
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        [Route("getNewMenuSettingForGroupMenu")]
        public ActionResult GetNewMenuSettingForGroupMenu(int groupMenuID)
        {
            try
            {
                List<MGroupMenuSettting> listMenuSetting = MGroupAccessBLL.GetNewMenuSettingForGroupMenu(groupMenuID);
                return Ok(listMenuSetting);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        [Route("getMenuSettingByGroupMenu")]
        public ActionResult GetMenuSettingByGroupMenu(int groupMenuID)
        {
            try
            {
                List<MGroupMenuSettting> listMenuSetting = MGroupAccessBLL.GetMenuSettingByGroupMenu(groupMenuID);
                return Ok(listMenuSetting);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        #endregion

        #region Save Menu Access
        [Authorize(Policy="ApplicationLock")]
        [HttpPost]
        [Route("saveGroupMenuAccess")]
        public ActionResult SaveGroupMenuAccess(MGroupAccessParam objParam)
        {
            try
            {
                Module objSchemaModule = ModuleBLL.GetModuleByID(objParam.ModuleID);
                if (objSchemaModule == null)
                    return StatusCode(500, "Schema Module not found");

                /* --- Cek hak akses user --- */
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MUser currentUser = new MUser()
                {
                    UserID = identity.FindFirst("UserID").Value,
                    FK_MRole_ID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value),
                    FK_MGroupMenu_ID = Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value),
                    PK_MUser_ID = Convert.ToInt32(identity.FindFirst("PK_MUser_ID").Value),
                };

                if (!CommonBLL.CheckMenuAccess(currentUser.FK_MGroupMenu_ID, objSchemaModule.PK_Module_ID, objParam.InputMode == "I" ? EActionType.Insert : EActionType.Update))
                    return StatusCode(500, "You don't have authorization to access this menu.");

                /* Cek group menu */
                MGroupMenu objGroupMenu = MGroupAccessBLL.GetGroupMenuByID(objParam.GroupMenuID);

                if (objGroupMenu == null)
                    return StatusCode(500, "Group Menu not found");

                string invalidMsg = ValidateSaveGroupMenuAccess(objParam);

                if (!string.IsNullOrEmpty(invalidMsg))
                    return StatusCode(500, invalidMsg);

                string successMsg = "";
                objGroupMenu.CreatedBy = currentUser.UserID;

                if (!objSchemaModule.IsUseApproval || currentUser.FK_MRole_ID == 1)
                {
                    MGroupAccessBLL.SaveMenuAccessNoApproval(objParam.InputMode == "I" ? EActionType.Insert : EActionType.Update, objGroupMenu, objParam.ListMenuAccess, objParam.ListMenuSetting, objSchemaModule);
                    successMsg = objSchemaModule.ModuleLabel + " for Group Menu " + objGroupMenu.GroupMenuName + " is saved into Database successfully.";
                    MGroupAccessBLL.GenerateMenuTemplate(objGroupMenu.PK_MGroupMenu_ID, objParam.ListMenuAccess);
                }
                else
                {
                    MGroupAccessBLL.SaveMenuAccessWithApproval(objParam.InputMode == "I" ? EActionType.Insert : EActionType.Update, objParam, objGroupMenu, currentUser.UserID, currentUser.PK_MUser_ID, currentUser.FK_MRole_ID);
                    successMsg = objSchemaModule.ModuleLabel + " for Group Menu " + objGroupMenu.GroupMenuName + " is saved into pending approval.";
                }

                return Ok(successMsg);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [HttpPost]
        [Route("actionMenuAccessApproval")]
        public ActionResult ActionMenuAccessApproval(CParameterApproval objParam)
        {
            try
            {
                string msg = "";
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MUser currentUser = new MUser()
                {
                    PK_MUser_ID = Convert.ToInt32(identity.FindFirst("PK_MUser_ID").Value),
                    UserID = identity.FindFirst("UserID").Value,
                    FK_MGroupMenu_ID = Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value)
                };

                if (!CommonBLL.CheckMenuAccessByApprovalID(currentUser.FK_MGroupMenu_ID, objParam.PK_ModuleApproval_ID, EActionType.Approval))
                {
                    return StatusCode(500, "You don't have authorization to access this menu.");
                }                  

                if (objParam.ActionApproval == EActionApprovalType.Approve)
                {
                    MGroupAccessBLL.ApproveApproval(objParam.PK_ModuleApproval_ID, currentUser.UserID, currentUser.PK_MUser_ID);
                    msg = "Data approved successfully";
                }
                else if (objParam.ActionApproval == EActionApprovalType.Reject)
                {
                    MGroupAccessBLL.RejectApproval(objParam.PK_ModuleApproval_ID, currentUser.UserID, currentUser.PK_MUser_ID);
                    msg = "Data rejected successfully";
                }
                
                return Ok(msg);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        private string ValidateSaveGroupMenuAccess(MGroupAccessParam objParam)
        {
            string validateMessage = "";

            if (objParam.ListMenuAccess.Count == 0)
                validateMessage += "Menu Access must be defined" + Environment.NewLine;
            else
                objParam.ListMenuAccess.ForEach(x => x.FK_GroupMenu_ID = objParam.GroupMenuID);

            if (objParam.ListMenuSetting.Count == 0)
                validateMessage += "Menu Setting must be defined" + Environment.NewLine;
            else
                objParam.ListMenuSetting.ForEach(x => x.FK_MGroupMenu_ID = objParam.GroupMenuID);

            return validateMessage;
        }

        #endregion

        #region Delete Menu Access
        [Authorize(Policy="ApplicationLock")]
        [HttpPost]
        [Route("deleteGroupMenuAccess")]
        public ActionResult DeleteGroupMenuAccess(MGroupAccessParam objParam)
        {
            try
            {
                Module objSchemaModule = ModuleBLL.GetModuleByID(objParam.ModuleID);
                if (objSchemaModule == null)
                    return StatusCode(500, "Schema Module not found");

                /* --- Cek hak akses user --- */
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MUser currentUser = new MUser()
                {
                    UserID = identity.FindFirst("UserID").Value,
                    FK_MRole_ID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value),
                    FK_MGroupMenu_ID = Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value)
                };

                if (!CommonBLL.CheckMenuAccess(currentUser.FK_MGroupMenu_ID, objSchemaModule.PK_Module_ID, EActionType.Delete))
                    return StatusCode(500, "You don't have authorization to access this menu.");

                /* Cek group menu */
                MGroupMenu objGroupMenu = MGroupAccessBLL.GetGroupMenuByID(objParam.GroupMenuID);
                if (objGroupMenu == null)
                    return StatusCode(500, "Group Menu not found");

                string successMsg = "";
                objGroupMenu.CreatedBy = currentUser.UserID;
                if (!objSchemaModule.IsUseApproval || currentUser.FK_MRole_ID == 1)
                {
                    MGroupAccessBLL.DeleteMenuAccessNoApproval(objGroupMenu, objSchemaModule);
                    successMsg = objSchemaModule.ModuleLabel + " for Group Menu " + objGroupMenu.GroupMenuName + " is deleted successfully.";
                }
                else
                {
                    MGroupAccessBLL.DeleteMenuAccessWithApproval(objGroupMenu, objSchemaModule);
                    successMsg = objSchemaModule.ModuleLabel + " for Group Menu " + objGroupMenu.GroupMenuName + " is saved into pending approval to be deleted.";
                }

                return Ok(successMsg);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        #endregion

        [Route("GetModuleAccess")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetModuleAccess(int moduleID)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MGroupMenuAccess menuAccess = CommonBLL.GetModuleAccess(moduleID, identity.FindFirst("FK_MGroupMenu_ID").Value);

                return Ok(menuAccess);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

    }
}