﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using NawaDataBLL;
using NawaDataDAL;
using NawaDataDAL.Models;

namespace NawaDataAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ThemeController : ControllerBase
    {
        [HttpGet]
        public ActionResult Get()
        {
            try
            {
                string GetParameterValue(int id)
                {
                    return SystemParameterBLL.GetSystemParameterGroup(id).SettingValue;
                }

                SystemParameterGroupView titleParameter = SystemParameterBLL.GetSystemParameterGroup(1);
                SystemParameterGroupView themeParameter = SystemParameterBLL.GetSystemParameterGroup(1018);
                SystemParameterGroupView secThemeParameter = SystemParameterBLL.GetSystemParameterGroup(1017);
                SystemParameterGroupView languageParameter = SystemParameterBLL.GetSystemParameterGroup(1019);
                SystemParameterGroupView headerPrimary = SystemParameterBLL.GetSystemParameterGroup(-67);
                SystemParameterGroupView headerSecondary = SystemParameterBLL.GetSystemParameterGroup(-66);


                Boolean enableCaptchaWeb = GetParameterValue(-1).ToLower() == "false" || GetParameterValue(-1) == "0" || GetParameterValue(-1) == "-1" ? false : true;
                Boolean enableCaptchaMobile = GetParameterValue(-2).ToLower() == "false" || GetParameterValue(-2) == "0" || GetParameterValue(-2) == "-1" ? false : true;
                Boolean enableGoogleLogin = GetParameterValue(-7).ToLower() == "false" || GetParameterValue(-7) == "0" || GetParameterValue(-7) == "-1" ? false : true;
                Boolean enableFacebookLogin = GetParameterValue(-8).ToLower() == "false" || GetParameterValue(-8) == "0" || GetParameterValue(-8) == "-1" ? false : true;

                return Ok(new
                {
                    title = titleParameter.SettingValue,
                    themeColor = themeParameter.SettingValue,
                    secThemeColor = secThemeParameter.SettingValue,
                    headerPrimary = headerPrimary.SettingValue,
                    headerSecondary = headerSecondary.SettingValue,
                    language = languageParameter.ApplicationAuthentication,
                    EnableCaptchaWeb = enableCaptchaWeb,
                    EnableCaptchaMobile = enableCaptchaMobile,
                    EnableGoogleLogin = enableGoogleLogin,
                    EnableFacebookLogin = enableFacebookLogin,

                });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }
    }
}

