﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NawaDataBLL;
using NawaDataDAL.Models;

namespace NawaDataAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuditTrailController : ControllerBase
    {
        [Route("GetAuditTrailByID")]
        //[Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetAuditTrailByID(Int64 id)
        {
            try
            {
                NawaDataDAL.Models.AuditTrailHeaderView auditTrailData = NawaDataBLL.AuditTrailBLL.GetAuditTrailByID(id);

                return Ok(auditTrailData);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetAuditTrailDetailByID")]
        //[Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetAuditTrailDetailByID(Int64 id, int page, string orderBy, string order)
        {
            try
            {
                int rowCount = 0;
                List<NawaDataDAL.Models.AuditTrailDetail> auditTrailDetails = NawaDataBLL.AuditTrailBLL.GetAuditTrailDetailByID(id, page, orderBy, order, out rowCount);

                return Ok(new { count = rowCount, rows = auditTrailDetails });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetChangesData")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetChangesData(int page, string orderBy, string order, string search, string filter, bool isAdv)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;

                int rowCount = 0;
                List<NawaDataDAL.DynamicDictionary> moduleData = NawaDataBLL.AuditTrailBLL.GetChangesData(
                    page, orderBy, order, search, filter, isAdv, out rowCount, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value, null);

                return Ok(new { count = rowCount, rows = moduleData });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetChangesData")]
        [Authorize(Policy="ApplicationLock")]
        [HttpPost]
        public ActionResult GetChangesData([FromBody] GenericPagingFilterPostBody body)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                int rowCount = 0;
                List<NawaDataDAL.DynamicDictionary> moduleData = NawaDataBLL.AuditTrailBLL.GetChangesData(
                    body.page, body.orderBy, body.order, body.search, body.filter, body.isAdv, out rowCount, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value, body.jsonFilter);

                return Ok(new { count = rowCount, rows = moduleData });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ExportModuleDataChanges")]
        [HttpGet]
        [Authorize(Policy="ApplicationLock")]
        public IActionResult ExportModuleDataChanges(int page, string orderBy, string order, string search, string filter, bool isAdv, string exportAs)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MemoryStream ms = NawaDataBLL.AuditTrailBLL.ExportModuleDataChanges(
                    page, orderBy, order, search, filter, isAdv, exportAs, null, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value, null);

                ms.Position = 0; ms.Position = 0;

                string mediaType;

                if (exportAs == "Text")
                {
                    mediaType = "text/plain";
                }
                else if (exportAs == "CSV")
                {
                    mediaType = "text/csv";
                }
                else
                {
                    mediaType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                }

                return File(ms, mediaType);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }

        }

        [Route("ExportModuleDataChanges")]
        [HttpPost]
        [Authorize(Policy="ApplicationLock")]
        public IActionResult ExportModuleDataChanges([FromBody] GenericPagingFilterPostBody body)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MemoryStream ms = NawaDataBLL.AuditTrailBLL.ExportModuleDataChanges(
                    body.page, body.orderBy, body.order, body.search, body.filter, body.isAdv, body.exportAs, body.fields, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value, body.jsonFilter);

                ms.Position = 0;
                return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Data.xlsx");
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ExportModuleDataAllChanges")]
        [HttpGet]
        [Authorize(Policy="ApplicationLock")]
        public IActionResult ExportModuleDataAllChanges(string orderBy, string order, string search, string filter, bool isAdv, string exportAs)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MemoryStream ms = NawaDataBLL.AuditTrailBLL.ExportModuleDataAllChanges(
                    orderBy, order, search, filter, isAdv, exportAs, null, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value, null);

                ms.Position = 0; ms.Position = 0;

                string mediaType;

                if (exportAs == "Text")
                {
                    mediaType = "text/plain";
                }
                else if (exportAs == "CSV")
                {
                    mediaType = "text/csv";
                }
                else
                {
                    mediaType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                }

                return File(ms, mediaType);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ExportModuleDataAllChanges")]
        [HttpPost]
        [Authorize(Policy="ApplicationLock")]
        public IActionResult ExportModuleDataAllChanges([FromBody] GenericPagingFilterPostBody body)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MemoryStream ms = NawaDataBLL.AuditTrailBLL.ExportModuleDataAllChanges(
                    body.orderBy, body.order, body.search, body.filter, body.isAdv, body.exportAs, body.fields, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value, body.jsonFilter);

                ms.Position = 0;
                return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Data.xlsx");
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ExportModuleDataSelectedChanges")]
        [HttpPost]
        [Authorize(Policy="ApplicationLock")]
        public IActionResult ExportModuleDataSelectedChanges(NawaDataDAL.Models.CSelectedData selectedData)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MemoryStream ms = NawaDataBLL.AuditTrailBLL.ExportModuleDataSelectedChanges(selectedData.SelectedData, selectedData.orderBy, selectedData.order, selectedData.exportAs, selectedData.fields, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value));

                ms.Position = 0;
                return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Data.xlsx");
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetUserLoginData")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetUserLoginData(int page, string orderBy, string order, string search, string filter, bool isAdv)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                int rowCount = 0;
                List<NawaDataDAL.DynamicDictionary> moduleData = NawaDataBLL.AuditTrailBLL.GetUserLoginData(
                   page, orderBy, order, search, filter, isAdv, out rowCount, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value,
 null);

                return Ok(new { count = rowCount, rows = moduleData });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetUserLoginData")]
        [Authorize(Policy="ApplicationLock")]
        [HttpPost]
        public ActionResult GetUserLoginData([FromBody] GenericPagingFilterPostBody body)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                int rowCount = 0;
                List<NawaDataDAL.DynamicDictionary> moduleData = NawaDataBLL.AuditTrailBLL.GetUserLoginData(
                    body.page, body.orderBy, body.order, body.search, body.filter, body.isAdv, out rowCount, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value, body.jsonFilter);

                return Ok(new { count = rowCount, rows = moduleData });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ExportModuleDataLogin")]
        [HttpGet]
        [Authorize(Policy="ApplicationLock")]
        public IActionResult ExportModuleDataLogin(int page, string orderBy, string order, string search, string filter, bool isAdv, string exportAs)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MemoryStream ms = NawaDataBLL.AuditTrailBLL.ExportModuleDataLogin(
                    page, orderBy, order, search, filter, isAdv, exportAs, null, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value, null);

                ms.Position = 0; ms.Position = 0;

                string mediaType;

                if (exportAs == "Text")
                {
                    mediaType = "text/plain";
                }
                else if (exportAs == "CSV")
                {
                    mediaType = "text/csv";
                }
                else
                {
                    mediaType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                }

                return File(ms, mediaType);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ExportModuleDataLogin")]
        [HttpPost]
        [Authorize(Policy="ApplicationLock")]
        public IActionResult ExportModuleDataLogin([FromBody] GenericPagingFilterPostBody body)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MemoryStream ms = NawaDataBLL.AuditTrailBLL.ExportModuleDataLogin(
                    body.page, body.orderBy, body.order, body.search, body.filter, body.isAdv, body.exportAs, body.fields, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value, body.jsonFilter);

                ms.Position = 0;
                return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Data.xlsx");
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ExportModuleDataAllLogin")]
        [HttpGet]
        [Authorize(Policy="ApplicationLock")]
        public IActionResult ExportModuleDataAllLogin(string orderBy, string order, string search, string filter, bool isAdv, string exportAs)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MemoryStream ms = NawaDataBLL.AuditTrailBLL.ExportModuleDataAllLogin(
                    orderBy, order, search, filter, isAdv, exportAs, null, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value, null);

                ms.Position = 0; ms.Position = 0;

                string mediaType;

                if (exportAs == "Text")
                {
                    mediaType = "text/plain";
                }
                else if (exportAs == "CSV")
                {
                    mediaType = "text/csv";
                }
                else
                {
                    mediaType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                }

                return File(ms, mediaType);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ExportModuleDataAllLogin")]
        [HttpPost]
        [Authorize(Policy="ApplicationLock")]
        public IActionResult ExportModuleDataAllLogin([FromBody] GenericPagingFilterPostBody body)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MemoryStream ms = NawaDataBLL.AuditTrailBLL.ExportModuleDataAllLogin(
                    body.orderBy, body.order, body.search, body.filter, body.isAdv, body.exportAs, body.fields, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value, body.jsonFilter);

                ms.Position = 0;
                return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Data.xlsx");
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ExportModuleDataSelectedLogin")]
        [HttpPost]
        [Authorize(Policy="ApplicationLock")]
        public IActionResult ExportModuleDataSelectedLogin(NawaDataDAL.Models.CSelectedData selectedData)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MemoryStream ms = NawaDataBLL.AuditTrailBLL.ExportModuleDataSelectedLogin(selectedData.SelectedData, selectedData.orderBy, selectedData.order, selectedData.exportAs, selectedData.fields, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value));

                ms.Position = 0; ms.Position = 0;

                string mediaType;

                if (selectedData.exportAs == "Text")
                {
                    mediaType = "text/plain";
                }
                else if (selectedData.exportAs == "CSV")
                {
                    mediaType = "text/csv";
                }
                else
                {
                    mediaType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                }

                return File(ms, mediaType);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetUserAccessData")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetUserAccessData(int page, string orderBy, string order, string search, string filter, bool isAdv)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                int rowCount = 0;
                List<NawaDataDAL.DynamicDictionary> moduleData = NawaDataBLL.AuditTrailBLL.GetUserAccessData(
                    page, orderBy, order, search, filter, isAdv, out rowCount, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value, null);

                return Ok(new { count = rowCount, rows = moduleData });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetUserAccessData")]
        [Authorize(Policy="ApplicationLock")]
        [HttpPost]
        public ActionResult GetUserAccessData([FromBody] GenericPagingFilterPostBody body)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                int rowCount = 0;
                List<NawaDataDAL.DynamicDictionary> moduleData = NawaDataBLL.AuditTrailBLL.GetUserAccessData(
                    body.page, body.orderBy, body.order, body.search, body.filter, body.isAdv, out rowCount, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value, body.jsonFilter);

                return Ok(new { count = rowCount, rows = moduleData });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ExportModuleDataAccess")]
        [HttpGet]
        [Authorize(Policy="ApplicationLock")]
        public IActionResult ExportModuleDataAccess(int page, string orderBy, string order, string search, string filter, bool isAdv, string exportAs)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MemoryStream ms = NawaDataBLL.AuditTrailBLL.ExportModuleDataAccess(
                    page, orderBy, order, search, filter, isAdv, exportAs, null, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value, null);

                ms.Position = 0; ms.Position = 0;

                string mediaType;

                if (exportAs == "Text")
                {
                    mediaType = "text/plain";
                }
                else if (exportAs == "CSV")
                {
                    mediaType = "text/csv";
                }
                else
                {
                    mediaType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                }

                return File(ms, mediaType);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ExportModuleDataAccess")]
        [HttpPost]
        [Authorize(Policy="ApplicationLock")]
        public IActionResult ExportModuleDataAccess([FromBody] GenericPagingFilterPostBody body)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MemoryStream ms = NawaDataBLL.AuditTrailBLL.ExportModuleDataAccess(
                    body.page, body.orderBy, body.order, body.search, body.filter, body.isAdv, body.exportAs, body.fields, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value, body.jsonFilter);

                ms.Position = 0;
                return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Data.xlsx");
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ExportModuleDataAllAccess")]
        [HttpGet]
        [Authorize(Policy="ApplicationLock")]
        public IActionResult ExportModuleDataAllAccess(string orderBy, string order, string search, string filter, bool isAdv, string exportAs)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MemoryStream ms = NawaDataBLL.AuditTrailBLL.ExportModuleDataAllAccess(
                    orderBy, order, search, filter, isAdv, exportAs, null, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value, null);

                ms.Position = 0; ms.Position = 0;

                string mediaType;

                if (exportAs == "Text")
                {
                    mediaType = "text/plain";
                }
                else if (exportAs == "CSV")
                {
                    mediaType = "text/csv";
                }
                else
                {
                    mediaType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                }

                return File(ms, mediaType);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ExportModuleDataAllAccess")]
        [HttpPost]
        [Authorize(Policy="ApplicationLock")]
        public IActionResult ExportModuleDataAllAccess([FromBody] GenericPagingFilterPostBody body)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MemoryStream ms = NawaDataBLL.AuditTrailBLL.ExportModuleDataAllAccess(
                    body.orderBy, body.order, body.search, body.filter, body.isAdv, body.exportAs, body.fields, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value, body.jsonFilter);

                ms.Position = 0;
                return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Data.xlsx");
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ExportModuleDataSelectedAccess")]
        [HttpPost]
        [Authorize(Policy="ApplicationLock")]
        public IActionResult ExportModuleDataSelectedAccess(NawaDataDAL.Models.CSelectedData selectedData)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MemoryStream ms = NawaDataBLL.AuditTrailBLL.ExportModuleDataSelectedAccess(selectedData.SelectedData, selectedData.orderBy, selectedData.order, selectedData.exportAs, selectedData.fields, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value));

                ms.Position = 0; ms.Position = 0;

                string mediaType;

                if (selectedData.exportAs == "Text")
                {
                    mediaType = "text/plain";
                }
                else if (selectedData.exportAs == "CSV")
                {
                    mediaType = "text/csv";
                }
                else
                {
                    mediaType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                }

                return File(ms, mediaType);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetSystemData")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetSystemData(int page, string orderBy, string order, string search, string filter, bool isAdv)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MUser currentUser = new MUser()
                {
                    UserID = identity.FindFirst("UserID").Value,
                    FK_MRole_ID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value),
                    FK_MGroupMenu_ID = Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value)
                };

                if (!CommonBLL.CheckMenuAccess(currentUser.FK_MGroupMenu_ID, 1012, EActionType.View))
                    return StatusCode(500, "You don't have authorization to access this menu.");

                int rowCount = 0;
                string field = "ErrorId,Host,StatusCode,Type,Message,[User],TimeUtc,Sequence";
                List<NawaDataDAL.DynamicDictionary> moduleData = NawaDataBLL.AuditTrailBLL.GetUserSystem(
                    page, orderBy, order, search, filter, isAdv, field, out rowCount, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value, null);
                return Ok(new { count = rowCount, rows = moduleData });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetSystemData")]
        [Authorize(Policy="ApplicationLock")]
        [HttpPost]
        public ActionResult GetSystemData([FromBody] GenericPagingFilterPostBody body)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MUser currentUser = new MUser()
                {
                    UserID = identity.FindFirst("UserID").Value,
                    FK_MRole_ID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value),
                    FK_MGroupMenu_ID = Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value)
                };

                if (!CommonBLL.CheckMenuAccess(currentUser.FK_MGroupMenu_ID, 1012, EActionType.View))
                    return StatusCode(500, "You don't have authorization to access this menu.");

                int rowCount = 0;

                string field = "ErrorId,Host,StatusCode,Type,Message,[User],TimeUtc,Sequence";

                List<NawaDataDAL.DynamicDictionary> moduleData = NawaDataBLL.AuditTrailBLL.GetUserSystem(
                    body.page, body.orderBy, body.order, body.search, body.filter, body.isAdv, field, out rowCount, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value, body.jsonFilter);

                return Ok(new { count = rowCount, rows = moduleData });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetAuditSystemDetail")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetAuditSystemDetail(string id)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                int rowCount = 0;
                string filter = "ErrorId = '" + id + "'";
                List<NawaDataDAL.DynamicDictionary> moduleData = NawaDataBLL.AuditTrailBLL.GetUserSystem(
                    1, "", "", "", filter, false, " * ", out rowCount, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value, null);
                return Ok(new { count = rowCount, rows = moduleData });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ExportModuleDataSystem")]
        [HttpGet]
        [Authorize(Policy="ApplicationLock")]
        public IActionResult ExportModuleDataSystem(int page, string orderBy, string order, string search, string filter, bool isAdv, string exportAs)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MemoryStream ms = NawaDataBLL.AuditTrailBLL.ExportModuleDataSystem(
                    page, orderBy, order, search, filter, isAdv, exportAs, null, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value, null);

                ms.Position = 0; ms.Position = 0;

                string mediaType;

                if (exportAs == "Text")
                {
                    mediaType = "text/plain";
                }
                else if (exportAs == "CSV")
                {
                    mediaType = "text/csv";
                }
                else
                {
                    mediaType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                }

                return File(ms, mediaType);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ExportModuleDataSystem")]
        [HttpPost]
        [Authorize(Policy="ApplicationLock")]
        public IActionResult ExportModuleDataSystem([FromBody] GenericPagingFilterPostBody body)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MemoryStream ms = NawaDataBLL.AuditTrailBLL.ExportModuleDataSystem(
                    body.page, body.orderBy, body.order, body.search, body.filter, body.isAdv, body.exportAs, body.fields, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value, body.jsonFilter);

                ms.Position = 0;
                return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Data.xlsx");
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ExportModuleDataAllSystem")]
        [HttpGet]
        [Authorize(Policy="ApplicationLock")]
        public IActionResult ExportModuleDataAllSystem(string orderBy, string order, string search, string filter, bool isAdv, string exportAs)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MemoryStream ms = NawaDataBLL.AuditTrailBLL.ExportModuleDataAllSystem(
                    orderBy, order, search, filter, isAdv, exportAs, null, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value, null);

                ms.Position = 0; ms.Position = 0;

                string mediaType;

                if (exportAs == "Text")
                {
                    mediaType = "text/plain";
                }
                else if (exportAs == "CSV")
                {
                    mediaType = "text/csv";
                }
                else
                {
                    mediaType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                }

                return File(ms, mediaType);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ExportModuleDataAllSystem")]
        [HttpPost]
        [Authorize(Policy="ApplicationLock")]
        public IActionResult ExportModuleDataAllSystem([FromBody] GenericPagingFilterPostBody body)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MemoryStream ms = NawaDataBLL.AuditTrailBLL.ExportModuleDataAllSystem(
                    body.orderBy, body.order, body.search, body.filter, body.isAdv, body.exportAs, body.fields, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value, body.jsonFilter);

                ms.Position = 0;
                return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Data.xlsx");
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ExportModuleDataSelectedSystem")]
        [HttpPost]
        [Authorize(Policy="ApplicationLock")]
        public IActionResult ExportModuleDataSelectedSystem(NawaDataDAL.Models.CSelectedData selectedData)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MemoryStream ms = NawaDataBLL.AuditTrailBLL.ExportModuleDataSelectedSystem(selectedData.SelectedData, selectedData.orderBy, selectedData.order, selectedData.exportAs, selectedData.fields, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value));

                ms.Position = 0;
                return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Data.xlsx");
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("PostUserAccessData")]
        [Authorize(Policy="ApplicationLock")]
        [HttpPost]
        public ActionResult PostUserAccessData(UserAccessDataRequest request)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string CurrentUser = identity.FindFirst("UserID").Value;
                int pk_MUser_ID = Convert.ToInt32(identity.FindFirst("PK_MUser_ID").Value);
                AuditTrailBLL.InputAuditTrailUserAccess(CurrentUser, pk_MUser_ID, request);

                return Ok();
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetModuleAccess")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetModuleAccess(int moduleID)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MGroupMenuAccess menuAccess = CommonBLL.GetModuleAccess(moduleID, identity.FindFirst("FK_MGroupMenu_ID").Value);

                return Ok(menuAccess);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

    }
}