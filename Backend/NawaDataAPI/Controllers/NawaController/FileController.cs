﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NawaDataBLL;
using NawaDataDAL.Models;

namespace NawaDataAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class FileController : ControllerBase
    {
        [Authorize(Policy="ApplicationLock")]
        [Route("AllFileType")]
        [HttpGet]
        public ActionResult GetAllFileType(string moduleName, string fieldName)
        {
            try
            {
                List<FileType> type = NawaDataBLL.FileBLL.getAllFileType(moduleName, fieldName);
                return Ok(type);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [Route("FileTypeSettingByID")]
        [HttpGet]
        public ActionResult GetFileTypeSettingByID(int id)
        {
            try
            {
                List<FileType> type = NawaDataBLL.FileBLL.getFileTypeSettingByID(id);
                return Ok(type);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }
        [Route("AddAllowFilePerModule")]
        [Authorize(Policy="ApplicationLock")]
        [HttpPost]
        public ActionResult AddAllowFilePerModule(AllowedFilePerModule allowedFilePerModule)
        {
            try
            {
                NawaDataBLL.FileBLL.AllowFilePerModule(allowedFilePerModule);
                return Ok();
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }
        [Authorize(Policy="ApplicationLock")]
        [Route("GetAllFile")]
        [HttpGet]
        public ActionResult GetAllFile()
        {
            try
            {
                List<FileType> type = NawaDataBLL.FileBLL.getAllFile();
                return Ok(type);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }
        [Authorize(Policy="ApplicationLock")]
        [Route("getAllFileEdit")]
        [HttpGet]
        public ActionResult getAllFileEdit(string ModuleName, string FieldName)
        {
            try
            {
                List<NawaDataDAL.DynamicDictionary> fileEdit = NawaDataBLL.FileBLL.getAllFileEdit(ModuleName, FieldName);
                return Ok(fileEdit);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }
        [Authorize(Policy="ApplicationLock")]
        [Route("deleteAllowFilePerModule")]
        [HttpGet]
        public ActionResult deleteAllowFilePerModule(string Mname, string Fname)
        {
            try
            {
                NawaDataBLL.FileBLL.deleteAllFileEdit(Mname, Fname);
                return Ok();
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }
        [Authorize(Policy="ApplicationLock")]
        [Route("GetAllowedFileByModuleName")]
        [HttpGet]
        public ActionResult GetAllowedFileByModuleName(string moduleName)
        {
            try
            {
                List<NawaDataDAL.DynamicDictionary> list = NawaDataBLL.FileBLL.GetAllowedFileByModuleName(moduleName);
                return Ok(list);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [Route("GetAllowedFileModuleDetailByModuleName")]
        [HttpGet]
        public ActionResult GetAllowedFileModuleDetailByModuleName(string moduleName)
        {
            try
            {
                List<NawaDataDAL.DynamicDictionary> list = NawaDataBLL.FileBLL.GetAllowedFileModuleDetailByModuleName(moduleName);
                return Ok(list);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("UpdateAllowFileUploadDetail")]
        [Authorize(Policy="ApplicationLock")]
        [HttpPost]
        public ActionResult UpdateAllowFileUploadDetail(AllowedFileDetailPerModule allowedFileDetailPerModule)
        {
            try
            {
                NawaDataBLL.FileBLL.UpdateAllowFileUploadDetail(allowedFileDetailPerModule);
                return Ok();
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("DownloadFile")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]

        public ActionResult DownloadFile(string fieldName,int id,string fileName, string primaryKey = "", int moduleid = 0, string moduleName = "")
        {
            
            try
            {
                byte[] res = FileBLL.DownloadFile(fieldName, id, primaryKey, moduleid, moduleName);

                return File(res, "application/octet-stream", fileName);
            }
            catch (Exception ex)
            {
                return StatusCode(500,ex.Message);
            
            }
            

        }

        [Route("DownloadFileModuleDetail")]
        [Authorize(Policy = "ApplicationLock")]
        [HttpGet]

        public ActionResult DownloadFileModuleDetail(string fieldName, int id, string fileName, string primaryKey = "", int moduleDetailId = 0, string moduleDetailName = "")
        {
            try
            {
                byte[] res = FileBLL.DownloadFileModuleDetail(fieldName, id, primaryKey, moduleDetailId, moduleDetailName);

                return File(res, "application/octet-stream", fileName);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);

            }


        }
    }
}
