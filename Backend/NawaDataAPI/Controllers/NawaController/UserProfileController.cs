﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NawaDataDAL.Models;
using System.IO;
using System.Security.Claims;
using NawaDataBLL;

namespace NawaDataAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserProfileController : ControllerBase
    {
        [Route("GetProfileData")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetProfileData(int PK_MUser_ID)
        {
            try
            {
                ProfileData Photo = NawaDataBLL.UserBLL.GetProfileData(PK_MUser_ID);

                return Ok(Photo);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string stringUserID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, stringUserID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [Route("SaveProfileData")]
        [HttpPost]
        public ActionResult SaveProfileData([FromBody] CProfile profile)
        {
            try
            {
                UserBLL.SaveProfileData(profile);
                return Ok("Profile data has been saved.");
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string stringUserID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, stringUserID);
                return StatusCode(500, ex.Message);
            }
        }

        //[Authorize(Policy="ApplicationLock")]
        [Route("SavePassword")]
        [HttpPost]
        public ActionResult SavePassword([FromBody] CProfile profile)
        {
            try
            {
                profile.NewPassword = EncryptionBLL.DecryptStringAES(profile.NewPassword);
                profile.OldPassword = EncryptionBLL.DecryptStringAES(profile.OldPassword);
                profile.confirmPassword = EncryptionBLL.DecryptStringAES(profile.confirmPassword);

                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                int MRoleID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value);
                MUser user = new MUser();
                bool success = false;
                string msg = UserBLL.SaveUserPassword(MRoleID,profile,out user,out success);
               
                if (user == null)
                {
                    return BadRequest(msg);
                }
                else
                {
                    if (success){                    
                        return Ok(msg);
                    }
                    else
                    {
                        return BadRequest(msg);
                    }
                    
                }
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string stringUserID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, stringUserID);
                return StatusCode(500, ex.Message);
            }
        }
    }
}
