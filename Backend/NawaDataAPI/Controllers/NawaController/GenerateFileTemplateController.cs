﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using NawaDataBLL;
using NawaDataDAL;
using System.IO;
using NawaDataDAL.Models;

namespace NawaDataAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GenerateFileTemplateController : ControllerBase
    {
        [Route("GetModuleData")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetModuleData(int page, string orderBy, string order, string search, string filter)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                int rowCount = 0;
                List<DynamicDictionary> moduleData = GenerateFileTemplateBLL.GetModuleData(
                    page, orderBy, order, search, filter, out rowCount, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value, null);

                return Ok(new { count = rowCount, rows = moduleData });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetModuleData")]
        [Authorize(Policy="ApplicationLock")]
        [HttpPost]
        public ActionResult GetModuleData([FromBody] GenericPagingFilterPostBody body)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                int rowCount = 0;
                List<DynamicDictionary> moduleData = GenerateFileTemplateBLL.GetModuleData(
                    body.page, body.orderBy, body.order, body.search, "", out rowCount, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value, body.jsonFilter);

                return Ok(new { count = rowCount, rows = moduleData });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [Route("validationQuery")]
        [HttpPost]
        public ActionResult validationQuery(GenerateFileTemplateAdditional queryTable)
        {
            try
            {
                List<string> col = GenerateFileTemplateBLL.ValidationQuery(queryTable);
                return Ok(col);

            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [Route("saveGenerateFileTemplate")]
        [HttpPost]
        public ActionResult saveGenerateFileTemplate(GenerateFileTemplateBLL.saveFile file)
        {
            try
            {
                //string currentUser = "Sysadmin";
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string CurrentUser = identity.FindFirst("UserID").Value;
                int MRoleID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value);

                GenerateFileTemplateBLL.saveGenerateFileWithoutApproval(file, CurrentUser);
                return Ok("Sucess");
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [Route("GetGenerateFileTemplate")]
        [HttpGet]
        public ActionResult GetGenerateFileTemplate(int FileID)
        {
            try
            {
                GenerateFileTemplateView file = GenerateFileTemplateBLL.GetGenerateFileTemplate(FileID);
                return Ok(file);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetModuleAccess")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetModuleAccess(int moduleID)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MGroupMenuAccess menuAccess = CommonBLL.GetModuleAccess(moduleID, identity.FindFirst("FK_MGroupMenu_ID").Value);

                return Ok(menuAccess);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }
    }
}