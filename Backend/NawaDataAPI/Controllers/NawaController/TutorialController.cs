﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NawaDataBLL;
using NawaDataDAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace NawaDataAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TutorialController : ControllerBase
    {
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        [Route("GetTutorial")]
        public ActionResult GetTutorial(int moduleId, int actionType, string pageUrl)
        {
            ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
            string userID = identity.FindFirst("UserID").Value;

            try
            {
                List<Tutorial> listTutorial = TutorialBLL.GetTutorial(moduleId, actionType, pageUrl);

                var result = new List<object>();
                foreach (Tutorial tutorial in listTutorial)
                {
                    result.Add(new
                    {
                        pk_tutorial_id = tutorial.pk_tutorial_id,
                        title = tutorial.title,
                        description = tutorial.description,
                        tutorial = tutorial.tutorial,
                    });
                }

                return Ok(result);
            }
            catch (Exception ex)
            {
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }
    }
}