﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using System.Security.Claims;
using NawaDataBLL;

namespace NawaDataAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DataFusionController : ControllerBase
    {
        [Route("GetInstances")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetInstances()
        {
            try
            {
                List<Dictionary<string, dynamic>> instancesList = NawaDataBLL.DataFusionBLL.getInstances();
                return Ok(instancesList);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetPipelines")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetPipelines(string instanceName, string region)
        {
            try
            {
                string pipelines = NawaDataBLL.DataFusionBLL.getPipelines(instanceName, region);
                return Ok(pipelines);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }
    }
}
