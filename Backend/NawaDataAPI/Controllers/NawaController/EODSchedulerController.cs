﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using NawaDataBLL;
using NawaDataDAL;
using NawaDataDAL.Models;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using System.Diagnostics.Tracing;
using System.IO;
using System.Data.SqlClient;

namespace NawaDataAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EODSchedulerController : ControllerBase
    {
        #region Master EODScheduler

        #region Get Module Data EOD Scheduler
        [Route("GetModuleData")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetModuleData(int page, string orderBy, string order, string search, string filter, bool isAdv, bool isRunProcess)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                int rowCount = 0;
                List<NawaDataDAL.DynamicDictionary> moduleData = NawaDataBLL.EODSchedulerBLL.GetModuleData(page, orderBy, order, search, filter, isAdv, isRunProcess, out rowCount, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value, null);

                return Ok(new { count = rowCount, rows = moduleData });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetModuleData")]
        [Authorize(Policy="ApplicationLock")]
        [HttpPost]
        public ActionResult GetModuleData([FromBody] GenericPagingFilterPostBody body)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                int rowCount = 0;
                List<NawaDataDAL.DynamicDictionary> moduleData = NawaDataBLL.EODSchedulerBLL.GetModuleData(body.page, body.orderBy, body.order, body.search, body.filter, body.isAdv, body.isRunProcess, out rowCount, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value, body.jsonFilter);

                return Ok(new { count = rowCount, rows = moduleData });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }
        #endregion

        #region Get EOD Task
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        [Route("GetEODTask")]
        public ActionResult GetEODTask()
        {
            try
            {
                List<EODTask> getTask = EODSchedulerBLL.GetEODTask();
                return Ok(getTask);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }
        #endregion

        #region Get EOD Period
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        [Route("GetMSEODPeriod")]
        public ActionResult GetMSEODPeriod()
        {
            try
            {
                List<EODPeriod> getMsPeriod = EODSchedulerBLL.GetMSEODPeriod();
                return Ok(getMsPeriod);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }
        #endregion

        #region GET EOD EDIT SCHEDULER DATA
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        [Route("GetDataEditScheduler")]
        public ActionResult GetDataEditScheduler(int schedulerID)
        {
            try
            {
                EODScheduler objScheduler = EODSchedulerBLL.GetSchedulerByID(schedulerID);

                if (objScheduler != null)
                {
                    objScheduler.ListField = EODSchedulerBLL.GetSchedulerDetail(schedulerID);
                }
                else
                {
                    throw new Exception("Scheduler not found");
                }

                return Ok(objScheduler);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }
        #endregion

        #region Save Scheduler
        [Authorize(Policy = "ApplicationLock")]
        [HttpPost]
        [Route("SaveScheduler")]
        public ActionResult SaveScheduler(SchedulerParam objParam)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string UserID = identity.FindFirst("UserID").Value;
                int MRoleID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value);
                int PK_MUser_ID = Convert.ToInt32(identity.FindFirst("PK_MUser_ID").Value);

                string errorMessage;

                Module objSchemaModule = ModuleBLL.GetModuleByID(objParam.ModuleID);
                if (objSchemaModule == null) return StatusCode(500, "Schema Module not found");

                bool CheckApprovalRow = EODSchedulerBLL.CheckApprovalRow(objParam.DataScheduler.PK_EODScheduler_ID, objParam.ModuleID);
                if (CheckApprovalRow == false)
                {
                    return StatusCode(500, "Data Already Exists in Pending Approval");
                }
                else
                {
                    int nameStatus = EODSchedulerBLL.IsSchedulerNameUnique(objParam.DataScheduler);
                    if (nameStatus == 0)
                    {
                        if (!objSchemaModule.IsUseApproval || MRoleID == 1)
                        {
                            EODSchedulerBLL.SaveScheduler(objParam.InputMode, objParam.DataScheduler, UserID, false);
                            return Ok("Scheduler " + objParam.DataScheduler.EODSchedulerName + " is saved into Database successfully.");
                        }
                        else
                        {
                            errorMessage = EODSchedulerBLL.SaveSchedulerWithApproval(objParam.InputMode, objParam.DataScheduler, UserID, MRoleID, PK_MUser_ID, objParam.ModuleID);
                            if (string.IsNullOrEmpty(errorMessage))
                            {
                                return Ok("Pending To Approval");
                            }
                            else
                            {
                                return StatusCode(400, errorMessage);
                            }
                        }
                    }
                    else
                    {
                        string msg = "";
                        if (nameStatus == 1)
                        {
                            msg = " Already Exist";
                        }
                        else if (nameStatus == 2)
                        {
                            msg = " Already Exist In Pending Approval";
                        }
                        return StatusCode(500, objParam.DataScheduler.EODSchedulerName + msg);
                    }
                }
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }
        #endregion

        #region Action Scheduler Approval
        [Route("ActionSchedulerApproval")]
        [HttpPost]
        [Authorize(Policy="ApplicationLock")]
        public ActionResult ActionSchedulerApproval(CParameterApproval parameterApproval)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string CurrentUser = identity.FindFirst("UserID").Value;
                int pk_MUser_ID = Convert.ToInt32(identity.FindFirst("PK_MUser_ID").Value);
                if (parameterApproval.PK_ModuleApproval_IDSelected != null && parameterApproval.PK_ModuleApproval_IDSelected.Length > 0)
                {
                    foreach (long pk in parameterApproval.PK_ModuleApproval_IDSelected)
                    {
                        CParameterApproval approvalData = new CParameterApproval
                        {
                            PK_ModuleApproval_ID = pk,
                            ActionApproval = parameterApproval.ActionApproval
                        };
                        EODSchedulerBLL.ActionSchedulerApproval(approvalData, CurrentUser, pk_MUser_ID);
                    }
                }
                else
                {
                    EODSchedulerBLL.ActionSchedulerApproval(parameterApproval, CurrentUser, pk_MUser_ID);
                }
                return Ok("Success");
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }
        #endregion

        #region Delete Scheduler
        [Authorize(Policy="ApplicationLock")]
        [HttpPost]
        [Route("DeleteEODScheduler")]
        public ActionResult DeleteEODScheduler(SchedulerParam objParam)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string UserID = identity.FindFirst("UserID").Value;
                int MRoleID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value);
                int PK_MUser_ID = Convert.ToInt32(identity.FindFirst("PK_MUser_ID").Value);

                string errorMessage;

                Module objSchemaModule = ModuleBLL.GetModuleByID(objParam.ModuleID);
                if (objSchemaModule == null) return StatusCode(500, "Schema Module not found");

                MUser currentUser = new MUser()
                {
                    UserID = identity.FindFirst("UserID").Value,
                    FK_MRole_ID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value),
                    FK_MGroupMenu_ID = Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value),
                    PK_MUser_ID = Convert.ToInt32(identity.FindFirst("PK_MUser_ID").Value),
                };

                bool CheckApprovalRow = EODSchedulerBLL.CheckApprovalRow(objParam.DataScheduler.PK_EODScheduler_ID, objParam.ModuleID);
                if (CheckApprovalRow == false)
                {
                    return StatusCode(500, "Data Already Exists in Pending Approval");
                }
                else
                {
                    if (!objSchemaModule.IsUseApproval || currentUser.FK_MRole_ID == 1)
                    {
                        EODScheduler objDeleteScheduler = EODSchedulerBLL.GetSchedulerByID(objParam.SchedulerID);

                        string successMsg = "";
                        EODSchedulerBLL.DeleteScheduler(objDeleteScheduler, UserID, false);
                        successMsg = "EODScheduler " + objDeleteScheduler.EODSchedulerName + " is deleted successfully.";
                        return Ok(successMsg);
                    }
                    else
                    {
                        errorMessage = EODSchedulerBLL.SaveSchedulerWithApproval(objParam.InputMode, objParam.DataScheduler, UserID, MRoleID, PK_MUser_ID, objParam.ModuleID);
                        return (string.IsNullOrEmpty(errorMessage)) ? Ok("Pending To Approval") : StatusCode(400, errorMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }
        #endregion
        
        #region Activate Scheduler
        [Authorize(Policy="ApplicationLock")]
        [HttpPost]
        [Route("activateEODScheduler")]
        public ActionResult activateEODScheduler(SchedulerParam objParam)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string UserID = identity.FindFirst("UserID").Value;
                int MRoleID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value);
                int PK_MUser_ID = Convert.ToInt32(identity.FindFirst("PK_MUser_ID").Value);

                string errorMessage;

                Module objSchemaModule = ModuleBLL.GetModuleByID(objParam.ModuleID);
                if (objSchemaModule == null) return StatusCode(500, "Schema Module not found");

                MUser currentUser = new MUser()
                {
                    UserID = identity.FindFirst("UserID").Value,
                    FK_MRole_ID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value),
                    FK_MGroupMenu_ID = Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value),
                    PK_MUser_ID = Convert.ToInt32(identity.FindFirst("PK_MUser_ID").Value),
                };

                bool CheckApprovalRow = EODSchedulerBLL.CheckApprovalRow(objParam.DataScheduler.PK_EODScheduler_ID, objParam.ModuleID);
                if (CheckApprovalRow == false)
                {
                    return StatusCode(500, "Data Already Exists in Pending Approval");
                }
                else
                {
                    if (!objSchemaModule.IsUseApproval || currentUser.FK_MRole_ID == 1)
                    {
                        EODScheduler objActivateScheduler = EODSchedulerBLL.GetSchedulerByID(objParam.SchedulerID);
                        string successMsg = "";
                        string activateStr = objActivateScheduler.Active ? "deactivated" : "activated";

                        objActivateScheduler.Active = !objActivateScheduler.Active;
                        EODSchedulerBLL.activateScheduler(objActivateScheduler, UserID, false);
                        successMsg = "EODScheduler " + objActivateScheduler.EODSchedulerName + " is " + activateStr + " successfully.";

                        return Ok(successMsg);
                    }
                    else
                    {
                        objParam.DataScheduler.Active = !objParam.DataScheduler.Active;
                        errorMessage = EODSchedulerBLL.SaveSchedulerWithApproval(objParam.InputMode, objParam.DataScheduler, UserID, MRoleID, PK_MUser_ID, objParam.ModuleID);
                        if (string.IsNullOrEmpty(errorMessage))
                        {
                            return Ok("Pending To Approval");
                        }
                        else
                        {
                            return StatusCode(400, errorMessage);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        #endregion

        #region Export Module Scheduler Data
        [Route("ExportModuleData")]
        [HttpGet]
        [Authorize(Policy="ApplicationLock")]
        public IActionResult ExportModuleData(int page, string orderBy, string order, string search, string filter)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MemoryStream ms = NawaDataBLL.EODSchedulerBLL.ExportModuleData(
                    page, orderBy, order, search, filter, false, "", null, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value, null);

                ms.Position = 0;
                return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Data.xlsx");
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ExportModuleData")]
        [HttpPost]
        [Authorize(Policy="ApplicationLock")]
        public IActionResult ExportModuleData([FromBody] GenericPagingFilterPostBody body)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MemoryStream ms = NawaDataBLL.EODSchedulerBLL.ExportModuleData(
                    body.page, body.orderBy, body.order, body.search, body.filter, body.isAdv, body.exportAs, body.fields, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value, body.jsonFilter);

                ms.Position = 0;
                return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Data.xlsx");
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }
        #endregion

        #region Export Module Data All Scheduler 
        [Route("ExportModuleDataAll")]
        [HttpGet]
        [Authorize(Policy="ApplicationLock")]
        public IActionResult ExportModuleDataAll(string orderBy, string order, string search, string filter)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MemoryStream ms = NawaDataBLL.EODSchedulerBLL.ExportModuleDataAll(
                    orderBy, order, search, filter, false, "", null, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value, null);

                ms.Position = 0;
                return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Data.xlsx");
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ExportModuleDataAll")]
        [HttpPost]
        [Authorize(Policy="ApplicationLock")]
        public IActionResult ExportModuleDataAll([FromBody] GenericPagingFilterPostBody body)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MemoryStream ms = NawaDataBLL.EODSchedulerBLL.ExportModuleDataAll(
                    body.orderBy, body.order, body.search, body.filter, body.isAdv, body.exportAs, body.fields, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value, body.jsonFilter);

                ms.Position = 0;
                return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Data.xlsx");
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }
        #endregion

        #region Export Module Scheduler Selected
        [Route("ExportModuleDataSelected")]
        [HttpPost]
        [Authorize(Policy="ApplicationLock")]
        public IActionResult ExportModuleDataSelected(NawaDataDAL.Models.SchedulerSelectedData selectedData)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MemoryStream ms = NawaDataBLL.EODSchedulerBLL.ExportModuleDataSelected(selectedData.SelectedData, selectedData.orderBy, selectedData.order, selectedData.exportAs, selectedData.fields, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value));

                ms.Position = 0;
                return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Data.xlsx");
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }
        #endregion

        #region Save Scheduler Log
        [Authorize(Policy="ApplicationLock")]
        [HttpPost]
        [Route("SaveSchedulerLog")]
        public ActionResult SaveSchedulerLog(SchedulerLogParam objParam)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string UserID = identity.FindFirst("UserID").Value;
                string successMsg = "";

                EODSchedulerBLL.SaveSchedulerLog(objParam.ProcessDate, objParam.ProcessID, objParam.ListTask, UserID);
                successMsg = "Scheduler Log is saved into Database successfully.";

                return Ok(successMsg);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }
        #endregion

        #region Get Module Access
        [Route("GetModuleAccess")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetModuleAccess(int moduleID)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MGroupMenuAccess menuAccess = CommonBLL.GetModuleAccess(moduleID, identity.FindFirst("FK_MGroupMenu_ID").Value);

                return Ok(menuAccess);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }
        #endregion

        #region Function Parameter
        public class SchedulerParam
        {
            public EActionType InputMode { get; set; } 
            public EODScheduler DataScheduler { get; set; }
            public int SchedulerID { get; set; }
            public int ModuleID { get; set; }
        }

        public class SchedulerLogParam
        {
            public DateTime ProcessDate { get; set; }
            public long ProcessID { get; set; }
            public long[] ListTask { get; set; }
        }
        #endregion

        #endregion
    }
}
