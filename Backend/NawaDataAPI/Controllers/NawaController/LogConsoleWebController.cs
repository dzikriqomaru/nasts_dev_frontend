﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NawaDataBLL;
using NawaDataDAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace NawaDataAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LogConsoleWebController : ControllerBase
    {
        [Route("CreateLog")]
        [Authorize]
        [HttpPost]
        public ActionResult CreateLog(LogConsoleWeb logConsoleWeb)
        {
            try
            {
                LogConsoleWebBLL.CreateLogConsoleWeb(logConsoleWeb);
                return Ok("Log Time for Save is saved into Database successfully.");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetLog")]
        [Authorize]
        [HttpGet]
        public ActionResult GetLog()
        {
            try
            {
                return Ok(LogConsoleWebBLL.GetLogConsoleWeb());
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ClearLog")]
        [Authorize]
        [HttpGet]
        public ActionResult ClearLog()
        {
            try
            {
                LogConsoleWebBLL.ClearLogConsoleWeb();
                return Ok("Log Time for Save is cleared successfully.");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}
