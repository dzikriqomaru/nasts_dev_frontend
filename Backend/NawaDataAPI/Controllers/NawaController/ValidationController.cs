﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NawaDataBLL;
using NawaDataDAL;
using NawaDataDAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace NawaDataAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValidationController : ControllerBase
    {
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        [Route("GetValidationProcess")]
        public ActionResult GetValidationProcess(int page)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                Module module = ModuleBLL.GetModuleByName("ValidationProcess");

                if (module != null)
                {
                    if (!CommonBLL.CheckMenuAccess(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), module.PK_Module_ID, EActionType.View))
                    {
                        SystemDictionary sysDictionary = SystemParameterBLL.GetSystemDictionary("unauthorized-access");

                        return StatusCode(500, sysDictionary.TextDefinition);
                    }

                    int rowCount = 0;
                    List<ValidationProcessView> listProcess = ValidationBLL.GetValidationProcess(page, out rowCount);

                    return Ok(new { count = rowCount, rows = listProcess });
                }
                else
                {
                    SystemDictionary sysDictionary = SystemParameterBLL.GetSystemDictionary("err-something-wrong");

                    return StatusCode(500, sysDictionary.TextDefinition);
                }
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string stringUserID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, stringUserID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [HttpPost]
        [Route("RunValidation")]
        public ActionResult RunValidation(RunValidation input)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                Module module = ModuleBLL.GetModuleByName("ValidationProcess");

                if (module != null)
                {
                    if (!CommonBLL.CheckMenuAccess(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), module.PK_Module_ID, EActionType.View))
                    {
                        SystemDictionary sysDictionary = SystemParameterBLL.GetSystemDictionary("unauthorized-access");

                        return StatusCode(500, sysDictionary.TextDefinition);
                    }

                    string message = ValidationBLL.RunValidation(input.ListModuleID, identity.FindFirst("UserID").Value);

                    return Ok(message);
                }
                else
                {
                    SystemDictionary sysDictionary = SystemParameterBLL.GetSystemDictionary("err-something-wrong");

                    return StatusCode(500, sysDictionary.TextDefinition);
                }
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        [Route("GetValidationSummary")]
        public ActionResult GetValidationSummary(int page, string orderBy, string order, string search, string filter)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                Module module = ModuleBLL.GetModuleByName("ValidationSummary");

                if (module != null)
                {
                    if (!CommonBLL.CheckMenuAccess(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), module.PK_Module_ID, EActionType.View))
                    {
                        SystemDictionary sysDictionary = SystemParameterBLL.GetSystemDictionary("unauthorized-access");

                        return StatusCode(500, sysDictionary.TextDefinition);
                    }

                    int rowCount = 0;
                    List<ValidationSummaryView> data = ValidationBLL.GetValidationSummary(
                        page, orderBy, order, search, filter, out rowCount, null);

                    return Ok(new { count = rowCount, rows = data });
                }
                else
                {
                    SystemDictionary sysDictionary = SystemParameterBLL.GetSystemDictionary("err-something-wrong");

                    return StatusCode(500, sysDictionary.TextDefinition);
                }
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [HttpPost]
        [Route("GetValidationSummary")]
        public ActionResult GetValidationSummary([FromBody] GenericPagingFilterPostBody body)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                Module module = ModuleBLL.GetModuleByName("ValidationSummary");

                if (module != null)
                {
                    if (!CommonBLL.CheckMenuAccess(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), module.PK_Module_ID, EActionType.View))
                    {
                        SystemDictionary sysDictionary = SystemParameterBLL.GetSystemDictionary("unauthorized-access");

                        return StatusCode(500, sysDictionary.TextDefinition);
                    }

                    int rowCount = 0;
                    List<ValidationSummaryView> data = ValidationBLL.GetValidationSummary(
                        body.page, body.orderBy, body.order, body.search, "", out rowCount, body.jsonFilter);

                    return Ok(new { count = rowCount, rows = data });
                }
                else
                {
                    SystemDictionary sysDictionary = SystemParameterBLL.GetSystemDictionary("err-something-wrong");

                    return StatusCode(500, sysDictionary.TextDefinition);
                }
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        [Route("GetValidationDetail")]
        public ActionResult GetValidationDetail(int page, string orderBy, string order, string search, string filter)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                Module module = ModuleBLL.GetModuleByName("ValidationDetails");

                if (module != null)
                {
                    if (!CommonBLL.CheckMenuAccess(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), module.PK_Module_ID, EActionType.View))
                    {
                        SystemDictionary sysDictionary = SystemParameterBLL.GetSystemDictionary("unauthorized-access");

                        return StatusCode(500, sysDictionary.TextDefinition);
                    }

                    int rowCount = 0;
                    int countPerPage = 0;

                    List<DynamicDictionary> data = ValidationBLL.GetValidationDetail(
                        page, orderBy, order, search, filter, out rowCount, out countPerPage, null);

                    return Ok(new { count = rowCount, countPerPage = countPerPage, rows = data });
                }
                else
                {
                    SystemDictionary sysDictionary = SystemParameterBLL.GetSystemDictionary("err-something-wrong");

                    return StatusCode(500, sysDictionary.TextDefinition);
                }
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [HttpPost]
        [Route("GetValidationDetail")]
        public ActionResult GetValidationDetail([FromBody] GenericPagingFilterPostBody body)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                Module module = ModuleBLL.GetModuleByName("ValidationDetails");

                if (module != null)
                {
                    if (!CommonBLL.CheckMenuAccess(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), module.PK_Module_ID, EActionType.View))
                    {
                        SystemDictionary sysDictionary = SystemParameterBLL.GetSystemDictionary("unauthorized-access");

                        return StatusCode(500, sysDictionary.TextDefinition);
                    }

                    int rowCount = 0;
                    int countPerPage = 0;

                    List<DynamicDictionary> data = ValidationBLL.GetValidationDetail(
                        body.page, body.orderBy, body.order, body.search, "", out rowCount, out countPerPage, body.jsonFilter);

                    return Ok(new { count = rowCount, countPerPage = countPerPage, rows = data });
                }
                else
                {
                    SystemDictionary sysDictionary = SystemParameterBLL.GetSystemDictionary("err-something-wrong");

                    return StatusCode(500, sysDictionary.TextDefinition);
                }
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

    }
}
