﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using NawaDataBLL;
using NawaDataDAL;
using NawaDataDAL.Models;
using ITfoxtec.Identity.Saml2;
using Microsoft.Extensions.Options;
using ITfoxtec.Identity.Saml2.MvcCore;
using ITfoxtec.Identity.Saml2.Schemas;
using System.Security.Authentication;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace NawaDataAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestController : ControllerBase
    {
        const string relayStateReturnUrl = "ReturnUrl";
        private readonly Saml2Configuration config;
        public IConfiguration _configuration;

        public TestController(IConfiguration conf, IOptions<Saml2Configuration> configAccessor)
        {
            config = configAccessor.Value;
            _configuration = conf;
        }
        //public IConfiguration _configuration;

        //public TestController(IConfiguration config)
        //{
        //    _configuration = config;
        //}

        //[HttpGet]
        //public IActionResult Get()
        //{
        //    return Ok("Hello World");
        //}

        //[Authorize(AuthenticationSchemes = "Bearer")]
        //[HttpGet("Validate")]
        //public IActionResult Validate()
        //{
        //    ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;

        //    return Ok(identity.FindFirst("UserID").Value);
        //}

        //[HttpGet("Login")]
        //public IActionResult Login()
        //{
        //    var binding = new Saml2RedirectBinding();
        //    binding.SetRelayStateQuery(new Dictionary<string, string> { { relayStateReturnUrl, Url.Content("~/") } });

        //    return binding.Bind(new Saml2AuthnRequest(config)).ToActionResult();
        //}

        //[Route("AssertionConsumerService")]
        //public IActionResult AssertionConsumerService()
        //{
        //    var binding = new Saml2PostBinding();
        //    var saml2AuthnResponse = new Saml2AuthnResponse(config);

        //    binding.ReadSamlResponse(Request.ToGenericHttpRequest(), saml2AuthnResponse);
        //    if (saml2AuthnResponse.Status != Saml2StatusCodes.Success)
        //    {
        //        throw new AuthenticationException($"SAML Response status: {saml2AuthnResponse.Status}");
        //    }
        //    //binding.Unbind(Request.ToGenericHttpRequest(), saml2AuthnResponse);
        //    //await saml2AuthnResponse.CreateSession(HttpContext, claimsTransform: (claimsPrincipal) => ClaimsTransform.Transform(claimsPrincipal));

        //    //var relayStateQuery = binding.GetRelayStateQuery();
        //    //var returnUrl = relayStateQuery.ContainsKey(relayStateReturnUrl) ? relayStateQuery[relayStateReturnUrl] : Url.Content("~/");
        //    //return Redirect(returnUrl);

        //    Claim[] claims = new Claim[]
        //                {
        //                    new Claim(JwtRegisteredClaimNames.Sub, _configuration["Jwt:Subject"]),
        //                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
        //                    new Claim(JwtRegisteredClaimNames.Iat, DateTime.Now.ToString()),
        //                    new Claim("UserID", saml2AuthnResponse.ClaimsIdentity.Name)
        //                };

        //    //Get session expiration time
        //    NawaDataDAL.Models.SystemParameter expSysParam = SystemParameterBLL.GetSystemParameterValue(17);
        //    int tokenExpiration = expSysParam == null ? 60 :
        //        string.IsNullOrEmpty(expSysParam.SettingValue) ? 60 : Convert.ToInt32(expSysParam.SettingValue);

        //    SymmetricSecurityKey key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]));
        //    SigningCredentials signIn = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
        //    JwtSecurityToken token = new JwtSecurityToken(_configuration["Jwt:Issuer"], _configuration["Jwt:Issuer"], claims, expires: DateTime.Now.AddMinutes(tokenExpiration), signingCredentials: signIn);

        //    return Ok(new JwtSecurityTokenHandler().WriteToken(token));
        //}

        //[HttpPost("Logout")]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> Logout()
        //{
        //    if (!User.Identity.IsAuthenticated)
        //    {
        //        return Redirect(Url.Content("~/"));
        //    }

        //    var binding = new Saml2PostBinding();
        //    var saml2LogoutRequest = await new Saml2LogoutRequest(config, User).DeleteSession(HttpContext);
        //    return Redirect("~/");
        //}

        //[Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult Get()
        {
            try
            {
                return Ok("Nawa Framework Version" + " " + NawaDataDAL.Version.GetVersion() + " " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            }
            catch (Exception ex)
            {
                ElmahErrorBLL.Log(ex,"");
                string error = ex.Message;
                while (ex.InnerException != null)
                {
                    error += ex.InnerException.Message;
                    ex = ex.InnerException;
                }
                return Ok(error);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [Route("test2")]
        [HttpGet]
        public ActionResult Get2()
        {
            string param = "";
            try
            {
                param = NawaDataBLL.SystemParameterBLL.GetAppSystemParameter(1).SettingValue;
            }
            catch (Exception ex)
            {
                NawaDataBLL.ElmahErrorBLL.Log(ex, "");
                string error = ex.Message;
                while (ex.InnerException != null)
                {
                    error += ex.InnerException.Message;
                    ex = ex.InnerException;
                }
                return Ok(error);
            }
            return Ok(param);
        }

        [Route("TestValidation")]
        [HttpPost]
        public ActionResult TestValidation(NawaDataBLL.CommonBLL.ValidationData data)
        {
            string errMsg = "Pass";

            if(NawaDataBLL.CommonBLL.StandartValidation(CommonBLL.GetObject(9, data.Data), EValidationErrorType.Empty))
            {
                errMsg = "empty";
            }
            else if(NawaDataBLL.CommonBLL.StandartValidation(CommonBLL.GetObject(9, data.Data), EValidationErrorType.Null))
            {
                errMsg = "null";
            }
            else if(NawaDataBLL.CommonBLL.StandartValidation(CommonBLL.GetObject(9, data.Data), data.Length, EValidationErrorType.Size))
            {
                errMsg = "over";
            }
            else if(NawaDataBLL.CommonBLL.StandartValidation(CommonBLL.GetObject(9, data.Data), data.Regex, EValidationErrorType.Regex))
            {
                errMsg = "invalid";
            }

            return Ok(errMsg);
        }

        [Route("TestJSONToQuery")]
        [HttpPost]
        public ActionResult TestJSONToQuery([FromBody] JSONTree jsonTree)
        {
            (string field, string value) result = (null, null);
            result.field = "";

            try
            {
                JSONToQueryOption jsonOptions = new JSONToQueryOption
                {
                    FieldOptions = new List<JSONFieldOption>{
                        new JSONFieldOption
                        {
                            Locator = item => true,
                            FieldPrefix = "PresetPrefix"
                        },
                        new JSONFieldOption
                        {
                            Locator = item => item.valueType[0] == JSONTreeValueType.Date || item.valueType[0] == JSONTreeValueType.Time || item.valueType[0] == JSONTreeValueType.Datetime,
                            OverrideData = (item) => (AdvanceFilterBLL.FormatQueryAs("CustomPrefix." + item.field, AdvanceFilterBLL.DatetimeFormat_ISO8601), null)
                        },
                    },
                    DBType = NawaDAO.DBType.SQLServer
                };

                string query = AdvanceFilterBLL.JSONToQuery(jsonTree, jsonOptions);

                return Ok(query);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        public class CTest
        {
            public string version { get; set; }
            public string username { get; set; }
        }
    }
}
