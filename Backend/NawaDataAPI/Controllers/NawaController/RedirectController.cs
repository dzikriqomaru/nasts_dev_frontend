﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DeviceDetectorNET;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using DeviceDetectorNET.Parser;
using System.Web;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using NawaDataBLL;

namespace NawaDataAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RedirectController : ControllerBase
    {
        [Route("to")]
        [HttpGet]
        public ActionResult RedirectTo(string parameter)
        {
            try
            {
                //return null;
                var userAgent = Request.Headers["User-Agent"];
                var dd = new DeviceDetector(userAgent);
                dd.Parse();
                var clientInfo = dd.GetClient(); // holds information about browser, feed reader, media player, ...
                var osInfo = dd.GetOs();
                var device = dd.GetDeviceName();
                var brand = dd.GetBrandName();
                var model = dd.GetModel();

                String parameterFinal = "";

                if (parameter != null)
                {
                    parameterFinal = "?" + parameter.Replace(",", "&");
                }
            
                string URL = NawaDataBLL.SystemParameterBLL.GetSystemParameterValue(-59).SettingValue + parameterFinal;
                string EncodedURL = System.Net.WebUtility.UrlEncode(URL);

                if (device == "desktop")
                {
                    return RedirectPermanent(URL);
                }
                else
                {
                    return RedirectPermanent("intent://" + parameterFinal + "#Intent;scheme=ndsmobile;package=com.ndsmobile;S.browser_fallback_url=" + EncodedURL + ";end");
                }
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }
    }
}
