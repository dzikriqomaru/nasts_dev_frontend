﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Text.Json.Serialization;
using NawaDataBLL;
using NawaDataDAL.Models;
using NawaEncryption;

namespace NawaDataAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GoogleController : ControllerBase
    {
        [Route("VerifyCaptchaV2")]
        [HttpPost]
        public async Task<ActionResult> VerifyCaptchaV2([FromBody] CaptchaRequest captchaRequest)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    //string secretKey = "6LeF1iYbAAAAAP2mpNcEAT09G-VQo2cYBKuRcdVs";
                    var paramId = -4;
                    bool isEnrypt = SystemParameterBLL.GetSystemParameterGroup(paramId).IsEncript;
                    string secretKey = "";

                    if (isEnrypt == true)
                    {
                        string encryptedValue = SystemParameterBLL.GetSystemParameterGroup(paramId).SettingValue;
                        string encryptionKey = SystemParameterBLL.GetSystemParameterGroup(paramId).EncriptionKey;
                        secretKey = Common.DecryptRijndael(encryptedValue, encryptionKey);
                    }
                    else
                    {
                        secretKey = SystemParameterBLL.GetSystemParameterGroup(paramId).SettingValue;
                    }

                    // request config
                    client.BaseAddress = new Uri("https://www.google.com/");
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("multipart/form-data"));

                    // Form Data
                    MultipartFormDataContent formData = new MultipartFormDataContent();
                    formData.Add(new StringContent(secretKey), "secret");
                    formData.Add(new StringContent(captchaRequest.token), "response");

                    // Post data 
                    HttpResponseMessage res = await client.PostAsync("recaptcha/api/siteverify", formData);

                    if (res.IsSuccessStatusCode)
                    {
                        var json = res.Content.ReadAsStringAsync().Result;
                        CaptchaResponse googleResponse = JsonSerializer.Deserialize<CaptchaResponse>(json);

                        return Ok(googleResponse);
                    }
                    else
                    {
                        return StatusCode(500, "Verify captcha failed");
                    }
                }
            }
            catch (Exception)
            {
                return StatusCode(500, "Verify captcha failed");
            }
        }

    }

    public class CaptchaRequest
    {
        public string token { get; set; }
    }

    public class CaptchaResponse
    {
        public bool success { get; set; }
        public DateTime challenge_ts { get; set; }
        public string hostname { get; set; }
        public int? score { get; set; }

        [JsonPropertyName("error-codes")]
        public List<string> errorcodes { get; set; }
    }
}
