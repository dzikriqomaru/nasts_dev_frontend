﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NawaDataBLL;
using NawaDataDAL;
using NawaDataDAL.Models;
using System.Security.Claims;

namespace NawaDataAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommonController : ControllerBase
    {
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        [Route("checkMenuAccess")]
        public ActionResult CheckMenuAccess(int groupMenuID, int moduleID, EActionType action)
        {
            try
            {
                bool result = CommonBLL.CheckMenuAccess(groupMenuID, moduleID, action);
                return Ok(result);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        [Route("executeTablePaging")]
        public ActionResult ExecuteTablePaging(string table, string fields, string filters, string sorts, int pageIndex, int pageSize)
        {
            try
            {
                int totalRow = 0;
                DataTable dtResult = CommonBLL.ExecuteTablePaging(table, fields, filters, sorts, pageIndex, pageSize, out totalRow);
                List<Dictionary<string, object>> result = dtResult.AsEnumerable().Select(
                                                            row => dtResult.Columns.Cast<DataColumn>().ToDictionary(
                                                                column => column.ColumnName, // Key
                                                                column => row[column] // Value
                                                            )).ToList();

                return Ok(new { TotalRow = totalRow, ListData = result });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        [Route("getModuleByID")]
        public ActionResult GetModuleByID(int id)
        {
            try
            {
                Module module = ModuleBLL.GetModuleByID(id);
                return Ok(module);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        #region Encrypt String
        [HttpPost]
        [Route("EncryptString")]
        public ActionResult EncryptString(EncryptView request)
        {
            try
            {
                if (request == null|| string.IsNullOrEmpty(request.encryptString) || string.IsNullOrEmpty(request.salt))
                {
                    return BadRequest("Some parameters does not have any value");
                }
                
                string encryptedString = CommonBLL.EncryptString(request.encryptString, request.salt);

                return Ok(encryptedString);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        public class EncryptView
        {
            public string encryptString { get; set; }
            public string salt { get; set; }
        }
        #endregion

        #region NawaIconList Options
        [Route("GetNawaIconListOptions")]
        [Authorize]
        [HttpGet]
        public ActionResult GetNawaIconListOptions()
        {
            try
            {
                List<COption> listOptions = NawaDataBLL.CommonBLL.GetNawaIconListOptions();
                return Ok(listOptions);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
        #endregion
    }
}