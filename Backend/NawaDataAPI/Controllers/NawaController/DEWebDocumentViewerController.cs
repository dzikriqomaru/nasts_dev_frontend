﻿using DevExpress.AspNetCore.Reporting.WebDocumentViewer;
using DevExpress.AspNetCore.Reporting.WebDocumentViewer.Native.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NawaDataBLL;
using NawaDataDAL;
using NawaDataDAL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace NawaDataAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DEWebDocumentViewerController : WebDocumentViewerController
    {
        public DEWebDocumentViewerController(IWebDocumentViewerMvcControllerService controllerService) : base(controllerService) { }
        public IActionResult Index()
        {
            return View();
        }

        [Route("DXXRDV")]
        [HttpPost]
        [HttpGet]
        public override Task<IActionResult> Invoke()
        {
            return base.Invoke();
        }


        [Authorize(Policy="ApplicationLock")]
        [Route("GetReportList")]
        [HttpGet]

        public ActionResult GetListReport(int page, string search)
        {
            try
            {
                int rowCount = 0;

                int totalPage = 0;

                List<ReportLayout> ReportList = ReportBLL.GetListReport(page, out rowCount, out totalPage, search);

                return Ok(new { count = rowCount, rows = ReportList, totalPage = totalPage });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }

        }

    }
}
