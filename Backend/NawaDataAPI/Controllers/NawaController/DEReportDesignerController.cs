﻿using DevExpress.AspNetCore.Reporting.ReportDesigner;
using DevExpress.AspNetCore.Reporting.ReportDesigner.Native.Services;
using DevExpress.DataAccess.ConnectionParameters;
using DevExpress.DataAccess.Sql;
using DevExpress.XtraReports.UI;
using DevExpress.XtraReports.Web.ReportDesigner;
using Microsoft.AspNetCore.Mvc;
using NawaDataDAL;
using System;
using System.Collections.Generic;
using System.Data;
using Microsoft.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using NawaDataDAL.Models;
using NawaDataBLL;

namespace NawaDataAPI.Controllers.EStatement
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomReportDesignerController : ReportDesignerController
    {

        public CustomReportDesignerController(IReportDesignerMvcControllerService controllerService) : base(controllerService) { }

        //private IReportDesignerMvcControllerService controllerService;

        //public CustomReportDesignerController(IReportDesignerMvcControllerService controllerService)
        //{
        //    this.controllerService = controllerService;
        //}

        [Route("Test")]
        [HttpGet]
        public IActionResult Test()
        {
            return Ok("123");
        }


        [Route("DXXRD")]
        [HttpPost]
        public override Task<IActionResult> Invoke()
        {
           return base.Invoke();
        }

        [Route("GetReportDesignerModel")]
        [HttpPost]
        public IActionResult GetReportDesignerModel([FromForm] string reportUrl,
    [FromServices] IReportDesignerClientSideModelGenerator reportDesignerClientSideModelGenerator)
        {
            string modelJsonScript;

            IDictionary<string, object> dataSourceReporting = null;

            string query = "SELECT COUNT(13) FROM remoteConnection WHERE active = 1";

            int count = Convert.ToInt32(NawaDAO.ExecuteScalar(query));

            if (count > 0)
            {

                dataSourceReporting = new System.Collections.Generic.Dictionary<string, object>();

                query = "SELECT * FROM remoteConnection";

                DataTable dt = NawaDAO.ExecuteTable(query);

                foreach (DataRow dr in dt.Rows)
                {
                    query = "EXEC usp_DecryptReportConnectionData @encryptedPass";

                    SqlParameter[] prms = new SqlParameter[]
                    {
                    new SqlParameter("@encryptedPass", SqlDbType.VarChar){ Value = Convert.ToString(dr["password"]) },
                    };


                    string decryptedPassword = Convert.ToString(NawaDAO.ExecuteScalar(query, prms));
                    DataConnectionParametersBase parameters = null;

                    //if connection type is MsSQL
                    if (Convert.ToInt32(dr["connectionType"]) == (int)EActionConnectionType.MsSql)
                    {
                        parameters = new MsSqlConnectionParameters(Convert.ToString(dr["serverName"]),
                        Convert.ToString(dr["connectionName"]), Convert.ToString(dr["userName"]), decryptedPassword, MsSqlAuthorizationType.SqlServer);
                    }
                    //if connection type is postgreSQL
                    else if (Convert.ToInt32(dr["connectionType"]) == (int)EActionConnectionType.PostgreSQL)
                    {
                        parameters = new PostgreSqlConnectionParameters(Convert.ToString(dr["serverName"]), Convert.ToString(dr["dbname"]), Convert.ToString(dr["userName"]), decryptedPassword);
                    }
                    else if (Convert.ToInt32(dr["connectionType"]) == (int)EActionConnectionType.BigQuery)
                    {
                        parameters = new BigQueryConnectionParameters(Convert.ToString(dr["serverName"]),
                        Convert.ToString(dr["connectionName"]), Convert.ToString(dr["userName"]), decryptedPassword, BigQueryAuthorizationType.OAuth.ToString());  
                    }
                    else
                    {
                        parameters = new PostgreSqlConnectionParameters(Convert.ToString(dr["servername"]), Convert.ToString(dr["dbname"]), Convert.ToString(dr["userName"]), decryptedPassword);
                    }

       

                    SqlDataSource dataSource = new SqlDataSource(parameters);

                    dataSourceReporting.Add(Convert.ToString(dr["connectionName"]), dataSource);

                    dataSource.RebuildResultSchema();
                }
            }
            if (string.IsNullOrEmpty(reportUrl))
            {
                modelJsonScript =
                reportDesignerClientSideModelGenerator
                .GetJsonModelScript(
                    // The name of a report (reportUrl)
                    // that the Report Designer opens when the application starts.
                    new XtraReport(),
                    // Data sources for the Report Designer.                
                    dataSourceReporting,
                    // The URI path of the default controller
                    // that processes requests from the Report Designer.
                    "CustomReportDesigner/" + DevExpress.AspNetCore.Reporting.ReportDesigner.ReportDesignerController.DefaultUri,
                    // The URI path of the default controller
                    // that processes requests from the Web Document Viewer.
                    "DEWebDocumentViewer/" + DevExpress.AspNetCore.Reporting.WebDocumentViewer.WebDocumentViewerController.DefaultUri,
                    // The URI path of the default controller
                    // that processes requests from the Query Builder.
                    "DEQueryBuilder/" + DevExpress.AspNetCore.Reporting.QueryBuilder.QueryBuilderController.DefaultUri
                );
            }
            else
            {
                modelJsonScript =
                reportDesignerClientSideModelGenerator
                .GetJsonModelScript(
                    // The name of a report (reportUrl)
                    // that the Report Designer opens when the application starts.
                    reportUrl,
                    // Data sources for the Report Designer.                
                    dataSourceReporting,
                    // The URI path of the default controller
                    // that processes requests from the Report Designer.
                    "CustomReportDesigner/" + DevExpress.AspNetCore.Reporting.ReportDesigner.ReportDesignerController.DefaultUri,
                    // The URI path of the default controller
                    // that processes requests from the Web Document Viewer.
                    "DEWebDocumentViewer/" + DevExpress.AspNetCore.Reporting.WebDocumentViewer.WebDocumentViewerController.DefaultUri,
                    // The URI path of the default controller
                    // that processes requests from the Query Builder.
                    "DEQueryBuilder/" + DevExpress.AspNetCore.Reporting.QueryBuilder.QueryBuilderController.DefaultUri
                );
            }


            return Content(modelJsonScript, "application/json");


        }

        internal Dictionary<string, object> GetDict(DataTable dt)
        {

            return dt.AsEnumerable()
            .ToDictionary<DataRow, string, object>(row => row[0].ToString(),
                                                   row => row[1].ToString());

        }


        Dictionary<string, object> GetAvailableDataSources()
        {
            DataTable dt = NawaDataDAL.NawaDAO.ExecuteTable("SELECT * FROM MUser");
            return GetDict(dt);
            //var dataSources = new Dictionary<string, object>();
            //SqlDataSource ds = new SqlDataSource("NWindConnectionString");
            //var query = SelectQueryFluentBuilder.AddTable("Products").SelectAllColumns().Build("Products");
            //ds.Queries.Add(query);
            //ds.RebuildResultSchema();
            //dataSources.Add("SqlDataSource", ds);
            //return dataSources;
        }
    }
}
