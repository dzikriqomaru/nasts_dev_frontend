﻿using DevExpress.DataAccess.ConnectionParameters;
using DevExpress.DataAccess.Native;
using DevExpress.DataAccess.Web;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NawaDataBLL;
using NawaDataDAL.Models;

namespace NawaDataAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DEDataSourceWizardConnectionStringsProviderController : IDataSourceWizardConnectionStringsProvider
    {
        public Dictionary<string, string> GetConnectionDescriptions()
        {
            Dictionary<string, string> connections = AppConfigHelper.GetConnections().Keys.ToDictionary(x => x, x => x);

            // Customize the loaded connections list. 
            connections.Remove("Cars");
            connections.Add("Custom Connection", "Custom SQL Connection");
            connections.Add("Custom BQ Connection", "Custom BQ Connection");
            connections.Add("PostgreSQL Connection", "PostgreSQL Connection");

            return connections;
        }

        public DataConnectionParametersBase GetDataConnectionParameters(string name)
        {
            try
            {
                if (name == "Custom Connection")
                {
                    SystemParameter sysParamServer = SystemParameterBLL.GetSystemParameterValue(-61);

                    if (sysParamServer == null) throw new Exception("");

                    SystemParameter sysParamDB = SystemParameterBLL.GetSystemParameterValue(-62);

                    if (sysParamDB == null) throw new Exception("");

                    SystemParameter sysParamUser = SystemParameterBLL.GetSystemParameterValue(-63);

                    if (sysParamUser == null) throw new Exception("");

                    AppSystemParameter sysParamPass = SystemParameterBLL.GetAppSystemParameter(-64);

                    if (sysParamPass == null) throw new Exception("");

                    return new MsSqlConnectionParameters(sysParamServer.SettingValue, sysParamDB.SettingValue, sysParamUser.SettingValue, sysParamPass.SettingValue, MsSqlAuthorizationType.SqlServer);
                }
                else if (name == "Custom BQ Connection")
                {
                    return new BigQueryConnectionParameters("enginestatement", "staging",
                        "googlebigquery@enginestatement.iam.gserviceaccount.com", "./enginestatement-56ba9f1c4f9e.p12");
                }
                else if (name == "PostgreSQL Connection")
                {
                    return new PostgreSqlConnectionParameters("34.101.78.189", "enginestatement", "postgres", "nawadata");
                }

                return AppConfigHelper.LoadConnectionParameters(name);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
