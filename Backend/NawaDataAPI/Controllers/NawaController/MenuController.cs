﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using NawaDataDAL.Models;
using NawaDataBLL;
using System.Security.Claims;
using Newtonsoft.Json;

namespace NawaDataAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MenuController : ControllerBase
    {
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult Get()
        {
            try
            {
                List<TreeMenu> treeMenus = MenuBLL.GetMenu();
                return Ok(treeMenus);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [HttpPost]
        public ActionResult Post(MenuTemplateParam objParam)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                Module objSchemaModule = ModuleBLL.GetModuleByID(objParam.ModuleID);
                if (objSchemaModule == null)
                    return StatusCode(500, "Schema Module not found");


                MUser currentUser = new MUser()
                {
                    UserID = identity.FindFirst("UserID").Value,
                    FK_MRole_ID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value),
                    FK_MGroupMenu_ID = Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value),
                    PK_MUser_ID = Convert.ToInt32(identity.FindFirst("PK_MUser_ID").Value),
                };

                string successMsg = "";
                if (!objSchemaModule.IsUseApproval || currentUser.FK_MRole_ID == 1)
                {
                    MenuBLL.SaveMenuNoApproval(objParam.treeMenu, currentUser.UserID);

                    if (objParam.ListMenuAccess != null)
                    {
                        MGroupAccessBLL.GenerateMenuTemplate(currentUser.FK_MGroupMenu_ID, objParam.ListMenuAccess);
                    }
                    successMsg = objSchemaModule.ModuleLabel + " is saved into Database successfully.";
                }
                else
                {
                    objParam.GroupMenuID = currentUser.FK_MGroupMenu_ID;
                    MenuBLL.SaveMenuWithApproval(objParam.InputMode == "I" ? EActionType.Insert : EActionType.Update, objParam, currentUser.UserID, currentUser.PK_MUser_ID, currentUser.FK_MRole_ID);
                    successMsg = objSchemaModule.ModuleLabel + " is saved into pending approval.";
                }
                return Ok(successMsg);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }


        [Route("ActionMenuTemplateApproval")]
        [Authorize(Policy="ApplicationLock")]
        [HttpPost]
        public ActionResult ActionMenuTemplateApproval(CParameterApproval objParam)
        {
            try
            {
                string msg = "";
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MUser currentUser = new MUser()
                {
                    PK_MUser_ID = Convert.ToInt32(identity.FindFirst("PK_MUser_ID").Value),
                    UserID = identity.FindFirst("UserID").Value,
                    FK_MGroupMenu_ID = Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value)
                };

                if (!CommonBLL.CheckMenuAccessByApprovalID(currentUser.FK_MGroupMenu_ID, objParam.PK_ModuleApproval_ID, EActionType.Approval))
                {
                    return StatusCode(500, "You don't have authorization to access this menu.");
                }

                if (objParam.ActionApproval == EActionApprovalType.Approve)
                {
                    MenuBLL.ApproveApproval(objParam.PK_ModuleApproval_ID, currentUser.UserID, currentUser.PK_MUser_ID);
                    msg = "Data approved successfully";
                }
                else if (objParam.ActionApproval == EActionApprovalType.Reject)
                {
                    MenuBLL.RejectApproval(objParam.PK_ModuleApproval_ID, currentUser.UserID, currentUser.PK_MUser_ID);
                    msg = "Data rejected successfully";
                }

                return Ok(msg);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetModuleAccess")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetModuleAccess(int moduleID)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MGroupMenuAccess menuAccess = CommonBLL.GetModuleAccess(moduleID, identity.FindFirst("FK_MGroupMenu_ID").Value);

                return Ok(menuAccess);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }
    }
}