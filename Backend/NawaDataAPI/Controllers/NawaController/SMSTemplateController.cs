﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using NawaDataBLL;
using NawaDataDAL;
using System.IO;
using NawaDataDAL.Models;

namespace NawaDataAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SMSTemplateController : ControllerBase
    {
        [Route("GetModuleData")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetModuleData(int page, string orderBy, string order, string search, string filter, bool isAdv)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;

                int rowCount = 0;
                List<DynamicDictionary> moduleData = SMSTemplateBLL.GetModuleData(
                    page, orderBy, order, search, filter, isAdv, null, out rowCount, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value, null);

                return Ok(new { count = rowCount, rows = moduleData });
            }
            catch (Exception)
            {
                return StatusCode(500);
            }
        }

        [Route("GetModuleData")]
        [Authorize(Policy="ApplicationLock")]
        [HttpPost]
        public ActionResult GetModuleData([FromBody] GenericPagingFilterPostBody body)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;

                int rowCount = 0;
                List<DynamicDictionary> moduleData = SMSTemplateBLL.GetModuleData(
                    body.page, body.orderBy, body.order, body.search, body.filter, body.isAdv, body.fields, out rowCount, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value, body.jsonFilter);

                return Ok(new { count = rowCount, rows = moduleData });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("getMonitoringDuration")]
        [HttpGet]
        public ActionResult getMonitoringDuration()
        {
            try
            {
                List<MonitoringDuration> MonitoringDurationList = SMSTemplateBLL.GetMonitoringDuration();
                return Ok(MonitoringDurationList);
            }
            catch (Exception)
            {
                return StatusCode(500);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [Route("getSMSTableType")]
        [HttpGet]
        public ActionResult getSMSTableType()
        {
            try
            {
                List<SMSTableType> SMSTableTypeList = SMSTemplateBLL.GetSMSTableType();
                return Ok(SMSTableTypeList);
            }
            catch (Exception)
            {
                return StatusCode(500);
            }
        }

        [Route("validationQuery")]
        [HttpPost]
        public ActionResult validationQuery(SMSTemplateAdditional queryTable)
        {
            try
            {
                string invalid = null;
                List<string> col = SMSTemplateBLL.ValidationQuery(queryTable, out invalid);
                return Ok(new { col = col, invalid = invalid });
            }
            catch (Exception)
            {
                return StatusCode(500);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [Route("saveSMSTemplate")]
        [HttpPost]
        public ActionResult saveSMSTemplate(SMSTemplateBLL.saveSMS sms)
        {
            try
            {
                //string currentUser = "Sysadmin";
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string CurrentUser = identity.FindFirst("UserID").Value;
                int MRoleID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value);

                SMSTemplateBLL.saveSMSWithoutApproval(sms, CurrentUser);
                return Ok("Data has been saved");
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [Route("GetSMSTemplate")]
        [HttpGet]
        public ActionResult GetSMSTemplate(int SMSID)
        {
            try
            {
                SMSTemplateView SMS = SMSTemplateBLL.GetSMSTemplate(SMSID);
                return Ok(SMS);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetModuleAccess")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetModuleAccess(int moduleID)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MGroupMenuAccess menuAccess = CommonBLL.GetModuleAccess(moduleID, identity.FindFirst("FK_MGroupMenu_ID").Value);

                return Ok(menuAccess);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }
    }
}