using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NawaDataBLL;
using NawaDataDAL;
using NawaDataDAL.Models;
using NPOI.SS.Formula.Functions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace NawaDataAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ModuleController : ControllerBase
    {
        #region Master
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        [Route("getExtType")]
        public ActionResult GetExtType()
        {
            try
            {
                List<MExtType> extTypes = ModuleBLL.GetExtType();
                return Ok(extTypes);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        [Route("getFieldType")]
        public ActionResult GetFieldType()
        {
            try
            {
                List<MFieldType> fieldTypes = ModuleBLL.GetFieldType();
                return Ok(fieldTypes);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        [Route("getReferenceTable")]
        public ActionResult GetReferenceTable()
        {
            try
            {
                List<string> listTable = CommonBLL.GetListTableName();
                return Ok(listTable);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        [Route("getReferenceColumn")]
        public ActionResult GetReferenceColumn(string tableName)
        {
            try
            {
                List<string> listColumn = CommonBLL.GetColumnByTableName(tableName);
                return Ok(listColumn);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        [Route("getModuleAction")]
        public ActionResult GetModuleAction()
        {
            try
            {
                List<ModuleAction> listAction = ModuleBLL.GetModuleAction();
                return Ok(listAction);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        [Route("getModuleActionByID")]
        public ActionResult getModuleActionByID(int PK_Module_ID)
        {
            try
            {
                List<COption> listAction = ModuleBLL.GetModuleAction(PK_Module_ID);
                return Ok(listAction);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        [Route("getModuleTime")]
        public ActionResult GetModuleTime()
        {
            try
            {
                List<ModuleTime> listTime = ModuleBLL.GetModuleTime();
                return Ok(listTime);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        #endregion

        #region Get Module
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        [Route("GetAllModules")]
        public ActionResult GetAllModules()
        {
            try
            {
                List<NawaDataDAL.Models.COption> listOptions = NawaDataBLL.ModuleBLL.GetListModules();

                return Ok(listOptions);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        [Route("GetModuleData")]
        public ActionResult GetModuleData(int page, string orderBy, string order, string search, string filter, bool isAdvanced)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                int rowCount = 0;
                List<NawaDataDAL.DynamicDictionary> moduleData = NawaDataBLL.ModuleBLL.GetModuleData(
                    page, orderBy, order, search, filter, isAdvanced, out rowCount, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value, null);

                return Ok(new { count = rowCount, rows = moduleData });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [HttpPost]
        [Route("GetModuleData")]
        public ActionResult GetModuleData([FromBody] GetModuleDataPostBody body)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                int rowCount = 0;
                List<NawaDataDAL.DynamicDictionary> moduleData = NawaDataBLL.ModuleBLL.GetModuleData(
                    body.page, body.orderBy, body.order, body.search, body.filter, body.isAdv, out rowCount, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value, body.jsonFilter);

                return Ok(new { count = rowCount, rows = moduleData });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }


        [Authorize(Policy = "ApplicationLock")]
        [HttpGet]
        [Route("GetModuleDesigner")]
        public ActionResult GetModuleDesigner(int page, string search)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                int rowCount = 0;
                List<DynamicDictionary> moduleData = ModuleBLL.GetModuleDesigner(
                    page, search, out rowCount);

                return Ok(new { count = rowCount, rows = moduleData });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy = "ApplicationLock")]
        [HttpGet]
        [Route("GetModuleField")]
        public ActionResult GetModuleField(long moduleID, int page, string search)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                int rowCount = 0;
                int allRowCount = 0;
                List<DynamicDictionary> moduleData = ModuleBLL.GetModuleField(moduleID, page, search,out rowCount, out allRowCount);

                return Ok(new { count = rowCount, rows = moduleData, countAll = allRowCount });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy = "ApplicationLock")]
        [HttpGet]
        [Route("GetAllModuleField")]
        public ActionResult GetAllModuleField(long moduleID)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                List<DynamicDictionary> moduleData = ModuleBLL.GetAllModuleField(moduleID);
                return Ok(new { rows = moduleData });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        [Route("getDataEditModule")]
        public ActionResult GetDataEditModule(int moduleID)
        {
            try
            {
                Module objModule = ModuleBLL.GetModuleByID(moduleID);
                if (objModule != null)
                {
                    objModule.ListField = ModuleBLL.GetModuleField(moduleID);
                    objModule.ListValidation = ModuleBLL.GetModuleValidation(moduleID);

                    /* --- Check Field Regex --- */
                    List<ModuleFieldRegexView> listRegex = new List<ModuleFieldRegexView>();

                    if (objModule.ListField.FirstOrDefault(x => x.IsUseRegexValidation) != null)
                        listRegex = ModuleBLL.GetModuleFieldRegex(moduleID);

                    objModule.ListField.ForEach(x =>
                    {
                        x.ListRegex = new List<ModuleFieldRegexView>();
                        x.ListRegex.AddRange(listRegex.Where(y => y.FieldName == x.FieldName).ToList());
                    });

                    #region Checks for ModuleDetail
                    objModule.ListModuleDetail = ModuleBLL.GetModuleDetail(moduleID);
                    #endregion

                    #region Checks for ModuleDetailField
                    if (objModule.ListModuleDetail != null)
                    {
                        foreach (var Data in objModule.ListModuleDetail)
                        {
                            Data.ListModuleDetailField = ModuleBLL.GetModuleDetailField(Data.PK_ModuleDetail_ID);

                            List<ModuleFieldRegexView> listDetailRegex = new List<ModuleFieldRegexView>();

                            if (Data.ListModuleDetailField.FirstOrDefault(x => x.IsUseRegexValidation) != null)
                                listDetailRegex = ModuleBLL.GetModuleDetailFieldRegex(Data.PK_ModuleDetail_ID);

                            Data.ListModuleDetailField.ForEach(x =>
                            {
                                x.ListRegex = new List<ModuleFieldRegexView>();
                                x.ListRegex.AddRange(listDetailRegex.Where(y => y.FieldName == x.FieldName).ToList());
                            });

                        }
                    }
                    #endregion

                    #region Checks for ModuleCustomSupport
                    objModule.ListCustomSupport = ModuleBLL.GetModuleCustomSupport(moduleID);
                    #endregion
                }
                else
                {
                    throw new Exception("Module not found");
                }

                return Ok(objModule);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        [Route("GetModuleUsingDesigner")]
        public ActionResult GetModuleUsingDesigner(int page, string search)
        {
            try
            {
                int rowCount = 0;
                List<Module> listModule = ModuleBLL.GetModuleUsingDesigner(page, search, out rowCount);

                return Ok(new { count = rowCount, rows = listModule });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }
        #endregion

        #region Save Module
        [Authorize(Policy="ApplicationLock")]
        [HttpPost]
        [Route("saveModule")]
        public ActionResult SaveModule(ModuleParam objParam)
        {
            try
            {
                ClaimsIdentity identity = new ClaimsIdentity();
                /* --- Cek hak akses user --- */
                if (objParam.context != null)
                {
                    identity = objParam.context.User.Identity as ClaimsIdentity;
                }
                else
                {
                    identity = HttpContext.User.Identity as ClaimsIdentity;
                }

                MUser currentUser = new MUser()
                {
                    UserID = identity.FindFirst("UserID").Value,
                    PK_MUser_ID = Convert.ToInt32(identity.FindFirst("PK_MUser_ID").Value),
                    FK_MRole_ID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value),
                    FK_MGroupMenu_ID = Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value)
                };

                string resMsg = "";
                Int64 modID = 0;
                bool isDataValid = ModuleBLL.SaveModule(objParam, currentUser, out resMsg, out modID, false, null);
                if (!isDataValid)
                {
                    return StatusCode(500, resMsg);
                }
                else
                {
                    return Ok(resMsg);
                }
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

      

        [Route("ActionModuleApproval")]
        [Authorize(Policy="ApplicationLock")]
        [HttpPost]
        public ActionResult ActionModuleApproval(CParameterApproval parameterApproval)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MUser currentUser = new MUser()
                {
                    UserID = identity.FindFirst("UserID").Value,
                    FK_MRole_ID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value),
                    FK_MGroupMenu_ID = Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value)
                };
                string alternateUser = identity.FindFirst("AlternateUser").Value;

                if (parameterApproval.PK_ModuleApproval_IDSelected != null
                    && parameterApproval.PK_ModuleApproval_IDSelected.Length > 0)
                {
                    foreach (long pk in parameterApproval.PK_ModuleApproval_IDSelected)
                    {
                        if (!CommonBLL.CheckMenuAccessByApprovalID(currentUser.FK_MGroupMenu_ID, Convert.ToInt32(pk), EActionType.Approval))
                            return StatusCode(500, "You don't have authorization to access this menu.");
                        if (parameterApproval.ActionApproval == EActionApprovalType.Approve)
                        {
                            ModuleBLL.ApproveModuleDesignerApproval(pk, identity.FindFirst("UserID").Value, Convert.ToInt32(identity.FindFirst("PK_MUser_ID").Value), parameterApproval.Review, alternateUser);
                        }
                        else if (parameterApproval.ActionApproval == EActionApprovalType.Reject)
                        {
                            ModuleBLL.RejectModuleDesignerApproval(pk, Convert.ToInt32(identity.FindFirst("PK_MUser_ID").Value), parameterApproval.Review, identity.FindFirst("UserID").Value);
                        }
                    }
                }
                else
                {
                    if (!CommonBLL.CheckMenuAccessByApprovalID(currentUser.FK_MGroupMenu_ID, Convert.ToInt32(parameterApproval.PK_ModuleApproval_ID), EActionType.Approval))
                        return StatusCode(500, "You don't have authorization to access this menu.");
                    if (parameterApproval.ActionApproval == EActionApprovalType.Approve)
                    {
                        ModuleBLL.ApproveModuleDesignerApproval(parameterApproval.PK_ModuleApproval_ID, identity.FindFirst("UserID").Value, Convert.ToInt32(identity.FindFirst("PK_MUser_ID").Value), parameterApproval.Review, alternateUser);
                    }
                    else if (parameterApproval.ActionApproval == EActionApprovalType.Reject)
                    {
                        ModuleBLL.RejectModuleDesignerApproval(parameterApproval.PK_ModuleApproval_ID, Convert.ToInt32(identity.FindFirst("PK_MUser_ID").Value), parameterApproval.Review, identity.FindFirst("UserID").Value);
                    }
                }

                return Ok();
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }
        #endregion

        #region Delete Module
        [Authorize(Policy="ApplicationLock")]
        [HttpPost]
        [Route("deleteModule")]
        public ActionResult DeleteModule(ModuleParam objParam)
        {
            try
            {
                Module objModule = ModuleBLL.GetModuleByName("Module");
                if (objModule == null)
                    return StatusCode(500, "Module not found");

                /* --- Cek hak akses user --- */
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MUser currentUser = new MUser()
                {
                    UserID = identity.FindFirst("UserID").Value,
                    FK_MRole_ID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value),
                    FK_MGroupMenu_ID = Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value)
                };

                if (!CommonBLL.CheckMenuAccess(currentUser.FK_MGroupMenu_ID, objModule.PK_Module_ID, EActionType.Delete))
                    return StatusCode(500, "You don't have authorization to access this menu.");

                Module objDeleteModule = ModuleBLL.GetModuleByID(objParam.ModuleID);
                if (objDeleteModule != null)
                {
                    string invalidMsg = ModuleBLL.CheckExistModule(objDeleteModule);
                    if (!string.IsNullOrEmpty(invalidMsg))
                        return StatusCode(500, invalidMsg);
                }
                else
                {
                    return StatusCode(500, "Invalid Data ID");
                }

                bool IsExistsInModuleApproval = ModuleBLL.IsExistsInModuleApproval(objParam.ModuleID, objModule.PK_Module_ID);
                if (IsExistsInModuleApproval)
                    return StatusCode(500, "Data Already Exists in Pending Approval");

                string successMsg = "";
                objDeleteModule.CreatedBy = currentUser.UserID;
                if (!objModule.IsUseApproval || currentUser.FK_MRole_ID == 1)
                {
                    ModuleBLL.DeleteModuleNoApproval(objDeleteModule, currentUser.UserID);
                    successMsg = "Module " + objDeleteModule.ModuleName + " is deleted successfully.";
                }
                else
                {
                    ModuleBLL.DeleteModuleWithApproval(objDeleteModule, currentUser);
                    successMsg = "Module " + objDeleteModule.ModuleName + " is saved into pending approval to be deleted.";
                }

                return Ok(successMsg);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        #endregion

        #region Activate Module
        [Authorize(Policy="ApplicationLock")]
        [HttpPost]
        [Route("activateModule")]
        public ActionResult ActivateModule(ModuleParam objParam)
        {
            try
            {
                Module objModule = ModuleBLL.GetModuleByName("Module");
                if (objModule == null)
                    return StatusCode(500, "Module not found");

                /* --- Cek hak akses user --- */
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MUser currentUser = new MUser()
                {
                    UserID = identity.FindFirst("UserID").Value,
                    FK_MRole_ID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value),
                    FK_MGroupMenu_ID = Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value)
                };

                if (!CommonBLL.CheckMenuAccess(currentUser.FK_MGroupMenu_ID, objModule.PK_Module_ID, EActionType.Activation))
                    return StatusCode(500, "You don't have authorization to access this menu.");

                Module objActivateModule = ModuleBLL.GetModuleByID(objParam.ModuleID);
                if (objActivateModule == null)
                    return StatusCode(500, "Invalid Data ID");

                bool IsExistsInModuleApproval = ModuleBLL.IsExistsInModuleApproval(objParam.ModuleID, objModule.PK_Module_ID);
                if (IsExistsInModuleApproval)
                    return StatusCode(500, "Data Already Exists in Pending Approval");

                string successMsg = "";
                string activateStr = objActivateModule.Active ? "deactivated" : "activated";
                objActivateModule.CreatedBy = currentUser.UserID;
                if (!objModule.IsUseApproval || currentUser.FK_MRole_ID == 1)
                {
                    ModuleBLL.ActivateModuleNoApproval(objActivateModule, currentUser.UserID);
                    successMsg = "Module " + objActivateModule.ModuleName + " is " + activateStr + " successfully.";
                }
                else
                {
                    ModuleBLL.ActivateModuleWithApproval(objActivateModule, currentUser);
                    successMsg = "Module " + objActivateModule.ModuleName + " is saved into pending approval to be " + activateStr + ".";
                }

                return Ok(successMsg);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        #endregion

        #region Function Parameter
        [Route("GetModuleSp")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetModuleSp(string sp)
        {
            try
            {
                return Ok(NawaDataBLL.ModuleBLL.GetModuleSp(sp));
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("CheckDuplicateModuleDetail")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult CheckDuplicateModuleDetail(string moduleDetailName, int PK_ModuleDetail_ID, bool isEdit)
        {
            try
            {
                return Ok(NawaDataBLL.ModuleBLL.CheckDuplicateModuleDetail(moduleDetailName, PK_ModuleDetail_ID, isEdit));
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        #endregion

        [Route("GetModuleAccess")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetModuleAccess(int moduleID)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MGroupMenuAccess menuAccess = CommonBLL.GetModuleAccess(moduleID, identity.FindFirst("FK_MGroupMenu_ID").Value);

                return Ok(menuAccess);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }
    }
}
