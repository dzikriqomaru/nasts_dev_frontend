﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NawaDataBLL;
using NawaDataDAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace NawaDataAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EODLogSPController : ControllerBase
    {
        #region GET EOD Log SP Controller
        [Route("GetEODLogSPByExecutionID")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetEODLogSPByExecutionID(Int64 ID, int page, string orderBy, string search, string order)
        {
            try
            {
                int rowCount = 0;
                List<EODLogSP> objLogSpDetail = EODLogSPBLL.GetEODLogSPByExecutionID(ID, page, orderBy, order, search, out rowCount);
                return Ok(new { count = rowCount, rows = objLogSpDetail });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }
        #endregion
    }
}
