﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DevExtreme.AspNet.Mvc.FileManagement;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using NawaDataDAL.Models;
using NawaDataBLL;

namespace NawaDataAPI.Controllers
{

    [Route("api/[controller]")]
    [ApiController]

    public class FileManagerController : ControllerBase
    {
        IHostingEnvironment _hostingEnvironment;

        public FileManagerController(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }
        [Authorize]
        [Route("runFileManager", Name = "FileManagementFileSystemApi")]
        public object FileSystem(FileSystemCommand command, string arguments)
        {
            AppSystemParameter sysParam = SystemParameterBLL.GetAppSystemParameter(-65);
            ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
            if (CommonBLL.CheckMenuAccess(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), 301094, EActionType.View))
            {
                if (arguments == null)
                {
                    if (Request.Method == "POST" && Request.Form.ContainsKey("command"))
                    {
                        command = (FileSystemCommand)Enum.Parse(typeof(FileSystemCommand), Request.Form["command"]);
                        arguments = Request.Form["arguments"];
                    }
                }
                var config = new FileSystemConfiguration
                {
                    Request = Request,
                    //FileSystemProvider = new PhysicalFileSystemProvider(sysParam.SettingValue),
                    FileSystemProvider = new PhysicalFileSystemProvider(sysParam.SettingValue),
                    //uncomment the code below to enable file/folder management
                    AllowCopy = true,
                    AllowCreate = true,
                    AllowMove = true,
                    AllowDelete = true,
                    AllowRename = true,
                    AllowUpload = true,
                    AllowDownload = true,
                    AllowedFileExtensions = new[] { ".js", ".json", ".css", ".pdf" }
                };
                var processor = new FileSystemCommandProcessor(config);
                var result = processor.Execute(command, arguments);
                return result.GetClientCommandResult();
            }
            else
            {
                return StatusCode(500, "You don't have authorization to access this menu.");
            }

        }

    }
}
