﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NawaDataBLL;
using NawaDataDAL;
using NawaDataDAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace NawaDataAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ApprovalDesignerController : ControllerBase
    {
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        [Route("GetApprovalDesignerData")]
        public ActionResult GetApprovalDesignerData(int page, string orderBy, string order, string search)
        {
            try
            {
                int rowCount = 0;
                List<DynamicDictionary> approvalDesignerData = ApprovalDesignerBLL.GetApprovalDesignerData(
                    page, orderBy, order, search, out rowCount, null);

                return Ok(new { count = rowCount, rows = approvalDesignerData });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [HttpPost]
        [Route("GetApprovalDesignerData")]
        public ActionResult GetApprovalDesignerData([FromBody] GenericPagingFilterPostBody body)
        {
            try
            {
                int rowCount = 0;
                List<DynamicDictionary> approvalDesignerData = ApprovalDesignerBLL.GetApprovalDesignerData(
                    body.page, body.orderBy, body.order, body.search, out rowCount, body.jsonFilter);

                return Ok(new { count = rowCount, rows = approvalDesignerData });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        [Route("GetDataEditApprovalDesigner")]
        public ActionResult GetDataEditApprovalDesigner(int approvalDesignerID)
        {
            try
            {
                ApprovalDesigner objApprovalDesigner = ApprovalDesignerBLL.GetApprovalDesignerByID(approvalDesignerID);

                if (objApprovalDesigner != null)
                {
                    objApprovalDesigner.ListField = ApprovalDesignerBLL.GetApprovalDesignerField(approvalDesignerID);
                    //objApprovalDesigner.ListValidation = ApprovalDesignerBLL.GetApprovalDesignerValidation(ApprovalDesignerID);
                    List<ApprovalDesignerFieldRegexView> listRegex = new List<ApprovalDesignerFieldRegexView>();

                    if (objApprovalDesigner.ListField.FirstOrDefault(x => x.IsUseRegexValidation) != null)
                    {
                        listRegex = ApprovalDesignerBLL.GetApprovalDesignerFieldRegex(approvalDesignerID);
                    }

                    objApprovalDesigner.ListField.ForEach(x =>
                    {
                        x.ListRegex = new List<ApprovalDesignerFieldRegexView>();
                        x.ListRegex.AddRange(listRegex.Where(y => y.FieldName == x.FieldName).ToList());
                    });
                }
                else
                {
                    throw new Exception("ApprovalDesigner not found");
                }

                return Ok(objApprovalDesigner);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        #region Save Approval Designer
        [Authorize(Policy="ApplicationLock")]
        [HttpPost]
        [Route("SaveApprovalDesigner")]
        public ActionResult SaveApprovalDesigner(ApprovalDesignerParam objParam)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MUser currentUser = new MUser()
                {
                    UserID = identity.FindFirst("UserID").Value
                };
                string invalidMsg = ValidateSaveApprovalDesigner(objParam);

                if (!string.IsNullOrEmpty(invalidMsg))
                {
                    return StatusCode(500, invalidMsg);
                }

                ApprovalDesignerBLL.SaveApprovalDesigner(objParam.InputMode, objParam.DataApprovalDesigner, currentUser.UserID);
                string successMsg = "ApprovalDesigner " + objParam.DataApprovalDesigner.ApprovalDesignerName + " is saved into Database successfully.";

                return Ok(successMsg);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        private string ValidateSaveApprovalDesigner(ApprovalDesignerParam objParam)
        {
            try
            {
                string validateMessage = "";

                if (!objParam.DataApprovalDesigner.IsSupportView && !objParam.DataApprovalDesigner.IsSupportAdd && !objParam.DataApprovalDesigner.IsSupportEdit
                    && !objParam.DataApprovalDesigner.IsSupportDelete && !objParam.DataApprovalDesigner.IsSupportDetail && !objParam.DataApprovalDesigner.IsUseApproval
                    && !objParam.DataApprovalDesigner.IsSupportActivation && !objParam.DataApprovalDesigner.IsSupportUpload)
                {
                    validateMessage += "At least 1 supported actions must be checked" + Environment.NewLine;
                }

                if (!objParam.DataApprovalDesigner.IsUseDesigner)
                {
                    objParam.DataApprovalDesigner.IsUseStoreProcedureValidation = false;
                    objParam.DataApprovalDesigner.ListField = new List<ApprovalDesignerField>();
                }

                #region Check URL Parameter
                if (objParam.DataApprovalDesigner.IsSupportView & string.IsNullOrEmpty(objParam.DataApprovalDesigner.UrlView.Trim()))
                    validateMessage += "URL View is required" + Environment.NewLine;
                if (objParam.DataApprovalDesigner.IsSupportAdd & string.IsNullOrEmpty(objParam.DataApprovalDesigner.UrlAdd.Trim()))
                    validateMessage += "URL Add is required" + Environment.NewLine;
                if (objParam.DataApprovalDesigner.IsSupportEdit & string.IsNullOrEmpty(objParam.DataApprovalDesigner.UrlEdit.Trim()))
                    validateMessage += "URL Edit is required" + Environment.NewLine;
                if (objParam.DataApprovalDesigner.IsSupportDelete & string.IsNullOrEmpty(objParam.DataApprovalDesigner.UrlDelete.Trim()))
                    validateMessage += "URL Delete is required" + Environment.NewLine;
                if (objParam.DataApprovalDesigner.IsSupportDetail & string.IsNullOrEmpty(objParam.DataApprovalDesigner.UrlDetail.Trim()))
                    validateMessage += "URL Detail is required" + Environment.NewLine;
                if (objParam.DataApprovalDesigner.IsSupportActivation & string.IsNullOrEmpty(objParam.DataApprovalDesigner.UrlActivation.Trim()))
                    validateMessage += "URL Activation is required" + Environment.NewLine;
                if (objParam.DataApprovalDesigner.IsSupportUpload & string.IsNullOrEmpty(objParam.DataApprovalDesigner.UrlUpload.Trim()))
                    validateMessage += "URL Upload is required" + Environment.NewLine;

                if (objParam.DataApprovalDesigner.IsUseApproval)
                {
                    if (string.IsNullOrEmpty(objParam.DataApprovalDesigner.UrlApproval.Trim()))
                        validateMessage += "URL Approval is required" + Environment.NewLine;
                    if (string.IsNullOrEmpty(objParam.DataApprovalDesigner.UrlApprovalDetail.Trim()))
                        validateMessage += "URL Approval Detail is required" + Environment.NewLine;
                }
                #endregion

                validateMessage += ApprovalDesignerBLL.CheckExistApprovalDesigner(objParam.DataApprovalDesigner);

                return validateMessage;
            }
            catch
            {
                throw;
            }

        }
        #endregion

        [Authorize(Policy="ApplicationLock")]
        [HttpPost]
        [Route("ActivateApprovalDesigner")]
        public ActionResult ActivateApprovalDesigner(ApprovalDesignerParam objParam)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MUser currentUser = new MUser()
                {
                    UserID = identity.FindFirst("UserID").Value
                };
                string successMsg = ApprovalDesignerBLL.ActivateApprovalDesigner(objParam.ApprovalDesignerID, currentUser.UserID);

                return Ok(successMsg);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [HttpPost]
        [Route("DeleteApprovalDesigner")]
        public ActionResult DeleteApprovalDesigner(ApprovalDesignerParam objParam)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MUser currentUser = new MUser()
                {
                    UserID = identity.FindFirst("UserID").Value
                };
                string successMsg = ApprovalDesignerBLL.DeleteApprovalDesigner(objParam.ApprovalDesignerID, currentUser.UserID);

                return Ok(successMsg);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        public class ApprovalDesignerParam
        {
            public EActionType InputMode { get; set; }
            public ApprovalDesigner DataApprovalDesigner { get; set; }
            public int ApprovalDesignerID { get; set; }
        }
    }
}
