﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NawaDataBLL;
using NawaDataDAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace NawaDataAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class QuickActionController : ControllerBase
    {
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        [Route("GetQuickAction")]
        public ActionResult GetQuickAction()
        {
            ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
            string userID = identity.FindFirst("UserID").Value;
            int pk_MUser_ID = Convert.ToInt32(identity.FindFirst("PK_MUser_ID").Value);

            try
            {
                List<UserAccessHistory> listQuickActions = QuickActionBLL.GetQuickAction(pk_MUser_ID);
                return Ok(listQuickActions);
            }
            catch (Exception ex)
            {
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }
    }
}