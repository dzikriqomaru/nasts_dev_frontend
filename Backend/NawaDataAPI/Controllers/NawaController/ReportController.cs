﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using NawaDataBLL;
using NawaDataDAL;
using NawaDataDAL.Models;
using System.Security.Claims;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace NawaDataAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReportController : Controller
    {
        private IConfiguration _configuration;

        public ReportController(IConfiguration iconfig)
        {
            _configuration = iconfig;
        }

        [Authorize(Policy="ApplicationLock")]
        [Route("download")]
        [HttpGet]
        public FileResult DownloadSsrs(String type)
        {
            try
            {
                byte[] bytes = System.IO.File.ReadAllBytes("D:/lamaran." + type);
                System.IO.MemoryStream ms = new System.IO.MemoryStream(bytes);
                ms.Position = 0;

                String Mime = FileBLL.getMimeType(type);

                return File(ms, Mime);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [Route("get")]
        [HttpGet]
        public ActionResult GetSsrs()
        {
            try
            {
                byte[] bytes = System.IO.File.ReadAllBytes("D:/lamaran.pdf");
                string file = Convert.ToBase64String(bytes);
                return Ok(file);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [Route("ReportQuery")]
        [HttpGet]
        public ActionResult ReportQuery(int page, string orderBy, string order, string search, string filter, bool isAdv)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                int rowCount = 0;
                List<ReportQuery> data = ReportBLL.GetReportQuery(
                    page, orderBy, order, search, filter, isAdv, out rowCount, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value, null);

                return Ok(new { count = rowCount, rows = data });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [Route("ReportQuery")]
        [HttpPost]
        public ActionResult ReportQuery([FromBody] GenericPagingFilterPostBody body)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                int rowCount = 0;
                List<ReportQuery> data = ReportBLL.GetReportQuery(
                    body.page, body.orderBy, body.order, body.search, body.filter, body.isAdv, out rowCount, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value, body.jsonFilter);

                return Ok(new { count = rowCount, rows = data });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [Route("InsertReportQuery")]
        [HttpPost]
        public ActionResult InsertReportQuery(ReportQuery data)
        {

            try
            {

                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MUser currentUser = new MUser()
                {
                    UserID = identity.FindFirst("UserID").Value,
                    PK_MUser_ID = Convert.ToInt32(identity.FindFirst("PK_MUser_ID").Value),
                    FK_MRole_ID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value),
                    FK_MGroupMenu_ID = Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value),
                };

                bool CheckApprovalRow = ReportBLL.CheckApprovalRow(data.PK_ReportQuery_ID);
                if (CheckApprovalRow == false)
                {
                    return StatusCode(500, "Data Already Exists in Pending Approval");
                }

                if (currentUser.FK_MRole_ID == 1)
                {
                    string result = ReportBLL.AddReportQuery(data, currentUser.UserID, false, "");
                    return Ok("Data Saved to database");
                }
                else
                {
                    EActionType InputType = EActionType.Insert;
                    string result = ReportBLL.AddReportQueryWithApproval(InputType,data, currentUser.UserID, currentUser.PK_MUser_ID, currentUser.FK_MRole_ID);
                    if (string.IsNullOrEmpty(result))
                    {
                        return Ok("Pending To Approval");
                    } else
                    {
                        return StatusCode(400, result);
                    }
                }

            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        #region Action Report Approval
        [Route("ActionReportApproval")]
        [HttpPost]
        [Authorize]
        public ActionResult ActionReportApproval(CParameterApproval parameterApproval)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string CurrentUser = identity.FindFirst("UserID").Value;
                int pk_MUser_ID = Convert.ToInt32(identity.FindFirst("PK_MUser_ID").Value);
                if (parameterApproval.PK_ModuleApproval_IDSelected != null && parameterApproval.PK_ModuleApproval_IDSelected.Length > 0)
                {
                    foreach (long pk in parameterApproval.PK_ModuleApproval_IDSelected)
                    {
                        CParameterApproval approvalData = new CParameterApproval
                        {
                            PK_ModuleApproval_ID = pk,
                            ActionApproval = parameterApproval.ActionApproval
                        };
                        ReportBLL.ActionReportApproval(approvalData, CurrentUser, pk_MUser_ID);
                    }
                }
                else
                {
                    ReportBLL.ActionReportApproval(parameterApproval, CurrentUser, pk_MUser_ID);
                }
                return Ok("Success");
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }
        #endregion

        [Route("ViewReportDetail")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult ViewReportDetail(int ID)
        {
            try
            {
                List<Dictionary<string, object>> data = ReportBLL.ViewReportDetail(ID);

                return Ok(data);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ViewReportData")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult ViewReportData(int ID, int page, string orderBy, string order, string search, string filter)
        {
            try
            {
                int rowCount = 0;
                List<DynamicDictionary> data = ReportBLL.ViewReportData(
                    ID, page, orderBy, order, search, filter, out rowCount, null);

                return Ok(new { count = rowCount, rows = data });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ViewReportData")]
        [Authorize(Policy="ApplicationLock")]
        [HttpPost]
        public ActionResult ViewReportData([FromBody] ViewReportDataPostBody body)
        {
            try
            {
                int rowCount = 0;
                List<DynamicDictionary> data = ReportBLL.ViewReportData(
                    body.ID, body.page, body.orderBy, body.order, body.search, "", out rowCount, body.jsonFilter);

                return Ok(new { count = rowCount, rows = data });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("DeleteReportData")]
        [Authorize(Policy="ApplicationLock")]
        [HttpPost]
        public ActionResult DeleteReportData(ReportQuery data)
        {
            ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
            MUser currentUser = new MUser()
            {
                UserID = identity.FindFirst("UserID").Value
            };
            try
            {
                bool CheckApprovalRow = ReportBLL.CheckApprovalRow(data.PK_ReportQuery_ID);
                if (CheckApprovalRow == false)
                {
                    return StatusCode(500, "Data Already Exists in Pending Approval");
                }

                int MRoleID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value);

                if (MRoleID == 1)
                {
                    string result = ReportBLL.DeleteReportQuery(data.PK_ReportQuery_ID, data.ReportName, currentUser.UserID, false, "");

                    return Ok("Data Saved to database");
                }
                else
                {
                    EActionType InputType = EActionType.Delete;
                    string result = ReportBLL.AddReportQueryWithApproval(InputType, data, currentUser.UserID, currentUser.PK_MUser_ID, MRoleID);
                    if (string.IsNullOrEmpty(result))
                    {
                        return Ok("Pending To Approval");
                    }
                    else
                    {
                        return StatusCode(400, result);
                    }
                }

            }

            catch(Exception ex)
            {
                string userID = currentUser.UserID;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }


        [Route("EditReportData")]
        [Authorize(Policy="ApplicationLock")]
        [HttpPost]
        public ActionResult EditReportData(ReportQuery data)
        {
            try
            {

                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MUser currentUser = new MUser()
                {
                    UserID = identity.FindFirst("UserID").Value
                };

                bool CheckApprovalRow = ReportBLL.CheckApprovalRow(data.PK_ReportQuery_ID);
                if (CheckApprovalRow == false)
                {
                    return StatusCode(500, "Data Already Exists in Pending Approval");
                }

                int MRoleID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value);

                if (MRoleID == 1)
                {
                    string result = ReportBLL.EditReportQuery(data, currentUser.UserID, false, "");
                    return Ok("Data Saved to database");
                }
                else
                {
                    EActionType InputType = EActionType.Update;
                    string result = ReportBLL.AddReportQueryWithApproval(InputType, data, currentUser.UserID, currentUser.PK_MUser_ID, MRoleID);
                    if (string.IsNullOrEmpty(result))
                    {
                        return Ok("Pending To Approval");
                    }
                    else
                    {
                        return StatusCode(400, result);
                    }
                }
            }

            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [Route("GetSSRSListAll")]
        [HttpGet]
        public ActionResult GetSSRSListAll()
        {
            try
            {
                CatalogItemOData data = ReportBLL.GetSSRSList();

                return Ok(data);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [Route("GetSSRSList")]
        [HttpGet]
        public ActionResult GetSSRSList(int page, string search)
        {
            try
            {
                CatalogItemOData data = ReportBLL.GetSSRSList(page, search);

                return Ok(data);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [Route("GetSSRSParameter")]
        [HttpPost]
        public ActionResult GetSSRSParameter(ReportData report)
        {
            try
            {
                List<CatalogItemParameter> data = ReportBLL.GetSSRSParameter(report);

                return Ok(data);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [Route("DownloadSSRSReport")]
        [HttpPost]
        public async Task<ActionResult> DownloadSSRSReport(CatalogItemWithParameter report)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                string data = await ReportBLL.DownloadSSRSReport(report.report, report.parameter, userID, _configuration, report.renderAs);

                return Ok(data);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        #region Export Module Data
        [Route("ExportModuleData")]
        [HttpGet]
        [Authorize(Policy="ApplicationLock")]
        public IActionResult ExportModuleData(int ModuleID, int page, string orderBy, string order, string search, string filter)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;

                if (!CommonBLL.CheckMenuAccess(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), ModuleID, EActionType.View))
                    return StatusCode(500, "You don't have authorization to access this menu.");

                MemoryStream ms = ModuleBLL.ExportModuleData(
                    ModuleID,
                    page,
                    orderBy,
                    order,
                    search,
                    filter,
                    false,
                    "",
                    null,
                    identity.FindFirst("UserID").Value,
                    Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value),
                    null
                );
                ms.Position = 0;

                return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Data.xlsx");
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ExportModuleData")]
        [HttpPost]
        [Authorize(Policy="ApplicationLock")]
        public IActionResult ExportModuleData([FromBody] GetModuleDataPostBody body)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;

                if (!CommonBLL.CheckMenuAccess(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), body.ModuleID, EActionType.View))
                    return StatusCode(500, "You don't have authorization to access this menu.");

                MemoryStream ms = ModuleBLL.ExportModuleData(
                    body.ModuleID,
                    body.page,
                    body.orderBy,
                    body.order,
                    body.search,
                    body.filter,
                    body.isAdv,
                    body.exportAs,
                    body.fields,
                    identity.FindFirst("UserID").Value,
                    Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value),
                    body.jsonFilter
                );
                ms.Position = 0;

                return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Data.xlsx");
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }
        #endregion

        #region Export Current Report Data
        [Route("ExportCurrentReportData")]
        [HttpGet]
        [Authorize(Policy="ApplicationLock")]
        public IActionResult ExportCurrentReportData(int ID, int page, string orderBy, string order, string search, string filter)
        {
            try
            {
                MemoryStream ms = ReportBLL.ExportCurrentPage(
                    ID, page, orderBy, order, search, filter, null);
                ms.Position = 0;

                return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ExportCurrentReportData")]
        [HttpPost]
        [Authorize(Policy="ApplicationLock")]
        public IActionResult ExportCurrentReportData([FromBody] ViewReportDataPostBody body)
        {
            try
            {
                MemoryStream ms = ReportBLL.ExportCurrentPage(
                    body.ID, body.page, body.orderBy, body.order, body.search, body.filter, body.fields, body.jsonFilter);
                ms.Position = 0;

                return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }
        #endregion

        #region Export All Report Data
        [Route("ExportAllReportData")]
        [HttpGet]
        [Authorize(Policy="ApplicationLock")]
        public IActionResult ExportAllReportData(int ID, string orderBy, string order, string search, string filter)
        {
            try
            {
                MemoryStream ms = ReportBLL.ExportAllPage(
                    ID, orderBy, order, search, filter, null);
                ms.Position = 0;

                return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ExportAllReportData")]
        [HttpPost]
        [Authorize(Policy="ApplicationLock")]
        public IActionResult ExportAllReportData([FromBody] ViewReportDataPostBody body)
        {
            try
            {
                MemoryStream ms = ReportBLL.ExportAllPage(
                    body.ID, body.orderBy, body.order, body.search, body.filter, body.fields, body.jsonFilter);
                ms.Position = 0;

                return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }
        #endregion

        #region Export Module Report Data
        [Route("ExportModuleReportData")]
        [HttpGet]
        [Authorize(Policy = "ApplicationLock")]
        public IActionResult ExportModuleReportData(int page, string orderBy, string order, string search, string filter)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MemoryStream ms = NawaDataBLL.ReportBLL.ExportModuleData(
                    page, orderBy, order, search, filter, false, "", null, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value, null);

                ms.Position = 0;
                return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Data.xlsx");
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ExportModuleReportData")]
        [HttpPost]
        [Authorize(Policy = "ApplicationLock")]
        public IActionResult ExportModuleReportData([FromBody] GenericPagingFilterPostBody body)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MemoryStream ms = NawaDataBLL.ReportBLL.ExportModuleData(
                    body.page, body.orderBy, body.order, body.search, body.filter, body.isAdv, body.exportAs, body.fields, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value, body.jsonFilter);

                ms.Position = 0;
                return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Data.xlsx");
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }
        #endregion

        #region Export Module All Report Data
        [Route("ExportModuleDataAll")]
        [HttpGet]
        [Authorize(Policy = "ApplicationLock")]
        public IActionResult ExportModuleDataAll(string orderBy, string order, string search, string filter)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MemoryStream ms = NawaDataBLL.ReportBLL.ExportModuleDataAll(
                    orderBy, order, search, filter, false, "", null, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value, null);

                ms.Position = 0;
                return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Data.xlsx");
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ExportModuleDataAll")]
        [HttpPost]
        [Authorize(Policy = "ApplicationLock")]
        public IActionResult ExportModuleDataAll([FromBody] GenericPagingFilterPostBody body)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MemoryStream ms = NawaDataBLL.ReportBLL.ExportModuleDataAll(
                    body.orderBy, body.order, body.search, body.filter, body.isAdv, body.exportAs, body.fields, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value, body.jsonFilter);

                ms.Position = 0;
                return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Data.xlsx");
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }
        #endregion

        #region Export Module Selected Report Data
        [Route("ExportModuleDataSelected")]
        [HttpPost]
        [Authorize(Policy = "ApplicationLock")]
        public IActionResult ExportModuleDataSelected(NawaDataDAL.Models.CSelectedData selectedData)
        {
            try
            {
                MemoryStream ms = NawaDataBLL.ReportBLL.ExportModuleDataSelected(selectedData.SelectedData, selectedData.orderBy, selectedData.order, selectedData.exportAs, selectedData.fields);

                ms.Position = 0;
                return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Data.xlsx");
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }
        #endregion


        [Authorize(Policy="ApplicationLock")]
        [Route("GetReportList")]
        [HttpGet]
        public ActionResult GetListReport(int page, string search)
        {
            try
            {
                int rowCount = 0;
                int totalPage = 0;
                List<ReportLayout> ReportList = ReportBLL.GetListReport(page, out rowCount, out totalPage, search);
                return Ok(new { count = rowCount, rows = ReportList, totalPage = totalPage });
             }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [Route("GetSSRSExportType")]
        [HttpGet]
        public ActionResult GetSSRSExportType()
        {
            try
            {
                List<SSRSExportType> data = ReportBLL.GetExportType();

                return Ok(new { rows = data });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetModuleAccess")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetModuleAccess(int moduleID)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MGroupMenuAccess menuAccess = CommonBLL.GetModuleAccess(moduleID, identity.FindFirst("FK_MGroupMenu_ID").Value);

                return Ok(menuAccess);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }
    }
}
