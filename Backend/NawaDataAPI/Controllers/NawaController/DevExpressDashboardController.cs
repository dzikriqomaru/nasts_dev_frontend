﻿using DevExpress.DashboardAspNetCore;
using DevExpress.DashboardWeb;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System.Xml.Linq;
using NawaDataAPI.Services;
using System;
using System.Collections.Generic;
using System.Data;
using Microsoft.Data.SqlClient;
using NawaDataDAL.Models;
using NawaDataBLL;

namespace WebDashboardAspNetCore.Controllers
{
    //[Route("api/[controller]")]
    //[ApiController]
    [TypeFilter(typeof(CustomExceptionFilter))]
    public class DefaultDashboardController : DashboardController

    {
        public DefaultDashboardController(DashboardConfigurator configurator, IDataProtectionProvider? dataProtectionProvider = null)
        : base(configurator, dataProtectionProvider){}
    }
}