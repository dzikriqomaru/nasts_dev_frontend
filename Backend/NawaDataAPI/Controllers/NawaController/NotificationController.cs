﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NawaDataBLL;
using NawaDataDAL;
using NawaDataDAL.Models;

namespace NawaDataAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NotificationController : ControllerBase
    {
        [Route("GetNotificationData")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetNotificationData(int userID)
        {
            try
            {
                List<MNotification> NotifData = NawaDataBLL.NotificationBLL.GetNotificationByUserID(userID);

                return Ok(NotifData);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string stringUserID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, stringUserID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetNotificationDataMobile")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetNotificationDataMobile(int userID, int page)
        {
            try
            {
                int rowCount = 0;
                List<DynamicDictionary> NotifData = NawaDataBLL.NotificationBLL.GetNotificationDataMobileByUserID(userID, page,out rowCount);

                return Ok(new { count = rowCount, rows = NotifData });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string stringUserID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, stringUserID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetTotalUnread")]
        [HttpGet]
        public ActionResult GetTotalUnread(int userID)
        {
            try
            {
                UnreadNotification unreadNotif = NawaDataBLL.NotificationBLL.getTotalUnreadByUserID(userID);

                return Ok(unreadNotif);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string stringUserID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, stringUserID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("SaveNotificationDetail")]
        [Authorize(Policy="ApplicationLock")]
        [HttpPost]
        public ActionResult SaveNotificationDetail(CNotificationDetail NotificationDetail)
        {
            try
            {
                NawaDataBLL.NotificationBLL.SaveData(NotificationDetail);

                return Ok();
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [Route("ReadNotification")]
        [HttpPost]
        public ActionResult ReadNotification(int NotificationID)
        {
            try
            {
                NawaDataBLL.NotificationBLL.ReadNotification(NotificationID);
                return Ok();
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string stringUserID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, stringUserID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [Route("InsertNotification")]
        [HttpPost]
        public ActionResult InsertNotification([FromBody]MNotification Input)
        {
            try
            {
                NawaDataBLL.NotificationBLL.InsertNotification(Input);
                return Ok("Notification has been saved.");
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string stringUserID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, stringUserID);
                return StatusCode(500, ex.Message);
            }
        }
    }
}
