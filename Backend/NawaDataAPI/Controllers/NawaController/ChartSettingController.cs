﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using NawaDataBLL;
using NawaDataDAL;
using NawaDataDAL.Models;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using System.Diagnostics.Tracing;
using System.IO;

namespace NawaDataAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ChartSettingController : ControllerBase
    {
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        [Route("GetChartSetting")]
        public ActionResult GetChartSetting()
        {
            try
            {
                List<ChartSetting> CharSettingType = ChartSettingBLL.GetChartSetting();
                return Ok(CharSettingType);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }
      

        [Route("validationQuery")]
        [HttpPost]
        public ActionResult validationQuery(ChartDataSet queryTable)
        {
            try
            {
                List<string> col = ChartSettingBLL.ValidationQuery(queryTable);
                return Ok(col);

            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetModuleData")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetModuleData(int page, string orderBy, string order, string search, string filter)
        {
            try
            {
                int rowCount = 0;
                List<NawaDataDAL.DynamicDictionary> moduleData = NawaDataBLL.ChartSettingBLL.GetModuleData(
                    page, orderBy, order, search, filter, out rowCount, null);

                return Ok(new { count = rowCount, rows = moduleData });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetModuleData")]
        [Authorize(Policy="ApplicationLock")]
        [HttpPost]
        public ActionResult GetModuleData([FromBody] GenericPagingFilterPostBody body)
        {
            try
            {
                int rowCount = 0;
                List<NawaDataDAL.DynamicDictionary> moduleData = NawaDataBLL.ChartSettingBLL.GetModuleData(
                    body.page, body.orderBy, body.order, body.search, "", out rowCount, body.jsonFilter);

                return Ok(new { count = rowCount, rows = moduleData });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        [Route("GetChartType")]
        public ActionResult GetChartType()
        {
            try
            {
                List<ChartType> ChartType = ChartSettingBLL.GetChartType();
                return Ok(ChartType);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [Route("GetChartSettingView")]
        [HttpGet]
        public ActionResult GetChartSettingView(int ChartID)
        {
            try
            {
                ChartSettingView chart = ChartSettingBLL.GetChartSettingView(ChartID);
                return Ok(chart);
            }
            catch(Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [Route("SaveChartSetting")]
        [HttpPost]
        public ActionResult SaveChartSetting(ChartSettingBLL.SaveChart chart)
        {
            try
            {
                //string currentUser = "Sysadmin";
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string CurrentUser = identity.FindFirst("UserID").Value;

                ChartSettingBLL.SaveChartWithoutApproval(chart, CurrentUser);
                return Ok("Sucess");
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }
    }
}
