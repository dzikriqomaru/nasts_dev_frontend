using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using NawaDataBLL;
using System;
using System.Security.Claims;
using NawaDataDAL.Models;
using Microsoft.Data.SqlClient;
using System.Data;
using NawaDataDAL;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;

namespace NawaDataAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class ViewBuilderController : ControllerBase
    {


        [Route("GetListViewBuilder")]
        [Authorize(Policy = "ApplicationLock")]
        [HttpGet]

        public ActionResult GetListViewBuilder(long PK_MViewBuilder_ID)
        {
            try
            {
                List<GetViewBuilder> listViewBuilder = ViewBuilderBLL.GetListViewBuilderbyMViewBuilderId(PK_MViewBuilder_ID);
                return Ok(listViewBuilder);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }


        [Route("SubmitViewBuilder")]
        [Authorize(Policy = "ApplicationLock")]
        [HttpPost]
        public ActionResult SubmitViewBuilder([FromBody] ViewBuilderModel body)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MUser currentUser = new MUser()
                {
                    UserID = identity.FindFirst("UserID").Value,
                    PK_MUser_ID = Convert.ToInt32(identity.FindFirst("PK_MUser_ID").Value),
                    FK_MRole_ID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value),
                    FK_MGroupMenu_ID = Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value)
                };

                ModuleParam objParam = new ModuleParam();
                objParam.DataModule = body.DataModule;
                objParam.context = HttpContext;

                object obj = ViewBuilderBLL.SubmitViewBuilder(body.ViewBuilder_JSON, body.FK_MViewBuilder_ID, body.FK_Module_ID, currentUser, objParam, body.IsSaveAsDraft);
                if (obj.GetType().GetProperty("valid").GetValue(obj, null).ToString().ToLower() == "true")
                    return Ok(obj);
                else
                    return StatusCode(500, obj.GetType().GetProperty("message").GetValue(obj, null).ToString());
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }
	
	//get pk view builder from moduleid
        [Route("GetPKViewBuilder")]
        [Authorize(Policy = "ApplicationLock")]
        [HttpGet]
        public ActionResult GetPKViewBuilder(long moduleid)
        {
            try
            {
                long PKViewBuilder = ViewBuilderBLL.GetPKViewBuilder(moduleid);
                return Ok(PKViewBuilder);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }
    }
}
