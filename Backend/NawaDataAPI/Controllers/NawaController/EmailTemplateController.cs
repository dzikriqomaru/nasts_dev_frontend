﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using NawaDataBLL;
using NawaDataDAL;
using System.IO;
using NawaDataDAL.Models;

namespace NawaDataAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmailTemplateController : ControllerBase
    {
        [Authorize(Policy="ApplicationLock")]
        [Route("getMonitoringDuration")]
        [HttpGet]
        public ActionResult getMonitoringDuration()
        {
            try
            {
                List<MonitoringDuration> MonitoringDurationList = EmailTemplateBLL.GetMonitoringDuration();
                return Ok(MonitoringDurationList);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [Route("getEmailTableType")]
        [HttpGet]
        public ActionResult getEmailTableType()
        {
            try
            {
                List<EmailTableType> EmailTableTypeList = EmailTemplateBLL.GetEmailTableType();
                return Ok(EmailTableTypeList);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("validationQuery")]
        [HttpPost]
        public ActionResult validationQuery(EmailTemplateAdditional queryTable)
        {
            try
            {
                string invalid = null;
                List<string> col = EmailTemplateBLL.ValidationQuery(queryTable, out invalid);
                return Ok(new { col = col, invalid = invalid });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("getEmailActionType")]
        [HttpGet]
        public ActionResult getEmailActionType()
        {
            try
            {
                List<EmailActionType> ActionTypes = EmailTemplateBLL.GetEmailActionType();
                return Ok(ActionTypes);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("getEmailAttachmentType")]
        [HttpGet]
        public ActionResult getEmailAttachmentType()
        {
            try
            {
                List<EmailAttachmentTypes> AttachmentType = EmailTemplateBLL.GetEmailAttachmentType();
                return Ok(AttachmentType);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("getEmailRenderAs")]
        [HttpGet]
        public ActionResult getEmailRenderAs()
        {
            try
            {
                List<EmailRenderAs> renderAs = EmailTemplateBLL.GetEmailRenderAs();
                return Ok(renderAs);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [Route("saveEmailTemplate")]
        [HttpPost]
        public ActionResult saveEmailTemplate([FromForm] EmailTemplateBLL.SaveEmail email)
        {
            try
            {
                EmailTemplateBLL.Email emailData = Newtonsoft.Json.JsonConvert.DeserializeObject<EmailTemplateBLL.Email>(email.EmailData);
                var listAttachment = emailData.ListAttachment.Where(item => item.FK_EmailAttachmentType_ID == 1 && item.IsEdited == true).ToList();

                if (email.IsiFiles != null)
                {
                    for (int i = 0; i < listAttachment.Count; i++)
                    {
                        MemoryStream ms = new MemoryStream();
                        email.IsiFiles[i].OpenReadStream().CopyTo(ms);
                        listAttachment[i].IsiFile = ms.ToArray();
                        listAttachment[i].FileType = email.IsiFiles[i].ContentType;

                        ms.Close();
                        ms.Dispose();
                    }
                }

                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string currentUser = identity.FindFirst("UserID").Value;
                int roleID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value);
                Module currentModule = ModuleBLL.GetModuleByName("EmailTemplate");
                string errorMessage;

                if (currentModule != null && currentModule.IsUseApproval && roleID != 1)
                {
                    int pkMUserID = Convert.ToInt32(identity.FindFirst("PK_MUser_ID").Value);
                    errorMessage = EmailTemplateBLL.saveEmailWithApproval(emailData, currentUser, currentModule.PK_Module_ID, pkMUserID, roleID);

                    if (string.IsNullOrEmpty(errorMessage))
                    {
                        return Ok("Pending To Approval");
                    }
                    else
                    {
                        return StatusCode(400, errorMessage);
                    }
                }
                else
                {
                    errorMessage = EmailTemplateBLL.saveEmailWithoutApproval(emailData, currentUser);

                    if (string.IsNullOrEmpty(errorMessage))
                    {
                        return Ok("Data Saved Into Database");
                    }
                    else
                    {
                        return StatusCode(400, errorMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetModuleData")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetModuleData(int page, string orderBy, string order, string search, string filter, bool isAdv)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;

                int rowCount = 0;
                List<DynamicDictionary> moduleData = EmailTemplateBLL.GetModuleData(
                    page, orderBy, order, search, filter, isAdv, out rowCount, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value, null);

                return Ok(new { count = rowCount, rows = moduleData });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetModuleData")]
        [Authorize(Policy="ApplicationLock")]
        [HttpPost]
        public ActionResult GetModuleData([FromBody] GenericPagingFilterPostBody body)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;

                int rowCount = 0;
                List<DynamicDictionary> moduleData = EmailTemplateBLL.GetModuleData(
                    body.page, body.orderBy, body.order, body.search, body.filter, body.isAdv, out rowCount, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value, body.jsonFilter);

                return Ok(new { count = rowCount, rows = moduleData });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("SaveUploadFile")]
        [HttpPost]
        public ActionResult GetUploadFile([FromForm] EmailTemplateAttachment file)
        {
            try
            {
                EmailTemplateBLL.saveFileUpload(file);
                return Ok();
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [Route("GetEmailTemplate")]
        [HttpGet]
        public ActionResult GetEmailTemplate(int EmailID)
        {
            try
            {
                EmailTemplateView email = EmailTemplateBLL.GetEmailTemplate(EmailID);
                return Ok(email);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [Route("GetEmailFile")]
        [HttpGet]
        public ActionResult GetEmailFile(int EmailAttachmentID)
        {
            try
            {
                MemoryStream ms = EmailTemplateBLL.GetEmailFile(EmailAttachmentID);
                EmailTemplateAttachment Att = EmailTemplateBLL.GetEmailTemplateAttachmentByID(EmailAttachmentID);
                ms.Position = 0;
                return File(ms, Att.FileType);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize]
        [Route("GetEmailSchedulerFile")]
        [HttpGet]
        public ActionResult GetEmailSchedulerFile(int SchedulerAttachmentID)
        {
            try
            {
                MemoryStream ms = EmailTemplateBLL.GetEmailSchedulerFile(SchedulerAttachmentID);
                EmailTemplateSchedulerDetailAttachment Att = EmailTemplateBLL.GetEmailTemplateSchedulerDetailAttachmentByID(SchedulerAttachmentID);
                ms.Position = 0;
                return File(ms, Att.FileType);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize]
        [Route("GetEmailTemplateSchedulerDetail")]
        [HttpGet]
        public ActionResult GetEmailTemplateSchedulerDetail(int page, string orderBy, string order, string search, string filter, bool isAdv)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;

                if (CommonBLL.CheckMenuAccess(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), 9095, EActionType.View))
                {
                    int rowCount = 0;
                    List<EmailTemplateSchedulerDetail> EmailTemplateSchedulerDetail = EmailTemplateBLL.GetEmailTemplateSchedulerDetails(
                        page, orderBy, order, search, filter, isAdv, out rowCount, null);
                    return Ok(new { count = rowCount, rows = EmailTemplateSchedulerDetail });
                }
                else
                {
                    return StatusCode(500, "You don't have authorization to access this menu.");
                }
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [Route("GetEmailTemplateSchedulerDetail")]
        [HttpPost]
        public ActionResult GetEmailTemplateSchedulerDetail([FromBody] GenericPagingFilterPostBody body)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;

                if (CommonBLL.CheckMenuAccess(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), 9095, EActionType.View))
                {
                    int rowCount = 0;
                    List<EmailTemplateSchedulerDetail> EmailTemplateSchedulerDetail = EmailTemplateBLL.GetEmailTemplateSchedulerDetails(
                        body.page, body.orderBy, body.order, body.search, "", false, out rowCount, body.jsonFilter);
                    return Ok(new { count = rowCount, rows = EmailTemplateSchedulerDetail });
                }
                else
                {
                    return StatusCode(500, "You don't have authorization to access this menu.");
                }
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize]
        [Route("GetEmailTemplatePreview")]
        [HttpPost]
        public ActionResult GetEmailTemplatePreview([FromBody] EmailTemplatePreviewRequestBody body)
        {
            string ErrorMsg = "";
            try
            {
                int countTotal = 0;
                List<EmailTemplatePreview> Listemail = EmailTemplateBLL.GetEmailTemplatePreview(body.EmailTemplateID, body.ListEmailTemplateAdditional, body.ListEmailTemplateDetail, out countTotal, out ErrorMsg, body.ID, body.PageNo);
                return Ok(new { Count = countTotal, Data = Listemail });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                if (!string.IsNullOrEmpty(ErrorMsg))
                    return StatusCode(500, "Error While Validating Query: " + ErrorMsg);
                else
                    return StatusCode(500, ex.Message);

            }
        }

        [Authorize]
        [Route("ApproveEmailTemplate")]
        [HttpPost]
        public ActionResult ApproveEmailTemplate([FromForm] EmailTemplateBLL.SaveEmail Email) 
        {
            try
            {
                EmailTemplateBLL.Email emailData = Newtonsoft.Json.JsonConvert.DeserializeObject<EmailTemplateBLL.Email>(Email.EmailData);
                int PK_ModuleApproval_ID = emailData.PK_ModuleApproval_ID;

                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string CurrentUser = identity.FindFirst("UserID").Value;
                int pk_MUser_ID = Convert.ToInt32(identity.FindFirst("PK_MUser_ID").Value);

                EmailTemplateBLL.ApproveEmailTemplate(emailData, CurrentUser, PK_ModuleApproval_ID, pk_MUser_ID);

                return Ok("Success");

            }
            catch (Exception ex) 
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [Route("RejectEmailTemplate")]
        [HttpGet]
        public ActionResult RejectEmailTemplate(int PK_ModuleApproval_ID)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                int pk_MUser_ID = Convert.ToInt32(identity.FindFirst("PK_MUser_ID").Value);
                string userID = identity.FindFirst("UserID").Value;

                EmailTemplateBLL.RejectEmailTemplate(PK_ModuleApproval_ID, pk_MUser_ID, userID);

                return Ok("Success");
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        #region Action Email Approval
        [Route("ActionEmailApproval")]
        [HttpPost]
        [Authorize(Policy = "ApplicationLock")]
        public ActionResult ActionEmailApproval(CParameterApproval parameterApproval)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string CurrentUser = identity.FindFirst("UserID").Value;
                int pk_MUser_ID = Convert.ToInt32(identity.FindFirst("PK_MUser_ID").Value);
                string userID = identity.FindFirst("UserID").Value;

                if (parameterApproval.PK_ModuleApproval_IDSelected != null && parameterApproval.PK_ModuleApproval_IDSelected.Length > 0)
                {
                    foreach (long pk in parameterApproval.PK_ModuleApproval_IDSelected)
                    {
                        if (parameterApproval.ActionApproval == EActionApprovalType.Approve)
                        {
                            ModuleApprovalView approvalData = ModuleBLL.GetCustomModuleApprovalByID(pk);
                            EmailTemplateBLL.Email emailData = Newtonsoft.Json.JsonConvert.DeserializeObject<EmailTemplateBLL.Email>(approvalData.ModuleField);
                            EmailTemplateBLL.ApproveEmailTemplate(emailData, CurrentUser, Convert.ToInt32(pk), pk_MUser_ID);
                        }
                        else
                        {
                            EmailTemplateBLL.RejectEmailTemplate(Convert.ToInt32(pk), pk_MUser_ID, userID);
                        }
                    }
                }
                return Ok("Success");
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }
        #endregion

        [Authorize(Policy="ApplicationLock")]
        [Route("GetEmailTemplateImportance")]
        [HttpGet]
        public ActionResult GetEmailTemplateImportance()
        {
            try
            {
                List<EmailTemplateImportance> ListImportance = EmailTemplateBLL.GetEmailTemplateImportance();
                return Ok(ListImportance);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [Route("GetEmailTemplateSensitivity")]
        [HttpGet]
        public ActionResult GetEmailTemplateSensitivity()
        {
            try
            {
                List<EmailTemplateSensitivity> ListSensitivity = EmailTemplateBLL.GetEmailTemplateSensitivity();
                return Ok(ListSensitivity);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetModuleAccess")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetModuleAccess(int moduleID)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MGroupMenuAccess menuAccess = CommonBLL.GetModuleAccess(moduleID, identity.FindFirst("FK_MGroupMenu_ID").Value);

                return Ok(menuAccess);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [Route("GetEmailTemplateSchedulerDetailByID")]
        [HttpGet]
        public ActionResult GetEmailTemplateSchedulerDetailByID(int id)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;

                if (CommonBLL.CheckMenuAccess(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), 9095, EActionType.Update)
                    || CommonBLL.CheckMenuAccess(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), 9095, EActionType.Insert))
                {
                    EmailTemplateSchedulerDetailView EmailTemplateSchedulerDetail = EmailTemplateBLL.GetEmailTemplateSchedulerDetailByID(id);

                    return Ok(EmailTemplateSchedulerDetail);
                }
                else
                {
                    return StatusCode(500, "You don't have authorization to access this menu.");
                }
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [Route("SaveEmailTemplateSchedulerDetail")]
        [HttpPost]
        public ActionResult SaveEmailTemplateSchedulerDetail([FromForm] EmailTemplateBLL.SchedulerData SchedulerData)
        {
            try
            {
                EmailTemplateSchedulerDetailView email = Newtonsoft.Json.JsonConvert.DeserializeObject<EmailTemplateSchedulerDetailView>(SchedulerData.EmailData);
                var listAttachment = email.ListAttachment.ToList();

                if (SchedulerData.body != null)
                {
                    int counter = 0;
                    for (int i = 0; i < listAttachment.Count; i++)
                    {
                        MemoryStream ms = new MemoryStream();
                        listAttachment[i].IsiFiles = SchedulerData.body[counter];
                        if (listAttachment[i].IsiFiles != null)
                        {
                            //SchedulerData.IsiFiles[i].OpenReadStream().CopyTo(ms);
                            listAttachment[i].IsiFiles.OpenReadStream().CopyTo(ms);
                            listAttachment[i].IsiFile = ms.ToArray();
                            listAttachment[i].FileType = SchedulerData.body[counter].ContentType;
                            ms.Close();
                            ms.Dispose();
                            counter++;
                        }
                    }
                }

                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;

                if (CommonBLL.CheckMenuAccess(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), 9095, EActionType.Update)
                    || CommonBLL.CheckMenuAccess(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), 9095, EActionType.Insert))
                {
                    EmailTemplateBLL.SaveEmailTemplateSchedulerDetail(email, identity.FindFirst("UserID").Value);

                    return Ok("Email is being resent");
                }
                else
                {
                    return StatusCode(500, "You don't have authorization to access this menu.");
                }
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }
    }
}
