﻿using DevExpress.AspNetCore.Reporting.WebDocumentViewer;
using DevExpress.AspNetCore.Reporting.WebDocumentViewer.Native.Services;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace NawadataAPI.Controllers.Estatement
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomWebDocumentViewerController : WebDocumentViewerController
    {
        public CustomWebDocumentViewerController(IWebDocumentViewerMvcControllerService controllerService):base(controllerService){}

        [Route("DXXRDV")]
        [HttpPost]
        public override Task<IActionResult> Invoke()
        {
            return base.Invoke();
        }
    }
}