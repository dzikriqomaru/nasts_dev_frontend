﻿using Microsoft.AspNetCore.Authorization;
using System.Threading.Tasks;
using NawaDataDAL;
using NawaDataBLL;
using System.Data;
using System.Security.Claims;
using System;
using Microsoft.Data.SqlClient;
using NawaDataAPI;
using System.Net.Http.Headers;
using Microsoft.AspNetCore.Http;
using NawaDataBLL;
using System.IdentityModel.Tokens.Jwt;

namespace NawaDataAPI.CustomAuth
{
    public class IsExpiredTokenHandler : AuthorizationHandler<MaintenanceLock>
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public IsExpiredTokenHandler(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }
        
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, MaintenanceLock requirement)
        {
            ClaimsIdentity identity = context.User.Identity as ClaimsIdentity;
            string token = _httpContextAccessor.HttpContext.Request.Headers["Authorization"].ToString();

            // unauthorized if empty or not bearer auth
            if (String.IsNullOrEmpty(token) || !token.ToLower().StartsWith("bearer ") || identity.FindFirst("UserID") == null) 
            {
                return Task.CompletedTask;
            }
            else
            {
                token = token.Substring("bearer ".Length); // remove 'bearer ' from token
            }

            //convert  token string to jwt to get expiry date
            DateTime validTo = new JwtSecurityTokenHandler().ReadJwtToken(token).ValidTo;

            try
            {
                string query = "SELECT COUNT(1) FROM usedToken WHERE token = @token";

                SqlParameter[] prms = new SqlParameter[] {
                    new SqlParameter("@token",SqlDbType.VarChar){ Value = token }
                };

                int tokenCount = Convert.ToInt32(NawaDAO.ExecuteScalar(query, prms));

                query = "DELETE FROM usedToken WHERE GETDATE() > dateExpired";

                NawaDAO.ExecuteNonQuery(query);

                //Check if token exist in usedToken table
                if ( tokenCount > 0)
                {
                    return Task.CompletedTask;
                }

                int roleID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value);

                //sysadmin gets login regardless
                if (roleID == 1)
                {
                    context.Succeed(requirement);
                    return Task.CompletedTask;
                }

                query = "SELECT COUNT(1) FROM Maintainer WHERE FK_Role_ID = @roleID";

                prms = new SqlParameter[] {
                    new SqlParameter("@roleID",SqlDbType.Int){Value = roleID }
                };

                int count = Convert.ToInt32(NawaDAO.ExecuteScalar(query, prms));
               
                //role is not found in maintainer
                if (count == 0) 
                {
                    query = "SELECT COUNT(1) FROM MaintenanceLock WHERE GETDATE() BETWEEN " +
                            "DATEADD(day, DATEDIFF(day,'19000101',startDate), CAST(startTime AS DATETIME2(7))) " +
                            "AND DATEADD(day, DATEDIFF(day,'19000101',endDate), CAST(endTime AS DATETIME2(7)))";

                    count = Convert.ToInt32(NawaDAO.ExecuteScalar(query));

                    if (count == 0)
                    {

                        context.Succeed(requirement);
                    }
                    else {
                        NawaDataBLL.UserBLL.InsertUsedToken(Convert.ToInt32(identity.FindFirst("PK_MUser_ID").Value), identity.FindFirst("UserID").Value.ToString(),token, validTo);
                    }
                }
                else
                {
                    context.Succeed(requirement);
                }
                return Task.CompletedTask;
            }
            catch {
                return Task.CompletedTask;
            }
        }


    }
}
