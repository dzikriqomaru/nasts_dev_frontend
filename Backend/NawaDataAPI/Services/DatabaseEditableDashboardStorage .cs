﻿using DevExpress.DashboardWeb;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Xml.Linq;
using Microsoft.Data.SqlClient;
using System;
using DevExpress.DashboardCommon;
using Microsoft.AspNetCore.Http;
using NawaDataDAL.Models;
using NawaDataBLL;
using NawaDataDAL;
using System.Security.Claims;

namespace NawaDataAPI.Services
{
    public class DatabaseEditableDashboardStorage : IEditableDashboardStorage
    {
        public IHttpContextAccessor contextAccessor;

        public DatabaseEditableDashboardStorage(IHttpContextAccessor _contextAccessor)
        {
            contextAccessor = _contextAccessor;
        }

        public string AddDashboard(XDocument document, string dashboardName)
        {
            NawaConnection connection = NawaDAO.CreateConnection();
            connection.BeginTransaction();

            MemoryStream stream = new MemoryStream();
            document.Save(stream);
            stream.Position = 0;

            string userID = "";
            int roleID = 0;

            try
            {

                if (contextAccessor != null)
                {
                    ClaimsIdentity identity = contextAccessor.HttpContext.User.Identity as ClaimsIdentity;
                    userID = identity.FindFirst("UserID").Value;
                    roleID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value);
                }


                Module objModule = ModuleBLL.GetModuleByName("DevExpDashboard");

                if (objModule.IsUseApproval && roleID != 1)
                {
                    int randomValue = -((new Random()).Next(Int32.MaxValue - 3));

                    string query = "DELETE FROM DevExpDashboard_Temporary WHERE CreatedBy = @UserID";
                    SqlParameter[] prms = new SqlParameter[]
                    {
                    new SqlParameter("@UserID", SqlDbType.VarChar){Value = userID}
                    };
                    NawaDAO.ExecuteNonQuery(query, prms);

                    query = "INSERT INTO DevExpDashboard_Temporary(DevExpDashboard_ID," +
                        "DevExpDashboard_Caption,DevExpDashboard_Dashboard,Active,CreatedBy, LastUpdateBy, CreatedDate, LastUpdateDate, DevExpDashboard_DashboardName)" +
                        " VALUES (@PrimaryKey,@Caption,@Dashboard,@Active,@CreatedBy,@LastUpdateBy,GETDATE(),GETDATE(),@DashboardName+'.xml')";

                    prms = new SqlParameter[] {
                    new SqlParameter("@PrimaryKey",SqlDbType.Int){ Value = randomValue},
                    new SqlParameter("@Caption",SqlDbType.VarChar){ Value = dashboardName},
                    new SqlParameter("@Dashboard",SqlDbType.VarBinary){ Value = stream.ToArray()},
                    new SqlParameter("@Active",SqlDbType.Bit){ Value = true},
                    new SqlParameter("@CreatedBy",SqlDbType.VarChar){ Value = userID},
                    new SqlParameter("@LastUpdateBy",SqlDbType.VarChar){ Value = userID},
                    new SqlParameter("@DashboardName",SqlDbType.VarChar){ Value = dashboardName},
                };

                    NawaDAO.ExecuteNonQuery(query, prms);

                    connection.Commit();
                    return randomValue.ToString();
                }
                else
                {
                    string query = "INSERT INTO DevExpDashboard(DevExpDashboard_Caption,DevExpDashboard_Dashboard,Active," +
                        "CreatedBy, LastUpdateBy, CreatedDate, LastUpdateDate, DevExpDashboard_DashboardName) " +
                        "OUTPUT INSERTED.DevExpDashboard_ID VALUES (@Caption,@Dashboard,@Active,@CreatedBy," +
                        "@LastUpdateBy,GETDATE(),GETDATE(),@DashboardName+'.xml')";

                    SqlParameter[] prms = new SqlParameter[] {
                    new SqlParameter("@Caption",SqlDbType.VarChar){ Value = dashboardName},
                    new SqlParameter("@Dashboard",SqlDbType.VarBinary){ Value = stream.ToArray()},
                    new SqlParameter("@Active",SqlDbType.Bit){ Value = true},
                    new SqlParameter("@CreatedBy",SqlDbType.VarChar){ Value = userID},
                    new SqlParameter("@LastUpdateBy",SqlDbType.VarChar){ Value = userID},
                    new SqlParameter("@DashboardName",SqlDbType.VarChar){ Value = dashboardName},
                };

                    string ID = NawaDataDAL.NawaDAO.ExecuteScalar(query, prms).ToString();

                    List<Dictionary<string, object>> oldData = new List<Dictionary<string, object>>();

                    List<Dictionary<string, object>> newData = new List<Dictionary<string, object>>();
                    Dictionary<string, object> dNewData = new Dictionary<string, object>();
                    dNewData.Add("DevExpDashboard_ID", Convert.ToInt32(ID));
                    dNewData.Add("DevExpDashboard_Caption", dashboardName);
                    dNewData.Add("DevExpDashboard_Dashboard", stream.ToArray());
                    dNewData.Add("DevExpDashboard_DashboardName", dashboardName + ".xml");
                    dNewData.Add("Active", true);
                    dNewData.Add("CreatedBy", userID);
                    dNewData.Add("LastUpdateBy", userID);
                    dNewData.Add("ApprovedBy", "");
                    dNewData.Add("CreatedDate", DateTime.Now);
                    dNewData.Add("LastUpdateDate", DateTime.Now);
                    dNewData.Add("ApprovedDate", null);
                    dNewData.Add("AlternateBy", "");
                    newData.Add(dNewData);

                    long headerID = AuditTrailBLL.InputAuditTrailHeader(connection, userID, dashboardName, EActionType.Insert, EAuditTrailStatus.AffectedDatabase, 0, "");

                    DataTable newDT = NawaDAO.ConvertListDictionary(newData);

                    AuditTrailBLL.InputAuditTrailDetail(connection, headerID, EActionType.Insert, newDT, NawaDAO.ConvertListDictionary(oldData));

                    connection.Commit();
                    return ID;
                }


            }
            catch (Exception Ex)
            {
                connection.Rollback();
                connection.Dispose();
                throw;
            }
        }

        public XDocument LoadDashboard(string sDashboardID)
        {
            int dashboardID = Convert.ToInt32(sDashboardID);

            if (dashboardID < 0)
            {
                string query = "SELECT DevExpDashboard_Dashboard FROM DevExpDashboard_Temporary WHERE DevExpDashboard_ID=@ID";

                SqlParameter[] prms = new SqlParameter[] {
                new SqlParameter("@ID",SqlDbType.Int){Value = dashboardID },
                };
                DataRow dr = NawaDataDAL.NawaDAO.ExecuteRow(query, prms);

                if (dr == null) return null;

                byte[] data = (byte[])dr["DevExpDashboard_Dashboard"];
                MemoryStream stream = new MemoryStream(data);

                return XDocument.Load(stream);
            }
            else {
                string query = "SELECT DevExpDashboard_Dashboard FROM DevExpDashboard WHERE DevExpDashboard_ID=@ID";

                SqlParameter[] prms = new SqlParameter[] {
                new SqlParameter("@ID",SqlDbType.Int){Value = dashboardID },
                };
                DataRow dr = NawaDataDAL.NawaDAO.ExecuteRow(query, prms);

                if (dr == null) return null;

                byte[] data = (byte[])dr["DevExpDashboard_Dashboard"];
                MemoryStream stream = new MemoryStream(data);

                return XDocument.Load(stream);
            }
        }

        public IEnumerable<DashboardInfo> GetAvailableDashboardsInfo()
        {
            DataTable dt = NawaDataDAL.NawaDAO.ExecuteTable("SELECT DevExpDashboard_ID AS 'ID', DevExpDashboard_Caption AS 'Name' FROM DevExpDashboard");
            List<DashboardInfo> list = NawaDataDAL.NawaDAO.ConvertDataTable<DashboardInfo>(dt);

            return list;
        }

        public void SaveDashboard(string sDashboardID, XDocument document)
        {
            NawaConnection connection = NawaDAO.CreateConnection();
            connection.BeginTransaction();

            try
            {
                int dashboardID = Convert.ToInt32(sDashboardID);

                Dashboard dashboard = new Dashboard();
                dashboard.LoadFromXDocument(document);
                string title = dashboard.Title.Text;

                string userID = "";
                int roleID = 0;

                string query = "";
                SqlParameter[] prms = null;

                if (contextAccessor != null)
                {
                    ClaimsIdentity identity = contextAccessor.HttpContext.User.Identity as ClaimsIdentity;
                    userID = identity.FindFirst("UserID").Value;
                    roleID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value);
                }

                Module objModule = ModuleBLL.GetModuleByName("DevExpDashboard");

                if (objModule.IsUseApproval && roleID != 1)
                {
                    query = "SELECT COUNT(1) CountRow FROM ModuleApproval WHERE FK_Module_ID = @ModuleID AND ModuleKey = @ModuleKey";
                    prms = new SqlParameter[]
                    {
                    new SqlParameter("@ModuleID", SqlDbType.Int) {Value = objModule.PK_Module_ID},
                    new SqlParameter("@ModuleKey", SqlDbType.Int) {Value = dashboardID}
                    };
                    int countRow = Convert.ToInt32(NawaDAO.ExecuteScalar(query, prms));

                    if (countRow > 0)
                    {
                        CustomException error = new CustomException();
                        error.SafeMessage = "Data already exists in Pending Approval.";
                        error.UnsafeMessage = "Data already exists in Pending Approval.";
                        throw new CustomException(error);
                    }

                    DevExpDashboard oldDashboard = null;

                    if (dashboardID < 0)
                    {
                        query = "SELECT * FROM DevExpDashboard_Temporary WHERE DevExpDashboard_ID = @ID";

                        prms = new SqlParameter[] {
                        new SqlParameter("@ID",SqlDbType.Int){ Value = dashboardID },
                        };
                    }
                    else {
                        query = "SELECT * FROM DevExpDashboard WHERE DevExpDashboard_ID = @ID";

                            prms = new SqlParameter[] {
                            new SqlParameter("@ID",SqlDbType.Int){ Value = dashboardID },
                        };
                    }

                  

                    DataRow drOldDashboard = NawaDAO.ExecuteRow(query, prms);

                    oldDashboard = NawaDAO.ConvertDataTable<DevExpDashboard>(drOldDashboard);

                    oldDashboard.DevExpDashboard_DashboardName = oldDashboard.DevExpDashboard_Caption + ".xml";

                    if (oldDashboard != null)
                    {
                        DevExpDashboard newDashboard = new DevExpDashboard()
                        {
                            DevExpDashboard_ID = oldDashboard.DevExpDashboard_ID,
                            DevExpDashboard_Caption = title,
                            DevExpDashboard_Dashboard = oldDashboard.DevExpDashboard_Dashboard,
                            DevExpDashboard_DashboardName = title + ".xml",
                            Active = oldDashboard.Active,
                            CreatedBy = oldDashboard.CreatedBy,
                            LastUpdateBy = oldDashboard.LastUpdateBy,
                            ApprovedBy = oldDashboard.ApprovedBy,
                            CreatedDate = oldDashboard.CreatedDate,
                            LastUpdateDate = oldDashboard.LastUpdateDate,
                            ApprovedDate = oldDashboard.ApprovedDate,
                            AlternateBy = oldDashboard.AlternateBy
                        };

                        if (newDashboard != null)
                        {
                            using (MemoryStream stream = new MemoryStream())
                            {
                                document.Save(stream);

                                stream.Position = 0;

                                newDashboard.LastUpdateBy = userID;
                                newDashboard.LastUpdateDate = DateTime.Now;
                                newDashboard.DevExpDashboard_Dashboard = stream.ToArray();
                            }

                            string newJSON = "[{\"DevExpDashboard_ID\":" + dashboardID + ",\"DevExpDashboard_Caption\":\"" + newDashboard.DevExpDashboard_Caption + "\",\"DevExpDashboard_DashboardName\":\"" + newDashboard.DevExpDashboard_DashboardName + "\",\"DevExpDashboard_Dashboard\":\"" + Convert.ToBase64String(newDashboard.DevExpDashboard_Dashboard) + "\"}]";
                            string oldJSON = "[{\"DevExpDashboard_ID\":" + dashboardID + ",\"DevExpDashboard_Caption\":\"" + oldDashboard.DevExpDashboard_Caption + "\",\"DevExpDashboard_DashboardName\":\"" + oldDashboard.DevExpDashboard_DashboardName + "\",\"DevExpDashboard_Dashboard\":\"" + Convert.ToBase64String(oldDashboard.DevExpDashboard_Dashboard) + "\",\"Active\":\"\",\"CreatedBy\":\"\",\"LastUpdateBy\":\"\",\"ApprovedBy\":\"\",\"CreatedDate\":\"\",\"LastUpdateDate\":\"\",\"ApprovedDate\":\"\",\"Alternateby\":\"\"}]";

                            query = @"INSERT INTO ModuleApproval (ModuleName,ModuleKey, ModuleField, ModuleFieldBefore, PK_ModuleAction_ID, " +
                                    "CreatedDate, CreatedBy, FK_Module_ID) VALUES (@ModuleName, @ModuleKey, @ModuleField, " +
                                    "@ModuleFieldBefore, @PK_ModuleAction_ID, GETDATE(), @CreatedBy, @FK_Module_ID); SELECT SCOPE_IDENTITY() AS LastID;";

                            prms = new SqlParameter[]
                            {
                            new SqlParameter("@ModuleName",SqlDbType.VarChar){ Value = objModule.ModuleName },
                            new SqlParameter("@ModuleKey",SqlDbType.VarChar){ Value = sDashboardID },
                            new SqlParameter("@ModuleField",SqlDbType.VarChar){ Value = newJSON },
                            new SqlParameter("@ModuleFieldBefore",SqlDbType.VarChar){ Value = dashboardID < 0 ? "":oldJSON },
                            new SqlParameter("@PK_ModuleAction_ID",SqlDbType.Int){ Value = dashboardID < 0 ? Convert.ToInt32(EActionType.Insert) : Convert.ToInt32(EActionType.Update) },
                            new SqlParameter("@CreatedBy",SqlDbType.VarChar){ Value = userID },
                            new SqlParameter("@FK_Module_ID",SqlDbType.Int){ Value = Convert.ToInt32(objModule.PK_Module_ID) },
                            };

                            long lastID = Convert.ToInt32(NawaDAO.ExecuteScalar(query, prms));

                            List<Dictionary<string, object>> oldData = new List<Dictionary<string, object>>();
                            Dictionary<string, object> dOldData = new Dictionary<string, object>();
                            dOldData.Add("DevExpDashboard_ID", oldDashboard.DevExpDashboard_ID);
                            dOldData.Add("DevExpDashboard_Caption", oldDashboard.DevExpDashboard_Caption);
                            dOldData.Add("DevExpDashboard_Dashboard", oldDashboard.DevExpDashboard_Dashboard);
                            dOldData.Add("DevExpDashboard_DashboardName", oldDashboard.DevExpDashboard_DashboardName);
                            dOldData.Add("Active", oldDashboard.Active);
                            dOldData.Add("CreatedBy", oldDashboard.CreatedBy);
                            dOldData.Add("LastUpdateBy", oldDashboard.LastUpdateBy);
                            dOldData.Add("ApprovedBy", oldDashboard.ApprovedBy);
                            dOldData.Add("CreatedDate", oldDashboard.CreatedDate);
                            dOldData.Add("LastUpdateDate", oldDashboard.LastUpdateDate);
                            dOldData.Add("ApprovedDate", oldDashboard.ApprovedDate);
                            dOldData.Add("AlternateBy", oldDashboard.AlternateBy);
                            oldData.Add(dOldData);

                            List<Dictionary<string, object>> newData = new List<Dictionary<string, object>>();
                            Dictionary<string, object> dNewData = new Dictionary<string, object>();
                            dNewData.Add("DevExpDashboard_ID", newDashboard.DevExpDashboard_ID);
                            dNewData.Add("DevExpDashboard_Caption", newDashboard.DevExpDashboard_Caption);
                            dNewData.Add("DevExpDashboard_Dashboard", newDashboard.DevExpDashboard_Dashboard);
                            dNewData.Add("DevExpDashboard_DashboardName", newDashboard.DevExpDashboard_DashboardName);
                            dNewData.Add("Active", newDashboard.Active);
                            dNewData.Add("CreatedBy", newDashboard.CreatedBy);
                            dNewData.Add("LastUpdateBy", newDashboard.LastUpdateBy);
                            dNewData.Add("ApprovedBy", newDashboard.ApprovedBy);
                            dNewData.Add("CreatedDate", newDashboard.CreatedDate);
                            dNewData.Add("LastUpdateDate", newDashboard.LastUpdateDate);
                            dNewData.Add("ApprovedDate", newDashboard.ApprovedDate);
                            dNewData.Add("AlternateBy", newDashboard.AlternateBy);
                            newData.Add(dNewData);

                            long headerID = AuditTrailBLL.InputAuditTrailHeader(connection, userID, 
                            newDashboard.DevExpDashboard_Caption,dashboardID < 0 ? EActionType.Insert : EActionType.Update, 
                            EAuditTrailStatus.WaitingApproval, lastID, "");

                            DataTable newDT = NawaDAO.ConvertListDictionary(newData);

                            AuditTrailBLL.InputAuditTrailDetail(connection, headerID, dashboardID < 0 ? EActionType.Insert : EActionType.Update, newDT, 
                            NawaDAO.ConvertListDictionary(oldData));
                        }
                    }
                }
                else
                {
                    DevExpDashboard oldDashboard = null;

                    query = "SELECT * FROM DevExpDashboard WHERE DevExpDashboard_ID = @ID";

                    prms = new SqlParameter[] {
                        new SqlParameter("@ID",SqlDbType.Int){ Value = dashboardID }
                    };

                    DataRow drOldDashboard = NawaDAO.ExecuteRow(query, prms);
                    oldDashboard = NawaDAO.ConvertDataTable<DevExpDashboard>(drOldDashboard);

                    using (MemoryStream stream = new MemoryStream())
                    {
                        document.Save(stream);
                        stream.Position = 0;

                        query = "UPDATE DevExpDashboard SET DevExpDashboard_Dashboard = @Dashboard, DevExpDashboard_Caption=@Title, DevExpDashboard_DashboardName = @Title+'.xml' WHERE DevExpDashboard_ID = @ID";

                        prms = new SqlParameter[] {
                            new SqlParameter("@Dashboard",SqlDbType.VarBinary){ Value = stream.ToArray()},
                            new SqlParameter("@Title",SqlDbType.VarChar){ Value = title },
                            new SqlParameter("@ID",SqlDbType.Int){Value = dashboardID },
                        };

                        NawaDataDAL.NawaDAO.ExecuteNonQuery(query, prms);
                    }
                    if (oldDashboard != null)
                    {
                        DevExpDashboard newDashboard = new DevExpDashboard()
                        {
                            DevExpDashboard_ID = oldDashboard.DevExpDashboard_ID,
                            DevExpDashboard_Caption = oldDashboard.DevExpDashboard_Caption,
                            DevExpDashboard_Dashboard = oldDashboard.DevExpDashboard_Dashboard,
                            DevExpDashboard_DashboardName = oldDashboard.DevExpDashboard_DashboardName,
                            Active = oldDashboard.Active,
                            CreatedBy = oldDashboard.CreatedBy,
                            LastUpdateBy = userID,
                            ApprovedBy = oldDashboard.ApprovedBy,
                            CreatedDate = oldDashboard.CreatedDate,
                            LastUpdateDate = DateTime.Now,
                            ApprovedDate = oldDashboard.ApprovedDate,
                            AlternateBy = oldDashboard.AlternateBy
                        };

                        if (newDashboard != null)
                        {
                            List<Dictionary<string, object>> oldData = new List<Dictionary<string, object>>();
                            Dictionary<string, object> dOldData = new Dictionary<string, object>();
                            dOldData.Add("DevExpDashboard_ID", oldDashboard.DevExpDashboard_ID);
                            dOldData.Add("DevExpDashboard_Caption", oldDashboard.DevExpDashboard_Caption);
                            dOldData.Add("DevExpDashboard_Dashboard", oldDashboard.DevExpDashboard_Dashboard);
                            dOldData.Add("DevExpDashboard_DashboardName", oldDashboard.DevExpDashboard_DashboardName);
                            dOldData.Add("Active", oldDashboard.Active);
                            dOldData.Add("CreatedBy", oldDashboard.CreatedBy);
                            dOldData.Add("LastUpdateBy", oldDashboard.LastUpdateBy);
                            dOldData.Add("ApprovedBy", oldDashboard.ApprovedBy);
                            dOldData.Add("CreatedDate", oldDashboard.CreatedDate);
                            dOldData.Add("LastUpdateDate", oldDashboard.LastUpdateDate);
                            dOldData.Add("ApprovedDate", oldDashboard.ApprovedDate);
                            dOldData.Add("AlternateBy", oldDashboard.AlternateBy);
                            oldData.Add(dOldData);

                            List<Dictionary<string, object>> newData = new List<Dictionary<string, object>>();
                            Dictionary<string, object> dNewData = new Dictionary<string, object>();
                            dNewData.Add("DevExpDashboard_ID", newDashboard.DevExpDashboard_ID);
                            dNewData.Add("DevExpDashboard_Caption", newDashboard.DevExpDashboard_Caption);
                            dNewData.Add("DevExpDashboard_Dashboard", newDashboard.DevExpDashboard_Dashboard);
                            dNewData.Add("DevExpDashboard_DashboardName", newDashboard.DevExpDashboard_DashboardName);
                            dNewData.Add("Active", newDashboard.Active);
                            dNewData.Add("CreatedBy", newDashboard.CreatedBy);
                            dNewData.Add("LastUpdateBy", newDashboard.LastUpdateBy);
                            dNewData.Add("ApprovedBy", newDashboard.ApprovedBy);
                            dNewData.Add("CreatedDate", newDashboard.CreatedDate);
                            dNewData.Add("LastUpdateDate", newDashboard.LastUpdateDate);
                            dNewData.Add("ApprovedDate", newDashboard.ApprovedDate);
                            dNewData.Add("AlternateBy", newDashboard.AlternateBy);
                            newData.Add(dNewData);

                            long headerID = AuditTrailBLL.InputAuditTrailHeader(connection, userID,
                            newDashboard.DevExpDashboard_Caption,EActionType.Update, EAuditTrailStatus.AffectedDatabase, 0, "");

                            DataTable newDT = NawaDAO.ConvertListDictionary(newData);

                            AuditTrailBLL.InputAuditTrailDetail(connection, headerID, dashboardID < 0 ? EActionType.Insert : EActionType.Update, newDT,
                            NawaDAO.ConvertListDictionary(oldData));
                        }

                       
                    }
                }
                connection.Commit();
                }
                catch (Exception ex)
                {
                    connection.Rollback();
                    connection.Dispose();

                    throw;
                }
            }
           
        }
}