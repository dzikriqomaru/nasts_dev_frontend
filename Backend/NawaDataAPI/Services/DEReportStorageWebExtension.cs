﻿using DevExpress.CodeParser;
using DevExpress.DataAccess.ConnectionParameters;
using DevExpress.DataAccess.Sql;
using DevExpress.XtraReports;
using DevExpress.XtraReports.UI;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.CodeAnalysis;
using Microsoft.Data.SqlClient;
using NawaDataBLL;
using NawaDataDAL;
using NawaDataDAL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Security.Claims;

namespace NawaDataAPI.Services
{
    public class DEReportStorageWebExtension : DevExpress.XtraReports.Web.Extensions.ReportStorageWebExtension
    {
        private DataTable reportsTable = new DataTable();

        public IHttpContextAccessor contextAccessor;

        public DEReportStorageWebExtension(IHttpContextAccessor _contextAccessor)
        {
            contextAccessor = _contextAccessor;
        }

        public DEReportStorageWebExtension(IWebHostEnvironment env)
        {
            try
            {
                #region Examples from DevExpress
                //ReportDirectory = Path.Combine(env.ContentRootPath, "Reports");
                //if (!Directory.Exists(ReportDirectory))
                //{
                //    Directory.CreateDirectory(ReportDirectory);
                //}
                #endregion

                reportsTable = NawaDataDAL.NawaDAO.ExecuteTable("SELECT PK_Report_ID,DisplayName FROM ES_ReportLayout");
            }
            catch (Exception)
            {

            }
        }

        public override bool CanSetData(string url) { return true; }
        
        public override bool IsValidUrl(string url) { return true; }
        
        public override byte[] GetData(string url)
        {
            #region Examples from DevExpress
            //try
            //{
            //    if (Directory.EnumerateFiles(ReportDirectory).
            //        Select(Path.GetFileNameWithoutExtension).Contains(url))
            //    {
            //        return File.ReadAllBytes(Path.Combine(ReportDirectory, url + FileExtension));
            //    }
            //    throw new DevExpress.XtraReports.Web.ClientControls.FaultException(
            //        string.Format("Could not find report '{0}'.", url));
            //}
            //catch (Exception ex)
            //{
            //    throw new DevExpress.XtraReports.Web.ClientControls.FaultException(
            //        string.Format("Could not find report '{0}'.", url));
            //}
            #endregion

            // Get the report data from the storage.
            try
            {
                long reportID = Convert.ToInt64(url);

                if (reportID < 0)
                {
                    string query = "SELECT * FROM ES_ReportLayout_Temporary where CAST(PK_Report_ID AS VARCHAR) = @url ";
                    SqlParameter[] prms = new SqlParameter[]
                    {
                    new SqlParameter("@url", SqlDbType.VarChar) { Value = url }
                    };
                    DataRow row = NawaDAO.ExecuteRow(query, prms);

                    if (row == null) return null;

                    byte[] reportData = RepopulateDataSource((Byte[])row["LayoutData"]);
                    return reportData;
                }
                else
                {
                    string query = "SELECT * FROM ES_ReportLayout where PK_Report_ID = @url ";
                    SqlParameter[] prms = new SqlParameter[]
                    {
                    new SqlParameter("@url", SqlDbType.VarChar) { Value = url }
                    };
                    DataRow row = NawaDAO.ExecuteRow(query, prms);

                    if (row == null) return null;

                    byte[] reportData = RepopulateDataSource((Byte[])row["LayoutData"]);
                    return reportData;

                }
            }
            catch (Exception ex)
            {
                //throw new DevExpress.XtraReports.Web.ClientControls.FaultException(string.Format("Could not find report '{0}'.", url));
                string messages = ex.Message;
                ex = ex.InnerException;
                while (ex != null)
                {
                    messages += "; " + ex.Message;
                    ex = ex.InnerException;
                }

                throw new DevExpress.XtraReports.Web.ClientControls.FaultException(string.Format("Could not find report '{0}'. {1}", url, messages));
            }
        }

        private byte[] RepopulateDataSource(byte[] report)
        {
            var xtraReport = XtraReport.FromStream(new MemoryStream(report));
            var list = xtraReport.AllControls<XRSubreport>().ToList();
            var sqlDataSources = new List<SqlDataSource>();

            try
            {
                sqlDataSources = DataSourceManager.GetDataSources<SqlDataSource>(xtraReport, true).ToList();
            }
            catch (Exception)
            {
                if (list.Count > 0)
                {
                    sqlDataSources = DataSourceManager.GetDataSources<SqlDataSource>(xtraReport, false).ToList();
                }
            }

            if (sqlDataSources.Count > 0)
            {
                foreach (var sqlDataSource in sqlDataSources)
                {
                    string query = "SELECT serverName,userName,password,connectionName,dbName, connectionType FROM remoteConnection WHERE connectionName = @name";

                    SqlParameter[] prms = new SqlParameter[] {
                        new SqlParameter("@name", SqlDbType.VarChar){Value = sqlDataSource.Name }
                    };

                    DataRow dr = NawaDAO.ExecuteRow(query, prms);

                    if (dr != null)
                    {
                        query = "SELECT pgp_sym_decrypt(@encryptedPass::bytea,'AES_KEY')";

                        prms = new SqlParameter[]
                        {
                        new SqlParameter("@encryptedPass", SqlDbType.VarChar){ Value = Convert.ToString(dr["password"]) },
                        };

                        string decryptedPassword = Convert.ToString(NawaDAO.ExecuteScalar(query, prms));

                        DataConnectionParametersBase parameters = null;

                        if (Convert.ToInt32(dr["connectionType"]) == (int)EActionConnectionType.PostgreSQL)
                        {
                            parameters = new PostgreSqlConnectionParameters(Convert.ToString(dr["serverName"]), Convert.ToString(dr["dbName"]), Convert.ToString(dr["userName"]), decryptedPassword);
                        }
                        else if (Convert.ToInt32(dr["connectionType"]) == (int)EActionConnectionType.MsSql)
                        {
                            parameters = new MsSqlConnectionParameters(Convert.ToString(dr["serverName"]), Convert.ToString(dr["dbName"]), Convert.ToString(dr["userName"]), decryptedPassword, MsSqlAuthorizationType.SqlServer);
                        }
                        else if (Convert.ToInt32(dr["connectionType"]) == (int)EActionConnectionType.BigQuery)
                        {
                            parameters = new BigQueryConnectionParameters(Convert.ToString(dr["serverName"]), Convert.ToString(dr["dbName"]), Convert.ToString(dr["userName"]), decryptedPassword);
                        }
                        else
                        {
                            parameters = new PostgreSqlConnectionParameters(Convert.ToString(dr["serverName"]), Convert.ToString(dr["dbName"]), Convert.ToString(dr["userName"]), decryptedPassword);
                        }

                        sqlDataSource.ConnectionParameters = parameters;
                        sqlDataSource.RebuildResultSchema();
                    }
                }
            }

            using (var newMemStream = new MemoryStream())
            {
                xtraReport.SaveLayoutToXml(newMemStream);
                return newMemStream.ToArray();
            }
        }

        public override Dictionary<string, string> GetUrls()
        {
            #region Examples from DevExpress
            //return Directory.GetFiles(ReportDirectory, "*" + FileExtension)
            //                         .Select(Path.GetFileNameWithoutExtension)
            //                         .ToDictionary<string, string>(x => x);
            #endregion

            //database
            reportsTable.Clear();
            reportsTable = NawaDAO.ExecuteTable("SELECT PK_Report_ID, DisplayName FROM ES_ReportLayout");

            // Get URLs and display names for all reports available in the storage.
            var v = reportsTable.AsEnumerable()
                  .ToDictionary<DataRow, string, string>(dataRow => ((Int64)dataRow["PK_Report_Id"]).ToString(),
                                                         dataRow => (string)dataRow["DisplayName"]);
            return v;
        }

        public override void SetData(XtraReport report, string url)
        {
            #region Examples from DevExpress
            //report.SaveLayoutToXml(Path.Combine(ReportDirectory, url + FileExtension));
            #endregion

            // Write a report to the storage under the specified URL.
            using (MemoryStream ms = new MemoryStream())
            {
                report.SaveLayoutToXml(ms);
                SaveEditData(ms.GetBuffer(), url);
            }
        }

        private void SaveEditData(byte[] ms, string url)
        {
            NawaConnection connection = NawaDAO.CreateConnection();
            connection.BeginTransaction();

            string query = "";
            SqlParameter[] prms = null;

            try
            {
                long reportID = Convert.ToInt64(url);

                string userID = "";
                int roleID = 0;

                if (contextAccessor != null)
                {
                    ClaimsIdentity identity = contextAccessor.HttpContext.User.Identity as ClaimsIdentity;
                    userID = identity.FindFirst("UserID").Value;
                    roleID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value);
                }

                NawaDataDAL.Models.Module objModule = ModuleBLL.GetModuleByName("ES_ReportLayout");
                if (objModule == null)
                {
                    throw new Exception("Module Report Layout not Found!");
                }

                if (objModule.IsUseApproval && roleID != 1)
                {
                    query = "SELECT COUNT(1) CountRow FROM ModuleApproval WHERE FK_Module_ID = @ModuleID AND ModuleKey = @ModuleKey";
                    prms = new SqlParameter[]
                    {
                        new SqlParameter("@ModuleID", DbType.Int32) {Value = objModule.PK_Module_ID},
                        new SqlParameter("@ModuleKey", DbType.String) {Value = url}
                    };
                    int countRow = Convert.ToInt32(NawaDAO.ExecuteScalar(query, prms, connection));

                    if (countRow > 0)
                    {
                        throw new Exception("Data already exists in Pending Approval.");
                    }

                    ES_ReportLayout oldReport = null;

                    if (reportID < 0)
                    {
                        query = "SELECT * FROM ES_ReportLayout_Temporary WHERE PK_Report_ID = @ID";
                        prms = new SqlParameter[] {
                            new SqlParameter("@ID", DbType.Int64){Value = reportID }
                        };

                        DataRow drOldReport = NawaDAO.ExecuteRow(query, prms, connection);
                        oldReport = NawaDAO.ConvertDataTable<ES_ReportLayout>(drOldReport);
                    }
                    else
                    {
                        query = "SELECT * FROM ES_ReportLayout WHERE PK_Report_ID = @ID";
                        prms = new SqlParameter[] {
                            new SqlParameter("@ID", DbType.Int64){Value = reportID }
                        };

                        DataRow drOldReport = NawaDAO.ExecuteRow(query, prms, connection);
                        oldReport = NawaDAO.ConvertDataTable<ES_ReportLayout>(drOldReport);
                    }

                    if (oldReport != null)
                    {
                        ES_ReportLayout newReport = new ES_ReportLayout()
                        {
                            PK_Report_ID = oldReport.PK_Report_ID,
                            DisplayName = oldReport.DisplayName,
                            LayoutData = oldReport.LayoutData,
                            LayoutDataName = oldReport.LayoutDataName,
                            Active = oldReport.Active,
                            CreatedBy = oldReport.CreatedBy,
                            LastUpdateBy = oldReport.LastUpdateBy,
                            ApprovedBy = oldReport.ApprovedBy,
                            CreatedDate = oldReport.CreatedDate,
                            LastUpdateDate = oldReport.LastUpdateDate,
                            ApprovedDate = oldReport.ApprovedDate,
                            AlternateBy = oldReport.AlternateBy,
                            Notes = oldReport.Notes
                        };

                        if (newReport != null)
                        {
                            newReport.LastUpdateBy = userID;
                            newReport.LastUpdateDate = DateTime.Now;
                            newReport.LayoutData = ms;

                            string newJSON = "[{\"PK_Report_ID\":" + reportID + ",\"DisplayName\":\"" + newReport.DisplayName + "\",\"LayoutDataName\":\"" + newReport.LayoutDataName + "\",\"LayoutData\":\"" + Convert.ToBase64String(newReport.LayoutData) + "\",\"Notes\":\"" + newReport.Notes + "\"}]";
                            string oldJSON = "[{\"PK_Report_ID\":" + reportID + ",\"DisplayName\":\"" + oldReport.DisplayName + "\",\"LayoutDataName\":\"" + oldReport.LayoutDataName + "\",\"LayoutData\":\"" + Convert.ToBase64String(oldReport.LayoutData) + "\",\"Notes\":\"" + oldReport.Notes + "\",\"Active\":\"\",\"CreatedBy\":\"\",\"LastUpdateBy\":\"\",\"ApprovedBy\":\"\",\"CreatedDate\":\"\",\"LastUpdateDate\":\"\",\"ApprovedDate\":\"\",\"Alternateby\":\"\"}]";

                            query = @"INSERT INTO ModuleApproval(ModuleName, ModuleKey, ModuleField, ModuleFieldBefore, PK_ModuleAction_ID, CreatedDate, CreatedBy, FK_Module_ID)
                                        VALUES(@ModuleName, @ModuleKey, @ModuleField, @ModuleFieldBefore, @PK_ModuleAction_ID, GETDATE(), @CreatedBy, @FK_Module_ID);
                                      SELECT SCOPE_IDENTITY() AS LastID;";
                            prms = new SqlParameter[]
                            {
                                new SqlParameter("@ModuleName", DbType.String) { Value = objModule.ModuleName },
                                new SqlParameter("@ModuleKey", DbType.String) { Value = url },
                                new SqlParameter("@ModuleField", DbType.String) { Value = newJSON },
                                new SqlParameter("@ModuleFieldBefore", DbType.String) { Value = reportID < 0 ? "" : oldJSON },
                                new SqlParameter("@PK_ModuleAction_ID", DbType.Int32) { Value = reportID < 0 ? Convert.ToInt32(EActionType.Insert) : Convert.ToInt32(EActionType.Update) },
                                new SqlParameter("@CreatedBy", DbType.String) { Value = userID },
                                new SqlParameter("@FK_Module_ID", DbType.Int32) { Value = Convert.ToInt32(objModule.PK_Module_ID) }
                            };
                            long lastID = Convert.ToInt64(NawaDAO.ExecuteScalar(query, prms, connection));

                            List<Dictionary<string, object>> oldData = new List<Dictionary<string, object>>();
                            Dictionary<string, object> dOldData = new Dictionary<string, object>();
                            dOldData.Add("PK_Report_ID", oldReport.PK_Report_ID);
                            dOldData.Add("DisplayName", oldReport.DisplayName);
                            dOldData.Add("LayoutData", oldReport.LayoutData);
                            dOldData.Add("LayoutDataName", oldReport.LayoutDataName);
                            dOldData.Add("Active", oldReport.Active);
                            dOldData.Add("CreatedBy", oldReport.CreatedBy);
                            dOldData.Add("LastUpdateBy", oldReport.LastUpdateBy);
                            dOldData.Add("ApprovedBy", oldReport.ApprovedBy);
                            dOldData.Add("CreatedDate", oldReport.CreatedDate);
                            dOldData.Add("LastUpdateDate", oldReport.LastUpdateDate);
                            dOldData.Add("ApprovedDate", oldReport.ApprovedDate);
                            dOldData.Add("AlternateBy", oldReport.AlternateBy);
                            dOldData.Add("Notes", oldReport.Notes);
                            oldData.Add(dOldData);

                            List<Dictionary<string, object>> newData = new List<Dictionary<string, object>>();
                            Dictionary<string, object> dNewData = new Dictionary<string, object>();
                            dNewData.Add("PK_Report_ID", newReport.PK_Report_ID);
                            dNewData.Add("DisplayName", newReport.DisplayName);
                            dNewData.Add("LayoutData", newReport.LayoutData);
                            dNewData.Add("LayoutDataName", newReport.LayoutDataName);
                            dNewData.Add("Active", newReport.Active);
                            dNewData.Add("CreatedBy", newReport.CreatedBy);
                            dNewData.Add("LastUpdateBy", newReport.LastUpdateBy);
                            dNewData.Add("ApprovedBy", newReport.ApprovedBy);
                            dNewData.Add("CreatedDate", newReport.CreatedDate);
                            dNewData.Add("LastUpdateDate", newReport.LastUpdateDate);
                            dNewData.Add("ApprovedDate", newReport.ApprovedDate);
                            dNewData.Add("AlternateBy", newReport.AlternateBy);
                            dNewData.Add("Notes", oldReport.Notes);
                            newData.Add(dNewData);

                            long headerID = AuditTrailBLL.InputAuditTrailHeader(connection, newReport.CreatedBy, newReport.DisplayName, reportID < 0 ? EActionType.Insert : EActionType.Update, EAuditTrailStatus.WaitingApproval, lastID, "");

                            DataTable newDT = NawaDAO.ConvertListDictionary(newData);

                            AuditTrailBLL.InputAuditTrailDetail(connection, headerID, reportID < 0 ? EActionType.Insert : EActionType.Update, newDT, NawaDAO.ConvertListDictionary(oldData));
                        }
                    }
                }
                else
                {
                    ES_ReportLayout oldReport = null;

                    query = "SELECT * FROM ES_ReportLayout WHERE PK_Report_ID = @ID";

                    prms = new SqlParameter[] {
                        new SqlParameter("@ID", SqlDbType.Int){Value = reportID }
                    };

                    DataRow drOldReport = NawaDAO.ExecuteRow(query, prms, connection);
                    oldReport = NawaDAO.ConvertDataTable<ES_ReportLayout>(drOldReport);

                    SqlParameter[] listParams = new SqlParameter[] {
                        new SqlParameter("@layoutData", SqlDbType.VarBinary) { Value = ms },
                        new SqlParameter("@url", SqlDbType.VarChar) { Value = url }
                    };

                    query = "UPDATE ES_ReportLayout " + Environment.NewLine;
                    query += "SET LayoutData = @layoutData" + Environment.NewLine;
                    query += "WHERE CAST(PK_Report_Id AS VARCHAR) = @url " + Environment.NewLine;

                    NawaDAO.ExecuteNonQuery(query, listParams.ToArray(), connection);

                    if (oldReport != null)
                    {
                        ES_ReportLayout newReport = new ES_ReportLayout()
                        {
                            PK_Report_ID = oldReport.PK_Report_ID,
                            DisplayName = oldReport.DisplayName,
                            LayoutData = oldReport.LayoutData,
                            LayoutDataName = oldReport.LayoutDataName,
                            Active = oldReport.Active,
                            CreatedBy = oldReport.CreatedBy,
                            LastUpdateBy = userID,
                            ApprovedBy = oldReport.ApprovedBy,
                            CreatedDate = oldReport.CreatedDate,
                            LastUpdateDate = DateTime.Now,
                            ApprovedDate = oldReport.ApprovedDate,
                            AlternateBy = oldReport.AlternateBy,
                            Notes = oldReport.Notes
                        };

                        if (newReport != null)
                        {
                            List<Dictionary<string, object>> oldData = new List<Dictionary<string, object>>();
                            Dictionary<string, object> dOldData = new Dictionary<string, object>();
                            dOldData.Add("PK_Report_ID", oldReport.PK_Report_ID);
                            dOldData.Add("DisplayName", oldReport.DisplayName);
                            dOldData.Add("LayoutData", oldReport.LayoutData);
                            dOldData.Add("LayoutDataName", oldReport.LayoutDataName);
                            dOldData.Add("Active", oldReport.Active);
                            dOldData.Add("CreatedBy", oldReport.CreatedBy);
                            dOldData.Add("LastUpdateBy", oldReport.LastUpdateBy);
                            dOldData.Add("ApprovedBy", oldReport.ApprovedBy);
                            dOldData.Add("CreatedDate", oldReport.CreatedDate);
                            dOldData.Add("LastUpdateDate", oldReport.LastUpdateDate);
                            dOldData.Add("ApprovedDate", oldReport.ApprovedDate);
                            dOldData.Add("AlternateBy", oldReport.AlternateBy);
                            dOldData.Add("Notes", oldReport.Notes);
                            oldData.Add(dOldData);

                            List<Dictionary<string, object>> newData = new List<Dictionary<string, object>>();
                            Dictionary<string, object> dNewData = new Dictionary<string, object>();
                            dNewData.Add("PK_Report_ID", newReport.PK_Report_ID);
                            dNewData.Add("DisplayName", newReport.DisplayName);
                            dNewData.Add("LayoutData", newReport.LayoutData);
                            dNewData.Add("LayoutDataName", newReport.LayoutDataName);
                            dNewData.Add("Active", newReport.Active);
                            dNewData.Add("CreatedBy", newReport.CreatedBy);
                            dNewData.Add("LastUpdateBy", newReport.LastUpdateBy);
                            dNewData.Add("ApprovedBy", newReport.ApprovedBy);
                            dNewData.Add("CreatedDate", newReport.CreatedDate);
                            dNewData.Add("LastUpdateDate", newReport.LastUpdateDate);
                            dNewData.Add("ApprovedDate", newReport.ApprovedDate);
                            dNewData.Add("AlternateBy", newReport.AlternateBy);
                            dNewData.Add("Notes", oldReport.Notes);
                            newData.Add(dNewData);

                            long headerID = AuditTrailBLL.InputAuditTrailHeader(connection, newReport.CreatedBy, newReport.DisplayName, EActionType.Update, EAuditTrailStatus.AffectedDatabase, 0, "");

                            DataTable newDT = NawaDAO.ConvertListDictionary(newData);

                            AuditTrailBLL.InputAuditTrailDetail(connection, headerID, EActionType.Update, newDT, NawaDAO.ConvertListDictionary(oldData));

                        }
                    }
                }

                connection.Commit();
            }
            catch (Exception ex)
            {
                connection.Rollback();
                connection.Dispose();

                throw;
            }
        }

        public override string SetNewData(XtraReport report, string defaultUrl)
        {
            #region Examples from DevExpress
            //SetData(report, defaultUrl);
            //return defaultUrl;
            #endregion

            // Save a report to the storage with a new URL. 
            // The defaultUrl parameter is the report name that the user specifies.

            using (MemoryStream ms = new MemoryStream())
            {
                report.SaveLayoutToXml(ms);
                long reportID = SaveAddData(ms.GetBuffer(), defaultUrl);

                return reportID.ToString();
            }
        }

        private long SaveAddData(byte[] ms, string defaultUrl)
        {
            NawaConnection connection = NawaDAO.CreateConnection();
            connection.BeginTransaction();

            long reportID = 0;

            try
            {
                string userID = "";
                int roleID = 0;

                if (contextAccessor != null)
                {
                    ClaimsIdentity identity = contextAccessor.HttpContext.User.Identity as ClaimsIdentity;
                    userID = identity.FindFirst("UserID").Value;
                    roleID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value);
                }

                NawaDataDAL.Models.Module objModule = ModuleBLL.GetModuleByName("ES_ReportLayout");

                if (objModule.IsUseApproval && roleID != 1)
                {
                    List<SqlParameter> listParams = new List<SqlParameter>();
                    string query;
                    string tableFields = "";
                    string insertParams = "";
                    string moduleName = "ES_ReportLayout_Temporary";

                    query = "DELETE FROM " + moduleName + " WHERE CreatedBy = @CreatedBy";
                    NawaDAO.ExecuteNonQuery(query, new SqlParameter[] { new SqlParameter("@CreatedBy", DbType.String) { Value = userID } }, connection);

                    if (!String.IsNullOrEmpty(tableFields)) tableFields += ", ";

                    tableFields += "PK_Report_ID, DisplayName, LayoutData, LayoutDataName, Active, CreatedBy, CreatedDate";

                    if (!String.IsNullOrEmpty(insertParams)) insertParams += ", ";

                    insertParams += "@PrimaryKey,@DisplayName, @LayoutData, @DisplayName + '.repx', @Active, @CreatedBy, GETDATE()";

                    reportID = -((new Random()).Next(Int32.MaxValue - 3));
                    listParams.Add(new SqlParameter("@PrimaryKey", SqlDbType.Int) { Value = reportID });
                    listParams.Add(new SqlParameter("@Active", SqlDbType.Bit) { Value = true });
                    listParams.Add(new SqlParameter("@CreatedBy", SqlDbType.VarChar) { Value = userID });
                    listParams.Add(new SqlParameter("@DisplayName", SqlDbType.VarChar) { Value = defaultUrl });
                    listParams.Add(new SqlParameter("@LayoutData", SqlDbType.Binary) { Value = ms });

                    query = "INSERT INTO " + moduleName + " (" + tableFields + ") " + Environment.NewLine;
                    query += "VALUES (" + insertParams + ");" + Environment.NewLine;
                    NawaDAO.ExecuteNonQuery(query, listParams.ToArray(), connection);

                    ES_ReportLayout newReport = new ES_ReportLayout()
                    {
                        PK_Report_ID = reportID,
                        DisplayName = defaultUrl,
                        LayoutData = ms,
                        LayoutDataName = defaultUrl + ".repx",
                        Active = true,
                        CreatedBy = userID,
                        LastUpdateBy = userID,
                        ApprovedBy = "",
                        CreatedDate = DateTime.Now,
                        LastUpdateDate = DateTime.Now,
                        ApprovedDate = DateTime.Now,
                        AlternateBy = "",
                        Notes = ""
                    };

                    if (newReport != null)
                    {
                        newReport.LastUpdateBy = userID;
                        newReport.LastUpdateDate = DateTime.Now;
                        newReport.LayoutData = ms;

                        string newJSON = "[{\"PK_Report_ID\":" + reportID + ",\"DisplayName\":\"" + newReport.DisplayName + "\",\"LayoutDataName\":\"" + newReport.LayoutDataName + "\",\"LayoutData\":\"" + Convert.ToBase64String(newReport.LayoutData) + "\",\"Notes\":\"" + newReport.Notes + "\"}]";
                        string oldJSON = "";

                        query = @"INSERT INTO ModuleApproval(ModuleName, ModuleKey, ModuleField, ModuleFieldBefore, PK_ModuleAction_ID, CreatedDate, CreatedBy, FK_Module_ID)
                                        VALUES(@ModuleName, @ModuleKey, @ModuleField, @ModuleFieldBefore, @PK_ModuleAction_ID, GETDATE(), @CreatedBy, @FK_Module_ID);
                                      SELECT SCOPE_IDENTITY() AS LastID;";
                        SqlParameter[] prms = new SqlParameter[]
                        {
                                new SqlParameter("@ModuleName", SqlDbType.VarChar) { Value = objModule.ModuleName },
                                new SqlParameter("@ModuleKey", SqlDbType.VarChar) { Value = reportID.ToString() },
                                new SqlParameter("@ModuleField", SqlDbType.VarChar) { Value = newJSON },
                                new SqlParameter("@ModuleFieldBefore", SqlDbType.VarChar) { Value = reportID < 0 ? "" : oldJSON },
                                new SqlParameter("@PK_ModuleAction_ID", SqlDbType.Int) { Value = reportID < 0 ? Convert.ToInt32(EActionType.Insert) : Convert.ToInt32(EActionType.Update) },
                                new SqlParameter("@CreatedBy", SqlDbType.VarChar) { Value = userID },
                                new SqlParameter("@FK_Module_ID", SqlDbType.Int) { Value = Convert.ToInt32(objModule.PK_Module_ID) }
                        };
                        long lastID = Convert.ToInt64(NawaDAO.ExecuteScalar(query, prms, connection));

                        List<Dictionary<string, object>> newData = new List<Dictionary<string, object>>();
                        Dictionary<string, object> dNewData = new Dictionary<string, object>();
                        dNewData.Add("PK_Report_ID", newReport.PK_Report_ID);
                        dNewData.Add("DisplayName", newReport.DisplayName);
                        dNewData.Add("LayoutData", newReport.LayoutData);
                        dNewData.Add("LayoutDataName", newReport.LayoutDataName);
                        dNewData.Add("Active", newReport.Active);
                        dNewData.Add("CreatedBy", newReport.CreatedBy);
                        dNewData.Add("LastUpdateBy", newReport.LastUpdateBy);
                        dNewData.Add("ApprovedBy", newReport.ApprovedBy);
                        dNewData.Add("CreatedDate", newReport.CreatedDate);
                        dNewData.Add("LastUpdateDate", newReport.LastUpdateDate);
                        dNewData.Add("ApprovedDate", newReport.ApprovedDate);
                        dNewData.Add("AlternateBy", newReport.AlternateBy);
                        dNewData.Add("Notes", "");
                        newData.Add(dNewData);

                        long headerID = AuditTrailBLL.InputAuditTrailHeader(connection,newReport.CreatedBy,newReport.DisplayName,EActionType.Insert, EAuditTrailStatus.WaitingApproval,lastID,"");

                        DataTable newDT = NawaDAO.ConvertListDictionary(newData);

                        AuditTrailBLL.InputAuditTrailDetail(connection,headerID,EActionType.Insert,newDT,null);
                    }
                }
                else
                {
                    List<SqlParameter> listParams = new List<SqlParameter>();
                    string query;
                    string tableFields = "";
                    string insertParams = "";
                    string moduleName = "ES_ReportLayout";

                    if (!String.IsNullOrEmpty(tableFields)) tableFields += ", ";

                    tableFields += "DisplayName, LayoutData, LayoutDataName, Active, CreatedBy, CreatedDate";

                    if (!String.IsNullOrEmpty(insertParams)) insertParams += ", ";

                    insertParams += "@DisplayName, @LayoutData, @DisplayName + '.repx', @Active, @CreatedBy, GETDATE()";

                    listParams.Add(new SqlParameter("@Active", DbType.Boolean) { Value = true });
                    listParams.Add(new SqlParameter("@CreatedBy", DbType.String) { Value = userID });
                    listParams.Add(new SqlParameter("@DisplayName", DbType.String) { Value = defaultUrl });
                    listParams.Add(new SqlParameter("@LayoutData", DbType.Binary) { Value = ms });

                    query = "INSERT INTO " + moduleName + " (" + tableFields + ") " + Environment.NewLine;
                    query += "VALUES (" + insertParams + "); SELECT SCOPE_IDENTITY();" + Environment.NewLine;
                    reportID = Convert.ToInt64(NawaDAO.ExecuteScalar(query, listParams.ToArray(), connection));

                    List<Dictionary<string, object>> oldData = new List<Dictionary<string, object>>();

                    List<Dictionary<string, object>> newData = new List<Dictionary<string, object>>();
                    Dictionary<string, object> dNewData = new Dictionary<string, object>();
                    dNewData.Add("PK_Report_ID", reportID);
                    dNewData.Add("DisplayName", defaultUrl);
                    dNewData.Add("LayoutData", ms);
                    dNewData.Add("LayoutDataName", defaultUrl + ".xml");
                    dNewData.Add("Active", true);
                    dNewData.Add("CreatedBy", userID);
                    dNewData.Add("LastUpdateBy", userID);
                    dNewData.Add("ApprovedBy", "");
                    dNewData.Add("CreatedDate", DateTime.Now);
                    dNewData.Add("LastUpdateDate", DateTime.Now);
                    dNewData.Add("ApprovedDate", null);
                    dNewData.Add("AlternateBy", "");
                    newData.Add(dNewData);

                    long headerID = AuditTrailBLL.InputAuditTrailHeader(connection, userID, defaultUrl, EActionType.Insert, EAuditTrailStatus.AffectedDatabase, 0, "");

                    DataTable newDT = NawaDAO.ConvertListDictionary(newData);

                    AuditTrailBLL.InputAuditTrailDetail(connection, headerID, EActionType.Insert, newDT, NawaDAO.ConvertListDictionary(oldData));
                }

                connection.Commit();

                return reportID;
            }
            catch (Exception ex)
            {
                connection.Rollback();
                connection.Dispose();

                throw;
            }
        }
    }
}