﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Hosting;
using NawaDataDAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace NawaDataAPI.Services
{
    public class CustomExceptionFilter : IExceptionFilter
    {

        internal bool isDevelopmentMode = false;

        public CustomExceptionFilter(IWebHostEnvironment hostingEnvironment)
        {
            this.isDevelopmentMode = hostingEnvironment.IsDevelopment();
        }
        string GetJson(string message)
        {
            return $"{{ \"Message\":\"{message}\" }}";
        }

        public virtual void OnException(ExceptionContext context)
        {
            if (context.ExceptionHandled || context.Exception == null)
            {
                return;
            }

            CustomException customException = context.Exception as CustomException;
            string message = customException != null ? (isDevelopmentMode ? customException.UnsafeMessage : customException.SafeMessage) : "";

            context.Result = new ContentResult
            {
                Content = GetJson(message),
                ContentType = "application/json",
                StatusCode = (int)HttpStatusCode.BadRequest
            };

            context.ExceptionHandled = true;
        }
    }
}
