﻿using DevExpress.DataAccess.ConnectionParameters;
using DevExpress.DataAccess.Sql;
using DevExpress.DataAccess.Web;
using System;
using System.Collections.Generic;
using System.Data;
using Microsoft.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using DevExpress.DataAccess.UI.Wizard;

namespace NawaDataAPI.Services
{
    public class MyDataSourceWizardConnectionStringsProvider : IDataSourceWizardConnectionStringsProvider
    {
        private const string salt = "33aba0ba-ab2f-42cf-a075-4da60a5b283f";

        public Dictionary<string, string> GetConnectionDescriptions()
        {
            Dictionary<string, string> connections = new Dictionary<string, string>();
            string query = "SELECT COUNT(13) FROM remoteConnection";

            int count = Convert.ToInt32(NawaDataDAL.NawaDAO.ExecuteScalar(query));

            if (count > 0)
            {
                query = "SELECT * FROM remoteConnection";

                DataTable dt = NawaDataDAL.NawaDAO.ExecuteTable(query);

                foreach (DataRow dr in dt.Rows)
                {
                    // Customize the loaded connections list.  
                    connections.Add(dr["PK_Conn_ID"].ToString(), dr["connectionName"].ToString());
                }
            }
                
            return connections;
        }

        public DataConnectionParametersBase GetDataConnectionParameters(string ID )
        {
            string query = "SELECT serverName,remoteConnection.[userName],password,connectionName FROM remoteConnection WHERE PK_Conn_ID = @ID";

            SqlParameter[] prms = new SqlParameter[] {
                new SqlParameter("@ID",SqlDbType.Int){Value = Convert.ToInt32(ID) }
            };

            DataRow dr = NawaDataDAL.NawaDAO.ExecuteRow(query,prms);


            query = "EXEC usp_DecryptReportConnectionData @encryptedPass";

            prms = new SqlParameter[]
            {
                    new SqlParameter("@encryptedPass", SqlDbType.VarChar){ Value = Convert.ToString(dr["password"]) },
            };


            string decryptedPass = Convert.ToString(NawaDataDAL.NawaDAO.ExecuteScalar(query, prms));

            // Return custom connection parameters for the custom connection.
            MsSqlConnectionParameters parameters = new MsSqlConnectionParameters(dr["serverName"].ToString(),
                    dr["connectionName"].ToString(), dr["userName"].ToString(), decryptedPass, MsSqlAuthorizationType.SqlServer);

            SqlDataSource dataSource = new SqlDataSource(parameters);

            dataSource.RebuildResultSchema();
            
            return new MsSqlConnectionParameters(dr["serverName"].ToString(), dr["connectionName"].ToString(), 
            dr["userName"].ToString(), decryptedPass, MsSqlAuthorizationType.SqlServer);
        }
    }
}
