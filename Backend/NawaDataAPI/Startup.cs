// EXCLUDE DEVEXPRESS
using DevExpress.AspNetCore;
using DevExpress.AspNetCore.Reporting;
using DevExpress.DashboardAspNetCore;
using DevExpress.DashboardWeb;
using DevExpress.XtraReports.Web.Extensions;
using ITfoxtec.Identity.Saml2;
using ITfoxtec.Identity.Saml2.MvcCore.Configuration;
using ITfoxtec.Identity.Saml2.Schemas.Metadata;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Primitives;
using Microsoft.IdentityModel.Tokens;
using NawaDataAPI.CustomAuth;
using NawaDataAPI.Services; // EXCLUDE DEVEXPRESS
using NawaDataDAL;
using Newtonsoft.Json.Serialization;
using Ocelot.DependencyInjection;
using Ocelot.Middleware;
using System;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
//using DevExpress.DashboardWeb;
//using DevExpress.DashboardAspNetCore;
//using DevExpress.DashboardCommon;

namespace NawaDataAPI;

public class Startup
{
    public static string userReport = "";
    public static string passReport = "";

    public Startup(IConfiguration configuration)
    {
        Configuration = configuration;
    }
    readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";
    readonly string authenticationProviderKey = "_myAuthenticationProvider";

    public IConfiguration Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {
        services.AddHttpContextAccessor();

        services.TryAddSingleton<IActionContextAccessor, ActionContextAccessor>();

        services.AddOcelot(Configuration).AddDelegatingHandler<HeaderDelegatingHandler>();

        services.AddScoped<DashboardConfigurator>((IServiceProvider serviceProvider) =>
        {
            DashboardConfigurator configurator = new DashboardConfigurator();
            configurator.SetConnectionStringsProvider(new MyDataSourceWizardConnectionStringsProvider());
            IHttpContextAccessor contextAccessor = (IHttpContextAccessor)serviceProvider.GetService(typeof(IHttpContextAccessor));
            DatabaseEditableDashboardStorage dataBaseDashboardStorage = new DatabaseEditableDashboardStorage(contextAccessor);
            configurator.SetDashboardStorage(dataBaseDashboardStorage);
            configurator.AllowExecutingCustomSql = true;
            //configurator.SetConnectionStringsProvider(new CustomConnectionStringProvider());
            return configurator;

        });



        // Register reporting services in an application's dependency injection container.
        services.AddDevExpressControls(); // EXCLUDE DEVEXPRESS

        services.AddScoped<ReportStorageWebExtension>((IServiceProvider serviceProvider) =>
        {
            IHttpContextAccessor contextAccessor = (IHttpContextAccessor)serviceProvider.GetService(typeof(IHttpContextAccessor));
            return new DEReportStorageWebExtension(contextAccessor);
        });  // EXCLUDE DEVEXPRESS

        services.AddMvcCore();

        services.AddMvc();

        services.ConfigureReportingServices(configurator =>
        {
            configurator.ConfigureWebDocumentViewer(viewerConfigurator =>
            {
                viewerConfigurator.UseCachedReportSourceBuilder();
            });

            configurator.ConfigureReportDesigner(designerConfigurator =>
            {
                designerConfigurator.EnableCustomSql();
            });
        }); // EXCLUDE DEVEXPRESS


        services.AddCors(options =>
                {
                    options.AddPolicy(MyAllowSpecificOrigins,
                    builder =>
                    {
                        builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader();
                    });
                });

        services.AddHsts(options =>
        {
            options.MaxAge = TimeSpan.FromDays(730);
            options.IncludeSubDomains = true;
            options.Preload = true;
        });



        services.AddControllers()
            //.AddJsonOptions(options => options.JsonSerializerOptions.PropertyNamingPolicy = null);
            .AddNewtonsoftJson(options => { options.SerializerSettings.ContractResolver = new DefaultContractResolver(); });
        //.AddNewtonsoftJson(options => options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);

        services.AddRazorPages();

        if (Configuration.GetSection("Saml2").Value != null)
        {
            services.Configure<Saml2Configuration>(Configuration.GetSection("Saml2"));

            services.Configure<Saml2Configuration>(saml2Configuration =>
            {
                saml2Configuration.AllowedAudienceUris.Add(saml2Configuration.Issuer);

                var entityDescriptor = new EntityDescriptor();
                entityDescriptor.ReadIdPSsoDescriptorFromUrl(new Uri(Configuration["Saml2:IdPMetadata"]));
                if (entityDescriptor.IdPSsoDescriptor != null)
                {
                    saml2Configuration.SingleSignOnDestination = entityDescriptor.IdPSsoDescriptor.SingleSignOnServices.First().Location;
                    saml2Configuration.SignatureValidationCertificates.AddRange(entityDescriptor.IdPSsoDescriptor.SigningCertificates);
                }
                else
                {
                    throw new Exception("IdPSsoDescriptor not loaded from metadata.");
                }
            });

            services.AddSaml2();
        }


        services.AddSingleton<IAuthorizationHandler, IsExpiredTokenHandler>();

            services.AddAuthorization(options =>
            {
                options.AddPolicy("ApplicationLock",
                    policyBuilder => policyBuilder.AddRequirements(
                        new MaintenanceLock()
                 ));    
            });

            services.AddMemoryCache();

        services.AddSingleton<IAuthorizationMiddlewareResultHandler, CustomAuthMiddlewareHandler>();

        services.AddAuthentication(o =>
        {
            o.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            o.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
        })
            .AddJwtBearer(options =>
        {
            options.RequireHttpsMetadata = false;
            options.SaveToken = true;
            options.TokenValidationParameters = new TokenValidationParameters()
            {
                ValidateIssuerSigningKey = true,
                ValidateIssuer = false,
                ValidateAudience = false,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:Key"])),
                ClockSkew = TimeSpan.Zero
            };
        });

        NawaDataBLL.CommonBLL.SetVersion();

        NawaDAO.Config(false, NawaDAO.DBType.SQLServer, Configuration["Connection:Server"], Configuration["Connection:Database"], Configuration["Connection:User"], Configuration["Connection:Password"], string.Empty, Convert.ToInt32(Configuration["Connection:CommandTimeout"]));
        //PostgreSQL config
        //NawaDAO.Config(NawaDAO.DBType.PostgreSQL, Configuration["Connection_PostgreSQL:Server"], Configuration["Connection_PostgreSQL:Database"], Configuration["Connection_PostgreSQL:User"], Configuration["Connection_PostgreSQL:Password"], Configuration["Connection_PostgreSQL:Port"], Convert.ToInt32(Configuration["Connection_PostgreSQL:CommandTimeout"]));

        userReport = NawaDataBLL.SystemParameterBLL.GetAppSystemParameter(9).SettingValue;
        passReport = NawaDataBLL.SystemParameterBLL.GetAppSystemParameter(10).SettingValue;
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        CultureInfo cultureInfo = new CultureInfo("en-US");

        cultureInfo.NumberFormat.NumberDecimalSeparator = ".";

        CultureInfo.DefaultThreadCurrentCulture = cultureInfo;
        //DevExpress.XtraReports.Web.ClientControls.LoggerService.Initialize(new MyLoggerService()); // EXCLUDE DEVEXPRESS

        app.UseStaticFiles(new StaticFileOptions()
        {
            OnPrepareResponse = context =>
            {
                context.Context.Response.Headers["Cache-Control"] = "no-cache, no-store, must-revalidate, max-age=0";
            }
        });

        app.UseDevExpressControls(); // EXCLUDE DEVEXPRESS


        if (env.IsDevelopment())
        {
            app.UseDeveloperExceptionPage();
        }
        else
        {
            app.UseHsts();
        }

        // Add other security headers
        //app.UseMiddleware<SecurityHeadersMiddleware>();


        //Used to redirect from http to https, uncomment this in production if https is needed
        //app.UseHttpsRedirection();

        app.UseRouting();

        app.UseCors(MyAllowSpecificOrigins);

        app.UseAuthentication();

        app.UseAuthorization();

        app.UseEndpoints(endpoints =>
        {
            //endpoints.MapControllers();

            //endpoints.MapRazorPages();

            EndpointRouteBuilderExtension.MapDashboardRoute(endpoints, "api/DevExpressDashboard", "DefaultDashboard"); // EXCLUDE DEVEXPRESS

            endpoints.MapControllerRoute(name: "default", pattern: "{controller=Home}/{action=Index}/{id?}");
        });

        app.UseOcelot().Wait();
    }
}

public class HeaderDelegatingHandler : DelegatingHandler
{
    private readonly IHttpContextAccessor _httpContextAccessor;

    public HeaderDelegatingHandler(IHttpContextAccessor httpContextAccessor)
    {
        _httpContextAccessor = httpContextAccessor;
    }

    protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
    {
        request.Headers.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.ASCII.GetBytes(String.Format("{0}:{1}", Startup.userReport, Startup.passReport))));

        return await base.SendAsync(request, cancellationToken);
    }
}

public sealed class SecurityHeadersMiddleware
{
    private readonly RequestDelegate _next;

    public SecurityHeadersMiddleware(RequestDelegate next)
    {
        _next = next;
    }

    public Task Invoke(HttpContext context)
    {
        // https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Referrer-Policy
        // TODO Change the value depending of your needs
        context.Response.Headers.Add("referrer-policy", new StringValues("strict-origin-when-cross-origin"));

        // https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Content-Type-Options
        context.Response.Headers.Add("x-content-type-options", new StringValues("nosniff"));

        //// https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Frame-Options
        //context.Response.Headers.Add("x-frame-options", new StringValues("DENY"));        //Needed for SSRS Frame

        // https://security.stackexchange.com/questions/166024/does-the-x-permitted-cross-domain-policies-header-have-any-benefit-for-my-websit
        context.Response.Headers.Add("X-Permitted-Cross-Domain-Policies", new StringValues("none"));

        // https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-XSS-Protection
        context.Response.Headers.Add("x-xss-protection", new StringValues("1; mode=block"));

        // https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Expect-CT
        // You can use https://report-uri.com/ to get notified when a misissued certificate is detected
        context.Response.Headers.Add("Expect-CT", new StringValues("max-age=0, enforce, report-uri=\"https://example.report-uri.com/r/d/ct/enforce\""));

        // https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Feature-Policy
        // https://github.com/w3c/webappsec-feature-policy/blob/master/features.md
        // https://developers.google.com/web/updates/2018/06/feature-policy
        // TODO change the value of each rule and check the documentation to see if new features are available
        context.Response.Headers.Add("Feature-Policy", new StringValues(
            "accelerometer 'none';" +
            "ambient-light-sensor 'none';" +
            "autoplay 'none';" +
            "battery 'none';" +
            "camera 'none';" +
            "display-capture 'none';" +
            "document-domain 'none';" +
            "encrypted-media 'none';" +
            "execution-while-not-rendered 'none';" +
            "execution-while-out-of-viewport 'none';" +
            "gyroscope 'none';" +
            "magnetometer 'none';" +
            "microphone 'none';" +
            "midi 'none';" +
            "navigation-override 'none';" +
            "payment 'none';" +
            "picture-in-picture 'none';" +
            "publickey-credentials-get 'none';" +
            "sync-xhr 'none';" +
            "usb 'none';" +
            "wake-lock 'none';" +
            "xr-spatial-tracking 'none';"
            ));

        // https://developer.mozilla.org/en-US/docs/Web/HTTP/CSP
        // https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy
        // TODO change the value of each rule and check the documentation to see if new rules are available
        context.Response.Headers.Add("Content-Security-Policy", new StringValues(
            "base-uri 'none';" +
            "block-all-mixed-content;" +
            "child-src 'none';" +
            "connect-src 'none';" +
            "default-src 'none';" +
            "font-src 'none';" +
            "form-action 'none';" +
            //"frame-ancestors 'none';" +       //Needed for SSRS Frame
            //"frame-src 'none';" +
            "img-src 'none';" +
            "manifest-src 'none';" +
            "media-src 'none';" +
            "object-src 'none';" +
            "sandbox;" +
            "script-src 'none';" +
            "script-src-attr 'none';" +
            "script-src-elem 'none';" +
            "style-src 'none';" +
            "style-src-attr 'none';" +
            "style-src-elem 'none';" +
            "upgrade-insecure-requests;" +
            "worker-src 'none';"
            ));

        return _next(context);
    }
}
