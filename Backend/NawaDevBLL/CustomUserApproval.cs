﻿using Microsoft.Data.SqlClient;
using NawaDataBLL;
using NawaDataDAL;
using NawaDataDAL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static NawaDataBLL.UserBLL;

namespace NawaDevBLL
{
    public class CustomUserApproval
    {
        public static void SaveUserApprove(long approvalID, string currentUser)
        {
            NawaConnection connection = NawaDAO.CreateConnection();
            connection.BeginTransaction();

            string query = "SELECT * FROM ModuleApproval WHERE PK_ModuleApproval_ID = @UserApprovalID";
            SqlParameter[] prms = new SqlParameter[]
            {
                new SqlParameter("@UserApprovalID", SqlDbType.BigInt){ Value = approvalID }
            };
            DataRow drModuleApproval = NawaDAO.ExecuteRow(query, prms, connection);

            if (drModuleApproval != null)
            {
                ModuleApproval moduleApproval = NawaDAO.ConvertDataTable<ModuleApproval>(drModuleApproval);
                string userData = moduleApproval.ModuleField;
                string getCreatedBy = moduleApproval.CreatedBy;

                SaveMUser user = JsonConvert.DeserializeObject<SaveMUser>(userData);

                DataTable oldDT = new DataTable();

                string rowQuery = "select * from MUser where PK_MUser_ID = @PK_MUser_ID";
                SqlParameter[] param = new SqlParameter[]
                {
                    new SqlParameter("@PK_MUser_ID", SqlDbType.Int){ Value = user.PK_MUser_ID }
                };
                DataRow oldRow = NawaDAO.ExecuteRow(rowQuery, param, connection);

                try
                {
                    if (user.Mode == EActionType.Insert)
                    {
                        string queryCheck = "SELECT * " +
                                            "FROM MUser " +
                                            "WHERE UserID = @UserID";
                        SqlParameter[] prmsCheck = new SqlParameter[]
                        {
                            new SqlParameter("@UserID", SqlDbType.VarChar){Value = user.UserID}
                        };
                        DataRow dt = NawaDAO.ExecuteRow(queryCheck, prmsCheck, connection);

                        //string passwordSalt = Guid.NewGuid().ToString();
                        //string password = Common.Encrypt(user.UserPasword, passwordSalt);
                        DateTime dateTime = DateTime.Now;
                        query = "INSERT INTO MUSER " +
                                        "(UserID, UserName, FK_MRole_ID, FK_MGroupMenu_ID, UserPasword, UserPasswordSalt, UserEmailAddress, CreatedBy, LastUpdateBy, ApprovedBy, " +
                                        "CreatedDate, LastUpdateDate, ApprovedDate, IPAddress, Active, InUsed, IsDisabled, LastLogin, LastChangePassword, Alternateby) " +
                                        "VALUES " +
                                        "(@UserID, @UserName, @MRoleID, @MGroupMenuID, @UserPassword, @UserPasswordSalt, @UserEmailAddress, @CreatedBy, @LastUpdatedBy, @ApprovedBy, " +
                                        "@CreatedDate, @LastUpdateDate, @ApprovedDate, @IPAddress, @Active, @InUsed, @IsDisabled, @LastLogin, @LastChangePassword, @Alternateby) " +
                                        "SELECT @@identity; ";
                        prms = new SqlParameter[]
                        {
                            new SqlParameter("@UserID", SqlDbType.VarChar){Value = user.UserID},
                            new SqlParameter("@UserName", SqlDbType.VarChar){Value = user.UserName},
                            new SqlParameter("@MRoleID", SqlDbType.Int){Value = user.FK_MRole_ID},
                            new SqlParameter("@MGroupMenuID", SqlDbType.Int){Value = user.FK_MGroupMenu_ID},
                            new SqlParameter("@UserPassword", SqlDbType.VarChar){Value = user.UserPasword},
                            new SqlParameter("@UserPasswordSalt", SqlDbType.VarChar){Value = user.UserPasswordSalt},
                            new SqlParameter("@UserEmailAddress", SqlDbType.VarChar){Value = user.UserEmailAddress},
                            new SqlParameter("@CreatedBy", SqlDbType.VarChar){Value = currentUser},
                            new SqlParameter("@LastUpdatedBy", SqlDbType.VarChar){Value = currentUser},
                            new SqlParameter("@ApprovedBy", SqlDbType.VarChar){Value = currentUser},
                            new SqlParameter("@CreatedDate", SqlDbType.DateTime){Value = dateTime},
                            new SqlParameter("@LastUpdateDate", SqlDbType.DateTime){Value = dateTime},
                            new SqlParameter("@ApprovedDate", SqlDbType.DateTime){Value = dateTime},
                            new SqlParameter("@IPAddress", SqlDbType.VarChar){Value = ""},
                            new SqlParameter("@Active", SqlDbType.Bit){Value = true},
                            new SqlParameter("@InUsed", SqlDbType.Bit){Value = false},
                            new SqlParameter("@IsDisabled", SqlDbType.Bit){Value = false},
                            new SqlParameter("@LastLogin", SqlDbType.DateTime){Value = dateTime},
                            new SqlParameter("@LastChangePassword", SqlDbType.DateTime){Value = "1753-01-01 01:01:01"},
                            new SqlParameter("@Alternateby", SqlDbType.VarChar){Value = currentUser}
                        };

                        user.PK_MUser_ID = Convert.ToInt32(NawaDAO.ExecuteScalar(query, prms, connection));
                    }
                    else
                    {
                        if (user.Mode == EActionType.Update)
                        {
                            oldDT = oldRow.Table;

                            string queryCheck = "SELECT * " +
                                                "FROM MUser " +
                                                "WHERE UserID = @UserID AND PK_MUser_ID <> @PK_MUser_ID";
                            SqlParameter[] prmsCheck = new SqlParameter[]
                            {
                                new SqlParameter("@UserID", SqlDbType.VarChar){Value = user.UserID},
                                new SqlParameter("@PK_MUser_ID", SqlDbType.Int){Value = user.PK_MUser_ID}
                            };
                            DataRow dt = NawaDAO.ExecuteRow(queryCheck, prmsCheck, connection);

                            if (user.UserPasword == "")
                            {
                                DateTime dateTime = DateTime.Now;
                                query = "UPDATE MUser " +
                                               "SET UserID = @UserID, UserName = @UserName, FK_MRole_ID = @MRoleID, " +
                                               "FK_MGroupMenu_ID = @MGroupMenuID, UserEmailAddress = @UserEmailAddress, " +
                                               "LastUpdateBy = @LastUpdatedBy, LastUpdateDate = @LastUpdateDate " +
                                               "WHERE PK_MUser_ID = @MUserID";
                                prms = new SqlParameter[]
                                {
                                    new SqlParameter("@UserID", SqlDbType.VarChar){Value = user.UserID},
                                    new SqlParameter("@UserName", SqlDbType.VarChar){Value = user.UserName},
                                    new SqlParameter("@MRoleID", SqlDbType.Int){Value = user.FK_MRole_ID},
                                    new SqlParameter("@MGroupMenuID", SqlDbType.Int){Value = user.FK_MGroupMenu_ID},
                                    new SqlParameter("@UserEmailAddress", SqlDbType.VarChar){Value = user.UserEmailAddress},
                                    new SqlParameter("@LastUpdatedBy", SqlDbType.VarChar){Value = currentUser},
                                    new SqlParameter("@LastUpdateDate", SqlDbType.DateTime){Value = dateTime},
                                    new SqlParameter("@MUserID", SqlDbType.Int){Value = user.PK_MUser_ID}
                                };

                                int dr = NawaDAO.ExecuteNonQuery(query, prms, connection);
                            }
                            else
                            {
                                // already encrypted
                                //string passwordSalt = Guid.NewGuid().ToString();
                                //string password = NawaEncryption.Common.Encrypt(user.UserPasword, passwordSalt);
                                DateTime dateTime = DateTime.Now;
                                query = "UPDATE MUser " +
                                               "SET UserID = @UserID, UserName = @UserName, FK_MRole_ID = @MRoleID, " +
                                               "FK_MGroupMenu_ID = @MGroupMenuID, UserEmailAddress = @UserEmailAddress, " +
                                               "UserPasword = @UserPassword, UserPasswordSalt = @UserPasswordSalt, " +
                                               "LastUpdateBy = @LastUpdatedBy, LastUpdateDate = @LastUpdateDate, LastChangePassword = @LastChangePassword " +
                                               "WHERE PK_MUser_ID = @MUserID";
                                prms = new SqlParameter[]
                                {
                                    new SqlParameter("@UserID", SqlDbType.VarChar){Value = user.UserID},
                                    new SqlParameter("@UserName", SqlDbType.VarChar){Value = user.UserName},
                                    new SqlParameter("@MRoleID", SqlDbType.Int){Value = user.FK_MRole_ID},
                                    new SqlParameter("@MGroupMenuID", SqlDbType.Int){Value = user.FK_MGroupMenu_ID},
                                    new SqlParameter("@UserPassword", SqlDbType.VarChar){Value = user.UserPasword},
                                    new SqlParameter("@UserPasswordSalt", SqlDbType.VarChar){Value = user.UserPasswordSalt},
                                    new SqlParameter("@UserEmailAddress", SqlDbType.VarChar){Value = user.UserEmailAddress},
                                    new SqlParameter("@LastUpdatedBy", SqlDbType.VarChar){Value = currentUser},
                                    new SqlParameter("@LastUpdateDate", SqlDbType.DateTime){Value = dateTime},
                                    new SqlParameter("@LastChangePassword", SqlDbType.DateTime){Value = "1753-01-01 01:01:01"},
                                    new SqlParameter("@MUserID", SqlDbType.Int){Value = user.PK_MUser_ID}
                                };

                                int dr = NawaDAO.ExecuteNonQuery(query, prms, connection);
                            }
                        }
                        else if (user.Mode == EActionType.Activation)
                        {
                            oldDT = oldRow.Table;

                            query = "UPDATE MUser " +
                                           "SET Active = @Active, " +
                                           "LastUpdateBy = @LastUpdatedBy, LastUpdateDate = @LastUpdateDate " +
                                           "WHERE PK_MUser_ID = @MUserID";
                            prms = new SqlParameter[]
                            {
                                new SqlParameter("@Active", SqlDbType.Bit){Value = user.Active},
                                new SqlParameter("@LastUpdatedBy", SqlDbType.VarChar){Value = currentUser},
                                new SqlParameter("@LastUpdateDate", SqlDbType.DateTime){Value = DateTime.Now},
                                new SqlParameter("@MUserID", SqlDbType.Int){Value = user.PK_MUser_ID}
                            };

                            int dr = NawaDAO.ExecuteNonQuery(query, prms, connection);
                        }
                        else if (user.Mode == EActionType.Delete)
                        {
                            oldDT = oldRow.Table;

                            query = "DELETE MUser " +
                                           "WHERE PK_MUser_ID = @MUserID";
                            prms = new SqlParameter[]
                            {
                                new SqlParameter("@MUserID", SqlDbType.Int){Value = user.PK_MUser_ID}
                            };

                            int dr = NawaDAO.ExecuteNonQuery(query, prms, connection);
                        }
                    }

                    query = "DELETE ModuleApproval WHERE PK_ModuleApproval_ID = @UserApprovalID";
                    prms = new SqlParameter[] {
                        new SqlParameter("@UserApprovalID", SqlDbType.BigInt){ Value = approvalID }
                    };

                    NawaDAO.ExecuteNonQuery(query, prms, connection);

                    //string pwordSalt = Guid.NewGuid().ToString();
                    //string pword = NawaEncryption.Common.Encrypt(user.UserPasword, pwordSalt);
                    DateTime currentTime = DateTime.Now;

                    Boolean isActive = true;
                    DateTime createdDate = DateTime.Now;
                    DateTime lastLogin = user.LastLogin;
                    DateTime approvedDate = DateTime.Now;
                    string createdBy = getCreatedBy;
                    string alternateBy = user.Alternateby;
                    int userPK = user.PK_MUser_ID;

                    if (user.Mode != EActionType.Insert)
                    {
                        createdDate = Convert.ToDateTime(oldRow["CreatedDate"]);
                        isActive = user.Mode == EActionType.Activation ? !Convert.ToBoolean(oldRow["Active"]) : Convert.ToBoolean(oldRow["Active"]);
                        createdBy = Convert.ToString(oldRow["CreatedBy"]);
                        lastLogin = Convert.ToDateTime(oldRow["LastLogin"]);
                        alternateBy = Convert.ToString(oldRow["Alternateby"]);
                        userPK = Convert.ToInt32(oldRow["PK_MUser_ID"]);
                    }

                    Dictionary<string, object> dict = new Dictionary<string, object>();
                    dict.Add("PK_MUser_ID", userPK);
                    dict.Add("UserID", user.UserID);
                    dict.Add("UserName", user.UserName);
                    dict.Add("FK_MRole_ID", user.FK_MRole_ID);
                    dict.Add("FK_MGroupMenu_ID", user.FK_MGroupMenu_ID);
                    dict.Add("UserPasword", user.UserPasword);
                    dict.Add("UserPasswordSalt", user.UserPasswordSalt);
                    dict.Add("UserEmailAddress", user.UserEmailAddress);
                    dict.Add("CreatedBy", createdBy);
                    dict.Add("LastUpdateBy", currentUser);
                    dict.Add("ApprovedBy", currentUser);
                    dict.Add("CreatedDate", createdDate);
                    dict.Add("LastUpdateDate", currentTime);
                    dict.Add("ApprovedDate", currentTime);
                    dict.Add("IPAddress", user.IPAddress);
                    dict.Add("Active", isActive);
                    dict.Add("InUsed", user.InUsed);
                    dict.Add("IsDisabled", user.IsDisabled);
                    dict.Add("Alternateby", alternateBy);

                    List<Dictionary<string, object>> listDetails = new List<Dictionary<string, object>>();
                    listDetails.Add(dict);
                    DataTable newDT = NawaDAO.ConvertListDictionary(listDetails);

                    long headerID = AuditTrailBLL.InputAuditTrailHeader(connection, getCreatedBy, "User", user.Mode, EAuditTrailStatus.AffectedDatabase, moduleApproval.PK_ModuleApproval_ID, currentUser);
                    AuditTrailBLL.InputAuditTrailDetail(connection, headerID, user.Mode, newDT, oldDT);

                    #region Send Email
                    query = "SELECT * FROM EmailTemplate WHERE EmailTemplateName = @TemplateName";
                    prms = new SqlParameter[]
                    {
                        new SqlParameter("@TemplateName", SqlDbType.VarChar){ Value = "Email Template Approve Message" }
                    };
                    DataRow drTemplate = NawaDAO.ExecuteRow(query, prms);

                    if (drTemplate != null)
                    {
                        EmailTemplate template = NawaDAO.ConvertDataTable<EmailTemplate>(drTemplate);

                        prms = new SqlParameter[]
                        {
                            new SqlParameter("@PK_EmailTemplate_ID", SqlDbType.BigInt){ Value = template.PK_EmailTemplate_ID },
                            new SqlParameter("@recordID", SqlDbType.VarChar){ Value = Convert.ToString(headerID) },
                            new SqlParameter("@EmailIDReference", SqlDbType.VarChar){ Value = "0" },
                        };

                        NawaDAO.ExecuteNonQuery("usp_EMailScheduler_Create", prms, connection, CommandType.StoredProcedure);
                    }
                    #endregion

                    connection.Commit();
                    connection.Dispose();
                }
                catch
                {
                    connection.Rollback();
                    connection.Dispose();
                    throw;
                }
            }
        }
    }
}
