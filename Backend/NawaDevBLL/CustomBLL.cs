﻿using Microsoft.Data.SqlClient;
using NawaDataDAL;
using NawaDataDAL.Models;
using Newtonsoft.Json;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;

namespace NawaDataBLL
{
    public class CustomBLL
    {
        public int moduleID;
        public string moduleName;
        public List<ModuleField> moduleFields = new List<ModuleField>();

        private void SetModuleName()
        {
            //string query = "SELECT ModuleName FROM Module WHERE PK_Module_ID = @ModuleID";
            //SqlParameter[] prms = new SqlParameter[]
            //{
            //        new SqlParameter("@ModuleID", SqlDbType.Int){ Value = moduleID }
            //};
            //this.moduleName = Convert.ToString(NawaDAO.ExecuteScalar(query, prms));
            moduleName = "ModuleTestScript";
        }

        private void SetModuleFields()
        {
            //string query = "SELECT * FROM ModuleField WHERE FK_Module_ID = @ModuleID";
            //SqlParameter[] prms = new SqlParameter[]
            //{
            //            new SqlParameter("@ModuleID", SqlDbType.Int){ Value = moduleID }
            //};
            //DataTable dt = NawaDAO.ExecuteTable(query, prms);
            //moduleFields = NawaDAO.ConvertDataTable<ModuleField>(dt);

            moduleFields.Add(new ModuleField() { PK_ModuleField_ID = 1, FieldName = "PrimaryKey", FieldLabel = "Primary Key", Sequence = 1, Required = true, IsPrimaryKey = true, IsUnik = true, IsShowInForm = false, IsShowInView = false, FK_FieldType_ID = (int)EFieldType.IDENTITY, FK_ExtType_ID = (int)EControlType.DisplayField, IsUseRegexValidation = false, BCasCade = false });
            moduleFields.Add(new ModuleField() { PK_ModuleField_ID = 2, FieldName = "DateFieldRequiredUnique", FieldLabel = "Date Field (RequiredUnique)", Sequence = 2, Required = true, IsPrimaryKey = false, IsUnik = true, IsShowInForm = true, IsShowInView = true, FK_FieldType_ID = (int)EFieldType.DATETIME, SizeField = 0, FK_ExtType_ID = (int)EControlType.DateField, IsUseRegexValidation = false, BCasCade = false });
            moduleFields.Add(new ModuleField() { PK_ModuleField_ID = 3, FieldName = "TextFieldUniqeReuquired", FieldLabel = "TextFieldUniqeReuquired", Sequence = 3, Required = true, IsPrimaryKey = false, IsUnik = true, IsShowInForm = true, IsShowInView = true, FK_FieldType_ID = (int)EFieldType.VARCHAR, SizeField = 50, FK_ExtType_ID = (int)EControlType.TextField, IsUseRegexValidation = false, BCasCade = false });
            moduleFields.Add(new ModuleField() { PK_ModuleField_ID = 4, FieldName = "PasswordField", FieldLabel = "PasswordField", Sequence = 4, Required = true, IsPrimaryKey = false, IsUnik = false, IsShowInForm = true, IsShowInView = true, FK_FieldType_ID = (int)EFieldType.VARCHAR, SizeField = 15, FK_ExtType_ID = (int)EControlType.Password, IsUseRegexValidation = false, BCasCade = false });
            moduleFields.Add(new ModuleField() { PK_ModuleField_ID = 5, FieldName = "Dropdown", FieldLabel = "Dropdown", Sequence = 5, Required = false, IsPrimaryKey = false, IsUnik = false, IsShowInForm = true, IsShowInView = true, FK_FieldType_ID = (int)EFieldType.ReferenceTable, FK_ExtType_ID = (int)EControlType.DropDownField, TabelReferenceName = "ModuleProvince", TableReferenceFieldKey = "pk_province", TableReferenceFieldDisplayName = "nm_province", IsUseRegexValidation = false, BCasCade = false });
            moduleFields.Add(new ModuleField() { PK_ModuleField_ID = 6, FieldName = "DropdownRequiredUnique", FieldLabel = "Dropdown (RequiredUnique)", Sequence = 6, Required = false, IsPrimaryKey = false, IsUnik = false, IsShowInForm = true, IsShowInView = true, FK_FieldType_ID = (int)EFieldType.ReferenceTable, FK_ExtType_ID = (int)EControlType.DropDownField, TabelReferenceName = "ModuleCity", TableReferenceFieldKey = "pk_city", TableReferenceFieldDisplayName = "nm_city", IsUseRegexValidation = false, BCasCade = false });
        }

        public CustomBLL(int moduleID)
        {
            this.moduleID = moduleID;

            SetModuleName();

            SetModuleFields();
        }

        public CustomBLL(long moduleApprovalID)
        {
            string query = "SELECT PK_Module_ID FROM ModuleApproval JOIN Module ON ModuleApproval.ModuleName = Module.ModuleName WHERE PK_ModuleApproval_ID = @ModuleApprovalID";
            SqlParameter[] prms = new SqlParameter[]
            {
                        new SqlParameter("@ModuleApprovalID", SqlDbType.BigInt){ Value = moduleApprovalID }
            };
            this.moduleID = Convert.ToInt32(NawaDAO.ExecuteScalar(query, prms));

            SetModuleName();

            SetModuleFields();
        }

        public CustomBLL()
        {
            SetModuleFields();
        }

        public ModuleView GetModuleDetail(int moduleID, string id, long moduleApprovalID, string userID)
        {
            try
            {
                string query = "SELECT * FROM Module WHERE PK_Module_ID = @ModuleID";
                SqlParameter[] prms = new SqlParameter[]
                {
                    new SqlParameter("@ModuleID", SqlDbType.Int){ Value = moduleID }
                };
                DataRow dr = NawaDAO.ExecuteRow(query, prms);
                ModuleView moduleViewOutput = null;

                if (dr != null)
                {
                    moduleViewOutput = NawaDAO.ConvertDataTable<ModuleView>(dr);

                    {
                        //List<ModuleField> moduleFields = NawaDAO.ConvertDataTable<ModuleField>(dt);

                        int lastSequence = 0;
                        if (moduleFields != null)
                        {
                            moduleViewOutput.ModuleFields = moduleFields;
                            lastSequence = moduleFields.Count > 0 ? moduleFields[moduleFields.Count - 1].Sequence : 0;
                        }

                        #region show last update
                        string[] array = { "CreatedBy", "LastUpdateBy", "ApprovedBy", "CreatedDate", "LastUpdateDate", "ApprovedDate", "Active" };
                        int[] arraySysPar = { 43, 44, 45, 46, 47, 48, 50 };

                        for (int i = 0; i < array.Length; i++)
                        {
                            AppSystemParameter appSystemParameter = SystemParameterBLL.GetAppSystemParameter(arraySysPar[i]);
                            string isShow = appSystemParameter != null ? appSystemParameter.SettingValue : "0";

                            if (isShow == "1")
                            {
                                ModuleField moduleColums = new ModuleField();
                                moduleColums.PK_ModuleField_ID = 0;
                                moduleColums.FK_Module_ID = moduleID;
                                moduleColums.FieldName = array[i];
                                moduleColums.FieldLabel = array[i];
                                moduleColums.Sequence = lastSequence + i + 2;
                                moduleColums.Required = false;
                                moduleColums.IsPrimaryKey = false;
                                moduleColums.IsUnik = false;
                                moduleColums.IsShowInView = true;
                                moduleColums.IsShowInForm = false;
                                moduleColums.DefaultValue = "";
                                if (array[i] == "CreatedBy" || array[i] == "LastUpdateBy" || array[i] == "ApprovedBy")
                                {
                                    moduleColums.FK_FieldType_ID = (int)EFieldType.VARCHAR;
                                }
                                else if (array[i] == "Active")
                                {
                                    moduleColums.FK_FieldType_ID = (int)EFieldType.Boolean;
                                }
                                else
                                {
                                    moduleColums.FK_FieldType_ID = (int)EFieldType.DATETIME;
                                }
                                moduleColums.FK_ExtType_ID = (int)EControlType.DisplayField;
                                moduleColums.SizeField = 0;
                                moduleColums.TabelReferenceName = "";
                                moduleColums.TabelReferenceNameAlias = "";
                                moduleColums.TableReferenceFieldKey = "";
                                moduleColums.TableReferenceFieldDisplayName = "";
                                moduleColums.TableReferenceFilter = "";
                                moduleColums.TableReferenceAdditonalJoin = "";
                                moduleColums.IsUseRegexValidation = false;
                                moduleColums.FieldNameParent = "";
                                moduleColums.FilterCascade = "";
                                moduleColums.ListRegex = new List<ModuleFieldRegexView>();
                                moduleColums.BCasCade = false;
                                moduleFields.Add(moduleColums);
                            }
                        }
                        #endregion 
                    }

                    prms = new SqlParameter[]
                    {
                        new SqlParameter("@pkmoduleid", SqlDbType.Int){ Value = moduleID }
                    };
                    int pkWorkflowID = Convert.ToInt32(NawaDAO.ExecuteScalar("IsHaveWorkflow", prms, null, CommandType.StoredProcedure));
                    moduleViewOutput.UseWorkflow = pkWorkflowID > 0;

                    if (String.IsNullOrEmpty(id) && moduleApprovalID > 0)
                    {
                        query = "SELECT ModuleKey FROM ModuleApproval WHERE PK_ModuleApproval_ID = " + moduleApprovalID;
                        id = Convert.ToString(NawaDAO.ExecuteScalar(query));
                    }

                    prms = new SqlParameter[]
                    {
                        new SqlParameter("@pkmoduleid", SqlDbType.Int){ Value = moduleID },
                        new SqlParameter("@pkunikid", SqlDbType.VarChar){ Value = id },
                        new SqlParameter("@userId", SqlDbType.VarChar){ Value = userID },
                        new SqlParameter("@action", SqlDbType.Int){ Value = EActionType.View }
                    };
                    pkWorkflowID = Convert.ToInt32(NawaDAO.ExecuteScalar("usp_ValidateMWorkflow", prms, null, CommandType.StoredProcedure));
                    moduleViewOutput.UserHasWorkflow = pkWorkflowID > 0;
                    prms = new SqlParameter[]
                    {
                        new SqlParameter("@PkMOduleid", SqlDbType.Int) { Value = moduleID },
                        new SqlParameter("@pkunikid", SqlDbType.VarChar) { Value = id }
                    };
                    pkWorkflowID = Convert.ToInt32(NawaDAO.ExecuteScalar("IsHaveWorkflowApproval", prms, null, CommandType.StoredProcedure));
                    moduleViewOutput.HaveActiveWorkflow = pkWorkflowID > 0;
                    moduleViewOutput.ActionList = new List<ActionView>();
                    List<MFieldType> ListFieldType = GetFieldType();

                    if (moduleViewOutput.ModuleFields != null)
                    {
                        foreach (ModuleField moduleField in moduleViewOutput.ModuleFields)
                        {
                            #region ListRegex
                            query = "SELECT * FROM ModuleFieldRegex WHERE FK_ModuleField_ID = " + moduleField.PK_ModuleField_ID;
                            DataTable dataRegex = NawaDAO.ExecuteTable(query);
                            List<ModuleFieldRegexView> moduleFieldRegexs = NawaDAO.ConvertDataTable<ModuleFieldRegexView>(dataRegex);
                            moduleField.ListRegex = moduleFieldRegexs;
                            #endregion
                        }
                    }

                    #region MGroupMenuAccess
                    query = "SELECT FK_MGroupMenu_ID FROM MUser WHERE UserID = @UserID";
                    prms = new SqlParameter[]
                    {
                        new SqlParameter("@UserID", SqlDbType.VarChar){ Value = userID }
                    };
                    int mGroupMenuID = Convert.ToInt32(NawaDAO.ExecuteScalar(query, prms));
                    query = "SELECT * FROM MGroupMenuAccess WHERE FK_GroupMenu_ID = @FK_GroupMenu_ID AND FK_Module_ID = @FK_Module_ID";
                    prms = new SqlParameter[]
                    {
                        new SqlParameter("@FK_GroupMenu_ID", SqlDbType.Int){ Value = mGroupMenuID },
                        new SqlParameter("@FK_Module_ID", SqlDbType.Int){ Value = moduleID }
                    };
                    DataRow drAccess = NawaDAO.ExecuteRow(query, prms);
                    MGroupMenuAccess mGMenuAccess = new MGroupMenuAccess();

                    if (drAccess != null)
                    {
                        mGMenuAccess = NawaDAO.ConvertDataTable<MGroupMenuAccess>(drAccess);
                    }
                    #endregion

                    if (moduleViewOutput.IsSupportAdd && mGMenuAccess.bAdd)
                    {
                        moduleViewOutput.ActionList.Add(new ActionView()
                        {
                            text = "Add",
                            url = moduleViewOutput.UrlAdd,
                            type = (int)EActionType.Insert
                        });
                    }

                    if (moduleViewOutput.IsSupportEdit && mGMenuAccess.bEdit)
                    {
                        moduleViewOutput.ActionList.Add(new ActionView()
                        {
                            text = "Edit",
                            url = moduleViewOutput.UrlEdit,
                            type = (int)EActionType.Update
                        });
                    }

                    if (moduleViewOutput.IsSupportDelete && mGMenuAccess.bDelete)
                    {
                        moduleViewOutput.ActionList.Add(new ActionView()
                        {
                            text = "Delete",
                            url = moduleViewOutput.UrlDelete,
                            type = (int)EActionType.Delete
                        });
                    }

                    if (moduleViewOutput.IsSupportDetail && mGMenuAccess.bDetail)
                    {
                        moduleViewOutput.ActionList.Add(new ActionView()
                        {
                            text = "Details",
                            url = moduleViewOutput.UrlDetail,
                            type = (int)EActionType.Detail
                        });
                    }

                    if (moduleViewOutput.IsSupportActivation && mGMenuAccess.bActivation)
                    {
                        moduleViewOutput.ActionList.Add(new ActionView()
                        {
                            text = "Activation",
                            url = moduleViewOutput.UrlActivation,
                            type = (int)EActionType.Activation
                        });
                    }

                    #region Custom Support
                    query = "SELECT * FROM ModuleCustomSupport WHERE FK_Module_ID = @ModuleID";
                    prms = new SqlParameter[]
                    {
                        new SqlParameter("@ModuleID", SqlDbType.Int){ Value = moduleID }
                    };
                    DataTable dtCustom = NawaDAO.ExecuteTable(query, prms);

                    if (dtCustom != null)
                    {
                        List<ModuleCustomSupport> listCustomSupport = NawaDAO.ConvertDataTable<ModuleCustomSupport>(dtCustom);

                        foreach (ModuleCustomSupport supportData in listCustomSupport)
                        {
                            moduleViewOutput.ActionList.Add(new ActionView()
                            {
                                text = supportData.SupportName,
                                url = supportData.Target,
                                type = supportData.ActionType,
                                parameter = supportData.Parameter,
                                parameterValue = supportData.ParameterValue,
                            });
                        }
                    }
                    #endregion

                    #region Checks for ModuleDetail
                    moduleViewOutput.ListModuleDetail = GetModuleDetail(moduleID);
                    #endregion

                    #region Checks for ModuleDetailField
                    if (moduleViewOutput.ListModuleDetail != null)
                    {
                        foreach (ModuleDetail dataModuleDetail in moduleViewOutput.ListModuleDetail)
                        {
                            dataModuleDetail.ListModuleDetailField = GetModuleDetailField(dataModuleDetail.PK_ModuleDetail_ID);
                        }
                    }
                    #endregion
                }

                return moduleViewOutput;
            }
            catch
            {
                throw;
            }
        }

        public List<MFieldType> GetFieldType()
        {
            try
            {
                string queryStr = @" SELECT PK_FieldType_ID, FieldTypeCaption FROM MFieldType ";
                DataTable dtFieldType = NawaDAO.ExecuteTable(queryStr);
                List<MFieldType> fieldTypes = NawaDAO.ConvertDataTable<MFieldType>(dtFieldType);
                return fieldTypes;
            }
            catch
            {
                throw;
            }
        }

        #region Get Module Detail
        public List<ModuleDetail> GetModuleDetail(int moduleID)
        {
            try
            {
                string queryStr = @"SELECT * FROM ModuleDetail WHERE FK_Module_ID = @ModuleID AND Active = 1";
                SqlParameter[] queryParams = new SqlParameter[]
                {
                    new SqlParameter("@ModuleID", SqlDbType.Int){ Value = moduleID }
                };
                DataTable dtModuleDetail = NawaDAO.ExecuteTable(queryStr, queryParams);
                List<ModuleDetail> listModuleDetail = NawaDAO.ConvertDataTable<ModuleDetail>(dtModuleDetail);

                return listModuleDetail;
            }
            catch
            {
                throw;
            }
        }

        public List<ModuleDetailField> GetModuleDetailField(int moduleDetailID)
        {
            try
            {
                string queryStr = @" 
                SELECT 
	                mf.PK_ModuleDetailField_ID, mf.FK_ModuleDetail_ID, mf.FieldName, mf.FieldLabel, mf.[Sequence], mf.[Required], mf.IsPrimaryKey, mf.IsUnique, 
	                mf.IsShowInView, mf.IsShowInForm, mf.DefaultValue, mf.FK_FieldType_ID, ft.FieldTypeCaption, mf.SizeField, mf.FK_ExtType_ID, ext.ExtTypeName,
	                mf.TabelReferenceName, mf.TabelReferenceNameAlias, mf.TableReferenceFieldKey, mf.TableReferenceFieldDisplayName, mf.TableReferenceFilter, 
	                mf.IsUseRegexValidation, mf.TableReferenceAdditonalJoin, mf.BCasCade, mf.FieldNameParent, mf.FilterCascade
                FROM ModuleDetailField mf
                LEFT JOIN MFieldType ft ON ft.PK_FieldType_ID = mf.FK_FieldType_ID
                LEFT JOIN MExtType ext ON ext.PK_ExtType_ID = mf.FK_ExtType_ID
                WHERE mf.FK_ModuleDetail_ID = @ModuleDetailID ";
                SqlParameter[] queryParams = new SqlParameter[]
                {
                    new SqlParameter("@ModuleDetailID", SqlDbType.Int){ Value = moduleDetailID }
                };
                DataTable dtModuleDetailID = NawaDAO.ExecuteTable(queryStr, queryParams);
                List<ModuleDetailField> listModuleDetailField = NawaDAO.ConvertDataTable<ModuleDetailField>(dtModuleDetailID);

                return listModuleDetailField;
            }
            catch
            {
                throw;
            }
        }
        #endregion

        public ModuleView GetValidModuleDetail(int ModuleID)
        {
            try
            {
                string query = "SELECT * FROM Module WHERE PK_Module_ID = @ModuleID";
                SqlParameter[] prms = new SqlParameter[] {
                    new SqlParameter("@ModuleID", SqlDbType.Int){ Value = ModuleID }
                };

                DataRow dr = NawaDAO.ExecuteRow(query, prms);

                if (dr != null)
                {
                    ModuleView moduleDetail = NawaDAO.ConvertDataTable<ModuleView>(dr);

                    {
                        //List<ModuleField> moduleFields = NawaDAO.ConvertDataTable<ModuleField>(dt);
                        if (moduleFields != null)
                        {
                            moduleFields.Where(item => item.IsPrimaryKey).FirstOrDefault().IsPrimaryKey = false;
                            moduleFields.Insert(0, new ModuleField()
                            {
                                FieldName = "nawa_Action",
                                FieldLabel = "Action",
                                IsPrimaryKey = false,
                                IsShowInView = true,
                                FK_FieldType_ID = 9,
                                FK_ExtType_ID = 5,
                            });
                            moduleFields.Insert(0, new ModuleField()
                            {
                                FieldName = "nawa_recordnumber",
                                FieldLabel = "Line.",
                                IsPrimaryKey = true,
                                IsShowInView = true,
                                FK_FieldType_ID = 2,
                                FK_ExtType_ID = 4,
                            });
                            moduleFields.Add(new ModuleField()
                            {
                                FieldName = "Active",
                                FieldLabel = "Active",
                                IsPrimaryKey = false,
                                IsShowInView = true,
                                FK_FieldType_ID = 13,
                                FK_ExtType_ID = 6,
                            });

                            moduleDetail.ModuleFields = moduleFields;
                        }
                    }

                    return moduleDetail;
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public ModuleView GetInvalidModuleDetail(int ModuleID)
        {
            try
            {
                string query = "SELECT * FROM Module WHERE PK_Module_ID = @ModuleID";
                SqlParameter[] prms = new SqlParameter[] {
                    new SqlParameter("@ModuleID", SqlDbType.Int){ Value = ModuleID }
                };

                DataRow dr = NawaDAO.ExecuteRow(query, prms);

                if (dr != null)
                {
                    ModuleView moduleDetail = NawaDAO.ConvertDataTable<ModuleView>(dr);

                    {
                        //List<ModuleField> moduleFields = NawaDAO.ConvertDataTable<ModuleField>(dt);
                        if (moduleFields != null)
                        {
                            moduleFields.Where(item => item.IsPrimaryKey).FirstOrDefault().IsPrimaryKey = false;
                            moduleFields.Insert(0, new ModuleField()
                            {
                                FieldName = "KeteranganError",
                                FieldLabel = "Keterangan",
                                IsPrimaryKey = false,
                                IsShowInView = true,
                                FK_FieldType_ID = 9,
                                FK_ExtType_ID = 5,
                            });
                            moduleFields.Insert(0, new ModuleField()
                            {
                                FieldName = "nawa_Action",
                                FieldLabel = "Action",
                                IsPrimaryKey = false,
                                IsShowInView = true,
                                FK_FieldType_ID = 9,
                                FK_ExtType_ID = 5,
                            });
                            moduleFields.Insert(0, new ModuleField()
                            {
                                FieldName = "nawa_recordnumber",
                                FieldLabel = "Line.",
                                IsPrimaryKey = true,
                                IsShowInView = true,
                                FK_FieldType_ID = 2,
                                FK_ExtType_ID = 4,
                            });
                            moduleFields.Add(new ModuleField()
                            {
                                FieldName = "Active",
                                FieldLabel = "Active",
                                IsPrimaryKey = false,
                                IsShowInView = true,
                                FK_FieldType_ID = 13,
                                FK_ExtType_ID = 6,
                            });

                            moduleDetail.ModuleFields = moduleFields;
                        }
                    }

                    return moduleDetail;
                }

                return null;
            }
            catch
            {
                throw;
            }
        }


        public List<DynamicDictionary> GetValidModuleData(int ModuleID, string UserID, int page, string orderBy, string order, string search, string filter, out int rowCount)
        {
            try
            {
                string query = "SELECT * FROM Module WHERE PK_Module_ID = @ModuleID";
                SqlParameter[] prms = new SqlParameter[] {
                    new SqlParameter("@ModuleID", SqlDbType.Int){ Value = ModuleID }
                };

                DataRow dr = NawaDAO.ExecuteRow(query, prms);

                if (dr != null)
                {
                    //string moduleName = Convert.ToString(dr["ModuleName"]);
                    bool splitUpload = Convert.ToBoolean(SystemParameterBLL.GetSystemParameterValue(53).SettingValue);

                    string moduleNameUpload = moduleName;

                    if (splitUpload)
                    {
                        moduleNameUpload += "_Upload_data_" + CommonBLL.StripTableName(UserID);
                    }
                    else
                    {
                        moduleNameUpload += "_Upload";
                    }

                    //string searchQuery = " WHERE KeteranganError = '' AND nawa_userid = '" + UserID + "' AND (1 = 1";
                    string searchQuery = "";
                    if (!String.IsNullOrEmpty(search))
                    {
                        foreach (ModuleField moduleField in moduleFields)
                        {
                            if (String.IsNullOrEmpty(searchQuery))
                            {
                                searchQuery = " WHERE ( CAST(" + Convert.ToString(moduleField.FieldName) + " AS VARCHAR(MAX)) LIKE '%" + search.Replace("'", "''") + "%' ";
                            }
                            else
                            {
                                searchQuery = searchQuery + " OR CAST(" + Convert.ToString(moduleField.FieldName) + " AS VARCHAR(MAX)) LIKE '%" + search.Replace("'", "''") + "%' ";
                            }
                        }
                    }
                    if (!string.IsNullOrEmpty(filter) && string.IsNullOrEmpty(search))
                    {
                        searchQuery = " WHERE (" + filter;
                    }

                    if (!String.IsNullOrEmpty(searchQuery))
                    {
                        searchQuery = searchQuery + " ) AND KeteranganError = '' AND nawa_userid = '" + UserID + "'";
                    }
                    else
                    {
                        searchQuery = " WHERE KeteranganError = '' AND nawa_userid = '" + UserID + "' ";
                    }

                    query = "SELECT COUNT(13) AS [RowCount] FROM " + moduleNameUpload + searchQuery;

                    rowCount = Convert.ToInt32(NawaDAO.ExecuteScalar(query));

                    int pageSize = Convert.ToInt32(SystemParameterBLL.GetAppSystemParameter(7).SettingValue);

                    query = "EXEC usp_PagingNew @Query, @OrderBy, @PageIndex, @PageSize";
                    prms = new SqlParameter[] {
                        new SqlParameter("@Query", SqlDbType.VarChar){ Value = "SELECT * FROM " + moduleNameUpload + searchQuery },
                        new SqlParameter("@OrderBy", SqlDbType.VarChar){ Value = (!String.IsNullOrEmpty(orderBy) ? orderBy + " " + order : "") },
                        new SqlParameter("@PageIndex", SqlDbType.Int){ Value = page },
                        new SqlParameter("@PageSize", SqlDbType.Int){ Value = pageSize }
                    };

                    DataTable dt = NawaDAO.ExecuteTable(query, prms);

                    List<DynamicDictionary> res = dt.AsEnumerable().Select(row => new DynamicDictionary(row)).ToList();
                    return res;
                }

                rowCount = 0;

                return null;
            }
            catch
            {
                throw;
            }
        }

        public List<DynamicDictionary> GetInvalidModuleData(int moduleID, string userID, int page, string orderBy, string order, string search, string filter, out int rowCount)
        {
            try
            {
                string query = "SELECT * FROM Module WHERE PK_Module_ID = @ModuleID";
                SqlParameter[] prms = new SqlParameter[]
                {
                    new SqlParameter("@ModuleID", SqlDbType.Int){ Value = moduleID }
                };
                DataRow dr = NawaDAO.ExecuteRow(query, prms);

                if (dr != null)
                {
                    //string moduleName = Convert.ToString(dr["ModuleName"]);
                    bool splitUpload = Convert.ToBoolean(SystemParameterBLL.GetSystemParameterValue(53).SettingValue);
                    string moduleNameUpload = moduleName;

                    if (splitUpload)
                    {
                        moduleNameUpload += "_Upload_data_" + CommonBLL.StripTableName(userID);
                    }
                    else
                    {
                        moduleNameUpload += "_Upload";
                    }

                    string searchQuery = InvalidModuleDataSearchQuery(moduleID, userID, search, filter);
                    query = "SELECT COUNT(13) AS [RowCount] FROM " + moduleNameUpload + searchQuery;
                    rowCount = Convert.ToInt32(NawaDAO.ExecuteScalar(query));
                    int pageSize = Convert.ToInt32(SystemParameterBLL.GetAppSystemParameter(7).SettingValue);
                    query = "EXEC usp_PagingNew @Query, @OrderBy, @PageIndex, @PageSize";
                    prms = new SqlParameter[]
                    {
                        new SqlParameter("@Query", SqlDbType.VarChar){ Value = "SELECT * FROM " + moduleNameUpload + searchQuery },
                        new SqlParameter("@OrderBy", SqlDbType.VarChar){ Value = (!String.IsNullOrEmpty(orderBy) ? orderBy + " " + order : "") },
                        new SqlParameter("@PageIndex", SqlDbType.Int){ Value = page },
                        new SqlParameter("@PageSize", SqlDbType.Int){ Value = pageSize }
                    };
                    DataTable dt = NawaDAO.ExecuteTable(query, prms);
                    List<DynamicDictionary> res = dt.AsEnumerable().Select(row => new DynamicDictionary(row)).ToList();

                    return res;
                }

                rowCount = 0;

                return null;
            }
            catch
            {
                throw;
            }
        }

        public string InvalidModuleDataSearchQuery(int moduleID, string userID, string search, string filter)
        {
            try
            {
                string searchQuery = "";

                if (!string.IsNullOrEmpty(search))
                {
                    foreach (ModuleField moduleField in moduleFields)
                    {
                        if (string.IsNullOrEmpty(searchQuery))
                        {
                            searchQuery = " WHERE ( CAST(" + Convert.ToString(moduleField.FieldName) + " AS VARCHAR(MAX)) LIKE '%" + search.Replace("'", "''") + "%' ";
                        }
                        else
                        {
                            searchQuery = searchQuery + " OR CAST(" + Convert.ToString(moduleField.FieldName) + " AS VARCHAR(MAX)) LIKE '%" + search.Replace("'", "''") + "%' ";
                        }
                    }
                }

                if (!string.IsNullOrEmpty(filter) && string.IsNullOrEmpty(search))
                {
                    searchQuery = " WHERE (" + filter;
                }

                if (!string.IsNullOrEmpty(searchQuery))
                {
                    searchQuery = searchQuery + " OR CAST(nawa_Action AS VARCHAR(MAX)) LIKE '%" + search.Replace("'", "''") + "%' OR CAST(KeteranganError AS VARCHAR(MAX)) LIKE '%" + search.Replace("'", "''") + "%' ) AND KeteranganError <> '' AND nawa_userid = '" + userID + "'";
                }
                else
                {
                    searchQuery = " WHERE KeteranganError <> '' AND nawa_userid = '" + userID + "'";
                }

                return searchQuery;
            }
            catch
            {
                throw;
            }
        }


        public CDataWithValidation GetModuleData(int moduleID, int id)
        {
            try
            {
                string query = "SELECT * FROM Module WHERE PK_Module_ID = @ModuleID";
                SqlParameter[] prms = new SqlParameter[]
                {
                    new SqlParameter("@ModuleID", SqlDbType.Int){ Value = moduleID }
                };
                DataRow dr = NawaDAO.ExecuteRow(query, prms);
                CDataWithValidation result = new CDataWithValidation() { validation = new List<KeyValuePair<long, string>>() };
                DynamicDictionary res = null;

                if (dr != null)
                {
                    //string moduleName = Convert.ToString(dr["ModuleName"]);

                    //string primaryField = Convert.ToString(NawaDAO.ExecuteScalar(query, prms));
                    string primaryField = moduleFields.Where(item => item.IsPrimaryKey == true).Select(item => item.FieldName).FirstOrDefault();

                    if (!string.IsNullOrEmpty(primaryField))
                    {
                        query = "SELECT * FROM " + moduleName + " WHERE " + primaryField + " = @ID";
                        prms = new SqlParameter[]
                        {
                            new SqlParameter("@ID", SqlDbType.Int){ Value = id }
                        };
                        DataRow row = NawaDAO.ExecuteRow(query, prms);

                        if (row != null)
                        {
                            res = new DynamicDictionary(row);

                            result.Dictionary = res.Dictionary;
                        }
                    }

                    query = "SELECT NamaField, ValidationMessage FROM ValidationReport_Simple WHERE ModuleID = @ModuleID AND RecordID = @RecordID";
                    prms = new SqlParameter[]
                    {
                        new SqlParameter("@ModuleID", SqlDbType.Int){ Value = moduleID },
                        new SqlParameter("@RecordID", SqlDbType.Int){ Value = id }
                    };
                    DataTable dt = NawaDAO.ExecuteTable(query, prms);
                    foreach (DataRow row in dt.Rows)
                    {
                        result.validation.Add(new KeyValuePair<long, string>(moduleFields.Where(item => item.FieldName == Convert.ToString(row["NamaField"])).Select(item => item.PK_ModuleField_ID).FirstOrDefault(), Convert.ToString(row["ValidationMessage"])));
                    }
                }

                return result;
            }
            catch
            {
                throw;
            }
        }

        public List<DynamicDictionary> GetModuleData(int moduleID, int page, string orderBy, string order, string search, string filter, bool isAdv, out int rowCount, string roleID, string userID)
        {
            try
            {
                // find module
                string query = "SELECT * FROM Module WHERE PK_Module_ID = @ModuleID";
                SqlParameter[] prms = new SqlParameter[]
                {
                    new SqlParameter("@ModuleID", SqlDbType.Int){ Value = moduleID }
                };
                DataRow dr = NawaDAO.ExecuteRow(query, prms);
                List<DynamicDictionary> res = null;
                rowCount = 0;

                if (dr != null)
                {
                    //string moduleName = Convert.ToString(dr["ModuleName"]);
                    string moduleJoin = "";
                    string moduleColumns = "";
                    string searchQuery = "";
                    string outerModuleColumns = "";
                    string advQuery = "";

                    // find module field
                    //query = "SELECT * FROM ModuleField WHERE FK_Module_ID = @ModuleID AND (IsShowInView = 1 OR IsPrimaryKey = 1)";
                    //prms = new SqlParameter[]
                    //{
                    //    new SqlParameter("@ModuleID", SqlDbType.Int){ Value = moduleID }
                    //};
                    //DataTable dtFields = NawaDAO.ExecuteTable(query, prms);

                    if (moduleFields != null && moduleFields.Count(item => item.IsShowInView || item.IsPrimaryKey) > 0)
                    {
                        SystemParameter sysParamFormatDate = SystemParameterBLL.GetSystemParameterValue(6);
                        //string dateFormat = sysParamFormatDate != null ? sysParamFormatDate.SettingValue : "yyyy-MM-dd";
                        string dateFormat = "dd MMMM yyyy HH:mm:ss";

                        #region Fill Data Query
                        List<string> referenceAliasHistory = new List<string>();

                        foreach (ModuleField moduleField in moduleFields.Where(item => item.IsShowInView || item.IsPrimaryKey).ToList())
                        {
                            if (!String.IsNullOrEmpty(moduleColumns))
                            {
                                moduleColumns += ", ";
                            }

                            if (!String.IsNullOrEmpty(outerModuleColumns))
                            {
                                outerModuleColumns += ", ";
                            }
                            outerModuleColumns += moduleField.FieldName;

                            int extType = Convert.ToInt32(moduleField.FK_ExtType_ID);
                            int fieldType = Convert.ToInt32(moduleField.FK_FieldType_ID);

                            if (extType == (int)EControlType.PopUpMultiCheckBox)
                            {
                                // get delimiter and fill query                 
                                string delim = SystemParameterBLL.GetSystemParameterGroup(39).SettingValue;
                                string multiCheckBoxLogic = "(" + "\n" +
                                                "\t" + "STUFF(" + "\n" +
                                                "\t\t" + $"(SELECT '{delim}(' + A.val + ') - ' + B.{moduleField.TableReferenceFieldDisplayName}" + "\n" +
                                                "\t\t" + $"FROM dbo.Split({moduleName}.{moduleField.FieldName}, '{delim}') AS A" + "\n" +
                                                "\t\t\t" + $"LEFT JOIN {moduleField.TabelReferenceName} AS B" + "\n" +
                                                "\t\t\t" + $"ON A.val = B.{moduleField.TableReferenceFieldKey}" + "\n" +
                                                "\t\t" + "FOR XML PATH (''))" + "\n" +
                                                "\t" + ", 1, 1, '')" + "\n" +
                                                ")";
                                moduleColumns += multiCheckBoxLogic + " AS " + moduleField.FieldName;

                                if (!string.IsNullOrEmpty(search))
                                {
                                    if (String.IsNullOrEmpty(searchQuery))
                                    {
                                        searchQuery = " WHERE ( " + multiCheckBoxLogic + " LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                                    }
                                    else
                                    {
                                        searchQuery = searchQuery + " OR " + multiCheckBoxLogic + " LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                                    }
                                }
                            }
                            else if (fieldType == (int)EFieldType.ReferenceTable && extType != (int)EControlType.PopUpMultiCheckBox)
                            {
                                // table reference alias handler, automaticlly generate new alias if found same name/alias
                                string referenceAlias = string.IsNullOrWhiteSpace(Convert.ToString(moduleField.TabelReferenceNameAlias)) ? Convert.ToString(moduleField.TabelReferenceName) : Convert.ToString(moduleField.TabelReferenceNameAlias);
                                bool duplicatedAlias = referenceAliasHistory.Any(pastAlias => pastAlias == referenceAlias);
                                if (duplicatedAlias == true)
                                {
                                    referenceAlias = referenceAlias + "_" + referenceAliasHistory.Count;
                                }

                                moduleJoin += " LEFT JOIN " + moduleField.TabelReferenceName + " " + referenceAlias + " ON " + moduleName + "." + moduleField.FieldName + " = " + referenceAlias + "." + moduleField.TableReferenceFieldKey + (Convert.ToBoolean(moduleField.BCasCade) ? " AND " + Convert.ToString(moduleField.FilterCascade).Replace("@Parent", moduleName + "." + Convert.ToString(moduleField.FieldNameParent)) : "");
                                moduleColumns += "'(' + CAST(" + moduleName + "." + moduleField.FieldName + " AS VARCHAR(MAX)) + ') - ' + CAST(" + referenceAlias + "." + moduleField.TableReferenceFieldDisplayName + " AS VARCHAR(MAX)) AS " + moduleField.FieldName;
                                referenceAliasHistory.Add(referenceAlias);

                                if (!string.IsNullOrEmpty(search))
                                {
                                    if (String.IsNullOrEmpty(searchQuery))
                                    {
                                        searchQuery = " WHERE ( ('(' + CAST(" + moduleName + "." + moduleField.FieldName + " AS VARCHAR(MAX)) + ') - ' + CAST(" + referenceAlias + "." + moduleField.TableReferenceFieldDisplayName + " AS VARCHAR(MAX))) LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                                    }
                                    else
                                    {
                                        searchQuery = searchQuery + " OR ('(' + CAST(" + moduleName + "." + moduleField.FieldName + " AS VARCHAR(MAX)) + ') - ' + CAST(" + referenceAlias + "." + moduleField.TableReferenceFieldDisplayName + " AS VARCHAR(MAX))) LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                                    }
                                }
                            }
                            else if (fieldType == (int)EFieldType.VarBinary && (extType == (int)EControlType.DisplayField || extType == (int)EControlType.FileUpload))
                            {
                                moduleColumns += moduleName + "." + moduleField.FieldName + ", ";
                                moduleColumns += moduleName + "." + moduleField.FieldName + "Name";

                                if (!string.IsNullOrEmpty(search))
                                {
                                    if (String.IsNullOrEmpty(searchQuery))
                                    {
                                        searchQuery = " WHERE ( CAST(" + moduleName + "." + Convert.ToString(moduleField.FieldName) + "Name AS VARCHAR(MAX)) LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                                    }
                                    else
                                    {
                                        searchQuery = searchQuery + " OR CAST(" + moduleName + "." + Convert.ToString(moduleField.FieldName) + "Name AS VARCHAR(MAX)) LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                                    }
                                }
                            }
                            else if (fieldType == (int)EFieldType.DATETIME)
                            {
                                moduleColumns += moduleName + "." + moduleField.FieldName;

                                if (!string.IsNullOrEmpty(search))
                                {
                                    if (String.IsNullOrEmpty(searchQuery))
                                    {
                                        searchQuery = " WHERE ( FORMAT(" + moduleName + "." + Convert.ToString(moduleField.FieldName) + ", '" + dateFormat + "') LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                                    }
                                    else
                                    {
                                        searchQuery = searchQuery + " OR FORMAT(" + moduleName + "." + Convert.ToString(moduleField.FieldName) + ", '" + dateFormat + "') LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                                    }
                                }
                            }
                            else if (fieldType == (int)EFieldType.FLOAT || fieldType == (int)EFieldType.NUMERICDECIMAL ||
                                fieldType == (int)EFieldType.REAL || fieldType == (int)EFieldType.MONEY)
                            {
                                moduleColumns += moduleName + "." + moduleField.FieldName;
                                if (!string.IsNullOrEmpty(search))
                                {
                                    if (String.IsNullOrEmpty(searchQuery))
                                    {
                                        searchQuery = " WHERE ( FORMAT(" + moduleName + "." + Convert.ToString(moduleField.FieldName) + ",'#.###############') LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                                    }
                                    else
                                    {
                                        searchQuery = searchQuery + " OR FORMAT(" + moduleName + "." + Convert.ToString(moduleField.FieldName) + ",'#.###############') LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                                    }
                                }
                            }
                            else
                            {
                                moduleColumns += moduleName + "." + moduleField.FieldName;

                                if (!string.IsNullOrEmpty(search))
                                {
                                    if (String.IsNullOrEmpty(searchQuery))
                                    {
                                        searchQuery = " WHERE ( CAST(" + moduleName + "." + Convert.ToString(moduleField.FieldName) + " AS VARCHAR(MAX)) LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                                    }
                                    else
                                    {
                                        searchQuery = searchQuery + " OR CAST(" + moduleName + "." + Convert.ToString(moduleField.FieldName) + " AS VARCHAR(MAX)) LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                                    }
                                }
                            }
                        }
                        #endregion

                        // Advance Filter
                        if (!string.IsNullOrEmpty(filter) && string.IsNullOrEmpty(search))
                        {
                            if (isAdv)
                            {
                                advQuery = " WHERE (" + filter;
                            }
                            else
                            {
                                string[] filterArr = filter.Split(' ');
                                string restOfFilter = string.Join(" ", filterArr.Skip(1));

                                List<ModuleField> filteredModuleFields = moduleFields.Where(item => item.IsShowInView || item.IsPrimaryKey).ToList();

                                ModuleField column = filteredModuleFields.Where(x => x.FieldName == filterArr[0]).FirstOrDefault();
                                if (column is not null && filterArr[1] == "LIKE")
                                {
                                    if (column.FK_FieldType_ID == (int)EFieldType.ReferenceTable)
                                    {
                                        if (string.IsNullOrEmpty(column.TabelReferenceNameAlias))
                                        {
                                            searchQuery = " WHERE (('(' + CAST(" + moduleName + "." + filterArr[0]
                                                + " AS VARCHAR(MAX)) + ') - ' + CAST(" + column.TabelReferenceName + "."
                                                + column.TableReferenceFieldDisplayName + " AS VARCHAR(MAX))) " + restOfFilter;
                                        }
                                        else
                                        {
                                            searchQuery = " WHERE (('(' + CAST(" + moduleName + "." + filterArr[0]
                                                + " AS VARCHAR(MAX)) + ') - ' + CAST(" + column.TabelReferenceNameAlias + "."
                                                + column.TableReferenceFieldDisplayName + " AS VARCHAR(MAX))) " + restOfFilter;
                                        }

                                    }
                                    else if (column.FK_FieldType_ID == (int)EFieldType.FLOAT || column.FK_FieldType_ID == (int)EFieldType.NUMERICDECIMAL ||
                                        column.FK_FieldType_ID == (int)EFieldType.REAL || column.FK_FieldType_ID == (int)EFieldType.MONEY)
                                    {
                                        searchQuery = " WHERE (FORMAT(" + moduleName + "." + filterArr[0]
                                            + ",'#.###############') " + restOfFilter;
                                    }
                                    else if (column.FK_FieldType_ID == (int)EFieldType.DATETIME)
                                    {
                                        searchQuery = " WHERE (FORMAT(" + moduleName + "." + filterArr[0]
                                            + ", '" + dateFormat + "') " + restOfFilter;
                                    }
                                    else
                                    {
                                        searchQuery = " WHERE (" + moduleName + "." + filter;
                                    }
                                }
                                else
                                {
                                    if (filterArr[0] == "CreatedDate" || filterArr[0] == "LastUpdateDate" ||
                                        filterArr[0] == "ApprovedDate")
                                    {
                                        searchQuery = " WHERE (FORMAT(" + moduleName + "." + filterArr[0]
                                            + ", '" + dateFormat + "') " + restOfFilter;
                                    }
                                    else
                                    {
                                        searchQuery = " WHERE (" + moduleName + "." + filter;
                                    }
                                }
                            }
                        }

                        #region Custom Support
                        query = "SELECT * FROM ModuleCustomSupport WHERE FK_Module_ID = @ModuleID";
                        prms = new SqlParameter[]
                        {
                            new SqlParameter("@ModuleID", SqlDbType.Int){ Value = moduleID }
                        };
                        DataTable dtCustom = NawaDAO.ExecuteTable(query, prms);

                        if (dtCustom != null && dtCustom.Rows.Count > 0)
                        {
                            List<ModuleCustomSupport> listCustomSupport = NawaDAO.ConvertDataTable<ModuleCustomSupport>(dtCustom);
                            List<ModuleField> listFields = moduleFields.Where(item => item.IsShowInView || item.IsPrimaryKey).ToList();
                            ModuleField primaryField = listFields.Where(x => x.IsPrimaryKey).FirstOrDefault();

                            if (primaryField != null)
                            {
                                foreach (ModuleCustomSupport item in listCustomSupport)
                                {
                                    if (!string.IsNullOrEmpty(item.EnableConditionQuery))
                                    {
                                        if (!String.IsNullOrEmpty(moduleColumns))
                                        {
                                            moduleColumns += ", ";
                                        }

                                        string customSupportAlias = "ALIAS_" + item.SupportName.Replace(' ', '_');
                                        moduleColumns += "CASE WHEN EXISTS(SELECT " + customSupportAlias + ".* FROM " + moduleName + " AS " + customSupportAlias + " WHERE " + item.EnableConditionQuery + " AND " + customSupportAlias + "." + primaryField.FieldName + " = " + moduleName + "." + primaryField.FieldName + ") THEN 1 ELSE 0 END AS CUSTOM_" + item.SupportName.Replace(' ', '_');
                                    }
                                }
                            }
                            else
                            {
                                throw new Exception("This module has no primary key");
                            }
                        }
                        #endregion

                        #region show last update
                        AppSystemParameter createdBySysParam = SystemParameterBLL.GetAppSystemParameter(43);
                        string createdBy = createdBySysParam != null ? createdBySysParam.SettingValue : "0";

                        AppSystemParameter lastUpdateBySysParam = SystemParameterBLL.GetAppSystemParameter(44);
                        string lastUpdateBy = lastUpdateBySysParam != null ? lastUpdateBySysParam.SettingValue : "0";

                        AppSystemParameter approvedBySysParam = SystemParameterBLL.GetAppSystemParameter(45);
                        string approvedBy = approvedBySysParam != null ? approvedBySysParam.SettingValue : "0";

                        AppSystemParameter createdDateSysParam = SystemParameterBLL.GetAppSystemParameter(46);
                        string createdDate = createdDateSysParam != null ? createdDateSysParam.SettingValue : "0";

                        AppSystemParameter lastUpdateDateSysParam = SystemParameterBLL.GetAppSystemParameter(47);
                        string lastUpdateDate = lastUpdateDateSysParam != null ? lastUpdateDateSysParam.SettingValue : "0";

                        AppSystemParameter approvedDateSysParam = SystemParameterBLL.GetAppSystemParameter(48);
                        string approvedDate = approvedDateSysParam != null ? approvedDateSysParam.SettingValue : "0";

                        AppSystemParameter activeSysParam = SystemParameterBLL.GetAppSystemParameter(50);
                        string active = activeSysParam != null ? activeSysParam.SettingValue : "0";

                        if (createdBy == "1")
                        {
                            moduleColumns += ", " + moduleName + "." + "CreatedBy";
                            outerModuleColumns += ", CreatedBy";

                            if (!string.IsNullOrEmpty(search))
                            {
                                if (String.IsNullOrEmpty(searchQuery))
                                {
                                    searchQuery = " WHERE ( " + moduleName + ".CreatedBy LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                                }
                                else
                                {
                                    searchQuery = searchQuery + " OR " + moduleName + ".CreatedBy LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                                }
                            }
                        }

                        if (lastUpdateBy == "1")
                        {
                            moduleColumns += ", " + moduleName + "." + "LastUpdateBy";
                            outerModuleColumns += ", LastUpdateBy";

                            if (!string.IsNullOrEmpty(search))
                            {
                                if (String.IsNullOrEmpty(searchQuery))
                                {
                                    searchQuery = " WHERE ( " + moduleName + ".LastUpdateBy LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                                }
                                else
                                {
                                    searchQuery = searchQuery + " OR " + moduleName + ".LastUpdateBy LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                                }
                            }
                        }

                        if (approvedBy == "1")
                        {
                            moduleColumns += ", " + moduleName + "." + "ApprovedBy";
                            outerModuleColumns += ", ApprovedBy";

                            if (!string.IsNullOrEmpty(search))
                            {
                                if (String.IsNullOrEmpty(searchQuery))
                                {
                                    searchQuery = " WHERE ( " + moduleName + ".ApprovedBy LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                                }
                                else
                                {
                                    searchQuery = searchQuery + " OR " + moduleName + ".ApprovedBy LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                                }
                            }
                        }

                        if (createdDate == "1")
                        {
                            moduleColumns += ", " + moduleName + "." + "CreatedDate";
                            outerModuleColumns += ", CreatedDate";

                            if (!string.IsNullOrEmpty(search))
                            {
                                if (String.IsNullOrEmpty(searchQuery))
                                {
                                    searchQuery = " WHERE ( FORMAT(" + moduleName + ".CreatedDate, '" + dateFormat + "') LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                                }
                                else
                                {
                                    searchQuery = searchQuery + " OR FORMAT(" + moduleName + ".CreatedDate, '" + dateFormat + "') LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                                }
                            }
                        }

                        if (lastUpdateDate == "1")
                        {
                            moduleColumns += ", " + moduleName + "." + "LastUpdateDate";
                            outerModuleColumns += ", LastUpdateDate";

                            if (!string.IsNullOrEmpty(search))
                            {
                                if (String.IsNullOrEmpty(searchQuery))
                                {
                                    searchQuery = " WHERE ( FORMAT(" + moduleName + ".LastUpdateDate, '" + dateFormat + "') LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                                }
                                else
                                {
                                    searchQuery = searchQuery + " OR FORMAT(" + moduleName + ".LastUpdateDate, '" + dateFormat + "') LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                                }
                            }
                        }

                        if (approvedDate == "1")
                        {
                            moduleColumns += ", " + moduleName + "." + "ApprovedDate";
                            outerModuleColumns += ", ApprovedDate";

                            if (!string.IsNullOrEmpty(search))
                            {
                                if (String.IsNullOrEmpty(searchQuery))
                                {
                                    searchQuery = " WHERE ( FORMAT(" + moduleName + ".ApprovedDate, '" + dateFormat + "') LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                                }
                                else
                                {
                                    searchQuery = searchQuery + " OR FORMAT(" + moduleName + ".ApprovedDate, '" + dateFormat + "') LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                                }
                            }
                        }

                        if (active == "1")
                        {
                            moduleColumns += ", " + moduleName + "." + "Active";
                            outerModuleColumns += ", Active";

                            if (!string.IsNullOrEmpty(search))
                            {
                                if (String.IsNullOrEmpty(searchQuery))
                                {
                                    searchQuery = " WHERE ( " + moduleName + ".Active LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                                }
                                else
                                {
                                    searchQuery = searchQuery + " OR CAST( CASE WHEN " + moduleName +
                                        ".Active = 1 THEN 'true' ELSE 'false' END AS VARCHAR) LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                                }
                            }
                        }

                        #endregion

                        if (!string.IsNullOrEmpty(searchQuery))
                        {
                            searchQuery = searchQuery + " )";
                        }

                        if (!string.IsNullOrEmpty(advQuery))
                        {
                            advQuery = advQuery + " )";
                        }

                        //Data Access has to be in the last order for the query to work as intended
                        #region data access
                        string queryDataAccess = "SELECT * From DataAccess Where FK_Module_ID = @ModuleID AND (FK_Role_ID = @RoleID OR FK_Role_ID = 0 OR FK_Role_ID IS NULL)";
                        prms = new SqlParameter[]
                        {
                            new SqlParameter("@ModuleID", SqlDbType.Int){ Value = moduleID },
                            new SqlParameter("@RoleID", SqlDbType.Int){ Value = roleID }
                        };
                        DataTable dtDataAccess = NawaDAO.ExecuteTable(queryDataAccess, prms);

                        if (dtDataAccess != null && dtDataAccess.Rows.Count > 0)
                        {
                            string filterData = "";

                            foreach (DataRow drDataAccess in dtDataAccess.Rows)
                            {
                                filterData += " AND " + Convert.ToString(drDataAccess["FilterData"]).ToLower().Replace("@userid", userID);
                            }

                            string filterDataFinal = filterData.Remove(0, 5);

                            if (string.IsNullOrEmpty(searchQuery))
                            {
                                searchQuery = " WHERE (" + filterDataFinal + ")";
                            }
                            else
                            {
                                searchQuery = searchQuery + " AND (" + filterDataFinal + ")";
                            }
                        }
                        #endregion

                        // result count
                        query = @"SELECT COUNT(13) AS [RowCount] FROM (SELECT " + moduleColumns + " FROM "
                            + moduleName + moduleJoin + searchQuery + ") X " + advQuery;
                        rowCount = Convert.ToInt32(NawaDAO.ExecuteScalar(query));

                        // result with pagination
                        int pageSize = Convert.ToInt32(SystemParameterBLL.GetAppSystemParameter(7).SettingValue);
                        query = "EXEC usp_PagingNew @Query, @OrderBy, @PageIndex, @PageSize";
                        prms = new SqlParameter[]
                        {
                            new SqlParameter("@Query", SqlDbType.VarChar){ Value = "SELECT " + outerModuleColumns + " FROM ( SELECT "  + moduleColumns + " FROM " + moduleName + moduleJoin + searchQuery + ") X " + advQuery},
                            new SqlParameter("@OrderBy", SqlDbType.VarChar){ Value = (!String.IsNullOrEmpty(orderBy) ? orderBy + " " + order : "") },
                            new SqlParameter("@PageIndex", SqlDbType.Int){ Value = page },
                            new SqlParameter("@PageSize", SqlDbType.Int){ Value = pageSize }
                        };
                        DataTable dt = NawaDAO.ExecuteTable(query, prms);
                        res = dt.AsEnumerable().Select(row => new DynamicDictionary(row)).ToList();
                    }
                }

                return res;
            }
            catch
            {
                throw;
            }
        }



        public List<ModuleApprovalView> GetModuleApproval(int moduleID, string userID, int page, string orderBy, string order, string search, string filterKey, string filterValue, out int rowCount)
        {
            try
            {
                string query = "EXEC usp_GetModuleApproval @ModuleKey, @UserID, @Search, @filterKey, @filterValue";
                SqlParameter[] prms = new SqlParameter[]
                {
                    new SqlParameter("@ModuleKey", SqlDbType.VarChar){ Value = moduleID.ToString() },
                    new SqlParameter("@UserID", SqlDbType.VarChar){ Value = userID },
                    new SqlParameter("@Search", SqlDbType.VarChar){ Value = search ?? "" },
                    new SqlParameter("@filterKey", SqlDbType.VarChar){ Value = filterKey ?? "" },
                    new SqlParameter("@filterValue", SqlDbType.VarChar){ Value = filterValue ?? "" },
                };
                DataTable dt = NawaDAO.ExecuteTable(query, prms);
                List<ModuleApprovalView> res = NawaDAO.ConvertDataTable<ModuleApprovalView>(dt);
                rowCount = res.Count;
                AppSystemParameter sysParamPageSize = SystemParameterBLL.GetAppSystemParameter(7);
                int pageSize = sysParamPageSize != null ? Convert.ToInt32(sysParamPageSize.SettingValue) : 12;
                res = res.Skip((page - 1) * pageSize).Take(pageSize).ToList();

                return res;
            }
            catch
            {
                throw;
            }
        }


        private void FinalApprove(Module module, ModuleApproval moduleApproval, string userID, out object identityTemp, NawaConnection connection)
        {
            object identity = 0;
            //string query = "SELECT * FROM ModuleField WHERE FK_Module_ID = @ModuleID AND ( IsShowInForm = 1 OR IsPrimaryKey = 1 ) ORDER BY Sequence";
            //SqlParameter[] prms = new SqlParameter[]
            //{
            //    new SqlParameter("@ModuleID", SqlDbType.Int) { Value = module.PK_Module_ID }
            //};
            //DataTable dtFields = NawaDAO.ExecuteTable(query, prms);
            List<SqlParameter> listParams = new List<SqlParameter>();
            Dictionary<string, object>[] newData = JsonConvert.DeserializeObject<Dictionary<string, object>[]>(moduleApproval.ModuleField);
            Dictionary<string, object>[] oldData = JsonConvert.DeserializeObject<Dictionary<string, object>[]>(moduleApproval.ModuleFieldBefore);

            #region Variables for Stored Procedure Validation
            List<ModuleField> listFields = moduleFields.Where(item => item.IsShowInView || item.IsPrimaryKey).OrderBy(item => item.Sequence).ToList();
            ModuleField primaryField = listFields.Where(x => x.IsPrimaryKey).FirstOrDefault();
            string primaryFieldName = primaryField != null ? primaryField.FieldName : string.Empty;
            CParameterInput parameterInput = new CParameterInput();
            parameterInput.ModuleID = module.PK_Module_ID;
            parameterInput.data = new Dictionary<string, object>();

            if (moduleApproval.PK_ModuleAction_ID == (int)EActionType.Insert || moduleApproval.PK_ModuleAction_ID == (int)EActionType.Update || moduleApproval.PK_ModuleAction_ID == (int)EActionType.Activation)
            {
                parameterInput.data = newData[0];
            }
            else if (moduleApproval.PK_ModuleAction_ID == (int)EActionType.Delete)
            {
                parameterInput.data = oldData[0];
            }
            #endregion

            //Stored Procedure Validation Before Save Approval
            parameterInput.ActionType = EActionType.Approval;
            ValidateStoreProcedure((int)EModuleTime.BeforeSave, parameterInput, listFields, connection, true, moduleApproval.CreatedBy);

            if (moduleApproval.PK_ModuleAction_ID == (int)EActionType.Insert)
            {
                string tableFields = "";
                string insertParams = "";
                bool isPkIdentity = true;

                foreach (ModuleField moduleField in moduleFields.Where(item => item.IsShowInView || item.IsPrimaryKey).OrderBy(item => item.Sequence).ToList())
                {
                    if (!Convert.ToBoolean(moduleField.IsPrimaryKey)
                        || (Convert.ToBoolean(moduleField.IsPrimaryKey) && Convert.ToInt32(moduleField.FK_FieldType_ID) != (int)EFieldType.IDENTITY && Convert.ToInt32(moduleField.FK_FieldType_ID) != (int)EFieldType.BIGIDENTITY))
                    {
                        if (Convert.ToInt32(moduleField.FK_FieldType_ID) == (int)EFieldType.ReferenceTable)
                        {
                            if (!string.IsNullOrEmpty(tableFields)) tableFields += ", ";

                            tableFields += moduleField.FieldName;

                            if (!string.IsNullOrEmpty(insertParams)) insertParams += ", ";

                            insertParams += "@" + moduleField.FieldName;
                            string value = Convert.ToString(newData[0][Convert.ToString(moduleField.FieldName)]);

                            if (string.IsNullOrEmpty(value))
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(moduleField.DefaultValue)))
                                {
                                    value = Convert.ToString(moduleField.DefaultValue);
                                }
                            }

                            listParams.Add(GetSqlParameterRaw(Convert.ToString(moduleField.FieldName), Convert.ToInt32(moduleField.FK_FieldType_ID), value));
                        }
                        else if (Convert.ToInt32(moduleField.FK_FieldType_ID) == (int)EFieldType.VarBinary)
                        {
                            #region Insert UploadFile FileName
                            string fieldFileName = Convert.ToString(moduleField.FieldName) + "Name";

                            if (!String.IsNullOrEmpty(tableFields)) tableFields += ", ";

                            tableFields += Convert.ToString(moduleField.FieldName);

                            if (!String.IsNullOrEmpty(tableFields)) tableFields += ", ";

                            tableFields += fieldFileName;

                            if (!String.IsNullOrEmpty(insertParams)) insertParams += ", ";

                            insertParams += "@" + Convert.ToString(moduleField.FieldName);

                            if (!String.IsNullOrEmpty(insertParams)) insertParams += ", ";

                            insertParams += "@" + fieldFileName;

                            listParams.Add(new SqlParameter("@" + fieldFileName, SqlDbType.VarChar) { Value = newData[0][fieldFileName] });
                            listParams.Add(GetSqlParameterRaw(Convert.ToString(moduleField.FieldName), Convert.ToInt32(moduleField.FK_FieldType_ID), Convert.FromBase64String(Convert.ToString(newData[0][Convert.ToString(moduleField.FieldName)]))));
                            #endregion
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(tableFields)) tableFields += ", ";

                            tableFields += moduleField.FieldName;

                            if (!string.IsNullOrEmpty(insertParams)) insertParams += ", ";

                            insertParams += "@" + moduleField.FieldName;

                            object value = newData[0][Convert.ToString(moduleField.FieldName)];

                            if (value == null)
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(moduleField.DefaultValue)))
                                {
                                    value = Convert.ToString(moduleField.DefaultValue);
                                }
                            }

                            listParams.Add(GetSqlParameterRaw(Convert.ToString(moduleField.FieldName), Convert.ToInt32(moduleField.FK_FieldType_ID), newData[0][Convert.ToString(moduleField.FieldName)] ?? DBNull.Value));
                        }
                    }

                    if (Convert.ToBoolean(moduleField.IsPrimaryKey) && Convert.ToInt32(moduleField.FK_FieldType_ID) != (int)EFieldType.IDENTITY && Convert.ToInt32(moduleField.FK_FieldType_ID) != (int)EFieldType.BIGIDENTITY)
                    {
                        isPkIdentity = false;
                        identity = newData[0][Convert.ToString(moduleField.FieldName)];
                    }
                }

                if (!string.IsNullOrEmpty(tableFields)) tableFields += ", ";

                tableFields += "Active, CreatedBy, LastUpdateBy, ApprovedBy, CreatedDate, LastUpdateDate, ApprovedDate";

                if (!string.IsNullOrEmpty(insertParams)) insertParams += ", ";

                insertParams += "@Active, @CreatedBy, @LastUpdateBy, @ApprovedBy, GETDATE(), GETDATE(), GETDATE()";

                listParams.Add(new SqlParameter("@Active", SqlDbType.Bit) { Value = true });
                listParams.Add(new SqlParameter("@CreatedBy", SqlDbType.VarChar) { Value = moduleApproval.CreatedBy });
                listParams.Add(new SqlParameter("@LastUpdateBy", SqlDbType.VarChar) { Value = moduleApproval.CreatedBy });
                listParams.Add(new SqlParameter("@ApprovedBy", SqlDbType.VarChar) { Value = userID });

                string query = "INSERT INTO dbo." + moduleName + " (" + tableFields + ") " + Environment.NewLine;
                query += "VALUES (" + insertParams + ")" + Environment.NewLine; ;

                if (isPkIdentity)
                {
                    query += "SELECT SCOPE_IDENTITY()";
                    identity = NawaDAO.ExecuteScalar(query, listParams.ToArray(), connection);
                    parameterInput.data[primaryFieldName] = identity;
                }
                else
                {
                    NawaDAO.ExecuteNonQuery(query, listParams.ToArray(), connection);
                }
            }
            else if (moduleApproval.PK_ModuleAction_ID == (int)EActionType.Update)
            {
                string updateFields = "";
                string updateKey = "";

                foreach (ModuleField moduleField in moduleFields.Where(item => item.IsShowInView || item.IsPrimaryKey).OrderBy(item => item.Sequence).ToList())
                {
                    if (Convert.ToBoolean(moduleField.IsPrimaryKey))
                    {
                        updateKey = moduleField.FieldName + " = @" + moduleField.FieldName;
                        identity = newData[0][Convert.ToString(moduleField.FieldName)];
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(updateFields))
                        {
                            updateFields += ", " + Environment.NewLine;
                        }

                        updateFields += moduleField.FieldName + " = @" + moduleField.FieldName;
                    }

                    if (Convert.ToInt32(moduleField.FK_FieldType_ID) == (int)EFieldType.VarBinary)
                    {
                        #region Update FileUpload FileName
                        string fieldFileName = Convert.ToString(moduleField.FieldName) + "Name";

                        if (!String.IsNullOrEmpty(updateFields))
                        {
                            updateFields += ", " + Environment.NewLine;
                        }

                        updateFields += fieldFileName + " = @" + fieldFileName;

                        listParams.Add(new SqlParameter("@" + fieldFileName, SqlDbType.VarChar) { Value = newData[0][fieldFileName] });
                        listParams.Add(GetSqlParameterRaw(Convert.ToString(moduleField.FieldName), Convert.ToInt32(moduleField.FK_FieldType_ID), Convert.FromBase64String(Convert.ToString(newData[0][Convert.ToString(moduleField.FieldName)]))));
                        #endregion
                    }
                    else if (Convert.ToInt32(moduleField.FK_FieldType_ID) == (int)EFieldType.ReferenceTable)
                    {
                        string value = Convert.ToString(newData[0][Convert.ToString(moduleField.FieldName)]);

                        listParams.Add(GetSqlParameterRaw(Convert.ToString(moduleField.FieldName), Convert.ToInt32(moduleField.FK_FieldType_ID), value));
                    }
                    else
                    {
                        listParams.Add(GetSqlParameterRaw(Convert.ToString(moduleField.FieldName), Convert.ToInt32(moduleField.FK_FieldType_ID), newData[0][Convert.ToString(moduleField.FieldName)] ?? DBNull.Value));
                    }
                }

                if (!string.IsNullOrEmpty(updateFields))
                {
                    updateFields += ", " + Environment.NewLine;
                }

                updateFields += "LastUpdateBy = @LastUpdateBy, " + Environment.NewLine;
                updateFields += "LastUpdateDate = GETDATE(), " + Environment.NewLine;
                updateFields += "ApprovedBy = @ApprovedBy, " + Environment.NewLine;
                updateFields += "ApprovedDate = GETDATE() ";

                listParams.Add(new SqlParameter("@LastUpdateBy", SqlDbType.VarChar) { Value = moduleApproval.CreatedBy });
                listParams.Add(new SqlParameter("@ApprovedBy", SqlDbType.VarChar) { Value = userID });

                string query = "UPDATE dbo." + moduleName + Environment.NewLine;
                query += "SET " + updateFields + Environment.NewLine;
                query += "WHERE " + updateKey + Environment.NewLine;

                NawaDAO.ExecuteScalar(query, listParams.ToArray(), connection);

                #region From Validation Summary Detail
                query = "UPDATE Web_ValidationReport_RowComplate SET Edited = 1 WHERE ModuleID = @ModuleID AND KeyFieldValue = @KeyFieldValue AND TanggalData < @TanggalData";
                SqlParameter[] prms = new SqlParameter[]
                {
                    new SqlParameter("@ModuleID", SqlDbType.Int) { Value = module.PK_Module_ID },
                    new SqlParameter("@KeyFieldValue", SqlDbType.VarChar) { Value = newData[0][primaryFieldName].ToString() },
                    new SqlParameter("@TanggalData", SqlDbType.DateTime) { Value = DateTime.Now }
                };

                NawaDAO.ExecuteNonQuery(query, prms, connection);
                #endregion
            }
            else if (moduleApproval.PK_ModuleAction_ID == (int)EActionType.Delete)
            {
                string query = "DELETE dbo." + moduleName + " WHERE " + primaryFieldName + " = @IDData";
                SqlParameter[] prms = new SqlParameter[]
                {
                    new SqlParameter("@IDData", SqlDbType.Int){ Value = oldData[0][primaryFieldName] }
                };
                int rowAffected = NawaDAO.ExecuteNonQuery(query, prms, connection);

                #region DELETE MODULE DETAIL
                List<ModuleDetail> moduleDetailList = GetModuleDetail(module.PK_Module_ID);

                foreach (ModuleDetail moduleDetail in moduleDetailList)
                {
                    query = "SELECT FieldName FROM ModuleDetailField WHERE FK_ModuleDetail_ID = @ModuleDetailID AND FK_FieldType_ID = 16";
                    prms = new SqlParameter[]
                    {
                        new SqlParameter("@ModuleDetailID", SqlDbType.Int){ Value = moduleDetail.PK_ModuleDetail_ID }
                    };
                    string foreignKey = Convert.ToString(NawaDAO.ExecuteScalar(query, prms, connection));
                    query = "DELETE dbo." + moduleDetail.ModuleDetailName + " WHERE " + foreignKey + " = @IDData";
                    prms = new SqlParameter[]
                    {
                        new SqlParameter("@IDData", SqlDbType.Int){ Value = oldData[0][primaryFieldName] }
                    };
                    rowAffected = NawaDAO.ExecuteNonQuery(query, prms, connection);
                }
                #endregion
            }
            else if (moduleApproval.PK_ModuleAction_ID == (int)EActionType.Import)
            {
                string query = "EXEC usp_ApproveUploadData @pkapprovalid, @userid";
                SqlParameter[] prms = new SqlParameter[]
                {
                    new SqlParameter("@pkapprovalid", SqlDbType.BigInt){ Value = moduleApproval.PK_ModuleApproval_ID },
                    new SqlParameter("@userid", SqlDbType.VarChar){ Value = userID }
                };
                NawaDAO.ExecuteNonQuery(query, prms, connection);
            }
            else if (moduleApproval.PK_ModuleAction_ID == (int)EActionType.Activation)
            {
                string query = "UPDATE dbo." + moduleName + Environment.NewLine;
                query += "SET Active = CASE WHEN Active = 1 THEN 0 ELSE 1 END, " + Environment.NewLine;
                query += "LastUpdateBy = @LastUpdateBy, " + Environment.NewLine;
                query += "LastUpdateDate = GETDATE(), " + Environment.NewLine;
                query += "ApprovedBy = @ApprovedBy, " + Environment.NewLine;
                query += "ApprovedDate = GETDATE() " + Environment.NewLine;
                query += "WHERE " + primaryFieldName + " = @IDData";
                SqlParameter[] prms = new SqlParameter[]
                {
                    new SqlParameter("@IDData", SqlDbType.Int){ Value = newData[0][primaryFieldName] },
                    new SqlParameter("@LastUpdateBy", SqlDbType.VarChar) { Value = moduleApproval.CreatedBy },
                    new SqlParameter("@ApprovedBy", SqlDbType.VarChar) { Value = userID }
                };

                NawaDAO.ExecuteNonQuery(query, prms, connection);
            }

            identityTemp = identity;

            #region Stored Procedure Validation After Save
            if (moduleApproval.PK_ModuleAction_ID == (int)EActionType.Import)
            {
                ValidateStoreProcedureAfterupload(EModuleTime.AfterSave, EActionType.Import, module.PK_Module_ID, moduleApproval.CreatedBy, connection);
            }
            else
            {
                parameterInput.ActionType = (EActionType)moduleApproval.PK_ModuleAction_ID;
                ValidateStoreProcedure((int)EModuleTime.AfterSave, parameterInput, listFields, connection, true, moduleApproval.CreatedBy);
            }

            //Stored Procedure Validation After Save Approval
            parameterInput.ActionType = EActionType.Approval;
            ValidateStoreProcedure((int)EModuleTime.AfterSave, parameterInput, listFields, connection, true, moduleApproval.CreatedBy);
            #endregion
        }


        public bool ValidateStoreProcedure(int validateTime, CParameterInput parameterInput, List<ModuleField> moduleFields, NawaConnection connection, bool isApproval, string userID)
        {
            string sql = @"SELECT StoreProcedureName, StoreProcedureParameter, StoreProcedureParameterValueFieldSequence
                           FROM dbo.ModuleValidation
                           WHERE FK_ModuleTime_ID = @ValidateTime AND FK_Module_ID = @PK_Module_ID AND FK_ModuleAction_ID = @PK_ModuleAction_ID";
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@PK_Module_ID", SqlDbType.Int) {Value = parameterInput.ModuleID},
                new SqlParameter("@PK_ModuleAction_ID", SqlDbType.Int) {Value = (int)parameterInput.ActionType},
                new SqlParameter("@ValidateTime", SqlDbType.Int) {Value = validateTime}
            };
            DataTable dt = NawaDAO.ExecuteTable(sql, parameters, connection);

            if (dt != null)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    if (parameterInput.ActionType == EActionType.Approval)
                    {
                        parameters = new SqlParameter[]
                        {
                            new SqlParameter("@UserID", SqlDbType.VarChar) { Value = userID },
                        };

                        if (validateTime == (int)EModuleTime.BeforeSave)
                        {
                            DataTable dtResult = NawaDAO.ExecuteTable(dr["StoreProcedureName"].ToString(), parameters, connection, CommandType.StoredProcedure);

                            if (dtResult != null)
                            {
                                string strError = "";

                                foreach (DataRow drResult in dtResult.Rows)
                                {
                                    strError += drResult[0] + Environment.NewLine;
                                }

                                if (!String.IsNullOrEmpty(strError))
                                {
                                    throw new Exception(strError);
                                }
                            }
                        }
                        else if (validateTime == (int)EModuleTime.AfterSave)
                        {
                            NawaDAO.ExecuteNonQuery(dr["StoreProcedureName"].ToString(), parameters, connection, CommandType.StoredProcedure);
                        }
                    }
                    else
                    {
                        string[] spParameters = dr["StoreProcedureParameter"].ToString().Replace(" ", "").Split(",");
                        string[] spParameterSequences = dr["StoreProcedureParameterValueFieldSequence"].ToString().Replace(" ", "").Split(",");

                        if (spParameters.Length > 0 && spParameters.Length == spParameterSequences.Length)
                        {
                            List<SqlParameter> sqlParameters = new List<SqlParameter>();

                            if (!String.IsNullOrEmpty(dr["StoreProcedureParameter"].ToString()))
                            {
                                for (int i = 0; i < spParameters.Length; i++)
                                {
                                    if (spParameterSequences[i].ToLower() == "@userid")
                                    {
                                        SqlParameter parameter = new SqlParameter(spParameters[i], userID);

                                        sqlParameters.Add(parameter);
                                    }
                                    else
                                    {
                                        ModuleField mf = moduleFields.Where(item => item.Sequence.ToString() == spParameterSequences[i]).FirstOrDefault();

                                        if (mf.FK_FieldType_ID == 12 || mf.FK_FieldType_ID == 15)
                                        {
                                            SqlParameter parameter = new SqlParameter(spParameters[i], validateTime == 1 ? (parameterInput.ActionType == EActionType.Insert ? 0 : NawaDataBLL.CommonBLL.GetObject(mf.FK_FieldType_ID, parameterInput.data[mf.FieldName])) : parameterInput.data[mf.FieldName]);

                                            sqlParameters.Add(parameter);
                                        }
                                        else
                                        {
                                            SqlParameter parameter = new SqlParameter(spParameters[i], NawaDataBLL.CommonBLL.GetObject(mf.FK_FieldType_ID, parameterInput.data[mf.FieldName]));

                                            sqlParameters.Add(parameter);
                                        }
                                    }
                                }
                            }

                            if (validateTime == 1)
                            {
                                DataTable dtResult = NawaDAO.ExecuteTable(dr["StoreProcedureName"].ToString(), sqlParameters.ToArray(), connection, CommandType.StoredProcedure);

                                if (dtResult != null)
                                {
                                    string strError = "";

                                    foreach (DataRow drResult in dtResult.Rows)
                                    {
                                        strError += drResult[0] + Environment.NewLine;
                                    }

                                    if (!String.IsNullOrEmpty(strError))
                                    {
                                        throw new Exception(strError);
                                    }
                                }
                            }
                            else if (validateTime == 2)
                            {
                                NawaDAO.ExecuteNonQuery(dr["StoreProcedureName"].ToString(), sqlParameters.ToArray(), connection, CommandType.StoredProcedure);
                            }
                        }
                        else
                        {
                            throw new Exception("Sql Parameter list and sql Value list not match");
                        }
                    }
                }
            }

            return true;
        }


        private void ValidateStoreProcedureAfterupload(EModuleTime intmoduleTime, EActionType intmoduleAction, int moduleID, string UserID, NawaConnection conn)
        {
            List<ModuleValidation> objshemaModuleValidation = new List<ModuleValidation>();

            Module objSchemaModule;

            string queryStr = @"SELECT * FROM Module WHERE PK_Module_ID = " + moduleID;
            DataRow drModule = NawaDAO.ExecuteRow(queryStr, null, conn);
            objSchemaModule = NawaDAO.ConvertDataTable<Module>(drModule);
            if (objSchemaModule != null)
            {
                if (objSchemaModule.IsUseDesigner)
                {
                    queryStr = @"SELECT * FROM ModuleValidation WHERE FK_Module_ID = " + moduleID;
                    DataTable drModuleValidation = NawaDAO.ExecuteTable(queryStr, null, conn);
                    objshemaModuleValidation = NawaDAO.ConvertDataTable<ModuleValidation>(drModuleValidation);
                }

                if (objSchemaModule.IsUseStoreProcedureValidation)
                {
                    foreach (ModuleValidation moduleValidation in objshemaModuleValidation)
                    {
                        if (moduleValidation.FK_ModuleTime_ID == Convert.ToInt32(intmoduleTime) && moduleValidation.FK_ModuleAction_ID == Convert.ToInt32(intmoduleAction))
                        {
                            SqlParameter[] parameters = new SqlParameter[] { new SqlParameter("@UserID", SqlDbType.VarChar) { Value = UserID } };
                            NawaDAO.ExecuteNonQuery(moduleValidation.StoreProcedureName, parameters, conn, CommandType.StoredProcedure);
                        }
                    }
                }
            }
        }

        public void ApproveModuleApproval(long pkModuleApprovalID, string userID, int pkMUserID, string review)
        {
            NawaConnection connection = NawaDAO.CreateConnection();
            connection.BeginTransaction();

            try
            {
                string query = "SELECT * FROM ModuleApproval WHERE PK_ModuleApproval_ID = @ID";
                SqlParameter[] prms = new SqlParameter[]
                {
                    new SqlParameter("@ID", SqlDbType.BigInt) { Value = pkModuleApprovalID }
                };
                DataRow drModuleApproval = NawaDAO.ExecuteRow(query, prms, connection);

                if (drModuleApproval != null)
                {
                    ModuleApproval moduleApproval = NawaDAO.ConvertDataTable<ModuleApproval>(drModuleApproval);
                    object identityTemp = 0;

                    if (moduleApproval.FK_Module_ID > 0)
                    {
                        query = "SELECT * FROM Module WHERE PK_Module_ID = @ModuleID";
                        prms = new SqlParameter[]
                        {
                            new SqlParameter("@ModuleID", SqlDbType.VarChar) { Value = moduleApproval.FK_Module_ID }
                        };
                    }
                    else
                    {
                        query = "SELECT * FROM Module WHERE ModuleName = @ModuleName";
                        prms = new SqlParameter[]
                        {
                            new SqlParameter("@ModuleName", SqlDbType.VarChar) { Value = moduleApproval.ModuleName }
                        };
                    }

                    DataRow drModule = NawaDAO.ExecuteRow(query, prms, connection);

                    if (drModule != null)
                    {
                        Module module = NawaDAO.ConvertDataTable<Module>(drModule);
                        prms = new SqlParameter[]
                        {
                            new SqlParameter("@PkMOduleid", SqlDbType.Int) { Value = module.PK_Module_ID },
                            new SqlParameter("@pkunikid", SqlDbType.VarChar) { Value = (moduleApproval.PK_ModuleAction_ID == (int)EActionType.Import ? moduleApproval.PK_ModuleApproval_ID + "import" : moduleApproval.ModuleKey) }
                        };
                        int workflowInprogress = Convert.ToInt32(NawaDAO.ExecuteScalar("IsHaveWorkflowApproval", prms, connection, CommandType.StoredProcedure));

                        if (workflowInprogress > 0)
                        {
                            bool bFinalApprove = false;
                            prms = new SqlParameter[]
                            {
                                new SqlParameter("@FK_ModuleApproval_ID", SqlDbType.BigInt) { Value = pkModuleApprovalID },
                                new SqlParameter("@pkmoduleid", SqlDbType.Int) { Value = module.PK_Module_ID },
                                new SqlParameter("@pkunik", SqlDbType.VarChar) { Value = (moduleApproval.PK_ModuleAction_ID == (int)EActionType.Import ? moduleApproval.PK_ModuleApproval_ID + "import" : moduleApproval.ModuleKey) },
                                new SqlParameter("@pkuseridexecute", SqlDbType.Int) { Value = pkMUserID },
                                new SqlParameter("@notes", SqlDbType.VarChar) { Value = review },
                                new SqlParameter("@intworkflowstatus", SqlDbType.Int) { Value = (int)EActionApprovalType.Approve }
                            };
                            bFinalApprove = Convert.ToBoolean(NawaDAO.ExecuteScalar("usp_SaveWorkflowModuleDesigner", prms, connection, CommandType.StoredProcedure));

                            if (bFinalApprove)
                            {
                                FinalApprove(module, moduleApproval, userID, out identityTemp, connection);

                                if (moduleApproval.PK_ModuleAction_ID != (int)EActionType.Delete)
                                {
                                    #region Save Master Module Detail
                                    query = "SELECT * FROM ModuleDetailApproval WHERE FK_ModuleApproval_ID = @ModuleApprovalID ";
                                    prms = new SqlParameter[]
                                    {
                                        new SqlParameter("ModuleApprovalID", SqlDbType.Int){Value = pkModuleApprovalID}
                                    };

                                    DataTable drModuleDetailApproval = NawaDAO.ExecuteTable(query, prms, connection);
                                    List<ModuleDetailApproval> moduleDetailApproval = NawaDAO.ConvertDataTable<ModuleDetailApproval>(drModuleDetailApproval);

                                    foreach (ModuleDetailApproval data in moduleDetailApproval)
                                    {
                                        if (data.FK_ModuleDetail_ID != null || data.FK_ModuleDetail_ID > 0)
                                        {
                                            query = "SELECT * FROM ModuleDetail WHERE PK_ModuleDetail_ID = @ModuleDetailID";
                                            prms = new SqlParameter[]
                                            {
                                                new SqlParameter("@ModuleDetailID", SqlDbType.VarChar) { Value = data.FK_ModuleDetail_ID }
                                            };
                                        }
                                        else
                                        {
                                            query = "SELECT * FROM ModuleDetail WHERE ModuleDetailName = @ModuleDetailName";
                                            prms = new SqlParameter[]
                                            {
                                                new SqlParameter("@ModuleDetailName", SqlDbType.VarChar) { Value = data.ModuleDetailName }
                                            };
                                        }
                                        DataRow drModuleDetail = NawaDAO.ExecuteRow(query, prms, connection);
                                        ModuleDetail moduleDetail = NawaDAO.ConvertDataTable<ModuleDetail>(drModuleDetail);

                                        FinalApproveDetail(moduleDetail, data, userID, identityTemp);
                                    }
                                    #endregion
                                }

                                #region Notification
                                query = "SELECT * FROM MUser WHERE UserID = @userID";
                                prms = new SqlParameter[]
                                {
                                    new SqlParameter("@userID", SqlDbType.VarChar) { Value = moduleApproval.CreatedBy }
                                };
                                DataRow drApprovalCreator = NawaDAO.ExecuteRow(query, prms, connection);

                                if (drApprovalCreator != null)
                                {
                                    SystemDictionary sysDictionary = SystemParameterBLL.GetSystemDictionary("notification-approval-approved");
                                    SystemDictionary sysDictionaryHeader = SystemParameterBLL.GetSystemDictionary("notification-approval-approved-header");

                                    MNotification notif = new MNotification();
                                    notif.FromUserID = pkMUserID;
                                    notif.ToUserID = Convert.ToInt32(drApprovalCreator["PK_MUser_ID"]);
                                    notif.NotificationHeader = sysDictionaryHeader.TextDefinition;
                                    notif.NotificationBody = sysDictionary.TextDefinition;
                                    notif.IsRead = false;
                                    notif.DirectPage = "";
                                    notif.FK_Module_ID = module.PK_Module_ID;
                                    notif.FK_ModuleAction_ID = moduleApproval.PK_ModuleAction_ID;
                                    notif.FK_ModuleApproval_ID = pkModuleApprovalID;
                                    notif.Active = true;
                                    notif.CreatedBy = userID;

                                    NotificationBLL.InsertNotification(notif);
                                }
                                #endregion

                                #region Delete After Approve
                                prms = new SqlParameter[]
                                {
                                    new SqlParameter("@ModuleApprovalID", SqlDbType.Int) { Value = pkModuleApprovalID }
                                };

                                query = "DELETE ModuleApproval WHERE PK_ModuleApproval_ID = @ModuleApprovalID";
                                NawaDAO.ExecuteNonQuery(query, prms, connection);

                                query = "DELETE ModuleDetailApproval WHERE FK_ModuleApproval_ID = @ModuleApprovalID";
                                NawaDAO.ExecuteNonQuery(query, prms, connection);

                                SystemParameter sysParamUploadUser = SystemParameterBLL.GetSystemParameterValue(53);
                                string tableUploadUser = sysParamUploadUser != null ? sysParamUploadUser.SettingValue : "0";
                                string moduleNameUploadApproval = moduleName;

                                if (tableUploadUser == "1")
                                {
                                    moduleNameUploadApproval += "_Upload_data_" + CommonBLL.StripTableName(userID) + "_approval";
                                }
                                else
                                {
                                    moduleNameUploadApproval += "_Upload_approval";
                                }

                                query = @"  IF OBJECT_ID(@TableUploadName, 'U') IS NOT NULL
                                            BEGIN
	                                            DELETE " + moduleNameUploadApproval + @" WHERE FK_ModuleApproval_ID = @ModuleApprovalID
                                            END";
                                prms = new SqlParameter[]
                                {
                                    new SqlParameter("@TableUploadName", SqlDbType.VarChar) { Value = moduleNameUploadApproval },
                                    new SqlParameter("@ModuleApprovalID", SqlDbType.BigInt) { Value = pkModuleApprovalID }
                                };
                                NawaDAO.ExecuteNonQuery(query, prms, connection);
                                #endregion

                                #region Audit Trail
                                long headerID = AuditTrailBLL.InputAuditTrailHeader(connection, moduleApproval.CreatedBy, moduleName, (EActionType)moduleApproval.PK_ModuleAction_ID, EAuditTrailStatus.AffectedDatabase, pkModuleApprovalID, userID);

                                if (moduleApproval.PK_ModuleAction_ID != (int)EActionType.Import)
                                {
                                    List<Dictionary<string, object>> newData = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(moduleApproval.ModuleField);
                                    List<Dictionary<string, object>> oldData = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(moduleApproval.ModuleFieldBefore);
                                    AuditTrailBLL.InputAuditTrailDetail(connection, headerID, (EActionType)moduleApproval.PK_ModuleAction_ID, newData, oldData);
                                }
                                #endregion

                                #region Send Email
                                query = "SELECT * FROM EmailTemplate WHERE EmailTemplateName = @TemplateName";
                                prms = new SqlParameter[]
                                {
                                    new SqlParameter("@TemplateName", SqlDbType.VarChar){ Value = "Email Template Approve Message" }
                                };
                                DataRow drTemplate = NawaDAO.ExecuteRow(query, prms);

                                if (drTemplate != null)
                                {
                                    EmailTemplate template = NawaDAO.ConvertDataTable<EmailTemplate>(drTemplate);

                                    prms = new SqlParameter[]
                                    {
                                        new SqlParameter("@PK_EmailTemplate_ID", SqlDbType.BigInt){ Value = template.PK_EmailTemplate_ID },
                                        new SqlParameter("@recordID", SqlDbType.VarChar){ Value = Convert.ToString(headerID) },
                                        new SqlParameter("@EmailIDReference", SqlDbType.VarChar){ Value = "0" },
                                    };

                                    NawaDAO.ExecuteNonQuery("usp_EMailScheduler_Create", prms, connection, CommandType.StoredProcedure);
                                }
                                #endregion
                            }
                        }
                        else
                        {
                            FinalApprove(module, moduleApproval, userID, out identityTemp, connection);

                            if (moduleApproval.PK_ModuleAction_ID != (int)EActionType.Delete)
                            {
                                #region Save Master Module Detail
                                query = "SELECT * FROM ModuleDetailApproval WHERE FK_ModuleApproval_ID = @ModuleApprovalID";
                                prms = new SqlParameter[]
                                {
                                    new SqlParameter("@ModuleApprovalID", SqlDbType.Int){Value = pkModuleApprovalID}
                                };

                                DataTable drModuleDetailApproval = NawaDAO.ExecuteTable(query, prms, connection);
                                List<ModuleDetailApproval> moduleDetailApproval = NawaDAO.ConvertDataTable<ModuleDetailApproval>(drModuleDetailApproval);

                                foreach (ModuleDetailApproval data in moduleDetailApproval)
                                {
                                    if (data.FK_ModuleDetail_ID != null || data.FK_ModuleDetail_ID > 0)
                                    {
                                        query = "SELECT * FROM ModuleDetail WHERE PK_ModuleDetail_ID = @ModuleDetailID";
                                        prms = new SqlParameter[]
                                        {
                                            new SqlParameter("@ModuleDetailID", SqlDbType.VarChar) { Value = data.FK_ModuleDetail_ID }
                                        };
                                    }
                                    else
                                    {
                                        query = "SELECT * FROM ModuleDetail WHERE ModuleDetailName = @ModuleDetailName AND FK_Module_ID = @PK_Module_ID";
                                        prms = new SqlParameter[]
                                        {
                                            new SqlParameter("@ModuleDetailName", SqlDbType.VarChar) { Value = data.ModuleDetailName },
                                            new SqlParameter("@PK_Module_ID", SqlDbType.Int) { Value = module.PK_Module_ID }
                                        };
                                    }
                                    DataRow drModuleDetail = NawaDAO.ExecuteRow(query, prms, connection);
                                    ModuleDetail moduleDetail = NawaDAO.ConvertDataTable<ModuleDetail>(drModuleDetail);

                                    FinalApproveDetail(moduleDetail, data, userID, identityTemp);
                                }
                                #endregion
                            }

                            #region Notification
                            query = "SELECT * FROM MUser WHERE UserID = @userID";
                            prms = new SqlParameter[]
                            {
                                new SqlParameter("@userID", SqlDbType.VarChar) { Value = moduleApproval.CreatedBy }
                            };
                            DataRow drApprovalCreator = NawaDAO.ExecuteRow(query, prms, connection);

                            if (drApprovalCreator != null)
                            {
                                SystemDictionary sysDictionary = SystemParameterBLL.GetSystemDictionary("notification-approval-approved");
                                SystemDictionary sysDictionaryHeader = SystemParameterBLL.GetSystemDictionary("notification-approval-approved-header");

                                MNotification notif = new MNotification();
                                notif.FromUserID = pkMUserID;
                                notif.ToUserID = Convert.ToInt32(drApprovalCreator["PK_MUser_ID"]);
                                notif.NotificationHeader = sysDictionaryHeader.TextDefinition;
                                notif.NotificationBody = sysDictionary.TextDefinition;
                                notif.IsRead = false;
                                notif.DirectPage = "";
                                notif.FK_Module_ID = module.PK_Module_ID;
                                notif.FK_ModuleAction_ID = moduleApproval.PK_ModuleAction_ID;
                                notif.FK_ModuleApproval_ID = pkModuleApprovalID;
                                notif.Active = true;
                                notif.CreatedBy = userID;

                                NotificationBLL.InsertNotification(notif);
                            }
                            #endregion

                            #region Delete After Approve
                            prms = new SqlParameter[]
                            {
                                new SqlParameter("@ModuleApprovalID", SqlDbType.Int){Value = pkModuleApprovalID}
                            };

                            query = "DELETE ModuleApproval WHERE PK_ModuleApproval_ID = @ModuleApprovalID";
                            NawaDAO.ExecuteNonQuery(query, prms, connection);

                            query = "DELETE ModuleDetailApproval WHERE FK_ModuleApproval_ID = @ModuleApprovalID";
                            NawaDAO.ExecuteNonQuery(query, prms, connection);

                            SystemParameter sysParamUploadUser = SystemParameterBLL.GetSystemParameterValue(53);
                            string tableUploadUser = sysParamUploadUser != null ? sysParamUploadUser.SettingValue : "0";
                            string moduleNameUploadApproval = moduleName;

                            if (tableUploadUser == "1")
                            {
                                moduleNameUploadApproval += "_Upload_data_" + CommonBLL.StripTableName(userID) + "_approval";
                            }
                            else
                            {
                                moduleNameUploadApproval += "_Upload_approval";
                            }

                            query = @"  IF OBJECT_ID(@TableUploadName, 'U') IS NOT NULL
                                        BEGIN
	                                        DELETE " + moduleNameUploadApproval + @" WHERE FK_ModuleApproval_ID = @ModuleApprovalID
                                        END";
                            prms = new SqlParameter[]
                            {
                                new SqlParameter("@TableUploadName", SqlDbType.VarChar) { Value = moduleNameUploadApproval },
                                new SqlParameter("@ModuleApprovalID", SqlDbType.BigInt) { Value = pkModuleApprovalID }
                            };
                            NawaDAO.ExecuteNonQuery(query, prms, connection);
                            #endregion

                            #region Audit Trail
                            long headerID = AuditTrailBLL.InputAuditTrailHeader(connection, moduleApproval.CreatedBy, moduleName, (EActionType)moduleApproval.PK_ModuleAction_ID, EAuditTrailStatus.AffectedDatabase, pkModuleApprovalID, userID);

                            if (moduleApproval.PK_ModuleAction_ID != (int)EActionType.Import)
                            {
                                List<Dictionary<string, object>> newData = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(moduleApproval.ModuleField);
                                List<Dictionary<string, object>> oldData = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(moduleApproval.ModuleFieldBefore);
                                AuditTrailBLL.InputAuditTrailDetail(connection, headerID, (EActionType)moduleApproval.PK_ModuleAction_ID, newData, oldData);
                            }
                            #endregion

                            #region Send Email
                            query = "SELECT * FROM EmailTemplate WHERE EmailTemplateName = @TemplateName";
                            prms = new SqlParameter[]
                            {
                                    new SqlParameter("@TemplateName", SqlDbType.VarChar){ Value = "Email Template Approve Message" }
                            };
                            DataRow drTemplate = NawaDAO.ExecuteRow(query, prms);

                            if (drTemplate != null)
                            {
                                EmailTemplate template = NawaDAO.ConvertDataTable<EmailTemplate>(drTemplate);

                                prms = new SqlParameter[]
                                {
                                        new SqlParameter("@PK_EmailTemplate_ID", SqlDbType.BigInt){ Value = template.PK_EmailTemplate_ID },
                                        new SqlParameter("@recordID", SqlDbType.VarChar){ Value = Convert.ToString(headerID) },
                                        new SqlParameter("@EmailIDReference", SqlDbType.VarChar){ Value = "0" },
                                };

                                NawaDAO.ExecuteNonQuery("usp_EMailScheduler_Create", prms, connection, CommandType.StoredProcedure);
                            }
                            #endregion
                        }

                        connection.Commit();
                        connection.Dispose();
                    }
                    else
                    {
                        throw new Exception("Module data was not found");
                    }
                }
                else
                {
                    throw new Exception("ModuleApproval data was not found");
                }
            }
            catch
            {
                connection.Rollback();
                connection.Dispose();

                throw;
            }
        }


        private void FinalApproveDetail(ModuleDetail moduleDetail, ModuleDetailApproval moduleDetailApproval, string userID, object scopeIdentity)
        {
            string query = "SELECT * FROM ModuleDetailField WHERE FK_ModuleDetail_ID = @ModuleDetailID";
            SqlParameter[] prms = new SqlParameter[]
            {
                new SqlParameter("@ModuleDetailID", SqlDbType.Int) { Value = moduleDetail.PK_ModuleDetail_ID }
            };

            DataTable dtFields = NawaDAO.ExecuteTable(query, prms);
            List<SqlParameter> listParams = new List<SqlParameter>();

            if (moduleDetailApproval.PK_ModuleAction_ID == (int)EActionType.Insert)
            {
                Dictionary<string, object>[] newData = JsonConvert.DeserializeObject<Dictionary<string, object>[]>(moduleDetailApproval.ModuleDetailField);

                string tableFields = "";
                string insertParams = "";
                foreach (DataRow drField in dtFields.Rows)
                {
                    if (!Convert.ToBoolean(drField["IsPrimaryKey"]))
                    {
                        if (Convert.ToInt32(drField["FK_FieldType_ID"]) == (int)EFieldType.ReferenceTable)
                        {
                            if (!String.IsNullOrEmpty(tableFields)) tableFields += ", ";
                            tableFields += drField["FieldName"];

                            if (!String.IsNullOrEmpty(insertParams)) insertParams += ", ";
                            insertParams += "@" + drField["FieldName"];

                            string value = Convert.ToString(newData[0][Convert.ToString(drField["FieldName"])]);
                            string valueFormatted = "";
                            if (value == "")
                            {
                                valueFormatted = "";
                            }
                            else
                            {
                                valueFormatted = value.Split('(')[1].Split(')')[0];
                            }

                            listParams.Add(GetSqlParameterRaw(Convert.ToString(drField["FieldName"]), Convert.ToInt32(drField["FK_FieldType_ID"]), valueFormatted));
                            continue;
                        }

                        if (Convert.ToInt32(drField["FK_FieldType_ID"]) == (int)EFieldType.ParentReference)
                        {
                            if (!String.IsNullOrEmpty(tableFields)) tableFields += ", ";
                            tableFields += drField["FieldName"];

                            if (!String.IsNullOrEmpty(insertParams)) insertParams += ", ";
                            insertParams += "@" + drField["FieldName"];

                            listParams.Add(new SqlParameter("@" + Convert.ToString(drField["FieldName"]), SqlDbType.Int) { Value = Convert.ToInt32(scopeIdentity) });
                            continue;
                        }
                        if (Convert.ToInt32(drField["FK_FieldType_ID"]) == 14)
                        {
                            #region Insert UploadFile FileName
                            string FieldFileName = Convert.ToString(drField["FieldName"]) + "Name";

                            if (!String.IsNullOrEmpty(tableFields)) tableFields += ", ";
                            tableFields += "[" + FieldFileName + "]";

                            if (!String.IsNullOrEmpty(insertParams)) insertParams += ", ";
                            insertParams += "@" + FieldFileName;

                            listParams.Add(new SqlParameter("@" + FieldFileName, SqlDbType.VarChar) { Value = newData[0][FieldFileName] });
                            listParams.Add(GetSqlParameterRaw(Convert.ToString(drField["FieldName"]), Convert.ToInt32(drField["FK_FieldType_ID"]), Convert.FromBase64String(Convert.ToString(newData[0][Convert.ToString(drField["FieldName"])]))));
                            continue;
                            #endregion
                        }

                        if (!String.IsNullOrEmpty(tableFields)) tableFields += ", ";
                        tableFields += drField["FieldName"];

                        if (!String.IsNullOrEmpty(insertParams)) insertParams += ", ";
                        insertParams += "@" + drField["FieldName"];

                        listParams.Add(GetSqlParameterRaw(Convert.ToString(drField["FieldName"]), Convert.ToInt32(drField["FK_FieldType_ID"]), newData[0][Convert.ToString(drField["FieldName"])]));
                    }
                }

                if (!String.IsNullOrEmpty(tableFields)) tableFields += ", ";
                tableFields += "Active, CreatedBy, LastUpdateBy, ApprovedBy, CreatedDate, LastUpdateDate, ApprovedDate";
                if (!String.IsNullOrEmpty(insertParams)) insertParams += ", ";
                insertParams += "@Active, @CreatedBy, @LastUpdateBy, @ApprovedBy, GETDATE(), GETDATE(), GETDATE()";

                listParams.Add(new SqlParameter("@Active", SqlDbType.Bit) { Value = true });
                listParams.Add(new SqlParameter("@CreatedBy", SqlDbType.VarChar) { Value = moduleDetailApproval.CreatedBy });
                listParams.Add(new SqlParameter("@LastUpdateBy", SqlDbType.VarChar) { Value = moduleDetailApproval.CreatedBy });
                listParams.Add(new SqlParameter("@ApprovedBy", SqlDbType.VarChar) { Value = userID });

                query = "INSERT INTO dbo." + moduleDetail.ModuleDetailName + " (" + tableFields + ") " + Environment.NewLine;
                query += "VALUES (" + insertParams + ")";

                int rowAffected = NawaDAO.ExecuteNonQuery(query, listParams.ToArray());
            }
            else if (moduleDetailApproval.PK_ModuleAction_ID == (int)EActionType.Update)
            {
                Dictionary<string, object>[] newData = JsonConvert.DeserializeObject<Dictionary<string, object>[]>(moduleDetailApproval.ModuleDetailField);

                string updateFields = "";
                string updateKey = "";
                foreach (DataRow drField in dtFields.Rows)
                {
                    if (Convert.ToBoolean(drField["IsPrimaryKey"]))
                    {
                        updateKey = drField["FieldName"] + " = @" + drField["FieldName"];
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(updateFields)) updateFields += ", " + Environment.NewLine;
                        updateFields += drField["FieldName"] + " = @" + drField["FieldName"];
                    }

                    if (Convert.ToInt32(drField["FK_FieldType_ID"]) == 14)
                    {
                        #region Update FileUpload FileName
                        string FieldFileName = Convert.ToString(drField["FieldName"]) + "Name";

                        if (!String.IsNullOrEmpty(updateFields)) updateFields += ", " + Environment.NewLine;
                        updateFields += FieldFileName + " = @" + FieldFileName;

                        listParams.Add(new SqlParameter("@" + FieldFileName, SqlDbType.VarChar) { Value = newData[0][FieldFileName] });

                        listParams.Add(GetSqlParameterRaw(Convert.ToString(drField["FieldName"]), Convert.ToInt32(drField["FK_FieldType_ID"]), Convert.FromBase64String(Convert.ToString(newData[0][Convert.ToString(drField["FieldName"])]))));
                        continue;
                        #endregion
                    }

                    if (Convert.ToInt32(drField["FK_FieldType_ID"]) == (int)EFieldType.ReferenceTable)
                    {
                        string value = Convert.ToString(newData[0][Convert.ToString(drField["FieldName"])]);
                        string valueFormatted = "";
                        if (value == "")
                        {
                            valueFormatted = "";
                        }
                        else
                        {
                            valueFormatted = value.Split('(')[1].Split(')')[0];
                        }

                        listParams.Add(GetSqlParameterRaw(Convert.ToString(drField["FieldName"]), Convert.ToInt32(drField["FK_FieldType_ID"]), valueFormatted));
                        continue;
                    }
                    else
                    {
                        listParams.Add(GetSqlParameterRaw(Convert.ToString(drField["FieldName"]), Convert.ToInt32(drField["FK_FieldType_ID"]), newData[0][Convert.ToString(drField["FieldName"])]));
                    }
                }

                if (!String.IsNullOrEmpty(updateFields)) updateFields += ", " + Environment.NewLine;

                updateFields += "LastUpdateBy = @LastUpdateBy, " + Environment.NewLine;
                updateFields += "LastUpdateDate = GETDATE() " + Environment.NewLine;
                updateFields += "ApprovedBy = @ApprovedBy, " + Environment.NewLine;
                updateFields += "ApprovedDate = GETDATE() ";

                listParams.Add(new SqlParameter("@LastUpdateBy", SqlDbType.VarChar) { Value = moduleDetailApproval.CreatedBy });
                listParams.Add(new SqlParameter("@ApprovedBy", SqlDbType.VarChar) { Value = userID });

                query = "UPDATE dbo." + moduleDetail.ModuleDetailName + Environment.NewLine;
                query += "SET " + updateFields + Environment.NewLine;
                query += "WHERE " + updateKey + Environment.NewLine;

                int rowAffected = NawaDAO.ExecuteNonQuery(query, listParams.ToArray());
            }
            else if (moduleDetailApproval.PK_ModuleAction_ID == (int)EActionType.Delete)
            {
                Dictionary<string, object>[] oldData = JsonConvert.DeserializeObject<Dictionary<string, object>[]>(moduleDetailApproval.ModuleDetailFieldBefore);

                query = "SELECT FieldName FROM ModuleDetailField WHERE FK_ModuleDetail_ID = @ModuleDetailID AND IsPrimaryKey = 1";
                prms = new SqlParameter[]
                {
                    new SqlParameter("@ModuleDetailID", SqlDbType.Int){ Value = moduleDetail.PK_ModuleDetail_ID }
                };

                string FieldID = Convert.ToString(NawaDAO.ExecuteScalar(query, prms));

                query = "DELETE dbo." + moduleDetail.ModuleDetailName + " WHERE " + FieldID + " = @IDData";
                prms = new SqlParameter[]
                {
                    new SqlParameter("@IDData", SqlDbType.Int){ Value = oldData[0][FieldID] }
                };

                int rowAffected = NawaDAO.ExecuteNonQuery(query, prms);
            }
        }


        public void RejectModuleApproval(long pkModuleApprovalID, int pkMUserID, string review, string userID)
        {
            NawaConnection connection = NawaDAO.CreateConnection();
            connection.BeginTransaction();

            try
            {
                string query = "SELECT * FROM ModuleApproval WHERE PK_ModuleApproval_ID = " + pkModuleApprovalID;
                DataRow drModuleApproval = NawaDAO.ExecuteRow(query);

                if (drModuleApproval != null)
                {
                    ModuleApproval moduleApproval = NawaDAO.ConvertDataTable<ModuleApproval>(drModuleApproval);
                    SqlParameter[] prms = new SqlParameter[] { };

                    if (moduleApproval.FK_Module_ID > 0)
                    {
                        query = "SELECT * FROM Module WHERE PK_Module_ID = @ModuleID";
                        prms = new SqlParameter[]
                        {
                            new SqlParameter("@ModuleID", SqlDbType.VarChar) { Value = moduleApproval.FK_Module_ID }
                        };
                    }
                    else
                    {
                        query = "SELECT * FROM Module WHERE ModuleName = @ModuleName";
                        prms = new SqlParameter[]
                        {
                            new SqlParameter("@ModuleName", SqlDbType.VarChar) { Value = moduleApproval.ModuleName }
                        };
                    }

                    DataRow drModule = NawaDAO.ExecuteRow(query, prms);

                    if (drModule != null)
                    {
                        Module module = NawaDAO.ConvertDataTable<Module>(drModule);
                        prms = new SqlParameter[]
                        {
                            new SqlParameter("@PkMOduleid", SqlDbType.Int) { Value = module.PK_Module_ID },
                            new SqlParameter("@pkunikid", SqlDbType.VarChar) { Value = (moduleApproval.PK_ModuleAction_ID == (int)EActionType.Import ? moduleApproval.PK_ModuleApproval_ID + "import" : moduleApproval.ModuleKey) }
                        };
                        int workflowInprogress = Convert.ToInt32(NawaDAO.ExecuteScalar("IsHaveWorkflowApproval", prms, null, CommandType.StoredProcedure));

                        if (workflowInprogress > 0)
                        {
                            prms = new SqlParameter[]
                            {
                                new SqlParameter("@FK_ModuleApproval_ID", SqlDbType.BigInt) { Value = pkModuleApprovalID },
                                new SqlParameter("@pkmoduleid", SqlDbType.Int) { Value = module.PK_Module_ID },
                                new SqlParameter("@pkunik", SqlDbType.VarChar) { Value = (moduleApproval.PK_ModuleAction_ID == (int)EActionType.Import ? moduleApproval.PK_ModuleApproval_ID + "import" : moduleApproval.ModuleKey) },
                                new SqlParameter("@pkuseridexecute", SqlDbType.Int) { Value = pkMUserID },
                                new SqlParameter("@notes", SqlDbType.VarChar) { Value = review },
                                new SqlParameter("@intworkflowstatus", SqlDbType.Int) { Value = (int)EActionApprovalType.Approve }
                            };

                            NawaDAO.ExecuteNonQuery("usp_SaveWorkflowModuleDesigner", prms, null, CommandType.StoredProcedure);
                        }

                        #region Notification
                        query = "SELECT * FROM MUser WHERE UserID = @userID";
                        prms = new SqlParameter[]
                        {
                            new SqlParameter("@userID", SqlDbType.VarChar) { Value = moduleApproval.CreatedBy }
                        };
                        DataRow drApprovalCreator = NawaDAO.ExecuteRow(query, prms, connection);

                        if (drApprovalCreator != null)
                        {
                            SystemDictionary sysDictionary = SystemParameterBLL.GetSystemDictionary("notification-approval-rejected");
                            SystemDictionary sysDictionaryHeader = SystemParameterBLL.GetSystemDictionary("notification-approval-rejected-header");

                            MNotification notif = new MNotification();
                            notif.FromUserID = pkMUserID;
                            notif.ToUserID = Convert.ToInt32(drApprovalCreator["PK_MUser_ID"]);
                            notif.NotificationHeader = sysDictionaryHeader.TextDefinition;
                            notif.NotificationBody = sysDictionary.TextDefinition;
                            notif.IsRead = false;
                            notif.DirectPage = "";
                            notif.FK_Module_ID = module.PK_Module_ID;
                            notif.FK_ModuleAction_ID = moduleApproval.PK_ModuleAction_ID;
                            notif.FK_ModuleApproval_ID = pkModuleApprovalID;
                            notif.Active = true;
                            notif.CreatedBy = userID;

                            NotificationBLL.InsertNotification(notif);
                        }
                        #endregion

                        #region Delete After Approve
                        prms = new SqlParameter[]
                        {
                            new SqlParameter("@PK_ModuleApproval_ID", SqlDbType.BigInt) { Value = pkModuleApprovalID }
                        };

                        query = "DELETE ModuleApproval WHERE PK_ModuleApproval_ID = @PK_ModuleApproval_ID";
                        NawaDAO.ExecuteNonQuery(query, prms, connection);

                        query = "DELETE ModuleDetailApproval WHERE FK_ModuleApproval_ID = @PK_ModuleApproval_ID";
                        NawaDAO.ExecuteNonQuery(query, prms, connection);

                        SystemParameter sysParamUploadUser = SystemParameterBLL.GetSystemParameterValue(53);
                        string tableUploadUser = sysParamUploadUser != null ? sysParamUploadUser.SettingValue : "0";
                        string moduleNameUploadApproval = moduleName;

                        if (tableUploadUser == "1")
                        {
                            moduleNameUploadApproval += "_Upload_data_" + CommonBLL.StripTableName(userID) + "_approval";
                        }
                        else
                        {
                            moduleNameUploadApproval += "_Upload_approval";
                        }

                        query = @"  IF OBJECT_ID(@TableUploadName, 'U') IS NOT NULL
                                    BEGIN
	                                    DELETE " + moduleNameUploadApproval + @" WHERE FK_ModuleApproval_ID = @ModuleApprovalID
                                    END";
                        prms = new SqlParameter[]
                        {
                            new SqlParameter("@TableUploadName", SqlDbType.VarChar) { Value = moduleNameUploadApproval },
                            new SqlParameter("@ModuleApprovalID", SqlDbType.BigInt) { Value = pkModuleApprovalID }
                        };
                        NawaDAO.ExecuteNonQuery(query, prms, connection);
                        #endregion

                        #region Audit Trail
                        long headerID = AuditTrailBLL.InputAuditTrailHeader(connection, moduleApproval.CreatedBy, moduleName, (EActionType)moduleApproval.PK_ModuleAction_ID, EAuditTrailStatus.Rejected, pkModuleApprovalID, userID);

                        if (moduleApproval.PK_ModuleAction_ID != (int)EActionType.Import)
                        {
                            List<Dictionary<string, object>> newData = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(moduleApproval.ModuleField);
                            List<Dictionary<string, object>> oldData = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(moduleApproval.ModuleFieldBefore);
                            AuditTrailBLL.InputAuditTrailDetail(connection, headerID, (EActionType)moduleApproval.PK_ModuleAction_ID, newData, oldData);
                        }
                        #endregion

                        #region Send Email
                        query = "SELECT * FROM EmailTemplate WHERE EmailTemplateName = @TemplateName";
                        prms = new SqlParameter[]
                        {
                            new SqlParameter("@TemplateName", SqlDbType.VarChar){ Value = "Email Template Rejected Message" }
                        };
                        DataRow drTemplate = NawaDAO.ExecuteRow(query, prms);

                        if (drTemplate != null)
                        {
                            EmailTemplate template = NawaDAO.ConvertDataTable<EmailTemplate>(drTemplate);

                            prms = new SqlParameter[]
                            {
                                new SqlParameter("@PK_EmailTemplate_ID", SqlDbType.BigInt){ Value = template.PK_EmailTemplate_ID },
                                new SqlParameter("@recordID", SqlDbType.VarChar){ Value = Convert.ToString(headerID) },
                                new SqlParameter("@EmailIDReference", SqlDbType.VarChar){ Value = "0" },
                            };

                            NawaDAO.ExecuteNonQuery("usp_EMailScheduler_Create", prms, connection, CommandType.StoredProcedure);
                        }
                        #endregion

                        connection.Commit();
                        connection.Dispose();
                    }
                    else
                    {
                        throw new Exception("Module data was not found");
                    }
                }
                else
                {
                    throw new Exception("ModuleApproval data was not found");
                }
            }
            catch
            {
                connection.Rollback();
                connection.Dispose();

                throw;
            }
        }

        public MemoryStream ExportModuleData(int ModuleID, int page, string orderBy, string order, string search, string filter, bool isAdv, string userID, int FK_MRole_ID)
        {
            try
            {
                string query = "SELECT * FROM Module WHERE PK_Module_ID = @ModuleID";
                SqlParameter[] prms = new SqlParameter[]
                {
                    new SqlParameter("@ModuleID", SqlDbType.Int){ Value = ModuleID }
                };
                DataRow dr = NawaDAO.ExecuteRow(query, prms);
                MemoryStream memoryStreamOutput = null;

                if (dr != null)
                {
                    //string moduleName = Convert.ToString(dr["ModuleName"]);
                    //query = "SELECT * FROM ModuleField WHERE FK_Module_ID = @ModuleID AND IsShowInView = 1";
                    //prms = new SqlParameter[]
                    //{
                    //    new SqlParameter("@ModuleID", SqlDbType.Int){ Value = ModuleID }
                    //};
                    //DataTable dtFields = NawaDAO.ExecuteTable(query, prms);
                    string sFields = "";
                    string moduleJoin = "";
                    string searchQuery = "";
                    string outerModuleColumns = "";
                    string advQuery = "";

                    #region Fill Data Query
                    List<string> referenceAliasHistory = new List<string>();
                    SystemParameter sysParamFormatDate = SystemParameterBLL.GetSystemParameterValue(6);
                    //string dateFormat = sysParamFormatDate != null ? sysParamFormatDate.SettingValue : "yyyy-MM-dd";
                    string dateFormat = "dd MMMM yyyy HH:mm:ss";

                    foreach (ModuleField moduleField in moduleFields.Where(item => item.IsShowInView).ToList())
                    {
                        if (!string.IsNullOrEmpty(sFields))
                        {
                            sFields += ", ";
                        }

                        if (!string.IsNullOrEmpty(outerModuleColumns))
                        {
                            outerModuleColumns += ", ";
                        }
                        outerModuleColumns += moduleField.FieldName;

                        int extType = Convert.ToInt32(moduleField.FK_ExtType_ID);
                        int fieldType = Convert.ToInt32(moduleField.FK_FieldType_ID);

                        if (extType == (int)EControlType.PopUpMultiCheckBox)
                        {
                            // get delimiter and fill query                 
                            string delim = SystemParameterBLL.GetSystemParameterGroup(39).SettingValue;
                            string multiCheckBoxLogic = "(" + "\n" +
                                            "\t" + "STUFF(" + "\n" +
                                            "\t\t" + $"(SELECT '{delim}(' + A.val + ') - ' + B.{moduleField.TableReferenceFieldDisplayName}" + "\n" +
                                            "\t\t" + $"FROM dbo.Split({moduleName}.{moduleField.FieldName}, '{delim}') AS A" + "\n" +
                                            "\t\t\t" + $"LEFT JOIN {moduleField.TabelReferenceName} AS B" + "\n" +
                                            "\t\t\t" + $"ON A.val = B.{moduleField.TableReferenceFieldKey}" + "\n" +
                                            "\t\t" + "FOR XML PATH (''))" + "\n" +
                                            "\t" + ", 1, 1, '')" + "\n" +
                                            ")";
                            sFields += multiCheckBoxLogic + " AS " + moduleField.FieldName;

                            if (!string.IsNullOrEmpty(search))
                            {
                                if (string.IsNullOrEmpty(searchQuery))
                                {
                                    searchQuery = " WHERE ( " + multiCheckBoxLogic + " LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                                }
                                else
                                {
                                    searchQuery = searchQuery + " OR " + multiCheckBoxLogic + " LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                                }
                            }
                        }
                        else if (fieldType == (int)EFieldType.ReferenceTable && extType != (int)EControlType.PopUpMultiCheckBox)
                        {
                            // table reference alias handler, automaticlly generate new alias if found same name/alias
                            string referenceAlias = string.IsNullOrWhiteSpace(Convert.ToString(moduleField.TabelReferenceNameAlias)) ? Convert.ToString(moduleField.TabelReferenceName) : Convert.ToString(moduleField.TabelReferenceNameAlias);
                            bool duplicatedAlias = referenceAliasHistory.Any(pastAlias => pastAlias == referenceAlias);
                            if (duplicatedAlias == true)
                            {
                                referenceAlias = referenceAlias + "_" + referenceAliasHistory.Count;
                            }

                            moduleJoin += " LEFT JOIN " + moduleField.TabelReferenceName + " " + referenceAlias + " ON " + moduleName + "." + moduleField.FieldName + " = " + referenceAlias + "." + moduleField.TableReferenceFieldKey + (Convert.ToBoolean(moduleField.BCasCade) ? " AND " + Convert.ToString(moduleField.FilterCascade).Replace("@Parent", moduleName + "." + Convert.ToString(moduleField.FieldNameParent)) : "");
                            sFields += "'(' + CAST(" + moduleName + "." + moduleField.FieldName + " AS VARCHAR(MAX)) + ') - ' + CAST(" + referenceAlias + "." + moduleField.TableReferenceFieldDisplayName + " AS VARCHAR(MAX)) AS " + moduleField.FieldName;
                            referenceAliasHistory.Add(referenceAlias);

                            if (!string.IsNullOrEmpty(search))
                            {
                                if (string.IsNullOrEmpty(searchQuery))
                                {
                                    searchQuery = " WHERE ( ('(' + CAST(" + moduleName + "." + moduleField.FieldName + " AS VARCHAR(MAX)) + ') - ' + CAST(" + referenceAlias + "." + moduleField.TableReferenceFieldDisplayName + " AS VARCHAR(MAX))) LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                                }
                                else
                                {
                                    searchQuery = searchQuery + " OR ('(' + CAST(" + moduleName + "." + moduleField.FieldName + " AS VARCHAR(MAX)) + ') - ' + CAST(" + referenceAlias + "." + moduleField.TableReferenceFieldDisplayName + " AS VARCHAR(MAX))) LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                                }
                            }
                        }
                        else if (fieldType == (int)EFieldType.VarBinary && (extType == (int)EControlType.DisplayField || extType == (int)EControlType.FileUpload))
                        {
                            sFields += moduleName + "." + moduleField.FieldName + ", ";
                            sFields += moduleName + "." + moduleField.FieldName + "Name";

                            if (!string.IsNullOrEmpty(search))
                            {
                                if (string.IsNullOrEmpty(searchQuery))
                                {
                                    searchQuery = " WHERE ( CAST(" + moduleName + "." + Convert.ToString(moduleField.FieldName) + "Name AS VARCHAR(MAX)) LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                                }
                                else
                                {
                                    searchQuery = searchQuery + " OR CAST(" + moduleName + "." + Convert.ToString(moduleField.FieldName) + "Name AS VARCHAR(MAX)) LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                                }
                            }
                        }
                        else if (fieldType == (int)EFieldType.DATETIME)
                        {
                            sFields += "FORMAT(" + moduleName + "." + moduleField.FieldName + ", '" + dateFormat + "') AS " + moduleField.FieldName;

                            if (!string.IsNullOrEmpty(search))
                            {
                                if (string.IsNullOrEmpty(searchQuery))
                                {
                                    searchQuery = " WHERE ( FORMAT(" + moduleName + "." + Convert.ToString(moduleField.FieldName) + ", '" + dateFormat + "') LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                                }
                                else
                                {
                                    searchQuery = searchQuery + " OR FORMAT(" + moduleName + "." + Convert.ToString(moduleField.FieldName) + ", '" + dateFormat + "') LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                                }
                            }
                        }
                        else if (fieldType == (int)EFieldType.FLOAT || fieldType == (int)EFieldType.NUMERICDECIMAL ||
                                fieldType == (int)EFieldType.REAL || fieldType == (int)EFieldType.MONEY)
                        {
                            sFields += moduleName + "." + moduleField.FieldName;

                            if (!string.IsNullOrEmpty(search))
                            {
                                if (String.IsNullOrEmpty(searchQuery))
                                {
                                    searchQuery = " WHERE ( FORMAT(" + moduleName + "." + Convert.ToString(moduleField.FieldName) + ",'#.###############') LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                                }
                                else
                                {
                                    searchQuery = searchQuery + " OR FORMAT(" + moduleName + "." + Convert.ToString(moduleField.FieldName) + ",'#.###############') LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                                }
                            }
                        }
                        else
                        {
                            sFields += moduleName + "." + moduleField.FieldName;

                            if (!string.IsNullOrEmpty(search))
                            {
                                if (string.IsNullOrEmpty(searchQuery))
                                {
                                    searchQuery = " WHERE ( CAST(" + moduleName + "." + Convert.ToString(moduleField.FieldName) + " AS VARCHAR(MAX)) LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                                }
                                else
                                {
                                    searchQuery = searchQuery + " OR CAST(" + moduleName + "." + Convert.ToString(moduleField.FieldName) + " AS VARCHAR(MAX)) LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                                }
                            }
                        }
                    }
                    #endregion

                    if (!string.IsNullOrEmpty(filter) && string.IsNullOrEmpty(search))
                    {
                        if (isAdv)
                        {
                            advQuery = " WHERE( " + filter;
                        }
                        else
                        {
                            string[] filterArr = filter.Split(' ');
                            string restOfFilter = string.Join(" ", filterArr.Skip(1));

                            ModuleField column = moduleFields.Where(item => item.IsShowInView && item.FieldName == filterArr[0]).FirstOrDefault();
                            if (column is not null && filterArr[1] == "LIKE")
                            {
                                if (column.FK_FieldType_ID == (int)EFieldType.ReferenceTable)
                                {
                                    if (string.IsNullOrEmpty(column.TabelReferenceNameAlias))
                                    {
                                        searchQuery = " WHERE (('(' + CAST(" + moduleName + "." + filterArr[0]
                                            + " AS VARCHAR(MAX)) + ') - ' + CAST(" + column.TabelReferenceName + "."
                                            + column.TableReferenceFieldDisplayName + " AS VARCHAR(MAX))) " + restOfFilter;
                                    }
                                    else
                                    {
                                        searchQuery = " WHERE (('(' + CAST(" + moduleName + "." + filterArr[0]
                                            + " AS VARCHAR(MAX)) + ') - ' + CAST(" + column.TabelReferenceNameAlias + "."
                                            + column.TableReferenceFieldDisplayName + " AS VARCHAR(MAX))) " + restOfFilter;
                                    }

                                }
                                else if (column.FK_FieldType_ID == (int)EFieldType.FLOAT || column.FK_FieldType_ID == (int)EFieldType.NUMERICDECIMAL ||
                                    column.FK_FieldType_ID == (int)EFieldType.REAL || column.FK_FieldType_ID == (int)EFieldType.MONEY)
                                {
                                    searchQuery = " WHERE (FORMAT(" + moduleName + "." + filterArr[0]
                                        + ",'#.###############') " + restOfFilter;
                                }
                                else if (column.FK_FieldType_ID == (int)EFieldType.DATETIME)
                                {
                                    searchQuery = " WHERE (FORMAT(" + moduleName + "." + filterArr[0]
                                        + ", '" + dateFormat + "') " + restOfFilter;
                                }
                                else
                                {
                                    searchQuery = " WHERE (" + moduleName + "." + filter;
                                }
                            }
                            else
                            {
                                if (filterArr[0] == "CreatedDate" || filterArr[0] == "LastUpdateDate" ||
                                    filterArr[0] == "ApprovedDate")
                                {
                                    searchQuery = " WHERE (FORMAT(" + moduleName + "." + filterArr[0]
                                        + ", '" + dateFormat + "') " + restOfFilter;
                                }
                                else
                                {
                                    searchQuery = " WHERE (" + moduleName + "." + filter;
                                }
                            }
                        }
                    }

                    #region show last update
                    AppSystemParameter createdBySysParam = SystemParameterBLL.GetAppSystemParameter(43);
                    string createdBy = createdBySysParam != null ? createdBySysParam.SettingValue : "0";

                    AppSystemParameter lastUpdateBySysParam = SystemParameterBLL.GetAppSystemParameter(44);
                    string lastUpdateBy = lastUpdateBySysParam != null ? lastUpdateBySysParam.SettingValue : "0";

                    AppSystemParameter approvedBySysParam = SystemParameterBLL.GetAppSystemParameter(45);
                    string approvedBy = approvedBySysParam != null ? approvedBySysParam.SettingValue : "0";

                    AppSystemParameter createdDateSysParam = SystemParameterBLL.GetAppSystemParameter(46);
                    string createdDate = createdDateSysParam != null ? createdDateSysParam.SettingValue : "0";

                    AppSystemParameter lastUpdateDateSysParam = SystemParameterBLL.GetAppSystemParameter(47);
                    string lastUpdateDate = lastUpdateDateSysParam != null ? lastUpdateDateSysParam.SettingValue : "0";

                    AppSystemParameter approvedDateSysParam = SystemParameterBLL.GetAppSystemParameter(48);
                    string approvedDate = approvedDateSysParam != null ? approvedDateSysParam.SettingValue : "0";

                    if (createdBy == "1")
                    {
                        if (!string.IsNullOrEmpty(sFields))
                        {
                            sFields = sFields + ", ";
                        }

                        sFields += moduleName + ".CreatedBy";

                        if (!string.IsNullOrEmpty(outerModuleColumns))
                        {
                            outerModuleColumns = outerModuleColumns + ", ";
                        }

                        outerModuleColumns += " CreatedBy";

                        if (!string.IsNullOrEmpty(search))
                        {
                            if (string.IsNullOrEmpty(searchQuery))
                            {
                                searchQuery = " WHERE ( " + moduleName + ".CreatedBy LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                            }
                            else
                            {
                                searchQuery = searchQuery + " OR " + moduleName + ".CreatedBy LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                            }
                        }
                    }

                    if (lastUpdateBy == "1")
                    {
                        if (!string.IsNullOrEmpty(sFields))
                        {
                            sFields = sFields + ", ";
                        }

                        sFields += moduleName + ".LastUpdateBy";


                        if (!string.IsNullOrEmpty(outerModuleColumns))
                        {
                            outerModuleColumns = outerModuleColumns + ", ";
                        }

                        outerModuleColumns += " LastUpdateBy";

                        if (!string.IsNullOrEmpty(search))
                        {
                            if (string.IsNullOrEmpty(searchQuery))
                            {
                                searchQuery = " WHERE ( " + moduleName + ".LastUpdateBy LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                            }
                            else
                            {
                                searchQuery = searchQuery + " OR " + moduleName + ".LastUpdateBy LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                            }
                        }
                    }

                    if (approvedBy == "1")
                    {
                        if (!string.IsNullOrEmpty(sFields))
                        {
                            sFields = sFields + ", ";
                        }

                        sFields += moduleName + ".ApprovedBy";

                        if (!string.IsNullOrEmpty(outerModuleColumns))
                        {
                            outerModuleColumns = outerModuleColumns + ", ";
                        }

                        outerModuleColumns += " ApprovedBy";

                        if (!string.IsNullOrEmpty(search))
                        {
                            if (string.IsNullOrEmpty(searchQuery))
                            {
                                searchQuery = " WHERE ( " + moduleName + ".ApprovedBy LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                            }
                            else
                            {
                                searchQuery = searchQuery + " OR " + moduleName + ".ApprovedBy LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                            }
                        }
                    }

                    if (createdDate == "1")
                    {
                        if (!string.IsNullOrEmpty(sFields))
                        {
                            sFields = sFields + ", ";
                        }

                        sFields += "FORMAT(" + moduleName + ".CreatedDate, '" + dateFormat + "') AS CreatedDate";

                        if (!string.IsNullOrEmpty(outerModuleColumns))
                        {
                            outerModuleColumns = outerModuleColumns + ", ";
                        }

                        outerModuleColumns += " CreatedDate";

                        if (!string.IsNullOrEmpty(search))
                        {
                            if (string.IsNullOrEmpty(searchQuery))
                            {
                                searchQuery = " WHERE ( FORMAT(" + moduleName + ".CreatedDate, '" + dateFormat + "') LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                            }
                            else
                            {
                                searchQuery = searchQuery + " OR FORMAT(" + moduleName + ".CreatedDate, '" + dateFormat + "') LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                            }
                        }
                    }

                    if (lastUpdateDate == "1")
                    {
                        if (!string.IsNullOrEmpty(sFields))
                        {
                            sFields = sFields + ", ";
                        }

                        sFields += "FORMAT(" + moduleName + ".LastUpdateDate, '" + dateFormat + "') AS LastUpdateDate";

                        if (!string.IsNullOrEmpty(outerModuleColumns))
                        {
                            outerModuleColumns = outerModuleColumns + ", ";
                        }

                        outerModuleColumns += " LastUpdateDate";

                        if (!string.IsNullOrEmpty(search))
                        {
                            if (string.IsNullOrEmpty(searchQuery))
                            {
                                searchQuery = " WHERE ( FORMAT(" + moduleName + ".LastUpdateDate, '" + dateFormat + "') LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                            }
                            else
                            {
                                searchQuery = searchQuery + " OR FORMAT(" + moduleName + ".LastUpdateDate, '" + dateFormat + "') LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                            }
                        }
                    }

                    if (approvedDate == "1")
                    {
                        if (!string.IsNullOrEmpty(sFields))
                        {
                            sFields = sFields + ", ";
                        }

                        sFields += "FORMAT(" + moduleName + ".ApprovedDate, '" + dateFormat + "') AS ApprovedDate";

                        if (!string.IsNullOrEmpty(outerModuleColumns))
                        {
                            outerModuleColumns = outerModuleColumns + ", ";
                        }

                        outerModuleColumns += " ApprovedDate";

                        if (!string.IsNullOrEmpty(search))
                        {
                            if (string.IsNullOrEmpty(searchQuery))
                            {
                                searchQuery = " WHERE ( FORMAT(" + moduleName + ".ApprovedDate, '" + dateFormat + "') LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                            }
                            else
                            {
                                searchQuery = searchQuery + " OR FORMAT(" + moduleName + ".ApprovedDate, '" + dateFormat + "') LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                            }
                        }
                    }
                    #endregion

                    if (!string.IsNullOrEmpty(searchQuery))
                    {
                        searchQuery = searchQuery + " )";
                    }

                    if (!string.IsNullOrEmpty(advQuery))
                    {
                        advQuery = advQuery + " )";
                    }

                    string queryDataAccess = "SELECT * From DataAccess Where FK_Module_ID = @ModuleID AND FK_Role_ID = @RoleID";
                    prms = new SqlParameter[]
                    {
                        new SqlParameter("@ModuleID", SqlDbType.Int){ Value = ModuleID },
                        new SqlParameter("@RoleID", SqlDbType.Int){ Value = FK_MRole_ID }
                    };
                    DataTable dtDataAccess = NawaDAO.ExecuteTable(queryDataAccess, prms);

                    if (dtDataAccess != null && dtDataAccess.Rows.Count > 0)
                    {
                        string filterData = "";

                        foreach (DataRow drDataAccess in dtDataAccess.Rows)
                        {
                            filterData += " AND " + Convert.ToString(drDataAccess["FilterData"]).ToLower().Replace("@userid", userID);
                        }

                        string filterDataFinal = filterData.Remove(0, 5);

                        if (string.IsNullOrEmpty(searchQuery))
                        {
                            searchQuery = " WHERE (" + filterDataFinal + ")";
                        }
                        else
                        {
                            searchQuery = searchQuery + " AND (" + filterDataFinal + ")";
                        }
                    }

                    int pageSize = Convert.ToInt32(SystemParameterBLL.GetAppSystemParameter(7).SettingValue);
                    query = "EXEC usp_PagingNew @Query, @OrderBy, @PageIndex, @PageSize";
                    prms = new SqlParameter[]
                    {
                        new SqlParameter("@Query", SqlDbType.VarChar){ Value = "SELECT " + outerModuleColumns + " FROM ( SELECT "  + sFields + " FROM " + moduleName + moduleJoin + searchQuery + ") X " + advQuery},
                        new SqlParameter("@OrderBy", SqlDbType.VarChar){ Value = (!string.IsNullOrEmpty(orderBy) ? orderBy + " " + order : "") },
                        new SqlParameter("@PageIndex", SqlDbType.Int){ Value = page },
                        new SqlParameter("@PageSize", SqlDbType.Int){ Value = pageSize }
                    };
                    DataTable dt = NawaDAO.ExecuteTable(query, prms);
                    memoryStreamOutput = NawaDAO.ConvertToExcel(dt);
                }

                return memoryStreamOutput;
            }
            catch
            {
                throw;
            }
        }

        public MemoryStream ExportModuleDataAll(int ModuleID, string orderBy, string order, string search, string filter, bool isAdv, string userID, int FK_MRole_ID)
        {
            try
            {
                string query = "SELECT * FROM Module WHERE PK_Module_ID = @ModuleID";
                SqlParameter[] prms = new SqlParameter[]
                {
                    new SqlParameter("@ModuleID", SqlDbType.Int){ Value = ModuleID }
                };
                DataRow dr = NawaDAO.ExecuteRow(query, prms);
                MemoryStream memoryStreamOutput = null;

                if (dr != null)
                {
                    string sFields = "";
                    string moduleJoin = "";
                    string searchQuery = "";
                    //string moduleName = Convert.ToString(dr["ModuleName"]);
                    string outerModuleColumns = "";
                    string advQuery = "";

                    //query = "SELECT * FROM ModuleField WHERE FK_Module_ID = @ModuleID AND IsShowInView = 1";
                    //prms = new SqlParameter[]
                    //{
                    //    new SqlParameter("@ModuleID", SqlDbType.Int){ Value = ModuleID }
                    //};
                    //DataTable dtFields = NawaDAO.ExecuteTable(query, prms);

                    #region Fill Data Query
                    List<string> referenceAliasHistory = new List<string>();
                    SystemParameter sysParamFormatDate = SystemParameterBLL.GetSystemParameterValue(6);
                    //string dateFormat = sysParamFormatDate != null ? sysParamFormatDate.SettingValue : "yyyy-MM-dd";
                    string dateFormat = "dd MMMM yyyy HH:mm:ss";

                    foreach (ModuleField moduleField in moduleFields.Where(item => item.IsShowInView).ToList())
                    {
                        if (!string.IsNullOrEmpty(sFields))
                        {
                            sFields += ", ";
                        }
                        if (!String.IsNullOrEmpty(outerModuleColumns))
                        {
                            outerModuleColumns += ", ";
                        }
                        outerModuleColumns += moduleField.FieldName;

                        int extType = Convert.ToInt32(moduleField.FK_ExtType_ID);
                        int fieldType = Convert.ToInt32(moduleField.FK_FieldType_ID);

                        if (extType == (int)EControlType.PopUpMultiCheckBox)
                        {
                            // get delimiter and fill query                 
                            string delim = SystemParameterBLL.GetSystemParameterGroup(39).SettingValue;
                            string multiCheckBoxLogic = "(" + "\n" +
                                            "\t" + "STUFF(" + "\n" +
                                            "\t\t" + $"(SELECT '{delim}(' + A.val + ') - ' + B.{moduleField.TableReferenceFieldDisplayName}" + "\n" +
                                            "\t\t" + $"FROM dbo.Split({moduleName}.{moduleField.FieldName}, '{delim}') AS A" + "\n" +
                                            "\t\t\t" + $"LEFT JOIN {moduleField.TabelReferenceName} AS B" + "\n" +
                                            "\t\t\t" + $"ON A.val = B.{moduleField.TableReferenceFieldKey}" + "\n" +
                                            "\t\t" + "FOR XML PATH (''))" + "\n" +
                                            "\t" + ", 1, 1, '')" + "\n" +
                                            ")";
                            sFields += multiCheckBoxLogic + " AS " + moduleField.FieldName;

                            if (!string.IsNullOrEmpty(search))
                            {
                                if (string.IsNullOrEmpty(searchQuery))
                                {
                                    searchQuery = " WHERE ( " + multiCheckBoxLogic + " LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                                }
                                else
                                {
                                    searchQuery = searchQuery + " OR " + multiCheckBoxLogic + " LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                                }
                            }
                        }
                        else if (fieldType == (int)EFieldType.ReferenceTable && extType != (int)EControlType.PopUpMultiCheckBox)
                        {
                            // table reference alias handler, automaticlly generate new alias if found same name/alias
                            string referenceAlias = string.IsNullOrWhiteSpace(Convert.ToString(moduleField.TabelReferenceNameAlias)) ? Convert.ToString(moduleField.TabelReferenceName) : Convert.ToString(moduleField.TabelReferenceNameAlias);
                            bool duplicatedAlias = referenceAliasHistory.Any(pastAlias => pastAlias == referenceAlias);
                            if (duplicatedAlias == true)
                            {
                                referenceAlias = referenceAlias + "_" + referenceAliasHistory.Count;
                            }

                            moduleJoin += " LEFT JOIN " + moduleField.TabelReferenceName + " " + referenceAlias + " ON " + moduleName + "." + moduleField.FieldName + " = " + referenceAlias + "." + moduleField.TableReferenceFieldKey + (Convert.ToBoolean(moduleField.BCasCade) ? " AND " + Convert.ToString(moduleField.FilterCascade).Replace("@Parent", moduleName + "." + Convert.ToString(moduleField.FieldNameParent)) : "");
                            sFields += "'(' + CAST(" + moduleName + "." + moduleField.FieldName + " AS VARCHAR(MAX)) + ') - ' + CAST(" + referenceAlias + "." + moduleField.TableReferenceFieldDisplayName + " AS VARCHAR(MAX)) AS " + moduleField.FieldName;
                            referenceAliasHistory.Add(referenceAlias);

                            if (!string.IsNullOrEmpty(search))
                            {
                                if (string.IsNullOrEmpty(searchQuery))
                                {
                                    searchQuery = " WHERE ( ('(' + CAST(" + moduleName + "." + moduleField.FieldName + " AS VARCHAR(MAX)) + ') - ' + CAST(" + referenceAlias + "." + moduleField.TableReferenceFieldDisplayName + " AS VARCHAR(MAX))) LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                                }
                                else
                                {
                                    searchQuery = searchQuery + " OR ('(' + CAST(" + moduleName + "." + moduleField.FieldName + " AS VARCHAR(MAX)) + ') - ' + CAST(" + referenceAlias + "." + moduleField.TableReferenceFieldDisplayName + " AS VARCHAR(MAX))) LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                                }
                            }
                        }
                        else if (fieldType == (int)EFieldType.VarBinary && (extType == (int)EControlType.DisplayField || extType == (int)EControlType.FileUpload))
                        {
                            sFields += moduleName + "." + moduleField.FieldName + ", ";
                            sFields += moduleName + "." + moduleField.FieldName + "Name";

                            if (!string.IsNullOrEmpty(search))
                            {
                                if (string.IsNullOrEmpty(searchQuery))
                                {
                                    searchQuery = " WHERE ( CAST(" + moduleName + "." + Convert.ToString(moduleField.FieldName) + "Name AS VARCHAR(MAX)) LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                                }
                                else
                                {
                                    searchQuery = searchQuery + " OR CAST(" + moduleName + "." + Convert.ToString(moduleField.FieldName) + "Name AS VARCHAR(MAX)) LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                                }
                            }
                        }
                        else if (fieldType == (int)EFieldType.DATETIME)
                        {
                            sFields += "FORMAT(" + moduleName + "." + moduleField.FieldName + ", '" + dateFormat + "') AS " + moduleField.FieldName;

                            if (!string.IsNullOrEmpty(search))
                            {
                                if (string.IsNullOrEmpty(searchQuery))
                                {
                                    searchQuery = " WHERE ( FORMAT(" + moduleName + "." + Convert.ToString(moduleField.FieldName) + ", '" + dateFormat + "') LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                                }
                                else
                                {
                                    searchQuery = searchQuery + " OR FORMAT(" + moduleName + "." + Convert.ToString(moduleField.FieldName) + ", '" + dateFormat + "') LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                                }
                            }
                        }
                        else if (fieldType == (int)EFieldType.FLOAT || fieldType == (int)EFieldType.NUMERICDECIMAL ||
                                fieldType == (int)EFieldType.REAL || fieldType == (int)EFieldType.MONEY)
                        {
                            sFields += moduleName + "." + moduleField.FieldName;

                            if (!string.IsNullOrEmpty(search))
                            {
                                if (String.IsNullOrEmpty(searchQuery))
                                {
                                    searchQuery = " WHERE ( FORMAT(" + moduleName + "." + Convert.ToString(moduleField.FieldName) + ",'#.###############') LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                                }
                                else
                                {
                                    searchQuery = searchQuery + " OR FORMAT(" + moduleName + "." + Convert.ToString(moduleField.FieldName) + ",'#.###############') LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                                }
                            }
                        }
                        else
                        {
                            sFields += moduleName + "." + moduleField.FieldName;

                            if (!string.IsNullOrEmpty(search))
                            {
                                if (string.IsNullOrEmpty(searchQuery))
                                {
                                    searchQuery = " WHERE ( CAST(" + moduleName + "." + Convert.ToString(moduleField.FieldName) + " AS VARCHAR(MAX)) LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                                }
                                else
                                {
                                    searchQuery = searchQuery + " OR CAST(" + moduleName + "." + Convert.ToString(moduleField.FieldName) + " AS VARCHAR(MAX)) LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                                }
                            }
                        }
                    }
                    #endregion

                    if (!string.IsNullOrEmpty(filter) && string.IsNullOrEmpty(search))
                    {
                        if (isAdv)
                        {
                            advQuery = " WHERE( " + filter;
                        }
                        else
                        {
                            string[] filterArr = filter.Split(' ');
                            string restOfFilter = string.Join(" ", filterArr.Skip(1));

                            ModuleField column = moduleFields.Where(item => item.IsShowInView && item.FieldName == filterArr[0]).FirstOrDefault();
                            if (column is not null && filterArr[1] == "LIKE")
                            {
                                if (column.FK_FieldType_ID == (int)EFieldType.ReferenceTable)
                                {
                                    if (string.IsNullOrEmpty(column.TabelReferenceNameAlias))
                                    {
                                        searchQuery = " WHERE (('(' + CAST(" + moduleName + "." + filterArr[0]
                                            + " AS VARCHAR(MAX)) + ') - ' + CAST(" + column.TabelReferenceName + "."
                                            + column.TableReferenceFieldDisplayName + " AS VARCHAR(MAX))) " + restOfFilter;
                                    }
                                    else
                                    {
                                        searchQuery = " WHERE (('(' + CAST(" + moduleName + "." + filterArr[0]
                                            + " AS VARCHAR(MAX)) + ') - ' + CAST(" + column.TabelReferenceNameAlias + "."
                                            + column.TableReferenceFieldDisplayName + " AS VARCHAR(MAX))) " + restOfFilter;
                                    }

                                }
                                else if (column.FK_FieldType_ID == (int)EFieldType.FLOAT || column.FK_FieldType_ID == (int)EFieldType.NUMERICDECIMAL ||
                                    column.FK_FieldType_ID == (int)EFieldType.REAL || column.FK_FieldType_ID == (int)EFieldType.MONEY)
                                {
                                    searchQuery = " WHERE (FORMAT(" + moduleName + "." + filterArr[0]
                                        + ",'#.###############') " + restOfFilter;
                                }
                                else if (column.FK_FieldType_ID == (int)EFieldType.DATETIME)
                                {
                                    searchQuery = " WHERE (FORMAT(" + moduleName + "." + filterArr[0]
                                        + ", '" + dateFormat + "') " + restOfFilter;
                                }
                                else
                                {
                                    searchQuery = " WHERE (" + moduleName + "." + filter;
                                }
                            }
                            else
                            {
                                if (filterArr[0] == "CreatedDate" || filterArr[0] == "LastUpdateDate" ||
                                    filterArr[0] == "ApprovedDate")
                                {
                                    searchQuery = " WHERE (FORMAT(" + moduleName + "." + filterArr[0]
                                        + ", '" + dateFormat + "') " + restOfFilter;
                                }
                                else
                                {
                                    searchQuery = " WHERE (" + moduleName + "." + filter;
                                }
                            }
                        }
                    }

                    #region show last update
                    AppSystemParameter createdBySysParam = SystemParameterBLL.GetAppSystemParameter(43);
                    string createdBy = createdBySysParam != null ? createdBySysParam.SettingValue : "0";

                    AppSystemParameter lastUpdateBySysParam = SystemParameterBLL.GetAppSystemParameter(44);
                    string lastUpdateBy = lastUpdateBySysParam != null ? lastUpdateBySysParam.SettingValue : "0";

                    AppSystemParameter approvedBySysParam = SystemParameterBLL.GetAppSystemParameter(45);
                    string approvedBy = approvedBySysParam != null ? approvedBySysParam.SettingValue : "0";

                    AppSystemParameter createdDateSysParam = SystemParameterBLL.GetAppSystemParameter(46);
                    string createdDate = createdDateSysParam != null ? createdDateSysParam.SettingValue : "0";

                    AppSystemParameter lastUpdateDateSysParam = SystemParameterBLL.GetAppSystemParameter(47);
                    string lastUpdateDate = lastUpdateDateSysParam != null ? lastUpdateDateSysParam.SettingValue : "0";

                    AppSystemParameter approvedDateSysParam = SystemParameterBLL.GetAppSystemParameter(48);
                    string approvedDate = approvedDateSysParam != null ? approvedDateSysParam.SettingValue : "0";

                    if (createdBy == "1")
                    {
                        if (!string.IsNullOrEmpty(sFields))
                        {
                            sFields = sFields + ", ";
                        }

                        sFields += moduleName + ".CreatedBy";

                        if (!string.IsNullOrEmpty(outerModuleColumns))
                        {
                            outerModuleColumns = outerModuleColumns + ", ";
                        }

                        outerModuleColumns += " CreatedBy";

                        if (!string.IsNullOrEmpty(search))
                        {
                            if (string.IsNullOrEmpty(searchQuery))
                            {
                                searchQuery = " WHERE ( " + moduleName + ".CreatedBy LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                            }
                            else
                            {
                                searchQuery = searchQuery + " OR " + moduleName + ".CreatedBy LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                            }
                        }
                    }

                    if (lastUpdateBy == "1")
                    {
                        if (!string.IsNullOrEmpty(sFields))
                        {
                            sFields = sFields + ", ";
                        }

                        sFields += moduleName + ".LastUpdateBy";

                        if (!string.IsNullOrEmpty(outerModuleColumns))
                        {
                            outerModuleColumns = outerModuleColumns + ", ";
                        }

                        outerModuleColumns += " LastUpdateBy";

                        if (!string.IsNullOrEmpty(search))
                        {
                            if (string.IsNullOrEmpty(searchQuery))
                            {
                                searchQuery = " WHERE ( " + moduleName + ".LastUpdateBy LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                            }
                            else
                            {
                                searchQuery = searchQuery + " OR " + moduleName + ".LastUpdateBy LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                            }
                        }
                    }

                    if (approvedBy == "1")
                    {
                        if (!string.IsNullOrEmpty(sFields))
                        {
                            sFields = sFields + ", ";
                        }

                        sFields += moduleName + ".ApprovedBy";

                        if (!string.IsNullOrEmpty(outerModuleColumns))
                        {
                            outerModuleColumns = outerModuleColumns + ", ";
                        }

                        outerModuleColumns += " ApprovedBy";

                        if (!string.IsNullOrEmpty(search))
                        {
                            if (string.IsNullOrEmpty(searchQuery))
                            {
                                searchQuery = " WHERE ( " + moduleName + ".ApprovedBy LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                            }
                            else
                            {
                                searchQuery = searchQuery + " OR " + moduleName + ".ApprovedBy LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                            }
                        }
                    }

                    if (createdDate == "1")
                    {
                        if (!string.IsNullOrEmpty(sFields))
                        {
                            sFields = sFields + ", ";
                        }

                        sFields += "FORMAT(" + moduleName + ".CreatedDate, '" + dateFormat + "') AS CreatedDate";

                        if (!string.IsNullOrEmpty(outerModuleColumns))
                        {
                            outerModuleColumns = outerModuleColumns + ", ";
                        }

                        outerModuleColumns += " CreatedDate";

                        if (!string.IsNullOrEmpty(search))
                        {
                            if (string.IsNullOrEmpty(searchQuery))
                            {
                                searchQuery = " WHERE ( FORMAT(" + moduleName + ".CreatedDate, '" + dateFormat + "') LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                            }
                            else
                            {
                                searchQuery = searchQuery + " OR FORMAT(" + moduleName + ".CreatedDate, '" + dateFormat + "') LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                            }
                        }
                    }

                    if (lastUpdateDate == "1")
                    {
                        if (!string.IsNullOrEmpty(sFields))
                        {
                            sFields = sFields + ", ";
                        }

                        sFields += "FORMAT(" + moduleName + ".LastUpdateDate, '" + dateFormat + "') AS LastUpdateDate";

                        if (!string.IsNullOrEmpty(outerModuleColumns))
                        {
                            outerModuleColumns = outerModuleColumns + ", ";
                        }

                        outerModuleColumns += " LastUpdateDate";

                        if (!string.IsNullOrEmpty(search))
                        {
                            if (string.IsNullOrEmpty(searchQuery))
                            {
                                searchQuery = " WHERE ( FORMAT(" + moduleName + ".LastUpdateDate, '" + dateFormat + "') LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                            }
                            else
                            {
                                searchQuery = searchQuery + " OR FORMAT(" + moduleName + ".LastUpdateDate, '" + dateFormat + "') LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                            }
                        }
                    }

                    if (approvedDate == "1")
                    {
                        if (!string.IsNullOrEmpty(sFields))
                        {
                            sFields = sFields + ", ";
                        }

                        sFields += "FORMAT(" + moduleName + ".ApprovedDate, '" + dateFormat + "') AS ApprovedDate";

                        if (!string.IsNullOrEmpty(outerModuleColumns))
                        {
                            outerModuleColumns = outerModuleColumns + ", ";
                        }

                        outerModuleColumns += " ApprovedDate";

                        if (!string.IsNullOrEmpty(search))
                        {
                            if (string.IsNullOrEmpty(searchQuery))
                            {
                                searchQuery = " WHERE ( FORMAT(" + moduleName + ".ApprovedDate, '" + dateFormat + "') LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                            }
                            else
                            {
                                searchQuery = searchQuery + " OR FORMAT(" + moduleName + ".ApprovedDate, '" + dateFormat + "') LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' ";
                            }
                        }
                    }
                    #endregion

                    if (!string.IsNullOrEmpty(searchQuery))
                    {
                        searchQuery = searchQuery + " )";
                    }

                    if (!string.IsNullOrEmpty(advQuery))
                    {
                        advQuery = advQuery + " )";
                    }

                    string queryDataAccess = "SELECT * From DataAccess Where FK_Module_ID = @ModuleID AND FK_Role_ID = @RoleID";
                    prms = new SqlParameter[]
                    {
                        new SqlParameter("@ModuleID", SqlDbType.Int){ Value = ModuleID },
                        new SqlParameter("@RoleID", SqlDbType.Int){ Value = FK_MRole_ID }
                    };
                    DataTable dtDataAccess = NawaDAO.ExecuteTable(queryDataAccess, prms);

                    if (dtDataAccess != null && dtDataAccess.Rows.Count > 0)
                    {
                        string filterData = "";

                        foreach (DataRow drDataAccess in dtDataAccess.Rows)
                        {
                            filterData += " AND " + Convert.ToString(drDataAccess["FilterData"]).ToLower().Replace("@userid", userID);
                        }

                        string filterDataFinal = filterData.Remove(0, 5);

                        if (String.IsNullOrEmpty(searchQuery))
                        {
                            searchQuery = " WHERE (" + filterDataFinal + ")";
                        }
                        else
                        {
                            searchQuery = searchQuery + " AND (" + filterDataFinal + ")";
                        }
                    }

                    query = "EXEC usp_SelectNew @Query, @OrderBy";
                    prms = new SqlParameter[] {
                        new SqlParameter("@Query", SqlDbType.VarChar){ Value = "SELECT " + outerModuleColumns + " FROM ( SELECT "  + sFields + " FROM " + moduleName + moduleJoin + searchQuery + ") X " + advQuery },
                        new SqlParameter("@OrderBy", SqlDbType.VarChar){ Value = (!String.IsNullOrEmpty(orderBy) ? orderBy + " " + order : "") }
                    };
                    DataTable dt = NawaDAO.ExecuteTable(query, prms);
                    memoryStreamOutput = NawaDAO.ConvertToExcel(dt);
                }

                return memoryStreamOutput;
            }
            catch
            {
                throw;
            }
        }

        public MemoryStream ExportModuleDataSelected(int ModuleID, List<string> SelectedData, string orderBy, string order)
        {
            try
            {
                string query = "SELECT * FROM Module WHERE PK_Module_ID = @ModuleID";
                SqlParameter[] prms = new SqlParameter[]
                {
                    new SqlParameter("@ModuleID", SqlDbType.Int){ Value = ModuleID }
                };
                DataRow dr = NawaDAO.ExecuteRow(query, prms);
                MemoryStream memoryStreamOutput = null;

                if (dr != null)
                {
                    //string moduleName = Convert.ToString(dr["ModuleName"]);
                    //query = "SELECT * FROM ModuleField WHERE FK_Module_ID = @ModuleID";
                    //prms = new SqlParameter[] {
                    //    new SqlParameter("@ModuleID", SqlDbType.Int){ Value = ModuleID }
                    //};
                    //DataTable dtFields = NawaDAO.ExecuteTable(query, prms);
                    string sFields = "";
                    string moduleJoin = "";
                    string searchQuery = "";
                    string sSelected = string.Join(",", SelectedData.Select(item => "'" + item + "'").ToList());

                    #region Fill Data Query
                    List<string> referenceAliasHistory = new List<string>();
                    SystemParameter sysParamFormatDate = SystemParameterBLL.GetSystemParameterValue(6);
                    //string dateFormat = sysParamFormatDate != null ? sysParamFormatDate.SettingValue : "yyyy-MM-dd";
                    string dateFormat = "dd MMMM yyyy HH:mm:ss";

                    foreach (ModuleField moduleField in moduleFields)
                    {
                        int extType = Convert.ToInt32(moduleField.FK_ExtType_ID);
                        int fieldType = Convert.ToInt32(moduleField.FK_FieldType_ID);

                        if (Convert.ToBoolean(moduleField.IsPrimaryKey))
                        {
                            searchQuery = " WHERE " + Convert.ToString(moduleField.FieldName) + " IN (" + sSelected + ")";
                        }

                        if (Convert.ToBoolean(moduleField.IsShowInView))
                        {
                            if (!string.IsNullOrEmpty(sFields)) sFields = sFields + ", ";

                            if (extType == (int)EControlType.PopUpMultiCheckBox)
                            {
                                // get delimiter and fill query                 
                                string delim = SystemParameterBLL.GetSystemParameterGroup(39).SettingValue;
                                string multiCheckBoxLogic = "(" + "\n" +
                                                "\t" + "STUFF(" + "\n" +
                                                "\t\t" + $"(SELECT '{delim}(' + A.val + ') - ' + B.{moduleField.TableReferenceFieldDisplayName}" + "\n" +
                                                "\t\t" + $"FROM dbo.Split({moduleName}.{moduleField.FieldName}, '{delim}') AS A" + "\n" +
                                                "\t\t\t" + $"LEFT JOIN {moduleField.TabelReferenceName} AS B" + "\n" +
                                                "\t\t\t" + $"ON A.val = B.{moduleField.TableReferenceFieldKey}" + "\n" +
                                                "\t\t" + "FOR XML PATH (''))" + "\n" +
                                                "\t" + ", 1, 1, '')" + "\n" +
                                                ")";
                                sFields += multiCheckBoxLogic + " AS " + moduleField.FieldName;
                            }
                            else if (fieldType == (int)EFieldType.ReferenceTable && extType != (int)EControlType.PopUpMultiCheckBox)
                            {
                                // table reference alias handler, automaticlly generate new alias if found same name/alias
                                string referenceAlias = string.IsNullOrWhiteSpace(Convert.ToString(moduleField.TabelReferenceNameAlias)) ? Convert.ToString(moduleField.TabelReferenceName) : Convert.ToString(moduleField.TabelReferenceNameAlias);
                                bool duplicatedAlias = referenceAliasHistory.Any(pastAlias => pastAlias == referenceAlias);
                                if (duplicatedAlias == true)
                                {
                                    referenceAlias = referenceAlias + "_" + referenceAliasHistory.Count;
                                }

                                moduleJoin += " LEFT JOIN " + moduleField.TabelReferenceName + " " + referenceAlias + " ON " + moduleName + "." + moduleField.FieldName + " = " + referenceAlias + "." + moduleField.TableReferenceFieldKey + (Convert.ToBoolean(moduleField.BCasCade) ? " AND " + Convert.ToString(moduleField.FilterCascade).Replace("@Parent", moduleName + "." + Convert.ToString(moduleField.FieldNameParent)) : "");
                                sFields += "'(' + CAST(" + moduleName + "." + moduleField.FieldName + " AS VARCHAR(MAX)) + ') - ' + CAST(" + referenceAlias + "." + moduleField.TableReferenceFieldDisplayName + " AS VARCHAR(MAX)) AS " + moduleField.FieldName;
                                referenceAliasHistory.Add(referenceAlias);
                            }
                            else if (fieldType == (int)EFieldType.VarBinary && (extType == (int)EControlType.DisplayField || extType == (int)EControlType.FileUpload))
                            {
                                sFields += moduleName + "." + moduleField.FieldName + ", ";
                                sFields += moduleName + "." + moduleField.FieldName + "Name";
                            }
                            else if (fieldType == (int)EFieldType.DATETIME)
                            {
                                sFields += "FORMAT(" + moduleName + "." + moduleField.FieldName + ", '" + dateFormat + "') AS " + moduleField.FieldName;
                            }
                            else
                            {
                                sFields += moduleName + "." + moduleField.FieldName;
                            }
                        }
                    }
                    #endregion

                    #region show last update
                    AppSystemParameter createdBySysParam = SystemParameterBLL.GetAppSystemParameter(43);
                    string createdBy = createdBySysParam != null ? createdBySysParam.SettingValue : "0";

                    AppSystemParameter lastUpdateBySysParam = SystemParameterBLL.GetAppSystemParameter(44);
                    string lastUpdateBy = lastUpdateBySysParam != null ? lastUpdateBySysParam.SettingValue : "0";

                    AppSystemParameter approvedBySysParam = SystemParameterBLL.GetAppSystemParameter(45);
                    string approvedBy = approvedBySysParam != null ? approvedBySysParam.SettingValue : "0";

                    AppSystemParameter createdDateSysParam = SystemParameterBLL.GetAppSystemParameter(46);
                    string createdDate = createdDateSysParam != null ? createdDateSysParam.SettingValue : "0";

                    AppSystemParameter lastUpdateDateSysParam = SystemParameterBLL.GetAppSystemParameter(47);
                    string lastUpdateDate = lastUpdateDateSysParam != null ? lastUpdateDateSysParam.SettingValue : "0";

                    AppSystemParameter approvedDateSysParam = SystemParameterBLL.GetAppSystemParameter(48);
                    string approvedDate = approvedDateSysParam != null ? approvedDateSysParam.SettingValue : "0";

                    if (createdBy == "1")
                    {
                        if (!string.IsNullOrEmpty(sFields))
                        {
                            sFields = sFields + ", ";
                        }

                        sFields += moduleName + ".CreatedBy";
                    }

                    if (lastUpdateBy == "1")
                    {
                        if (!string.IsNullOrEmpty(sFields))
                        {
                            sFields = sFields + ", ";
                        }

                        sFields += moduleName + ".LastUpdateBy";
                    }

                    if (approvedBy == "1")
                    {
                        if (!string.IsNullOrEmpty(sFields))
                        {
                            sFields = sFields + ", ";
                        }

                        sFields += moduleName + ".ApprovedBy";
                    }

                    if (createdDate == "1")
                    {
                        if (!string.IsNullOrEmpty(sFields))
                        {
                            sFields = sFields + ", ";
                        }

                        sFields += "FORMAT(" + moduleName + ".CreatedDate, '" + dateFormat + "') AS CreatedDate";
                    }

                    if (lastUpdateDate == "1")
                    {
                        if (!string.IsNullOrEmpty(sFields))
                        {
                            sFields = sFields + ", ";
                        }

                        sFields += "FORMAT(" + moduleName + ".LastUpdateDate, '" + dateFormat + "') AS LastUpdateDate";
                    }

                    if (approvedDate == "1")
                    {
                        if (!string.IsNullOrEmpty(sFields))
                        {
                            sFields = sFields + ", ";
                        }

                        sFields += "FORMAT(" + moduleName + ".ApprovedDate, '" + dateFormat + "') AS ApprovedDate";
                    }
                    #endregion

                    query = "EXEC usp_SelectNew @Query, @OrderBy";
                    prms = new SqlParameter[]
                    {
                        new SqlParameter("@Query", SqlDbType.VarChar){ Value = "SELECT " + sFields + " FROM " + moduleName + moduleJoin + searchQuery },
                        new SqlParameter("@OrderBy", SqlDbType.VarChar){ Value = (!String.IsNullOrEmpty(orderBy) ? orderBy + " " + order : "") }
                    };
                    DataTable dt = NawaDAO.ExecuteTable(query, prms);
                    memoryStreamOutput = NawaDAO.ConvertToExcel(dt);
                }

                return memoryStreamOutput;
            }
            catch
            {
                throw;
            }
        }

        public MemoryStream ExportUploadModuleDataSelected(int ModuleID, List<string> SelectedData, string orderBy, string order, string userID)
        {
            try
            {
                string query = "SELECT * FROM Module WHERE PK_Module_ID = @ModuleID";
                SqlParameter[] prms = new SqlParameter[] {
                    new SqlParameter("@ModuleID", SqlDbType.Int){ Value = ModuleID }
                };

                DataRow dr = NawaDAO.ExecuteRow(query, prms);

                if (dr != null)
                {
                    //string moduleName = Convert.ToString(dr["ModuleName"]);

                    bool splitUpload = Convert.ToBoolean(SystemParameterBLL.GetSystemParameterValue(53).SettingValue);

                    string moduleNameUpload = moduleName;

                    if (splitUpload)
                    {
                        moduleNameUpload += "_Upload_data_" + CommonBLL.StripTableName(userID);
                    }
                    else
                    {
                        moduleNameUpload += "_Upload";
                    }

                    string sFields = "";

                    string searchQuery = "";
                    string sSelected = String.Join(",", SelectedData.Select(item => "'" + item + "'").ToList());
                    foreach (ModuleField moduleField in moduleFields)
                    {
                        if (Convert.ToBoolean(moduleField.IsPrimaryKey))
                        {
                            searchQuery = " WHERE " + Convert.ToString(moduleField.FieldName) + " IN (" + sSelected + ")";
                        }

                        if (Convert.ToBoolean(moduleField.IsShowInView))
                        {
                            if (!String.IsNullOrEmpty(sFields)) sFields = sFields + ", ";
                            sFields = sFields + Convert.ToString(moduleField.FieldName);
                        }
                    }

                    query = "EXEC usp_SelectNew @Query, @OrderBy";
                    prms = new SqlParameter[] {
                        new SqlParameter("@Query", SqlDbType.VarChar){ Value = "SELECT " + sFields + " FROM " + moduleNameUpload + searchQuery },
                        new SqlParameter("@OrderBy", SqlDbType.VarChar){ Value = (!String.IsNullOrEmpty(orderBy) ? orderBy + " " + order : "") }
                    };

                    DataTable dt = NawaDAO.ExecuteTable(query, prms);

                    return NawaDAO.ConvertToExcel(dt);
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public MemoryStream ExportInvalidModuleDataAll(int moduleID, string orderBy, string order, string search, string filter, string userID, int mRole_ID)
        {
            try
            {
                MemoryStream memoryStreamOutput = null;
                string query = "SELECT * FROM Module WHERE PK_Module_ID = @ModuleID";
                SqlParameter[] prms = new SqlParameter[]
                {
                    new SqlParameter("@ModuleID", SqlDbType.Int){ Value = moduleID }
                };
                DataRow dr = NawaDAO.ExecuteRow(query, prms);

                if (dr != null)
                {
                    //string moduleName = Convert.ToString(dr["ModuleName"]);
                    bool splitUpload = Convert.ToBoolean(SystemParameterBLL.GetSystemParameterValue(53).SettingValue);
                    string moduleNameUpload = moduleName;

                    if (splitUpload)
                    {
                        moduleNameUpload += "_Upload_data_" + CommonBLL.StripTableName(userID);
                    }
                    else
                    {
                        moduleNameUpload += "_Upload";
                    }

                    string searchQuery = InvalidModuleDataSearchQuery(moduleID, userID, search, filter);
                    query = "SELECT * FROM " + moduleNameUpload + searchQuery;

                    if (!string.IsNullOrEmpty(orderBy))
                    {
                        query += " ORDER BY " + orderBy + " " + order;
                    }

                    DataTable dt = NawaDAO.ExecuteTable(query);
                    memoryStreamOutput = NawaDAO.ConvertToExcel(dt);
                }

                return memoryStreamOutput;
            }
            catch
            {
                throw;
            }
        }

        public List<ModuleField> GetModuleField(int moduleID)
        {
            try
            {
                string queryStr = @" 
                SELECT 
	                mf.PK_ModuleField_ID, mf.FK_Module_ID, mf.FieldName, mf.FieldLabel, mf.[Sequence], mf.[Required], mf.IsPrimaryKey, mf.IsUnik, 
	                mf.IsShowInView, mf.IsShowInForm, mf.DefaultValue, mf.FK_FieldType_ID, ft.FieldTypeCaption, mf.SizeField, mf.FK_ExtType_ID, ext.ExtTypeName,
	                mf.TabelReferenceName, mf.TabelReferenceNameAlias, mf.TableReferenceFieldKey, mf.TableReferenceFieldDisplayName, mf.TableReferenceFilter, 
	                mf.IsUseRegexValidation, mf.TableReferenceAdditonalJoin, mf.BCasCade, mf.FieldNameParent, mf.FilterCascade
                FROM ModuleField mf
                LEFT JOIN MFieldType ft ON ft.PK_FieldType_ID = mf.FK_FieldType_ID
                LEFT JOIN MExtType ext ON ext.PK_ExtType_ID = mf.FK_ExtType_ID
                WHERE mf.FK_Module_ID = @ModuleID ";

                SqlParameter[] queryParams = new SqlParameter[]
                {
                    new SqlParameter("@ModuleID", SqlDbType.Int){ Value = moduleID }
                };

                DataTable dtModuleField = NawaDAO.ExecuteTable(queryStr, queryParams);
                List<ModuleField> listField = NawaDAO.ConvertDataTable<ModuleField>(dtModuleField);
                return listField;
            }
            catch
            {
                throw;
            }
        }

        public List<ModuleFieldRegexView> GetModuleFieldRegex(int moduleID)
        {
            try
            {
                string queryStr = @" 
                SELECT rg.PK_ModuleFieldRegex, rg.FK_ModuleField_ID, mf.FieldName, rg.Regex
                FROM ModuleFieldRegex rg
                INNER JOIN ModuleField mf ON mf.PK_ModuleField_ID = rg.FK_ModuleField_ID
                WHERE EXISTS(SELECT 1 FROM Module md WHERE md.PK_Module_ID = mf.FK_Module_ID
				                AND md.PK_Module_ID = @ModuleID) ";

                SqlParameter[] queryParams = new SqlParameter[]
                {
                    new SqlParameter("@ModuleID", SqlDbType.Int){ Value = moduleID }
                };

                DataTable dtRegex = NawaDAO.ExecuteTable(queryStr, queryParams);
                List<ModuleFieldRegexView> listRegex = NawaDAO.ConvertDataTable<ModuleFieldRegexView>(dtRegex);
                return listRegex;
            }
            catch
            {
                throw;
            }
        }

        public List<COption> GetListOptions(long PK_ModuleField_ID, string ParentValue, string userID, string search = "")
        {
            try
            {
                string queryStr = @"EXEC dbo.usp_GetListOptionsCustom @TableReferenceName, @TableReferenceNameAlias, @TableReferenceFieldKey, @TableReferenceFieldDisplayName, @TableReferenceFilter, @TableReferenceAdditonalJoin, @FilterCascade, @ParentValue, @UserID, @SearchKeyword";

                ModuleField mf = moduleFields.Where(item => item.PK_ModuleField_ID == PK_ModuleField_ID).FirstOrDefault();
                if (mf != null)
                {
                    SqlParameter[] queryParams = new SqlParameter[]
                    {
                        new SqlParameter("@TableReferenceName", SqlDbType.VarChar){ Value = mf.TabelReferenceName },
                        new SqlParameter("@TableReferenceNameAlias", SqlDbType.VarChar){ Value = mf.TabelReferenceNameAlias },
                        new SqlParameter("@TableReferenceFieldKey", SqlDbType.VarChar){ Value = mf.TableReferenceFieldKey },
                        new SqlParameter("@TableReferenceFieldDisplayName", SqlDbType.VarChar){ Value = mf.TableReferenceFieldDisplayName },
                        new SqlParameter("@TableReferenceFilter", SqlDbType.VarChar){ Value = mf.TableReferenceFilter },
                        new SqlParameter("@TableReferenceAdditonalJoin", SqlDbType.VarChar){ Value = mf.TableReferenceAdditonalJoin },
                        new SqlParameter("@FilterCascade", SqlDbType.VarChar){ Value = mf.FilterCascade },
                        new SqlParameter("@ParentValue", SqlDbType.VarChar){ Value = String.IsNullOrEmpty(ParentValue) ? DBNull.Value : (object)ParentValue },
                        new SqlParameter("@UserID", SqlDbType.VarChar){ Value = userID },
                        new SqlParameter("@SearchKeyword", SqlDbType.VarChar){ Value = String.IsNullOrEmpty(search) ? "" : search.Replace("'", "''") }
                    };

                    DataTable dtListOptions = NawaDAO.ExecuteTable(queryStr, queryParams);
                    List<COption> listOptions = NawaDAO.ConvertDataTable<COption>(dtListOptions);
                    return listOptions;
                }

                return new List<COption>();
            }
            catch
            {
                throw;
            }
        }

        public List<COption> GetListOptionsWithPage(
            long PK_ModuleField_ID,
            string ParentValue,
            string userID,
            out int rowCount,
            out int pageSize,
            int page = 1,
            string search = ""
        )
        {
            try
            {
                // SQL PARAMETER (both count and data SP have the same parameter)
                pageSize = Convert.ToInt32(SystemParameterBLL.GetAppSystemParameter(7).SettingValue);

                SqlParameter[] queryParams = new SqlParameter[]
                {
                    new SqlParameter("@PK_ModuleField_ID", SqlDbType.BigInt){ Value = PK_ModuleField_ID },
                    new SqlParameter("@ParentValue", SqlDbType.VarChar){ Value = String.IsNullOrEmpty(ParentValue) ? DBNull.Value : (object)ParentValue },
                    new SqlParameter("@UserID", SqlDbType.VarChar){ Value = userID },
                    new SqlParameter("@SearchKeyword", SqlDbType.VarChar){ Value = String.IsNullOrEmpty(search) ? "" : search.Replace("'", "''") },
                    new SqlParameter("@PageSize", SqlDbType.Int){ Value = pageSize },
                    new SqlParameter("@PageIndex", SqlDbType.Int){ Value = page }
                };

                // COUNT DATA
                string queryStr = "";
                queryStr = @"EXEC dbo.usp_GetListOptionsCount " +
                                @"@PK_ModuleField_ID, " +
                                @"@ParentValue, " +
                                @"@UserID, " +
                                @"@SearchKeyword ";

                rowCount = Convert.ToInt32(NawaDAO.ExecuteScalar(queryStr, queryParams));

                // PAGING DATA
                queryStr = @"EXEC dbo.usp_GetListOptionsWithPage " +
                                @"@PK_ModuleField_ID, " +
                                @"@ParentValue, " +
                                @"@UserID, " +
                                @"@SearchKeyword, " +
                                @"@PageIndex, " +
                                @"@PageSize ";

                DataTable dtListOptions = NawaDAO.ExecuteTable(queryStr, queryParams);
                List<COption> listOptions = NawaDAO.ConvertDataTable<COption>(dtListOptions);
                return listOptions;
            }
            catch
            {
                throw;
            }
        }

        public List<COption> GetListOptionsByID(long PK_ModuleField_ID, string ParentValue, string userID, string FieldKey = "")
        {
            try
            {
                List<COption> listOptions = new List<COption>();

                if (!String.IsNullOrEmpty(FieldKey))
                {
                    // split FieldKey with delimiter, example: FieldKey = "7463|273|1", then return array [7463, 273, 1]                    
                    string delim = SystemParameterBLL.GetSystemParameterGroup(39).SettingValue;
                    string[] possibleDelimiters = { "|", ";", delim };
                    string[] keys = FieldKey.Split(possibleDelimiters, StringSplitOptions.RemoveEmptyEntries);

                    // looping each FieldKey
                    foreach (string key in keys)
                    {
                        string queryStr = @"EXEC dbo.usp_GetListOptionsByID @PK_ModuleField_ID, @ParentValue, @UserID, @FieldKey";

                        SqlParameter[] queryParams = new SqlParameter[]
                        {
                        new SqlParameter("@PK_ModuleField_ID", SqlDbType.BigInt){ Value = PK_ModuleField_ID },
                        new SqlParameter("@ParentValue", SqlDbType.VarChar){ Value = String.IsNullOrEmpty(ParentValue) ? DBNull.Value : (object)ParentValue },
                        new SqlParameter("@UserID", SqlDbType.VarChar){ Value = userID },
                        new SqlParameter("@FieldKey", SqlDbType.VarChar){ Value = key.Replace("'", "''") }
                        };

                        DataTable dtListOptions = NawaDAO.ExecuteTable(queryStr, queryParams);
                        List<COption> tempListOptions = NawaDAO.ConvertDataTable<COption>(dtListOptions);
                        if (tempListOptions.Count > 0)
                        {
                            listOptions.Add(tempListOptions[0]);
                        }
                    }
                }

                return listOptions;
            }
            catch
            {
                throw;
            }
        }

        public List<COption> GetListOptionsModuleDetail(long PK_ModuleDetailField_ID, string ParentValue, string search = "")
        {
            try
            {
                string queryStr = @"EXEC dbo.usp_GetListOptionsModuleDetail @PK_ModuleDetailField_ID, @ParentValue, @SearchKeyword";

                SqlParameter[] queryParams = new SqlParameter[]
                {
                    new SqlParameter("@PK_ModuleDetailField_ID", SqlDbType.BigInt){ Value = PK_ModuleDetailField_ID },
                    new SqlParameter("@ParentValue", SqlDbType.VarChar){ Value = String.IsNullOrEmpty(ParentValue) ? DBNull.Value : (object)ParentValue },
                    new SqlParameter("@SearchKeyword", SqlDbType.VarChar){ Value = String.IsNullOrEmpty(search) ? "" : search.Replace("'", "''") }
                };

                DataTable dtListOptions = NawaDAO.ExecuteTable(queryStr, queryParams);
                List<COption> listOptions = NawaDAO.ConvertDataTable<COption>(dtListOptions);
                return listOptions;
            }
            catch
            {
                throw;
            }
        }

        public List<COption> GetListOptionsModuleDetailWithPage(
            long PK_ModuleDetailField_ID,
            string ParentValue,
            out int rowCount,
            out int pageSize,
            int page = 1,
            string search = ""
        )
        {
            try
            {
                // SQL PARAMETER (both count and data SP have the same parameter)
                pageSize = Convert.ToInt32(SystemParameterBLL.GetAppSystemParameter(7).SettingValue);

                SqlParameter[] queryParams = new SqlParameter[]
                {
                    new SqlParameter("@PK_ModuleDetailField_ID", SqlDbType.BigInt){ Value = PK_ModuleDetailField_ID },
                    new SqlParameter("@ParentValue", SqlDbType.VarChar){ Value = String.IsNullOrEmpty(ParentValue) ? DBNull.Value : (object)ParentValue },
                    new SqlParameter("@SearchKeyword", SqlDbType.VarChar){ Value = String.IsNullOrEmpty(search) ? "" : search.Replace("'", "''") },
                    new SqlParameter("@PageSize", SqlDbType.Int){ Value = pageSize },
                    new SqlParameter("@PageIndex", SqlDbType.Int){ Value = page }
                };

                // COUNT DATA
                string queryStr = "";
                queryStr = @"EXEC dbo.usp_GetListOptionsModuleDetailCount " +
                                @"@PK_ModuleDetailField_ID, " +
                                @"@ParentValue, " +
                                @"@SearchKeyword ";

                rowCount = Convert.ToInt32(NawaDAO.ExecuteScalar(queryStr, queryParams));

                // PAGING DATA
                queryStr = @"EXEC dbo.usp_GetListOptionsModuleDetailWithPage " +
                                @"@PK_ModuleDetailField_ID, " +
                                @"@ParentValue, " +
                                @"@SearchKeyword, " +
                                @"@PageIndex, " +
                                @"@PageSize ";

                DataTable dtListOptions = NawaDAO.ExecuteTable(queryStr, queryParams);
                List<COption> listOptions = NawaDAO.ConvertDataTable<COption>(dtListOptions);
                return listOptions;
            }
            catch
            {
                throw;
            }
        }

        public List<COption> GetListOptionsModuleDetailByID(long PK_ModuleDetailField_ID, string ParentValue, string FieldKey = "")
        {
            try
            {
                List<COption> listOptions = new List<COption>();

                if (!String.IsNullOrEmpty(FieldKey))
                {
                    // split FieldKey with delimiter, example: FieldKey = "7463|273|1", then return array [7463, 273, 1]                    
                    string delim = SystemParameterBLL.GetSystemParameterGroup(39).SettingValue;
                    string[] possibleDelimiters = { "|", ";", delim };
                    string[] keys = FieldKey.Split(possibleDelimiters, StringSplitOptions.RemoveEmptyEntries);

                    // looping each FieldKey
                    foreach (string key in keys)
                    {
                        string queryStr = @"EXEC dbo.usp_GetListOptionsModuleDetailByID @PK_ModuleDetailField_ID, @ParentValue, @FieldKey";

                        SqlParameter[] queryParams = new SqlParameter[]
                        {
                            new SqlParameter("@PK_ModuleDetailField_ID", SqlDbType.BigInt){ Value = PK_ModuleDetailField_ID },
                            new SqlParameter("@ParentValue", SqlDbType.VarChar){ Value = String.IsNullOrEmpty(ParentValue) ? DBNull.Value : (object)ParentValue },
                            new SqlParameter("@FieldKey", SqlDbType.VarChar){ Value = key.Replace("'", "''") }
                        };

                        DataTable dtListOptions = NawaDAO.ExecuteTable(queryStr, queryParams);
                        List<COption> tempListOptions = NawaDAO.ConvertDataTable<COption>(dtListOptions);
                        if (tempListOptions.Count > 0)
                        {
                            listOptions.Add(tempListOptions[0]);
                        }
                    }
                }

                return listOptions;
            }
            catch
            {
                throw;
            }
        }

        public List<KeyValuePair<long, string>> SaveData(CParameterInput parameterInput, string userID, int pkMUserID, int roleID)
        {

            NawaConnection connection = NawaDAO.CreateConnection();
            connection.BeginTransaction();

            try
            {
                string query = "SELECT * FROM Module WHERE PK_Module_ID = @ModuleID";
                bool isApproval = false;
                object identityTemp = 0;
                SqlParameter[] prms = new SqlParameter[]
                {
                    new SqlParameter("@ModuleID", SqlDbType.Int){ Value = parameterInput.ModuleID }
                };
                DataRow dr = NawaDAO.ExecuteRow(query, prms, connection);

                if (dr != null)
                {
                    //Validate Duplicate
                    string sUniqueCheck = "";
                    string sPrimaryCheck = "";
                    List<SqlParameter> listParams = new List<SqlParameter>();
                    SqlParameter primaryParam = null;

                    if (moduleFields != null && moduleFields.Count > 0)
                    {
                        List<KeyValuePair<long, string>> validationData = new List<KeyValuePair<long, string>>();

                        #region Validations
                        foreach (KeyValuePair<string, object> inputData in parameterInput.data)
                        {
                            ModuleField dataField = moduleFields.Where(x => x.FieldName == inputData.Key && !x.IsPrimaryKey).FirstOrDefault();

                            if (dataField != null)
                            {
                                object inputValue = CommonBLL.GetObject(dataField.FK_FieldType_ID, inputData.Value);

                                if (dataField.Required && CommonBLL.StandartValidation(inputValue, EValidationErrorType.Empty))
                                {
                                    validationData.Add(new KeyValuePair<long, string>(dataField.PK_ModuleField_ID, "Data " + inputData.Key + " is empty"));
                                    //throw new Exception("Data " + inputData.Key + " is empty");
                                }

                                if (dataField.Required && CommonBLL.StandartValidation(inputValue, EValidationErrorType.Null))
                                {
                                    validationData.Add(new KeyValuePair<long, string>(dataField.PK_ModuleField_ID, "Data " + inputData.Key + " is null"));
                                    //throw new Exception("Data " + inputData.Key + " is null");
                                }

                                if (
                                        dataField.FK_FieldType_ID == (int)EFieldType.VARCHAR
                                        || dataField.FK_FieldType_ID == (int)EFieldType.FLOAT
                                        || dataField.FK_FieldType_ID == (int)EFieldType.REAL
                                        || dataField.FK_FieldType_ID == (int)EFieldType.MONEY
                                        || dataField.FK_FieldType_ID == (int)EFieldType.VarBinary
                                    )
                                {
                                    if (CommonBLL.StandartValidation(inputValue.ToString(), dataField.SizeField, EValidationErrorType.Size))
                                    {
                                        validationData.Add(new KeyValuePair<long, string>(dataField.PK_ModuleField_ID, "Data " + inputData.Key + " out of maximum character"));
                                        //throw new Exception("Data " + inputData.Key + " out of maximum character");
                                    }
                                }

                                if (dataField.IsUseRegexValidation)
                                {
                                    query = "SELECT * FROM ModuleFieldRegex WHERE FK_ModuleField_ID = @ID";
                                    prms = new SqlParameter[]
                                    {
                                        new SqlParameter("@ID", SqlDbType.Int) { Value = dataField.PK_ModuleField_ID }
                                    };
                                    DataTable dtRegex = NawaDAO.ExecuteTable(query, prms, connection);

                                    if (dtRegex != null && dtRegex.Rows.Count > 0)
                                    {
                                        foreach (DataRow rowRegex in dtRegex.Rows)
                                        {
                                            if (CommonBLL.StandartValidation(inputValue, Convert.ToString(rowRegex["Regex"]), EValidationErrorType.Regex))
                                            {
                                                validationData.Add(new KeyValuePair<long, string>(dataField.PK_ModuleField_ID, "Data " + inputData.Key + " out of maximum character"));
                                                //throw new Exception("Data " + inputData.Key + " out of maximum character");
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        #endregion

                        #region Unique & Primary Fields Check
                        List<ModuleField> uniquePrimaryFields = moduleFields.Where(x => x.IsUnik || x.IsPrimaryKey).ToList();
                        string approvalUniqueCheck = string.Empty;
                        List<SqlParameter> listApprovalUniqueParams = new List<SqlParameter>();
                        string approvalUploadUniqueCheck = string.Empty;
                        List<SqlParameter> listApprovalUploadUniqueParams = new List<SqlParameter>();

                        if (uniquePrimaryFields != null && uniquePrimaryFields.Count > 0)
                        {
                            foreach (ModuleField field in uniquePrimaryFields)
                            {
                                if (!field.IsPrimaryKey)
                                {
                                    if (!string.IsNullOrEmpty(sUniqueCheck)) sUniqueCheck += " OR ";

                                    if (field.FK_FieldType_ID == (int)EFieldType.DATETIME)
                                    {
                                        sUniqueCheck += "CAST(" + field.FieldName + " AS DATE) =" + "CAST(" + "@" + field.FieldName + " AS DATE)";
                                    }
                                    else
                                    {
                                        sUniqueCheck += field.FieldName + " = @" + field.FieldName;
                                    }

                                    listParams.Add(GetSqlParameter(field.FieldName, field.FK_FieldType_ID, parameterInput.data[field.FieldName], field.DefaultValue));

                                    #region For Approval Unique Check
                                    if (!string.IsNullOrEmpty(approvalUniqueCheck)) approvalUniqueCheck += " OR ";

                                    approvalUniqueCheck += "(ModuleField LIKE @" + field.FieldName + " OR ModuleFieldBefore LIKE @" + field.FieldName + ")";
                                    string jsonValueByFieldType = string.Empty;

                                    if (field.FK_FieldType_ID == (int)EFieldType.BIGINT || field.FK_FieldType_ID == (int)EFieldType.INT || field.FK_FieldType_ID == (int)EFieldType.SMALLINT
                                         || field.FK_FieldType_ID == (int)EFieldType.TINYINT || field.FK_FieldType_ID == (int)EFieldType.NUMERICDECIMAL || field.FK_FieldType_ID == (int)EFieldType.FLOAT
                                          || field.FK_FieldType_ID == (int)EFieldType.REAL)
                                    {
                                        jsonValueByFieldType = Convert.ToString(parameterInput.data[field.FieldName]);
                                    }
                                    else
                                    {
                                        jsonValueByFieldType = string.Format("\"{0}\"", Convert.ToString(parameterInput.data[field.FieldName]).Replace("%", "[%]"));
                                    }

                                    listApprovalUniqueParams.Add(new SqlParameter("@" + field.FieldName, SqlDbType.VarChar) { Value = string.Format("%\"{0}\":{1}%", field.FieldName, jsonValueByFieldType) });
                                    #endregion

                                    #region For Approval Upload Unique Check
                                    if (!string.IsNullOrEmpty(approvalUploadUniqueCheck)) approvalUploadUniqueCheck += " OR ";

                                    approvalUploadUniqueCheck += field.FieldName + " = @" + field.FieldName;
                                    listApprovalUploadUniqueParams.Add(new SqlParameter("@" + field.FieldName, SqlDbType.VarChar) { Value = Convert.ToString(parameterInput.data[field.FieldName]) });
                                    #endregion
                                }
                                else
                                {
                                    sPrimaryCheck = field.FieldName + " <> @" + field.FieldName;
                                    primaryParam = GetSqlParameter(field.FieldName, field.FK_FieldType_ID, parameterInput.data[field.FieldName], field.DefaultValue);

                                    if (parameterInput.ActionType == EActionType.Update)
                                    {
                                        #region For Approval Unique Check
                                        if (!String.IsNullOrEmpty(approvalUniqueCheck)) approvalUniqueCheck += " OR ";

                                        approvalUniqueCheck += "(ModuleField LIKE @" + field.FieldName + " OR ModuleFieldBefore LIKE @" + field.FieldName + ")";
                                        listApprovalUniqueParams.Add(new SqlParameter("@" + field.FieldName, SqlDbType.VarChar) { Value = string.Format("%\"{0}\":{1}%", field.FieldName, Convert.ToString(parameterInput.data[field.FieldName])) });
                                        #endregion

                                        #region For Approval Upload Unique Check
                                        if (!string.IsNullOrEmpty(approvalUploadUniqueCheck)) approvalUploadUniqueCheck += " OR ";

                                        approvalUploadUniqueCheck += field.FieldName + " = @" + field.FieldName;
                                        listApprovalUploadUniqueParams.Add(new SqlParameter("@" + field.FieldName, SqlDbType.VarChar) { Value = Convert.ToString(parameterInput.data[field.FieldName]) });
                                        #endregion
                                    }
                                }
                            }
                        }

                        if (!string.IsNullOrEmpty(sUniqueCheck))
                        {
                            query = "SELECT COUNT(13) AS DataCount FROM " + moduleName + " WHERE (" + sUniqueCheck + ")";

                            if (parameterInput.ActionType == EActionType.Update)
                            {
                                query += " AND " + sPrimaryCheck;

                                if (!listParams.Any(item => item.ParameterName == primaryParam.ParameterName)) listParams.Add(primaryParam);
                            }

                            int countDuplicate = Convert.ToInt32(NawaDAO.ExecuteScalar(query, listParams.ToArray(), connection));



                            if (countDuplicate > 0)
                            {
                                SystemDictionary sysDictionary = SystemParameterBLL.GetSystemDictionary("unique-data");

                                throw new Exception(sysDictionary.TextDefinition);
                            }
                        }

                        if (!string.IsNullOrEmpty(approvalUniqueCheck))
                        {
                            query = @"
                                    SELECT COUNT(13) AS DataCount 
                                    FROM ModuleApproval 
                                    WHERE (ModuleName = @ModuleName OR FK_Module_ID = @FK_Module_ID)
	                                AND (" + approvalUniqueCheck + ")";

                            listApprovalUniqueParams.Add(new SqlParameter("@ModuleName", SqlDbType.VarChar) { Value = moduleName });
                            listApprovalUniqueParams.Add(new SqlParameter("@FK_Module_ID", SqlDbType.Int) { Value = parameterInput.ModuleID });
                            int countDuplicate = Convert.ToInt32(NawaDAO.ExecuteScalar(query, listApprovalUniqueParams.ToArray(), connection));

                            if (countDuplicate > 0)
                            {
                                SystemDictionary sysDictionary = SystemParameterBLL.GetSystemDictionary("unique-data-approval");

                                throw new Exception(sysDictionary.TextDefinition);
                            }
                        }

                        if (!string.IsNullOrEmpty(approvalUploadUniqueCheck))
                        {
                            SystemParameter sysParamUploadUser = SystemParameterBLL.GetSystemParameterValue(53);
                            string tableUploadUser = sysParamUploadUser != null ? sysParamUploadUser.SettingValue : "0";
                            string moduleNameUploadApproval = moduleName;

                            if (tableUploadUser == "1")
                            {
                                moduleNameUploadApproval += "_Upload_data_" + CommonBLL.StripTableName(userID) + "_approval";
                            }
                            else
                            {
                                moduleNameUploadApproval += "_Upload_approval";
                            }

                            listApprovalUploadUniqueParams.Add(new SqlParameter("@TableUploadName", SqlDbType.VarChar) { Value = moduleNameUploadApproval });
                            query = @"IF OBJECT_ID(@TableUploadName, 'U') IS NULL
                                    BEGIN
	                                    SELECT 0 AS DataCount
                                    END
                                    ELSE
                                    BEGIN
                                        SELECT COUNT(13) AS DataCount 
                                        FROM " + moduleNameUploadApproval + @"
                                        WHERE " + approvalUploadUniqueCheck + @"
                                    END";
                            int countDuplicate = Convert.ToInt32(NawaDAO.ExecuteScalar(query, listApprovalUploadUniqueParams.ToArray(), connection));

                            if (countDuplicate > 0)
                            {
                                SystemDictionary sysDictionary = SystemParameterBLL.GetSystemDictionary("unique-data-approval");

                                throw new Exception(sysDictionary.TextDefinition);
                            }
                        }
                        #endregion

                        //Validate Validation Parameter
                        validationData = ValidateValidationParameter(moduleName, parameterInput, moduleFields, userID, connection);

                        if (validationData.Count > 0)
                        {
                            connection.Rollback();
                            connection.Dispose();
                            return validationData;
                        }

                        //Before Save
                        ValidateStoreProcedure(1, parameterInput, moduleFields, connection, isApproval, userID);

                        #region Initialize Audit Trail Variables
                        long approvalID = 0;
                        DataTable auditTrailOld = new DataTable();
                        DataTable auditTrailNew = new DataTable();
                        #endregion

                        //Save Data
                        listParams = new List<SqlParameter>();

                        if (Convert.ToBoolean(dr["IsUseApproval"]) && roleID != 1)
                        {
                            string oldJSON = "";
                            isApproval = true;
                            string primaryFieldName = moduleFields.Where(x => x.IsPrimaryKey).Select(x => x.FieldName).FirstOrDefault();

                            if (parameterInput.ActionType == EActionType.Update)
                            {
                                query = "SELECT * FROM " + moduleName + " WHERE " + primaryFieldName + " = @" + primaryFieldName;
                                prms = new SqlParameter[]
                                {
                                    GetSqlParameter(primaryFieldName, (int)EFieldType.IDENTITY, parameterInput.data[primaryFieldName])
                                };
                                DataRow row = NawaDAO.ExecuteRow(query, prms, connection);
                                oldJSON = JsonConvert.SerializeObject(row.Table);

                                //For Audit Trail
                                auditTrailOld = row.Table;
                            }

                            int randKey = 0;
                            Dictionary<string, object> newData = new Dictionary<string, object>();

                            foreach (ModuleField moduleField in moduleFields)
                            {
                                if (parameterInput.ActionType == EActionType.Insert)
                                {
                                    if (Convert.ToInt32(moduleField.FK_FieldType_ID) == 12 || Convert.ToInt32(moduleField.FK_FieldType_ID) == 15)
                                    {
                                        randKey = -((new Random()).Next(Int32.MaxValue - 2)) + 1;

                                        newData.Add(Convert.ToString(moduleField.FieldName), randKey);

                                        continue;
                                    }
                                }

                                if (Convert.ToInt32(moduleField.FK_FieldType_ID) == (int)EFieldType.VarBinary
                                    && (Convert.ToInt32(moduleField.FK_ExtType_ID) == (int)EControlType.DisplayField || Convert.ToInt32(moduleField.FK_ExtType_ID) == (int)EControlType.FileUpload))
                                {
                                    if (NawaDataBLL.CommonBLL.GetObject(9, parameterInput.data[Convert.ToString(moduleField.FieldName) + "Name"]) == DBNull.Value)
                                    {
                                        newData.Add(Convert.ToString(moduleField.FieldName + "Name"), "");
                                    }
                                    else
                                    {
                                        newData.Add(Convert.ToString(moduleField.FieldName + "Name"), NawaDataBLL.CommonBLL.GetObject(9, parameterInput.data[Convert.ToString(moduleField.FieldName) + "Name"]));
                                    }
                                }

                                newData.Add(Convert.ToString(moduleField.FieldName), NawaDataBLL.CommonBLL.GetObject(Convert.ToInt32(moduleField.FK_FieldType_ID), parameterInput.data[Convert.ToString(moduleField.FieldName)]));
                            }

                            string newJSON = JsonConvert.SerializeObject(new Dictionary<string, object>[] { newData });
                            query = "EXEC usp_InsertModuleApproval @ModuleName, @ModuleKey, @ModuleField, @ModuleFieldBefore, @PK_ModuleAction_ID, @CreatedDate, @CreatedBy, @FK_Module_ID";
                            prms = new SqlParameter[]
                            {
                                new SqlParameter("@ModuleName", SqlDbType.VarChar) { Value = moduleName },
                                new SqlParameter("@ModuleKey", SqlDbType.VarChar) { Value = (randKey < 0 ? randKey : newData[primaryFieldName]) },
                                new SqlParameter("@ModuleField", SqlDbType.Text) { Value = newJSON },
                                new SqlParameter("@ModuleFieldBefore", SqlDbType.Text) { Value = oldJSON },
                                new SqlParameter("@PK_ModuleAction_ID", SqlDbType.Int) { Value = Convert.ToInt32(parameterInput.ActionType) },
                                new SqlParameter("@CreatedDate", SqlDbType.DateTime) { Value = DateTime.Now },
                                new SqlParameter("@CreatedBy", SqlDbType.VarChar) { Value = userID },
                                new SqlParameter("@FK_Module_ID", SqlDbType.Int) { Value = Convert.ToInt32(parameterInput.ModuleID) }
                            };
                            long lastID = Convert.ToInt64(NawaDAO.ExecuteScalar(query, prms, connection));

                            #region Notification
                            query = "SELECT * FROM MUser WHERE FK_MRole_ID = @RoleID AND PK_MUser_ID != @PKUserID";
                            prms = new SqlParameter[]
                            {
                                new SqlParameter("@RoleID", SqlDbType.Int) { Value = roleID },
                                new SqlParameter("@PKUserID", SqlDbType.Int) { Value = pkMUserID },
                            };
                            DataTable dtSameRole = NawaDAO.ExecuteTable(query, prms, connection);

                            if (dtSameRole != null && dtSameRole.Rows.Count > 0)
                            {
                                List<MUser> listUserSameRole = NawaDAO.ConvertDataTable<MUser>(dtSameRole);
                                SystemDictionary sysDictionary = SystemParameterBLL.GetSystemDictionary("message-approval");
                                SystemDictionary sysDictionaryHeader = SystemParameterBLL.GetSystemDictionary("message-approval-header");

                                foreach (MUser user in listUserSameRole)
                                {
                                    MNotification notif = new MNotification();
                                    notif.FromUserID = pkMUserID;
                                    notif.ToUserID = user.PK_MUser_ID;
                                    notif.NotificationHeader = sysDictionaryHeader.TextDefinition;
                                    notif.NotificationBody = sysDictionary.TextDefinition;
                                    notif.IsRead = false;
                                    notif.DirectPage = "ParameterApprovalDetail?moduleid=" + parameterInput.ModuleID.ToString() + "&id=" + lastID.ToString();
                                    notif.FK_Module_ID = parameterInput.ModuleID;
                                    notif.FK_ModuleAction_ID = (int)parameterInput.ActionType;
                                    notif.FK_ModuleApproval_ID = lastID;
                                    notif.Active = true;
                                    notif.CreatedBy = userID;

                                    NotificationBLL.InsertNotification(notif);
                                }
                            }
                            #endregion

                            #region Send Email
                            query = "SELECT * FROM EmailTemplate WHERE EmailTemplateName = @TemplateName";
                            prms = new SqlParameter[]
                            {
                                new SqlParameter("@TemplateName", SqlDbType.VarChar){ Value = "Email Template for Approval All Parameter" }
                            };
                            DataRow drTemplate = NawaDAO.ExecuteRow(query, prms);

                            if (drTemplate != null)
                            {
                                EmailTemplate template = NawaDAO.ConvertDataTable<EmailTemplate>(drTemplate);

                                prms = new SqlParameter[]
                                {
                                    new SqlParameter("@PK_EmailTemplate_ID", SqlDbType.BigInt){ Value = template.PK_EmailTemplate_ID },
                                    new SqlParameter("@recordID", SqlDbType.VarChar){ Value = Convert.ToString(lastID) },
                                    new SqlParameter("@EmailIDReference", SqlDbType.VarChar){ Value = "0" },
                                };

                                NawaDAO.ExecuteNonQuery("usp_EMailScheduler_Create", prms, connection, CommandType.StoredProcedure);
                            }
                            #endregion

                            #region Audit Trail
                            List<Dictionary<string, object>> listDetails = new List<Dictionary<string, object>>();

                            listDetails.Add(newData);

                            auditTrailNew = NawaDAO.ConvertListDictionary(listDetails);
                            approvalID = lastID;
                            #endregion

                            if (parameterInput.ActionType == EActionType.Insert)
                            {
                                query = "DROP TABLE IF EXISTS dbo." + moduleName + "_WorkflowAdd_" + CommonBLL.StripTableName(userID);

                                NawaDAO.ExecuteNonQuery(query, null, connection);

                                query = "SELECT TOP 0 * INTO dbo." + moduleName + "_WorkflowAdd_" + CommonBLL.StripTableName(userID) + " FROM dbo." + moduleName;

                                NawaDAO.ExecuteNonQuery(query, null, connection);

                                string tableFields = "";
                                string insertParams = "";
                                bool identity = false;

                                foreach (ModuleField moduleField in moduleFields)
                                {
                                    if ((Convert.ToInt32(moduleField.FK_FieldType_ID) == 12 && Convert.ToBoolean(moduleField.IsUnik)) || (Convert.ToInt32(moduleField.FK_FieldType_ID) == 15 && Convert.ToBoolean(moduleField.IsUnik)))
                                    {
                                        identity = true;

                                        if (!String.IsNullOrEmpty(tableFields)) tableFields += ", ";

                                        tableFields += moduleField.FieldName;

                                        if (!String.IsNullOrEmpty(insertParams)) insertParams += ", ";

                                        insertParams += "@" + moduleField.FieldName;

                                        listParams.Add(GetSqlParameterRaw(Convert.ToString(moduleField.FieldName), Convert.ToInt32(moduleField.FK_FieldType_ID), randKey < 0 ? randKey : newData[primaryFieldName]));
                                    }
                                    else
                                    {
                                        if (!String.IsNullOrEmpty(tableFields)) tableFields += ", ";

                                        tableFields += moduleField.FieldName;

                                        if (!String.IsNullOrEmpty(insertParams)) insertParams += ", ";

                                        insertParams += "@" + moduleField.FieldName;

                                        listParams.Add(GetSqlParameter(Convert.ToString(moduleField.FieldName), Convert.ToInt32(moduleField.FK_FieldType_ID), parameterInput.data[Convert.ToString(moduleField.FieldName)]));
                                    }
                                }

                                if (!String.IsNullOrEmpty(tableFields)) tableFields += ", ";

                                tableFields += "Active, CreatedBy, LastUpdateBy, CreatedDate, LastUpdateDate";

                                if (!String.IsNullOrEmpty(insertParams)) insertParams += ", ";

                                insertParams += "@Active, @CreatedBy, @LastUpdateBy, GETDATE(), GETDATE()";

                                listParams.Add(new SqlParameter("@Active", SqlDbType.Bit) { Value = true });

                                listParams.Add(new SqlParameter("@CreatedBy", SqlDbType.VarChar) { Value = userID });

                                listParams.Add(new SqlParameter("@LastUpdateBy", SqlDbType.VarChar) { Value = userID });

                                query = identity ? "SET IDENTITY_INSERT dbo." + moduleName + "_WorkflowAdd_" + CommonBLL.StripTableName(userID) + " ON;" + Environment.NewLine : "";
                                query += "INSERT INTO dbo." + dr["ModuleName"] + "_WorkflowAdd_" + CommonBLL.StripTableName(userID) + " (" + tableFields + ") " + Environment.NewLine;
                                query += "VALUES (" + insertParams + ");" + Environment.NewLine;
                                query += identity ? "SET IDENTITY_INSERT dbo." + moduleName + "_WorkflowAdd_" + CommonBLL.StripTableName(userID) + " OFF;" + Environment.NewLine : "";
                                int rowAffected = NawaDAO.ExecuteNonQuery(query, listParams.ToArray(), connection);
                            }

                            prms = new SqlParameter[]
                            {
                                new SqlParameter("@pkmoduleid", SqlDbType.Int){ Value = parameterInput.ModuleID },
                                new SqlParameter("@pkunikid", SqlDbType.VarChar){ Value = (randKey < 0 ? randKey : newData[primaryFieldName]) },
                                new SqlParameter("@userId", SqlDbType.VarChar){ Value = userID },
                                new SqlParameter("@action", SqlDbType.Int){ Value = parameterInput.ActionType }
                            };
                            int pkworkflowid = Convert.ToInt32(NawaDAO.ExecuteScalar("usp_ValidateMWorkflow", prms, connection, CommandType.StoredProcedure));

                            if (pkworkflowid > 0)
                            {
                                NawaDAO.ExecuteNonQuery("usp_CreateInitWorkflowModuleDesigner '" + pkworkflowid + "','" + (randKey < 0 ? randKey : newData[primaryFieldName]) + "','" + pkMUserID + "','" + parameterInput.ModuleID + "','" + pkMUserID + "','" + parameterInput.Review + "','3'," + lastID + ", '4'", null, connection);
                            }

                            identityTemp = approvalID;

                            SaveMasterDetail(parameterInput.dataDetail, connection, userID, identityTemp, isApproval);
                        }
                        else
                        {
                            string primaryVal = "";
                            isApproval = false;
                            bool isPkIdentity = true;
                            int pkType = 0;
                            string primaryFieldName = moduleFields.Where(x => x.IsPrimaryKey).Select(x => x.FieldName).FirstOrDefault();
                            int primaryFieldType = moduleFields.Where(x => x.IsPrimaryKey).Select(x => x.FK_FieldType_ID).FirstOrDefault();

                            if (parameterInput.ActionType == EActionType.Insert)
                            {
                                string tableFields = "";
                                string insertParams = "";

                                foreach (ModuleField field in moduleFields)
                                {
                                    if (field.IsPrimaryKey)
                                    {
                                        primaryVal = field.FieldName;
                                        pkType = field.FK_FieldType_ID;

                                        if (pkType != (int)EFieldType.IDENTITY && pkType != (int)EFieldType.BIGIDENTITY)
                                        {
                                            isPkIdentity = false;
                                        }
                                    }

                                    if (field.FK_FieldType_ID != (int)EFieldType.IDENTITY && field.FK_FieldType_ID != (int)EFieldType.BIGIDENTITY)
                                    {
                                        if (!String.IsNullOrEmpty(tableFields)) tableFields += ", ";

                                        tableFields += "[" + field.FieldName + "]";

                                        if (!String.IsNullOrEmpty(insertParams)) insertParams += ", ";

                                        insertParams += "@" + field.FieldName;

                                        listParams.Add(GetSqlParameter(field.FieldName, field.FK_FieldType_ID, parameterInput.data[field.FieldName]));
                                    }

                                    if (field.FK_FieldType_ID == (int)EFieldType.VarBinary &&
                                        (field.FK_ExtType_ID == (int)EControlType.DisplayField || field.FK_ExtType_ID == (int)EControlType.FileUpload))
                                    {
                                        #region Insert UploadFile FileName
                                        string fieldFileName = field.FieldName + "Name";

                                        if (!String.IsNullOrEmpty(tableFields)) tableFields += ", ";

                                        tableFields += "[" + fieldFileName + "]";

                                        if (!String.IsNullOrEmpty(insertParams)) insertParams += ", ";

                                        insertParams += "@" + fieldFileName;

                                        listParams.Add(GetSqlParameter(fieldFileName, 9, parameterInput.data[fieldFileName]));
                                        #endregion
                                    }
                                }

                                if (!string.IsNullOrEmpty(tableFields)) tableFields += ", ";

                                tableFields += "Active, CreatedBy, LastUpdateBy, CreatedDate, LastUpdateDate";

                                if (!string.IsNullOrEmpty(insertParams)) insertParams += ", ";

                                insertParams += "@Active, @CreatedBy, @LastUpdateBy, GETDATE(), GETDATE()";

                                listParams.Add(new SqlParameter("@Active", SqlDbType.Bit) { Value = true });
                                listParams.Add(new SqlParameter("@CreatedBy", SqlDbType.VarChar) { Value = userID });
                                listParams.Add(new SqlParameter("@LastUpdateBy", SqlDbType.VarChar) { Value = userID });

                                query = "INSERT INTO dbo." + moduleName + " (" + tableFields + ") " + Environment.NewLine;
                                query += "VALUES (" + insertParams + ") ";
                                query += "SELECT SCOPE_IDENTITY()";
                            }
                            else if (parameterInput.ActionType == EActionType.Update)
                            {
                                string updateFields = "";
                                string updateKey = "";

                                #region Audit Trail
                                query = "SELECT * FROM " + moduleName + " WHERE " + primaryFieldName + " = @" + primaryFieldName;
                                prms = new SqlParameter[]
                                {
                                    GetSqlParameter(primaryFieldName, primaryFieldType, parameterInput.data[primaryFieldName])
                                };
                                DataRow row = NawaDAO.ExecuteRow(query, prms, connection);
                                auditTrailOld = row.Table;
                                #endregion

                                foreach (ModuleField field in moduleFields)
                                {
                                    if (field.IsPrimaryKey)
                                    {
                                        updateKey = "[" + field.FieldName + "] = @" + field.FieldName;
                                        primaryVal = field.FieldName;

                                        if (pkType != (int)EFieldType.IDENTITY && pkType != (int)EFieldType.BIGIDENTITY)
                                        {
                                            isPkIdentity = false;
                                        }
                                    }
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(updateFields)) updateFields += ", " + Environment.NewLine;

                                        updateFields += "[" + field.FieldName + "] = @" + field.FieldName;
                                    }

                                    listParams.Add(GetSqlParameter(field.FieldName, field.FK_FieldType_ID, parameterInput.data[field.FieldName]));

                                    if (field.FK_FieldType_ID == (int)EFieldType.VarBinary &&
                                        (field.FK_ExtType_ID == (int)EControlType.DisplayField || field.FK_ExtType_ID == (int)EControlType.FileUpload))
                                    {
                                        #region Update FileUpload FileName
                                        string fieldFileName = field.FieldName + "Name";

                                        if (!String.IsNullOrEmpty(updateFields)) updateFields += ", " + Environment.NewLine;

                                        updateFields += "[" + fieldFileName + "] = @" + fieldFileName;

                                        listParams.Add(GetSqlParameter(fieldFileName, 9, parameterInput.data[fieldFileName]));
                                        #endregion
                                    }
                                }

                                if (!string.IsNullOrEmpty(updateFields)) updateFields += ", " + Environment.NewLine;

                                updateFields += "LastUpdateBy = @LastUpdateBy, " + Environment.NewLine;
                                updateFields += "LastUpdateDate = GETDATE() ";

                                listParams.Add(new SqlParameter("@LastUpdateBy", SqlDbType.VarChar) { Value = userID });

                                query = "UPDATE dbo." + moduleName + Environment.NewLine;
                                query += "SET " + updateFields + Environment.NewLine;
                                query += "WHERE " + updateKey + Environment.NewLine;
                                query += " SELECT " + updateKey + " FROM " + moduleName + " WHERE " + updateKey + Environment.NewLine;
                            }

                            if (isPkIdentity)
                            {
                                parameterInput.data[primaryVal] = NawaDAO.ExecuteScalar(query, listParams.ToArray(), connection);
                                identityTemp = parameterInput.data[primaryVal];
                            }
                            else
                            {
                                NawaDAO.ExecuteScalar(query, listParams.ToArray(), connection);
                                identityTemp = CommonBLL.GetObject(pkType, parameterInput.data[primaryVal]);
                            }

                            #region From Validation Summary Detail
                            if (parameterInput.ActionType == EActionType.Update)
                            {
                                query = "UPDATE Web_ValidationReport_RowComplate SET Edited = 1 WHERE ModuleID = @ModuleID AND KeyFieldValue = @KeyFieldValue AND TanggalData < @TanggalData";
                                prms = new SqlParameter[]
                                {
                                    new SqlParameter("@ModuleID", SqlDbType.Int) { Value = parameterInput.ModuleID },
                                    new SqlParameter("@KeyFieldValue", SqlDbType.VarChar) { Value = parameterInput.data[primaryFieldName].ToString() },
                                    new SqlParameter("@TanggalData", SqlDbType.DateTime) { Value = DateTime.Now }
                                };

                                NawaDAO.ExecuteNonQuery(query, prms, connection);
                            }
                            #endregion

                            SaveMasterDetail(parameterInput.dataDetail, connection, userID, identityTemp, isApproval);

                            //After Save
                            ValidateStoreProcedure(2, parameterInput, moduleFields, connection, isApproval, userID);

                            #region Audit Trail
                            query = "SELECT * FROM " + moduleName + " WHERE " + primaryFieldName + " = @" + primaryFieldName;
                            prms = new SqlParameter[]
                            {
                                GetSqlParameterRaw(primaryFieldName, primaryFieldType, identityTemp)
                            };
                            DataRow rowNewData = NawaDAO.ExecuteRow(query, prms, connection);
                            auditTrailNew = rowNewData.Table;
                            #endregion
                        }

                        #region Audit Trail
                        long headerID = AuditTrailBLL.InputAuditTrailHeader(connection, userID, moduleName, parameterInput.ActionType, (approvalID > 0 ? EAuditTrailStatus.WaitingApproval : EAuditTrailStatus.AffectedDatabase), approvalID, userID);

                        AuditTrailBLL.InputAuditTrailDetail(connection, headerID, parameterInput.ActionType, auditTrailNew, auditTrailOld);
                        #endregion

                        connection.Commit();
                        connection.Dispose();

                        return new List<KeyValuePair<long, string>>();
                    }
                    else
                    {
                        throw new Exception("ModuleField data was not found");
                    }
                }
                else
                {
                    throw new Exception("Module data was not found");
                }
            }
            catch
            {
                connection.Rollback();
                connection.Dispose();

                throw;
            }
        }

        public List<KeyValuePair<long, string>> ValidateValidationParameter(string moduleName, CParameterInput parameterInput, List<ModuleField> moduleFields, string userID, NawaConnection connection)
        {
            List<KeyValuePair<long, string>> validationList = new List<KeyValuePair<long, string>>();

            System.Xml.XmlDocument objData = new System.Xml.XmlDocument();
            System.Xml.XmlNode objNodeData = objData.CreateElement("Data");
            objData.AppendChild(objNodeData);
            System.Xml.XmlNode ObjNodeTable = objData.CreateElement(moduleName);
            objNodeData.AppendChild(ObjNodeTable);

            foreach (ModuleField moduleField in moduleFields)
            {
                System.Xml.XmlNode objrow = objData.CreateElement(moduleField.FieldName);
                try
                {
                    objrow.InnerXml = Convert.ToString(parameterInput.data[moduleField.FieldName]);
                }
                catch
                {
                    objrow.InnerText = Convert.ToString(parameterInput.data[moduleField.FieldName]);
                }
                ObjNodeTable.AppendChild(objrow);
            }

            SqlParameter[] sqlParameters = new SqlParameter[]
            {
                new SqlParameter("@moduleID", SqlDbType.Int) {Value = parameterInput.ModuleID},
                new SqlParameter("@userid", SqlDbType.VarChar) {Value = userID}
            };

            int intjmlvalidasi = Convert.ToInt32(NawaDAO.ExecuteScalar("usp_ValidateModuleParameterWithNoData", sqlParameters, connection, CommandType.StoredProcedure));

            if (intjmlvalidasi > 0)
            {
                sqlParameters = new SqlParameter[]
                {
                    new SqlParameter("@xmlData", SqlDbType.Xml) {Value = objData.InnerXml.Replace("'", "''")},
                    new SqlParameter("@moduleID", SqlDbType.Int) {Value = parameterInput.ModuleID},
                    new SqlParameter("@userid", SqlDbType.VarChar) {Value = userID}
                };

                DataTable dt = NawaDAO.ExecuteTable("usp_ValidateModuleParameter_New", sqlParameters, connection, CommandType.StoredProcedure);

                foreach (DataRow dr in dt.Rows)
                {
                    validationList.Add(new KeyValuePair<long, string>(moduleFields.Where(item => item.FieldName == Convert.ToString(dr["FieldName"])).Select(item => item.PK_ModuleField_ID).FirstOrDefault(), Convert.ToString(dr["ValidationMessage"])));
                }
            }

            return validationList;
        }

        public void SaveData(CParameterDetail parameterDetail, string userID, int roleID)
        {
            NawaConnection connection = NawaDAO.CreateConnection();
            connection.BeginTransaction();

            try
            {
                bool isApproval = false;
                long scopeIdentity = 0;
                string query = "SELECT * FROM Module WHERE PK_Module_ID = @ModuleID";
                SqlParameter[] prms = new SqlParameter[]
                {
                    new SqlParameter("@ModuleID", SqlDbType.Int){ Value = parameterDetail.ModuleID }
                };
                DataRow dr = NawaDAO.ExecuteRow(query, prms, connection);

                if (dr != null)
                {
                    query = "SELECT FieldName FROM ModuleField WHERE FK_Module_ID = @ModuleID AND IsPrimaryKey = 1";
                    prms = new SqlParameter[]
                    {
                        new SqlParameter("@ModuleID", SqlDbType.Int) { Value = parameterDetail.ModuleID }
                    };
                    string primaryFieldName = moduleFields.Where(item => item.IsPrimaryKey).Select(item => item.FieldName).FirstOrDefault();

                    if (!string.IsNullOrEmpty(primaryFieldName))
                    {
                        #region Check Data in ModuleApproval
                        query = "SELECT COUNT(13) AS DataCount FROM ModuleApproval WHERE (ModuleName = @ModuleName OR FK_Module_ID = @FK_Module_ID) AND ModuleKey = @ModuleKey";
                        prms = new SqlParameter[]
                        {
                            new SqlParameter("@ModuleName", SqlDbType.VarChar) { Value = moduleName },
                            new SqlParameter("@FK_Module_ID", SqlDbType.Int) { Value = parameterDetail.ModuleID },
                            new SqlParameter("@ModuleKey", SqlDbType.VarChar) { Value = parameterDetail.data }
                        };
                        int countDuplicate = Convert.ToInt32(NawaDAO.ExecuteScalar(query, prms, connection));

                        if (countDuplicate > 0)
                        {
                            SystemDictionary sysDictionary = SystemParameterBLL.GetSystemDictionary("unique-data-approval");

                            throw new Exception(sysDictionary.TextDefinition);
                        }
                        #endregion

                        #region Check Data in Upload Approval
                        SystemParameter sysParamUploadUser = SystemParameterBLL.GetSystemParameterValue(53);
                        string tableUploadUser = sysParamUploadUser != null ? sysParamUploadUser.SettingValue : "0";
                        string moduleNameUploadApproval = moduleName;

                        if (tableUploadUser == "1")
                        {
                            moduleNameUploadApproval += "_Upload_data_" + CommonBLL.StripTableName(userID) + "_approval";
                        }
                        else
                        {
                            moduleNameUploadApproval += "_Upload_approval";
                        }

                        query = @"IF OBJECT_ID(@TableUploadName, 'U') IS NULL
                                    BEGIN
	                                    SELECT 0 AS DataCount
                                    END
                                    ELSE
                                    BEGIN
                                        SELECT COUNT(13) AS DataCount FROM " + moduleNameUploadApproval + " WHERE " + primaryFieldName + @" = @ID
                                    END";
                        prms = new SqlParameter[]
                        {
                            new SqlParameter("@ID", SqlDbType.VarChar) { Value = parameterDetail.data },
                            new SqlParameter("@TableUploadName", SqlDbType.VarChar) { Value = moduleNameUploadApproval }
                        };
                        countDuplicate = Convert.ToInt32(NawaDAO.ExecuteScalar(query, prms, connection));

                        if (countDuplicate > 0)
                        {
                            SystemDictionary sysDictionary = SystemParameterBLL.GetSystemDictionary("unique-data-approval");

                            throw new Exception(sysDictionary.TextDefinition);
                        }
                        #endregion

                        #region Initialize Audit Trail Variables
                        DataTable auditTrailOld = new DataTable();
                        DataTable auditTrailNew = new DataTable();
                        #endregion

                        if (Convert.ToBoolean(dr["IsUseApproval"]) && roleID != 1)
                        {
                            isApproval = true;
                            query = "SELECT * FROM " + moduleName + " WHERE " + primaryFieldName + " = @" + primaryFieldName;
                            prms = new SqlParameter[]
                            {
                                new SqlParameter(primaryFieldName, SqlDbType.Int) { Value = parameterDetail.data }
                            };
                            DataRow row = NawaDAO.ExecuteRow(query, prms, connection);

                            if (row != null)
                            {
                                string oldJSON = JsonConvert.SerializeObject(row.Table);
                                string newJSON = "";
                                auditTrailOld = row.Table;

                                if (parameterDetail.ActionType == EActionType.Activation)
                                {
                                    newJSON = oldJSON;
                                    auditTrailNew = row.Table;
                                }
                                else if (parameterDetail.ActionType == EActionType.Delete)
                                {
                                    newJSON = "";
                                }

                                query = "INSERT INTO ModuleApproval(ModuleName, ModuleKey, ModuleField, ModuleFieldBefore, PK_ModuleAction_ID, CreatedDate, CreatedBy, FK_Module_ID) VALUES(@ModuleName, @ModuleKey, @ModuleField, @ModuleFieldBefore, @PK_ModuleAction_ID, @CreatedDate, @CreatedBy, @FK_Module_ID) SELECT SCOPE_IDENTITY() AS LastID";
                                prms = new SqlParameter[]
                                {
                                    new SqlParameter("@ModuleName", SqlDbType.VarChar) { Value = moduleName },
                                    new SqlParameter("@ModuleKey", SqlDbType.VarChar) { Value = parameterDetail.data },
                                    new SqlParameter("@ModuleField", SqlDbType.Text) { Value = newJSON },
                                    new SqlParameter("@ModuleFieldBefore", SqlDbType.Text) { Value = oldJSON },
                                    new SqlParameter("@PK_ModuleAction_ID", SqlDbType.Int) { Value = Convert.ToInt32(parameterDetail.ActionType) },
                                    new SqlParameter("@CreatedDate", SqlDbType.DateTime) { Value = DateTime.Now },
                                    new SqlParameter("@CreatedBy", SqlDbType.VarChar) { Value = userID },
                                    new SqlParameter("@FK_Module_ID", SqlDbType.Int) { Value = parameterDetail.ModuleID }
                                };
                                long lastID = Convert.ToInt64(NawaDAO.ExecuteScalar(query, prms, connection));
                                scopeIdentity = lastID;
                            }
                            else
                            {
                                throw new Exception("Module data was not found");
                            }
                        }
                        else
                        {
                            #region Get old data for audit trail
                            query = "SELECT * FROM " + moduleName + " WHERE " + primaryFieldName + " = @IDData";
                            prms = new SqlParameter[]
                            {
                                new SqlParameter("@IDData", SqlDbType.Int){ Value = parameterDetail.data }
                            };
                            DataRow rowOld = NawaDAO.ExecuteRow(query, prms, connection);

                            if (rowOld != null)
                            {
                                auditTrailOld = rowOld.Table;
                            }
                            #endregion

                            if (parameterDetail.ActionType == EActionType.Delete)
                            {
                                query = "DELETE dbo." + moduleName + " WHERE " + primaryFieldName + " = @IDData";
                                prms = new SqlParameter[]
                                {
                                    new SqlParameter("@IDData", SqlDbType.Int){ Value = parameterDetail.data }
                                };
                            }
                            else if (parameterDetail.ActionType == EActionType.Activation)
                            {
                                query = "UPDATE dbo." + moduleName + " SET Active = CASE WHEN Active = 1 THEN 0 ELSE 1 END WHERE " + primaryFieldName + " = @IDData";
                                prms = new SqlParameter[]
                                {
                                    new SqlParameter("@IDData", SqlDbType.Int){ Value = parameterDetail.data }
                                };
                            }

                            int rowAffected = NawaDAO.ExecuteNonQuery(query, prms, connection);

                            #region Get new data for audit trail
                            query = "SELECT * FROM " + moduleName + " WHERE " + primaryFieldName + " = @IDData";
                            prms = new SqlParameter[]
                            {
                                new SqlParameter("@IDData", SqlDbType.Int){ Value = parameterDetail.data }
                            };
                            DataRow rowNew = NawaDAO.ExecuteRow(query, prms, connection);

                            if (rowNew != null)
                            {
                                auditTrailNew = rowNew.Table;
                            }
                            #endregion
                        }

                        #region Save Module Detail
                        List<ModuleDetail> moduleDetails = GetModuleDetail(parameterDetail.ModuleID);

                        foreach (ModuleDetail moduleDetail in moduleDetails)
                        {
                            SaveDataDetail(moduleDetail, parameterDetail.ActionType, parameterDetail.data, isApproval, scopeIdentity, userID, connection);
                        }
                        #endregion

                        #region Audit Trail
                        long headerID = AuditTrailBLL.InputAuditTrailHeader(connection, userID, moduleName, parameterDetail.ActionType, (scopeIdentity > 0 ? EAuditTrailStatus.WaitingApproval : EAuditTrailStatus.AffectedDatabase), scopeIdentity, userID);

                        AuditTrailBLL.InputAuditTrailDetail(connection, headerID, parameterDetail.ActionType, auditTrailNew, auditTrailOld);
                        #endregion

                        connection.Commit();
                        connection.Dispose();
                    }
                    else
                    {
                        throw new Exception("Module doesn't have a primary key");
                    }
                }
                else
                {
                    throw new Exception("Module data was not found");
                }
            }
            catch
            {
                connection.Rollback();
                connection.Dispose();

                throw;
            }
        }


        public void SaveMasterDetail(List<CBaseDetailInput> dataDetails, NawaConnection connection, string userID, object identityTemp, bool isApproval)
        {
            try
            {
                #region Save Master Detail
                foreach (CBaseDetailInput dataDetail in dataDetails)
                {
                    int pkModuleDetail = dataDetail.ModuleDetailID;
                    string query = "SELECT * FROM ModuleDetailField WHERE FK_ModuleDetail_ID = @ModuleDetailID";
                    SqlParameter[] prms = new SqlParameter[]
                    {
                        new SqlParameter("@ModuleDetailID", SqlDbType.Int) { Value = dataDetail.ModuleDetailID }
                    };
                    DataTable dt = NawaDAO.ExecuteTable(query, prms, connection);
                    List<ModuleDetailField> moduleDetailFields = NawaDAO.ConvertDataTable<ModuleDetailField>(dt);
                    List<ModuleDetailField> filtered = moduleDetailFields.FindAll(e => e.FK_FieldType_ID == Convert.ToInt32(EFieldType.ReferenceTable));
                    Dictionary<string, List<COption>> listOption = new Dictionary<string, List<COption>>();

                    foreach (ModuleDetailField data in filtered)
                    {
                        List<COption> list = GetListOptionsModuleDetail(data.PK_ModuleDetailField_ID, " ");

                        listOption.Add(data.FieldName, list);
                    }

                    foreach (Dictionary<string, object> dataReady in dataDetail.data.added)
                    {
                        SaveDataDetail(pkModuleDetail, dataReady, EActionType.Insert, userID, identityTemp, isApproval, dt, listOption, connection);
                    }

                    // EDIT DATA TO MODULE DETAIL
                    foreach (Dictionary<string, object> dataReady in dataDetail.data.edited)
                    {
                        SaveDataDetail(pkModuleDetail, dataReady, EActionType.Update, userID, identityTemp, isApproval, dt, listOption, connection);
                    }

                    // DELETE DATA TO MODULE DETAIL
                    foreach (Dictionary<string, object> dataReady in dataDetail.data.deleted)
                    {
                        SaveDataDetail(pkModuleDetail, dataReady, EActionType.Delete, userID, identityTemp, isApproval, dt, listOption, connection);
                    }
                }
                #endregion
            }
            catch
            {
                throw;
            }
        }


        public void SaveDataDetail(ModuleDetail moduleDetail, EActionType type, long id, bool isApproval, long scopeIdentity, string userID, NawaConnection connection)
        {
            try
            {
                string moduleDetailName = moduleDetail.ModuleDetailName;
                string moduleJoin = "";
                string foreignKey = "";
                string query = "SELECT * FROM ModuleDetailField WHERE FK_ModuleDetail_ID = @ModuleDetailID";
                SqlParameter[] prms = new SqlParameter[]
                {
                    new SqlParameter("@ModuleDetailID", SqlDbType.Int){ Value = moduleDetail.PK_ModuleDetail_ID }
                };
                DataTable dtFields = NawaDAO.ExecuteTable(query, prms);

                foreach (DataRow drField in dtFields.Rows)
                {
                    if (Convert.ToInt32(drField["FK_FieldType_ID"]) == 11)
                    {
                        string referenceAlias = string.IsNullOrWhiteSpace(Convert.ToString(drField["TabelReferenceNameAlias"])) ? Convert.ToString(drField["TabelReferenceName"]) : Convert.ToString(drField["TabelReferenceNameAlias"]);
                        moduleJoin += " LEFT JOIN " + drField["TabelReferenceName"] + " " + drField["TabelReferenceNameAlias"] + " ON " + moduleDetailName + "." + drField["FieldName"] + " = " + referenceAlias + "." + drField["TableReferenceFieldKey"];
                    }
                    else if (Convert.ToInt32(drField["FK_FieldType_ID"]) == 16)
                    {
                        foreignKey = drField["FieldName"].ToString();
                    }
                }

                if (isApproval)
                {
                    query = "SELECT FieldName FROM ModuleDetailField WHERE FK_ModuleDetail_ID = @ModuleDetailID AND IsPrimaryKey = 1";
                    prms = new SqlParameter[]
                    {
                        new SqlParameter("@ModuleDetailID", SqlDbType.Int) { Value = moduleDetail.PK_ModuleDetail_ID }
                    };
                    string primaryFieldName = Convert.ToString(NawaDAO.ExecuteScalar(query, prms, connection));
                    query = "SELECT * FROM " + moduleDetailName + " WHERE " + foreignKey + " = @" + foreignKey;
                    prms = new SqlParameter[]
                    {
                       new SqlParameter("@" + foreignKey, SqlDbType.Int) { Value = id }
                    };
                    DataTable dt = NawaDAO.ExecuteTable(query, prms, connection);
                    List<DynamicDictionary> res = dt.AsEnumerable().Select(row => new DynamicDictionary(row)).ToList();

                    foreach (DynamicDictionary row in res)
                    {
                        string oldJSON = "[" + JsonConvert.SerializeObject(row.Dictionary) + "]";
                        string newJSON = "";

                        if (type == EActionType.Activation)
                        {
                            newJSON = oldJSON;
                        }
                        else if (type == EActionType.Delete)
                        {
                            newJSON = "";
                        }

                        query = @"INSERT INTO ModuleDetailApproval(ModuleDetailName, ModuleDetailKey, ModuleDetailField, ModuleDetailFieldBefore, FK_ModuleApproval_ID, PK_ModuleAction_ID, CreatedDate, CreatedBy, FK_ModuleDetail_ID)
                                VALUES(@ModuleDetailName, @ModuleDetailKey, @ModuleDetailField, @ModuleDetailFieldBefore, @FK_ModuleApproval_ID, @PK_ModuleAction_ID, @CreatedDate, @CreatedBy, @FK_ModuleDetail_ID)";
                        prms = new SqlParameter[]
                        {
                            new SqlParameter("@ModuleDetailName", SqlDbType.VarChar) { Value = moduleDetailName },
                            new SqlParameter("@ModuleDetailKey", SqlDbType.VarChar) { Value = id},
                            new SqlParameter("@ModuleDetailField", SqlDbType.Text) { Value = newJSON },
                            new SqlParameter("@ModuleDetailFieldBefore", SqlDbType.Text) { Value = oldJSON },
                            new SqlParameter("@FK_ModuleApproval_ID", SqlDbType.BigInt) { Value = scopeIdentity },
                            new SqlParameter("@PK_ModuleAction_ID", SqlDbType.Int) { Value = Convert.ToInt32(type) },
                            new SqlParameter("@CreatedDate", SqlDbType.DateTime) { Value = DateTime.Now },
                            new SqlParameter("@CreatedBy", SqlDbType.VarChar) { Value = userID },
                            new SqlParameter("@FK_ModuleDetail_ID", SqlDbType.VarChar) { Value = moduleDetail.PK_ModuleDetail_ID }
                        };
                        int rowAffected = NawaDAO.ExecuteNonQuery(query, prms, connection);
                    }
                }
                else
                {
                    if (type == EActionType.Delete)
                    {
                        query = "DELETE dbo." + moduleDetailName + " WHERE " + foreignKey + " = @ModuleID";
                        prms = new SqlParameter[]
                        {
                            new SqlParameter("@ModuleID", SqlDbType.Int){ Value = id }
                        };
                    }
                    else if (type == EActionType.Activation)
                    {
                        query = "UPDATE dbo." + moduleDetailName + " SET Active = CASE WHEN Active = 1 THEN 0 ELSE 1 END WHERE " + foreignKey + " = @ModuleID";
                        prms = new SqlParameter[]
                        {
                            new SqlParameter("@ModuleID", SqlDbType.Int){ Value = id }
                        };
                    }

                    int rowAffected = NawaDAO.ExecuteNonQuery(query, prms, connection);
                }
            }
            catch
            {
                throw;
            }
        }

        public void SaveDataDetail(int moduleDetailID, Dictionary<string, object> data, EActionType actionType, string userID, object scopeIdentity, bool isApproval, DataTable dtFields, Dictionary<string, List<COption>> listOptions, NawaConnection connection)
        {
            //NawaConnection connection = NawaDAO.CreateConnection();
            //connection.BeginTransaction();

            try
            {
                string query = "SELECT * FROM ModuleDetail WHERE PK_ModuleDetail_ID = @ModuleDetailID";
                SqlParameter[] prms = new SqlParameter[]
                {
                    new SqlParameter("@ModuleDetailID", SqlDbType.Int){ Value = moduleDetailID }
                };
                DataRow dr = NawaDAO.ExecuteRow(query, prms);

                if (dr != null)
                {
                    ModuleDetailView moduleDetail = NawaDAO.ConvertDataTable<ModuleDetailView>(dr);

                    //query = "SELECT * FROM ModuleDetailField WHERE FK_ModuleDetail_ID = @ModuleDetailID";
                    //prms = new SqlParameter[] { new SqlParameter("@ModuleDetailID", SqlDbType.Int) { Value = ModuleDetailID } };

                    //DataTable dtFields = NawaDAO.ExecuteTable(query, prms, connection);
                    //List<ModuleDetailField> moduleDetailFields = NawaDAO.ConvertDataTable<ModuleDetailField>(dtFields);

                    string sUniqueCheck = "";
                    string sPrimaryCheck = "";
                    List<SqlParameter> listParams = new List<SqlParameter>();
                    SqlParameter primaryParam = null;

                    foreach (DataRow field in dtFields.Rows)
                    {
                        if (Convert.ToBoolean(field["IsUnique"]) && Convert.ToInt32(field["FK_FieldType_ID"]) != 12 && Convert.ToInt32(field["FK_FieldType_ID"]) != 15)
                        {
                            if (!String.IsNullOrEmpty(sUniqueCheck)) sUniqueCheck += " OR ";

                            sUniqueCheck += field["FieldName"] + " = @" + field["FieldName"];

                            listParams.Add(GetSqlParameter(Convert.ToString(field["FieldName"]), Convert.ToInt32(field["FK_FieldType_ID"]), data[Convert.ToString(field["FieldName"])]));
                        }

                        if (Convert.ToBoolean(field["IsPrimaryKey"]))
                        {
                            sPrimaryCheck = field["FieldName"] + " <> @" + field["FieldName"];
                            primaryParam = GetSqlParameter(Convert.ToString(field["FieldName"]), Convert.ToInt32(field["FK_FieldType_ID"]), data[Convert.ToString(field["FieldName"])]);
                        }
                    }

                    if (!String.IsNullOrEmpty(sUniqueCheck))
                    {
                        query = "SELECT COUNT(13) AS DataCount FROM " + moduleDetail.ModuleDetailName + " WHERE (" + sUniqueCheck + ")";

                        if (actionType == EActionType.Update)
                        {
                            query += " AND " + sPrimaryCheck;
                            if (!listParams.Any(item => item.ParameterName == primaryParam.ParameterName)) listParams.Add(primaryParam);
                        }

                        int countDuplicate = Convert.ToInt32(NawaDAO.ExecuteScalar(query, listParams.ToArray(), connection));

                        if (countDuplicate > 0)
                        {
                            throw new Exception("Data Already Exists!");
                        }
                    }

                    listParams = new List<SqlParameter>();
                    string primaryVal = "";

                    if (isApproval)
                    {
                        string oldJSON = "";
                        isApproval = true;
                        query = "SELECT FieldName FROM ModuleDetailField WHERE FK_ModuleDetail_ID = @ModuleDetailID AND IsPrimaryKey = 1";
                        prms = new SqlParameter[] { new SqlParameter("@ModuleDetailID", SqlDbType.Int) { Value = moduleDetailID } };
                        string primaryFieldName = Convert.ToString(NawaDAO.ExecuteScalar(query, prms, connection));
                        query = "SELECT ModuleDetailName FROM ModuleDetail WHERE PK_ModuleDetail_ID = @ModuleDetailID";
                        prms = new SqlParameter[] { new SqlParameter("@ModuleDetailID", SqlDbType.Int) { Value = moduleDetailID } };
                        string moduleDetailName = Convert.ToString(NawaDAO.ExecuteScalar(query, prms, connection));

                        if (actionType == EActionType.Update)
                        {
                            query = "SELECT * FROM " + moduleDetailName + " WHERE " + primaryFieldName + " = @" + primaryFieldName;
                            prms = new SqlParameter[]
                            {
                                GetSqlParameter(primaryFieldName, (int)EFieldType.IDENTITY, data[primaryFieldName])
                            };
                            DataRow row = NawaDAO.ExecuteRow(query, prms, connection);
                            oldJSON = JsonConvert.SerializeObject(row.Table);

                            //For Audit Trail
                            //DtDetailsOld = row.Table;
                        }

                        int randKey = 0;
                        Dictionary<string, object> newData = new Dictionary<string, object>();

                        foreach (DataRow field in dtFields.Rows)
                        {
                            if (actionType == EActionType.Insert)
                            {
                                if (Convert.ToInt32(field["FK_FieldType_ID"]) == 12 || Convert.ToInt32(field["FK_FieldType_ID"]) == 15)
                                {
                                    randKey = -((new Random()).Next(Int32.MaxValue - 2)) + 1;

                                    newData.Add(Convert.ToString(field["FieldName"]), randKey);

                                    continue;
                                }
                            }
                            if (Convert.ToInt32(field["FK_ExtType_ID"]) == 8)
                            {
                                if (NawaDataBLL.CommonBLL.GetObject(9, data[Convert.ToString(field["FieldName"]) + "Name"]) == DBNull.Value)
                                {
                                    newData.Add(Convert.ToString(field["FieldName"] + "Name"), "");
                                }
                                else
                                {
                                    newData.Add(Convert.ToString(field["FieldName"] + "Name"), NawaDataBLL.CommonBLL.GetObject(9, data[Convert.ToString(field["FieldName"]) + "Name"]));
                                }
                            }

                            if (Convert.ToInt32(field["FK_FieldType_ID"]) == (int)EFieldType.ReferenceTable)
                            {
                                //long primary = Convert.ToInt32(field["PK_ModuleDetailField_ID"]);
                                //List<COption> list = GetListOptionsModuleDetail(primary, " ");

                                List<COption> list = listOptions[Convert.ToString(field["FieldName"])];
                                COption value = list.FirstOrDefault(e => e.value == data[Convert.ToString(field["FieldName"])].ToString());
                                string dataEdited = "";

                                if (value != null)
                                {
                                    dataEdited = "(" + data[Convert.ToString(field["FieldName"])].ToString() + ")" + " - " + value.text;
                                }

                                newData.Add(Convert.ToString(field["FieldName"]), dataEdited);

                                continue;
                            }

                            newData.Add(Convert.ToString(field["FieldName"]), NawaDataBLL.CommonBLL.GetObject(Convert.ToInt32(field["FK_FieldType_ID"]), data[Convert.ToString(field["FieldName"])]));
                        }
                        string newJSON = JsonConvert.SerializeObject(new Dictionary<string, object>[] { newData });

                        if (actionType == EActionType.Delete)
                        {
                            oldJSON = newJSON;
                            newJSON = "";
                        }

                        query = @"INSERT INTO ModuleDetailApproval(ModuleDetailName, ModuleDetailKey, ModSuleDetailField, ModuleDetailFieldBefore, FK_ModuleApproval_ID, PK_ModuleAction_ID, CreatedDate, CreatedBy, FK_ModuleDetail_ID)
                                VALUES(@ModuleDetailName, @ModuleDetailKey, @ModuleDetailField, @ModuleDetailFieldBefore, @FK_ModuleApproval_ID, @PK_ModuleAction_ID, @CreatedDate, @CreatedBy, @FK_ModuleDetail_ID)";
                        prms = new SqlParameter[]
                        {
                            new SqlParameter("@ModuleDetailName", SqlDbType.VarChar) { Value = moduleDetailName },
                            new SqlParameter("@ModuleDetailKey", SqlDbType.VarChar) { Value = (randKey < 0 ? randKey : newData[primaryFieldName]) },
                            new SqlParameter("@ModuleDetailField", SqlDbType.Text) { Value = newJSON },
                            new SqlParameter("@ModuleDetailFieldBefore", SqlDbType.Text) { Value = oldJSON },
                            new SqlParameter("@FK_ModuleApproval_ID", SqlDbType.BigInt) { Value = scopeIdentity },
                            new SqlParameter("@PK_ModuleAction_ID", SqlDbType.Int) { Value = Convert.ToInt32(actionType) },
                            new SqlParameter("@CreatedDate", SqlDbType.DateTime) { Value = DateTime.Now },
                            new SqlParameter("@CreatedBy", SqlDbType.VarChar) { Value = userID },
                            new SqlParameter("@FK_ModuleDetail_ID", SqlDbType.Int) { Value = moduleDetailID }
                        };
                        long LastID = Convert.ToInt64(NawaDAO.ExecuteScalar(query, prms, connection));

                        //For Audit Trail
                        //ApprovalID = LastID;
                    }
                    else
                    {
                        if (actionType == EActionType.Insert)
                        {
                            string tableFields = "";
                            string insertParams = "";

                            foreach (DataRow drField in dtFields.Rows)
                            {
                                if (Convert.ToBoolean(drField["IsPrimaryKey"]))
                                {
                                    primaryVal = drField["FieldName"].ToString();
                                }
                                if (Convert.ToInt32(drField["FK_FieldType_ID"]) != 12 && Convert.ToInt32(drField["FK_FieldType_ID"]) != 15 && Convert.ToInt32(drField["FK_FieldType_ID"]) != 16)
                                {
                                    if (!String.IsNullOrEmpty(tableFields)) tableFields += ", ";

                                    tableFields += "[" + drField["FieldName"] + "]";

                                    if (!String.IsNullOrEmpty(insertParams)) insertParams += ", ";

                                    insertParams += "@" + drField["FieldName"];

                                    listParams.Add(GetSqlParameter(Convert.ToString(drField["FieldName"]), Convert.ToInt32(drField["FK_FieldType_ID"]), data[Convert.ToString(drField["FieldName"])]));
                                }
                                if (Convert.ToInt32(drField["FK_FieldType_ID"]) == 14)
                                {
                                    #region Insert UploadFile FileName
                                    string fieldFileName = Convert.ToString(drField["FieldName"]) + "Name";

                                    if (!String.IsNullOrEmpty(tableFields)) tableFields += ", ";

                                    tableFields += "[" + fieldFileName + "]";

                                    if (!String.IsNullOrEmpty(insertParams)) insertParams += ", ";

                                    insertParams += "@" + fieldFileName;

                                    listParams.Add(GetSqlParameter(fieldFileName, 9, data[fieldFileName]));
                                    #endregion
                                }
                                if (Convert.ToInt32(drField["FK_FieldType_ID"]) == 16)
                                {
                                    if (!String.IsNullOrEmpty(tableFields)) tableFields += ", ";

                                    tableFields += "[" + drField["FieldName"] + "]";

                                    if (!String.IsNullOrEmpty(insertParams)) insertParams += ", ";

                                    insertParams += "@" + drField["FieldName"];

                                    listParams.Add(new SqlParameter("@" + Convert.ToString(drField["FieldName"]), SqlDbType.Int) { Value = Convert.ToInt32(scopeIdentity) });
                                }
                            }

                            if (!String.IsNullOrEmpty(tableFields)) tableFields += ", ";

                            tableFields += "Active, CreatedBy, LastUpdateBy, CreatedDate, LastUpdateDate";

                            if (!String.IsNullOrEmpty(insertParams)) insertParams += ", ";

                            insertParams += "@Active, @CreatedBy, @LastUpdateBy, GETDATE(), GETDATE()";

                            listParams.Add(new SqlParameter("@Active", SqlDbType.Bit) { Value = true });
                            listParams.Add(new SqlParameter("@CreatedBy", SqlDbType.VarChar) { Value = userID });
                            listParams.Add(new SqlParameter("@LastUpdateBy", SqlDbType.VarChar) { Value = userID });

                            query = "INSERT INTO dbo." + moduleDetail.ModuleDetailName + " (" + tableFields + ") " + Environment.NewLine;
                            query += "VALUES (" + insertParams + ") ";
                            query += "SELECT SCOPE_IDENTITY()";
                        }
                        else if (actionType == EActionType.Update)
                        {
                            string updateFields = "";
                            string updateKey = "";

                            foreach (DataRow drField in dtFields.Rows)
                            {
                                if (Convert.ToBoolean(drField["IsPrimaryKey"]))
                                {
                                    updateKey = "[" + drField["FieldName"] + "] = @" + drField["FieldName"];
                                    primaryVal = drField["FieldName"].ToString();
                                }
                                else
                                {
                                    if (!String.IsNullOrEmpty(updateFields)) updateFields += ", " + Environment.NewLine;

                                    updateFields += "[" + drField["FieldName"] + "] = @" + drField["FieldName"];
                                }

                                listParams.Add(GetSqlParameter(Convert.ToString(drField["FieldName"]), Convert.ToInt32(drField["FK_FieldType_ID"]), data[Convert.ToString(drField["FieldName"])]));

                                if (Convert.ToInt32(drField["FK_FieldType_ID"]) == 14)
                                {
                                    #region Update FileUpload FileName
                                    string fieldFileName = Convert.ToString(drField["FieldName"]) + "Name";

                                    if (!String.IsNullOrEmpty(updateFields)) updateFields += ", " + Environment.NewLine;

                                    updateFields += "[" + fieldFileName + "] = @" + fieldFileName;

                                    listParams.Add(GetSqlParameter(fieldFileName, 9, data[fieldFileName]));
                                    #endregion
                                }
                            }

                            if (!String.IsNullOrEmpty(updateFields)) updateFields += ", " + Environment.NewLine;

                            updateFields += "LastUpdateBy = @LastUpdateBy, " + Environment.NewLine;
                            updateFields += "LastUpdateDate = GETDATE() ";

                            listParams.Add(new SqlParameter("@LastUpdateBy", SqlDbType.VarChar) { Value = userID });

                            query = "UPDATE dbo." + moduleDetail.ModuleDetailName + Environment.NewLine;
                            query += "SET " + updateFields + Environment.NewLine;
                            query += "WHERE " + updateKey + Environment.NewLine;
                            query += " SELECT " + updateKey + " FROM " + moduleDetail.ModuleDetailName + " WHERE " + updateKey + Environment.NewLine;
                        }
                        else if (actionType == EActionType.Delete)
                        {
                            string updateKey = "";

                            foreach (DataRow drField in dtFields.Rows)
                            {
                                if (Convert.ToBoolean(drField["IsPrimaryKey"]))
                                {
                                    updateKey = "[" + drField["FieldName"] + "] = @" + drField["FieldName"];
                                    primaryVal = drField["FieldName"].ToString();

                                    listParams.Add(GetSqlParameter(Convert.ToString(drField["FieldName"]), Convert.ToInt32(drField["FK_FieldType_ID"]), data[Convert.ToString(drField["FieldName"])]));
                                }
                            }

                            query = "DELETE FROM dbo." + moduleDetail.ModuleDetailName + Environment.NewLine;
                            query += "WHERE " + updateKey + Environment.NewLine;
                        }

                        data[primaryVal] = NawaDAO.ExecuteScalar(query, listParams.ToArray(), connection);
                    }

                }
                //connection.Commit();
                //connection.Dispose();
            }
            catch
            {
                //connection.Rollback();
                //connection.Dispose();
                throw;
            }
        }
        public MemoryStream ExportTemplateData(int moduleID, string UserID, int PK_MUser_ID, int FK_MRole_ID)
        {
            string parentColRefName = "";
            string query = @"SELECT * FROM Module WHERE PK_Module_ID = " + moduleID;
            DataRow drModule = NawaDAO.ExecuteRow(query);
            Module module = NawaDAO.ConvertDataTable<Module>(drModule);

            query = @"SELECT * FROM ModuleDetail WHERE FK_Module_ID = " + moduleID + " AND Active = 1";
            DataTable dtListModuleDetail = NawaDAO.ExecuteTable(query);
            List<ModuleDetail> moduleDetails = NawaDAO.ConvertDataTable<ModuleDetail>(dtListModuleDetail);

            foreach (ModuleDetail moduleDetail in moduleDetails)
            {
                query = @"SELECT * FROM ModuleDetailField WHERE FK_ModuleDetail_ID = " + moduleDetail.PK_ModuleDetail_ID + " ORDER BY Sequence";
                DataTable dtListModuleDetailField = NawaDAO.ExecuteTable(query);
                moduleDetail.ListModuleDetailField = NawaDAO.ConvertDataTable<ModuleDetailField>(dtListModuleDetailField);
            }

            query = "SELECT * FROM ModuleFieldDefault";
            DataTable dtModuleFieldDefaults = NawaDAO.ExecuteTable(query);
            List<ModuleFieldDefault> moduleFieldDefaults = NawaDAO.ConvertDataTable<ModuleFieldDefault>(dtModuleFieldDefaults);

            query = GenerateSqlImportData(module, moduleFields, moduleFieldDefaults, UserID, PK_MUser_ID, FK_MRole_ID);

            DataTable objtb = NawaDAO.ExecuteTable(query);

            int intmaxrow = 0;
            if (objtb.Rows.Count + 1000 > 1048575)
            {
                intmaxrow = 1048575;
            }
            else
            {
                intmaxrow = objtb.Rows.Count + 1000;
            }

            IWorkbook workbook = new XSSFWorkbook();
            ISheet objWorksheetData = workbook.CreateSheet("Data");
            foreach (ModuleDetail moduleDetail in moduleDetails)
            {
                workbook.CreateSheet(moduleDetail.ModuleDetailLabel);
            }
            ISheet objWorksheetParam = workbook.CreateSheet("Parameter");
            IName name;

            #region Dropdown Reference in Parameter Sheet
            int intCounterReference = 0;
            List<IRow> parameterRows = new List<IRow>();

            if (moduleFields.Any(item => item.FK_FieldType_ID == (int)EFieldType.ReferenceTable && item.FK_ExtType_ID != (int)EControlType.PopUpMultiCheckBox))
            {
                parameterRows.Add(objWorksheetParam.CreateRow(0));
                foreach (ModuleField item in moduleFields.Where(item => item.FK_FieldType_ID == (int)EFieldType.ReferenceTable && item.FK_ExtType_ID != (int)EControlType.PopUpMultiCheckBox).ToList())
                {
                    ICell cell = parameterRows[0].CreateCell(intCounterReference, CellType.String);
                    cell.SetCellValue(item.FieldLabel);

                    int intx = 1;
                    string stralias = String.IsNullOrEmpty(item.TabelReferenceNameAlias) ? item.TabelReferenceName : item.TabelReferenceNameAlias;
                    string strfilter = item.TableReferenceFilter.Replace("@userid", UserID);
                    strfilter = strfilter.Replace("@PK_MUser_ID", PK_MUser_ID.ToString());

                    query = GetQueryRef(item.TabelReferenceName + " " + stralias, item.TableReferenceFieldKey, item.TableReferenceFieldDisplayName, item.TableReferenceAdditonalJoin, strfilter, item.BCasCade, UserID, PK_MUser_ID);
                    DataTable objdata = NawaDAO.ExecuteTable(query);

                    foreach (DataRow item1 in objdata.Rows)
                    {
                        if (intx >= parameterRows.Count) parameterRows.Add(objWorksheetParam.CreateRow(intx));

                        parameterRows[intx].CreateCell(intCounterReference, CellType.String).SetCellValue("(" + item1[item.TableReferenceFieldKey] + ") " + item1[item.TableReferenceFieldDisplayName]);

                        intx++;
                    }

                    name = workbook.CreateName();
                    name.NameName = item.FieldName;
                    name.RefersToFormula = "Parameter!$" + GetExcelColumnName(intCounterReference + 1) + "$2:$" + GetExcelColumnName(intCounterReference + 1) + "$" + intx;
                    name.Comment = "Parameter!" + GetExcelColumnName(intCounterReference + 1) + "2:" + GetExcelColumnName(intCounterReference + 1) + intx;

                    intCounterReference++;
                }
            }

            foreach (ModuleDetail moduleDetail in moduleDetails)
            {
                if (moduleDetail.ListModuleDetailField.Any(item => item.FK_FieldType_ID == (int)EFieldType.ReferenceTable && item.FK_ExtType_ID != (int)EControlType.PopUpMultiCheckBox))
                {
                    foreach (ModuleDetailField item in moduleDetail.ListModuleDetailField.Where(item => item.FK_FieldType_ID == (int)EFieldType.ReferenceTable && item.FK_ExtType_ID != (int)EControlType.PopUpMultiCheckBox).ToList())
                    {
                        parameterRows[0].CreateCell(intCounterReference, CellType.String).SetCellValue(item.FieldLabel);

                        int intx = 1;
                        string stralias = String.IsNullOrEmpty(item.TabelReferenceNameAlias) ? item.TabelReferenceName : item.TabelReferenceNameAlias;
                        string strfilter = item.TableReferenceFilter.Replace("@userid", UserID);
                        strfilter = strfilter.Replace("@PK_MUser_ID", PK_MUser_ID.ToString());

                        query = GetQueryRef(item.FieldNameParent + " " + stralias, item.TableReferenceFieldKey, item.TableReferenceFieldDisplayName, item.TableReferenceAdditonalJoin, strfilter, item.BCasCade, UserID, PK_MUser_ID);
                        DataTable objdata = NawaDAO.ExecuteTable(query);

                        foreach (DataRow item1 in objdata.Rows)
                        {
                            if (intx >= parameterRows.Count) parameterRows.Add(objWorksheetParam.CreateRow(intx));

                            parameterRows[intx].CreateCell(intCounterReference, CellType.String).SetCellValue("(" + item1[item.TableReferenceFieldKey] + ") " + item1[item.TableReferenceFieldDisplayName]);

                            intx++;
                        }

                        name = workbook.CreateName();
                        name.NameName = moduleDetail.ModuleDetailName + "_" + item.FieldName;
                        name.RefersToFormula = "Parameter!$" + GetExcelColumnName(intCounterReference + 1) + "$2:$" + GetExcelColumnName(intCounterReference + 1) + "$" + intx;
                        name.Comment = "Parameter!" + GetExcelColumnName(intCounterReference + 1) + "2:" + GetExcelColumnName(intCounterReference + 1) + intx;

                        intCounterReference++;
                    }
                }
            }
            #endregion

            #region Action Reference in Parameter Sheet
            SystemParameter objParamSettingUpload = SystemParameterBLL.GetSystemParameterValue(31);
            bool bParam = false;

            if (objParamSettingUpload != null) bParam = Convert.ToBoolean(objParamSettingUpload.SettingValue);

            int intcounterx = 0;

            if (bParam)
            {
                if (intcounterx >= parameterRows.Count) parameterRows.Add(objWorksheetParam.CreateRow(intcounterx));

                parameterRows[intcounterx].CreateCell(intCounterReference, CellType.String).SetCellValue("Action");

                if (module.IsSupportAdd)
                {
                    intcounterx++;
                    if (intcounterx >= parameterRows.Count) parameterRows.Add(objWorksheetParam.CreateRow(intcounterx));
                    parameterRows[intcounterx].CreateCell(intCounterReference, CellType.String).SetCellValue("Insert");
                }

                if (module.IsSupportEdit)
                {
                    intcounterx++;
                    if (intcounterx >= parameterRows.Count) parameterRows.Add(objWorksheetParam.CreateRow(intcounterx));
                    parameterRows[intcounterx].CreateCell(intCounterReference, CellType.String).SetCellValue("Update");
                }

                if (module.IsSupportDelete)
                {
                    intcounterx++;
                    if (intcounterx >= parameterRows.Count) parameterRows.Add(objWorksheetParam.CreateRow(intcounterx));
                    parameterRows[intcounterx].CreateCell(intCounterReference, CellType.String).SetCellValue("Delete");
                }
            }
            else
            {
                if (intcounterx >= parameterRows.Count) parameterRows.Add(objWorksheetParam.CreateRow(intcounterx));
                parameterRows[intcounterx].CreateCell(intCounterReference, CellType.String).SetCellValue("Action");
                intcounterx++;
                if (intcounterx >= parameterRows.Count) parameterRows.Add(objWorksheetParam.CreateRow(intcounterx));
                parameterRows[intcounterx].CreateCell(intCounterReference, CellType.String).SetCellValue("Insert");
                intcounterx++;
                if (intcounterx >= parameterRows.Count) parameterRows.Add(objWorksheetParam.CreateRow(intcounterx));
                parameterRows[intcounterx].CreateCell(intCounterReference, CellType.String).SetCellValue("Update");
                intcounterx++;
                if (intcounterx >= parameterRows.Count) parameterRows.Add(objWorksheetParam.CreateRow(intcounterx));
                parameterRows[intcounterx].CreateCell(intCounterReference, CellType.String).SetCellValue("Delete");
            }

            name = workbook.CreateName();
            name.NameName = "Action";
            name.RefersToFormula = "Parameter!$" + GetExcelColumnName(intCounterReference + 1) + "$2:$" + GetExcelColumnName(intCounterReference + 1) + "$" + (intcounterx + 1);
            #endregion

            #region Parent Reference in Data Sheet
            if (moduleDetails.Count > 0)
            {
                ModuleField PrimaryField = moduleFields.Where(x => x.IsPrimaryKey).FirstOrDefault();

                if (PrimaryField != null)
                {
                    ISheet objWorksheetKeyDisplay = workbook.CreateSheet("keyDisplay");
                    List<IRow> detailRows = new List<IRow>();
                    int counterParentRef = 0;

                    foreach (ModuleDetail moduleDetail in moduleDetails)
                    {
                        if (moduleDetail.ListModuleDetailField.Any(item => item.FK_FieldType_ID == (int)EFieldType.ParentReference && item.FK_ExtType_ID != (int)EControlType.PopUpMultiCheckBox))
                        {
                            detailRows.Add(objWorksheetKeyDisplay.CreateRow(0));

                            foreach (ModuleDetailField item in moduleDetail.ListModuleDetailField.Where(item => item.FK_FieldType_ID == (int)EFieldType.ParentReference && item.FK_ExtType_ID != (int)EControlType.PopUpMultiCheckBox).ToList())
                            {
                                ICell cell = detailRows[0].CreateCell(counterParentRef, CellType.String);
                                cell.SetCellValue(item.FieldLabel);
                                int intx = 1;
                                string stralias = String.IsNullOrEmpty(item.TableReferenceFieldDisplayName) ? item.FieldNameParent : item.TabelReferenceNameAlias;
                                string strfilter = item.TableReferenceFilter.Replace("@userid", UserID);
                                strfilter = strfilter.Replace("@PK_MUser_ID", PK_MUser_ID.ToString());
                                query = GetQueryRef(item.TabelReferenceName + " " + stralias, item.TableReferenceFieldKey, item.TableReferenceFieldDisplayName, item.TableReferenceAdditonalJoin, strfilter, item.BCasCade, UserID, PK_MUser_ID);
                                DataTable objdata = NawaDAO.ExecuteTable(query);

                                foreach (DataRow item1 in objdata.Rows)
                                {
                                    if (intx >= detailRows.Count) detailRows.Add(objWorksheetKeyDisplay.CreateRow(intx));

                                    detailRows[intx].CreateCell(counterParentRef, CellType.String).SetCellValue("(" + item1[item.TableReferenceFieldKey] + ") " + item1[item.FieldNameParent]);
                                    intx++;
                                }

                                name = workbook.CreateName();
                                name.NameName = "ParentReference_" + moduleDetail.ModuleDetailName + "_" + item.FieldName;
                                name.RefersToFormula = "keyDisplay!$" + GetExcelColumnName(counterParentRef + 1) + "$2:$" + GetExcelColumnName(counterParentRef + 1) + "$" + intx;
                                name.Comment = "keyDisplay!" + GetExcelColumnName(counterParentRef + 1) + "2:" + GetExcelColumnName(counterParentRef + 1) + intx;

                                counterParentRef++;
                            }
                        }
                    }
                }
            }
            #endregion

            XSSFFont headerFont = (XSSFFont)workbook.CreateFont();
            headerFont.IsBold = true;
            headerFont.SetColor(new XSSFColor(IndexedColors.White));
            XSSFCellStyle headerStyle = (XSSFCellStyle)workbook.CreateCellStyle();
            headerStyle.SetFont(headerFont);
            headerStyle.FillPattern = FillPattern.SolidForeground;
            headerStyle.SetFillForegroundColor(new XSSFColor(IndexedColors.Blue));

            IDataFormat dataFormatCustom = workbook.CreateDataFormat();
            XSSFCellStyle dateStyle = (XSSFCellStyle)workbook.CreateCellStyle();
            dateStyle.DataFormat = dataFormatCustom.GetFormat(SystemParameterBLL.GetSystemParameterValue(6).SettingValue);

            IRow headerRow = objWorksheetData.CreateRow(0);
            ICell firstCell = headerRow.CreateCell(0, CellType.String);
            firstCell.SetCellValue("Action");
            firstCell.CellStyle = headerStyle;
            //firstCell.CellComment = ...

            XSSFDataValidationHelper dvHelper = new XSSFDataValidationHelper((XSSFSheet)objWorksheetData);
            XSSFDataValidationConstraint dvConstraint = (XSSFDataValidationConstraint)dvHelper.CreateFormulaListConstraint("Action");
            CellRangeAddressList addressList = new CellRangeAddressList(1, intmaxrow - 1, 0, 0);
            XSSFDataValidation validation = (XSSFDataValidation)dvHelper.CreateValidation(dvConstraint, addressList);
            validation.ShowErrorBox = true;
            objWorksheetData.AddValidationData(validation);

            int intDataY = 1;

            foreach (ModuleField item in moduleFields.OrderBy(item => item.Sequence).ToList())
            {
                ICell headerCell = headerRow.CreateCell(intDataY, CellType.String);
                headerCell.SetCellValue(item.FieldLabel);
                headerCell.CellStyle = headerStyle;

                switch (item.FK_FieldType_ID)
                {
                    case (int)EFieldType.ReferenceTable:
                        if (item.FK_ExtType_ID != (int)EControlType.PopUpMultiCheckBox)
                        {
                            XSSFDataValidationHelper fieldDVHelper = new XSSFDataValidationHelper((XSSFSheet)objWorksheetData);
                            XSSFDataValidationConstraint fieldDVConstraint = (XSSFDataValidationConstraint)fieldDVHelper.CreateFormulaListConstraint(item.FieldName);
                            CellRangeAddressList fieldAddressList = new CellRangeAddressList(1, intmaxrow - 1, intDataY, intDataY);
                            XSSFDataValidation fieldValidation = (XSSFDataValidation)fieldDVHelper.CreateValidation(fieldDVConstraint, fieldAddressList);
                            fieldValidation.ShowErrorBox = false;
                            objWorksheetData.AddValidationData(fieldValidation);
                        }
                        break;
                    default:
                        break;
                }

                intDataY++;
            }

            if (module.IsSupportActivation)
            {
                foreach (ModuleFieldDefault item in moduleFieldDefaults.FindAll(item => item.PK_ModuleField_ID == 1))
                {
                    if (item.FK_FieldType_ID == 13) //BooleanValue
                    {
                        ICell lastCell = headerRow.CreateCell(intDataY, CellType.String);
                        lastCell.SetCellValue(item.FieldLabel);
                        lastCell.CellStyle = headerStyle;
                        intDataY++;
                    }
                }
            }

            foreach (DataRow row in objtb.Rows)
            {
                IRow excelRow = objWorksheetData.CreateRow(objtb.Rows.IndexOf(row) + 1);
                excelRow.CreateCell(0).SetCellValue("Update");

                for (int i = 0; i < objtb.Columns.Count; i++)
                {
                    if (row[i] != DBNull.Value)
                    {
                        ICell excelCell = excelRow.CreateCell(i + 1);

                        switch (Type.GetTypeCode(objtb.Columns[i].DataType))
                        {
                            case TypeCode.Int16:
                            case TypeCode.Int32:
                            case TypeCode.Int64:
                            case TypeCode.Single:
                            case TypeCode.Double:
                                excelCell.SetCellValue(Convert.ToDouble(row[i]));
                                break;
                            case TypeCode.Boolean:
                                excelCell.SetCellValue(Convert.ToBoolean(row[i]));
                                break;
                            case TypeCode.DateTime:
                                excelCell.CellStyle = dateStyle;
                                excelCell.SetCellValue(Convert.ToDateTime(row[i]));
                                break;
                            case TypeCode.String:
                                string val = Convert.ToString(row[i]);
                                if (val.Length > 32767) val = val.Substring(0, 32767);
                                excelCell.SetCellValue(val);
                                break;
                            default:
                                excelCell.SetCellValue(Convert.ToString(row[i]));
                                break;
                        }
                    }
                }
            }

            for (int i = 0; i <= objtb.Columns.Count; i++) objWorksheetData.AutoSizeColumn(i);

            for (int i = 0; i < moduleFields.Count; i++)
            {
                if (!moduleFields[i].IsShowInForm)
                {
                    objWorksheetData.SetColumnHidden(i + 1, true);
                }
            }

            #region Module Detail Sheets
            foreach (ModuleDetail moduleDetail in moduleDetails)
            {
                string SheetName = moduleDetail.ModuleDetailLabel.Length > 31 ? moduleDetail.ModuleDetailLabel.Substring(0, 31) : moduleDetail.ModuleDetailLabel;
                ISheet ObjWorksheetDataDetail = workbook.GetSheet(SheetName);

                query = GenerateSqlImportDataModuleDetail(module, moduleDetail, moduleDetail.ListModuleDetailField, moduleFieldDefaults, UserID, PK_MUser_ID);
                DataTable ObjtbDetail = NawaDAO.ExecuteTable(query);

                IRow DetailHeaderRow = ObjWorksheetDataDetail.CreateRow(0);
                ICell DetailFirstCell = DetailHeaderRow.CreateCell(0, CellType.String);
                DetailFirstCell.SetCellValue("Action");
                DetailFirstCell.CellStyle = headerStyle;

                XSSFDataValidationHelper DetailDvHelper = new XSSFDataValidationHelper((XSSFSheet)ObjWorksheetDataDetail);
                XSSFDataValidationConstraint DetailDvConstraint = (XSSFDataValidationConstraint)DetailDvHelper.CreateFormulaListConstraint("Action");
                CellRangeAddressList DetailAddressList = new CellRangeAddressList(1, intmaxrow - 1, 0, 0);
                XSSFDataValidation DetailValidation = (XSSFDataValidation)DetailDvHelper.CreateValidation(DetailDvConstraint, DetailAddressList);
                DetailValidation.ShowErrorBox = true;
                ObjWorksheetDataDetail.AddValidationData(DetailValidation);

                int IntDataDetailY = 1;

                foreach (ModuleDetailField item in moduleDetail.ListModuleDetailField.OrderBy(item => item.Sequence).ToList())
                {
                    ICell headerCell = DetailHeaderRow.CreateCell(IntDataDetailY, CellType.String);
                    headerCell.SetCellValue(item.FieldLabel);
                    headerCell.CellStyle = headerStyle;

                    switch (item.FK_FieldType_ID)
                    {
                        case (int)EFieldType.ReferenceTable:
                            if (item.FK_ExtType_ID != (int)EControlType.PopUpMultiCheckBox)
                            {
                                XSSFDataValidationHelper DetailFieldDVHelper = new XSSFDataValidationHelper((XSSFSheet)ObjWorksheetDataDetail);
                                XSSFDataValidationConstraint DetailFieldDVConstraint = (XSSFDataValidationConstraint)DetailFieldDVHelper.CreateFormulaListConstraint(moduleDetail.ModuleDetailName + "_" + item.FieldName);
                                CellRangeAddressList DetailFieldAddressList = new CellRangeAddressList(1, intmaxrow - 1, IntDataDetailY, IntDataDetailY);
                                XSSFDataValidation DetailFieldValidation = (XSSFDataValidation)DetailFieldDVHelper.CreateValidation(DetailFieldDVConstraint, DetailFieldAddressList);
                                DetailFieldValidation.ShowErrorBox = false;
                                ObjWorksheetDataDetail.AddValidationData(DetailFieldValidation);
                            }
                            break;
                        case (int)EFieldType.ParentReference:
                            parentColRefName = item.FieldName;
                            XSSFDataValidationHelper ParentDVHelper = new XSSFDataValidationHelper((XSSFSheet)ObjWorksheetDataDetail);
                            XSSFDataValidationConstraint ParentDVConstraint = (XSSFDataValidationConstraint)ParentDVHelper.CreateFormulaListConstraint("ParentReference_" + moduleDetail.ModuleDetailName + "_" + item.FieldName);
                            CellRangeAddressList ParentAddressList = new CellRangeAddressList(1, intmaxrow - 1, IntDataDetailY, IntDataDetailY);
                            XSSFDataValidation ParentValidation = (XSSFDataValidation)ParentDVHelper.CreateValidation(ParentDVConstraint, ParentAddressList);
                            ParentValidation.ShowErrorBox = false;
                            ObjWorksheetDataDetail.AddValidationData(ParentValidation);
                            break;
                        default:
                            break;
                    }

                    IntDataDetailY++;
                }

                if (module.IsSupportActivation)
                {
                    foreach (ModuleFieldDefault item in moduleFieldDefaults.FindAll(item => item.PK_ModuleField_ID == 1))
                    {
                        if (item.FK_FieldType_ID == (int)EFieldType.Boolean)
                        {
                            ICell lastCell = DetailHeaderRow.CreateCell(IntDataDetailY, CellType.String);
                            lastCell.SetCellValue(item.FieldLabel);
                            lastCell.CellStyle = headerStyle;
                            IntDataDetailY++;
                        }
                    }
                }

                foreach (DataRow row in ObjtbDetail.Rows)
                {
                    IRow excelRow = ObjWorksheetDataDetail.CreateRow(ObjtbDetail.Rows.IndexOf(row) + 1);
                    excelRow.CreateCell(0).SetCellValue("Update");

                    for (int i = 0; i < ObjtbDetail.Columns.Count; i++)
                    {
                        if (row[i] != DBNull.Value)
                        {
                            ICell excelCell = excelRow.CreateCell(i + 1);


                            switch (Type.GetTypeCode(ObjtbDetail.Columns[i].DataType))
                            {
                                case TypeCode.Int16:
                                case TypeCode.Int32:
                                case TypeCode.Int64:
                                case TypeCode.Single:
                                case TypeCode.Double:
                                    excelCell.SetCellValue(Convert.ToDouble(row[i]));
                                    break;
                                case TypeCode.Boolean:
                                    excelCell.SetCellValue(Convert.ToBoolean(row[i]));
                                    break;
                                case TypeCode.DateTime:
                                    excelCell.CellStyle = dateStyle;
                                    excelCell.SetCellValue(Convert.ToDateTime(row[i]));
                                    break;
                                case TypeCode.String:
                                    string val = Convert.ToString(row[i]);
                                    if (val.Length > 32767) val = val.Substring(0, 32767);
                                    excelCell.SetCellValue(val);
                                    break;
                                default:
                                    excelCell.SetCellValue(Convert.ToString(row[i]));
                                    break;
                            }


                        }
                    }
                }

                for (int i = 0; i <= ObjtbDetail.Columns.Count; i++) ObjWorksheetDataDetail.AutoSizeColumn(i);

                for (int i = 0; i < moduleDetail.ListModuleDetailField.Count; i++)
                {
                    if (!moduleDetail.ListModuleDetailField[i].IsShowInForm)
                    {
                        ObjWorksheetDataDetail.SetColumnHidden(i + 1, true);
                    }
                }
            }
            #endregion

            MemoryStream ms = new MemoryStream();
            using (MemoryStream tempStream = new MemoryStream())
            {
                workbook.Write(tempStream);
                byte[] byteArray = tempStream.ToArray();
                ms.Write(byteArray, 0, byteArray.Length);
            }

            return ms;
        }

        private string GenerateSqlImportData(Module module, List<ModuleField> moduleFields, List<ModuleFieldDefault> moduleFieldDefaults, string UserID, int PK_MUser_ID, int FK_MRole_ID)
        {
            string strprimarykey = "";
            string strfield = "";
            List<string> arrtablename = new List<string>();
            string strtable = " from " + moduleName + " ";

            foreach (ModuleField item in moduleFields.OrderBy(item => item.Sequence).ToList())
            {
                if (item.IsPrimaryKey)
                {
                    strprimarykey = item.FieldName;
                }

                string stralias = string.IsNullOrEmpty(item.TabelReferenceNameAlias) ? item.TabelReferenceName : item.TabelReferenceNameAlias;

                if (item.FK_FieldType_ID == (int)EFieldType.ReferenceTable)
                {
                    if (item.FK_ExtType_ID == (int)EControlType.PopUpMultiCheckBox)
                    {
                        strfield += moduleName + "." + item.FieldName + ",";
                    }
                    else
                    {
                        string strjoin = "";

                        if (!string.IsNullOrEmpty(item.TableReferenceAdditonalJoin))
                        {
                            strjoin = item.TableReferenceAdditonalJoin.Replace("@userid", UserID);
                            strjoin = strjoin.Replace("@PK_MUser_ID", PK_MUser_ID.ToString());
                        }

                        string strfulljoin = " left join " + item.TabelReferenceName + " as " + stralias + " on " + moduleName + "." + item.FieldName + "=" + stralias + "." + item.TableReferenceFieldKey + " " + strjoin;

                        if (!arrtablename.Contains(strfulljoin))
                        {
                            strtable += strfulljoin;
                            arrtablename.Add(strfulljoin);
                        }

                        strfield += " '('+ CONVERT(VARCHAR(MAX), " + moduleName + "." + item.FieldName + ") +') '+ " + stralias + "." + item.TableReferenceFieldDisplayName + " AS " + item.FieldName + ",";
                    }
                }
                else if (item.FK_FieldType_ID == (int)EFieldType.VarBinary)
                {
                    if (item.FK_ExtType_ID == (int)EControlType.FileUpload)
                    {
                        strfield += moduleName + "." + item.FieldName + "Name AS " + item.FieldName + ",";
                    }
                    else
                    {
                        strfield += "'' AS " + item.FieldName + ",";
                    }
                }
                else
                {
                    strfield += moduleName + "." + item.FieldName + ",";
                }
            }

            foreach (ModuleFieldDefault item in moduleFieldDefaults.FindAll(item => item.PK_ModuleField_ID == 1))
            {
                if (module.IsSupportActivation)
                {
                    if (item.FK_FieldType_ID == 13)
                    {
                        strfield += moduleName + "." + item.FieldName + ",";
                    }
                }
            }

            if (!string.IsNullOrEmpty(strfield))
            {
                strfield = strfield.Remove(strfield.Length - 1, 1);
            }

            strtable = " select " + strfield + strtable;

            //Filter Data Access
            string searchQuery = "";
            string queryDataAccess = "SELECT * From DataAccess Where FK_Module_ID = @ModuleID AND FK_Role_ID = @RoleID";
            SqlParameter[] prms = new SqlParameter[]
            {
                new SqlParameter("@ModuleID", SqlDbType.Int){ Value = module.PK_Module_ID },
                new SqlParameter("@RoleID", SqlDbType.Int){ Value = FK_MRole_ID }
            };
            DataTable dtDataAccess = NawaDAO.ExecuteTable(queryDataAccess, prms);

            if (dtDataAccess != null && dtDataAccess.Rows.Count > 0)
            {
                string filterData = "";

                foreach (DataRow drDataAccess in dtDataAccess.Rows)
                {
                    filterData += " AND " + Convert.ToString(drDataAccess["FilterData"]).ToLower().Replace("@userid", UserID);
                }

                string filterDataFinal = filterData.Remove(0, 5);

                if (String.IsNullOrEmpty(searchQuery))
                {
                    searchQuery = " WHERE (" + filterDataFinal + ")";
                }
                else
                {
                    searchQuery = searchQuery + " AND (" + filterDataFinal + ")";
                }
            }

            strtable = strtable + searchQuery;

            //Filter User Branch & Date Process
            strtable = " SELECT * FROM ( " + strtable + ")tabletoquery";

            if (!string.IsNullOrEmpty(strprimarykey))
            {
                strtable += " order by " + strprimarykey;
            }

            return strtable;
        }


        private string GenerateSqlImportDataModuleDetail(Module parentModule, ModuleDetail module, List<ModuleDetailField> moduleFields, List<ModuleFieldDefault> moduleFieldDefaults, string UserID, int PK_MUser_ID)
        {
            string strprimarykey = "";
            string strfield = "";
            List<string> arrtablename = new List<string>();
            string strtable = " from " + module.ModuleDetailName + " ";

            foreach (ModuleDetailField item in moduleFields.OrderBy(item => item.Sequence).ToList())
            {
                if (item.IsPrimaryKey)
                {
                    strprimarykey = item.FieldName;
                }

                string stralias = string.IsNullOrEmpty(item.TabelReferenceNameAlias) ? item.TabelReferenceName : item.TabelReferenceNameAlias;

                if (item.FK_FieldType_ID == (int)EFieldType.ReferenceTable)
                {
                    if (item.FK_ExtType_ID == (int)EControlType.PopUpMultiCheckBox)
                    {
                        strfield += module.ModuleDetailName + "." + item.FieldName + ",";
                    }
                    else
                    {
                        string strjoin = "";
                        if (!string.IsNullOrEmpty(item.TableReferenceAdditonalJoin))
                        {
                            strjoin = item.TableReferenceAdditonalJoin.Replace("@userid", UserID);
                            strjoin = strjoin.Replace("@PK_MUser_ID", PK_MUser_ID.ToString());
                        }

                        string strfulljoin = " left join " + item.TabelReferenceName + " as " + stralias + " on " + module.ModuleDetailName + "." + item.FieldName + "=" + stralias + "." + item.TableReferenceFieldKey + " " + strjoin;
                        if (!arrtablename.Contains(strfulljoin))
                        {
                            strtable += strfulljoin;
                            arrtablename.Add(strfulljoin);
                        }

                        strfield += " '('+ CONVERT(VARCHAR, " + module.ModuleDetailName + "." + item.FieldName + ") +') '+ " + stralias + "." + item.TableReferenceFieldDisplayName + " AS " + item.FieldName + ",";
                    }
                }
                else if (item.FK_FieldType_ID == (int)EFieldType.VarBinary)
                {
                    if (item.FK_ExtType_ID == (int)EControlType.FileUpload)
                    {
                        strfield += module.ModuleDetailName + "." + item.FieldName + "Name AS " + item.FieldName + ",";
                    }
                    else
                    {
                        strfield += "'' AS " + item.FieldName + ",";
                    }
                }
                else
                {
                    strfield += module.ModuleDetailName + "." + item.FieldName + ",";
                }
            }

            foreach (ModuleFieldDefault item in moduleFieldDefaults.FindAll(item => item.PK_ModuleField_ID == 1))
            {
                if (parentModule.IsSupportActivation)
                {
                    if (item.FK_FieldType_ID == 13)
                    {
                        strfield += module.ModuleDetailName + "." + item.FieldName + ",";
                    }
                }
            }

            if (!string.IsNullOrEmpty(strfield))
            {
                strfield = strfield.Remove(strfield.Length - 1, 1);
            }

            strtable = " select " + strfield + strtable;

            strtable = " SELECT * FROM ( " + strtable + ")tabletoquery";

            if (!string.IsNullOrEmpty(strprimarykey))
            {
                strtable += " order by " + strprimarykey;
            }

            return strtable;
        }


        #region Upload Parameter
        public bool SaveParameterUpload(int moduleID, Stream fileStream, string UserID)
        {
            //baca file excel bentuk ke datatable
            DataTable dtImportData = new DataTable();

            XSSFWorkbook wb = new XSSFWorkbook(fileStream);

            ISheet ws = wb.GetSheetAt(0);
            IRow headerRow = ws.GetRow(0);

            if (headerRow == null)
            {
                throw new Exception("Invalid format upload, please check excel format");
            }

            foreach (ICell cell in headerRow.Cells)
            {
                dtImportData.Columns.Add(cell.StringCellValue);
            }

            NawaConnection conn = NawaDAO.CreateConnection();
            conn.BeginTransaction();

            int intStartY = headerRow.GetCell(0).StringCellValue == "Action" ? 1 : 0;

            try
            {
                foreach (ModuleField moduleField in moduleFields.Where(item => item.IsShowInForm || item.IsPrimaryKey).OrderBy(item => item.Sequence).ToList())
                {
                    if (headerRow.GetCell(intStartY).StringCellValue != moduleField.FieldLabel)
                    {
                        throw new Exception("Column " + (intStartY + 1) + ": " + headerRow.GetCell(intStartY).StringCellValue + " does not match with db : " + moduleField.FieldLabel);
                    }

                    intStartY++;
                }

                for (int rowNum = 1; rowNum <= ws.LastRowNum; rowNum++)
                {
                    IRow wsRow = ws.GetRow(rowNum);
                    DataRow row = dtImportData.NewRow();
                    bool bvalid = false;
                    foreach (ICell cell in wsRow.Cells)
                    {

                        if (cell.CellType != CellType.Blank)
                        {
                            bvalid = true;
                        }

                        switch (cell.CellType)
                        {
                            case CellType.Blank:
                                row[headerRow.GetCell(cell.ColumnIndex).StringCellValue] = DBNull.Value;
                                break;
                            case CellType.Boolean:
                                row[headerRow.GetCell(cell.ColumnIndex).StringCellValue] = cell.BooleanCellValue;
                                break;
                            case CellType.Numeric:
                                if (moduleFields.Any(item => item.FieldLabel == headerRow.GetCell(cell.ColumnIndex).StringCellValue && item.FK_FieldType_ID == 10))
                                {
                                    row[headerRow.GetCell(cell.ColumnIndex).StringCellValue] = DateTime.FromOADate(cell.NumericCellValue).ToString("dd MMM yyyy", new System.Globalization.CultureInfo("en-US"));
                                }
                                else
                                {
                                    row[headerRow.GetCell(cell.ColumnIndex).StringCellValue] = cell.NumericCellValue;
                                }
                                break;
                            case CellType.String:
                                row[headerRow.GetCell(cell.ColumnIndex).StringCellValue] = cell.StringCellValue;
                                break;
                            case CellType.Formula:
                                row[headerRow.GetCell(cell.ColumnIndex).StringCellValue] = cell.StringCellValue;
                                break;
                            default:
                                row[headerRow.GetCell(cell.ColumnIndex).StringCellValue] = cell.StringCellValue;
                                break;
                        }
                    }

                    if (bvalid)
                    {
                        dtImportData.Rows.Add(row);
                    }
                }

                wb.Close();

                string queryStr = @"SELECT ModuleName FROM Module WHERE PK_Module_ID = " + moduleID;
                //string ModuleName = Convert.ToString(NawaDAO.ExecuteScalar(queryStr, null, conn));

                bool splitUpload = Convert.ToBoolean(SystemParameterBLL.GetSystemParameterValue(53).SettingValue);

                string ModuleNameUpload = moduleName;

                if (splitUpload)
                {
                    ModuleNameUpload += "_Upload_data_" + CommonBLL.StripTableName(UserID);

                    NawaDAO.ExecuteNonQuery("usp_NDS_CreateTableUploadPerUser", new SqlParameter[] { new SqlParameter("@userid", UserID), new SqlParameter("@modulename", moduleName) });
                }
                else
                {
                    ModuleNameUpload += "_Upload";

                    queryStr = "DELETE FROM " + ModuleNameUpload + " WHERE nawa_userid ='" + UserID + "'";
                    NawaDAO.ExecuteNonQuery(queryStr, null, conn);
                }

                if (dtImportData.Rows.Count > 0)
                {
                    DataColumn autouserid = new DataColumn("nawa_userid", typeof(string));
                    autouserid.DefaultValue = UserID;
                    autouserid.AllowDBNull = false;
                    dtImportData.Columns.Add(autouserid);

                    DataTableReader objreader = new DataTableReader(dtImportData);
                    using (DataTable objnewdt = new DataTable())
                    {
                        DataColumn auto = new DataColumn("nawa_recordnumber", typeof(long));
                        auto.AllowDBNull = false;
                        auto.AutoIncrement = true;
                        auto.AutoIncrementSeed = 1;
                        auto.AutoIncrementStep = 1;

                        objnewdt.Columns.Add(auto);

                        objnewdt.Load(objreader);

                        using (SqlBulkCopy sqlBulk = new SqlBulkCopy((SqlConnection)conn.conn, SqlBulkCopyOptions.Default, (SqlTransaction)conn.trans))
                        {
                            sqlBulk.BatchSize = 1000;
                            sqlBulk.BulkCopyTimeout = 3600;
                            sqlBulk.DestinationTableName = ModuleNameUpload;


                            Dictionary<string, string> mapColumns = MakeMappingColumns(objnewdt, moduleFields.Where(item => item.FK_FieldType_ID != 14).ToList(), conn);
                            if (mapColumns != null)
                            {
                                foreach (KeyValuePair<string, string> mapping in mapColumns)
                                {
                                    sqlBulk.ColumnMappings.Add(mapping.Key, mapping.Value);
                                }
                            }

                            sqlBulk.WriteToServer(objnewdt);

                            DetailUpload(moduleID, wb, UserID, conn);

                            SqlParameter[] oparam = new SqlParameter[]
                            {
                                new SqlParameter("@ModuleID", SqlDbType.Int) {DbType = DbType.Int32, Value = moduleID},
                                new SqlParameter("@UserID", SqlDbType.VarChar) {DbType = DbType.AnsiString, Value = UserID},
                                new SqlParameter("@IntMode", SqlDbType.Int) {DbType = DbType.Int32, Value = 0}
                            };

                            NawaDAO.ExecuteNonQuery("EXEC usp_trimTableUpload @ModuleID, @UserID, @IntMode", oparam, conn);
                            NawaDAO.ExecuteNonQuery("EXEC usp_ValidateUploadFormModule @ModuleID, @UserID, @IntMode", oparam, conn);
                            ValidateStoreProcedureBeforeupload(EModuleTime.BeforeSave, EActionType.Import, oparam, moduleID, moduleFields, conn);
                        }
                    }
                }

                conn.Commit();
                conn.Dispose();
            }
            catch
            {
                conn.Rollback();
                conn.Dispose();

                throw;
            }
            return true;
        }

        public void DetailUpload(int moduleID, XSSFWorkbook file, string UserID, NawaConnection conn)
        {
            try
            {
                string query = @"SELECT * FROM ModuleDetail WHERE FK_Module_ID = @ModuleID AND Active = 1";
                SqlParameter[] prms = new SqlParameter[]
                {
                    new SqlParameter("ModuleID", SqlDbType.Int){Value = moduleID}
                };
                DataTable dt = NawaDAO.ExecuteTable(query, prms, conn);

                if (dt != null)
                {
                    List<ModuleDetail> listModuleDetail = NawaDAO.ConvertDataTable<ModuleDetail>(dt).ToList();
                    foreach (ModuleDetail md in listModuleDetail)
                    {
                        DataTable dtImportData = new DataTable();
                        string SheetName = md.ModuleDetailLabel.Length > 31 ? md.ModuleDetailLabel.Substring(0, 31) : md.ModuleDetailLabel;
                        ISheet dtSheet = file.GetSheet(SheetName);
                        IRow headerRow = dtSheet.GetRow(0);

                        foreach (ICell cell in headerRow.Cells)
                        {
                            dtImportData.Columns.Add(cell.StringCellValue);
                        }

                        query = @"SELECT * FROM ModuleDetailField WHERE FK_ModuleDetail_ID = @PK_ModuleDetail_ID ORDER BY Sequence";
                        SqlParameter[] prmsDt = new SqlParameter[]
                        {
                            new SqlParameter("@PK_ModuleDetail_ID", SqlDbType.Int) { Value = md.PK_ModuleDetail_ID }
                        };

                        DataTable dtListModuleDetailField = NawaDAO.ExecuteTable(query, prmsDt, conn);
                        List<ModuleDetailField> moduleDetailFields = NawaDAO.ConvertDataTable<ModuleDetailField>(dtListModuleDetailField);

                        for (int rowNum = 1; rowNum <= dtSheet.LastRowNum; rowNum++)
                        {
                            IRow dtRow = dtSheet.GetRow(rowNum);
                            DataRow row = dtImportData.NewRow();
                            bool bvalid = false;
                            foreach (ICell cell in dtRow.Cells)
                            {
                                if (cell.CellType != CellType.Blank)
                                {
                                    bvalid = true;
                                }

                                switch (cell.CellType)
                                {
                                    case CellType.Blank:
                                        row[headerRow.GetCell(cell.ColumnIndex).StringCellValue] = DBNull.Value;
                                        break;
                                    case CellType.Boolean:
                                        row[headerRow.GetCell(cell.ColumnIndex).StringCellValue] = cell.BooleanCellValue;
                                        break;
                                    case CellType.Numeric:
                                        if (moduleDetailFields.Any(item => item.FieldLabel == headerRow.GetCell(cell.ColumnIndex).StringCellValue && item.FK_FieldType_ID == 10))
                                        {
                                            row[headerRow.GetCell(cell.ColumnIndex).StringCellValue] = DateTime.FromOADate(cell.NumericCellValue).ToString("dd MMM yyyy", new System.Globalization.CultureInfo("en-US"));
                                        }
                                        else
                                        {
                                            row[headerRow.GetCell(cell.ColumnIndex).StringCellValue] = cell.NumericCellValue;
                                        }
                                        break;
                                    case CellType.String:
                                        row[headerRow.GetCell(cell.ColumnIndex).StringCellValue] = cell.StringCellValue;
                                        break;
                                    case CellType.Formula:
                                        row[headerRow.GetCell(cell.ColumnIndex).StringCellValue] = cell.StringCellValue;
                                        break;
                                    default:
                                        row[headerRow.GetCell(cell.ColumnIndex).StringCellValue] = cell.StringCellValue;
                                        break;
                                }
                            }

                            if (bvalid)
                            {
                                dtImportData.Rows.Add(row);
                            }
                        }

                        int intStartY = headerRow.GetCell(0).StringCellValue == "Action" ? 1 : 0;

                        foreach (ModuleDetailField moduleDetailField in moduleDetailFields.OrderBy(item => item.Sequence).ToList())
                        {
                            if ((headerRow.GetCell(intStartY).StringCellValue != moduleDetailField.FieldLabel) && moduleDetailField.FK_FieldType_ID != 8)
                            {
                                throw new Exception("Column " + (intStartY + 1) + ": " + headerRow.GetCell(intStartY).StringCellValue + " does not match with db : " + moduleDetailField.FieldLabel);
                            }

                            intStartY++;
                        }

                        file.Close();

                        query = @"SELECT ModuleDetailName FROM ModuleDetail WHERE PK_ModuleDetail_ID = @PK_ModuleDetail_ID";
                        string ModuleDetailName = Convert.ToString(NawaDAO.ExecuteScalar(query, prmsDt, conn));

                        query = "DELETE FROM " + md.ModuleDetailName.ToString() + "_upload WHERE nawa_userid ='" + UserID + "'";
                        NawaDAO.ExecuteNonQuery(query, null, conn);

                        if (dtImportData.Rows.Count > 0)
                        {
                            DataColumn autouserid = new DataColumn("nawa_userid", typeof(string));
                            autouserid.DefaultValue = UserID;
                            autouserid.AllowDBNull = false;
                            dtImportData.Columns.Add(autouserid);

                            DataTableReader objreader = new DataTableReader(dtImportData);
                            using (DataTable objnewdt = new DataTable())
                            {
                                DataColumn auto = new DataColumn("nawa_recordnumber", typeof(long));
                                auto.AllowDBNull = false;
                                auto.AutoIncrement = true;
                                auto.AutoIncrementSeed = 1;
                                auto.AutoIncrementStep = 1;

                                objnewdt.Columns.Add(auto);

                                objnewdt.Load(objreader);

                                using (SqlBulkCopy sqlBulk = new SqlBulkCopy((SqlConnection)conn.conn, SqlBulkCopyOptions.Default, (SqlTransaction)conn.trans))
                                {
                                    sqlBulk.BatchSize = 1000;
                                    sqlBulk.BulkCopyTimeout = 3600;
                                    sqlBulk.DestinationTableName = md.ModuleDetailName.ToString() + "_upload";


                                    Dictionary<string, string> mapColumns = MakeMappingColumnsDetail(objnewdt, moduleDetailFields.Where(item => item.FK_FieldType_ID != 14 && item.FK_FieldType_ID != 8).ToList(), conn);
                                    if (mapColumns != null)
                                    {
                                        foreach (KeyValuePair<string, string> mapping in mapColumns)
                                        {

                                            sqlBulk.ColumnMappings.Add(mapping.Key, mapping.Value);
                                        }
                                    }

                                    sqlBulk.WriteToServer(objnewdt);

                                    SqlParameter[] oparam = new SqlParameter[]
                                    {
                                        new SqlParameter("@moduleDetailID", SqlDbType.Int) {DbType = DbType.Int32, Value = md.PK_ModuleDetail_ID},
                                        new SqlParameter("@userID", SqlDbType.VarChar) {DbType = DbType.AnsiString, Value = UserID},
                                        new SqlParameter("@intMode", SqlDbType.Int) {DbType = DbType.Int32, Value = 0}
                                    };
                                    NawaDAO.ExecuteNonQuery("EXEC usp_ValidateUploadFormModuleDetail @moduleDetailID, @userID, @intMode", oparam, conn);
                                }
                            }
                        }
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        public void CommitParameterUpload(int moduleID, string userID, int roleID)
        {
            NawaConnection conn = NawaDAO.CreateConnection();
            conn.BeginTransaction();

            try
            {
                string query = @"SELECT ModuleName, IsUseApproval FROM Module WHERE PK_Module_ID = @moduleID";
                SqlParameter[] parameters = new SqlParameter[]
                {
                    new SqlParameter("@moduleID", SqlDbType.Int) {Value = moduleID}
                };
                DataRow module = NawaDAO.ExecuteRow(query, parameters, conn);

                if (module != null)
                {
                    //string moduleName = Convert.ToString(module["ModuleName"]);

                    bool splitUpload = Convert.ToBoolean(SystemParameterBLL.GetSystemParameterValue(53).SettingValue);

                    string moduleNameUpload = moduleName;

                    if (splitUpload)
                    {
                        moduleNameUpload += "_Upload_data_" + CommonBLL.StripTableName(userID);
                    }
                    else
                    {
                        moduleNameUpload += "_Upload";
                    }

                    if (Convert.ToBoolean(module["IsUseApproval"]) && roleID != 1)
                    {
                        #region Audit Trail
                        DataTable auditTrailUpdateOld = new DataTable();
                        List<Dictionary<string, object>> auditTrailUpdateNew = new List<Dictionary<string, object>>();
                        List<Dictionary<string, object>> auditTrailInsert = new List<Dictionary<string, object>>();
                        List<Dictionary<string, object>> auditTrailDelete = new List<Dictionary<string, object>>();
                        query = "SELECT * FROM " + moduleNameUpload + " WHERE nawa_userid = @userID AND ISNULL(KeteranganError, '') = ''";
                        parameters = new SqlParameter[]
                        {
                        new SqlParameter("@userID", SqlDbType.VarChar) { Value = userID }
                        };
                        DataTable uploadData = NawaDAO.ExecuteTable(query, parameters, conn);

                        if (uploadData != null && uploadData.Rows.Count > 0)
                        {
                            if (moduleFields != null && moduleFields.Count > 0)
                            {
                                List<ModuleField> listModuleField = moduleFields;
                                string primaryFieldName = listModuleField.Where(x => x.IsPrimaryKey).Select(x => x.FieldName).FirstOrDefault();

                                foreach (DataRow rowUpload in uploadData.Rows)
                                {
                                    switch (Convert.ToString(rowUpload["nawa_Action"]))
                                    {
                                        case "Update":
                                            #region Get old data
                                            query = "SELECT * FROM " + moduleName + " WHERE " + primaryFieldName + " = @ID";
                                            parameters = new SqlParameter[]
                                            {
                                            new SqlParameter("@ID", SqlDbType.Int) { Value = Convert.ToInt32(rowUpload[primaryFieldName]) }
                                            };
                                            DataRow rowOldData = NawaDAO.ExecuteRow(query, parameters, conn);

                                            if (rowOldData != null)
                                            {
                                                if (auditTrailUpdateOld.Rows.Count > 0)
                                                {
                                                    auditTrailUpdateOld.Rows.Add(rowOldData.ItemArray);
                                                }
                                                else
                                                {
                                                    auditTrailUpdateOld = rowOldData.Table;
                                                }
                                            }
                                            else
                                            {
                                                throw new Exception(moduleName + " data with ID = " + Convert.ToString(rowUpload[primaryFieldName]) + " was not found.");
                                            }
                                            #endregion

                                            Dictionary<string, object> updateNewDictionary = new Dictionary<string, object>();

                                            foreach (ModuleField field in listModuleField)
                                            {
                                                updateNewDictionary.Add(field.FieldName, rowUpload[field.FieldName]);
                                            }

                                            auditTrailUpdateNew.Add(updateNewDictionary);

                                            break;

                                        case "Delete":
                                            Dictionary<string, object> deleteDictionary = new Dictionary<string, object>();

                                            foreach (ModuleField field in listModuleField)
                                            {
                                                deleteDictionary.Add(field.FieldName, rowUpload[field.FieldName]);
                                            }

                                            auditTrailDelete.Add(deleteDictionary);

                                            break;

                                        case "Insert":
                                            Dictionary<string, object> insertDictionary = new Dictionary<string, object>();

                                            foreach (ModuleField field in listModuleField)
                                            {
                                                insertDictionary.Add(field.FieldName, rowUpload[field.FieldName]);
                                            }

                                            auditTrailInsert.Add(insertDictionary);

                                            break;

                                        default:
                                            break;
                                    }
                                }
                            }
                        }
                        #endregion

                        query = "EXEC usp_SaveUploadApproval @Modeid, @userid, @ModuleID, @roleid";
                        parameters = new SqlParameter[]
                        {
                            new SqlParameter("@Modeid", SqlDbType.Int) {Value = Convert.ToInt32(EUploadMode.Update)},
                            new SqlParameter("@userid", SqlDbType.VarChar) {Value = userID},
                            new SqlParameter("@ModuleID", SqlDbType.Int) {Value = moduleID},
                            new SqlParameter("@roleid", SqlDbType.Int) {Value = roleID}
                        };

                        NawaDAO.ExecuteNonQuery(query, parameters, conn);

                        //string query = 

                        //NOT IMPLEMENTED YET
                        //query = "SELECT * FROM ModuleDetail WHERE FK_Module_ID = " + moduleID + " AND Active = 1";
                        //DataTable DtListModuleDetail = NawaDAO.ExecuteTable(query);
                        //List<ModuleDetail> listModuleDetail = NawaDAO.ConvertDataTable<ModuleDetail>(DtListModuleDetail);

                        //foreach (ModuleDetail item in listModuleDetail)
                        //{
                        //    query = "EXEC usp_SaveUploadDataModuleDetail @IntMode, @ModuleDetailID, @UserID";
                        //    parameters = new SqlParameter[]
                        //    {
                        //    new SqlParameter("@IntMode", SqlDbType.Int) {Value = Convert.ToInt32(EUploadMode.Update)},
                        //    new SqlParameter("@ModuleDetailID", SqlDbType.Int) {Value = item.PK_ModuleDetail_ID},
                        //    new SqlParameter("@UserID", SqlDbType.VarChar) {Value = userID}
                        //    };
                        //    NawaDAO.ExecuteNonQuery(query, parameters, conn);
                        //}

                        ValidateStoreProcedureAfterupload(EModuleTime.AfterSave, EActionType.Import, moduleID, userID, conn);

                        #region Audit Trail
                        if (auditTrailUpdateNew.Count > 0)
                        {
                            DataTable dtAuditTrailUpdateNew = NawaDAO.ConvertListDictionary(auditTrailUpdateNew);
                            long headerID = AuditTrailBLL.InputAuditTrailHeader(conn, userID, moduleName, EActionType.Update, EAuditTrailStatus.WaitingApproval, 0, "");

                            List<Dictionary<string, object>> auditTrailInsertOld = new List<Dictionary<string, object>>();
                            List<Dictionary<string, object>> auditTrailDeleteNew = new List<Dictionary<string, object>>();
                            AuditTrailBLL.InputAuditTrailDetail(conn, headerID, EActionType.Update, dtAuditTrailUpdateNew, auditTrailUpdateOld);
                            AuditTrailBLL.InputAuditTrailDetail(conn, headerID, EActionType.Insert, auditTrailInsert, auditTrailInsertOld);
                            AuditTrailBLL.InputAuditTrailDetail(conn, headerID, EActionType.Delete, auditTrailDeleteNew, auditTrailDelete);
                        }
                        #endregion

                        query = "DELETE FROM " + moduleNameUpload + " WHERE nawa_userid = '" + userID + "' AND ISNULL(KeteranganError, '') = ''";

                        NawaDAO.ExecuteNonQuery(query, null, conn);

                        //foreach (ModuleDetail item in listModuleDetail)
                        //{
                        //    query = "DELETE FROM " + item.ModuleDetailName + "_upload WHERE nawa_userid = '" + userID + "' AND ISNULL(KeteranganError, '') = ''";
                        //    NawaDAO.ExecuteNonQuery(query, null, conn);
                        //}
                    }
                    else
                    {
                        #region Audit Trail
                        DataTable auditTrailUpdateOld = new DataTable();
                        List<Dictionary<string, object>> auditTrailUpdateNew = new List<Dictionary<string, object>>();
                        List<Dictionary<string, object>> auditTrailInsert = new List<Dictionary<string, object>>();
                        List<Dictionary<string, object>> auditTrailDelete = new List<Dictionary<string, object>>();
                        query = "SELECT * FROM " + moduleNameUpload + " WHERE nawa_userid = @userID AND ISNULL(KeteranganError, '') = ''";
                        parameters = new SqlParameter[]
                        {
                        new SqlParameter("@userID", SqlDbType.VarChar) { Value = userID }
                        };
                        DataTable uploadData = NawaDAO.ExecuteTable(query, parameters, conn);

                        if (uploadData != null && uploadData.Rows.Count > 0)
                        {
                            if (moduleFields != null && moduleFields.Count > 0)
                            {
                                List<ModuleField> listModuleField = moduleFields;
                                string primaryFieldName = listModuleField.Where(x => x.IsPrimaryKey).Select(x => x.FieldName).FirstOrDefault();

                                foreach (DataRow rowUpload in uploadData.Rows)
                                {
                                    switch (Convert.ToString(rowUpload["nawa_Action"]))
                                    {
                                        case "Update":
                                            #region Get old data
                                            query = "SELECT * FROM " + moduleName + " WHERE " + primaryFieldName + " = @ID";
                                            parameters = new SqlParameter[]
                                            {
                                            new SqlParameter("@ID", SqlDbType.Int) { Value = Convert.ToInt32(rowUpload[primaryFieldName]) }
                                            };
                                            DataRow rowOldData = NawaDAO.ExecuteRow(query, parameters, conn);

                                            if (rowOldData != null)
                                            {
                                                if (auditTrailUpdateOld.Rows.Count > 0)
                                                {
                                                    auditTrailUpdateOld.Rows.Add(rowOldData.ItemArray);
                                                }
                                                else
                                                {
                                                    auditTrailUpdateOld = rowOldData.Table;
                                                }
                                            }
                                            else
                                            {
                                                throw new Exception(moduleName + " data with ID = " + Convert.ToString(rowUpload[primaryFieldName]) + " was not found.");
                                            }
                                            #endregion

                                            Dictionary<string, object> updateNewDictionary = new Dictionary<string, object>();

                                            foreach (ModuleField field in listModuleField)
                                            {
                                                updateNewDictionary.Add(field.FieldName, rowUpload[field.FieldName]);
                                            }

                                            auditTrailUpdateNew.Add(updateNewDictionary);

                                            break;

                                        case "Delete":
                                            Dictionary<string, object> deleteDictionary = new Dictionary<string, object>();

                                            foreach (ModuleField field in listModuleField)
                                            {
                                                deleteDictionary.Add(field.FieldName, rowUpload[field.FieldName]);
                                            }

                                            auditTrailDelete.Add(deleteDictionary);

                                            break;

                                        case "Insert":
                                            Dictionary<string, object> insertDictionary = new Dictionary<string, object>();

                                            foreach (ModuleField field in listModuleField)
                                            {
                                                insertDictionary.Add(field.FieldName, rowUpload[field.FieldName]);
                                            }

                                            auditTrailInsert.Add(insertDictionary);

                                            break;

                                        default:
                                            break;
                                    }
                                }
                            }
                        }
                        #endregion

                        query = "EXEC usp_SaveUploadData @intmode, @moduleid, @userid";
                        parameters = new SqlParameter[]
                        {
                            new SqlParameter("@intmode", SqlDbType.Int) {Value = Convert.ToInt32(EUploadMode.Update)},
                            new SqlParameter("@moduleid", SqlDbType.Int) {Value = moduleID},
                            new SqlParameter("@userid", SqlDbType.VarChar) {Value = userID}
                        };

                        NawaDAO.ExecuteNonQuery(query, parameters, conn);

                        query = "SELECT * FROM ModuleDetail WHERE FK_Module_ID = " + moduleID + " AND Active = 1";
                        DataTable DtListModuleDetail = NawaDAO.ExecuteTable(query);
                        List<ModuleDetail> listModuleDetail = NawaDAO.ConvertDataTable<ModuleDetail>(DtListModuleDetail);

                        foreach (ModuleDetail item in listModuleDetail)
                        {
                            query = "EXEC usp_SaveUploadDataModuleDetail @IntMode, @ModuleDetailID, @UserID";
                            parameters = new SqlParameter[]
                            {
                            new SqlParameter("@IntMode", SqlDbType.Int) {Value = Convert.ToInt32(EUploadMode.Update)},
                            new SqlParameter("@ModuleDetailID", SqlDbType.Int) {Value = item.PK_ModuleDetail_ID},
                            new SqlParameter("@UserID", SqlDbType.VarChar) {Value = userID}
                            };
                            NawaDAO.ExecuteNonQuery(query, parameters, conn);
                        }

                        ValidateStoreProcedureAfterupload(EModuleTime.AfterSave, EActionType.Import, moduleID, userID, conn);

                        #region Audit Trail
                        if (auditTrailUpdateNew.Count > 0)
                        {
                            DataTable dtAuditTrailUpdateNew = NawaDAO.ConvertListDictionary(auditTrailUpdateNew);
                            long headerID = AuditTrailBLL.InputAuditTrailHeader(conn, userID, moduleName, EActionType.Update, EAuditTrailStatus.AffectedDatabase, 0, userID);

                            List<Dictionary<string, object>> auditTrailInsertOld = new List<Dictionary<string, object>>();
                            List<Dictionary<string, object>> auditTrailDeleteNew = new List<Dictionary<string, object>>();
                            AuditTrailBLL.InputAuditTrailDetail(conn, headerID, EActionType.Update, dtAuditTrailUpdateNew, auditTrailUpdateOld);
                            AuditTrailBLL.InputAuditTrailDetail(conn, headerID, EActionType.Insert, auditTrailInsert, auditTrailInsertOld);
                            AuditTrailBLL.InputAuditTrailDetail(conn, headerID, EActionType.Delete, auditTrailDeleteNew, auditTrailDelete);
                        }
                        #endregion

                        query = "DELETE FROM " + moduleNameUpload + " WHERE nawa_userid = '" + userID + "' AND ISNULL(KeteranganError, '') = ''";

                        NawaDAO.ExecuteNonQuery(query, null, conn);

                        foreach (ModuleDetail item in listModuleDetail)
                        {
                            query = "DELETE FROM " + item.ModuleDetailName + "_upload WHERE nawa_userid = '" + userID + "' AND ISNULL(KeteranganError, '') = ''";
                            NawaDAO.ExecuteNonQuery(query, null, conn);
                        }
                    }

                    conn.Commit();
                    conn.Dispose();
                }
                else
                {
                    throw new Exception("Module data was not found");
                }
            }
            catch
            {
                conn.Rollback();
                conn.Dispose();

                throw;
            }
        }

        private void ValidateStoreProcedureBeforeupload(EModuleTime intmoduleTime, EActionType intmoduleAction, SqlParameter[] Listsqlparam, int moduleID, List<ModuleField> moduleFields, NawaConnection conn)
        {
            List<ModuleValidation> objshemaModuleValidation = new List<ModuleValidation>();

            Module objSchemaModule;

            string queryStr = @"SELECT * FROM Module WHERE PK_Module_ID = " + moduleID;
            DataRow drModule = NawaDAO.ExecuteRow(queryStr, null, conn);
            objSchemaModule = NawaDAO.ConvertDataTable<Module>(drModule);
            if (objSchemaModule != null)
            {
                if (objSchemaModule.IsUseDesigner)
                {
                    queryStr = @"SELECT * FROM ModuleValidation WHERE FK_Module_ID = " + moduleID;
                    DataTable drModuleValidation = NawaDAO.ExecuteTable(queryStr, null, conn);
                    objshemaModuleValidation = NawaDAO.ConvertDataTable<ModuleValidation>(drModuleValidation);
                }

                if (objSchemaModule.IsUseStoreProcedureValidation)
                {
                    foreach (ModuleValidation moduleValidation in objshemaModuleValidation)
                    {
                        if (moduleValidation.FK_ModuleTime_ID == Convert.ToInt32(intmoduleTime) && moduleValidation.FK_ModuleAction_ID == Convert.ToInt32(intmoduleAction))
                        {
                            NawaDAO.ExecuteNonQuery(moduleValidation.StoreProcedureName, Listsqlparam, conn, CommandType.StoredProcedure);
                        }
                    }
                }
            }
        }

        private Dictionary<string, string> MakeMappingColumns(DataTable objdt, List<ModuleField> moduleFields, NawaConnection conn)
        {
            Dictionary<string, string> mappingColumns = new Dictionary<string, string>();

            foreach (DataColumn item in objdt.Columns)
            {
                if (item.ColumnName == "nawa_userid" || item.ColumnName == "nawa_recordnumber")
                    mappingColumns.Add(item.ColumnName, item.ColumnName);
                else if (item.ColumnName == "Action")
                    mappingColumns.Add(item.ColumnName, "nawa_Action");
                else
                {
                    ModuleField objresult = moduleFields.Where(x => x.FieldLabel == item.ColumnName).FirstOrDefault();
                    if (objresult != null)
                    {
                        mappingColumns.Add(item.ColumnName, objresult.FieldName);
                    }

                    string query = "SELECT * FROM ModuleFieldDefault";
                    DataTable dtModuleFieldDefaults = NawaDAO.ExecuteTable(query, null, conn);
                    List<ModuleFieldDefault> moduleFieldDefaults = NawaDAO.ConvertDataTable<ModuleFieldDefault>(dtModuleFieldDefaults);

                    ModuleFieldDefault objresultdefault = moduleFieldDefaults.Where(x => x.FieldLabel == item.ColumnName).FirstOrDefault();
                    if (objresultdefault != null)
                    {
                        mappingColumns.Add(item.ColumnName, objresultdefault.FieldName);
                    }
                }
            }

            return mappingColumns;
        }

        private Dictionary<string, string> MakeMappingColumnsDetail(DataTable objdt, List<ModuleDetailField> moduleDetailFields, NawaConnection conn)
        {
            Dictionary<string, string> mappingColumns = new Dictionary<string, string>();

            foreach (DataColumn item in objdt.Columns)
            {
                if (item.ColumnName == "nawa_userid" || item.ColumnName == "nawa_recordnumber")
                    mappingColumns.Add(item.ColumnName, item.ColumnName);
                else if (item.ColumnName == "Action")
                    mappingColumns.Add(item.ColumnName, "nawa_Action");
                else
                {
                    ModuleDetailField objresult = moduleDetailFields.Where(x => x.FieldLabel == item.ColumnName).FirstOrDefault();
                    if (objresult != null)
                    {
                        mappingColumns.Add(item.ColumnName, objresult.FieldName);
                    }

                    string query = "SELECT * FROM ModuleFieldDefault";
                    DataTable dtModuleFieldDefaults = NawaDAO.ExecuteTable(query, null, conn);
                    List<ModuleFieldDefault> moduleFieldDefaults = NawaDAO.ConvertDataTable<ModuleFieldDefault>(dtModuleFieldDefaults);

                    ModuleFieldDefault objresultdefault = moduleFieldDefaults.Where(x => x.FieldLabel == item.ColumnName).FirstOrDefault();
                    if (objresultdefault != null)
                    {
                        mappingColumns.Add(item.ColumnName, objresultdefault.FieldName);
                    }
                }
            }

            return mappingColumns;
        }
        #endregion

        public void CustomSupportSP(CustomSupportSPRequest Input)
        {
            NawaConnection connection = NawaDAO.CreateConnection();


            connection.BeginTransaction();

            try
            {
                string[] parameters = Input.Parameter.Replace(" ", "").Split(',');
                string[] parameterValues = Input.ParameterValue.Replace(" ", "").Split(',');

                if (parameters.Length > 0 && parameters.Length == parameterValues.Length)
                {

                    if (!String.IsNullOrEmpty(Input.Parameter))
                    {
                        List<ModuleField> listModuleField = moduleFields;
                        List<SqlParameter> listSqlParameter = new List<SqlParameter>();

                        for (int i = 0; i < parameters.Length; i++)
                        {

                            ModuleField moduleField = listModuleField.Find(x => x.Sequence == Convert.ToInt32(parameterValues[i]));

                            SqlParameter parameter = new SqlParameter(parameters[i], CommonBLL.GetObject(moduleField.FK_FieldType_ID, Input.Data[moduleField.FieldName]));

                            listSqlParameter.Add(parameter);

                        }



                        NawaDAO.ExecuteNonQuery(Input.Target, listSqlParameter.ToArray(), connection, CommandType.StoredProcedure);

                        connection.Commit();
                        connection.Dispose();
                    }
                }
                else
                {
                    throw new Exception("Sql Parameter list and sql Value list not match");
                }
            }
            catch
            {
                connection.Rollback();
                connection.Dispose();

                throw;
            }
        }

        private SqlParameter GetSqlParameter(string FieldName, int FieldType, object value, string defaultValue = null)
        {
            return GetSqlParameterRaw(FieldName, FieldType, NawaDataBLL.CommonBLL.GetObject(FieldType, value, defaultValue));
        }

        private SqlParameter GetSqlParameterRaw(string FieldName, int FieldType, object value)
        {
            SqlDbType dbType;
            object objValue = value;

            switch (FieldType)
            {
                case 1:
                    dbType = SqlDbType.BigInt;
                    break;
                case 2:
                    dbType = SqlDbType.Int;
                    break;
                case 3:
                    dbType = SqlDbType.SmallInt;
                    break;
                case 4:
                    dbType = SqlDbType.TinyInt;
                    break;
                case 5:
                    dbType = SqlDbType.Decimal;
                    break;
                case 6:
                    dbType = SqlDbType.Float;
                    break;
                case 7:
                    dbType = SqlDbType.Real;
                    break;
                case 8:
                    dbType = SqlDbType.Money;
                    break;
                case 9:
                    dbType = SqlDbType.VarChar;
                    break;
                case 10:
                    dbType = SqlDbType.DateTime;
                    break;
                case 11:
                    dbType = SqlDbType.VarChar; //WARNING
                    break;
                case 12:
                    dbType = SqlDbType.Int;
                    break;
                case 13:
                    dbType = SqlDbType.Bit;
                    break;
                case 14:
                    dbType = SqlDbType.VarBinary;
                    break;
                case 15:
                    dbType = SqlDbType.BigInt;
                    break;
                case 16:
                    dbType = SqlDbType.Int;
                    break;
                default:
                    dbType = SqlDbType.VarChar;
                    break;
            }

            return new SqlParameter("@" + FieldName, dbType) { Value = objValue };
        }

        private string GetQueryRef(string strTable, string strfieldkey, string strfielddisplay, string stradditionaljoin, string strfilter, bool bcascade, string UserID, int PK_MUser_ID)
        {
            if (String.IsNullOrEmpty(stradditionaljoin)) stradditionaljoin = "";
            if (bcascade) stradditionaljoin = "";

            string query = "select " + strfieldkey + "," + strfielddisplay + " from " + strTable;
            if (String.IsNullOrEmpty(strfilter))
            {
                if (!String.IsNullOrEmpty(stradditionaljoin) && stradditionaljoin.Substring(0, stradditionaljoin.IndexOf(" ")).ToLower() == "and")
                {
                    strfilter = strfilter + stradditionaljoin.Substring(4, stradditionaljoin.Length - 4);
                }
            }
            else
            {
                strfilter = strfilter + stradditionaljoin;
            }

            if (!String.IsNullOrEmpty(strfilter))
            {
                strfilter = strfilter.Replace("@userid", UserID);
                strfilter = strfilter.Replace("@PK_MUser_ID", PK_MUser_ID.ToString());
                query = query + " where " + strfilter;
            }

            return query;
        }


        private string GetExcelColumnName(int columnNumber)
        {
            int dividend = columnNumber;
            string columnName = String.Empty;
            int modulo;

            while (dividend > 0)
            {
                modulo = (dividend - 1) % 26;
                columnName = Convert.ToChar(65 + modulo).ToString() + columnName;
                dividend = (int)((dividend - modulo) / 26);
            }

            return columnName;
        }

        public List<NawaDataDAL.DynamicDictionary> GetImportApprovalByID(long pkModuleApprovalID, int page, string orderBy, string order, string search, string filter, out int rowCount)
        {
            try
            {
                string query = @"SELECT *
                                 FROM ModuleApproval
                                 WHERE PK_ModuleApproval_ID = @ModuleApprovalID";
                SqlParameter[] prms = new SqlParameter[]
                {
                    new SqlParameter("@ModuleApprovalID", SqlDbType.Int){Value = pkModuleApprovalID}
                };
                DataRow drModuleApproval = NawaDAO.ExecuteRow(query, prms);
                List<DynamicDictionary> moduleData = new List<DynamicDictionary>();
                rowCount = 0;

                if (drModuleApproval != null)
                {
                    ModuleApproval moduleApproval = NawaDAO.ConvertDataTable<ModuleApproval>(drModuleApproval);
                    bool splitUpload = Convert.ToBoolean(SystemParameterBLL.GetSystemParameterValue(53).SettingValue);
                    string moduleNameUpload = moduleName;

                    if (splitUpload)
                    {
                        moduleNameUpload += "_Upload_data_" + CommonBLL.StripTableName(moduleApproval.CreatedBy) + "_approval";
                    }
                    else
                    {
                        moduleNameUpload += "_Upload_approval";
                    }

                    //query = "SELECT * FROM ModuleField WHERE FK_Module_ID = @ModuleID AND (IsShowInView = 1 OR IsPrimaryKey = 1)";
                    //prms = new SqlParameter[]
                    //{
                    //    new SqlParameter("@ModuleID", SqlDbType.Int){ Value = moduleApproval.FK_Module_ID }
                    //};
                    //DataTable dtFields = NawaDAO.ExecuteTable(query, prms);
                    string moduleColumns = "";
                    string searchQuery = "";

                    if (moduleFields != null && moduleFields.Count(item => item.IsShowInView || item.IsPrimaryKey) > 0)
                    {
                        List<ModuleField> listModuleFields = moduleFields.Where(item => item.IsShowInView || item.IsPrimaryKey).ToList();

                        if (!string.IsNullOrEmpty(search))
                        {
                            foreach (ModuleField moduleField in moduleFields.Where(item => item.IsShowInView || item.IsPrimaryKey).ToList())
                            {
                                if (string.IsNullOrEmpty(searchQuery))
                                {
                                    searchQuery = "WHERE ( FK_ModuleApproval_ID = " + pkModuleApprovalID + "  AND CAST(" + moduleNameUpload + "." + Convert.ToString(moduleField.FieldName) + " AS VARCHAR(MAX)) LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' )";
                                }
                                else
                                {
                                    searchQuery = searchQuery + " OR ( FK_ModuleApproval_ID = " + pkModuleApprovalID + "  AND CAST(" + moduleNameUpload + "." + Convert.ToString(moduleField.FieldName) + " AS VARCHAR(MAX)) LIKE '%" + search.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]") + "%' )";
                                }
                            }
                        }
                        else if (!string.IsNullOrEmpty(filter))
                        {
                            searchQuery = " WHERE FK_ModuleApproval_ID = " + pkModuleApprovalID + " AND " + filter;
                        }
                        else
                        {
                            searchQuery = " WHERE FK_ModuleApproval_ID = " + pkModuleApprovalID;
                        }

                        foreach (ModuleField moduleField in moduleFields.Where(item => item.IsShowInView || item.IsPrimaryKey).ToList())
                        {
                            if (!string.IsNullOrEmpty(moduleColumns))
                            {
                                moduleColumns += ", ";
                            }

                            moduleColumns += moduleNameUpload + "." + moduleField.FieldName;
                        }

                        if (!string.IsNullOrEmpty(searchQuery))
                        {
                            searchQuery = searchQuery + " ";
                        }

                        #region OrderBy
                        string orderByFinal = "";
                        string[] arrFields = { "PK_upload_ID", "nawa_userid", "nawa_recordnumber", "KeteranganError", "nawa_Action", "Active" };

                        if (!string.IsNullOrEmpty(orderBy))
                        {
                            ModuleField fieldOrderBy = listModuleFields.Where(x => x.FieldName.ToLower() == orderBy.ToLower()).FirstOrDefault();
                            string stringOrderBy = arrFields.Where(x => x.ToLower() == orderBy.ToLower()).FirstOrDefault();

                            if (fieldOrderBy != null && fieldOrderBy.PK_ModuleField_ID > 0)
                            {
                                orderByFinal = fieldOrderBy.FieldName;

                                if (order != null && order.ToLower() == "desc")
                                {
                                    orderByFinal += " desc";
                                }
                                else
                                {
                                    orderByFinal += " asc";
                                }
                            }
                            else if (!string.IsNullOrEmpty(stringOrderBy))
                            {
                                orderByFinal = stringOrderBy;

                                if (order != null && order.ToLower() == "desc")
                                {
                                    orderByFinal += " desc";
                                }
                                else
                                {
                                    orderByFinal += " asc";
                                }
                            }
                        }
                        #endregion

                        moduleColumns += ", " + moduleNameUpload + ".nawa_Action ";
                        query = "SELECT COUNT(13) AS [RowCount] FROM " + moduleNameUpload + ' ' + searchQuery;
                        rowCount = Convert.ToInt32(NawaDAO.ExecuteScalar(query));
                        int pageSize = Convert.ToInt32(SystemParameterBLL.GetAppSystemParameter(7).SettingValue);
                        query = "EXEC usp_PagingNew @Query, @OrderBy, @PageIndex, @PageSize";
                        prms = new SqlParameter[]
                        {
                            new SqlParameter("@Query", SqlDbType.VarChar){ Value = "SELECT " + moduleColumns + " FROM " + moduleNameUpload + ' ' + searchQuery },
                            new SqlParameter("@OrderBy", SqlDbType.VarChar){ Value = orderByFinal },
                            new SqlParameter("@PageIndex", SqlDbType.Int){ Value = page },
                            new SqlParameter("@PageSize", SqlDbType.Int){ Value = pageSize }
                        };
                        DataTable dt = NawaDAO.ExecuteTable(query, prms);
                        moduleData = dt.AsEnumerable().Select(row => new DynamicDictionary(row)).ToList();
                    }
                }

                return moduleData;
            }
            catch
            {
                throw;
            }
        }

        public ModuleDetailView GetModuleDetailDetail(int ModuleDetailID, string ID, long PK_ModuleApproval_ID, string UserID)
        {
            try
            {
                string query = "SELECT * FROM ModuleDetail WHERE PK_ModuleDetail_ID = @ModuleDetailID";
                SqlParameter[] prms = new SqlParameter[]
                {
                    new SqlParameter("@ModuleDetailID", SqlDbType.Int){ Value = ModuleDetailID }
                };

                DataRow dr = NawaDAO.ExecuteRow(query, prms);

                if (dr != null)
                {
                    ModuleDetailView moduleDetail = NawaDAO.ConvertDataTable<ModuleDetailView>(dr);

                    //ModuleView moduleDetail = (ModuleView)module;

                    query = "SELECT * FROM ModuleDetailField WHERE FK_ModuleDetail_ID = @ModuleDetailID";
                    prms = new SqlParameter[]
                    {
                        new SqlParameter("@ModuleDetailID", SqlDbType.Int){ Value = ModuleDetailID }
                    };

                    DataTable dt = NawaDAO.ExecuteTable(query, prms);
                    if (dt != null)
                    {
                        List<ModuleDetailField> moduleDetailFields = NawaDAO.ConvertDataTable<ModuleDetailField>(dt);
                        if (moduleDetailFields != null)
                        {
                            moduleDetail.ModuleDetailFields = moduleDetailFields;
                        }
                    }

                    prms = new SqlParameter[]
                    {
                        new SqlParameter("@pkmoduleid", SqlDbType.Int){ Value = ModuleDetailID }
                    };
                    int pkworkflowid = Convert.ToInt32(NawaDAO.ExecuteScalar("IsHaveWorkflow", prms, null, CommandType.StoredProcedure));
                    moduleDetail.UseWorkflow = pkworkflowid > 0;

                    if (String.IsNullOrEmpty(ID))
                    {
                        if (PK_ModuleApproval_ID > 0)
                        {
                            query = "SELECT ModuleKey FROM ModuleApproval WHERE PK_ModuleApproval_ID = " + PK_ModuleApproval_ID;
                            ID = Convert.ToString(NawaDAO.ExecuteScalar(query));
                        }
                    }

                    prms = new SqlParameter[] {
                        new SqlParameter("@pkmoduleid", SqlDbType.Int){ Value = ModuleDetailID },
                        new SqlParameter("@pkunikid", SqlDbType.VarChar){ Value = ID },
                        new SqlParameter("@userId", SqlDbType.VarChar){ Value = UserID },
                        new SqlParameter("@action", SqlDbType.Int){ Value = EActionType.View }
                    };
                    pkworkflowid = Convert.ToInt32(NawaDAO.ExecuteScalar("usp_ValidateMWorkflow", prms, null, CommandType.StoredProcedure));
                    moduleDetail.UserHasWorkflow = pkworkflowid > 0;

                    prms = new SqlParameter[] {
                        new SqlParameter("@PkMOduleid", SqlDbType.Int) { Value = ModuleDetailID },
                        new SqlParameter("@pkunikid", SqlDbType.VarChar) { Value = ID }
                    };
                    pkworkflowid = Convert.ToInt32(NawaDAO.ExecuteScalar("IsHaveWorkflowApproval", prms, null, CommandType.StoredProcedure));
                    moduleDetail.HaveActiveWorkflow = pkworkflowid > 0;

                    moduleDetail.ActionList = new List<ActionView>();

                    List<MFieldType> ListFieldType = GetFieldType();
                    foreach (ModuleDetailField moduleField in moduleDetail.ModuleDetailFields)
                    {
                        #region ListRegex
                        query = "SELECT * FROM ModuleFieldRegex WHERE FK_ModuleField_ID = " + moduleField.PK_ModuleDetailField_ID;
                        DataTable dtR = NawaDAO.ExecuteTable(query);
                        List<ModuleFieldRegexView> moduleFieldRegexs = NawaDAO.ConvertDataTable<ModuleFieldRegexView>(dtR);
                        //moduleField.ListRegex = moduleFieldRegexs;
                        #endregion
                    }

                    #region MGroupMenuAccess
                    query = "SELECT FK_MGroupMenu_ID FROM MUser WHERE UserID = '" + UserID + "'";
                    int MGroupMenuID = Convert.ToInt32(NawaDAO.ExecuteScalar(query));

                    query = "SELECT * FROM MGroupMenuAccess WHERE FK_GroupMenu_ID = @FK_GroupMenu_ID AND FK_Module_ID = @FK_Module_ID";
                    prms = new SqlParameter[] {
                        new SqlParameter("@FK_GroupMenu_ID", SqlDbType.Int){ Value = MGroupMenuID },
                        new SqlParameter("@FK_Module_ID", SqlDbType.Int){ Value = ModuleDetailID }
                    };
                    DataRow drAccess = NawaDAO.ExecuteRow(query, prms);
                    MGroupMenuAccess MGMenuAccess = new MGroupMenuAccess();

                    if (drAccess != null)
                    {
                        MGMenuAccess = NawaDAO.ConvertDataTable<MGroupMenuAccess>(drAccess);
                    }
                    #endregion

                    return moduleDetail;
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public ModuleApprovalView GetModuleApprovalByID(long pkModuleApprovalID)
        {
            try
            {
                string query = @"SELECT ModuleApproval.*, ModuleAction.ModuleActionName 
                                 FROM ModuleApproval 
                                 INNER JOIN ModuleAction 
                                    ON ModuleApproval.PK_ModuleAction_ID = ModuleAction.PK_ModuleAction_ID 
                                 WHERE PK_ModuleApproval_ID = @ModuleApprovalID";
                SqlParameter[] prms = new SqlParameter[]
                {
                    new SqlParameter("@ModuleApprovalID", SqlDbType.Int){Value = pkModuleApprovalID}
                };
                DataRow dr = NawaDAO.ExecuteRow(query, prms);

                if (dr != null)
                {

                    ModuleApprovalView res = NawaDAO.ConvertDataTable<ModuleApprovalView>(dr);
                    res.oldData = JsonConvert.DeserializeObject<Dictionary<string, object>[]>(res.ModuleFieldBefore);
                    res.newData = JsonConvert.DeserializeObject<Dictionary<string, object>[]>(res.ModuleField);

                    return res;
                }
                return null;
            }
            catch
            {
                throw;
            }
        }
        public List<MWorkFlow_HistoryView> GetModuleWorkflowHistory(long PK_ModuleApproval_ID, int page, string orderBy, string order, string search, out int rowCount)
        {
            try
            {
                string query = "EXEC usp_CountWorkflowHistory @ModuleApprovalID, @Search";
                SqlParameter[] prms = new SqlParameter[]
                {
                    new SqlParameter("@ModuleApprovalID", SqlDbType.BigInt){ Value = PK_ModuleApproval_ID },
                    new SqlParameter("@Search", SqlDbType.VarChar){ Value = search ?? "" }
                };
                rowCount = Convert.ToInt32(NawaDAO.ExecuteScalar(query, prms));

                #region OrderBy
                string orderByFinal = "";
                string[] arrFields = { "PK_MWorkflow_History_ID", "FK_Module_ID", "FK_Unik_ID", "FK_MUserId", "FK_MRoleId"
                    , "intLevel", "RoleName", "UserName", "UserNameExecute", "CreatedDate_Basic", "ResponseDate"
                    , "FK_MWorkflow_ApprovalStatus_ID", "Notes", "Active", "CreatedBy", "LastUpdateBy", "ApprovedBy"
                    , "CreatedDate", "LastUpdateDate", "ApprovedDate", "Alternateby", "FK_ModuleApproval_ID", "ApprovalStatusName" };

                if (!string.IsNullOrEmpty(orderBy))
                {
                    string stringOrderBy = arrFields.Where(x => x.ToLower() == orderBy.ToLower()).FirstOrDefault();

                    if (!string.IsNullOrEmpty(stringOrderBy))
                    {
                        orderByFinal = stringOrderBy;

                        if (order != null && order.ToLower() == "desc")
                        {
                            orderByFinal += " desc";
                        }
                        else
                        {
                            orderByFinal += " asc";
                        }
                    }
                }
                #endregion

                int pageSize = Convert.ToInt32(SystemParameterBLL.GetAppSystemParameter(7).SettingValue);
                query = "EXEC usp_GetWorkflowHistory @ModuleApprovalID, @Search, @OrderBy, @PageIndex, @PageSize";
                prms = new SqlParameter[]
                {
                    new SqlParameter("@ModuleApprovalID", SqlDbType.BigInt){ Value = PK_ModuleApproval_ID },
                    new SqlParameter("@Search", SqlDbType.VarChar){ Value = search ?? "" },
                    new SqlParameter("@OrderBy", SqlDbType.VarChar){ Value = orderByFinal },
                    new SqlParameter("@PageIndex", SqlDbType.Int){ Value = page },
                    new SqlParameter("@PageSize", SqlDbType.Int){ Value = pageSize }
                };
                DataTable dt = NawaDAO.ExecuteTable(query, prms);
                List<MWorkFlow_HistoryView> res = NawaDAO.ConvertDataTable<MWorkFlow_HistoryView>(dt);

                return res;
            }
            catch
            {
                throw;
            }
        }

        public void ReviseModuleApproval(long PK_ModuleApproval_ID, int PK_MUser_ID, string Review)
        {
            string query = "SELECT * FROM ModuleApproval WHERE PK_ModuleApproval_ID = " + PK_ModuleApproval_ID;
            DataRow drModuleApproval = NawaDAO.ExecuteRow(query);
            ModuleApproval moduleApproval = NawaDAO.ConvertDataTable<ModuleApproval>(drModuleApproval);

            query = "SELECT * FROM Module WHERE ModuleName = @ModuleName";
            SqlParameter[] prms = new SqlParameter[]
            {
                    new SqlParameter("@ModuleName", SqlDbType.VarChar) { Value = moduleName }
            };
            DataRow drModule = NawaDAO.ExecuteRow(query, prms);
            Module module = NawaDAO.ConvertDataTable<Module>(drModule);

            prms = new SqlParameter[]
            {
                new SqlParameter("@FK_ModuleApproval_ID", SqlDbType.BigInt) { Value = PK_ModuleApproval_ID },
                new SqlParameter("@pkmoduleid", SqlDbType.Int) { Value = module.PK_Module_ID },
                new SqlParameter("@pkunik", SqlDbType.VarChar) { Value = (moduleApproval.PK_ModuleAction_ID == (int)EActionType.Import ? moduleApproval.PK_ModuleApproval_ID + "import" : moduleApproval.ModuleKey) },
                new SqlParameter("@pkuseridexecute", SqlDbType.Int) { Value = PK_MUser_ID },
                new SqlParameter("@notes", SqlDbType.VarChar) { Value = Review },
                new SqlParameter("@intworkflowstatus", SqlDbType.Int) { Value = (int)EActionApprovalType.Revise }
            };

            NawaDAO.ExecuteNonQuery("usp_SaveWorkflowModuleDesigner", prms, null, CommandType.StoredProcedure);
        }

    }
}
