﻿using Microsoft.Data.SqlClient;
using NawaDataDAL;
using NawaDataDAL.Models;
using Newtonsoft.Json;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;

namespace NawaDataBLL
{
    public class UserBLL
    {
        public int moduleID;
        public string moduleName;
        public List<ModuleField> moduleFields = new List<ModuleField>();

        private void SetModuleName()
        {
            //string query = "SELECT ModuleName FROM Module WHERE PK_Module_ID = @ModuleID";
            //SqlParameter[] prms = new SqlParameter[]
            //{
            //        new SqlParameter("@ModuleID", SqlDbType.Int){ Value = moduleID }
            //};
            //this.moduleName = Convert.ToString(NawaDAO.ExecuteScalar(query, prms));
            moduleName = "MUser";
        }

        private void SetModuleFields()
        {
            //string query = "SELECT * FROM ModuleField WHERE FK_Module_ID = @ModuleID";
            //SqlParameter[] prms = new SqlParameter[]
            //{
            //            new SqlParameter("@ModuleID", SqlDbType.Int){ Value = moduleID }
            //};
            //DataTable dt = NawaDAO.ExecuteTable(query, prms);
            //moduleFields = NawaDAO.ConvertDataTable<ModuleField>(dt);

            moduleFields.Add(new ModuleField() { FieldName = "PK_MUserID", FieldLabel = "ID", Sequence = 1, Required = false, IsPrimaryKey = true, IsUnik = true, IsShowInForm = false, IsShowInView = true, FK_FieldType_ID = (int)EFieldType.IDENTITY, FK_ExtType_ID = 7, IsUseRegexValidation = false, BCasCade = false });
            moduleFields.Add(new ModuleField() { FieldName = "ServerName", FieldLabel = "Server Name", Sequence = 2, Required = true, IsPrimaryKey = false, IsUnik = true, IsShowInForm = true, IsShowInView = true, FK_FieldType_ID = (int)EFieldType.VARCHAR, SizeField = 200, FK_ExtType_ID = 5, IsUseRegexValidation = false, BCasCade = false });
            moduleFields.Add(new ModuleField() { FieldName = "ServiceName", FieldLabel = "Service Name", Sequence = 3, Required = true, IsPrimaryKey = false, IsUnik = true, IsShowInForm = true, IsShowInView = true, FK_FieldType_ID = (int)EFieldType.VARCHAR, SizeField = 200, FK_ExtType_ID = 5, IsUseRegexValidation = false, BCasCade = false });
            moduleFields.Add(new ModuleField() { FieldName = "FK_ServiceStatus_ID", FieldLabel = "Service Status", Sequence = 4, Required = false, IsPrimaryKey = false, IsUnik = true, IsShowInForm = true, IsShowInView = true, FK_FieldType_ID = (int)EFieldType.ReferenceTable, FK_ExtType_ID = 2, TabelReferenceName = "MServiceStatus", TableReferenceFieldKey = "PK_ServiceStatus_ID", TableReferenceFieldDisplayName = "ServiceStatus", IsUseRegexValidation = false, BCasCade = false });
            moduleFields.Add(new ModuleField() { FieldName = "ErrorFile", FieldLabel = "Error File", Sequence = 5, Required = false, IsPrimaryKey = false, IsUnik = true, IsShowInForm = true, IsShowInView = true, FK_FieldType_ID = (int)EFieldType.VarBinary, FK_ExtType_ID = 8, IsUseRegexValidation = false, BCasCade = false });
        }

        public UserBLL(int moduleID)
        {
            this.moduleID = moduleID;

            SetModuleName();

            SetModuleFields();
        }

        public UserBLL(long moduleApprovalID)
        {
            string query = "SELECT PK_Module_ID FROM ModuleApproval JOIN Module ON ModuleApproval.ModuleName = Module.ModuleName WHERE PK_ModuleApproval_ID = @ModuleApprovalID";
            SqlParameter[] prms = new SqlParameter[]
            {
                        new SqlParameter("@ModuleApprovalID", SqlDbType.BigInt){ Value = moduleApprovalID }
            };
            this.moduleID = Convert.ToInt32(NawaDAO.ExecuteScalar(query, prms));

            SetModuleName();

            SetModuleFields();
        }

        public ModuleView GetModuleDetail(int moduleID, string id, long moduleApprovalID, string userID)
        {
            try
            {
                string query = "SELECT * FROM Module WHERE PK_Module_ID = @ModuleID";
                SqlParameter[] prms = new SqlParameter[]
                {
                    new SqlParameter("@ModuleID", SqlDbType.Int){ Value = moduleID }
                };
                DataRow dr = NawaDAO.ExecuteRow(query, prms);
                ModuleView moduleViewOutput = null;

                if (dr != null)
                {
                    moduleViewOutput = NawaDAO.ConvertDataTable<ModuleView>(dr);
                    //query = "SELECT * FROM ModuleField WHERE FK_Module_ID = @ModuleID";
                    //prms = new SqlParameter[]
                    //{
                    //    new SqlParameter("@ModuleID", SqlDbType.Int){ Value = moduleID }
                    //};
                    //DataTable dt = NawaDAO.ExecuteTable(query, prms);

                    if (moduleFields.Count > 0)
                    {
                        //List<ModuleField> moduleFields = NawaDAO.ConvertDataTable<ModuleField>(dt);

                        if (moduleFields != null)
                        {
                            moduleViewOutput.ModuleFields = moduleFields;
                        }

                        int lastSequence = moduleFields.Count > 0 ? moduleFields[moduleFields.Count - 1].Sequence : 0;
                        string[] array = { "CreatedBy", "LastUpdateBy", "ApprovedBy", "CreatedDate", "LastUpdateDate", "ApprovedDate" };

                        for (int i = 0; i < array.Length; i++)
                        {
                            Boolean isShow = Convert.ToBoolean(SystemParameterBLL.GetAppSystemParameter(i + 43).SettingValue);

                            if (isShow)
                            {
                                ModuleField moduleColums = new ModuleField();
                                moduleColums.PK_ModuleField_ID = 0;
                                moduleColums.FK_Module_ID = moduleID;
                                moduleColums.FieldName = array[i];
                                moduleColums.FieldLabel = array[i];
                                moduleColums.Sequence = lastSequence + i + 2;
                                moduleColums.Required = false;
                                moduleColums.IsPrimaryKey = false;
                                moduleColums.IsUnik = false;
                                moduleColums.IsShowInView = true;
                                moduleColums.IsShowInForm = false;
                                moduleColums.DefaultValue = "";
                                moduleColums.FK_FieldType_ID = (int)EFieldType.DATETIME;
                                moduleColums.FK_ExtType_ID = (int)EControlType.DisplayField;
                                moduleColums.SizeField = 0;
                                moduleColums.TabelReferenceName = "";
                                moduleColums.TabelReferenceNameAlias = "";
                                moduleColums.TableReferenceFieldKey = "";
                                moduleColums.TableReferenceFieldDisplayName = "";
                                moduleColums.TableReferenceFilter = "";
                                moduleColums.TableReferenceAdditonalJoin = "";
                                moduleColums.IsUseRegexValidation = false;
                                moduleColums.FieldNameParent = "";
                                moduleColums.FilterCascade = "";
                                moduleColums.ListRegex = new List<ModuleFieldRegexView>();
                                moduleColums.BCasCade = false;
                                moduleFields.Add(moduleColums);
                            }
                        }
                    }

                    prms = new SqlParameter[]
                    {
                        new SqlParameter("@pkmoduleid", SqlDbType.Int){ Value = moduleID }
                    };
                    int pkWorkflowID = Convert.ToInt32(NawaDAO.ExecuteScalar("IsHaveWorkflow", prms, null, CommandType.StoredProcedure));
                    moduleViewOutput.UseWorkflow = pkWorkflowID > 0;

                    if (String.IsNullOrEmpty(id) && moduleApprovalID > 0)
                    {
                        query = "SELECT ModuleKey FROM ModuleApproval WHERE PK_ModuleApproval_ID = " + moduleApprovalID;
                        id = Convert.ToString(NawaDAO.ExecuteScalar(query));
                    }

                    prms = new SqlParameter[]
                    {
                        new SqlParameter("@pkmoduleid", SqlDbType.Int){ Value = moduleID },
                        new SqlParameter("@pkunikid", SqlDbType.VarChar){ Value = id },
                        new SqlParameter("@userId", SqlDbType.VarChar){ Value = userID },
                        new SqlParameter("@action", SqlDbType.Int){ Value = EActionType.View }
                    };
                    pkWorkflowID = Convert.ToInt32(NawaDAO.ExecuteScalar("usp_ValidateMWorkflow", prms, null, CommandType.StoredProcedure));
                    moduleViewOutput.UserHasWorkflow = pkWorkflowID > 0;
                    prms = new SqlParameter[]
                    {
                        new SqlParameter("@PkMOduleid", SqlDbType.Int) { Value = moduleID },
                        new SqlParameter("@pkunikid", SqlDbType.VarChar) { Value = id }
                    };
                    pkWorkflowID = Convert.ToInt32(NawaDAO.ExecuteScalar("IsHaveWorkflowApproval", prms, null, CommandType.StoredProcedure));
                    moduleViewOutput.HaveActiveWorkflow = pkWorkflowID > 0;
                    moduleViewOutput.ActionList = new List<ActionView>();
                    List<MFieldType> ListFieldType = ModuleBLL.GetFieldType();

                    //if (moduleViewOutput.ModuleFields != null)
                    //{
                    //    foreach (ModuleField moduleField in moduleViewOutput.ModuleFields)
                    //    {
                    //        query = "SELECT * FROM ModuleFieldRegex WHERE FK_ModuleField_ID = " + moduleField.PK_ModuleField_ID;
                    //        DataTable dataRegex = NawaDAO.ExecuteTable(query);
                    //        List<ModuleFieldRegexView> moduleFieldRegexs = NawaDAO.ConvertDataTable<ModuleFieldRegexView>(dataRegex);
                    //        moduleField.ListRegex = moduleFieldRegexs;
                    //    }
                    //}

                    query = "SELECT FK_MGroupMenu_ID FROM MUser WHERE UserID = @UserID";
                    prms = new SqlParameter[]
                    {
                        new SqlParameter("@UserID", SqlDbType.VarChar){ Value = userID }
                    };
                    int mGroupMenuID = Convert.ToInt32(NawaDAO.ExecuteScalar(query, prms));
                    query = "SELECT * FROM MGroupMenuAccess WHERE FK_GroupMenu_ID = @FK_GroupMenu_ID AND FK_Module_ID = @FK_Module_ID";
                    prms = new SqlParameter[]
                    {
                        new SqlParameter("@FK_GroupMenu_ID", SqlDbType.Int){ Value = mGroupMenuID },
                        new SqlParameter("@FK_Module_ID", SqlDbType.Int){ Value = moduleID }
                    };
                    DataRow drAccess = NawaDAO.ExecuteRow(query, prms);
                    MGroupMenuAccess mGMenuAccess = new MGroupMenuAccess();

                    if (drAccess != null)
                    {
                        mGMenuAccess = NawaDAO.ConvertDataTable<MGroupMenuAccess>(drAccess);
                    }

                    if (moduleViewOutput.IsSupportAdd && mGMenuAccess.bAdd)
                    {
                        moduleViewOutput.ActionList.Add(new ActionView()
                        {
                            text = "Add",
                            url = moduleViewOutput.UrlAdd,
                            type = (int)EActionType.Insert
                        });
                    }

                    if (moduleViewOutput.IsSupportEdit && mGMenuAccess.bEdit)
                    {
                        moduleViewOutput.ActionList.Add(new ActionView()
                        {
                            text = "Edit",
                            url = moduleViewOutput.UrlEdit,
                            type = (int)EActionType.Update
                        });
                    }

                    if (moduleViewOutput.IsSupportDelete && mGMenuAccess.bDelete)
                    {
                        moduleViewOutput.ActionList.Add(new ActionView()
                        {
                            text = "Delete",
                            url = moduleViewOutput.UrlDelete,
                            type = (int)EActionType.Delete
                        });
                    }

                    if (moduleViewOutput.IsSupportDetail && mGMenuAccess.bDetail)
                    {
                        moduleViewOutput.ActionList.Add(new ActionView()
                        {
                            text = "Details",
                            url = moduleViewOutput.UrlDetail,
                            type = (int)EActionType.Detail
                        });
                    }

                    if (moduleViewOutput.IsSupportActivation && mGMenuAccess.bActivation)
                    {
                        moduleViewOutput.ActionList.Add(new ActionView()
                        {
                            text = "Activation",
                            url = moduleViewOutput.UrlActivation,
                            type = (int)EActionType.Activation
                        });
                    }

                    //query = "SELECT * FROM ModuleCustomSupport WHERE FK_Module_ID = @ModuleID";
                    //prms = new SqlParameter[]
                    //{
                    //    new SqlParameter("@ModuleID", SqlDbType.Int){ Value = moduleID }
                    //};
                    //DataTable dtCustom = NawaDAO.ExecuteTable(query, prms);

                    //if (dtCustom != null)
                    //{
                    //    List<ModuleCustomSupport> listCustomSupport = NawaDAO.ConvertDataTable<ModuleCustomSupport>(dtCustom);

                    //    foreach (ModuleCustomSupport supportData in listCustomSupport)
                    //    {
                    //        moduleViewOutput.ActionList.Add(new ActionView()
                    //        {
                    //            text = supportData.SupportName,
                    //            url = supportData.Target,
                    //            type = supportData.ActionType,
                    //            parameter = supportData.Parameter,
                    //            parameterValue = supportData.ParameterValue,
                    //        });
                    //    }
                    //}
                }

                return moduleViewOutput;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ModuleView GetValidModuleDetail(int ModuleID)
        {
            try
            {
                string query = "SELECT * FROM Module WHERE PK_Module_ID = @ModuleID";
                SqlParameter[] prms = new SqlParameter[] {
                    new SqlParameter("@ModuleID", SqlDbType.Int){ Value = ModuleID }
                };

                DataRow dr = NawaDAO.ExecuteRow(query, prms);

                if (dr != null)
                {
                    ModuleView moduleDetail = NawaDAO.ConvertDataTable<ModuleView>(dr);

                    //query = "SELECT * FROM ModuleField WHERE FK_Module_ID = @ModuleID";
                    //prms = new SqlParameter[] {
                    //    new SqlParameter("@ModuleID", SqlDbType.Int){ Value = ModuleID }
                    //};

                    //DataTable dt = NawaDAO.ExecuteTable(query, prms);
                    //if (dt != null)
                    {
                        //List<ModuleField> moduleFields = NawaDAO.ConvertDataTable<ModuleField>(dt);
                        if (moduleFields != null)
                        {
                            moduleFields.Where(item => item.IsPrimaryKey).FirstOrDefault().IsPrimaryKey = false;
                            moduleFields.Insert(0, new ModuleField()
                            {
                                FieldName = "nawa_Action",
                                FieldLabel = "Action",
                                IsPrimaryKey = false,
                                IsShowInView = true
                            });
                            moduleFields.Insert(0, new ModuleField()
                            {
                                FieldName = "nawa_recordnumber",
                                FieldLabel = "Line.",
                                IsPrimaryKey = true,
                                IsShowInView = true
                            });
                            moduleFields.Add(new ModuleField()
                            {
                                FieldName = "Active",
                                FieldLabel = "Active",
                                IsPrimaryKey = false,
                                IsShowInView = true
                            });

                            moduleDetail.ModuleFields = moduleFields;
                        }
                    }

                    return moduleDetail;
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public ModuleView GetInvalidModuleDetail(int ModuleID)
        {
            try
            {
                string query = "SELECT * FROM Module WHERE PK_Module_ID = @ModuleID";
                SqlParameter[] prms = new SqlParameter[] {
                    new SqlParameter("@ModuleID", SqlDbType.Int){ Value = ModuleID }
                };

                DataRow dr = NawaDAO.ExecuteRow(query, prms);

                if (dr != null)
                {
                    ModuleView moduleDetail = NawaDAO.ConvertDataTable<ModuleView>(dr);

                    //query = "SELECT * FROM ModuleField WHERE FK_Module_ID = @ModuleID";
                    //prms = new SqlParameter[] {
                    //    new SqlParameter("@ModuleID", SqlDbType.Int){ Value = ModuleID }
                    //};

                    //DataTable dt = NawaDAO.ExecuteTable(query, prms);
                    //if (dt != null)
                    {
                        //List<ModuleField> moduleFields = NawaDAO.ConvertDataTable<ModuleField>(dt);
                        if (moduleFields != null)
                        {
                            moduleFields.Where(item => item.IsPrimaryKey).FirstOrDefault().IsPrimaryKey = false;
                            moduleFields.Insert(0, new ModuleField()
                            {
                                FieldName = "KeteranganError",
                                FieldLabel = "Keterangan",
                                IsPrimaryKey = false,
                                IsShowInView = true
                            });
                            moduleFields.Insert(0, new ModuleField()
                            {
                                FieldName = "nawa_Action",
                                FieldLabel = "Action",
                                IsPrimaryKey = false,
                                IsShowInView = true
                            });
                            moduleFields.Insert(0, new ModuleField()
                            {
                                FieldName = "nawa_recordnumber",
                                FieldLabel = "Line.",
                                IsPrimaryKey = true,
                                IsShowInView = true
                            });
                            moduleFields.Add(new ModuleField()
                            {
                                FieldName = "Active",
                                FieldLabel = "Active",
                                IsPrimaryKey = false,
                                IsShowInView = true
                            });

                            moduleDetail.ModuleFields = moduleFields;
                        }
                    }

                    return moduleDetail;
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public List<DynamicDictionary> GetValidModuleData(int ModuleID, string UserID, int page, string orderBy, string order, string search, out int rowCount)
        {
            try
            {
                string query = "SELECT * FROM Module WHERE PK_Module_ID = @ModuleID";
                SqlParameter[] prms = new SqlParameter[] {
                    new SqlParameter("@ModuleID", SqlDbType.Int){ Value = ModuleID }
                };

                DataRow dr = NawaDAO.ExecuteRow(query, prms);

                if (dr != null)
                {
                    string moduleName = this.moduleName + "_upload";

                    //query = "SELECT * FROM ModuleField WHERE FK_Module_ID = @ModuleID";
                    //prms = new SqlParameter[] {
                    //    new SqlParameter("@ModuleID", SqlDbType.Int){ Value = ModuleID }
                    //};

                    //DataTable dtFields = NawaDAO.ExecuteTable(query, prms);

                    //string searchQuery = " WHERE KeteranganError = '' AND nawa_userid = '" + UserID + "' AND (1 = 1";
                    string searchQuery = "";
                    if (!String.IsNullOrEmpty(search))
                    {
                        //foreach (DataRow drField in dtFields.Rows)
                        foreach (ModuleField moduleField in moduleFields)
                        {
                            if (String.IsNullOrEmpty(searchQuery))
                            {
                                searchQuery = " WHERE ( CAST(" + Convert.ToString(moduleField.FieldName) + " AS VARCHAR) LIKE '%" + search.Replace("'", "''") + "%' ";
                            }
                            else
                            {
                                searchQuery = searchQuery + " OR CAST(" + Convert.ToString(moduleField.FieldName) + " AS VARCHAR) LIKE '%" + search.Replace("'", "''") + "%' ";
                            }
                        }
                    }

                    if (!String.IsNullOrEmpty(searchQuery))
                    {
                        searchQuery = searchQuery + " ) AND KeteranganError = '' AND nawa_userid = '" + UserID + "'";
                    }
                    else
                    {
                        searchQuery = " WHERE KeteranganError = '' AND nawa_userid = '" + UserID + "' ";
                    }

                    query = "SELECT COUNT(13) AS [RowCount] FROM " + moduleName + searchQuery;

                    rowCount = Convert.ToInt32(NawaDAO.ExecuteScalar(query));

                    int pageSize = Convert.ToInt32(SystemParameterBLL.GetAppSystemParameter(7).SettingValue);

                    query = "EXEC usp_PagingNew @Query, @OrderBy, @PageIndex, @PageSize";
                    prms = new SqlParameter[] {
                        new SqlParameter("@Query", SqlDbType.VarChar){ Value = "SELECT * FROM " + moduleName + searchQuery },
                        new SqlParameter("@OrderBy", SqlDbType.VarChar){ Value = (!String.IsNullOrEmpty(orderBy) ? orderBy + " " + order : "") },
                        new SqlParameter("@PageIndex", SqlDbType.Int){ Value = page },
                        new SqlParameter("@PageSize", SqlDbType.Int){ Value = pageSize }
                    };

                    System.Diagnostics.Debug.WriteLine("SELECT * FROM " + moduleName + searchQuery);

                    DataTable dt = NawaDAO.ExecuteTable(query, prms);

                    List<DynamicDictionary> res = dt.AsEnumerable().Select(row => new DynamicDictionary(row)).ToList();
                    return res;
                }

                rowCount = 0;

                return null;
            }
            catch
            {
                throw;
            }
        }

        public List<DynamicDictionary> GetInvalidModuleData(int ModuleID, string UserID, int page, string orderBy, string order, string search, out int rowCount)
        {
            try
            {
                string query = "SELECT * FROM Module WHERE PK_Module_ID = @ModuleID";
                SqlParameter[] prms = new SqlParameter[] {
                    new SqlParameter("@ModuleID", SqlDbType.Int){ Value = ModuleID }
                };

                DataRow dr = NawaDAO.ExecuteRow(query, prms);

                if (dr != null)
                {
                    string moduleName = this.moduleName + "_upload";

                    //query = "SELECT * FROM ModuleField WHERE FK_Module_ID = @ModuleID";
                    //prms = new SqlParameter[] {
                    //    new SqlParameter("@ModuleID", SqlDbType.Int){ Value = ModuleID }
                    //};

                    //DataTable dtFields = NawaDAO.ExecuteTable(query, prms);

                    //string searchQuery = " WHERE KeteranganError <> '' AND nawa_userid = '" + UserID + "' AND (1 = 1";
                    string searchQuery = "";
                    if (!String.IsNullOrEmpty(search))
                    {
                        //foreach (DataRow drField in dtFields.Rows)
                        foreach (ModuleField moduleField in moduleFields)
                        {
                            if (String.IsNullOrEmpty(searchQuery))
                            {
                                searchQuery = " WHERE ( CAST(" + Convert.ToString(moduleField.FieldName) + " AS VARCHAR) LIKE '%" + search.Replace("'", "''") + "%' ";
                            }
                            else
                            {
                                searchQuery = searchQuery + " OR CAST(" + Convert.ToString(moduleField.FieldName) + " AS VARCHAR) LIKE '%" + search.Replace("'", "''") + "%' ";
                            }
                        }
                    }

                    if (!String.IsNullOrEmpty(searchQuery))
                    {
                        searchQuery = searchQuery + " ) AND KeteranganError <> '' AND nawa_userid = '" + UserID + "'";
                    }
                    else
                    {
                        searchQuery = " WHERE KeteranganError <> '' AND nawa_userid = '" + UserID + "' ";
                    }

                    query = "SELECT COUNT(13) AS [RowCount] FROM " + moduleName + searchQuery;

                    rowCount = Convert.ToInt32(NawaDAO.ExecuteScalar(query));

                    int pageSize = Convert.ToInt32(SystemParameterBLL.GetAppSystemParameter(7).SettingValue);


                    query = "EXEC usp_PagingNew @Query, @OrderBy, @PageIndex, @PageSize";
                    prms = new SqlParameter[] {
                        new SqlParameter("@Query", SqlDbType.VarChar){ Value = "SELECT * FROM " + moduleName + searchQuery },
                        new SqlParameter("@OrderBy", SqlDbType.VarChar){ Value = (!String.IsNullOrEmpty(orderBy) ? orderBy + " " + order : "") },
                        new SqlParameter("@PageIndex", SqlDbType.Int){ Value = page },
                        new SqlParameter("@PageSize", SqlDbType.Int){ Value = pageSize }
                    };

                    DataTable dt = NawaDAO.ExecuteTable(query, prms);

                    List<DynamicDictionary> res = dt.AsEnumerable().Select(row => new DynamicDictionary(row)).ToList();
                    return res;
                }

                rowCount = 0;

                return null;
            }
            catch
            {
                throw;
            }
        }

        
        public DynamicDictionary GetModuleData(int moduleID, int id)
        {
            try
            {
                string query = "SELECT * FROM Module WHERE PK_Module_ID = @ModuleID";
                SqlParameter[] prms = new SqlParameter[]
                {
                    new SqlParameter("@ModuleID", SqlDbType.Int){ Value = moduleID }
                };
                DataRow dr = NawaDAO.ExecuteRow(query, prms);
                DynamicDictionary res = null;

                if (dr != null)
                {
                    //string moduleName = Convert.ToString(dr["ModuleName"]);
                    //query = "SELECT FieldName FROM ModuleField WHERE FK_Module_ID = @ModuleID AND IsPrimaryKey = 1";
                    //prms = new SqlParameter[]
                    //{
                    //    new SqlParameter("@ModuleID", SqlDbType.Int){ Value = moduleID }
                    //};
                    //string primaryField = Convert.ToString(NawaDAO.ExecuteScalar(query, prms));
                    string primaryField = moduleFields.Where(item => item.IsPrimaryKey).Select(item => item.FieldName).FirstOrDefault();

                    if (!string.IsNullOrEmpty(primaryField))
                    {
                        query = "SELECT * FROM " + moduleName + " WHERE " + primaryField + " = @ID";
                        prms = new SqlParameter[]
                        {
                            new SqlParameter("@ID", SqlDbType.Int){ Value = id }
                        };
                        DataRow row = NawaDAO.ExecuteRow(query, prms);

                        if (row != null)
                        {
                            res = new DynamicDictionary(row);
                        }
                    }
                }

                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DynamicDictionary> GetModuleData(int moduleID, int page, string orderBy, string order, string search, string filter, out int rowCount, string roleID)
        {
            try
            {
                string query = "SELECT * FROM Module WHERE PK_Module_ID = @ModuleID";
                SqlParameter[] prms = new SqlParameter[]
                {
                    new SqlParameter("@ModuleID", SqlDbType.Int){ Value = moduleID }
                };
                DataRow dr = NawaDAO.ExecuteRow(query, prms);
                List<DynamicDictionary> res = null;
                rowCount = 0;

                if (dr != null)
                {
                    //string moduleName = Convert.ToString(dr["ModuleName"]);
                    string moduleJoin = "";
                    string moduleColumns = "";
                    string searchQuery = "";
                    //query = "SELECT * FROM ModuleField WHERE FK_Module_ID = @ModuleID AND (IsShowInView = 1 OR IsPrimaryKey = 1)";
                    //prms = new SqlParameter[]
                    //{
                    //    new SqlParameter("@ModuleID", SqlDbType.Int){ Value = moduleID }
                    //};
                    //DataTable dtFields = NawaDAO.ExecuteTable(query, prms);

                    if (moduleFields.Count(item => item.IsShowInView || item.IsPrimaryKey) > 0)
                    {
                        if (!String.IsNullOrEmpty(search))
                        {
                            //foreach (DataRow drField in dtFields.Rows)
                            foreach (ModuleField moduleField in moduleFields.Where(item => item.IsShowInView || item.IsPrimaryKey).ToList())
                            {
                                if (String.IsNullOrEmpty(searchQuery))
                                {
                                    searchQuery = " WHERE ( CAST(" + moduleName + "." + Convert.ToString(moduleField.FieldName) + " AS VARCHAR) LIKE '%" + search.Replace("'", "''") + "%' ";
                                }
                                else
                                {
                                    searchQuery = searchQuery + " OR CAST(" + moduleName + "." + Convert.ToString(moduleField.FieldName) + " AS VARCHAR) LIKE '%" + search.Replace("'", "''") + "%' ";
                                }
                            }
                        }
                        else if (!String.IsNullOrEmpty(filter))
                        {
                            searchQuery = " WHERE (" + filter.Replace('"', ' ');
                        }

                        string queryDataAccess = "SELECT * From DataAccess Where FK_Module_ID = @ModuleID AND FK_Role_ID = @RoleID";
                        prms = new SqlParameter[]
                        {
                        new SqlParameter("@ModuleID", SqlDbType.Int){ Value = moduleID },
                        new SqlParameter("@RoleID", SqlDbType.Int){ Value = roleID }
                        };
                        DataTable dtDataAccess = NawaDAO.ExecuteTable(queryDataAccess, prms);

                        if (dtDataAccess != null && dtDataAccess.Rows.Count > 0)
                        {
                            string filterData = "";

                            foreach (DataRow drDataAccess in dtDataAccess.Rows)
                            {
                                filterData += " AND " + Convert.ToString(drDataAccess["FilterData"]);
                            }

                            string filterDataFinal = filterData.Remove(0, 5);

                            if (String.IsNullOrEmpty(searchQuery))
                            {
                                searchQuery = " WHERE " + filterDataFinal;
                            }
                            else
                            {
                                searchQuery = searchQuery + " AND (" + filterDataFinal + ")";
                            }
                        }

                        //foreach (DataRow drField in dtFields.Rows)
                        foreach (ModuleField moduleField in moduleFields.Where(item => item.IsShowInView || item.IsPrimaryKey).ToList())
                        {
                            if (!String.IsNullOrEmpty(moduleColumns))
                            {
                                moduleColumns += ", ";
                            }
                            if (Convert.ToInt32(moduleField.FK_FieldType_ID) == (int)EFieldType.ReferenceTable)
                            {
                                string referenceAlias = string.IsNullOrWhiteSpace(Convert.ToString(moduleField.TabelReferenceNameAlias)) ? Convert.ToString(moduleField.TabelReferenceName) : Convert.ToString(moduleField.TabelReferenceNameAlias);
                                moduleJoin += " LEFT JOIN " + moduleField.TabelReferenceName + " " + moduleField.TabelReferenceNameAlias + " ON " + moduleName + "." + moduleField.FieldName + " = " + referenceAlias + "." + moduleField.TableReferenceFieldKey;
                                moduleColumns += "'(' + CAST(" + moduleName + "." + moduleField.FieldName + " AS VARCHAR) + ') - ' + CAST(" + referenceAlias + "." + moduleField.TableReferenceFieldDisplayName + " AS VARCHAR) AS " + moduleField.FieldName;
                            }
                            else if (Convert.ToInt32(moduleField.FK_ExtType_ID) == (int)EControlType.FileUpload)
                            {
                                moduleColumns += moduleName + "." + moduleField.FieldName + ", ";
                                moduleColumns += moduleName + "." + moduleField.FieldName + "Name";
                            }
                            else
                            {
                                moduleColumns += moduleName + "." + moduleField.FieldName;
                            }
                        }

                        query = "SELECT * FROM ModuleCustomSupport WHERE FK_Module_ID = @ModuleID";
                        prms = new SqlParameter[]
                        {
                            new SqlParameter("@ModuleID", SqlDbType.Int){ Value = moduleID }
                        };
                        DataTable dtCustom = NawaDAO.ExecuteTable(query, prms);

                        if (dtCustom != null && dtCustom.Rows.Count > 0)
                        {
                            List<ModuleCustomSupport> listCustomSupport = NawaDAO.ConvertDataTable<ModuleCustomSupport>(dtCustom);
                            //List<ModuleField> listFields = NawaDAO.ConvertDataTable<ModuleField>(dtFields);
                            List<ModuleField> listFields = moduleFields.Where(item => item.IsShowInView || item.IsPrimaryKey).ToList();
                            ModuleField primaryField = listFields.Where(x => x.IsPrimaryKey).FirstOrDefault();

                            if (primaryField != null)
                            {
                                foreach (ModuleCustomSupport item in listCustomSupport)
                                {
                                    if (!string.IsNullOrEmpty(item.EnableConditionQuery))
                                    {
                                        if (!String.IsNullOrEmpty(moduleColumns))
                                        {
                                            moduleColumns += ", ";
                                        }

                                        string customSupportAlias = "ALIAS_" + item.SupportName.Replace(' ', '_');
                                        moduleColumns += "CASE WHEN EXISTS(SELECT " + customSupportAlias + ".* FROM " + moduleName + " AS " + customSupportAlias + " WHERE " + item.EnableConditionQuery + " AND " + customSupportAlias + "." + primaryField.FieldName + " = " + moduleName + "." + primaryField.FieldName + ") THEN 1 ELSE 0 END AS CUSTOM_" + item.SupportName.Replace(' ', '_');
                                    }
                                }
                            }
                            else
                            {
                                throw new Exception("This module has no primary key");
                            }
                        }

                        Boolean createdBy = Convert.ToBoolean(SystemParameterBLL.GetAppSystemParameter(43).SettingValue);
                        Boolean lastUpdateBy = Convert.ToBoolean(SystemParameterBLL.GetAppSystemParameter(44).SettingValue);
                        Boolean approvedBy = Convert.ToBoolean(SystemParameterBLL.GetAppSystemParameter(45).SettingValue);
                        Boolean createdDate = Convert.ToBoolean(SystemParameterBLL.GetAppSystemParameter(46).SettingValue);
                        Boolean lastUpdateDate = Convert.ToBoolean(SystemParameterBLL.GetAppSystemParameter(47).SettingValue);
                        Boolean approvedDate = Convert.ToBoolean(SystemParameterBLL.GetAppSystemParameter(48).SettingValue);

                        if (createdBy)
                        {
                            moduleColumns += ", " + moduleName + "." + "CreatedBy";
                        }

                        if (lastUpdateBy)
                        {
                            moduleColumns += ", " + moduleName + "." + "LastUpdateBy";
                        }

                        if (approvedBy)
                        {
                            moduleColumns += ", " + moduleName + "." + "ApprovedBy";
                        }

                        if (createdDate)
                        {
                            moduleColumns += ", " + moduleName + "." + "CreatedDate";
                        }

                        if (lastUpdateDate)
                        {
                            moduleColumns += ", " + moduleName + "." + "LastUpdateDate";
                        }

                        if (approvedDate)
                        {
                            moduleColumns += ", " + moduleName + "." + "ApprovedDate";
                        }

                        if (!String.IsNullOrEmpty(searchQuery))
                        {
                            searchQuery = searchQuery + " )";
                        }

                        query = "SELECT COUNT(13) AS [RowCount] FROM " + moduleName + searchQuery;
                        rowCount = Convert.ToInt32(NawaDAO.ExecuteScalar(query));
                        int pageSize = Convert.ToInt32(SystemParameterBLL.GetAppSystemParameter(7).SettingValue);
                        query = "EXEC usp_PagingNew @Query, @OrderBy, @PageIndex, @PageSize";
                        prms = new SqlParameter[]
                        {
                        new SqlParameter("@Query", SqlDbType.VarChar){ Value = "SELECT " + moduleColumns + " FROM " + moduleName + moduleJoin + searchQuery },
                        new SqlParameter("@OrderBy", SqlDbType.VarChar){ Value = (!String.IsNullOrEmpty(orderBy) ? orderBy + " " + order : "") },
                        new SqlParameter("@PageIndex", SqlDbType.Int){ Value = page },
                        new SqlParameter("@PageSize", SqlDbType.Int){ Value = pageSize }
                        };
                        DataTable dt = NawaDAO.ExecuteTable(query, prms);
                        res = dt.AsEnumerable().Select(row => new DynamicDictionary(row)).ToList();
                    }
                }

                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<ModuleApprovalView> GetModuleApproval(int moduleID, string userID, int page, string orderBy, string order, string search, string filterKey, string filterValue, out int rowCount)
        {
            try
            {

                string query = "EXEC usp_CountModuleApproval @ModuleKey, @UserID, @Search, @filterKey, @filterValue";


                SqlParameter[] prms = new SqlParameter[]
                {
                    new SqlParameter("@ModuleKey", SqlDbType.VarChar){ Value = moduleID.ToString() },
                    new SqlParameter("@UserID", SqlDbType.VarChar){ Value = userID },
                    new SqlParameter("@Search", SqlDbType.VarChar){ Value = search ?? "" },
                    new SqlParameter("@filterKey", SqlDbType.VarChar){ Value = filterKey ?? "" },
                    new SqlParameter("@filterValue", SqlDbType.VarChar){ Value = filterValue ?? "" },
                };
                rowCount = Convert.ToInt32(NawaDAO.ExecuteScalar(query, prms));
                int pageSize = Convert.ToInt32(SystemParameterBLL.GetAppSystemParameter(7).SettingValue);
                query = "EXEC usp_GetModuleApproval @ModuleKey, @UserID, @Search, @OrderBy, @PageIndex, @PageSize, @filterKey, @filterValue";
                prms = new SqlParameter[]
                {
                    new SqlParameter("@ModuleKey", SqlDbType.VarChar){ Value = moduleID.ToString() },
                    new SqlParameter("@UserID", SqlDbType.VarChar){ Value = userID },
                    new SqlParameter("@Search", SqlDbType.VarChar){ Value = search ?? "" },
                    new SqlParameter("@OrderBy", SqlDbType.VarChar){ Value = (!String.IsNullOrEmpty(orderBy) ? orderBy + " " + order : "") },
                    new SqlParameter("@PageIndex", SqlDbType.Int){ Value = page },
                    new SqlParameter("@PageSize", SqlDbType.Int){ Value = pageSize },
                    new SqlParameter("@filterKey", SqlDbType.VarChar){ Value = filterKey ?? "" },
                    new SqlParameter("@filterValue", SqlDbType.VarChar){ Value = filterValue ?? "" },

                };
                DataTable dt = NawaDAO.ExecuteTable(query, prms);
                List<ModuleApprovalView> res = NawaDAO.ConvertDataTable<ModuleApprovalView>(dt);

                return res;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        private void FinalApprove(Module module, ModuleApproval moduleApproval, string userID, out object identityTemp)
        {
            object identity = 0;
            //string query = "SELECT * FROM ModuleField WHERE FK_Module_ID = @ModuleID AND ( IsShowInForm = 1 OR IsPrimaryKey = 1 )";
            //SqlParameter[] prms = new SqlParameter[]
            //{
            //    new SqlParameter("@ModuleID", SqlDbType.Int) { Value = module.PK_Module_ID }
            //};
            //DataTable dtFields = NawaDAO.ExecuteTable(query, prms);
            List<SqlParameter> listParams = new List<SqlParameter>();

            if (moduleApproval.PK_ModuleAction_ID == (int)EActionType.Insert)
            {
                Dictionary<string, object>[] newData = JsonConvert.DeserializeObject<Dictionary<string, object>[]>(moduleApproval.ModuleField);
                string tableFields = "";
                string insertParams = "";

                //foreach (DataRow drField in dtFields.Rows)
                foreach (ModuleField moduleField in moduleFields.Where(item => item.IsShowInForm || item.IsPrimaryKey).ToList())
                {
                    if (!Convert.ToBoolean(moduleField.IsPrimaryKey))
                    {
                        if (Convert.ToInt32(moduleField.FK_FieldType_ID) == (int)EFieldType.ReferenceTable)
                        {
                            if (!String.IsNullOrEmpty(tableFields)) tableFields += ", ";

                            tableFields += moduleField.FieldName;

                            if (!String.IsNullOrEmpty(insertParams)) insertParams += ", ";

                            insertParams += "@" + moduleField.FieldName;
                            string value = Convert.ToString(newData[0][Convert.ToString(moduleField.FieldName)]);

                            listParams.Add(GetSqlParameterRaw(Convert.ToString(moduleField.FieldName), Convert.ToInt32(moduleField.FK_FieldType_ID), value));
                        }
                        else if (Convert.ToInt32(moduleField.FK_FieldType_ID) == (int)EFieldType.VarBinary)
                        {
                            string fieldFileName = Convert.ToString(moduleField.FieldName) + "Name";

                            if (!String.IsNullOrEmpty(tableFields)) tableFields += ", ";

                            tableFields += fieldFileName;

                            if (!String.IsNullOrEmpty(insertParams)) insertParams += ", ";

                            insertParams += "@" + fieldFileName;

                            listParams.Add(new SqlParameter("@" + fieldFileName, SqlDbType.VarChar) { Value = newData[0][fieldFileName] });
                            listParams.Add(GetSqlParameterRaw(Convert.ToString(moduleField.FieldName), Convert.ToInt32(moduleField.FK_FieldType_ID), Convert.FromBase64String(Convert.ToString(newData[0][Convert.ToString(moduleField.FieldName)]))));
                        }
                        else
                        {
                            if (!String.IsNullOrEmpty(tableFields)) tableFields += ", ";

                            tableFields += moduleField.FieldName;

                            if (!String.IsNullOrEmpty(insertParams)) insertParams += ", ";

                            insertParams += "@" + moduleField.FieldName;

                            listParams.Add(GetSqlParameterRaw(Convert.ToString(moduleField.FieldName), Convert.ToInt32(moduleField.FK_FieldType_ID), newData[0][Convert.ToString(moduleField.FieldName)] ?? DBNull.Value));
                        }
                    }
                }

                if (!String.IsNullOrEmpty(tableFields)) tableFields += ", ";

                tableFields += "Active, CreatedBy, LastUpdateBy, CreatedDate, LastUpdateDate";

                if (!String.IsNullOrEmpty(insertParams)) insertParams += ", ";

                insertParams += "@Active, @CreatedBy, @LastUpdateBy, GETDATE(), GETDATE()";

                listParams.Add(new SqlParameter("@Active", SqlDbType.Bit) { Value = true });
                listParams.Add(new SqlParameter("@CreatedBy", SqlDbType.VarChar) { Value = userID });
                listParams.Add(new SqlParameter("@LastUpdateBy", SqlDbType.VarChar) { Value = userID });

                string query = "INSERT INTO dbo." + moduleName + " (" + tableFields + ") " + Environment.NewLine;
                query += "VALUES (" + insertParams + ")" + Environment.NewLine; ;
                query += "SELECT SCOPE_IDENTITY()";
                identity = NawaDAO.ExecuteScalar(query, listParams.ToArray());
            }
            else if (moduleApproval.PK_ModuleAction_ID == (int)EActionType.Update)
            {
                Dictionary<string, object>[] newData = JsonConvert.DeserializeObject<Dictionary<string, object>[]>(moduleApproval.ModuleField);
                string updateFields = "";
                string updateKey = "";

                //foreach (DataRow drField in dtFields.Rows)
                foreach (ModuleField moduleField in moduleFields.Where(item => item.IsShowInForm || item.IsPrimaryKey).ToList())
                {
                    if (Convert.ToBoolean(moduleField.IsPrimaryKey))
                    {
                        updateKey = moduleField.FieldName + " = @" + moduleField.FieldName;
                        identity = newData[0][Convert.ToString(moduleField.FieldName)];
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(updateFields))
                        {
                            updateFields += ", " + Environment.NewLine;
                        }

                        updateFields += moduleField.FieldName + " = @" + moduleField.FieldName;
                    }

                    if (Convert.ToInt32(moduleField.FK_FieldType_ID) == (int)EFieldType.VarBinary)
                    {
                        string fieldFileName = Convert.ToString(moduleField.FieldName) + "Name";

                        if (!String.IsNullOrEmpty(updateFields))
                        {
                            updateFields += ", " + Environment.NewLine;
                        }

                        updateFields += fieldFileName + " = @" + fieldFileName;

                        listParams.Add(new SqlParameter("@" + fieldFileName, SqlDbType.VarChar) { Value = newData[0][fieldFileName] });
                        listParams.Add(GetSqlParameterRaw(Convert.ToString(moduleField.FieldName), Convert.ToInt32(moduleField.FK_FieldType_ID), Convert.FromBase64String(Convert.ToString(newData[0][Convert.ToString(moduleField.FieldName)]))));
                    }
                    else if (Convert.ToInt32(moduleField.FK_FieldType_ID) == (int)EFieldType.ReferenceTable)
                    {
                        string value = Convert.ToString(newData[0][Convert.ToString(moduleField.FieldName)]);

                        listParams.Add(GetSqlParameterRaw(Convert.ToString(moduleField.FieldName), Convert.ToInt32(moduleField.FK_FieldType_ID), value));
                    }
                    else
                    {
                        listParams.Add(GetSqlParameterRaw(Convert.ToString(moduleField.FieldName), Convert.ToInt32(moduleField.FK_FieldType_ID), newData[0][Convert.ToString(moduleField.FieldName)] ?? DBNull.Value));
                    }
                }

                if (!String.IsNullOrEmpty(updateFields))
                {
                    updateFields += ", " + Environment.NewLine;
                }

                updateFields += "LastUpdateBy = @LastUpdateBy, " + Environment.NewLine;
                updateFields += "LastUpdateDate = GETDATE() ";

                listParams.Add(new SqlParameter("@LastUpdateBy", SqlDbType.VarChar) { Value = userID });

                string query = "UPDATE dbo." + moduleName + Environment.NewLine;
                query += "SET " + updateFields + Environment.NewLine;
                query += "WHERE " + updateKey + Environment.NewLine;

                NawaDAO.ExecuteScalar(query, listParams.ToArray());
            }
            else if (moduleApproval.PK_ModuleAction_ID == (int)EActionType.Delete)
            {
                Dictionary<string, object>[] oldData = JsonConvert.DeserializeObject<Dictionary<string, object>[]>(moduleApproval.ModuleFieldBefore);
                //string query = "SELECT FieldName FROM ModuleField WHERE FK_Module_ID = @ModuleID AND IsPrimaryKey = 1";
                //SqlParameter[] prms = new SqlParameter[]
                //{
                //    new SqlParameter("@ModuleID", SqlDbType.Int){ Value = module.PK_Module_ID }
                //};
                //string fieldID = Convert.ToString(NawaDAO.ExecuteScalar(query, prms));
                string fieldID = moduleFields.Where(item => item.IsPrimaryKey).Select(item => item.FieldName).FirstOrDefault();
                string query = "DELETE dbo." + moduleName + " WHERE " + fieldID + " = @IDData";
                SqlParameter[] prms = new SqlParameter[]
                {
                    new SqlParameter("@IDData", SqlDbType.Int){ Value = oldData[0][fieldID] }
                };
                int rowAffected = NawaDAO.ExecuteNonQuery(query, prms);
            }

            identityTemp = identity;
        }

        public void ApproveModuleApproval(long pkModuleApprovalID, string userID, int pkMUserID, string review)
        {
            NawaConnection connection = NawaDAO.CreateConnection();
            connection.BeginTransaction();

            try
            {
                string query = "SELECT * FROM ModuleApproval WHERE PK_ModuleApproval_ID = @ID";
                SqlParameter[] prms = new SqlParameter[]
                {
                    new SqlParameter("@ID", SqlDbType.BigInt) { Value = pkModuleApprovalID }
                };
                DataRow drModuleApproval = NawaDAO.ExecuteRow(query, prms, connection);

                if (drModuleApproval != null)
                {
                    ModuleApproval moduleApproval = NawaDAO.ConvertDataTable<ModuleApproval>(drModuleApproval);
                    object identityTemp = 0;

                    if (moduleApproval.FK_Module_ID != null || moduleApproval.FK_Module_ID > 0)
                    {
                        query = "SELECT * FROM Module WHERE PK_Module_ID = @ModuleID";
                        prms = new SqlParameter[]
                        {
                            new SqlParameter("@ModuleID", SqlDbType.VarChar) { Value = moduleApproval.FK_Module_ID }
                        };
                    }
                    else
                    {
                        query = "SELECT * FROM Module WHERE ModuleName = @ModuleName";
                        prms = new SqlParameter[]
                        {
                            new SqlParameter("@ModuleName", SqlDbType.VarChar) { Value = moduleApproval.ModuleName }
                        };
                    }

                    DataRow drModule = NawaDAO.ExecuteRow(query, prms, connection);

                    if (drModule != null)
                    {
                        Module module = NawaDAO.ConvertDataTable<Module>(drModule);
                        prms = new SqlParameter[]
                        {
                            new SqlParameter("@PkMOduleid", SqlDbType.Int) { Value = module.PK_Module_ID },
                            new SqlParameter("@pkunikid", SqlDbType.VarChar) { Value = (moduleApproval.PK_ModuleAction_ID == (int)EActionType.Import ? moduleApproval.PK_ModuleApproval_ID + "import" : moduleApproval.ModuleKey) }
                        };
                        int workflowInprogress = Convert.ToInt32(NawaDAO.ExecuteScalar("IsHaveWorkflowApproval", prms, connection, CommandType.StoredProcedure));

                        if (workflowInprogress > 0)
                        {
                            bool bFinalApprove = false;
                            prms = new SqlParameter[]
                            {
                                new SqlParameter("@FK_ModuleApproval_ID", SqlDbType.BigInt) { Value = pkModuleApprovalID },
                                new SqlParameter("@pkmoduleid", SqlDbType.Int) { Value = module.PK_Module_ID },
                                new SqlParameter("@pkunik", SqlDbType.VarChar) { Value = (moduleApproval.PK_ModuleAction_ID == (int)EActionType.Import ? moduleApproval.PK_ModuleApproval_ID + "import" : moduleApproval.ModuleKey) },
                                new SqlParameter("@pkuseridexecute", SqlDbType.Int) { Value = pkMUserID },
                                new SqlParameter("@notes", SqlDbType.VarChar) { Value = review },
                                new SqlParameter("@intworkflowstatus", SqlDbType.Int) { Value = (int)EActionApprovalType.Approve }
                            };
                            bFinalApprove = Convert.ToBoolean(NawaDAO.ExecuteScalar("usp_SaveWorkflowModuleDesigner", prms, connection, CommandType.StoredProcedure));

                            if (bFinalApprove)
                            {
                                FinalApprove(module, moduleApproval, userID, out identityTemp);

                                prms = new SqlParameter[]
                                {
                                    new SqlParameter("@ModuleApprovalID", SqlDbType.Int){Value = pkModuleApprovalID}
                                };

                                query = "DELETE ModuleApproval WHERE PK_ModuleApproval_ID = @ModuleApprovalID";
                                NawaDAO.ExecuteNonQuery(query, prms, connection);
                            }
                        }
                        else
                        {
                            FinalApprove(module, moduleApproval, userID, out identityTemp);

                            prms = new SqlParameter[]
                            {
                                new SqlParameter("@ModuleApprovalID", SqlDbType.Int){Value = pkModuleApprovalID}
                            };

                            query = "DELETE ModuleApproval WHERE PK_ModuleApproval_ID = @ModuleApprovalID";
                            NawaDAO.ExecuteNonQuery(query, prms, connection);
                        }

                        List<Dictionary<string, object>> newData = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(moduleApproval.ModuleField);
                        List<Dictionary<string, object>> oldData = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(moduleApproval.ModuleFieldBefore);
                        long headerID = AuditTrailBLL.InputAuditTrailHeader(connection, userID, module.ModuleName, (EActionType)moduleApproval.PK_ModuleAction_ID, EAuditTrailStatus.AffectedDatabase, pkModuleApprovalID);

                        AuditTrailBLL.InputAuditTrailDetail(connection, headerID, (EActionType)moduleApproval.PK_ModuleAction_ID, newData, oldData);
                        
                        connection.Commit();
                        connection.Dispose();
                    }
                    else
                    {
                        throw new Exception("Module data was not found");
                    }
                }
                else
                {
                    throw new Exception("ModuleApproval data was not found");
                }
            }
            catch (Exception ex)
            {
                connection.Rollback();
                connection.Dispose();

                throw (ex);
            }
        }

        public void RejectModuleApproval(long pkModuleApprovalID, int pkMUserID, string review, string userID)
        {
            NawaConnection connection = NawaDAO.CreateConnection();
            connection.BeginTransaction();

            try
            {
                string query = "SELECT * FROM ModuleApproval WHERE PK_ModuleApproval_ID = " + pkModuleApprovalID;
                DataRow drModuleApproval = NawaDAO.ExecuteRow(query);

                if (drModuleApproval != null)
                {
                    ModuleApproval moduleApproval = NawaDAO.ConvertDataTable<ModuleApproval>(drModuleApproval);
                    SqlParameter[] prms = new SqlParameter[] { };

                    if (moduleApproval.FK_Module_ID != null || moduleApproval.FK_Module_ID > 0)
                    {
                        query = "SELECT * FROM Module WHERE PK_Module_ID = @ModuleID";
                        prms = new SqlParameter[]
                        {
                            new SqlParameter("@ModuleID", SqlDbType.VarChar) { Value = moduleApproval.FK_Module_ID }
                        };
                    }
                    else
                    {
                        query = "SELECT * FROM Module WHERE ModuleName = @ModuleName";
                        prms = new SqlParameter[]
                        {
                            new SqlParameter("@ModuleName", SqlDbType.VarChar) { Value = moduleApproval.ModuleName }
                        };
                    }

                    DataRow drModule = NawaDAO.ExecuteRow(query, prms);

                    if (drModule != null)
                    {
                        Module module = NawaDAO.ConvertDataTable<Module>(drModule);
                        prms = new SqlParameter[]
                        {
                            new SqlParameter("@PkMOduleid", SqlDbType.Int) { Value = module.PK_Module_ID },
                            new SqlParameter("@pkunikid", SqlDbType.VarChar) { Value = (moduleApproval.PK_ModuleAction_ID == (int)EActionType.Import ? moduleApproval.PK_ModuleApproval_ID + "import" : moduleApproval.ModuleKey) }
                        };
                        int workflowInprogress = Convert.ToInt32(NawaDAO.ExecuteScalar("IsHaveWorkflowApproval", prms, null, CommandType.StoredProcedure));

                        if (workflowInprogress > 0)
                        {
                            prms = new SqlParameter[]
                            {
                                new SqlParameter("@FK_ModuleApproval_ID", SqlDbType.BigInt) { Value = pkModuleApprovalID },
                                new SqlParameter("@pkmoduleid", SqlDbType.Int) { Value = module.PK_Module_ID },
                                new SqlParameter("@pkunik", SqlDbType.VarChar) { Value = (moduleApproval.PK_ModuleAction_ID == (int)EActionType.Import ? moduleApproval.PK_ModuleApproval_ID + "import" : moduleApproval.ModuleKey) },
                                new SqlParameter("@pkuseridexecute", SqlDbType.Int) { Value = pkMUserID },
                                new SqlParameter("@notes", SqlDbType.VarChar) { Value = review },
                                new SqlParameter("@intworkflowstatus", SqlDbType.Int) { Value = (int)EActionApprovalType.Approve }
                            };

                            NawaDAO.ExecuteNonQuery("usp_SaveWorkflowModuleDesigner", prms, null, CommandType.StoredProcedure);

                            query = "DELETE ModuleApproval WHERE PK_ModuleApproval_ID = @PK_ModuleApproval_ID";
                            prms = new SqlParameter[]
                            {
                                new SqlParameter("@PK_ModuleApproval_ID", SqlDbType.BigInt) { Value = pkModuleApprovalID }
                            };

                            NawaDAO.ExecuteNonQuery(query, prms, connection);
                        }
                        else
                        {
                            query = "DELETE ModuleApproval WHERE PK_ModuleApproval_ID = @PK_ModuleApproval_ID";
                            prms = new SqlParameter[]
                            {
                                new SqlParameter("@PK_ModuleApproval_ID", SqlDbType.BigInt) { Value = pkModuleApprovalID }
                            };

                            NawaDAO.ExecuteNonQuery(query, prms, connection);
                        }

                        List<Dictionary<string, object>> newData = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(moduleApproval.ModuleField);
                        List<Dictionary<string, object>> oldData = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(moduleApproval.ModuleFieldBefore);
                        long headerID = AuditTrailBLL.InputAuditTrailHeader(connection, userID, module.ModuleName, (EActionType)moduleApproval.PK_ModuleAction_ID, EAuditTrailStatus.Rejected, pkModuleApprovalID);

                        AuditTrailBLL.InputAuditTrailDetail(connection, headerID, (EActionType)moduleApproval.PK_ModuleAction_ID, newData, oldData);
                        
                        connection.Commit();
                        connection.Dispose();
                    }
                    else
                    {
                        throw new Exception("Module data was not found");
                    }
                }
                else
                {
                    throw new Exception("ModuleApproval data was not found");
                }
            }
            catch (Exception ex)
            {
                connection.Rollback();
                connection.Dispose();
                throw (ex);
            }
        }

        public MemoryStream ExportModuleData(int ModuleID, int page, string orderBy, string order, string search)
        {
            try
            {
                string query = "SELECT * FROM Module WHERE PK_Module_ID = @ModuleID";
                SqlParameter[] prms = new SqlParameter[]
                {
                    new SqlParameter("@ModuleID", SqlDbType.Int){ Value = ModuleID }
                };
                DataRow dr = NawaDAO.ExecuteRow(query, prms);
                MemoryStream memoryStreamOutput = null;

                if (dr != null)
                {
                    //string moduleName = Convert.ToString(dr["ModuleName"]);
                    //query = "SELECT * FROM ModuleField WHERE FK_Module_ID = @ModuleID AND IsShowInView = 1";
                    //prms = new SqlParameter[]
                    //{
                    //    new SqlParameter("@ModuleID", SqlDbType.Int){ Value = ModuleID }
                    //};
                    //DataTable dtFields = NawaDAO.ExecuteTable(query, prms);
                    string sFields = "";
                    string searchQuery = "";

                    //foreach (DataRow drField in dtFields.Rows)
                    foreach (ModuleField moduleField in moduleFields.Where(item => item.IsShowInView).ToList())
                    {
                        if (!String.IsNullOrEmpty(search))
                        {
                            if (String.IsNullOrEmpty(searchQuery))
                            {
                                searchQuery = " WHERE ( CAST(" + Convert.ToString(moduleField.FieldName) + " AS VARCHAR) LIKE '%" + search.Replace("'", "''") + "%' ";
                            }
                            else
                            {
                                searchQuery = searchQuery + " OR CAST(" + Convert.ToString(moduleField.FieldName) + " AS VARCHAR) LIKE '%" + search.Replace("'", "''") + "%' ";
                            }
                        }

                        if (!String.IsNullOrEmpty(sFields))
                        {
                            sFields = sFields + ", ";
                        }

                        sFields = sFields + Convert.ToString(moduleField.FieldName);
                    }

                    if (!String.IsNullOrEmpty(searchQuery))
                    {
                        searchQuery = searchQuery + " )";
                    }

                    int pageSize = Convert.ToInt32(SystemParameterBLL.GetAppSystemParameter(7).SettingValue);
                    query = "EXEC usp_PagingNew @Query, @OrderBy, @PageIndex, @PageSize";
                    prms = new SqlParameter[]
                    {
                        new SqlParameter("@Query", SqlDbType.VarChar){ Value = "SELECT " + sFields + " FROM " + moduleName + searchQuery },
                        new SqlParameter("@OrderBy", SqlDbType.VarChar){ Value = (!String.IsNullOrEmpty(orderBy) ? orderBy + " " + order : "") },
                        new SqlParameter("@PageIndex", SqlDbType.Int){ Value = page },
                        new SqlParameter("@PageSize", SqlDbType.Int){ Value = pageSize }
                    };
                    DataTable dt = NawaDAO.ExecuteTable(query, prms);
                    memoryStreamOutput = NawaDAO.ConvertToExcel(dt);
                }

                return memoryStreamOutput;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public MemoryStream ExportModuleDataAll(int ModuleID, string orderBy, string order, string search, string filter)
        {
            try
            {
                string query = "SELECT * FROM Module WHERE PK_Module_ID = @ModuleID";
                SqlParameter[] prms = new SqlParameter[]
                {
                    new SqlParameter("@ModuleID", SqlDbType.Int){ Value = ModuleID }
                };
                DataRow dr = NawaDAO.ExecuteRow(query, prms);
                MemoryStream memoryStreamOutput = null;

                if (dr != null)
                {
                    string sFields = "";
                    string searchQuery = "";
                    //string moduleName = Convert.ToString(dr["ModuleName"]);
                    //query = "SELECT * FROM ModuleField WHERE FK_Module_ID = @ModuleID AND IsShowInView = 1";
                    //prms = new SqlParameter[]
                    //{
                    //    new SqlParameter("@ModuleID", SqlDbType.Int){ Value = ModuleID }
                    //};
                    //DataTable dtFields = NawaDAO.ExecuteTable(query, prms);

                    //foreach (DataRow drField in dtFields.Rows)
                    foreach (ModuleField moduleField in moduleFields.Where(item => item.IsShowInView).ToList())
                    {
                        if (String.IsNullOrEmpty(filter))
                        {
                            if (!String.IsNullOrEmpty(search))
                            {
                                if (String.IsNullOrEmpty(searchQuery))
                                {
                                    searchQuery = " WHERE ( CAST(" + Convert.ToString(moduleField.FieldName) + " AS VARCHAR) LIKE '%" + search.Replace("'", "''") + "%' ";
                                }
                                else
                                {
                                    searchQuery = searchQuery + " OR CAST(" + Convert.ToString(moduleField.FieldName) + " AS VARCHAR) LIKE '%" + search.Replace("'", "''") + "%' ";
                                }
                            }
                        }

                        if (!String.IsNullOrEmpty(sFields))
                        {
                            sFields = sFields + ", ";
                        }

                        sFields = sFields + Convert.ToString(moduleField.FieldName);
                    }

                    if (!String.IsNullOrEmpty(filter))
                    {
                        searchQuery = " WHERE ( CAST(" + filter + " AS VARCHAR) LIKE '%" + search.Replace("'", "''") + "%' ";
                    }

                    if (!String.IsNullOrEmpty(searchQuery))
                    {
                        searchQuery = searchQuery + " )";
                    }

                    query = "EXEC usp_SelectNew @Query, @OrderBy";
                    prms = new SqlParameter[] {
                        new SqlParameter("@Query", SqlDbType.VarChar){ Value = "SELECT " + sFields + " FROM " + moduleName + searchQuery },
                        new SqlParameter("@OrderBy", SqlDbType.VarChar){ Value = (!String.IsNullOrEmpty(orderBy) ? orderBy + " " + order : "") }
                    };
                    DataTable dt = NawaDAO.ExecuteTable(query, prms);
                    memoryStreamOutput = NawaDAO.ConvertToExcel(dt);
                }

                return memoryStreamOutput;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public MemoryStream ExportModuleDataSelected(int ModuleID, List<string> SelectedData, string orderBy, string order)
        {
            try
            {
                string query = "SELECT * FROM Module WHERE PK_Module_ID = @ModuleID";
                SqlParameter[] prms = new SqlParameter[]
                {
                    new SqlParameter("@ModuleID", SqlDbType.Int){ Value = ModuleID }
                };
                DataRow dr = NawaDAO.ExecuteRow(query, prms);
                MemoryStream memoryStreamOutput = null;

                if (dr != null)
                {
                    //string moduleName = Convert.ToString(dr["ModuleName"]);
                    //query = "SELECT * FROM ModuleField WHERE FK_Module_ID = @ModuleID";
                    //prms = new SqlParameter[] {
                    //    new SqlParameter("@ModuleID", SqlDbType.Int){ Value = ModuleID }
                    //};
                    //DataTable dtFields = NawaDAO.ExecuteTable(query, prms);
                    string sFields = "";
                    string searchQuery = "";
                    string sSelected = String.Join(",", SelectedData.Select(item => "'" + item + "'").ToList());

                    //foreach (DataRow drField in dtFields.Rows)
                    foreach (ModuleField moduleField in moduleFields)
                    {
                        if (Convert.ToBoolean(moduleField.IsPrimaryKey))
                        {
                            searchQuery = " WHERE " + Convert.ToString(moduleField.FieldName) + " IN (" + sSelected + ")";
                        }

                        if (Convert.ToBoolean(moduleField.IsShowInView))
                        {
                            if (!String.IsNullOrEmpty(sFields)) sFields = sFields + ", ";
                            sFields = sFields + Convert.ToString(moduleField.FieldName);
                        }
                    }

                    query = "EXEC usp_SelectNew @Query, @OrderBy";
                    prms = new SqlParameter[]
                    {
                        new SqlParameter("@Query", SqlDbType.VarChar){ Value = "SELECT " + sFields + " FROM " + moduleName + searchQuery },
                        new SqlParameter("@OrderBy", SqlDbType.VarChar){ Value = (!String.IsNullOrEmpty(orderBy) ? orderBy + " " + order : "") }
                    };
                    DataTable dt = NawaDAO.ExecuteTable(query, prms);
                    memoryStreamOutput = NawaDAO.ConvertToExcel(dt);
                }

                return memoryStreamOutput;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public MemoryStream ExportUploadModuleDataSelected(int ModuleID, List<string> SelectedData, string orderBy, string order)
        {
            try
            {
                string query = "SELECT * FROM Module WHERE PK_Module_ID = @ModuleID";
                SqlParameter[] prms = new SqlParameter[] {
                    new SqlParameter("@ModuleID", SqlDbType.Int){ Value = ModuleID }
                };

                DataRow dr = NawaDAO.ExecuteRow(query, prms);

                if (dr != null)
                {
                    string moduleName = this.moduleName + "_upload";

                    //query = "SELECT * FROM ModuleField WHERE FK_Module_ID = @ModuleID";
                    //prms = new SqlParameter[] {
                    //    new SqlParameter("@ModuleID", SqlDbType.Int){ Value = ModuleID }
                    //};

                    //DataTable dtFields = NawaDAO.ExecuteTable(query, prms);
                    string sFields = "";

                    string searchQuery = "";
                    string sSelected = String.Join(",", SelectedData.Select(item => "'" + item + "'").ToList());
                    //foreach (DataRow drField in dtFields.Rows)
                    foreach (ModuleField moduleField in moduleFields)
                    {
                        if (Convert.ToBoolean(moduleField.IsPrimaryKey))
                        {
                            searchQuery = " WHERE " + Convert.ToString(moduleField.FieldName) + " IN (" + sSelected + ")";
                        }

                        if (Convert.ToBoolean(moduleField.IsShowInView))
                        {
                            if (!String.IsNullOrEmpty(sFields)) sFields = sFields + ", ";
                            sFields = sFields + Convert.ToString(moduleField.FieldName);
                        }
                    }

                    query = "EXEC usp_SelectNew @Query, @OrderBy";
                    prms = new SqlParameter[] {
                        new SqlParameter("@Query", SqlDbType.VarChar){ Value = "SELECT " + sFields + " FROM " + moduleName + searchQuery },
                        new SqlParameter("@OrderBy", SqlDbType.VarChar){ Value = (!String.IsNullOrEmpty(orderBy) ? orderBy + " " + order : "") }
                    };

                    DataTable dt = NawaDAO.ExecuteTable(query, prms);

                    return NawaDAO.ConvertToExcel(dt);
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public List<ModuleField> GetModuleField(int moduleID)
        {
            try
            {
                //string queryStr = @" 
                //SELECT 
	               // mf.PK_ModuleField_ID, mf.FK_Module_ID, mf.FieldName, mf.FieldLabel, mf.[Sequence], mf.[Required], mf.IsPrimaryKey, mf.IsUnik, 
	               // mf.IsShowInView, mf.IsShowInForm, mf.DefaultValue, mf.FK_FieldType_ID, ft.FieldTypeCaption, mf.SizeField, mf.FK_ExtType_ID, ext.ExtTypeName,
	               // mf.TabelReferenceName, mf.TabelReferenceNameAlias, mf.TableReferenceFieldKey, mf.TableReferenceFieldDisplayName, mf.TableReferenceFilter, 
	               // mf.IsUseRegexValidation, mf.TableReferenceAdditonalJoin, mf.BCasCade, mf.FieldNameParent, mf.FilterCascade
                //FROM ModuleField mf
                //LEFT JOIN MFieldType ft ON ft.PK_FieldType_ID = mf.FK_FieldType_ID
                //LEFT JOIN MExtType ext ON ext.PK_ExtType_ID = mf.FK_ExtType_ID
                //WHERE mf.FK_Module_ID = @ModuleID ";

                //SqlParameter[] queryParams = new SqlParameter[]
                //{
                //    new SqlParameter("@ModuleID", SqlDbType.Int){ Value = moduleID }
                //};

                //DataTable dtModuleField = NawaDAO.ExecuteTable(queryStr, queryParams);
                //List<ModuleField> listField = NawaDAO.ConvertDataTable<ModuleField>(dtModuleField);
                return moduleFields;
            }
            catch
            {
                throw;
            }
        }

        //public List<ModuleFieldRegexView> GetModuleFieldRegex(int moduleID)
        //{
        //    try
        //    {
        //        string queryStr = @" 
        //        SELECT rg.PK_ModuleFieldRegex, rg.FK_ModuleField_ID, mf.FieldName, rg.Regex
        //        FROM ModuleFieldRegex rg
        //        INNER JOIN ModuleField mf ON mf.PK_ModuleField_ID = rg.FK_ModuleField_ID
        //        WHERE EXISTS(SELECT 1 FROM Module md WHERE md.PK_Module_ID = mf.FK_Module_ID
				    //            AND md.PK_Module_ID = @ModuleID) ";

        //        SqlParameter[] queryParams = new SqlParameter[]
        //        {
        //            new SqlParameter("@ModuleID", SqlDbType.Int){ Value = moduleID }
        //        };

        //        DataTable dtRegex = NawaDAO.ExecuteTable(queryStr, queryParams);
        //        List<ModuleFieldRegexView> listRegex = NawaDAO.ConvertDataTable<ModuleFieldRegexView>(dtRegex);
        //        return listRegex;
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}

        //public List<ModuleValidationView> GetModuleValidation(int moduleID)
        //{
        //    try
        //    {
        //        string queryStr = @" 
        //        SELECT
	       //         mv.PK_ModuleValidation_ID, mv.FK_Module_ID, mv.FK_ModuleAction_ID, act.ModuleActionName, mv.FK_ModuleTime_ID, tm.ModuleTimeName,
	       //         mv.StoreProcedureName, mv.StoreProcedureParameter, mv.StoreProcedureParameterValueFieldSequence
        //        FROM ModuleValidation mv
        //        LEFT JOIN ModuleAction act ON act.PK_ModuleAction_ID = mv.FK_ModuleAction_ID
        //        LEFT JOIN ModuleTime tm ON tm.PK_ModuleTime_ID = mv.FK_ModuleTime_ID
        //        WHERE mv.FK_Module_ID = @ModuleID ";

        //        SqlParameter[] queryParams = new SqlParameter[]
        //        {
        //            new SqlParameter("@ModuleID", SqlDbType.Int){ Value = moduleID }
        //        };

        //        DataTable dtModuleValidation = NawaDAO.ExecuteTable(queryStr, queryParams);
        //        List<ModuleValidationView> listValidation = NawaDAO.ConvertDataTable<ModuleValidationView>(dtModuleValidation);
        //        return listValidation;
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}

        //public List<COption> GetListOptions(long PK_ModuleField_ID, string ParentValue)
        //{
        //    try
        //    {
        //        string queryStr = @"EXEC dbo.usp_GetListOptions @PK_ModuleField_ID, @ParentValue";

        //        SqlParameter[] queryParams = new SqlParameter[]
        //        {
        //            new SqlParameter("@PK_ModuleField_ID", SqlDbType.BigInt){ Value = PK_ModuleField_ID },
        //            new SqlParameter("@ParentValue", SqlDbType.VarChar){ Value = String.IsNullOrEmpty(ParentValue) ? DBNull.Value : (object)ParentValue }
        //        };

        //        DataTable dtListOptions = NawaDAO.ExecuteTable(queryStr, queryParams);
        //        List<COption> listOptions = NawaDAO.ConvertDataTable<COption>(dtListOptions);
        //        return listOptions;
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}

        public void SaveData(CParameterInput parameterInput, string userID, int pkMUserID)
        {
            NawaConnection connection = NawaDAO.CreateConnection();
            connection.BeginTransaction();

            try
            {
                string query = "SELECT * FROM Module WHERE PK_Module_ID = @ModuleID";
                bool isApproval = false;
                object identityTemp = 0;
                SqlParameter[] prms = new SqlParameter[]
                {
                    new SqlParameter("@ModuleID", SqlDbType.Int){ Value = parameterInput.ModuleID }
                };
                DataRow dr = NawaDAO.ExecuteRow(query, prms, connection);

                if (dr != null)
                {
                    //Validate Duplicate
                    string sUniqueCheck = "";
                    string sPrimaryCheck = "";
                    List<SqlParameter> listParams = new List<SqlParameter>();
                    SqlParameter primaryParam = null;
                    //query = "SELECT * FROM ModuleField WHERE FK_Module_ID = @ModuleID";
                    //prms = new SqlParameter[]
                    //{
                    //    new SqlParameter("@ModuleID", SqlDbType.Int) { Value = parameterInput.ModuleID }
                    //};
                    //DataTable dtFields = NawaDAO.ExecuteTable(query, prms, connection);

                    //if (dtFields != null && dtFields.Rows.Count > 0)
                    if (moduleFields.Count > 0)
                    {
                        //List<ModuleField> moduleFields = NawaDAO.ConvertDataTable<ModuleField>(dtFields);

                        foreach (KeyValuePair<string, object> inputData in parameterInput.data)
                        {
                            ModuleField dataField = moduleFields.Where(x => x.FieldName == inputData.Key && !x.IsPrimaryKey).FirstOrDefault();

                            if (dataField != null)
                            {
                                object inputValue = CommonBLL.GetObject(dataField.FK_FieldType_ID, inputData.Value);

                                if (dataField.Required && CommonBLL.StandartValidation(inputValue, EValidationErrorType.Empty))
                                {
                                    throw new Exception("Data " + inputData.Key + " is empty");
                                }

                                if (dataField.Required && CommonBLL.StandartValidation(inputValue, EValidationErrorType.Null))
                                {
                                    throw new Exception("Data " + inputData.Key + " is null");
                                }

                                if (
                                        dataField.FK_FieldType_ID == (int)EFieldType.VARCHAR
                                        || dataField.FK_FieldType_ID == (int)EFieldType.FLOAT
                                        || dataField.FK_FieldType_ID == (int)EFieldType.REAL
                                        || dataField.FK_FieldType_ID == (int)EFieldType.MONEY
                                        || dataField.FK_FieldType_ID == (int)EFieldType.VarBinary
                                    )
                                {
                                    if (CommonBLL.StandartValidation(inputValue.ToString(), dataField.SizeField, EValidationErrorType.Size))
                                    {
                                        throw new Exception("Data " + inputData.Key + " out of maximum character");
                                    }
                                }

                                //if (dataField.IsUseRegexValidation)
                                //{
                                //    query = "SELECT * FROM ModuleFieldRegex WHERE FK_ModuleField_ID = @ID";
                                //    prms = new SqlParameter[]
                                //    {
                                //        new SqlParameter("@ID", SqlDbType.Int) { Value = dataField.PK_ModuleField_ID }
                                //    };
                                //    DataTable dtRegex = NawaDAO.ExecuteTable(query, prms, connection);

                                //    if (dtRegex != null && dtRegex.Rows.Count > 0)
                                //    {
                                //        foreach (DataRow rowRegex in dtRegex.Rows)
                                //        {
                                //            if (CommonBLL.StandartValidation(inputValue, Convert.ToString(rowRegex["Regex"]), EValidationErrorType.Regex))
                                //            {
                                //                throw new Exception("Data " + inputData.Key + " out of maximum character");
                                //            }
                                //        }
                                //    }
                                //}
                            }
                        }

                        //foreach (DataRow field in dtFields.Rows)
                        foreach (ModuleField moduleField in moduleFields)
                        {
                            if (Convert.ToBoolean(moduleField.IsUnik) && Convert.ToInt32(moduleField.FK_FieldType_ID) != 12 && Convert.ToInt32(moduleField.FK_FieldType_ID) != 15)
                            {
                                if (!String.IsNullOrEmpty(sUniqueCheck)) sUniqueCheck += " OR ";

                                sUniqueCheck += moduleField.FieldName + " = @" + moduleField.FieldName;

                                listParams.Add(GetSqlParameter(Convert.ToString(moduleField.FieldName), Convert.ToInt32(moduleField.FK_FieldType_ID), parameterInput.data[Convert.ToString(moduleField.FieldName)]));
                            }

                            if (Convert.ToBoolean(moduleField.IsPrimaryKey))
                            {
                                sPrimaryCheck = moduleField.FieldName + " <> @" + moduleField.FieldName;
                                primaryParam = GetSqlParameter(Convert.ToString(moduleField.FieldName), Convert.ToInt32(moduleField.FK_FieldType_ID), parameterInput.data[Convert.ToString(moduleField.FieldName)]);
                            }
                        }

                        if (!String.IsNullOrEmpty(sUniqueCheck) && userID.ToLower() != "sysadmin")
                        {
                            query = "SELECT COUNT(13) AS DataCount FROM " + this.moduleName + " WHERE (" + sUniqueCheck + ")";

                            if (parameterInput.ActionType == EActionType.Update)
                            {
                                query += " AND " + sPrimaryCheck;

                                if (!listParams.Any(item => item.ParameterName == primaryParam.ParameterName)) listParams.Add(primaryParam);
                            }

                            int countDuplicate = Convert.ToInt32(NawaDAO.ExecuteScalar(query, listParams.ToArray(), connection));

                            if (countDuplicate > 0)
                            {
                                throw new Exception("Data Already Exists!");
                            }
                        }

                        //Before Save
                        ValidateStoreProcedure(1, parameterInput, moduleFields, connection, isApproval);

                        long approvalID = 0;
                        DataTable auditTrailOld = new DataTable();
                        DataTable auditTrailNew = new DataTable();

                        //Save Data
                        listParams = new List<SqlParameter>();

                        if (Convert.ToBoolean(dr["IsUseApproval"]) && userID.ToLower() != "sysadmin")
                        {
                            string oldJSON = "";
                            isApproval = true;
                            //query = "SELECT FieldName FROM ModuleField WHERE FK_Module_ID = @ModuleID AND IsPrimaryKey = 1";
                            //prms = new SqlParameter[]
                            //{
                            //    new SqlParameter("@ModuleID", SqlDbType.Int) { Value = parameterInput.ModuleID }
                            //};
                            string primaryFieldName = moduleFields.Where(item => item.IsPrimaryKey).Select(item => item.FieldName).FirstOrDefault();

                            if (parameterInput.ActionType == EActionType.Update)
                            {
                                query = "SELECT * FROM " + this.moduleName + " WHERE " + primaryFieldName + " = @" + primaryFieldName;
                                prms = new SqlParameter[]
                                {
                                    GetSqlParameter(primaryFieldName, (int)EFieldType.IDENTITY, parameterInput.data[primaryFieldName])
                                };
                                DataRow row = NawaDAO.ExecuteRow(query, prms, connection);
                                oldJSON = JsonConvert.SerializeObject(row.Table);

                                //For Audit Trail
                                auditTrailOld = row.Table;
                            }

                            int randKey = 0;
                            Dictionary<string, object> newData = new Dictionary<string, object>();

                            //foreach (DataRow field in dtFields.Rows)
                            foreach (ModuleField moduleField in moduleFields)
                            {
                                if (parameterInput.ActionType == EActionType.Insert)
                                {
                                    if (Convert.ToInt32(moduleField.FK_FieldType_ID) == 12 || Convert.ToInt32(moduleField.FK_FieldType_ID) == 15)
                                    {
                                        randKey = -((new Random()).Next(Int32.MaxValue - 2)) + 1;

                                        newData.Add(Convert.ToString(moduleField.FieldName), randKey);

                                        continue;
                                    }
                                }
                                if (Convert.ToInt32(moduleField.FK_ExtType_ID) == 8)
                                {
                                    if (NawaDataBLL.CommonBLL.GetObject(9, parameterInput.data[Convert.ToString(moduleField.FieldName) + "Name"]) == DBNull.Value)
                                    {
                                        newData.Add(Convert.ToString(moduleField.FieldName + "Name"), "");
                                    }
                                    else
                                    {
                                        newData.Add(Convert.ToString(moduleField.FieldName + "Name"), NawaDataBLL.CommonBLL.GetObject(9, parameterInput.data[Convert.ToString(moduleField.FieldName) + "Name"]));
                                    }
                                }

                                newData.Add(Convert.ToString(moduleField.FieldName), NawaDataBLL.CommonBLL.GetObject(Convert.ToInt32(moduleField.FK_FieldType_ID), parameterInput.data[Convert.ToString(moduleField.FieldName)]));
                            }

                            string newJSON = JsonConvert.SerializeObject(new Dictionary<string, object>[] { newData });
                            query = "EXEC usp_InsertModuleApproval @ModuleName, @ModuleKey, @ModuleField, @ModuleFieldBefore, @PK_ModuleAction_ID, @CreatedDate, @CreatedBy, @FK_Module_ID";
                            prms = new SqlParameter[]
                            {
                                new SqlParameter("@ModuleName", SqlDbType.VarChar) { Value = dr["ModuleName"] },
                                new SqlParameter("@ModuleKey", SqlDbType.VarChar) { Value = (randKey < 0 ? randKey : newData[primaryFieldName]) },
                                new SqlParameter("@ModuleField", SqlDbType.Text) { Value = newJSON },
                                new SqlParameter("@ModuleFieldBefore", SqlDbType.Text) { Value = oldJSON },
                                new SqlParameter("@PK_ModuleAction_ID", SqlDbType.Int) { Value = Convert.ToInt32(parameterInput.ActionType) },
                                new SqlParameter("@CreatedDate", SqlDbType.DateTime) { Value = DateTime.Now },
                                new SqlParameter("@CreatedBy", SqlDbType.VarChar) { Value = userID },
                                new SqlParameter("@FK_Module_ID", SqlDbType.Int) { Value = Convert.ToInt32(parameterInput.ModuleID) }
                            };
                            long lastID = Convert.ToInt64(NawaDAO.ExecuteScalar(query, prms, connection));

                            List<Dictionary<string, object>> listDetails = new List<Dictionary<string, object>>();

                            listDetails.Add(newData);

                            auditTrailNew = NawaDAO.ConvertListDictionary(listDetails);
                            approvalID = lastID;

                            if (parameterInput.ActionType == EActionType.Insert)
                            {
                                query = "DROP TABLE IF EXISTS dbo." + this.moduleName + "_WorkflowAdd_" + userID;

                                NawaDAO.ExecuteNonQuery(query, null, connection);

                                query = "SELECT TOP 0 * INTO dbo." + this.moduleName + "_WorkflowAdd_" + userID + " FROM dbo." + this.moduleName;

                                NawaDAO.ExecuteNonQuery(query, null, connection);

                                string tableFields = "";
                                string insertParams = "";
                                bool identity = false;

                                //foreach (DataRow drField in dtFields.Rows)
                                foreach (ModuleField moduleField in moduleFields)
                                {
                                    if ((Convert.ToInt32(moduleField.FK_FieldType_ID) == 12 && Convert.ToBoolean(moduleField.IsUnik)) || (Convert.ToInt32(moduleField.FK_FieldType_ID) == 15 && Convert.ToBoolean(moduleField.IsUnik)))
                                    {
                                        identity = true;

                                        if (!String.IsNullOrEmpty(tableFields)) tableFields += ", ";

                                        tableFields += moduleField.FieldName;

                                        if (!String.IsNullOrEmpty(insertParams)) insertParams += ", ";

                                        insertParams += "@" + moduleField.FieldName;

                                        listParams.Add(GetSqlParameterRaw(Convert.ToString(moduleField.FieldName), Convert.ToInt32(moduleField.FK_FieldType_ID), randKey < 0 ? randKey : newData[primaryFieldName]));
                                    }
                                    else
                                    {
                                        if (!String.IsNullOrEmpty(tableFields)) tableFields += ", ";

                                        tableFields += moduleField.FieldName;

                                        if (!String.IsNullOrEmpty(insertParams)) insertParams += ", ";

                                        insertParams += "@" + moduleField.FieldName;

                                        listParams.Add(GetSqlParameter(Convert.ToString(moduleField.FieldName), Convert.ToInt32(moduleField.FK_FieldType_ID), parameterInput.data[Convert.ToString(moduleField.FieldName)]));
                                    }
                                }

                                if (!String.IsNullOrEmpty(tableFields)) tableFields += ", ";

                                tableFields += "Active, CreatedBy, LastUpdateBy, CreatedDate, LastUpdateDate";

                                if (!String.IsNullOrEmpty(insertParams)) insertParams += ", ";

                                insertParams += "@Active, @CreatedBy, @LastUpdateBy, GETDATE(), GETDATE()";

                                listParams.Add(new SqlParameter("@Active", SqlDbType.Bit) { Value = true });

                                listParams.Add(new SqlParameter("@CreatedBy", SqlDbType.VarChar) { Value = userID });

                                listParams.Add(new SqlParameter("@LastUpdateBy", SqlDbType.VarChar) { Value = userID });

                                query = identity ? "SET IDENTITY_INSERT dbo." + this.moduleName + "_WorkflowAdd_" + userID + " ON;" + Environment.NewLine : "";
                                query += "INSERT INTO dbo." + this.moduleName + "_WorkflowAdd_" + userID + " (" + tableFields + ") " + Environment.NewLine;
                                query += "VALUES (" + insertParams + ");" + Environment.NewLine;
                                query += identity ? "SET IDENTITY_INSERT dbo." + this.moduleName + "_WorkflowAdd_" + userID + " OFF;" + Environment.NewLine : "";
                                int rowAffected = NawaDAO.ExecuteNonQuery(query, listParams.ToArray(), connection);
                            }

                            prms = new SqlParameter[]
                            {
                                new SqlParameter("@pkmoduleid", SqlDbType.Int){ Value = parameterInput.ModuleID },
                                new SqlParameter("@pkunikid", SqlDbType.VarChar){ Value = (randKey < 0 ? randKey : newData[primaryFieldName]) },
                                new SqlParameter("@userId", SqlDbType.VarChar){ Value = userID },
                                new SqlParameter("@action", SqlDbType.Int){ Value = parameterInput.ActionType }
                            };
                            int pkworkflowid = Convert.ToInt32(NawaDAO.ExecuteScalar("usp_ValidateMWorkflow", prms, connection, CommandType.StoredProcedure));

                            if (pkworkflowid > 0)
                            {
                                NawaDAO.ExecuteNonQuery("usp_CreateInitWorkflowModuleDesigner '" + pkworkflowid + "','" + (randKey < 0 ? randKey : newData[primaryFieldName]) + "','" + pkMUserID + "','" + parameterInput.ModuleID + "','" + pkMUserID + "','" + parameterInput.Review + "','3'," + lastID + ", '4'", null, connection);
                            }

                            identityTemp = approvalID;

                            //SaveMasterDetail(parameterInput.dataDetail, connection, userID, identityTemp, isApproval);
                        }
                        else
                        {
                            string primaryVal = "";
                            isApproval = false;
                            bool isPkIdentity = true;
                            int pkType = 0;

                            if (parameterInput.ActionType == EActionType.Insert)
                            {
                                string tableFields = "";
                                string insertParams = "";

                                //foreach (DataRow drField in dtFields.Rows)
                                foreach (ModuleField moduleField in moduleFields)
                                {
                                    if (Convert.ToBoolean(moduleField.IsPrimaryKey))
                                    {
                                        primaryVal = moduleField.FieldName.ToString();
                                        pkType = Convert.ToInt32(moduleField.FK_FieldType_ID);

                                        if (pkType != 12 && pkType != 15)
                                        {
                                            isPkIdentity = false;
                                        }
                                    }
                                    if (Convert.ToInt32(moduleField.FK_FieldType_ID) != 12 && Convert.ToInt32(moduleField.FK_FieldType_ID) != 15)
                                    {
                                        if (!String.IsNullOrEmpty(tableFields)) tableFields += ", ";

                                        tableFields += "[" + moduleField.FieldName + "]";

                                        if (!String.IsNullOrEmpty(insertParams)) insertParams += ", ";

                                        insertParams += "@" + moduleField.FieldName;

                                        if (Convert.ToBoolean(moduleField.FieldNameParent != ""))
                                        {
                                            int val = Convert.ToInt32(NawaDAO.ExecuteScalar($"SELECT IDENT_CURRENT('{this.moduleName}')"));
                                            listParams.Add(new SqlParameter("@" + moduleField.FieldName, SqlDbType.Int) { Value = val + 1 });
                                        }
                                        else
                                        {
                                            listParams.Add(GetSqlParameter(Convert.ToString(moduleField.FieldName), Convert.ToInt32(moduleField.FK_FieldType_ID), parameterInput.data[Convert.ToString(moduleField.FieldName)]));
                                        }
                                    }
                                    if (Convert.ToInt32(moduleField.FK_FieldType_ID) == 14)
                                    {
                                        string fieldFileName = Convert.ToString(moduleField.FieldName) + "Name";

                                        if (!String.IsNullOrEmpty(tableFields)) tableFields += ", ";

                                        tableFields += "[" + fieldFileName + "]";

                                        if (!String.IsNullOrEmpty(insertParams)) insertParams += ", ";

                                        insertParams += "@" + fieldFileName;

                                        listParams.Add(GetSqlParameter(fieldFileName, 9, parameterInput.data[fieldFileName]));
                                    }
                                }

                                if (!String.IsNullOrEmpty(tableFields)) tableFields += ", ";

                                tableFields += "Active, CreatedBy, LastUpdateBy, CreatedDate, LastUpdateDate";

                                if (!String.IsNullOrEmpty(insertParams)) insertParams += ", ";

                                insertParams += "@Active, @CreatedBy, @LastUpdateBy, GETDATE(), GETDATE()";

                                listParams.Add(new SqlParameter("@Active", SqlDbType.Bit) { Value = true });
                                listParams.Add(new SqlParameter("@CreatedBy", SqlDbType.VarChar) { Value = userID });
                                listParams.Add(new SqlParameter("@LastUpdateBy", SqlDbType.VarChar) { Value = userID });

                                query = "INSERT INTO dbo." + this.moduleName + " (" + tableFields + ") " + Environment.NewLine;
                                query += "VALUES (" + insertParams + ") ";
                                query += "SELECT SCOPE_IDENTITY()";
                            }
                            else if (parameterInput.ActionType == EActionType.Update)
                            {
                                string updateFields = "";
                                string updateKey = "";

                                //query = "SELECT FieldName FROM ModuleField WHERE FK_Module_ID = @ModuleID AND IsPrimaryKey = 1";
                                //prms = new SqlParameter[] { new SqlParameter("@ModuleID", SqlDbType.Int) { Value = parameterInput.ModuleID } };
                                //string primaryFieldName = Convert.ToString(NawaDAO.ExecuteScalar(query, prms, connection));
                                string primaryFieldName = moduleFields.Where(item => item.IsPrimaryKey).Select(item => item.FieldName).FirstOrDefault();
                                query = "SELECT * FROM " + this.moduleName + " WHERE " + primaryFieldName + " = @" + primaryFieldName;
                                prms = new SqlParameter[]
                                {
                                    GetSqlParameter(primaryFieldName, (int)EFieldType.IDENTITY, parameterInput.data[primaryFieldName])
                                };
                                DataRow row = NawaDAO.ExecuteRow(query, prms, connection);
                                auditTrailOld = row.Table;

                                //foreach (DataRow drField in dtFields.Rows)
                                foreach (ModuleField moduleField in moduleFields)
                                {
                                    if (Convert.ToBoolean(moduleField.IsPrimaryKey))
                                    {
                                        updateKey = "[" + moduleField.FieldName + "] = @" + moduleField.FieldName;
                                        primaryVal = moduleField.FieldName.ToString();
                                    }
                                    else
                                    {
                                        if (!String.IsNullOrEmpty(updateFields)) updateFields += ", " + Environment.NewLine;

                                        updateFields += "[" + moduleField.FieldName + "] = @" + moduleField.FieldName;
                                    }

                                    listParams.Add(GetSqlParameter(Convert.ToString(moduleField.FieldName), Convert.ToInt32(moduleField.FK_FieldType_ID), parameterInput.data[Convert.ToString(moduleField.FieldName)]));

                                    if (Convert.ToInt32(moduleField.FK_FieldType_ID) == 14)
                                    {
                                        string fieldFileName = Convert.ToString(moduleField.FieldName) + "Name";

                                        if (!String.IsNullOrEmpty(updateFields)) updateFields += ", " + Environment.NewLine;

                                        updateFields += "[" + fieldFileName + "] = @" + fieldFileName;

                                        listParams.Add(GetSqlParameter(fieldFileName, 9, parameterInput.data[fieldFileName]));
                                    }
                                }

                                if (!String.IsNullOrEmpty(updateFields)) updateFields += ", " + Environment.NewLine;

                                updateFields += "LastUpdateBy = @LastUpdateBy, " + Environment.NewLine;
                                updateFields += "LastUpdateDate = GETDATE() ";

                                listParams.Add(new SqlParameter("@LastUpdateBy", SqlDbType.VarChar) { Value = userID });

                                query = "UPDATE dbo." + this.moduleName + Environment.NewLine;
                                query += "SET " + updateFields + Environment.NewLine;
                                query += "WHERE " + updateKey + Environment.NewLine;
                                query += " SELECT " + updateKey + " FROM " + this.moduleName + " WHERE " + updateKey + Environment.NewLine;
                            }

                            if (isPkIdentity)
                            {
                                parameterInput.data[primaryVal] = NawaDAO.ExecuteScalar(query, listParams.ToArray(), connection);
                                identityTemp = parameterInput.data[primaryVal];
                            }
                            else
                            {
                                NawaDAO.ExecuteScalar(query, listParams.ToArray(), connection);
                                identityTemp = CommonBLL.GetObject(pkType, parameterInput.data[primaryVal]);
                            }


                            //SaveMasterDetail(parameterInput.dataDetail, connection, userID, identityTemp, isApproval);

                            //After Save
                            ValidateStoreProcedure(2, parameterInput, moduleFields, connection, isApproval);

                            List<Dictionary<string, object>> listDetails = new List<Dictionary<string, object>>();

                            listDetails.Add(parameterInput.data);

                            auditTrailNew = NawaDAO.ConvertListDictionary(listDetails);
                        }

                        long headerID = AuditTrailBLL.InputAuditTrailHeader(connection, userID, Convert.ToString(dr["ModuleName"]), parameterInput.ActionType, (approvalID > 0 ? EAuditTrailStatus.WaitingApproval : EAuditTrailStatus.AffectedDatabase), approvalID);

                        AuditTrailBLL.InputAuditTrailDetail(connection, headerID, parameterInput.ActionType, auditTrailNew, auditTrailOld);
                        
                        connection.Commit();
                        connection.Dispose();
                    }
                    else
                    {
                        throw new Exception("ModuleField data was not found");
                    }
                }
                else
                {
                    throw new Exception("Module data was not found");
                }
            }
            catch (Exception ex)
            {
                connection.Rollback();
                connection.Dispose();

                throw ex;
            }
        }

        public bool ValidateStoreProcedure(int validateTime, CParameterInput parameterInput, List<ModuleField> moduleFields, NawaConnection connection, bool isApproval)
        {
            string sql = @"SELECT StoreProcedureName, StoreProcedureParameter, StoreProcedureParameterValueFieldSequence
                           FROM dbo.ModuleValidation
                           WHERE FK_ModuleTime_ID = @ValidateTime AND FK_Module_ID = @PK_Module_ID AND FK_ModuleAction_ID = @PK_ModuleAction_ID";
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@PK_Module_ID", SqlDbType.Int) {Value = parameterInput.ModuleID},
                new SqlParameter("@PK_ModuleAction_ID", SqlDbType.Int) {Value = (int)parameterInput.ActionType},
                new SqlParameter("ValidateTime", SqlDbType.Int) {Value = validateTime}
            };
            DataTable dt = NawaDAO.ExecuteTable(sql, parameters, connection);

            if (dt != null)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    string[] spParameters = dr["StoreProcedureParameter"].ToString().Replace(" ", "").Split(",");
                    string[] spParameterSequences = dr["StoreProcedureParameterValueFieldSequence"].ToString().Replace(" ", "").Split(",");

                    if (spParameters.Length > 0 && spParameters.Length == spParameterSequences.Length)
                    {
                        List<SqlParameter> sqlParameters = new List<SqlParameter>();

                        if (!String.IsNullOrEmpty(dr["StoreProcedureParameter"].ToString()))
                        {
                            for (int i = 0; i < spParameters.Length; i++)
                            {
                                ModuleField mf = moduleFields.Where(item => item.Sequence.ToString() == spParameterSequences[i]).FirstOrDefault();

                                if (mf.FK_FieldType_ID == 12 || mf.FK_FieldType_ID == 15)
                                {
                                    SqlParameter parameter = new SqlParameter(spParameters[i], validateTime == 1 ? (parameterInput.ActionType == EActionType.Insert ? 0 : isApproval ? NawaDataBLL.CommonBLL.GetObject(mf.FK_FieldType_ID, parameterInput.data[mf.FieldName]) : NawaDataBLL.CommonBLL.GetObject(mf.FK_FieldType_ID, parameterInput.data[mf.FieldName])) : parameterInput.data[mf.FieldName]);

                                    sqlParameters.Add(parameter);
                                }
                                else
                                {
                                    SqlParameter parameter = new SqlParameter(spParameters[i], NawaDataBLL.CommonBLL.GetObject(mf.FK_FieldType_ID, parameterInput.data[mf.FieldName]));

                                    sqlParameters.Add(parameter);
                                }
                            }
                        }

                        if (validateTime == 1)
                        {
                            DataTable dtResult = NawaDAO.ExecuteTable(dr["StoreProcedureName"].ToString(), sqlParameters.ToArray(), connection, CommandType.StoredProcedure);

                            if (dtResult != null)
                            {
                                string strError = "";

                                foreach (DataRow drResult in dtResult.Rows)
                                {
                                    strError += drResult[0] + Environment.NewLine;
                                }

                                if (!String.IsNullOrEmpty(strError))
                                {
                                    throw new Exception(strError);
                                }
                            }
                        }
                        else if (validateTime == 2)
                        {
                            NawaDAO.ExecuteNonQuery(dr["StoreProcedureName"].ToString(), sqlParameters.ToArray(), connection, CommandType.StoredProcedure);
                        }
                    }
                    else
                    {
                        throw new Exception("Sql Parameter list and sql Value list not match");
                    }
                }
            }

            return true;
        }

        public void SaveData(CParameterDetail parameterDetail, string userID)
        {
            NawaConnection connection = NawaDAO.CreateConnection();
            connection.BeginTransaction();

            try
            {
                bool isApproval = false;
                long scopeIdentity = 0;
                string query = "SELECT * FROM Module WHERE PK_Module_ID = @ModuleID";
                SqlParameter[] prms = new SqlParameter[]
                {
                    new SqlParameter("@ModuleID", SqlDbType.Int){ Value = parameterDetail.ModuleID }
                };
                DataRow dr = NawaDAO.ExecuteRow(query, prms, connection);

                if (dr != null)
                {
                    DataTable auditTrailOld = new DataTable();
                    DataTable auditTrailNew = new DataTable();

                    if (Convert.ToBoolean(dr["IsUseApproval"]) && userID.ToLower() != "sysadmin")
                    {
                        isApproval = true;
                        //query = "SELECT FieldName FROM ModuleField WHERE FK_Module_ID = @ModuleID AND IsPrimaryKey = 1";
                        //prms = new SqlParameter[]
                        //{
                        //    new SqlParameter("@ModuleID", SqlDbType.Int) { Value = parameterDetail.ModuleID }
                        //};
                        //string primaryFieldName = Convert.ToString(NawaDAO.ExecuteScalar(query, prms, connection));
                        string primaryFieldName = moduleFields.Where(item => item.IsPrimaryKey).Select(item => item.FieldName).FirstOrDefault();

                        if (!string.IsNullOrEmpty(primaryFieldName))
                        {
                            query = "SELECT * FROM " + this.moduleName + " WHERE " + primaryFieldName + " = @" + primaryFieldName;
                            prms = new SqlParameter[]
                            {
                                new SqlParameter(primaryFieldName, SqlDbType.Int) { Value = parameterDetail.data }
                            };
                            DataRow row = NawaDAO.ExecuteRow(query, prms, connection);

                            if (row != null)
                            {
                                string oldJSON = JsonConvert.SerializeObject(row.Table);
                                string newJSON = "";
                                auditTrailOld = row.Table;

                                if (parameterDetail.ActionType == EActionType.Activation)
                                {
                                    newJSON = oldJSON;
                                    auditTrailNew = row.Table;
                                }
                                else if (parameterDetail.ActionType == EActionType.Delete)
                                {
                                    newJSON = "";
                                }

                                query = "INSERT INTO ModuleApproval(ModuleName, ModuleKey, ModuleField, ModuleFieldBefore, PK_ModuleAction_ID, CreatedDate, CreatedBy, FK_Module_ID) VALUES(@ModuleName, @ModuleKey, @ModuleField, @ModuleFieldBefore, @PK_ModuleAction_ID, @CreatedDate, @CreatedBy, @FK_Module_ID) SELECT SCOPE_IDENTITY() AS LastID";
                                prms = new SqlParameter[]
                                {
                                    new SqlParameter("@ModuleName", SqlDbType.VarChar) { Value = dr["ModuleName"] },
                                    new SqlParameter("@ModuleKey", SqlDbType.VarChar) { Value = parameterDetail.data },
                                    new SqlParameter("@ModuleField", SqlDbType.Text) { Value = newJSON },
                                    new SqlParameter("@ModuleFieldBefore", SqlDbType.Text) { Value = oldJSON },
                                    new SqlParameter("@PK_ModuleAction_ID", SqlDbType.Int) { Value = Convert.ToInt32(parameterDetail.ActionType) },
                                    new SqlParameter("@CreatedDate", SqlDbType.DateTime) { Value = DateTime.Now },
                                    new SqlParameter("@CreatedBy", SqlDbType.VarChar) { Value = userID },
                                    new SqlParameter("@FK_Module_ID", SqlDbType.Int) { Value = parameterDetail.ModuleID }
                                };
                                long lastID = Convert.ToInt64(NawaDAO.ExecuteScalar(query, prms, connection));
                                scopeIdentity = lastID;
                            }
                            else
                            {
                                throw new Exception("Module data was not found");
                            }
                        }
                        else
                        {
                            throw new Exception("Module doesn't have a primary key");
                        }
                    }
                    else
                    {
                        //query = "SELECT FieldName FROM ModuleField WHERE FK_Module_ID = @ModuleID AND IsPrimaryKey = 1";
                        //prms = new SqlParameter[]
                        //{
                        //    new SqlParameter("@ModuleID", SqlDbType.Int){ Value = parameterDetail.ModuleID }
                        //};
                        //string fieldID = Convert.ToString(NawaDAO.ExecuteScalar(query, prms, connection));
                        string fieldID = moduleFields.Where(item => item.IsPrimaryKey).Select(item => item.FieldName).FirstOrDefault();

                        query = "SELECT * FROM " + this.moduleName + " WHERE " + fieldID + " = @IDData";
                        prms = new SqlParameter[]
                        {
                            new SqlParameter("@IDData", SqlDbType.Int){ Value = parameterDetail.data }
                        };
                        DataRow rowOld = NawaDAO.ExecuteRow(query, prms, connection);

                        if (rowOld != null)
                        {
                            auditTrailOld = rowOld.Table;
                        }

                        if (!string.IsNullOrEmpty(fieldID))
                        {
                            if (parameterDetail.ActionType == EActionType.Delete)
                            {
                                query = "DELETE dbo." + this.moduleName + " WHERE " + fieldID + " = @IDData";
                                prms = new SqlParameter[]
                                {
                                    new SqlParameter("@IDData", SqlDbType.Int){ Value = parameterDetail.data }
                                };
                            }
                            else if (parameterDetail.ActionType == EActionType.Activation)
                            {
                                query = "UPDATE dbo." + this.moduleName + " SET Active = CASE WHEN Active = 1 THEN 0 ELSE 1 END WHERE " + fieldID + " = @IDData";
                                prms = new SqlParameter[]
                                {
                                    new SqlParameter("@IDData", SqlDbType.Int){ Value = parameterDetail.data }
                                };
                            }

                            int rowAffected = NawaDAO.ExecuteNonQuery(query, prms, connection);

                            query = "SELECT * FROM " + this.moduleName + " WHERE " + fieldID + " = @IDData";
                            prms = new SqlParameter[]
                            {
                                new SqlParameter("@IDData", SqlDbType.Int){ Value = parameterDetail.data }
                            };
                            DataRow rowNew = NawaDAO.ExecuteRow(query, prms, connection);

                            if (rowNew != null)
                            {
                                auditTrailNew = rowNew.Table;
                            }
                        }
                        else
                        {
                            throw new Exception("Module doesn't have a primary key");
                        }
                    }

                    long headerID = AuditTrailBLL.InputAuditTrailHeader(connection, userID, Convert.ToString(dr["ModuleName"]), parameterDetail.ActionType, (scopeIdentity > 0 ? EAuditTrailStatus.WaitingApproval : EAuditTrailStatus.AffectedDatabase), scopeIdentity);

                    AuditTrailBLL.InputAuditTrailDetail(connection, headerID, parameterDetail.ActionType, auditTrailNew, auditTrailOld);

                    connection.Commit();
                    connection.Dispose();
                }
                else
                {
                    throw new Exception("Module data was not found");
                }
            }
            catch (Exception ex)
            {
                connection.Rollback();
                connection.Dispose();

                throw ex;
            }
        }

        public MemoryStream ExportTemplateData(int moduleID, string UserID, int PK_MUser_ID)
        {
            string parentColRefName = "";
            string query = @"SELECT * FROM Module WHERE PK_Module_ID = " + moduleID;
            DataRow drModule = NawaDAO.ExecuteRow(query);
            Module module = NawaDAO.ConvertDataTable<Module>(drModule);

            //query = @"SELECT * FROM ModuleField WHERE FK_Module_ID = " + moduleID + " ORDER BY Sequence";
            //DataTable dtListModuleField = NawaDAO.ExecuteTable(query);
            //List<ModuleField> moduleFields = NawaDAO.ConvertDataTable<ModuleField>(dtListModuleField);

            query = "SELECT * FROM ModuleFieldDefault";
            DataTable dtModuleFieldDefaults = NawaDAO.ExecuteTable(query);
            List<ModuleFieldDefault> moduleFieldDefaults = NawaDAO.ConvertDataTable<ModuleFieldDefault>(dtModuleFieldDefaults);

            query = GenerateSqlImportData(module, moduleFields, moduleFieldDefaults, UserID, PK_MUser_ID);

            System.Diagnostics.Debug.WriteLine(query);

            DataTable objtb = NawaDAO.ExecuteTable(query);

            int intmaxrow = 0;
            if (objtb.Rows.Count + 1000 > 1048575)
            {
                intmaxrow = 1048575;
            }
            else
            {
                intmaxrow = objtb.Rows.Count + 1000;
            }

            IWorkbook workbook = new XSSFWorkbook();
            ISheet objWorksheetData = workbook.CreateSheet("Data");
            ISheet objWorksheetParam = workbook.CreateSheet("Parameter");
            IName name;

            int intCounterReference = 0;
            List<IRow> parameterRows = new List<IRow>();
            if (moduleFields.Any(item => item.FK_FieldType_ID == (int)EFieldType.ReferenceTable && item.FK_ExtType_ID != (int)EControlType.PopUpMultiCheckBox))
            {
                parameterRows.Add(objWorksheetParam.CreateRow(0));
                foreach (ModuleField item in moduleFields.Where(item => item.FK_FieldType_ID == (int)EFieldType.ReferenceTable && item.FK_ExtType_ID != (int)EControlType.PopUpMultiCheckBox).ToList())
                {
                    ICell cell = parameterRows[0].CreateCell(intCounterReference, CellType.String);
                    cell.SetCellValue(item.FieldLabel);

                    int intx = 1;
                    string stralias = String.IsNullOrEmpty(item.TabelReferenceNameAlias) ? item.TabelReferenceName : item.TabelReferenceNameAlias;
                    string strfilter = item.TableReferenceFilter.Replace("@userid", UserID);
                    strfilter = strfilter.Replace("@PK_MUser_ID", PK_MUser_ID.ToString());

                    query = GetQueryRef(item.TabelReferenceName + " " + stralias, item.TableReferenceFieldKey, item.TableReferenceFieldDisplayName, item.TableReferenceAdditonalJoin, strfilter, item.BCasCade, UserID, PK_MUser_ID);
                    DataTable objdata = NawaDAO.ExecuteTable(query);

                    foreach (DataRow item1 in objdata.Rows)
                    {
                        if (intx >= parameterRows.Count) parameterRows.Add(objWorksheetParam.CreateRow(intx));

                        parameterRows[intx].CreateCell(intCounterReference, CellType.String).SetCellValue("(" + item1[item.TableReferenceFieldKey] + ") " + item1[item.TableReferenceFieldDisplayName]);

                        intx++;
                    }

                    name = workbook.CreateName();
                    name.NameName = item.FieldName;
                    name.RefersToFormula = "Parameter!$" + GetExcelColumnName(intCounterReference + 1) + "$2:$" + GetExcelColumnName(intCounterReference + 1) + "$" + intx;
                    name.Comment = "Parameter!" + GetExcelColumnName(intCounterReference + 1) + "2:" + GetExcelColumnName(intCounterReference + 1) + intx;

                    intCounterReference++;
                }
            }

            SystemParameter objParamSettingUpload = SystemParameterBLL.GetSystemParameterValue(31);
            bool bParam = false;

            if (objParamSettingUpload != null) bParam = Convert.ToBoolean(objParamSettingUpload.SettingValue);

            int intcounterx = 0;

            if (bParam)
            {
                if (intcounterx >= parameterRows.Count) parameterRows.Add(objWorksheetParam.CreateRow(intcounterx));

                parameterRows[intcounterx].CreateCell(intCounterReference, CellType.String).SetCellValue("Action");

                if (module.IsSupportAdd)
                {
                    intcounterx++;
                    if (intcounterx >= parameterRows.Count) parameterRows.Add(objWorksheetParam.CreateRow(intcounterx));
                    parameterRows[intcounterx].CreateCell(intCounterReference, CellType.String).SetCellValue("Insert");
                }

                if (module.IsSupportEdit)
                {
                    intcounterx++;
                    if (intcounterx >= parameterRows.Count) parameterRows.Add(objWorksheetParam.CreateRow(intcounterx));
                    parameterRows[intcounterx].CreateCell(intCounterReference, CellType.String).SetCellValue("Update");
                }

                if (module.IsSupportDelete)
                {
                    intcounterx++;
                    if (intcounterx >= parameterRows.Count) parameterRows.Add(objWorksheetParam.CreateRow(intcounterx));
                    parameterRows[intcounterx].CreateCell(intCounterReference, CellType.String).SetCellValue("Delete");
                }
            }
            else
            {
                if (intcounterx >= parameterRows.Count) parameterRows.Add(objWorksheetParam.CreateRow(intcounterx));
                parameterRows[intcounterx].CreateCell(intCounterReference, CellType.String).SetCellValue("Action");
                intcounterx++;
                if (intcounterx >= parameterRows.Count) parameterRows.Add(objWorksheetParam.CreateRow(intcounterx));
                parameterRows[intcounterx].CreateCell(intCounterReference, CellType.String).SetCellValue("Insert");
                intcounterx++;
                if (intcounterx >= parameterRows.Count) parameterRows.Add(objWorksheetParam.CreateRow(intcounterx));
                parameterRows[intcounterx].CreateCell(intCounterReference, CellType.String).SetCellValue("Update");
                intcounterx++;
                if (intcounterx >= parameterRows.Count) parameterRows.Add(objWorksheetParam.CreateRow(intcounterx));
                parameterRows[intcounterx].CreateCell(intCounterReference, CellType.String).SetCellValue("Delete");
            }

            name = workbook.CreateName();
            name.NameName = "Action";
            name.RefersToFormula = "Parameter!$" + GetExcelColumnName(intCounterReference + 1) + "$2:$" + GetExcelColumnName(intCounterReference + 1) + "$" + (intcounterx + 1);
            
            XSSFFont headerFont = (XSSFFont)workbook.CreateFont();
            headerFont.IsBold = true;
            headerFont.SetColor(new XSSFColor(Color.White));
            XSSFCellStyle headerStyle = (XSSFCellStyle)workbook.CreateCellStyle();
            headerStyle.SetFont(headerFont);
            headerStyle.FillPattern = FillPattern.SolidForeground;
            headerStyle.SetFillForegroundColor(new XSSFColor(Color.Blue));

            IDataFormat dataFormatCustom = workbook.CreateDataFormat();
            XSSFCellStyle dateStyle = (XSSFCellStyle)workbook.CreateCellStyle();
            dateStyle.DataFormat = dataFormatCustom.GetFormat(SystemParameterBLL.GetSystemParameterValue(6).SettingValue);

            IRow headerRow = objWorksheetData.CreateRow(0);
            ICell firstCell = headerRow.CreateCell(0, CellType.String);
            firstCell.SetCellValue("Action");
            firstCell.CellStyle = headerStyle;
            //firstCell.CellComment = ...

            XSSFDataValidationHelper dvHelper = new XSSFDataValidationHelper((XSSFSheet)objWorksheetData);
            XSSFDataValidationConstraint dvConstraint = (XSSFDataValidationConstraint)dvHelper.CreateFormulaListConstraint("Action");
            CellRangeAddressList addressList = new CellRangeAddressList(1, intmaxrow - 1, 0, 0);
            XSSFDataValidation validation = (XSSFDataValidation)dvHelper.CreateValidation(dvConstraint, addressList);
            validation.ShowErrorBox = true;
            objWorksheetData.AddValidationData(validation);

            int intDataY = 1;

            foreach (ModuleField item in moduleFields.OrderBy(item => item.Sequence).ToList())
            {
                ICell headerCell = headerRow.CreateCell(intDataY, CellType.String);
                headerCell.SetCellValue(item.FieldLabel);
                headerCell.CellStyle = headerStyle;

                switch (item.FK_FieldType_ID)
                {
                    case (int)EFieldType.ReferenceTable:
                        if (item.FK_ExtType_ID != (int)EControlType.PopUpMultiCheckBox)
                        {
                            XSSFDataValidationHelper fieldDVHelper = new XSSFDataValidationHelper((XSSFSheet)objWorksheetData);
                            XSSFDataValidationConstraint fieldDVConstraint = (XSSFDataValidationConstraint)fieldDVHelper.CreateFormulaListConstraint(item.FieldName);
                            CellRangeAddressList fieldAddressList = new CellRangeAddressList(1, intmaxrow - 1, intDataY, intDataY);
                            XSSFDataValidation fieldValidation = (XSSFDataValidation)fieldDVHelper.CreateValidation(fieldDVConstraint, fieldAddressList);
                            fieldValidation.ShowErrorBox = false;
                            objWorksheetData.AddValidationData(fieldValidation);
                        }
                        break;
                    case (int)EFieldType.IDENTITY:
                    case (int)EFieldType.BIGIDENTITY:
                        if (item.IsPrimaryKey)
                        {

                        }
                        else
                        {

                        }
                        break;
                    default:
                        break;
                }

                intDataY++;
            }

            if (module.IsSupportActivation)
            {
                foreach (ModuleFieldDefault item in moduleFieldDefaults.FindAll(item => item.PK_ModuleField_ID == 1))
                {
                    if (item.FK_FieldType_ID == 13) //BooleanValue
                    {
                        ICell lastCell = headerRow.CreateCell(intDataY, CellType.String);
                        lastCell.SetCellValue(item.FieldLabel);
                        lastCell.CellStyle = headerStyle;
                    }
                }
            }

            foreach (DataRow row in objtb.Rows)
            {
                IRow excelRow = objWorksheetData.CreateRow(objtb.Rows.IndexOf(row) + 1);
                excelRow.CreateCell(0).SetCellValue("Update");

                for (int i = 0; i < objtb.Columns.Count; i++)
                {
                    if (row[i] != DBNull.Value)
                    {
                        ICell excelCell = excelRow.CreateCell(i + 1);

                        switch (Type.GetTypeCode(objtb.Columns[i].DataType))
                        {
                            case TypeCode.Int16:
                            case TypeCode.Int32:
                            case TypeCode.Int64:
                            case TypeCode.Single:
                            case TypeCode.Double:
                                excelCell.SetCellValue(Convert.ToDouble(row[i]));
                                break;
                            case TypeCode.Boolean:
                                excelCell.SetCellValue(Convert.ToBoolean(row[i]));
                                break;
                            case TypeCode.DateTime:
                                excelCell.CellStyle = dateStyle;
                                excelCell.SetCellValue(Convert.ToDateTime(row[i]));
                                break;
                            case TypeCode.String:
                                string val = Convert.ToString(row[i]);
                                if (val.Length > 32767) val = val.Substring(0, 32767);
                                excelCell.SetCellValue(val);
                                break;
                            default:
                                excelCell.SetCellValue(Convert.ToString(row[i]));
                                break;
                        }
                    }
                }
            }

            for (int i = 0; i <= objtb.Columns.Count; i++) objWorksheetData.AutoSizeColumn(i);

            for (int i = 0; i < moduleFields.Count; i++)
            {
                if (!moduleFields[i].IsShowInForm)
                {
                    objWorksheetData.SetColumnHidden(i + 1, true);
                }
            }

            MemoryStream ms = new MemoryStream();
            using (MemoryStream tempStream = new MemoryStream())
            {
                workbook.Write(tempStream);
                byte[] byteArray = tempStream.ToArray();
                ms.Write(byteArray, 0, byteArray.Length);
            }

            return ms;
        }

        private string GenerateSqlImportData(Module module, List<ModuleField> moduleFields, List<ModuleFieldDefault> moduleFieldDefaults, string UserID, int PK_MUser_ID)
        {
            string strprimarykey = "";
            string strfield = "";
            List<string> arrtablename = new List<string>();
            string strtable = " from " + this.moduleName + " ";
            foreach (ModuleField item in moduleFields.OrderBy(item => item.Sequence).ToList())
            {
                if (item.IsPrimaryKey)
                {
                    strprimarykey = item.FieldName;
                }

                string stralias = String.IsNullOrEmpty(item.TabelReferenceNameAlias) ? item.TabelReferenceName : item.TabelReferenceNameAlias;

                if (item.FK_FieldType_ID == (int)EFieldType.ReferenceTable)
                {
                    if (item.FK_ExtType_ID == (int)EControlType.PopUpMultiCheckBox)
                    {
                        strfield += this.moduleName + "." + item.FieldName + ",";
                    }
                    else
                    {
                        string strjoin = "";
                        if (!String.IsNullOrEmpty(item.TableReferenceAdditonalJoin))
                        {
                            strjoin = item.TableReferenceAdditonalJoin.Replace("@userid", UserID);
                            strjoin = strjoin.Replace("@PK_MUser_ID", PK_MUser_ID.ToString());
                        }

                        string strfulljoin = " left join " + item.TabelReferenceName + " as " + stralias + " on " + this.moduleName + "." + item.FieldName + "=" + stralias + "." + item.TableReferenceFieldKey + " " + strjoin;
                        if (!arrtablename.Contains(strfulljoin))
                        {
                            strtable += strfulljoin;
                            arrtablename.Add(strfulljoin);
                        }

                        strfield += " '('+ CONVERT(VARCHAR, " + this.moduleName + "." + item.FieldName + ") +') '+ " + stralias + "." + item.TableReferenceFieldDisplayName + " AS " + item.FieldName + ",";
                    }
                }
                else if (item.FK_FieldType_ID == (int)EFieldType.VarBinary)
                {
                    if (item.FK_ExtType_ID == (int)EControlType.FileUpload)
                    {
                        strfield += this.moduleName + "." + item.FieldName + "Name AS " + item.FieldName + ",";
                    }
                    else
                    {
                        strfield += "'' AS " + item.FieldName + ",";
                    }
                }
                else
                {
                    strfield += this.moduleName + "." + item.FieldName + ",";
                }
            }

            foreach (ModuleFieldDefault item in moduleFieldDefaults.FindAll(item => item.PK_ModuleField_ID == 1))
            {
                if (module.IsSupportActivation)
                {
                    if (item.FK_FieldType_ID == 13)
                    {
                        strfield += this.moduleName + "." + item.FieldName + ",";
                    }
                }
            }

            if (!String.IsNullOrEmpty(strfield))
            {
                strfield = strfield.Remove(strfield.Length - 1, 1);
            }

            strtable = " select " + strfield + strtable;

            //Filter Data Access
            //Filter User Branch & Date Process

            strtable = " SELECT * FROM ( " + strtable + ")tabletoquery";

            if (!String.IsNullOrEmpty(strprimarykey))
            {
                strtable += " order by " + strprimarykey;
            }

            return strtable;
        }

        public bool SaveParameterUpload(int moduleID, Stream fileStream, string UserID)
        {
            //baca file excel bentuk ke datatable
            DataTable dtImportData = new DataTable();

            XSSFWorkbook wb = new XSSFWorkbook(fileStream);

            ISheet ws = wb.GetSheetAt(0);
            IRow headerRow = ws.GetRow(0);
            foreach (ICell cell in headerRow.Cells)
            {
                dtImportData.Columns.Add(cell.StringCellValue);
            }

            NawaConnection conn = NawaDAO.CreateConnection();
            conn.BeginTransaction();

            //string queryStr = @"SELECT * FROM ModuleField WHERE FK_Module_ID = " + moduleID + " AND ( IsShowInForm = 1 OR IsPrimaryKey = 1 ) ORDER BY Sequence";
            //DataTable dtListModuleField = NawaDAO.ExecuteTable(queryStr, null, conn);
            //List<ModuleField> moduleFields = NawaDAO.ConvertDataTable<ModuleField>(dtListModuleField);
            List<ModuleField> moduleFieldShow = moduleFields.Where(item => item.IsShowInForm || item.IsPrimaryKey).ToList();

            for (int rowNum = 1; rowNum <= ws.LastRowNum; rowNum++)
            {
                IRow wsRow = ws.GetRow(rowNum);
                DataRow row = dtImportData.NewRow();
                bool bvalid = false;
                foreach (ICell cell in wsRow.Cells)
                {

                    if (cell.CellType != CellType.Blank)
                    {
                        bvalid = true;
                    }

                    switch (cell.CellType)
                    {
                        case CellType.Blank:
                            row[headerRow.GetCell(cell.ColumnIndex).StringCellValue] = DBNull.Value;
                            break;
                        case CellType.Boolean:
                            row[headerRow.GetCell(cell.ColumnIndex).StringCellValue] = cell.BooleanCellValue;
                            break;
                        case CellType.Numeric:
                            if (moduleFieldShow.Any(item => item.FieldLabel == headerRow.GetCell(cell.ColumnIndex).StringCellValue && item.FK_FieldType_ID == 10))
                            {
                                row[headerRow.GetCell(cell.ColumnIndex).StringCellValue] = DateTime.FromOADate(cell.NumericCellValue).ToString("dd MMM yyyy", new System.Globalization.CultureInfo("en-US"));
                            }
                            else
                            {
                                row[headerRow.GetCell(cell.ColumnIndex).StringCellValue] = cell.NumericCellValue;
                            }
                            break;
                        case CellType.String:
                            row[headerRow.GetCell(cell.ColumnIndex).StringCellValue] = cell.StringCellValue;
                            break;
                        case CellType.Formula:
                            row[headerRow.GetCell(cell.ColumnIndex).StringCellValue] = cell.StringCellValue;
                            break;
                        default:
                            row[headerRow.GetCell(cell.ColumnIndex).StringCellValue] = cell.StringCellValue;
                            break;
                    }
                }

                if (bvalid)
                {
                    dtImportData.Rows.Add(row);
                }
            }

            int intStartY = headerRow.GetCell(0).StringCellValue == "Action" ? 1 : 0;

            try
            {
                foreach (ModuleField moduleField in moduleFieldShow.OrderBy(item => item.Sequence).ToList())
                {
                    if (headerRow.GetCell(intStartY).StringCellValue != moduleField.FieldLabel)
                    {
                        throw new Exception("Column " + (intStartY + 1) + ": " + headerRow.GetCell(intStartY).StringCellValue + " does not match with db : " + moduleField.FieldLabel);
                    }

                    intStartY++;
                }

                wb.Close();

                //string queryStr = @"SELECT ModuleName FROM Module WHERE PK_Module_ID = " + moduleID;
                //string ModuleName = Convert.ToString(NawaDAO.ExecuteScalar(queryStr, null, conn));

                string queryStr = "DELETE FROM " + this.moduleName + "_upload WHERE nawa_userid ='" + UserID + "'";
                NawaDAO.ExecuteNonQuery(queryStr, null, conn);

                if (dtImportData.Rows.Count > 0)
                {
                    DataColumn autouserid = new DataColumn("nawa_userid", typeof(string));
                    autouserid.DefaultValue = UserID;
                    autouserid.AllowDBNull = false;
                    dtImportData.Columns.Add(autouserid);

                    DataTableReader objreader = new DataTableReader(dtImportData);
                    using (DataTable objnewdt = new DataTable())
                    {
                        DataColumn auto = new DataColumn("nawa_recordnumber", typeof(long));
                        auto.AllowDBNull = false;
                        auto.AutoIncrement = true;
                        auto.AutoIncrementSeed = 1;
                        auto.AutoIncrementStep = 1;

                        objnewdt.Columns.Add(auto);

                        objnewdt.Load(objreader);

                        using (SqlBulkCopy sqlBulk = new SqlBulkCopy((SqlConnection)conn.conn, SqlBulkCopyOptions.Default, (SqlTransaction)conn.trans))
                        {
                            sqlBulk.BatchSize = 1000;
                            sqlBulk.BulkCopyTimeout = 3600;
                            sqlBulk.DestinationTableName = this.moduleName + "_upload";


                            Dictionary<string, string> mapColumns = MakeMappingColumns(objnewdt, moduleFieldShow.Where(item => item.FK_FieldType_ID != 14).ToList(), conn);
                            if (mapColumns != null)
                            {
                                foreach (KeyValuePair<string, string> mapping in mapColumns)
                                {
                                    sqlBulk.ColumnMappings.Add(mapping.Key, mapping.Value);
                                }
                            }

                            sqlBulk.WriteToServer(objnewdt);

                            //DetailUpload(moduleID, wb, UserID, conn);

                            SqlParameter[] oparam = new SqlParameter[]
                            {
                                new SqlParameter("@ModuleID", SqlDbType.Int) {DbType = DbType.Int32, Value = moduleID},
                                new SqlParameter("@UserID", SqlDbType.VarChar) {DbType = DbType.AnsiString, Value = UserID},
                                new SqlParameter("@IntMode", SqlDbType.Int) {DbType = DbType.Int32, Value = 0}
                            };

                            NawaDAO.ExecuteNonQuery("EXEC usp_trimTableUpload @ModuleID, @UserID, @IntMode", oparam, conn);
                            NawaDAO.ExecuteNonQuery("EXEC usp_ValidateUploadFormModule @ModuleID, @UserID, @IntMode", oparam, conn);
                            ValidateStoreProcedureBeforeupload(EModuleTime.AfterSave, EActionType.Import, oparam, moduleID, moduleFieldShow, conn);
                        }
                    }
                }


                conn.Commit();
                conn.Dispose();
            }
            catch (Exception ex)
            {
                conn.Rollback();
                throw ex;
            }
            return true;
        }

        public void CommitParameterUpload(int moduleID, string userID)
        {
            NawaConnection conn = NawaDAO.CreateConnection();
            conn.BeginTransaction();

            try
            {
                //string query = @"SELECT ModuleName FROM Module WHERE PK_Module_ID = @moduleID";
                //SqlParameter[] parameters = new SqlParameter[]
                //{
                //    new SqlParameter("@moduleID", SqlDbType.Int) {Value = moduleID}
                //};
                //string moduleName = Convert.ToString(NawaDAO.ExecuteScalar(query, parameters, conn));

                if (!string.IsNullOrEmpty(moduleName))
                {
                    DataTable auditTrailUpdateOld = new DataTable();
                    List<Dictionary<string, object>> auditTrailUpdateNew = new List<Dictionary<string, object>>();
                    List<Dictionary<string, object>> auditTrailInsert = new List<Dictionary<string, object>>();
                    List<Dictionary<string, object>> auditTrailDelete = new List<Dictionary<string, object>>();
                    string query = "SELECT * FROM " + moduleName + "_upload WHERE nawa_userid = @userID AND ISNULL(KeteranganError, '') = ''";
                    SqlParameter[] parameters = new SqlParameter[]
                    {
                        new SqlParameter("@userID", SqlDbType.VarChar) { Value = userID }
                    };
                    DataTable uploadData = NawaDAO.ExecuteTable(query, parameters, conn);

                    if (uploadData != null && uploadData.Rows.Count > 0)
                    {
                        //query = "SELECT * FROM ModuleField WHERE FK_Module_ID = @moduleID";
                        //parameters = new SqlParameter[]
                        //{
                        //    new SqlParameter("@moduleID", SqlDbType.Int) { Value = moduleID }
                        //};
                        //DataTable dtModuleField = NawaDAO.ExecuteTable(query, parameters, conn);

                        //if (dtModuleField != null && dtModuleField.Rows.Count > 0)
                        if (moduleFields.Count > 0)
                        {
                            List<ModuleField> listModuleField = moduleFields;
                            string primaryFieldName = listModuleField.Where(x => x.IsPrimaryKey).Select(x => x.FieldName).FirstOrDefault();

                            foreach (DataRow rowUpload in uploadData.Rows)
                            {
                                switch (Convert.ToString(rowUpload["nawa_Action"]))
                                {
                                    case "Update":
                                        query = "SELECT * FROM " + moduleName + " WHERE " + primaryFieldName + " = @ID";
                                        parameters = new SqlParameter[]
                                        {
                                            new SqlParameter("@ID", SqlDbType.Int) { Value = Convert.ToInt32(rowUpload[primaryFieldName]) }
                                        };
                                        DataRow rowOldData = NawaDAO.ExecuteRow(query, parameters, conn);

                                        if (rowOldData != null)
                                        {
                                            if (auditTrailUpdateOld.Rows.Count > 0)
                                            {
                                                auditTrailUpdateOld.Rows.Add(rowOldData.ItemArray);
                                            }
                                            else
                                            {
                                                auditTrailUpdateOld = rowOldData.Table;
                                            }
                                        }
                                        else
                                        {
                                            throw new Exception(moduleName + " data with ID = " + Convert.ToString(rowUpload[primaryFieldName]) + " was not found.");
                                        }

                                        Dictionary<string, object> updateNewDictionary = new Dictionary<string, object>();

                                        foreach (ModuleField field in listModuleField)
                                        {
                                            updateNewDictionary.Add(field.FieldName, rowUpload[field.FieldName]);
                                        }

                                        auditTrailUpdateNew.Add(updateNewDictionary);

                                        break;

                                    case "Delete":
                                        Dictionary<string, object> deleteDictionary = new Dictionary<string, object>();

                                        foreach (ModuleField field in listModuleField)
                                        {
                                            deleteDictionary.Add(field.FieldName, rowUpload[field.FieldName]);
                                        }

                                        auditTrailDelete.Add(deleteDictionary);

                                        break;

                                    case "Insert":
                                        Dictionary<string, object> insertDictionary = new Dictionary<string, object>();

                                        foreach (ModuleField field in listModuleField)
                                        {
                                            insertDictionary.Add(field.FieldName, rowUpload[field.FieldName]);
                                        }

                                        auditTrailInsert.Add(insertDictionary);

                                        break;

                                    default:
                                        break;
                                }
                            }
                        }
                    }

                    query = "EXEC usp_SaveUploadData @intmode, @moduleid, @userid";
                    parameters = new SqlParameter[]
                    {
                        new SqlParameter("@intmode", SqlDbType.Int) {Value = Convert.ToInt32(EUploadMode.Update)},
                        new SqlParameter("@moduleid", SqlDbType.Int) {Value = moduleID},
                        new SqlParameter("@userid", SqlDbType.VarChar) {Value = userID}
                    };

                    NawaDAO.ExecuteNonQuery(query, parameters, conn);

                    ValidateStoreProcedureAfterupload(EModuleTime.AfterSave, EActionType.Import, moduleID, userID, conn);

                    if (auditTrailUpdateNew.Count > 0)
                    {
                        DataTable dtAuditTrailUpdateNew = NawaDAO.ConvertListDictionary(auditTrailUpdateNew);
                        long headerID = AuditTrailBLL.InputAuditTrailHeader(conn, userID, moduleName, EActionType.Update, EAuditTrailStatus.AffectedDatabase, 0);

                        AuditTrailBLL.InputAuditTrailDetail(conn, headerID, EActionType.Update, dtAuditTrailUpdateNew, auditTrailUpdateOld);
                    }

                    if (auditTrailInsert.Count > 0)
                    {
                        List<Dictionary<string, object>> auditTrailInsertOld = new List<Dictionary<string, object>>();
                        long headerID = AuditTrailBLL.InputAuditTrailHeader(conn, userID, moduleName, EActionType.Insert, EAuditTrailStatus.AffectedDatabase, 0);

                        AuditTrailBLL.InputAuditTrailDetail(conn, headerID, EActionType.Insert, auditTrailInsert, auditTrailInsertOld);
                    }

                    if (auditTrailDelete.Count > 0)
                    {
                        List<Dictionary<string, object>> auditTrailDeleteNew = new List<Dictionary<string, object>>();
                        long headerID = AuditTrailBLL.InputAuditTrailHeader(conn, userID, moduleName, EActionType.Delete, EAuditTrailStatus.AffectedDatabase, 0);

                        AuditTrailBLL.InputAuditTrailDetail(conn, headerID, EActionType.Delete, auditTrailDeleteNew, auditTrailDelete);
                    }

                    query = "DELETE FROM " + moduleName + "_upload WHERE nawa_userid = '" + userID + "' AND ISNULL(KeteranganError, '') = ''";

                    NawaDAO.ExecuteNonQuery(query, null, conn);

                    conn.Commit();
                    conn.Dispose();
                }
                else
                {
                    throw new Exception("Module data was not found");
                }
            }
            catch (Exception ex)
            {
                conn.Rollback();
                conn.Dispose();

                throw ex;
            }
        }

        private void ValidateStoreProcedureAfterupload(EModuleTime intmoduleTime, EActionType intmoduleAction, int moduleID, string UserID, NawaConnection conn)
        {
            List<ModuleValidation> objshemaModuleValidation = new List<ModuleValidation>();

            Module objSchemaModule;

            string queryStr = @"SELECT * FROM Module WHERE PK_Module_ID = " + moduleID;
            DataRow drModule = NawaDAO.ExecuteRow(queryStr, null, conn);
            objSchemaModule = NawaDAO.ConvertDataTable<Module>(drModule);
            if (objSchemaModule != null)
            {
                if (objSchemaModule.IsUseDesigner)
                {
                    queryStr = @"SELECT * FROM ModuleValidation WHERE FK_Module_ID = " + moduleID;
                    DataTable drModuleValidation = NawaDAO.ExecuteTable(queryStr, null, conn);
                    objshemaModuleValidation = NawaDAO.ConvertDataTable<ModuleValidation>(drModuleValidation);
                }

                if (objSchemaModule.IsUseStoreProcedureValidation)
                {
                    foreach (ModuleValidation moduleValidation in objshemaModuleValidation)
                    {
                        if (moduleValidation.FK_ModuleTime_ID == Convert.ToInt32(intmoduleTime) && moduleValidation.FK_ModuleAction_ID == Convert.ToInt32(intmoduleAction))
                        {
                            SqlParameter[] parameters = new SqlParameter[] { new SqlParameter("@UserID", SqlDbType.VarChar) { Value = UserID } };
                            NawaDAO.ExecuteNonQuery(moduleValidation.StoreProcedureName, parameters, conn, CommandType.StoredProcedure);
                        }
                    }
                }
            }
        }

        private void ValidateStoreProcedureBeforeupload(EModuleTime intmoduleTime, EActionType intmoduleAction, SqlParameter[] Listsqlparam, int moduleID, List<ModuleField> moduleFields, NawaConnection conn)
        {
            List<ModuleValidation> objshemaModuleValidation = new List<ModuleValidation>();

            Module objSchemaModule;

            string queryStr = @"SELECT * FROM Module WHERE PK_Module_ID = " + moduleID;
            DataRow drModule = NawaDAO.ExecuteRow(queryStr, null, conn);
            objSchemaModule = NawaDAO.ConvertDataTable<Module>(drModule);
            if (objSchemaModule != null)
            {
                if (objSchemaModule.IsUseDesigner)
                {
                    queryStr = @"SELECT * FROM ModuleValidation WHERE FK_Module_ID = " + moduleID;
                    DataTable drModuleValidation = NawaDAO.ExecuteTable(queryStr, null, conn);
                    objshemaModuleValidation = NawaDAO.ConvertDataTable<ModuleValidation>(drModuleValidation);
                }

                if (objSchemaModule.IsUseStoreProcedureValidation)
                {
                    foreach (ModuleValidation moduleValidation in objshemaModuleValidation)
                    {
                        if (moduleValidation.FK_ModuleTime_ID == Convert.ToInt32(intmoduleTime) && moduleValidation.FK_ModuleAction_ID == Convert.ToInt32(intmoduleAction))
                        {
                            NawaDAO.ExecuteNonQuery(moduleValidation.StoreProcedureName, Listsqlparam, conn, CommandType.StoredProcedure);
                        }
                    }
                }
            }
        }

        private Dictionary<string, string> MakeMappingColumns(DataTable objdt, List<ModuleField> moduleFields, NawaConnection conn)
        {
            Dictionary<string, string> mappingColumns = new Dictionary<string, string>();

            foreach (DataColumn item in objdt.Columns)
            {
                if (item.ColumnName == "nawa_userid" || item.ColumnName == "nawa_recordnumber")
                    mappingColumns.Add(item.ColumnName, item.ColumnName);
                else if (item.ColumnName == "Action")
                    mappingColumns.Add(item.ColumnName, "nawa_Action");
                else
                {
                    ModuleField objresult = moduleFields.Where(x => x.FieldLabel == item.ColumnName).FirstOrDefault();
                    if (objresult != null)
                    {
                        mappingColumns.Add(item.ColumnName, objresult.FieldName);
                    }

                    string query = "SELECT * FROM ModuleFieldDefault";
                    DataTable dtModuleFieldDefaults = NawaDAO.ExecuteTable(query, null, conn);
                    List<ModuleFieldDefault> moduleFieldDefaults = NawaDAO.ConvertDataTable<ModuleFieldDefault>(dtModuleFieldDefaults);

                    ModuleFieldDefault objresultdefault = moduleFieldDefaults.Where(x => x.FieldLabel == item.ColumnName).FirstOrDefault();
                    if (objresultdefault != null)
                    {
                        mappingColumns.Add(item.ColumnName, objresultdefault.FieldName);
                    }
                }
            }

            return mappingColumns;
        }

        public void CustomSupportSP(CustomSupportSPRequest Input)
        {
            NawaConnection connection = NawaDAO.CreateConnection();

            System.Diagnostics.Debug.WriteLine(Input.Target);

            connection.BeginTransaction();

            try
            {
                string[] parameters = Input.Parameter.Replace(" ", "").Split(',');
                string[] parameterValues = Input.ParameterValue.Replace(" ", "").Split(',');

                if (parameters.Length > 0 && parameters.Length == parameterValues.Length)
                {

                    if (!String.IsNullOrEmpty(Input.Parameter))
                    {


                        //string query = "SELECT * FROM ModuleField WHERE FK_Module_ID = @ModuleID";
                        //SqlParameter[] sqlParameter = new SqlParameter[]
                        //{
                        //    new SqlParameter("@ModuleID", SqlDbType.Int) { Value = Input.ModuleID }
                        //};
                        //DataTable dtFields = NawaDAO.ExecuteTable(query, sqlParameter, connection);
                        //List<ModuleField> listModuleField = NawaDAO.ConvertDataTable<ModuleField>(dtFields);
                        List<ModuleField> listModuleField = moduleFields;
                        List<SqlParameter> listSqlParameter = new List<SqlParameter>();

                        for (int i = 0; i < parameters.Length; i++)
                        {

                            ModuleField moduleField = listModuleField.Find(x => x.Sequence == Convert.ToInt32(parameterValues[i]));

                            SqlParameter parameter = new SqlParameter(parameters[i], CommonBLL.GetObject(moduleField.FK_FieldType_ID, Input.Data[moduleField.FieldName]));

                            listSqlParameter.Add(parameter);

                        }



                        NawaDAO.ExecuteNonQuery(Input.Target, listSqlParameter.ToArray(), connection, CommandType.StoredProcedure);

                        connection.Commit();
                        connection.Dispose();
                    }
                }
                else
                {
                    throw new Exception("Sql Parameter list and sql Value list not match");
                }
            }
            catch (Exception ex)
            {
                connection.Rollback();
                connection.Dispose();
                throw ex;
            }
        }

        private static SqlParameter GetSqlParameter(string FieldName, int FieldType, object value)
        {
            return GetSqlParameterRaw(FieldName, FieldType, NawaDataBLL.CommonBLL.GetObject(FieldType, value));
        }

        private static SqlParameter GetSqlParameterRaw(string FieldName, int FieldType, object value)
        {
            SqlDbType dbType;
            object objValue = value;

            switch (FieldType)
            {
                case 1:
                    dbType = SqlDbType.BigInt;
                    break;
                case 2:
                    dbType = SqlDbType.Int;
                    break;
                case 3:
                    dbType = SqlDbType.SmallInt;
                    break;
                case 4:
                    dbType = SqlDbType.TinyInt;
                    break;
                case 5:
                    dbType = SqlDbType.Decimal;
                    break;
                case 6:
                    dbType = SqlDbType.Float;
                    break;
                case 7:
                    dbType = SqlDbType.Real;
                    break;
                case 8:
                    dbType = SqlDbType.Money;
                    break;
                case 9:
                    dbType = SqlDbType.VarChar;
                    break;
                case 10:
                    dbType = SqlDbType.DateTime;
                    break;
                case 11:
                    dbType = SqlDbType.VarChar; //WARNING
                    break;
                case 12:
                    dbType = SqlDbType.Int;
                    break;
                case 13:
                    dbType = SqlDbType.Bit;
                    break;
                case 14:
                    dbType = SqlDbType.VarBinary;
                    break;
                case 15:
                    dbType = SqlDbType.BigInt;
                    break;
                case 16:
                    dbType = SqlDbType.Int;
                    break;
                default:
                    dbType = SqlDbType.VarChar;
                    break;
            }

            return new SqlParameter("@" + FieldName, dbType) { Value = objValue };
        }

        private static string GetQueryRef(string strTable, string strfieldkey, string strfielddisplay, string stradditionaljoin, string strfilter, bool bcascade, string UserID, int PK_MUser_ID)
        {
            if (String.IsNullOrEmpty(stradditionaljoin)) stradditionaljoin = "";
            if (bcascade) stradditionaljoin = "";

            string query = "select " + strfieldkey + "," + strfielddisplay + " from " + strTable;
            if (String.IsNullOrEmpty(strfilter))
            {
                if (!String.IsNullOrEmpty(stradditionaljoin) && stradditionaljoin.Substring(0, stradditionaljoin.IndexOf(" ")).ToLower() == "and")
                {
                    strfilter = strfilter + stradditionaljoin.Substring(4, stradditionaljoin.Length - 4);
                }
            }
            else
            {
                strfilter = strfilter + stradditionaljoin;
            }

            strfilter = strfilter.Replace("@userid", UserID);
            strfilter = strfilter.Replace("@PK_MUser_ID", PK_MUser_ID.ToString());

            if (!String.IsNullOrEmpty(strfilter.Trim()))
            {
                query = query + " where " + strfilter;
            }

            return query;
        }

        private static string GetExcelColumnName(int columnNumber)
        {
            int dividend = columnNumber;
            string columnName = String.Empty;
            int modulo;

            while (dividend > 0)
            {
                modulo = (dividend - 1) % 26;
                columnName = Convert.ToChar(65 + modulo).ToString() + columnName;
                dividend = (int)((dividend - modulo) / 26);
            }

            return columnName;
        }
    }
}
