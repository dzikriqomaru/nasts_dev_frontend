import React, { Suspense, lazy, useState, useContext, useEffect } from "react";
import { Switch, Redirect, Route, useLocation } from "react-router-dom";

import { RouteWithLayout } from "@vikitheolorado/nawadata-web";
import { MainLayout, MinimalLayout } from "@vikitheolorado/nawadata-web";
import { UserProfileContext } from "@vikitheolorado/nawadata-web";

import { APIRequest } from "@vikitheolorado/nawadata-web";

import { Loader } from "@vikitheolorado/nawadata-web";

const LoginView = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.LoginView };
  })
);
const ChoseUserAlternateView = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.ChoseUserAlternateView };
  })
);
const ResetPasswordView = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.ResetPasswordView };
  })
);
const DashboardView = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.DashboardView };
  })
);
const RegisterUser = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.RegisterUserView };
  })
);
const DashboardSatuan = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.DashboardSatuan };
  })
);
const ParameterView = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.ParameterView };
  })
);
const ParameterInput = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.ParameterInput };
  })
);
const ParameterDetail = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.ParameterDetail };
  })
);
const ParameterUpload = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.ParameterUpload };
  })
);
const ParameterApproval = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.ParameterApproval };
  })
);
const ParameterApprovalDetail = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.ParameterApprovalDetail };
  })
);
const ParameterCorrection = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.ParameterCorrection };
  })
);
const ParameterCorrectionDetail = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.ParameterCorrectionDetail };
  })
);
const UserLockView = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.UserLockView };
  })
);
const UserLockEdit = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.UserLockEdit };
  })
);
const UserLockApproval = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.UserLockApproval };
  })
);
const UserLockApprovalDetail = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.UserLockApprovalDetail };
  })
);
const TypographyView = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.TypographyView };
  })
);
const IconsView = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.IconsView };
  })
);
const AccountView = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.AccountView };
  })
);
const SettingsView = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.SettingsView };
  })
);
const NotFoundView = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.NotFoundView };
  })
);
const ModuleView = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.ModuleView };
  })
);
const ModuleInputView = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.ModuleInputView };
  })
);
const ModuleDetailView = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.ModuleDetailView };
  })
);
const MenuAccessView = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.MenuAccessView };
  })
);
const MenuAccessEdit = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.MenuAccessEdit };
  })
);
const MenuAccessApproval = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.MenuAccessApproval };
  })
);
const MenuAccessApprovalDetail = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.MenuAccessApprovalDetail };
  })
);
const AuditTrailChangesView = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.AuditTrailChangesView };
  })
);
const AuditTrailChangesDetail = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.AuditTrailChangesDetail };
  })
);
const AuditTrailUserLogin = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.AuditTrailUserLogin };
  })
);
const AuditTrailUserAccess = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.AuditTrailUserAccess };
  })
);
const AuditTrailSystem = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.AuditTrailSystem };
  })
);
const AuditTrailSystemDetail = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.AuditTrailSystemDetail };
  })
);
const AppParamView = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.AppParamView };
  })
);
const AppParamInput = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.AppParamInput };
  })
);
const AppParamDetail = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.AppParamDetail };
  })
);
const AppParamApproval = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.AppParamApproval };
  })
);
const AppParamApprovalDetail = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.AppParamApprovalDetail };
  })
);
const ChartSettingView = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.ChartSettingView };
  })
);
const ChartSettingInput = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.ChartSettingInput };
  })
);
const MUserView = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.MUserView };
  })
);
const MUserApproval = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.MUserApproval };
  })
);
const UserApproval = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.UserApproval };
  })
);
const UserApprovalDetail = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.UserApprovalDetail };
  })
);
const MUserInput = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.MUserInput };
  })
);
const MUserDetail = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.MUserDetail };
  })
);
const MUserDetailApproval = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.MUserDetailApproval };
  })
);
const EmailTemplateView = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.EmailTemplateView };
  })
);
const EmailTemplateInput = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.EmailTemplateInput };
  })
);
const EmailTemplateDetail = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.EmailTemplateDetail };
  })
);
const SMSTemplateView = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.SMSTemplateView };
  })
);
const SMSTemplateInput = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.SMSTemplateInput };
  })
);
const SMSTemplateDetail = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.SMSTemplateDetail };
  })
);
const GenerateFileTemplateView = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.GenerateFileTemplateView };
  })
);
const GenerateFileTemplateInput = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.GenerateFileTemplateInput };
  })
);
const GenerateFileTemplateDetail = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.GenerateFileTemplateDetail };
  })
);
const TreeMenu = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.TreeMenu };
  })
);
const MenuTemplate = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.MenuTemplate };
  })
);
const MenuTemplateApproval = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.MenuTemplateApproval };
  })
);
const MenuTemplateApprovalDetail = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.MenuTemplateApprovalDetail };
  })
);
const TaskView = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.TaskView };
  })
);
const TaskViewInput = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.TaskViewInput };
  })
);
const TaskDetail = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.TaskDetail };
  })
);
const SchedulerView = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.SchedulerView };
  })
);
const TaskApproval = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.TaskApproval };
  })
);
const TaskApprovalDetail = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.TaskApprovalDetail };
  })
);
const SchedulerInput = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.SchedulerInput };
  })
);
const SchedulerDetail = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.SchedulerDetail };
  })
);
const SchedulerApproval = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.SchedulerApproval };
  })
);
const SchedulerApprovalDetail = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.SchedulerApprovalDetail };
  })
);
const SelfServiceReportingView = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.SelfServiceReportingView };
  })
);
const ReportToolDesigner = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.ReportToolDesigner };
  })
);
const ReportToolInput = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.ReportToolInput };
  })
);
const ReportQueryDetail = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.ReportQueryDetail };
  })
);
const ReportToolDelete = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.ReportToolDelete };
  })
);
const ReportToolEdit = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.ReportToolEdit };
  })
);
const SSRSViewer = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.SSRSViewer };
  })
);
const SSRSViewerDirect = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.SSRSViewerDirect };
  })
);
const SSRSViewerFrame = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.SSRSViewerFrame };
  })
);
const ParameterDetailMobile = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.ParameterDetailMobile };
  })
);
const RunProcess = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.RunProcess };
  })
);
const LogProcessView = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.LogProcessView };
  })
);
const EmailTemplateApproval = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.EmailTemplateApproval };
  })
);
const EmailTemplateApprovalDetail = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.EmailTemplateApprovalDetail };
  })
);
const ValidationSummary = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.ValidationSummary };
  })
);
const ValidationDetail = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.ValidationDetail };
  })
);
const ValidationProgress = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.ValidationProgress };
  })
);
const RunValidation = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.RunValidation };
  })
);
const DevExpressDesigner = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.DevExpressReportDesigner };
  })
);
const DevExpressView = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.DevExpressReportView };
  })
);
const DevExpressMenu = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.DevExpressMenuView };
  })
);
const NF5FileManager = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.NF5FileManager };
  })
);
const DevExpressDashboard = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.DevExpressDashboard };
  })
);
const AutoLogin = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.AutoLogin };
  })
);
const DevExpressDashboardMenu = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.DevExpressDashboardMenu };
  })
);
const DevExpressDashboardView = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.DevExpressDashboardView };
  })
);
const SchedulerDetailView = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.SchedulerDetailView };
  })
);
const SchedulerDetailEdit = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.SchedulerDetailEdit };
  })
);
const SendOTP = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.SendOTP };
  })
);
const VersionView = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.Version };
  })
);
const KubernetesView = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.KubernetesView };
  })
);
const KubernetesDetail = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.KubernetesDetail };
  })
);
const SomethingWrong = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.SomethingWrong };
  })
);
const ViewBuilder = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.ViewBuilder };
  })
);
const ViewBuilderView = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.ViewBuilderView };
  })
);
const CustomPageView = lazy(() => import("./views/CustomPage/ParameterView"));
const NASTSCustomInput = lazy(() => import("./views/CustomPage/NASTSCustomInput"));
const NASTSCustomView = lazy(() => import("./views/CustomPage/NASTSCustomView"));

const NASTSMonthSummary = lazy(() => import("./views/CustomPage/NASTSMonthSummary"));
const NASTSMonthlyBOD = lazy(() => import("./views/CustomPage/NASTSMonthlyBOD"));
const NASTSBreachMonthReport = lazy(() => import("./views/CustomPage/NASTSBreachMonthReport"));
const NASTSColumnReport = lazy(() => import("./views/CustomPage/NASTSColumnReport"));
const NASTSWeeklyReport = lazy(() => import("./views/CustomPage/NASTSWeeklyReport"));
const CustomCodeExample = lazy(() => import("./views/CustomPage/CustomCodeExample"));

const ViewBuilderInterpreter = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.ViewBuilderInterpreter };
  })
);
const ViewBuilderInterpreterDetail = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.ViewBuilderInterpreterDetail };
  })
);
const ViewBuilderInterpreterInput = lazy(() =>
  import("@vikitheolorado/nawadata-web").then((module) => {
    return { default: module.ViewBuilderInterpreterInput };
  })
);

const CustomPageInput = lazy(() => import("./views/CustomPage/ParameterInput"));
const CustomPageDetail = lazy(() =>
  import("./views/CustomPage/ParameterDetail")
);
const CustomPageApproval = lazy(() =>
  import("./views/CustomPage/ParameterApproval")
);
const CustomPageApprovalDetail = lazy(() =>
  import("./views/CustomPage/ParameterApprovalDetail")
);
const CustomPageUpload = lazy(() =>
  import("./views/CustomPage/ParameterUpload")
);

const TestingOnlyCheckOneRow = lazy(()=>
  import("./views/CustomPage/TestingOnlyCheckOneRow")
);

const loadRoute = (menus, arrRoute) => {
  menus.forEach((menu) => {
    if (menu.child) {
      loadRoute(menu.child, arrRoute);
    } else {
      if (!arrRoute.includes(menu.href)) {
        arrRoute.push(menu.href);
      }
    }
  });
};

const Routes = (props) => {
  const { resetPasswordURL, resetPasswordToken } = props;

  // const {TreeMenu} = React.useContext(UserProfileContext);
  // const [arrRoute, setArrRoute] = React.useState([]);

  // React.useEffect(() => {
  //   let newRoutes = [];
  //   loadRoute(TreeMenu, newRoutes);
  //   setArrRoute(newRoutes);
  // }, [TreeMenu]);

  const UserProfile = useContext(UserProfileContext);
  let location = useLocation();

  const postAccessAPI = (url, moduleID, title, actionID) => {
    APIRequest.Send(
      {
        method: "Post",
        url: "api/AuditTrail/PostUserAccessData",
        data: {
          Url: url,
          ModuleID: moduleID,
          MenuName: title,
          ActionType: actionID,
        },
      },
      UserProfile
    )
      .then((res) => {
        if (res.status !== 200) {
          console.log("Unable to audit user access");
        }
      })
      .catch((err) => {
        console.log("Unable to audit user access");
      });
  };

  useEffect(() => {
    let moduleID;
    let actionID;
    let title;

    if (location.pathname !== "/" && location.pathname !== "/login") {
      if (location.params) {
        let dictModuleID = location.params.find(
          (item) => item.key == "ModuleID"
        );
        let dictAction = location.params.find((item) => item.key == "ActionID");
        let dictTitle = location.params.find((item) => item.key == "Title");

        moduleID = dictModuleID ? dictModuleID.value : 0;
        actionID = dictAction ? dictAction.value : 0;
        title = dictTitle ? dictTitle.value : "";
      }

      if (location.state) {
        moduleID = location.state.moduleid;
        actionID = location.state.type;
        // title = location.params.find((item) => item.key == 'Title').value;
      }
      if (UserProfile.IsAuth) {
        postAccessAPI(location.pathname, moduleID, title, actionID);
      }
    }
  }, [location]);
  const generateState = (search) => {
    let state = {};
        let decodeSearch = decodeURI(search)

        // remove Question Mark, singel and double quotes
        let removedQuestionMark = decodeSearch.substring(1).replace(/"/g, '').replace(/'/g, '');
        let splitSearch = removedQuestionMark.split('&');
    
        for (let i = 0; i < splitSearch.length; i++) {
            let splitKeyValue = splitSearch[i].split("=")
                state[splitKeyValue[0]]=splitKeyValue[1]
        }
        return state
    }

  const onLoad = () => {
    if (resetPasswordURL && resetPasswordToken) {
      return (
        <Redirect
          exact
          from='/'
          to={{
            pathname: resetPasswordURL,
            state: { token: resetPasswordToken },
          }}
        />
      );
    } else if (window.location.pathname === "/AutoLogin") {
      return <Redirect exact from='/' to='/AutoLogin' />;
    } else if (window.location.pathname.toLowerCase() === "/version") {
      return <Redirect exact from='/' to='/Version' />;
    } else if (window.location.pathname && window.location.search){
        const state = generateState(window.location.search)
        console.log(state, window.location.pathname);
        return <Redirect 
            exact from='/' 
            to={{
                pathname: window.location.pathname,
                state : state
            }}
            />;
    } else {
      return <Redirect exact from='/' to='/Dashboard' />;
    }
  };


  return (
    <Suspense fallback={<Loader />}>
      <Switch>
        {onLoad()}
        <Route
          component={LoginView}
          exact
          path="/login"
          location="http://localhost:3000/"
        />
        <Route component={AutoLogin} path="/AutoLogin" />
        <Route component={ResetPasswordView} exact path="/ResetPassword" />
        <Route
          component={ChoseUserAlternateView}
          exact
          path="/ChoseUserAlternate"
        />
        <Route component={RegisterUser} exact path="/RegisterUser" />
        <Route component={SendOTP} exact path="/SendOTP" />
        <Route component={ViewBuilder} path="/ViewBuilder" />
        <Route component={VersionView} path="/Version" />
        {/* Application Parameter */}
        <RouteWithLayout
          component={AppParamView}
          exact
          layout={MainLayout}
          path="/AppParamView"
        />
        <RouteWithLayout
          component={AppParamInput}
          exact
          layout={MainLayout}
          path="/AppParamInput"
        />
        <RouteWithLayout
          component={AppParamDetail}
          exact
          layout={MainLayout}
          path="/AppParamDetail"
        />
        <RouteWithLayout
          component={AppParamApproval}
          exact
          layout={MainLayout}
          path='/AppParamApproval'
        />
        <RouteWithLayout
          component={AppParamApprovalDetail}
          exact
          layout={MainLayout}
          path='/AppParamApprovalDetail'
        />
        {/* END */}

        {/* User Management */}
        <RouteWithLayout
          component={MUserView}
          exact
          layout={MainLayout}
          path="/MUserView"
        />
        <RouteWithLayout
          component={MUserApproval}
          exact
          layout={MainLayout}
          path="/MUserApproval"
        />
        <RouteWithLayout
          component={UserApproval}
          exact
          layout={MainLayout}
          path="/UserApproval"
        />
        <RouteWithLayout
          component={UserApprovalDetail}
          exact
          layout={MainLayout}
          path="/UserApprovalDetail"
        />
        <RouteWithLayout
          component={MUserInput}
          exact
          layout={MainLayout}
          path="/MUserInput"
        />
        <RouteWithLayout
          component={MUserDetail}
          exact
          layout={MainLayout}
          path="/MUserDetail"
        />
        <RouteWithLayout
          component={MUserDetailApproval}
          exact
          layout={MainLayout}
          path="/MUserDetailApproval"
        />
        {/* END */}

        {/* Email Template */}
        <RouteWithLayout
          component={EmailTemplateView}
          exact
          layout={MainLayout}
          path="/EmailTemplateView"
        />
        <RouteWithLayout
          component={EmailTemplateInput}
          exact
          layout={MainLayout}
          path="/EmailTemplateInput"
        />
        <RouteWithLayout
          component={EmailTemplateDetail}
          exact
          layout={MainLayout}
          path="/EmailTemplateDetail"
        />
        <RouteWithLayout
          component={EmailTemplateApproval}
          exact
          layout={MainLayout}
          path="/EmailTemplateApproval"
        />
        <RouteWithLayout
          component={EmailTemplateApprovalDetail}
          exact
          layout={MainLayout}
          path="/EmailTemplateApprovalDetail"
        />
        <RouteWithLayout
          component={SchedulerDetailView}
          exact
          layout={MainLayout}
          path="/EmailTemplateSchedulerDetailView"
        />
        <RouteWithLayout
          component={SchedulerDetailEdit}
          exact
          layout={MainLayout}
          path="/EmailTemplateSchedulerDetailEdit"
        />
        {/* END */}

        {/* SMS Template */}
        <RouteWithLayout
          component={SMSTemplateView}
          exact
          layout={MainLayout}
          path="/SMSTemplateView"
        />
        <RouteWithLayout
          component={SMSTemplateInput}
          exact
          layout={MainLayout}
          path="/SMSTemplateInput"
        />
        <RouteWithLayout
          component={SMSTemplateDetail}
          exact
          layout={MainLayout}
          path="/SMSTemplateDetail"
        />
        {/* END */}

        {/* Generate File Template */}
        <RouteWithLayout
          component={GenerateFileTemplateView}
          exact
          layout={MainLayout}
          path="/GenerateFileTemplateView"
        />
        <RouteWithLayout
          component={GenerateFileTemplateInput}
          exact
          layout={MainLayout}
          path="/GenerateFileTemplateInput"
        />
        <RouteWithLayout
          component={GenerateFileTemplateDetail}
          exact
          layout={MainLayout}
          path="/GenerateFileTemplateDetail"
        />
        {/* END */}

        <RouteWithLayout
          component={ChartSettingView}
          exact
          layout={MainLayout}
          path="/ChartSettingView"
        />
        <RouteWithLayout
          component={ChartSettingInput}
          exact
          layout={MainLayout}
          path="/ChartSettingInput"
        />

        <RouteWithLayout
          component={DashboardView}
          exact
          layout={MainLayout}
          path="/dashboard"
        />
        <RouteWithLayout
          component={ParameterView}
          exact
          layout={MainLayout}
          path="/parameterview"
        />
        <RouteWithLayout
          component={ParameterInput}
          exact
          layout={MainLayout}
          path="/parameterinput"
        />
        <RouteWithLayout
          component={ParameterDetail}
          exact
          layout={MainLayout}
          path="/parameterdetail"
        />

        {/* MOBILE VIEW FOR PARAMETER DETAIL */}
        <RouteWithLayout
          component={ParameterDetailMobile}
          exact
          layout={MainLayout}
          path="/parameterDetailMobile"
        />

        {/* Module Designer */}
        <RouteWithLayout
          component={ParameterUpload}
          exact
          layout={MainLayout}
          path="/parameterupload"
        />
        <RouteWithLayout
          component={ParameterApproval}
          exact
          layout={MainLayout}
          path="/parameterapproval"
        />
        <RouteWithLayout
          component={ParameterApprovalDetail}
          exact
          layout={MainLayout}
          path="/parameterapprovaldetail"
        />
        <RouteWithLayout
          component={ParameterCorrection}
          exact
          layout={MainLayout}
          path="/ParameterCorrection"
        />
        <RouteWithLayout
          component={ParameterCorrectionDetail}
          exact
          layout={MainLayout}
          path="/ParameterCorrectionDetail"
        />
        <RouteWithLayout
          component={ModuleView}
          exact
          layout={MainLayout}
          path="/module"
        />
        <RouteWithLayout
          component={ModuleInputView}
          exact
          layout={MainLayout}
          path="/module-input"
        />
        <RouteWithLayout
          component={ModuleDetailView}
          exact
          layout={MainLayout}
          path="/module-detail"
        />
        {/* Module Designer END */}

        {/* Menu Access */}
        <RouteWithLayout
          component={MenuAccessView}
          exact
          layout={MainLayout}
          path="/menu-access"
        />
        <RouteWithLayout
          component={MenuAccessEdit}
          exact
          layout={MainLayout}
          path="/menu-access-edit"
        />
        <RouteWithLayout
          component={MenuAccessApproval}
          exact
          layout={MainLayout}
          path="/menu-access-approval"
        />
        <RouteWithLayout
          component={MenuAccessApprovalDetail}
          exact
          layout={MainLayout}
          path="/menu-access-approval-detail"
        />
        {/* <RouteWithLayout
                component={MenuAccessDetail}
                exact
                layout={MainLayout}
                path='/menu-access-detail'
            /> */}
        {/* Menu Access END */}

        {/* AUDIT TRAIL */}
        <RouteWithLayout
          component={AuditTrailChangesDetail}
          exact
          layout={MainLayout}
          path="/AuditTrailChangesDetail"
        />
        <RouteWithLayout
          component={AuditTrailChangesView}
          exact
          layout={MainLayout}
          path="/AuditTrailChanges"
        />
        <RouteWithLayout
          component={AuditTrailUserLogin}
          exact
          layout={MainLayout}
          path="/AuditTrailUserLogin"
        />
        <RouteWithLayout
          component={AuditTrailUserAccess}
          exact
          layout={MainLayout}
          path="/AuditTrailUserAccess"
        />
        <RouteWithLayout
          component={AuditTrailSystem}
          exact
          layout={MainLayout}
          path="/AuditTrailSystem"
        />
        <RouteWithLayout
          component={AuditTrailSystemDetail}
          exact
          layout={MainLayout}
          path="/AuditTrailSystemDetail"
        />
        {/* AUDIT TRAIL END */}

        {/* {EOD Task} */}
        <RouteWithLayout
          component={TaskView}
          exact
          layout={MainLayout}
          path="/TaskView"
        />
        <RouteWithLayout
          component={TaskViewInput}
          exact
          layout={MainLayout}
          path="/TaskViewInput"
        />
        <RouteWithLayout
          component={TaskDetail}
          exact
          layout={MainLayout}
          path="/TaskDetail"
        />
        <RouteWithLayout
          component={TaskApproval}
          exact
          layout={MainLayout}
          path="/TaskApproval"
        />
        <RouteWithLayout
          component={TaskApprovalDetail}
          exact
          layout={MainLayout}
          path="/TaskApprovalDetail"
        />
        {/* {END EOD Task} */}

        {/* {EOD SCHEDULER} */}
        <RouteWithLayout
          component={SchedulerView}
          exact
          layout={MainLayout}
          path="/SchedulerView"
        />

        <RouteWithLayout
          component={SchedulerInput}
          exact
          layout={MainLayout}
          path="/SchedulerInput"
        />

        <RouteWithLayout
          component={SchedulerApproval}
          exact
          layout={MainLayout}
          path="/SchedulerApproval"
        />
        <RouteWithLayout
          component={SchedulerApprovalDetail}
          exact
          layout={MainLayout}
          path="/SchedulerApprovalDetail"
        />

        <RouteWithLayout
          component={SchedulerDetail}
          exact
          layout={MainLayout}
          path="/SchedulerDetail"
        />
        {/* {END EOD SCHEDULER} */}

        {/* {PROCESS} */}
        <RouteWithLayout
          component={RunProcess}
          exact
          layout={MainLayout}
          path="/RunProcess"
        />
        {/* {END PROCESS} */}

        {/* {VALIDATION} */}
        <RouteWithLayout
          component={ValidationSummary}
          exact
          layout={MainLayout}
          path="/ValidationSummary"
        />

        <RouteWithLayout
          component={ValidationDetail}
          exact
          layout={MainLayout}
          path="/ValidationDetails"
        />
        {/* {END VALIDATION} */}

        {/* {REPORT QUERY} */}
        <RouteWithLayout
          component={ReportToolInput}
          exact
          layout={MainLayout}
          path="/ReportQueryAdd"
        />

        <RouteWithLayout
          component={ReportToolEdit}
          exact
          layout={MainLayout}
          path="/ReportQueryEdit"
        />

        <RouteWithLayout
          component={ReportToolDelete}
          exact
          layout={MainLayout}
          path="/ReportQueryDelete"
        />

        <RouteWithLayout
          component={ReportToolDesigner}
          exact
          layout={MainLayout}
          path="/ReportQueryView"
        />

        <RouteWithLayout
          component={DevExpressDesigner}
          exact
          layout={MainLayout}
          path="/DevExpressReportDesigner"
        />

        <RouteWithLayout
          component={DevExpressDashboard}
          exact
          layout={MainLayout}
          path="/DevExpressDashboard"
        />

        <RouteWithLayout
          component={DevExpressDashboardMenu}
          exact
          layout={MainLayout}
          path="/DevExpressDashboardMenu"
        />

        <RouteWithLayout
          component={DevExpressDashboardView}
          exact
          layout={MainLayout}
          path="/DevExpressDashboardView"
        />

        <RouteWithLayout
          component={DevExpressView}
          exact
          layout={MainLayout}
          path="/DevExpressReportView"
        />

        <RouteWithLayout
          component={DevExpressMenu}
          exact
          layout={MainLayout}
          path="/DevExpressMenuView"
        />

        <RouteWithLayout
          component={SSRSViewer}
          exact
          layout={MainLayout}
          path="/SSRSViewer"
        />

        <RouteWithLayout
          component={SSRSViewerDirect}
          exact
          layout={MainLayout}
          path="/SSRSViewerDirect"
        />

        <RouteWithLayout
          component={SSRSViewerFrame}
          exact
          layout={MainLayout}
          path="/SSRSViewerFrame"
        />

        {/* <RouteWithLayout
                component={ReportToolDesigner}
                exact
                layout={MainLayout}
                path='/ReportQueryAdd'
            /> */}
        {/* {END REPORT QUERY} */}

        <RouteWithLayout
          component={UserLockView}
          exact
          layout={MainLayout}
          path="/user-lock"
        />
        <RouteWithLayout
          component={UserLockEdit}
          exact
          layout={MainLayout}
          path="/user-lock-edit"
        />
        <RouteWithLayout
          component={UserLockApproval}
          exact
          layout={MainLayout}
          path="/user-lock-approval"
        />
        <RouteWithLayout
          component={UserLockApprovalDetail}
          exact
          layout={MainLayout}
          path="/user-lock-approval-detail"
        />
        <RouteWithLayout
          component={TypographyView}
          exact
          layout={MainLayout}
          path="/typography"
        />
        <RouteWithLayout
          component={IconsView}
          exact
          layout={MainLayout}
          path="/icons"
        />
        <RouteWithLayout
          component={AccountView}
          exact
          layout={MainLayout}
          path="/account"
        />
        <RouteWithLayout
          component={SettingsView}
          exact
          layout={MainLayout}
          path="/settings"
        />
        <RouteWithLayout
          component={TreeMenu}
          exact
          layout={MainLayout}
          path="/TreeMenu"
        />
        <RouteWithLayout
          component={MenuTemplate}
          exact
          layout={MainLayout}
          path="/MenuTemplateView"
        />
        <RouteWithLayout
          component={MenuTemplateApproval}
          exact
          layout={MainLayout}
          path="/menu-template-approval"
        />
        <RouteWithLayout
          component={MenuTemplateApprovalDetail}
          exact
          layout={MainLayout}
          path="/menu-template-approval-detail"
        />
        <RouteWithLayout
          component={SelfServiceReportingView}
          exact
          layout={MainLayout}
          path="/SelfServiceReportingView"
        />
        <RouteWithLayout
          component={ReportQueryDetail}
          exact
          layout={MainLayout}
          path="/ReportQueryDetail"
        />
        <RouteWithLayout
          component={LogProcessView}
          exact
          layout={MainLayout}
          path="/LogProcessView"
        />
        <RouteWithLayout
          component={ValidationProgress}
          exact
          layout={MainLayout}
          path="/ValidationProgress"
        />
        <RouteWithLayout
          component={NASTSCustomView}
          exact
          layout={MainLayout}
          path="/NASTSView"
        />
        <RouteWithLayout
          component={NASTSCustomInput}
          exact
          layout={MainLayout}
          path="/NASTSInput"
        />
        <RouteWithLayout
          component={NASTSMonthSummary}
          exact
          layout={MainLayout}
          path="/NASTSMonthSummary"
        />
        <RouteWithLayout
          component={NASTSMonthlyBOD}
          exact
          layout={MainLayout}
          path="/NASTSMonthlyBOD"
        />
        <RouteWithLayout
          component={NASTSBreachMonthReport}
          exact
          layout={MainLayout}
          path="/NASTSBreachMonthReport"
        />
        <RouteWithLayout
          component={NASTSColumnReport}
          exact
          layout={MainLayout}
          path="/NASTSColumnReport"
        />
        <RouteWithLayout
          component={NASTSWeeklyReport}
          exact
          layout={MainLayout}
          path="/NASTSWeeklyReport"
        />
        <RouteWithLayout
          component={CustomPageView}
          exact
          layout={MainLayout}
          path="/CustomPageView"
        />
        <RouteWithLayout
          component={CustomPageInput}
          exact
          layout={MainLayout}
          path="/CustomPageInput"
        />
        <RouteWithLayout
          component={CustomPageDetail}
          exact
          layout={MainLayout}
          path="/CustomPageDetail"
        />
        <RouteWithLayout
          component={CustomPageApproval}
          exact
          layout={MainLayout}
          path="/CustomPageApproval"
        />
        <RouteWithLayout
          component={CustomPageApprovalDetail}
          exact
          layout={MainLayout}
          path="/CustomPageApprovalDetail"
        />
        <RouteWithLayout
          component={CustomPageUpload}
          exact
          layout={MainLayout}
          path="/CustomPageUpload"
        />
        <RouteWithLayout
          exact
          component={NotFoundView}
          layout={MinimalLayout}
          path="/NotFound"
        />
        <RouteWithLayout
          exact
          component={SomethingWrong}
          layout={MinimalLayout}
          path="/SomethingWrong"
        />
        <Route component={CustomCodeExample} exact path="/CustomCodeExample" />

        {/* Run Validation */}
        <RouteWithLayout
          exact
          component={RunValidation}
          layout={MainLayout}
          path="/RunValidation"
        />
        {/* <RouteWithLayout
                exact
                component={NF5FileManager}
                layout={MainLayout}
                path='/filemanager'
            /> */}
        <RouteWithLayout
          exact
          component={DashboardSatuan}
          layout={MainLayout}
          path="/DashboardSatuan"
        />
        {/* Kubernetes */}
        <RouteWithLayout
          exact
          component={KubernetesView}
          layout={MainLayout}
          path="/KubernetesView"
        />
        <RouteWithLayout
          exact
          component={KubernetesDetail}
          layout={MainLayout}
          path="/KubernetesDetail"
        />
        {/* End Kubernetes */}
        {/* View Builder */}
        <RouteWithLayout
          exact
          component={ViewBuilderView}
          layout={MainLayout}
          path="/ViewBuilderView"
        />
        <Route component={ViewBuilder} path="/ViewBuilder" />
        <RouteWithLayout
          exact
          component={ViewBuilderInterpreter} // set to ViewBuilderInterpreterDetail for testing
          layout={MainLayout}
          path="/ViewBuilderInterpreter"
        />
        <RouteWithLayout
          exact
          component={ViewBuilderInterpreterDetail}
          layout={MainLayout}
          path="/DetailBuilderInterpreter"
        />
        <RouteWithLayout
          exact
          component={ViewBuilderInterpreterInput}
          layout={MainLayout}
          path="/InputBuilderInterpreter"
        />
        <RouteWithLayout
        exact
        component={TestingOnlyCheckOneRow}
        layout={MainLayout}
        path="/testingOnlyCheckOneRowView"
        />

        {/* End View Builder */}
        <Redirect to="/NotFound" />
      </Switch>
    </Suspense>
  );
};

export default Routes;
