import React, { useState, useEffect, useContext, memo } from 'react';
import useMediaQuery from '@material-ui/core/useMediaQuery';

import {
    APIRequest,
    EActionType,
    EDataType,
    EInputType,
    UserProfileContext, useInfoPanel,
    Infopanel,
    UnsavedChanges,
    useDateFormatList,
    useTimeFormat,
    NawaTable,

} from '@vikitheolorado/nawadata-web';
import FileSaver from 'file-saver';
import { Accordion, AccordionSummary, Button, Drawer, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ParameterDetailInput from './NASTSCustomDetailInput';

const useStyles = makeStyles((theme) => ({
    drawerPaper: {
        width: '100%'
    },
    buttonPos: {
        borderBottom: '1px solid rgba(0, 0, 0, 0.1)',
        paddingBottom: '5%',
        paddingTop: '5%',
        display: 'flex',
        justifyContent: 'flex-end'
    }
}));

export default function ParameterDetailView(props) {
    const classes = useStyles();
    const UserProfile = useContext(UserProfileContext);
    const { moduleDetailID, moduleName, parentID, approvalID, actionType, classContainer, toggleSend, sendEditedData, parentData, setIsDataChanged, detailType, setIsStartDev, setIsEndDev, sendDevDate, isClosed } = props;

    const [moduleDetailName, setModuleDetailName] = useState('');
    const [columns, setColumns] = useState([]);
    const [columnKey, setColumnKey] = useState("");
    const [parentColumn, setParentColumn] = useState("");
    const [parentDisplayField, setparentDisplayField] = useState('');
    const [referenceColumns, setReferenceColumns] = useState([]);
    const [multiCheckboxColumns, setMultiCheckboxColumns] = useState([]);
    const [drawerVariant, setDrawerVariant] = useState('permanent');
    const [rowCount, setRowCount] = useState(0);

    const [rows, setRows] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    const [open, setOpen] = useState(false);
    const [inputActionType, setInputActionType] = useState(EActionType.Insert);
    const [inputColumns, setInputColumns] = useState([]);
    const [redirect, setRedirect] = useState(null);
    const [dataToShow, setDataToShow] = useState({});
    const [openDrawer, setOpenDrawer] = useState(false);
    const [isSLAorLog, setIsSLAorLog] = useState(false);
    const [closedAssignment, setClosedAssignment] = useState(false);
    const [changedData, setChangedData] = useState({
        ModuleDetailID: moduleDetailID,
        data: {
            added: [],
            deleted: [],
            edited: []
        }
    });

    const detailActionList = moduleDetailID == 25691 ? [{
        text: "Detail",
        // url: '/ParameterDetail',
        type: EActionType.Detail
    }] : [{
        text: "Edit",
        // url: '/ParameterInput',
        type: EActionType.Update
    }, {
        text: "Detail",
        // url: '/ParameterDetail',
        type: EActionType.Detail
    }];
    const [emptyData, setEmptyData] = useState([]);

    const isMobile = useMediaQuery('(max-width:600px)');
    const tabletMin = useMediaQuery('(min-width:601px)');
    const tabletMax = useMediaQuery('(max-width:1024px)');
    const isTablet = tabletMin && tabletMax;

    const { showInfoPanel } = useInfoPanel();

    const exportTypesList = [{
        Label: 'Excel',
        Extension: '.xlsx'
    }, {
        Label: 'CSV',
        Extension: '.csv'
    }, {
        Label: 'Text',
        Extension: '.txt'
    }];

    const [delimiter, setDelimiter] = useState(",");

    const getModuleDetailView = (page, orderBy, order, search, filter, isAdvanced) => {
        setIsLoading(true);
        APIRequest.Send({
            method: 'GET',
            url: 'api/parameter/GetModuleDetailDataView',
            params: {
                ModuleDetailID: moduleDetailID,
                id: parentID,
                page: page,
                orderBy: orderBy,
                order: order,
                search: search,
                filter: filter,
                isAdv: isAdvanced,
                detailType: detailType,
                actionType: actionType
            }
        }, UserProfile).then((resDetail) => {
            if (resDetail.status === 200 && resDetail.data) {
                setRowCount(resDetail.data.count);
                setRows(resDetail.data.rows.map((valueRow) => valueRow.Dictionary));
                setIsLoading(false);
            }
            setIsLoading(false);
        }).catch((err) => {
            showInfoPanel.error(err.response ? err.response.data : err);
            setIsLoading(false);
        });
    };

    const getDelimiter = () => {
        APIRequest.Send({
            url: 'api/SystemParameter/getSysParam',
            method: 'GET',
            params: {
                SysParamID: 39
            }
        }, UserProfile).then((res) => {
            setDelimiter(res.data.SettingValue);
        }).catch((err) => {
            showInfoPanel.error(err.response ? err.response.data : err);
        });
    };

    React.useEffect(() => {
        const ABORTCONTROLLER = new AbortController();
        if (moduleDetailID == 25682 || moduleDetailID == 25686 || moduleDetailID == 25690) {
            setIsSLAorLog(true)
        }
        if (moduleDetailID == 25683 && isClosed) {
            setClosedAssignment(true)
        }

        getDelimiter();

        APIRequest.Send({
            method: "GET",
            url: `api/parameter/GetModuleDetailDetail?ModuleDetailID=${moduleDetailID}`
        }, UserProfile).then((res) => {
            if (res.status === 200) {
                if (res.data) {
                    setModuleDetailName(res.data.ModuleDetailLabel);
                    let _emptyData = {};
                    let newColumns = res.data.ModuleDetailFields.map((value) => {
                        if (value.IsPrimaryKey) {
                            setColumnKey(value.FieldName);
                        }
                        if (value.FK_FieldType_ID === EDataType.ParentReference) {
                            setParentColumn(value.FieldName);
                        } else if (value.FK_FieldType_ID === EDataType.ReferenceTable && value.FK_ExtType_ID !== EInputType.PopUpMultiCheckBox) {
                            let tempRef = [...referenceColumns];
                            tempRef.push(value.FieldName);
                            setReferenceColumns(tempRef);
                        } else if (value.FK_ExtType_ID === EInputType.PopUpMultiCheckBox) {
                            let _tempRef = [...multiCheckboxColumns];
                            _tempRef.push(value.FieldName);
                            setMultiCheckboxColumns(_tempRef);
                        }
                        if (value.FK_ExtType_ID == EInputType.FileUpload) {
                            _emptyData[value.FieldName] = {
                                fileName: "",
                                fileData: ""
                            };
                        } else {
                            _emptyData[value.FieldName] = "";
                        }
                        return {
                            id: value.FieldName,
                            label: value.FieldLabel,
                            key: value.IsPrimaryKey,
                            isShow: value.IsShowInView,
                            fieldType: value.FK_FieldType_ID,
                            inputType: value.FK_ExtType_ID == 1 ? EInputType.DateTimeField : value.FK_ExtType_ID,
                            tabelReferenceName: value.TabelReferenceName,
                            tableReferenceFieldKey: value.TableReferenceFieldKey,
                            tableReferenceFieldDisplayName: value.TableReferenceFieldDisplayName,
                            sequence: value.Sequence,
                            PK_ModuleField_ID: value.PK_ModuleDetailField_ID
                        };
                    });
                    setEmptyData(_emptyData);
                    setColumns(newColumns);
                    setInputColumns(res.data.ModuleDetailFields);

                    if (approvalID) {
                        APIRequest.Send({
                            method: "GET",
                            url: `api/parameter/GetModuleDetailDataViewApproval`,
                            params: {
                                ModuleDetailID: moduleDetailID,
                                ModuleApprovalID: approvalID
                            }
                        }, UserProfile).then((resDetail) => {
                            if (resDetail.status === 200 && resDetail.data) {
                                setRowCount(resDetail.data.count);
                                setRows(resDetail.data.rows.map((valueRow) => {
                                    let dict = valueRow.Dictionary;
                                    dict["IsDBData"] = true;
                                    return dict;
                                }));
                                isDeveloperMenu(resDetail.data.rows)
                            }
                        }).catch((err) => {
                            showInfoPanel.error(err.response ? err.response.data : err);
                        });

                        return () => {
                            ABORTCONTROLLER.abort();
                        };
                    } else {
                        if (actionType !== EActionType.Insert) {
                            APIRequest.Send({
                                method: "GET",
                                url: `api/parameter/GetModuleDetailDataView`,
                                params: {
                                    ModuleDetailID: moduleDetailID,
                                    id: parentID,
                                    detailType: detailType,
                                    actionType: actionType
                                    // page: 1
                                }
                            }, UserProfile).then((resDetail) => {
                                if (resDetail.status === 200 && resDetail.data) {
                                    setRowCount(resDetail.data.count);
                                    setRows(resDetail.data.rows.map((valueRow) => valueRow.Dictionary));
                                    isDeveloperMenu(resDetail.data.rows)
                                }
                            }).catch((err) => {
                                showInfoPanel.error(err.response ? err.response.data : err);
                            });

                            return () => {
                                ABORTCONTROLLER.abort();
                            };
                        }
                    }
                }
            } else {
                showInfoPanel.error("Invalid Credential");
            }
        }).catch((err) => {
            showInfoPanel.error(err.response ? err.response.data : err);
        });
        ABORTCONTROLLER.abort();
    }, [UserProfile, isClosed]);

    const cleanForDetail = (string) => {
        if (parseInt(string)) {
            return string;
        } else {
            return String(string).split('-')[0].split('(')[1].split(')')[0];
        }
    };

    const getKeyFromMultiCheckBox = new Promise((resolve, reject) => {
        try {
            if (changedData && changedData.data) {
                if (changedData.data.added) {
                    changedData.data.added.forEach((item) => {
                        multiCheckboxColumns.forEach((column) => {
                            if (item[column]) {
                                let str = '';
                                let value = item[column];
                                value = value.split(delimiter);
                                value.forEach((item, index) => {
                                    if (index == 0) {
                                        str += cleanForDetail(item);
                                    } else {
                                        str += `${delimiter}${cleanForDetail(item)}`;
                                    }
                                });
                                item[column] = str;
                            }
                        });
                    });
                }

                if (changedData.data.deleted) {
                    changedData.data.deleted.forEach((item) => {
                        multiCheckboxColumns.forEach((column) => {
                            if (item[column]) {
                                let str = '';
                                let value = item[column];
                                value = value.split(delimiter);
                                value.forEach((item, index) => {
                                    if (index == 0) {
                                        str += cleanForDetail(item);
                                    } else {
                                        str += `${delimiter}${cleanForDetail(item)}`;
                                    }
                                });
                                item[column] = str;
                            }
                        });
                    });
                }

                if (changedData.data.edited) {
                    changedData.data.edited.forEach((item) => {
                        multiCheckboxColumns.forEach((column) => {
                            if (item[column]) {
                                let str = '';
                                let value = item[column];
                                value = value.split(delimiter);
                                value.forEach((item, index) => {
                                    if (index == 0) {
                                        str += cleanForDetail(item);
                                    } else {
                                        str += `${delimiter}${cleanForDetail(item)}`;
                                    }
                                });
                                item[column] = str;
                            }
                        });
                    });
                }
            }
            resolve();
        } catch (e) {
            reject();
        }
    });


    React.useEffect(() => {
        const ABORTCONTROLLER = new AbortController();

        getKeyFromMultiCheckBox.then(() => {
            sendEditedData(changedData);
        });

        return () => {
            ABORTCONTROLLER.abort();
        };
    }, [toggleSend]);

    const onClickAction = (sender, record) => {
        // referenceColumns.forEach((refColumn) => {
        //   let refValue = record[refColumn];
        //   record[refColumn] = refValue.substring(refValue.indexOf("(") + 1, refValue.indexOf(")"));
        // })
        setDataToShow(formatData(record));
        setInputActionType(sender.type);
        setOpen(true);
    };

    const onClickAdd = () => {
        if (actionType == EActionType.Insert || actionType == EActionType.Update) {
            let rawInput = {};
            Object.keys(emptyData).forEach((item) => {
                let defaultValue = inputColumns.find((value) => {
                    return value.FieldName === item;
                }).DefaultValue;
                rawInput[item] = defaultValue ? defaultValue : '';
            });
            setDataToShow(rawInput);
            setInputActionType(EActionType.Insert);
            setOpen(true);
        }
    };

    const getRandomID = () => {
        let randomID = Math.floor(Math.random() * (-99999 + 90000) - 90000);
        if (rows.find((e) => {
            return e[columnKey] == randomID;
        }) == undefined) {
            return String(randomID);
        } else {
            getRandomID();
        }
    };

    const formatData = (editData) => {
        let newData = {};
        Object.keys(emptyData).forEach((key) => {
            if (inputColumns.find((value) => value.FieldName === key).FK_ExtType_ID == EInputType.DateField) {
                newData[key] = editData[key] ? new Date(editData[key]) : '';
            } else if (inputColumns.find((value) => value.FieldName === key).FK_FieldType_ID == EDataType.BOOLEAN) {
                newData[key] = typeof editData[key] == "boolean" ? editData[key] == 1 ? 'true' : 'false' : '';
            } else if (inputColumns.find((value) => value.FieldName === key).FK_FieldType_ID == EDataType.VARBINARY) {
                newData[key] = {
                    fileName: editData[key + 'Name'],
                    fileData: editData[key],
                    fieldName: key,
                    id: editData[columnKey],
                    moduleid: moduleDetailID
                };
            } else if (inputColumns.find((value) => value.FieldName === key).FK_ExtType_ID == EInputType.NumberField) {
                let hasHypen = false;
                let b = String(editData[key]).split('.');
                if (b[1]) {
                    b[1] = b[1].replaceAll('-', '');
                }
                let d = b[0].split('');
                if (d[0] === '-') {
                    hasHypen = true;
                }
                b[0] = b[0].replaceAll('-', '');
                let c = '';
                if (UserProfile.DecimalSeparator === ',') {
                    b[0] = b[0].replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                    c = b.join(',');
                } else {
                    b[0] = b[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                    c = b.join('.');
                }
                hasHypen ? newData[key] = '-' + c : newData[key] = c;
            } else {
                newData[key] = editData[key];
            }
            if ((newData[key] == '' || newData[key] == null) && actionType == EActionType.Insert) {
                newData[key] = inputColumns.find((value) => value.FieldName === key).DefaultValue;
            }
        });
        return newData;
    };


    const _onAdd = (value) => {
        setOpen(false);
        value[columnKey] = getRandomID();
        value[parentColumn] = String(parentID);

        let valueForRows = { ...value };
        let tempData = [...rows];
        tempData.push(valueForRows);
        setRows(tempData);

        referenceColumns.forEach((refColumn) => {
            let refValue = value[refColumn];
            if (refValue && refValue.includes('(')) {
                value[refColumn] = refValue.substring(refValue.indexOf("(") + 1, refValue.indexOf(")"));
            } else if (refValue) {
                value[refColumn] = refValue;
            } else {
                value[refColumn] = "";
            }
        });

        let tempAddedData = { ...changedData };
        tempAddedData.data.added.push(value);
        setChangedData(tempAddedData);
        setIsDataChanged(true);
    };

    const _onEdit = (value) => {
        setOpen(false);
        let valueForRows = { ...value };
        let editedIndex = rows.findIndex((e) => e[columnKey] == valueForRows[columnKey]);

        referenceColumns.forEach((refColumn) => {
            let refValue = value[refColumn];
            if (refValue && refValue.includes('(')) {
                value[refColumn] = refValue.substring(refValue.indexOf("(") + 1, refValue.indexOf(")"));
            } else if (refValue) {
                value[refColumn] = refValue;
            } else {
                value[refColumn] = "";
            }
        });

        // Negative value key mean its an added data
        if (Number(value[columnKey]) < 0 && !rows[editedIndex].IsDBData) {
            let editedIndexAdded = changedData.data.added.findIndex((e) => e[columnKey] == value[columnKey]);

            let tempAddedData = { ...changedData };
            if (editedIndexAdded != undefined) {
                tempAddedData.data.added.splice(editedIndexAdded, 1, value);
                setChangedData(tempAddedData);
            } else {
                tempAddedData.data.added.push(value);
                setChangedData(tempAddedData);
            }
        } else {
            let editedIndexEdited = changedData.data.edited.findIndex((e) => e[columnKey] == value[columnKey]);

            let tempEditedData = { ...changedData };
            if (editedIndexEdited != undefined && editedIndexEdited >= 0) {
                tempEditedData.data.edited.splice(editedIndexEdited, 1, value);
                setChangedData(tempEditedData);
            } else {
                tempEditedData.data.edited.push(value);
                setChangedData(tempEditedData);
            }
        }
        let tempData = [...rows];
        tempData.splice(editedIndex, 1, valueForRows);
        setRows(tempData);
        setIsDataChanged(true);
    };

    const _onDelete = (value) => {
        setOpen(false);
        let deletedIndex = rows.findIndex((e) => e[columnKey] == value[columnKey]);

        referenceColumns.forEach((refColumn) => {
            let refValue = value[refColumn];
            if (refValue && refValue.includes('(')) {
                value[refColumn] = refValue.substring(refValue.indexOf("(") + 1, refValue.indexOf(")"));
            } else if (refValue) {
                value[refColumn] = refValue;
            } else {
                value[refColumn] = "";
            }
        });

        // Negative value key mean its an added data
        if (Number(value[columnKey]) < 0 && !rows[deletedIndex].IsDBData) {
            let deletedIndexAdded = changedData.data.added.findIndex((e) => e[columnKey] == value[columnKey]);

            let tempAddedData = { ...changedData };
            tempAddedData.data.added.splice(deletedIndexAdded, 1);
            setChangedData(tempAddedData);
        } else {
            let _deletedIndexAdded = changedData.data.edited.findIndex((e) => e[columnKey] == value[columnKey]);

            let tempEditedData = { ...changedData };
            if (_deletedIndexAdded != undefined && _deletedIndexAdded >= 0) {
                tempEditedData.data.edited.splice(_deletedIndexAdded, 1);
            }
            tempEditedData.data.deleted.push(value);
            setChangedData(tempEditedData);
        }

        let tempData = [...rows];
        tempData.splice(deletedIndex, 1);
        setRows(tempData);
        setIsDataChanged(true);
    };

    const handleMobileDetail = () => {
        setOpenDrawer(!openDrawer);
    };

    const onClickDownloadCurrentPage = (page, orderBy, order, search, filter, isUsingAdv, exportType, fields, name) => {
        if (moduleDetailID) {
            APIRequest.Send({
                url: 'api/parameter/ExportModuleDetailData',
                method: 'POST',
                responseType: 'blob',
                data: {
                    moduleDetailID: moduleDetailID,
                    page: page,
                    orderBy: orderBy,
                    order: order,
                    search: search,
                    filter: filter,
                    isAdv: isUsingAdv,
                    exportAs: exportType ? exportType.Label : '',
                    fields: fields,
                    parentID: parentID
                }
            }, UserProfile).then((res) => {
                if (res.status === 200) {
                    if (res.data) {
                        if (exportType && exportType.Extension) {
                            FileSaver.saveAs(res.data, moduleDetailName + exportType.Extension);
                        } else {
                            FileSaver.saveAs(res.data, moduleDetailName + '.xlsx');
                        }
                    }
                }
            }).catch((err) => {
                showInfoPanel.error(err.response ? err.response.data : err);
            });
        }
    };

    const onClickDownloadAll = (orderBy, order, search, filter, isUsingAdv, exportType, fields) => {
        if (moduleDetailID) {
            APIRequest.Send({
                url: 'api/parameter/ExportModuleDetailDataAll',
                method: 'POST',
                responseType: 'blob',
                data: {
                    moduleDetailID: moduleDetailID,
                    orderBy: orderBy,
                    order: order,
                    search: search,
                    filter: filter,
                    isAdv: isUsingAdv,
                    exportAs: exportType ? exportType.Label : '',
                    fields: fields,
                    parentID: parentID
                }
            }, UserProfile).then((res) => {
                if (res.status === 200) {
                    if (res.data) {
                        if (exportType && exportType.Extension) {
                            FileSaver.saveAs(res.data, moduleDetailName + exportType.Extension);
                        } else {
                            FileSaver.saveAs(res.data, moduleDetailName + '.xlsx');
                        }
                    }
                }
            }).catch((err) => {
                showInfoPanel.error(err.response ? err.response.data : err);
            });
        }
    };

    const onClickDownloadSelected = (selected, orderBy, order, exportType, fields) => {
        if (moduleDetailID) {
            APIRequest.Send({
                url: 'api/parameter/ExportModuleDetailDataSelected',
                method: 'POST',
                data: {
                    moduleDetailID: moduleDetailID,
                    SelectedData: selected,
                    orderBy: orderBy,
                    order: order,
                    exportAs: exportType ? exportType.Label : '',
                    fields: fields,
                    parentID: parentID
                },
                responseType: 'blob'
            }, UserProfile).then((res) => {
                if (res.status === 200) {
                    if (res.data) {
                        if (exportType && exportType.Extension) {
                            FileSaver.saveAs(res.data, moduleDetailName + exportType.Extension);
                        } else {
                            FileSaver.saveAs(res.data, moduleDetailName + '.xlsx');
                        }
                    }
                }
            }).catch((err) => {
                showInfoPanel.error(err.response ? err.response.data : err);
            });
        }
    };

    // Get the day of the week

    const isDeveloperMenu = (datarow) => {
        const temp = {
            ModuleDetailID: moduleDetailID,
            data: {
                added: [],
                deleted: [],
                edited: []
            }
        };
        if (moduleDetailID == 25683) {
            setIsStartDev(false)
            setIsEndDev(false)
            datarow.some(row => {
                const match = row.Dictionary.FK_MUser_ID.match(/\((\d+)\)/);
                const { StartDateTime, EndDateTime } = row.Dictionary;
                const datetemp = new Date;
                if (match && match[1] == UserProfile.PK_MUser_ID) {
                    if (!StartDateTime && !EndDateTime) {
                        setIsStartDev(true);
                        temp.data.edited.push({
                            PK_Ticket_Issue_Assignment_ID: row.Dictionary.PK_Ticket_Issue_Assignment_ID.toString(),
                            FK_Ticket_Issue_ID: row.Dictionary.FK_Ticket_Issue_ID.toString(),
                            Level: row.Dictionary.Level,
                            FK_MUser_ID: match[1],
                            StartDateTime: datetemp.toLocaleString(),
                            EndDateTime: '',
                        });
                        return true; // stop execution after pushing data
                    }
                    else if (!EndDateTime) {
                        setIsEndDev(true)
                        temp.data.edited.push({
                            PK_Ticket_Issue_Assignment_ID: row.Dictionary.PK_Ticket_Issue_Assignment_ID.toString(),
                            FK_Ticket_Issue_ID: row.Dictionary.FK_Ticket_Issue_ID.toString(),
                            Level: row.Dictionary.Level,
                            FK_MUser_ID: match[1],
                            StartDateTime: row.Dictionary.StartDateTime,
                            EndDateTime: datetemp.toLocaleString(),
                        });
                        return true; // stop execution after pushing data
                    }
                }
            });            
        }
        sendDevDate(temp)
    }

    return (
        <React.Fragment>
            {isMobile || isTablet ? (
                <div>
                    <Accordion onClick={handleMobileDetail}>
                        <AccordionSummary>
                            <div style={{ display: 'flex', width: '100%', justifyContent: 'space-between' }}>
                                <Typography>{moduleDetailName}</Typography>
                                <ChevronRightIcon />
                            </div>
                        </AccordionSummary>
                    </Accordion>
                    <Drawer
                        anchor="right"
                        open={openDrawer}
                        classes={{ paper: classes.drawerPaper }}
                        style={{ zIndex: '1000' }}
                    >
                        <div className={classes.buttonPos}>
                            <Button
                                variant="outlined"
                                color="default"
                                onClick={handleMobileDetail}
                                style={{ marginRight: '2%' }}
                            >
                                close
                            </Button>
                        </div>
                        <NawaTable
                            id={moduleDetailName}
                            rowCount={!isSLAorLog ? rows.length : rows.filter((val) => val.active).length}
                            moduleName={moduleDetailName}
                            moduleLabel={moduleDetailName}
                            actionList={actionType == EActionType.Insert || actionType == EActionType.Update ? (isSLAorLog || closedAssignment ? [] : detailActionList) : []}
                            columns={columns}
                            columnKey={columnKey}
                            rows={!isSLAorLog ? rows : rows.filter((val) => val.active)}
                            searchable={true}
                            onClickAction={onClickAction}
                            onClickAdd={actionType == EActionType.Insert || actionType == EActionType.Update ? (isSLAorLog || closedAssignment ? null : onClickAdd) : null}
                            detailColumns={inputColumns}
                            parentData={parentData}
                            isDetail={true}
                            onClickDownload={onClickDownloadCurrentPage}
                            onClickDownloadAll={onClickDownloadAll}
                            onClickDownloadSelected={onClickDownloadSelected}
                            getModuleData={getModuleDetailView}
                            exportTypesList={exportTypesList}
                            isLoading={isLoading}
                        />
                    </Drawer>
                </div>
            ) : (
                <NawaTable
                    id={moduleDetailName}
                    rowCount={!isSLAorLog ? rows.length : rows.filter((val) => val.active).length}
                    moduleName={moduleDetailName}
                    moduleLabel={moduleDetailName}
                    actionList={actionType == EActionType.Insert || actionType == EActionType.Update ? (isSLAorLog || closedAssignment ? [] : detailActionList) : []}
                    columns={columns}
                    columnKey={columnKey}
                    rows={!isSLAorLog ? rows : rows.filter((val) => val.active)}
                    searchable={true}
                    onClickAction={onClickAction}
                    onClickAdd={actionType == EActionType.Insert || actionType == EActionType.Update ? (isSLAorLog || closedAssignment ? null : onClickAdd) : null}
                    detailColumns={inputColumns}
                    parentData={parentData}
                    isDetail={true}
                    onClickDownload={onClickDownloadCurrentPage}
                    onClickDownloadAll={onClickDownloadAll}
                    onClickDownloadSelected={onClickDownloadSelected}
                    exportTypesList={exportTypesList}
                    getModuleData={getModuleDetailView}
                    isLoading={isLoading}
                />
            )}
            <ParameterDetailInput
                open={open}
                moduleName={moduleName}
                actionType={inputActionType}
                columns={inputColumns}
                toggleClose={() => setOpen(false)}
                data={dataToShow}
                onAdd={_onAdd}
                onEdit={_onEdit}
                onDelete={_onDelete}
                rows={rows}
                parentData={parentData}
            />
        </React.Fragment>
    );
}