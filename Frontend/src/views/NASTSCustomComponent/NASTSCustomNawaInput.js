import React from "react";
import {
    EDataType,
    EInputType,
    EActionType,
    NawaInput
} from '@vikitheolorado/nawadata-web';

const InputDataField = (props) => {
    const { itemObject, index, data, type, moduleDbName, onChangeData, focusOnID, setFocusOnID, isDataChanged } = props

    return itemObject.FieldLabel.toLowerCase() === 'raise date' ?
        (
            <div style={{ display: 'flex', gap: '10px' }}>
                <div style={{ flex: 1 }}>
                    <NawaInput
                        key={index}
                        name={itemObject.FieldLabel}
                        type={itemObject.FK_ExtType_ID}
                        required={itemObject.Required}
                        IsPrimaryKey={itemObject.IsPrimaryKey}
                        value={data ? data[itemObject.FieldName] : null}
                        ParentValue={
                            itemObject.BCasCade ? (data ? data[itemObject.FieldNameParent] : null) : ''
                        }
                        dataType={itemObject.FK_FieldType_ID}
                        PK_ModuleField_ID={itemObject.PK_ModuleField_ID}
                        onChange={(value) =>
                            onChangeData(
                                itemObject.FieldName,
                                value,
                                data ? data[itemObject.FieldName] : null,
                                itemObject.FK_ExtType_ID
                            )
                        }
                        sizeField={
                            itemObject.SizeField == null || itemObject.SizeField == undefined
                                ? 0
                                : itemObject.SizeField
                        }
                        listRegex={itemObject.ListRegex}
                        isFocus={itemObject.PK_ModuleField_ID === focusOnID}
                        setFocus={(value) => setFocusOnID(value)}
                        actionType={type}
                        moduleName={moduleDbName}
                        fieldName={itemObject.FieldName}
                        isDataChanged={isDataChanged}
                        bCascade={itemObject.BCasCade}
                    />
                </div>
                <div style={{ flex: 1 }}>
                    <NawaInput
                        key={index}
                        name={'Raise Time'}
                        type={EInputType.TimeField}
                        required={itemObject.Required}
                        IsPrimaryKey={itemObject.IsPrimaryKey}
                        value={data ? data[itemObject.FieldName] : null}
                        ParentValue={
                            itemObject.BCasCade ? (data ? data[itemObject.FieldNameParent] : null) : ''
                        }
                        dataType={EDataType.DATETIME}
                        PK_ModuleField_ID={itemObject.PK_ModuleField_ID}
                        onChange={(value) =>
                            onChangeData(
                                itemObject.FieldName,
                                value,
                                data ? data[itemObject.FieldName] : null,
                                EInputType.TimeField
                            )
                        }
                        sizeField={
                            itemObject.SizeField == null || itemObject.SizeField == undefined
                                ? 0
                                : itemObject.SizeField
                        }
                        listRegex={itemObject.ListRegex}
                        isFocus={itemObject.PK_ModuleField_ID === focusOnID}
                        setFocus={(value) => setFocusOnID(value)}
                        actionType={type}
                        moduleName={moduleDbName}
                        fieldName={itemObject.FieldName}
                        isDataChanged={isDataChanged}
                        bCascade={itemObject.BCasCade}
                    />
                </div>
            </div>
        ) : (
            <NawaInput
                key={index}
                name={itemObject.FieldLabel}
                type={itemObject.FK_ExtType_ID}
                required={itemObject.Required}
                IsPrimaryKey={itemObject.IsPrimaryKey}
                value={data ? data[itemObject.FieldName] : null}
                ParentValue={
                    itemObject.BCasCade ? (data ? data[itemObject.FieldNameParent] : null) : ''
                }
                dataType={itemObject.FK_FieldType_ID}
                PK_ModuleField_ID={itemObject.PK_ModuleField_ID}
                onChange={(value) =>
                    onChangeData(
                        itemObject.FieldName,
                        value,
                        data ? data[itemObject.FieldName] : null,
                        itemObject.FK_ExtType_ID
                    )
                }
                sizeField={
                    itemObject.SizeField == null || itemObject.SizeField == undefined
                        ? 0
                        : itemObject.SizeField
                }
                listRegex={itemObject.ListRegex}
                isFocus={itemObject.PK_ModuleField_ID === focusOnID}
                setFocus={(value) => setFocusOnID(value)}
                actionType={type}
                moduleName={moduleDbName}
                fieldName={itemObject.FieldName}
                isDataChanged={isDataChanged}
                bCascade={itemObject.BCasCade}
            />
        )
};

const ViewDataField = (props) => {
    const { itemObject, index, data } = props

    return itemObject.FieldLabel.toLowerCase() === 'raise date' ?
        (
            <div style={{ display: 'flex', gap: '10px' }}>
                <div style={{ flex: 1 }}>
                    <NawaInput
                        key={index}
                        PK_ModuleField_ID={itemObject.PK_ModuleField_ID}
                        value={data ? data[itemObject.FieldName] : null}
                        ParentValue={itemObject.BCasCade ? (data ? data[itemObject.FieldNameParent] : null) : ''}
                        name={itemObject.FieldLabel}
                        type={itemObject.FK_ExtType_ID}
                        dataType={itemObject.FK_FieldType_ID}
                        required={itemObject.Required}
                        disabled={true}
                        sizeField={itemObject.SizeField == null || itemObject.SizeField == undefined ? 0 : itemObject.SizeField}
                        fontColor='black'
                        actionType={EActionType.Detail}
                    />
                </div>
                <div style={{ flex: 1 }}>
                    <NawaInput
                        key={index}
                        PK_ModuleField_ID={itemObject.PK_ModuleField_ID}
                        value={data ? data[itemObject.FieldName] : null}
                        ParentValue={itemObject.BCasCade ? (data ? data[itemObject.FieldNameParent] : null) : ''}
                        name={'Raise Time'}
                        type={EInputType.TimeField}
                        dataType={itemObject.FK_FieldType_ID}
                        required={itemObject.Required}
                        disabled={true}
                        sizeField={itemObject.SizeField == null || itemObject.SizeField == undefined ? 0 : itemObject.SizeField}
                        fontColor='black'
                        actionType={EActionType.Detail}
                    />
                </div>
            </div>
        ) : (
            <NawaInput
                key={index}
                PK_ModuleField_ID={itemObject.PK_ModuleField_ID}
                value={data ? data[itemObject.FieldName] : null}
                ParentValue={itemObject.BCasCade ? (data ? data[itemObject.FieldNameParent] : null) : ''}
                name={itemObject.FieldLabel}
                type={itemObject.FK_ExtType_ID}
                dataType={itemObject.FK_FieldType_ID}
                required={itemObject.Required}
                disabled={true}
                sizeField={itemObject.SizeField == null || itemObject.SizeField == undefined ? 0 : itemObject.SizeField}
                fontColor='black'
                actionType={EActionType.Detail}
            />
        )
};

export {
    InputDataField,
    ViewDataField
};
