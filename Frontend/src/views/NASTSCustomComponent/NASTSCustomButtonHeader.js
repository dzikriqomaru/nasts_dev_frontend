import React from "react";
import Text from 'react-text';
import {
    EActionType,
} from '@vikitheolorado/nawadata-web';
import { Button } from "@material-ui/core";

const TicketButtonHeader = (props) => {
    const {classes,type,currentStatus,isClosed,isInternal,isExternal,submitStatus,onUpdateStatus,onCancel,showPrompt,isInDev ,isEndDev, isStartDev,RunDev} = props

    return (
        <div className={classes.buttonsContainer}>
            {type !== EActionType.Detail ?
                <>{isExternal && !isClosed && type === EActionType.Update && (
                    <>
                        {(currentStatus == 'SIT' || currentStatus == 'UAT') && (
                            <Button
                                variant="contained"
                                size="small"
                                color="primary"
                                className={classes.submitButton}
                                onClick={() => onUpdateStatus('BOUNCE')}
                            >
                                {'Bounce'}
                            </Button>
                        )}
                        {currentStatus == 'DEPLOY' && (
                            <>
                                <Button
                                    variant="contained"
                                    size="small"
                                    color="primary"
                                    className={classes.submitButton}
                                    onClick={() => onUpdateStatus('test')}
                                >
                                    {'Close'}
                                </Button>
                                <Button
                                    variant="contained"
                                    size="small"
                                    color="primary"
                                    className={classes.submitButton}
                                    onClick={() => onUpdateStatus('BOUNCE')}
                                >
                                    {'Bounce'}
                                </Button>
                            </>
                        )}
                    </>
                )}
                    {isInternal && !isClosed && type === EActionType.Update &&
                        <>
                            <Button
                                variant="contained"
                                size="small"
                                color="primary"
                                className={classes.submitButton}
                                onClick={() => onUpdateStatus('test')}
                            >
                                {submitStatus(currentStatus)}
                            </Button>
                            {(currentStatus == 'SIT' || currentStatus == 'UAT' || currentStatus == 'DEPLOY' || currentStatus == 'INTERNAL TEST') &&
                                <Button
                                    variant="contained"
                                    size="small"
                                    color="primary"
                                    className={classes.submitButton}
                                    onClick={() => onUpdateStatus('BOUNCE')}
                                >
                                    {'Bounce'}
                                </Button>
                            }
                            {currentStatus == 'IN PROGRESS' &&
                                <Button
                                    variant="contained"
                                    size="small"
                                    color="primary"
                                    className={classes.submitButton}
                                    onClick={() => onUpdateStatus('HOLD')}
                                >
                                    {'Hold'}
                                </Button>
                            }
                            {isInDev && (isEndDev || isStartDev) &&
                                <Button variant="contained" size="small" color="primary" onClick={RunDev}>
                                    {isEndDev ? 'End' : 'Start'}
                                </Button>
                            }
                        </>
                    }
                    <Button
                        variant="contained"
                        size="small"
                        color="primary"
                        onClick={showPrompt}
                    >
                        <Text save />
                    </Button></>
                : (null)}
            <Button
                variant="contained"
                size="small"
                color="default"
                onClick={onCancel}
            >
                <Text cancel />
            </Button>
        </div>
    )
};

export default TicketButtonHeader;