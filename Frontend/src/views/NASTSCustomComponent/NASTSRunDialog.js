import React from 'react';
import { Card, CardActionArea, CardMedia, CardContent, Typography, CardActions, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles({
    divContainer: {
        background: 'rgba(0, 0, 0, 0.4)',
        position: 'fixed',
        height: '100vh',
        width: '100%',
        zIndex: 9998,
        margin: 0,
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    container: {
        width: '100vw',
        height: '100vh',
        zIndex: 9999,
        position: 'fixed',
        top: '16vh',
        left: 'calc(50vw - 160px)'
    },
    root: {
        width: '350px',
        height: 'auto',
        backgroundColor: '#F2F5F8'
    },
    rootDelete: {
        width: '320px',
        height: '280px',
        backgroundColor: '#F2F5F8'
    },
    media: {
        margin: '5px',
        height: 130
    },
    content: {
        backgroundColor: '#FFFFFF',
        padding: '16px'
    },
    contentDelete: {
        backgroundColor: '#FFFFFF',
        padding: '28px 16px'
    },
    buttonContent: {
        backgroundColor: '#FFFFFF',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: '8px 16px'
    },
    title: {
        position: 'static',
        width: 'auto',
        height: 'auto',
        left: '0px',
        top: '0px',
        fontFamily: 'OpenSans',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: '16px',
        display: 'flex',
        alignItems: 'center',
        letterSpacing: '-0.02em',
        color: '#3D3D3D',
        flex: 'none',
        order: 0,
        flexGrow: 1
    },
    text: {
        position: 'static',
        left: '0px',
        top: '56px',
        fontFamily: 'OpenSans',
        fontStyle: 'normal',
        fontWeight: 'normal',
        fontSize: '14px',
        display: 'flex',
        alignItems: 'center',
        letterSpacing: '-0.02em',
        color: '#A6A6A6',
        flex: 'none',
        order: 1,
        flexGrow: 1,
        margin: '8px 4px'
    }
});

export default function RunConfirmation(props) {
    const classes = useStyles();
    const { cancel, discard, save, type, activeState } = props;

    return (
        <div className={classes.divContainer}>
            <div className={classes.container}>
                <Card className={classes.root}>
                    <CardActionArea style={{ cursor: 'default' }}>
                        <CardMedia
                            className={classes.media}
                            image='/images/UnsavedChages.png'
                            title='Contemplative Reptile'
                        />
                        <CardContent className={classes.contentDelete}>
                            <Typography className={classes.title}>
                                {'Are you sure you want to '}
                                {type == 'End' ? 'finish' : 'start'}
                                {' the development'}
                                {' on this ticket?'}
                            </Typography>
                        </CardContent>
                    </CardActionArea>
                    <CardActions className={classes.buttonContent}>
                        <Button
                            size='small'
                            color='primary'
                            onClick={() => cancel(false)}
                        >
                            Cancel
                        </Button>
                        <div>
                            <Button
                                style={{ margin: '0 8px' }}
                                size='small'
                                color='primary'
                                variant='contained'
                                onClick={() => {
                                    save();
                                    cancel(false);
                                }}
                            >
                                {type}
                            </Button>
                        </div>
                    </CardActions>
                </Card>
            </div>
        </div>
    );
}
