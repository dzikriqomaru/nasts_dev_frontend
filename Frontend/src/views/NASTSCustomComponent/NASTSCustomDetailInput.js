
import { Button, Dialog, DialogActions, DialogContent, DialogTitle, List, ListItem, ListItemIcon, ListItemText, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Delete, KeyboardArrowRight, Save } from '@material-ui/icons';
import { EActionType, EDataType, EInputType, NawaInput, UserProfileContext, useDateFormatList, useInfoPanel, useTimeFormat } from '@vikitheolorado/nawadata-web';
import moment from 'moment';
import React, { useContext, useEffect, useState } from 'react';
import Text from 'react-text';

const useStyles = makeStyles((theme) => ({
    root: {
        height: "calc(" + theme.contentHeight + ")"
    },
    flexCol: {
        flexDirection: "column"
    },
    errorBox: {
        backgroundColor: "#ffbbbb",
        border: "2px solid #808080",
        borderRadius: "5px",
        margin: "10px 0px"
    },
    errorHeader: {
        padding: "10px",
        fontWeight: "bold"
    },
    btnFixed: {
        alignItems: "center",
        justifyContent: "center"
    },
    dialogBanner: {
        minHeight: '80vh',
        maxHeight: '80vh',
        marginLeft: "calc(2.5vw + 240px)",
        marginRight: '2.5vw'
    },
    dialogBanner2: {
        minHeight: '80vh',
        maxHeight: '80vh',
        marginLeft: "calc(2.5vw + 72px)",
        marginRight: '2.5vw'
    }
}));

export default function ParameterDetailInput(props) {
    const classes = useStyles();
    const { open, moduleName, columns = [], actionType, toggleClose, onAdd, onEdit, rows, onDelete, parentData } = props;

    const [data, setData] = useState(props.data == null || props.data == undefined ? {} : props.data);
    const [focusOnID, setFocusOnID] = useState(null);
    const [errorList, setErrorList] = useState([]);
    const [oldDataChanged, setOldDataChanged] = useState({});
    const [isDataChanged, setIsDataChanged] = useState(false);

    const UserProfile = useContext(UserProfileContext);
    const dateFormatIndex = useDateFormatList().map((x) => x.PK_SystemParameter_ID).indexOf(6);
    const dateFormatList = useDateFormatList()[dateFormatIndex];
    const timeFormatList = useTimeFormat()[0];

    const dateFormat = dateFormatList ? dateFormatList.MomentJSFormat : '';
    const timeFormat = timeFormatList ? timeFormatList.MomentJSFormat : '';

    const { showInfoPanel } = useInfoPanel();

    useEffect(() => {
        setData(props.data);
    }, [actionType, props.data]);

    useEffect(() => {
        if (open) {
            setOldDataChanged({ ...props.data });
        }
    }, [open]);

    const onChangeData = (fieldName, value) => {
        let newData = data;
        newData[fieldName] = value;
        setData({ ...newData });
        setIsDataChanged(true);
    };

    const validateData = (actionType) => {
        let errStatus = false;
        let newErrorList = [];
        let PKDetailValueID = 0;
        let PKDetailName = "";

        columns.forEach((item) => {
            let value = data[item.FieldName];

            switch (item.FK_ExtType_ID) {
                case EInputType.DateField:
                    let validDate = moment(new Date(value), dateFormat);
                    if (!value && item.Required) {
                        errStatus = true;
                        newErrorList.push({
                            message: `${item.FieldLabel} is required`,
                            PK_ModuleDetailField_ID: item.PK_ModuleDetailField_ID
                        });
                    } else if (value && !validDate.isValid()) {
                        errStatus = true;
                        newErrorList.push({
                            message: `${item.FieldLabel} Invalid date format. Please enter the date in the format "DD/MM/YYYY".`,
                            PK_ModuleDetailField_ID: item.PK_ModuleDetailField_ID
                        });
                    }
                    break;
                case EInputType.TimeField:
                    let validTime = moment(new Date(value), timeFormat);
                    if (!value && item.Required) {
                        errStatus = true;
                        newErrorList.push({
                            message: `${item.FieldLabel} is required`,
                            PK_ModuleDetailField_ID: item.PK_ModuleDetailField_ID
                        });
                    } else if (value && !validTime.isValid()) {
                        errStatus = true;
                        newErrorList.push({
                            message: `${item.FieldLabel} Invalid Time format. Please enter the time in the format "hh(hour)/mm(minutes)/a(AM or PM).`,
                            PK_ModuleDetailField_ID: item.PK_ModuleDetailField_ID
                        });
                    }
                    break;
                case EInputType.TextField:
                    break;
                case EInputType.PasswordField:
                    break;
                case EInputType.FileUpload:
                    if ((!value || value.fileName === '' && value.fileData === '') && item.Required) {
                        errStatus = true;
                        newErrorList.push({
                            message: `${item.FieldLabel} is required`,
                            PK_ModuleDetailField_ID: item.PK_ModuleDetailField_ID
                        });
                    }
                    break;
                case EInputType.QueryField:
                    if (value && item.SizeField > 0 && value.length > item.SizeField) {
                        errStatus = true;
                        newErrorList.push({
                            message: `${item.FieldLabel} is limited to ${item.SizeField} characters`,
                            PK_ModuleDetailField_ID: item.PK_ModuleDetailField_ID
                        });
                    } else if (!value && item.Required && item.FK_FieldType_ID !== EDataType.IDENTITY && item.FK_FieldType_ID !== EDataType.BIGIDENTITY) {
                        errStatus = true;
                        newErrorList.push({
                            message: `${item.FieldLabel} is required`,
                            PK_ModuleDetailField_ID: item.PK_ModuleDetailField_ID
                        });
                    } else if (item.ListRegex) {
                        let regexError = false;
                        item.ListRegex.forEach((itemRegex) => {
                            let reg = new RegExp(itemRegex.Regex);
                            if (!reg.test(value)) {
                                regexError = true;
                            }
                        });
                        if (regexError) {
                            errStatus = true;
                            newErrorList.push({
                                message: `${item.FieldLabel} input format is invalid`,
                                PK_ModuleDetailField_ID: item.PK_ModuleDetailField_ID
                            });
                        }
                    }
                    break;
                case EInputType.RichText:
                    if ((!value || value === '<p><br></p>') && item.Required) {
                        errStatus = true;
                        newErrorList.push({
                            message: `${item.FieldLabel} ${document.getElementById('require').innerHTML}`,
                            PK_ModuleDetailField_ID: item.PK_ModuleDetailField_ID
                        });
                    }
                    break;
                case EInputType.NumberField:
                    // Required validation
                    if (item.Required && (value === "" || value === null)) {
                        if (item.FK_FieldType_ID !== EDataType.IDENTITY && item.FK_FieldType_ID !== EDataType.BIGIDENTITY) {
                            errStatus = true;
                            newErrorList.push({
                                message: `${item.FieldLabel} Is Required`,
                                PK_ModuleDetailField_ID: item.PK_ModuleDetailField_ID
                            });
                        }
                    }

                    let regex = "";
                    if (value === '-' || value === '.' || value === ',' || value === '-.' || value === '-,' || value[value.length - 1] === '.' || value[value.length - 1] === ',') {
                        errStatus = true;
                        newErrorList.push({
                            message: `${item.FieldLabel} Invalid Format for Numberfield`,
                            PK_ModuleDetailField_ID: item.PK_ModuleDetailField_ID
                        });
                    } else if (item.FK_FieldType_ID === EDataType.INT || item.FK_FieldType_ID === EDataType.BIGINT || item.FK_FieldType_ID === EDataType.SMALLINT || item.FK_FieldType_ID === EDataType.TINYINT) {
                        if (UserProfile.DecimalSeparator === '.') {
                            regex = /[^0-9-,]/ig;
                        } else {
                            regex = /[^0-9-.]/ig;
                        }
                        if (regex.test(value)) {
                            errStatus = true;
                            newErrorList.push({
                                message: `${item.FieldLabel} Invalid Format for Numberfield`,
                                PK_ModuleDetailField_ID: item.PK_ModuleDetailField_ID
                            });
                        }
                    } else if (item.FK_FieldType_ID === EDataType.FLOAT || item.FK_FieldType_ID === EDataType.MONEY || item.FK_FieldType_ID === EDataType.NUMERIC_DECIMAL || item.FK_FieldType_ID === EDataType.REAL) {
                        regex = /[^0-9-,.]/ig;
                        if (regex.test(value)) {
                            errStatus = true;
                            newErrorList.push({
                                message: `${item.FieldLabel} Invalid Format for Numberfield`,
                                PK_ModuleDetailField_ID: item.PK_ModuleDetailField_ID
                            });
                        }
                    }

                    let val = '';
                    let splitVal = '';
                    if (value) {
                        if (UserProfile.DecimalSeparator === ',') {
                            val = value.replaceAll('.', '').replace(',', '.');
                        } else {
                            val = value.replaceAll(',', '');
                        }
                    } else {
                        val = '';
                    }
                    splitVal = val.replace('-', '').split('.');

                    if (item.FK_FieldType_ID === EDataType.BIGINT && (val < Number.MIN_SAFE_INTEGER || val > Number.MAX_SAFE_INTEGER)) {
                        errStatus = true;
                        newErrorList.push({
                            message: `${item.FieldLabel} Reach BIGINT Limit from -9,007,199,254,740,991 to 9,007,199,254,740,991`,
                            PK_ModuleDetailField_ID: item.PK_ModuleDetailField_ID
                        });
                    } else if (item.FK_FieldType_ID === EDataType.INT && (val < -2147483648 || val > 2147483647)) {
                        errStatus = true;
                        newErrorList.push({
                            message: `${item.FieldLabel} Reach INT Limit from -2,147,483,648 to 2,147,483,647`,
                            PK_ModuleDetailField_ID: item.PK_ModuleDetailField_ID
                        });
                    } else if (item.FK_FieldType_ID === EDataType.SMALLINT && (val < -32768 || val > 32767)) {
                        errStatus = true;
                        newErrorList.push({
                            message: `${item.FieldLabel} Reach SMALLINT Limit from -32,768 to 32,767`,
                            PK_ModuleDetailField_ID: item.PK_ModuleDetailField_ID
                        });
                    } else if (item.FK_FieldType_ID === EDataType.TINYINT && (val < 0 || val > 255)) {
                        errStatus = true;
                        newErrorList.push({
                            message: `${item.FieldLabel} Reach TINYINT Limit from 0 to 255`,
                            PK_ModuleDetailField_ID: item.PK_ModuleDetailField_ID
                        });
                    } else if (item.FK_FieldType_ID === EDataType.MONEY && (val < -922337203685477 || val > 922337203685477)) {
                        errStatus = true;
                        newErrorList.push({
                            message: `${item.FieldLabel} Reach MONEY Limit from -922337203685477 to 922337203685477`,
                            PK_ModuleDetailField_ID: item.PK_ModuleDetailField_ID
                        });
                    } else if ((item.FK_FieldType_ID === EDataType.REAL || item.FK_FieldType_ID === EDataType.FLOAT || item.FK_FieldType_ID === EDataType.NUMERIC_DECIMAL) && (val < Number.MIN_SAFE_INTEGER || val > Number.MAX_SAFE_INTEGER)) {
                        errStatus = true;
                        newErrorList.push({
                            message: `${item.FieldLabel} Reach Limit from Javascript MIN_SAFE_INTEGER to Javascript MAX_SAFE_INTEGER`,
                            PK_ModuleDetailField_ID: item.PK_ModuleDetailField_ID
                        });
                    } else if ((item.FK_FieldType_ID === EDataType.MONEY || item.FK_FieldType_ID === EDataType.FLOAT || item.FK_FieldType_ID === EDataType.REAL) && splitVal[1] && item.SizeField > 0 && splitVal[1].length > item.SizeField) {
                        errStatus = true;
                        newErrorList.push({
                            message: `${item.FieldLabel} is limited to ${item.SizeField} characters`,
                            PK_ModuleDetailField_ID: item.PK_ModuleDetailField_ID
                        });
                    }
                    break;
                default:
                    if (item.Required && item.FK_FieldType_ID !== EDataType.IDENTITY && item.FK_FieldType_ID !== EDataType.BIGIDENTITY && !value) {
                        errStatus = true;
                        newErrorList.push({
                            message: `${item.FieldLabel} is required`,
                            PK_ModuleDetailField_ID: item.PK_ModuleDetailField_ID
                        });
                    }
                    if (item.FK_FieldType_ID === EDataType.BIGINT && (value < Number.MIN_SAFE_INTEGER || value > Number.MAX_SAFE_INTEGER)) {
                        errStatus = true;
                        newErrorList.push({
                            message: `${item.FieldLabel} Reach BIGINT Limit from -9,007,199,254,740,991 to 9,007,199,254,740,991`,
                            PK_ModuleDetailField_ID: item.PK_ModuleDetailField_ID
                        });
                    } else if (item.FK_FieldType_ID === EDataType.INT && (value < -2147483648 || value > 2147483647)) {
                        errStatus = true;
                        newErrorList.push({
                            message: `${item.FieldLabel} Reach INT Limit from -2,147,483,648 to 2,147,483,647`,
                            PK_ModuleDetailField_ID: item.PK_ModuleDetailField_ID
                        });
                    } else if (item.FK_FieldType_ID === EDataType.SMALLINT && (value < -32768 || value > 32767)) {
                        errStatus = true;
                        newErrorList.push({
                            message: `${item.FieldLabel} Reach SMALLINT Limit from -32,768 to 32,767`,
                            PK_ModuleDetailField_ID: item.PK_ModuleDetailField_ID
                        });
                    } else if (item.FK_FieldType_ID === EDataType.TINYINT && (value < 0 || value > 255)) {
                        errStatus = true;
                        newErrorList.push({
                            message: `${item.FieldLabel} Reach TINYINT Limit from 0 to 255`,
                            PK_ModuleDetailField_ID: item.PK_ModuleDetailField_ID
                        });
                    } else if (item.FK_FieldType_ID === EDataType.BIGINT || item.FK_FieldType_ID === EDataType.INT || item.FK_FieldType_ID === EDataType.SMALLINT || item.FK_FieldType_ID === EDataType.TINYINT) {
                        let pattern = /^[-+]?\d+$/;
                        let reg = new RegExp(pattern);
                        if (!reg.test(value) && value.length > 0) {
                            errStatus = true;
                            newErrorList.push({
                                message: `incorrect ${item.FieldLabel} format`,
                                PK_ModuleDetailField_ID: item.PK_ModuleDetailField_ID
                            });
                        }
                    }
                    break;
            }

            if (item.IsPrimaryKey) {
                PKDetailValueID = data[item.FieldName] ? data[item.FieldName].toString().replaceAll('.', '') : 0;
                PKDetailName = item.FieldName;
            }

            if (oldDataChanged[item.FieldName] !== value && (actionType === EActionType.Insert || actionType === EActionType.Update) && item.IsUnik) {
                let countDuplicate = rows.filter((obj) => {
                    if (item.FK_ExtType_ID === EInputType.DateField) {
                        return new Date(obj[item.FieldName]).setHours(0, 0, 0, 0) === new Date(value).setHours(0, 0, 0, 0) && value !== "" && obj[PKDetailName] !== PKDetailValueID;
                    } else if (item.FK_ExtType_ID === EInputType.TimeField) {
                        let momOld = moment(new Date(obj[item.FieldName])).format(timeFormat).toString();
                        let momNew = moment(new Date(value)).format(timeFormat).toString();
                        return momOld === momNew && value !== "" && obj[PKDetailName] !== PKDetailValueID;
                    } else if (item.FK_ExtType_ID === EInputType.NumberField) {
                        let valueNumber = value;
                        if (value) {
                            if (UserProfile.DecimalSeparator === ',') {
                                let a = valueNumber ? valueNumber.replaceAll('.', '').replace(',', '.') : "";
                                valueNumber = valueNumber ? String(a) : null;
                            } else {
                                let _a = valueNumber ? valueNumber.replaceAll(',', '') : "";
                                valueNumber = valueNumber ? String(_a) : null;
                            }
                        } else {
                            valueNumber = String(valueNumber);
                        }
                        return obj[item.FieldName] === valueNumber && valueNumber !== "" && obj[PKDetailName] !== PKDetailValueID;
                    } else {
                        return obj[item.FieldName] === value && value !== "" && obj[PKDetailName] !== PKDetailValueID;
                    }
                }).length;

                if (countDuplicate > 0) {
                    errStatus = true;
                    newErrorList.push({
                        message: `${item.FieldLabel} duplicate`,
                        PK_ModuleDetailField_ID: item.PK_ModuleDetailField_ID
                    });
                }
            }
        });
        setErrorList(newErrorList);
        return errStatus;
    };

    const onSave = () => {
        let arr = {};
        setIsDataChanged(false);
        Object.entries(JSON.parse(JSON.stringify(data))).forEach(([key, value]) => {
            if (columns.find((item) => item.FieldName === key).FK_FieldType_ID === EDataType.DATE || columns.find((item) => item.FieldName === key).FK_FieldType_ID === EDataType.DATETIME) {
                if (columns.find((item) => item.FieldName === key).FK_ExtType_ID === EInputType.DateField) {
                    arr[key] = value ? new Date(value).toDateString() : '';
                } else {
                    arr[key] = value ? value.toLocaleString() : '';
                }
            } else if (columns.find((item) => item.FieldName === key).FK_ExtType_ID === EInputType.FileUpload || columns.find((item) => item.FieldName === key).FK_ExtType_ID === EInputType.DisplayField && columns.find((item) => item.FieldName === key).FK_FieldType_ID === EDataType.VARBINARY) {
                arr[key] = value ? value.fileData ? String(value.fileData) : '' : '';
                let columnFileName = `${key}Name`;
                arr[columnFileName] = value ? value.fileName ? String(value.fileName) : '' : '';
            } else if (columns.find((item) => item.FieldName === key).FK_ExtType_ID === EInputType.DropDownField || columns.find((item) => item.FieldName === key).FK_ExtType_ID === EInputType.PopUpCombobox) {
                arr[key] = value ? String(value) : null;
            } else if (columns.find((item) => item.FieldName === key).FK_ExtType_ID === EInputType.PopUpMultiCheckBox) {
                arr[key] = value ? String(value) : '';
            } else if (columns.find((item) => item.FieldName === key).FK_ExtType_ID === EInputType.NumberField) {
                if (value) {
                    if (UserProfile.DecimalSeparator === ',') {
                        let a = value.replaceAll('.', '').replace(',', '.');
                        arr[key] = value ? String(a) : null;
                    } else {
                        let _a2 = value.replaceAll(',', '');
                        arr[key] = value ? String(_a2) : null;
                    }
                } else {
                    arr[key] = String(value);
                }
            } else if (columns.find((item) => item.FieldName === key).FK_FieldType_ID === EDataType.BOOLEAN) {
                if (value === '') {
                    arr[key] = '';
                } else if (value === true || value === 1 || value === 'true') {
                    arr[key] = true;
                } else {
                    arr[key] = false;
                }
            } else {
                arr[key] = String(value);
            }
        });

        if (validateData(actionType)) {
            showInfoPanel.error("There are some problems with your input(s). Please check error message for more detail.");
        } else {
            switch (actionType) {
                case EActionType.Insert:
                    onAdd(arr);
                    break;
                case EActionType.Update:
                    onEdit(arr);
                    break;
                case EActionType.Delete:
                    onDelete(arr);
                    break;
                default:
                    break;
            }
        }
    };

    return (
        <Dialog
            open={open}
            scroll="paper"
            aria-labelledby="parameter-dialog-title"
            fullWidth
            maxWidth="lg"
            classes={{ paper: UserProfile.OpenedTreeMenu ? classes.dialogBanner : classes.dialogBanner2 }}
            disableBackdropClick
            disableEscapeKeyDown
        >
            <DialogTitle id="parameter-dialog-title">
                {actionType === EActionType.Insert ? "Add Data" : actionType === EActionType.Update ? "Edit Data" : "Delete Data"}
            </DialogTitle>
            <DialogContent dividers className={classes.flexCol}>
                {errorList.length > 0 ? (
                    <div className={classes.errorBox}>
                        <Typography variant="h5" className={classes.errorHeader}>
                            "There were some problems with your input:"
                        </Typography>
                        <List dense>
                            {errorList.map((item) => (
                                <ListItem key={item.PK_ModuleDetailField_ID} button onClick={() => setFocusOnID(item.PK_ModuleDetailField_ID)}>
                                    <ListItemIcon>
                                        <KeyboardArrowRight style={{ color: "black" }} />
                                    </ListItemIcon>
                                    <ListItemText primary={<Typography variant="h6" style={{ color: "black" }}>{item.message}</Typography>} />
                                </ListItem>
                            ))}
                        </List>
                    </div>
                ) : <React.Fragment />}
                {columns.filter((val) => val.IsShowInForm).map((item, index) => (
                    <NawaInput
                        key={index}
                        moduleName={moduleName}
                        fieldName={item.FieldName}
                        name={item.FieldLabel}
                        type={item.FK_ExtType_ID}
                        required={item.Required}
                        value={data[item.FieldName]}
                        ParentValue={item.BCasCade ? data[item.FieldNameParent] ? String(data[item.FieldNameParent]).split('-')[0].split('(')[1].split(')')[0] : "" : ""}
                        dataType={item.FK_FieldType_ID}
                        PK_ModuleField_ID={item.PK_ModuleDetailField_ID}
                        onChange={actionType === EActionType.Delete || actionType === EActionType.Detail ? null : (value) => onChangeData(item.FieldName, value)}
                        actionType={actionType}
                        sizeField={item.SizeField == null || item.SizeField == undefined ? 0 : item.SizeField}
                        listRegex={item.ListRegex}
                        isFocus={item.PK_ModuleDetailField_ID === focusOnID}
                        setFocus={(value) => setFocusOnID(value)}
                        isModuleDetail
                        isDataChanged={isDataChanged}
                        bCascade={item.BCasCade}
                    />
                ))}
            </DialogContent>
            <DialogActions className={classes.btnFixed}>
                {actionType === EActionType.Detail ? null :
                    <Button
                        size="small"
                        color="primary"
                        variant="contained"
                        startIcon={actionType === EActionType.Delete ? <Delete /> : <Save />}
                        onClick={onSave}
                    >
                        {actionType === EActionType.Delete ? <Text delete /> : <Text save />}
                    </Button>
                }
                <Button
                    size="small"
                    variant="contained"
                    onClick={() => {
                        toggleClose(); setErrorList([]);
                    }}
                >
                    <Text cancel />
                </Button>
            </DialogActions>
        </Dialog>
    );
}