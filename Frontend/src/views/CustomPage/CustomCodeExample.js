import React from 'react'

function CustomCodeExample(props) {
const {moduleid, type, id, PK_MViewBuilder_ID, Param1, Param2} = props.location.state

  return (
    <React.Fragment>
        <div>CustomCodeExample</div>
        <br/>
        <div>Default Parameter :</div>
        <div>
            ModuleID : {moduleid}<br/>
            ActionType : {type}<br/>
            ID : {id}<br/>
            PK_MViewBuilder_ID {PK_MViewBuilder_ID}<br/>
            Param1 : {Param1?Param1:''}
            Param2 : {Param2?Param2:''}
        </div>

    </React.Fragment>
  )
}

export default CustomCodeExample