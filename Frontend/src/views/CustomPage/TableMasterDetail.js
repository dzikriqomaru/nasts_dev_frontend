import React, { useEffect, useContext, useState }from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Paper,
} from '@material-ui/core';
import {
    APIRequest,
    UserProfileContext,
    DialogMessage as MsgDialog
} from '@vikitheolorado/nawadata-web';
const controllerUrl = "api/custom/";

const GetCell = props => {
    const { action, dataNew, dataOld } = props;
    const listNew = dataNew ? Object.values(dataNew[0]) : null;
    const listOld = dataOld ? Object.values(dataOld[0]) : null;

    return (
        action=='Insert'?listNew.map((item,index)=>
            <React.Fragment>
                <TableCell align='left'>
                    { listNew[index] === true
                        ? '1'
                        : listNew[index] === false
                            ? '0'
                            : listNew[index]
                    }
                </TableCell>
                <TableCell align='left' />
            </React.Fragment>
        )
            : action=='Update'?listNew.map((item,index)=>
                <React.Fragment>
                    <TableCell align='left'>
                        { listNew[index] === true
                            ? '1'
                            : listNew[index] === false
                                ? '0'
                                : listNew[index]
                        }
                    </TableCell>
                    <TableCell align='left'>
                        { listOld[index] === true
                            ? '1'
                            : listOld[index] === false
                                ? '0'
                                : listOld[index]
                        }
                    </TableCell>
                </React.Fragment>
            )
                : listOld.map((item,index)=>
                    <React.Fragment>
                        <TableCell align='left' />
                        <TableCell align='left'>
                            { listOld[index] === true
                                ? '1'
                                : listOld[index] === false
                                    ? '0'
                                    : listOld[index]
                            }
                        </TableCell>
                    </React.Fragment>
                )
    )
}

const useStyles = makeStyles({
    table: {
        minWidth: 650,
    },
});

export default function TableMasterDetail(props) {
    const { list, PK_ModuleApproval_ID } = props;
    const classes = useStyles();
    const UserProfile = useContext(UserProfileContext);
    const [data,setData] = React.useState([]);

    const [msgDialogOpen, setMsgDialogOpen] = useState(false);
    const [msgDialogTitle, setMsgDialogTitle] = useState('');
    const [msgDialogContent, setMsgDialogContent] = useState('');

    const dialogHandle = () => {
        setMsgDialogOpen(!msgDialogOpen);
    }
    
    useEffect(() => {
        getModuleDetailApprovalByFKID();
    }, []);

    const getModuleDetailApprovalByFKID = () => {
        if (PK_ModuleApproval_ID && list.PK_ModuleDetail_ID) {
            APIRequest.Send(
                {
                    method: 'GET',
                    url: controllerUrl + 'GetModuleDetailApprovalByFkID', // No API yet
                    params : {
                        PK_ModuleApproval_ID: PK_ModuleApproval_ID,
                        ModuleDetailID: list.PK_ModuleDetail_ID,
                    },
                },
                UserProfile
            ).then(function (res) {
                if (res.status === 200) {
                    setData(res.data);
                } else {
                    console.log('Invalid Credential'); // Wait for API changes (add message dialog)
                }
            });
        }
        else {
            // console.log("Wrong Data");
            setMsgDialogTitle('Error');
            setMsgDialogContent('ID Not Found');
            dialogHandle();
        }
    };

    return (
        <TableContainer
            component={Paper} 
            style={{ margin:'10px',width:'auto' }}
        >
            <Table 
                className={classes.table} 
                size='small' 
                aria-label='a dense table'
            >
                <TableHead>
                    <TableRow>
                        <TableCell align='center' rowSpan='2'>
                            Action
                        </TableCell>
                        {
                            list.ListModuleDetailField.map(item=>
                                <TableCell align='center' colSpan='2'>
                                    {item.FieldLabel}
                                </TableCell>
                            )
                        }
                    </TableRow>
                    <TableRow>
                        {
                            list.ListModuleDetailField.map(item=>
                                <React.Fragment>
                                    <TableCell align='center' >New</TableCell>
                                    <TableCell align='center' >Old</TableCell>
                                </React.Fragment>
                            )
                        }

                    </TableRow>
                </TableHead>
                <TableBody>
                    {
                        data.map(item=>
                            <TableRow >
                                <TableCell 
                                    component='th' 
                                    scope='row' 
                                    align='left'
                                >
                                    {item.ModuleActionName}
                                </TableCell>
                                <GetCell 
                                    action={item.ModuleActionName} 
                                    dataNew={item.newData} 
                                    dataOld={item.oldData}
                                />
                            </TableRow>
                        )
                    }
                </TableBody>
            </Table>

            <MsgDialog 
                isOpen={msgDialogOpen}
                msgTitle={msgDialogTitle}
                message={msgDialogContent}
                handleDialog={dialogHandle}
            />
        </TableContainer>
    );
}
