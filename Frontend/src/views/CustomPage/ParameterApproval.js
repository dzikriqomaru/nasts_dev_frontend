import React, { useState, useEffect, useContext } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
    Paper,
} from '@material-ui/core';
import Text from 'react-text';

import { Redirect } from 'react-router-dom';

import {
	APIRequest,
	EActionType,
	UserProfileContext, useInfoPanel,
	NawaTable
} from '@vikitheolorado/nawadata-web';

const controllerUrl = "api/custom/";

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
    },
    container: {
        height: `calc(${theme.contentHeight} - 105px)`,
    },
    visuallyHidden: {
        border: 0,
        clip: 'rect(0 0 0 0)',
        height: 1,
        margin: -1,
        overflow: 'hidden',
        padding: 0,
        position: 'absolute',
        top: 20,
        width: 1,
    },
}));

export default function ParameterApproval(props) {
    const params = props.location.params;
    const ModuleID = params.find((item) => item.key == 'ModuleID').value;
    const classes = useStyles();
    const UserProfile = useContext(UserProfileContext);
    const [rowCount, setRowCount] = useState(0);
    const [moduleName, setModuleName] = useState('Module');
    const [textModuleName, setTextModuleName] = useState(null);
    const [redirect, setRedirect] = useState(null);
    const [columns, setColumns] = useState([]);
    const [columnKey, setColumnKey] = useState(['']);
    const [rows, setRows] = useState([]);
    const [isLoading, setIsLoading] = useState(true);
    const { showInfoPanel } = useInfoPanel();

    const actionList = [
        {
            text: 'Details',
            url: '/ParameterApprovalDetail',
            type: EActionType.Detail,
        },
    ];

    const getModuleData = (page, orderBy, order, search, filter) => {
        setIsLoading(true);
        let tempFilter = '';
        let filterKey = '';
        let filterValue = '';
        let src= '';
        if( filter ) {
            src = '';
            tempFilter = filter.split(" ");
            filterKey = tempFilter[0];
            filterValue = tempFilter[2];
            if (filterValue) {
                filterValue = filterValue.slice(2, filterValue.length-2);
            }
            if (filterKey=='ModuleName') {
                filterKey='ModuleApproval.ModuleName';
            } 
            else if (filterKey == 'CreatedBy') {
                filterKey='ModuleApproval.CreatedBy';
            } 
            else if (filterKey === 'CreatedDate') {
                filterKey='ModuleApproval.CreatedDate';
            }
        }
        if (search) {
            src = search;
        } 
        else {
            src = '';
        }      
        if (ModuleID) {
            APIRequest.Send(
                {
                    method: 'GET',
                    url: controllerUrl + 'GetModuleApproval',
                    params: {
                        ModuleID: ModuleID,
                        page: page,
                        orderBy: orderBy,
                        order: order,
                        search: src,
                        filterKey:src ? '' : filterKey,
                        filterValue:filterValue
                    }
                },
                UserProfile
            ).then(function (res) {
                if (res.status === 200) {
                    if (res.data) {
                        setRowCount(res.data.count);
                        setRows(res.data.rows.map((value) => value));

                    } else {
                        showInfoPanel.error('Invalid Credential');
                    }
                } else {
                    showInfoPanel.error('Invalid Credential');
                }
            }).finally(() => setIsLoading(false));
        }
        else {
            showInfoPanel.error('Module ID Not Found');
        }
    };

    const getModuleDetail = () => {
        if (ModuleID) {
            APIRequest.Send(
                {
                    method: 'GET',
                    url:controllerUrl + 'GetModuleDetail?ModuleID=' + ModuleID,
                },
                UserProfile
            ).then(function (res) {
                if (res.status === 200) {
                    if (res.data) {
                        setModuleName(res.data.ModuleLabel);
                        setTextModuleName(<Text module-approval name={res.data.ModuleLabel} />);
                    } 
                    else {
                        showInfoPanel.error('Invalid Credential')
                    }
                } 
                else {
                    showInfoPanel.error('Invalid Credential')
                }
            });
        }

        // still hardcoded ?
        setColumnKey('PK_ModuleApproval_ID');
        setColumns([
            { 
                id: 'PK_ModuleApproval_ID', label: 'ID', key: true,
                isShow: true,
                fieldType: 12,
                inputType: 4,
                tabelReferenceName: '',
                tableReferenceFieldKey: '',
                tableReferenceFieldDisplayName: '',
                sequence: 1,
        },
            { 
                id: 'ModuleName', label: 'Module Name', key: false,
                isShow: true,
                fieldType: 9,
                inputType: 5,
                tabelReferenceName: '',
                tableReferenceFieldKey: '',
                tableReferenceFieldDisplayName: '',
                sequence: 2,
        },
            { 
                id: 'ModuleKey', label: 'Module Key', key: false,
                isShow: true,
                fieldType: 9,
                inputType: 5,
                tabelReferenceName: '',
                tableReferenceFieldKey: '',
                tableReferenceFieldDisplayName: '',
                sequence: 3,
        },
            { 
                id: 'ModuleActionName', label: 'Module Action', key: false,
                isShow: true,
                fieldType: 9,
                inputType: 5,
                tabelReferenceName: '',
                tableReferenceFieldKey: '',
                tableReferenceFieldDisplayName: '',
                sequence: 4,
        },
            { 
                id: 'CreatedDate', label: 'Created Date', key: false,
                isShow: true,
                fieldType: 10,
                inputType: 5,
                tabelReferenceName: '',
                tableReferenceFieldKey: '',
                tableReferenceFieldDisplayName: '',
                sequence: 5,
        },
            { 
                id: 'CreatedBy', label: 'Created By', key: false,
                isShow: true,
                fieldType: 9,
                inputType: 5,
                tabelReferenceName: '',
                tableReferenceFieldKey: '',
                tableReferenceFieldDisplayName: '',
                sequence: 6,
        },
        ]);

        getModuleData(1, '', '', '', '');
    };

    useEffect(() => {
        getModuleDetail();
    }, [props.location.params]);

    const onClickAction = (sender, record) => {
        setRedirect({
            pathname: sender.url,
            state: {
                type: sender.type,
                moduleid: ModuleID,
                id: record[columnKey],
            },
        });
    };

    return redirect ? (
        <Redirect to={redirect} />
    ) : (
        <Paper className={classes.root}>
            <NawaTable
                id='approval-table'
                moduleName={textModuleName}
                rowCount={rowCount}
                rows={rows}
                columns={columns}
                actionList={actionList}
                getModuleData={getModuleData}
                onClickAction={onClickAction}
                noAdvFilter={true}
                isLoading={isLoading}
            />
        </Paper>
    );
}