import React, { useContext, useEffect, useState } from "react";
import { Redirect } from 'react-router-dom';
import { APIRequest, EActionType, EInputType, NawaTable, UserProfileContext, theme, useInfoPanel } from "@vikitheolorado/nawadata-web";
import { makeStyles } from '@material-ui/core/styles';
import Text from 'react-text';
import FileSaver from 'file-saver';
import { Button, Paper } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
    root: {
        position: 'relative',
        width: '100%'
    },
    buttonContainer: {
        display: 'flex',
        justifyContent: 'space-between',
        position: 'absolute',
        right: 0,
        top: 0,
        paddingTop: '9px',
        paddingRight: '16px',
        zIndex: 2, // to position it on top of the table
        width: '20vw', // to make it take up half of the viewport width
    }
}));


export default function ParameterView(props) {
    const { moduleid, title, actionID } = props.location.state;
    const { showInfoPanel } = useInfoPanel();
    const classes = useStyles();
    const UserProfile = useContext(UserProfileContext);
    const tokenState = UserProfile.APIToken

    const [rowCount, setRowCount] = useState(0);
    const [moduleName, setModuleName] = useState('Module');
    const [moduleLabel, setModuleLabel] = useState('Module');
    const [redirect, setRedirect] = useState(null);
    const [actionList, setActionList] = useState([]);
    const [columns, setColumns] = useState([]);
    const [columnKey, setColumnKey] = useState('');
    const [rows, setRows] = useState([]);
    const [addAccess, setAddAccess] = useState(true);
    const [actionAdd, setActionAdd] = useState([]);
    const [supportApproval, setSupportApproval] = useState(false);
    const [isLoading, setIsLoading] = useState(true);
    const [clickedCS, setClickedCS] = useState(false);
    const [isInternal, setIsInternal] = useState(UserProfile.GroupMenuName == 'Internal');
    const [isExternal, setIsExternal] = useState(UserProfile.GroupMenuName == 'External');

    const exportTypesList = [
        { Label: 'Excel', Extension: '.xlsx' },
        { Label: 'CSV', Extension: '.csv' },
        { Label: 'Text', Extension: '.txt' }
    ];

    const getModuleData = (page, orderBy, order, search, filter, isAdvanced) => {
        const hardOrderBy = (() => {
            if (orderBy == '') {
                return isExternal ? 'TicketNo' : (isInternal ? 'FK_Ms_Severity_ID' : orderBy);
            }
            return orderBy;
        })();
        const hardOrder = (() => {
            if (order == '') {
                return isExternal ? 'DESC' : (isInternal ? 'ASC' : order);
            }
            return order;
        })();
        const hardFilter = (() => {
            if (search == '') {
                return isInternal ? `[IssueStatus] <> 'CLOSE'` : filter;
            }
            return filter;
        })();
        const hardIsAdvanced = (() => {
            if (search == '') {
                return isInternal ? `true` : isAdvanced;
            }
            return isAdvanced;
        })();
        setIsLoading(true);
        
        if (moduleid) {
            APIRequest.Send({
                method: 'GET',
                url: 'api/parameter/GetModuleData',
                params: {
                    ModuleID: moduleid,
                    page: page,
                    orderBy: hardOrderBy,
                    order: hardOrder,
                    search: search,
                    filter: hardFilter,
                    isAdv: hardIsAdvanced
                }
            }, UserProfile).then((res) => {
                if (res.status === 200) {
                    if (res.data) {
                        setRowCount(res.data.count);
                        setRows(res.data.rows.map((value) => value.Dictionary));
                    } else {
                        showInfoPanel.error('Invalid Credential');
                    }
                } else {
                    showInfoPanel.error('Invalid Credential');
                }
            }).catch((err) => {
                showInfoPanel.error(err.response ? err.response.data : err);
                setActionList([]);
                setRows([]);
            }).finally(() => setIsLoading(false));
        } else {
            showInfoPanel.error('Module ID not found');
            setIsLoading(false);
        }
    };

    const hardFilter = (object) => {
        switch (object.FieldName) {
            case 'CreatedBy':
            case 'ApprovedBy':
            case 'ApprovedDate':
                return false
            default:
                return object.IsShowInView
        }
    }
    
    const getModuleDetail = () => {
        setIsLoading(true);
        if (moduleid && tokenState) {
            APIRequest.Send({
                method: 'GET',
                url: 'api/parameter/GetModuleDetail',
                params: {
                    ModuleID: moduleid,
                    action: EActionType.View
                }
            }, UserProfile).then((res) => {
                if (res.status === 200) {
                    if (res.data) {
                        setSupportApproval(res.data.IsUseApproval);
                        setModuleName(res.data.ModuleName);
                        setModuleLabel(res.data.ModuleLabel);
                        setColumnKey(res.data.ModuleFields.find((value) => value.IsPrimaryKey).FieldName);
                        const newColumns = res.data.ModuleFields.filter((value) => value.bView).map((value) => ({
                            id: value.FieldName,
                            label: value.FieldLabel,
                            key: value.IsPrimaryKey,
                            isShow: hardFilter(value),
                            fieldType: value.FK_FieldType_ID,
                            inputType: value.FK_ExtType_ID == 1 ? EInputType.DateTimeField : value.FK_ExtType_ID,
                            tabelReferenceName: value.TabelReferenceName,
                            tableReferenceFieldKey: value.TableReferenceFieldKey,
                            tableReferenceFieldDisplayName: value.TableReferenceFieldDisplayName,
                            sequence: value.Sequence,
                            PK_ModuleField_ID: value.PK_ModuleField_ID,
                            active: value.Active,
                            bView: value.bView
                        }));
                        setColumns(newColumns);
                        setActionList(res.data.ActionList);
                        const _actionAdd = res.data.ActionList.filter((item) => item.type === EActionType.Insert);
                        setAddAccess(_actionAdd.length > 0);
                        if (_actionAdd.length === 1) {
                            setActionAdd(_actionAdd[0]);
                        }
                        getModuleData(1, '', '', '', '');
                    } else {
                        showInfoPanel.error('Invalid Credential');
                    }
                } else {
                    showInfoPanel.error('Invalid Credential');
                }
            }).catch((err) => {
                showInfoPanel.error(err.response ? err.response.data : err);
                setRedirect({
                    pathname: '/SomethingWrong',
                    state: {
                        errorMessage: err.response ? err.response.data : err
                    }
                });
            });
        } else {
            showInfoPanel.error('Module ID not found');
            setIsLoading(false);
        }
    };

    const onClickAction = (sender, record) => {
        for (let data in record) {
            if (typeof record[data] !== 'string') {
                record[data] = record[data].toString();
            }
        }
        if (sender.type === EActionType.OthersWithSP) {
            APIRequest.Send({
                method: 'POST',
                url: 'api/parameter/CustomSupportSP',
                data: {
                    Target: sender.url,
                    Parameter: sender.parameter,
                    ParameterValue: sender.parameterValue,
                    ModuleID: moduleid,
                    Data: record
                }
            }, UserProfile).then((res) => {
                if (res.status === 200) {
                    handleClicked();
                    setIsLoading(false);
                } else {
                    showInfoPanel.error('Invalid Credential');
                }
            }).catch((err) => {
                showInfoPanel.error(err.response ? err.response.data : err);
            }).finally(() => setIsLoading(false));
        } else if (sender.type === EActionType.OthersWithPage) {
            let _moduleid = moduleid;
            let type = sender.type;
            let id = record[columnKey];
            let text = sender.text;
            let url = sender.url;

            let parameters = sender.parameter.replace(/[@\s]/g, '');
            let arrParameter = parameters.split(',');
            let arrParameterValue = sender.parameterValue.replace(/\s/g, '').split(',');
            let stateRedirect = {};

            for (let i = 0; i < arrParameter.length; i++) {
                let field = columns.find((item) => item.sequence == arrParameterValue[i]);
                if (field != null) {
                    stateRedirect[arrParameter[i]] = record[field.id];

                    if (arrParameter[i].toLowerCase() === "moduleid") {
                        _moduleid = record[field.id];
                    }
                    if (arrParameter[i].toLowerCase() === "type") {
                        type = record[field.id];
                    }
                    if (arrParameter[i].toLowerCase() === "id") {
                        id = record[field.id];
                    }
                    if (arrParameter[i].toLowerCase() === "text") {
                        text = record[field.id];
                    }
                }
            }
            if (url.split("?").length > 1) {
                let urlParam = url.split("?").slice(1).join("?");
                url = url.split("?")[0];

                let arrParams = urlParam.split("&");
                for (let i = 0; i < arrParams.length; i++) {
                    let paramDetail = arrParams[i].split("=");
                    if (paramDetail.length === 2) {
                        stateRedirect[paramDetail[0]] = paramDetail[1];

                        if (paramDetail[0].toLowerCase() === "moduleid") {
                            _moduleid = paramDetail[1];
                        }
                        if (paramDetail[0].toLowerCase() === "type") {
                            type = paramDetail[1];
                        }
                        if (paramDetail[0].toLowerCase() === "id") {
                            id = paramDetail[1];
                        }
                        if (paramDetail[0].toLowerCase() === "text") {
                            text = paramDetail[1];
                        }
                    }
                }
            }

            setRedirect({
                pathname: url,
                state: {
                    type: type,
                    moduleid: _moduleid,
                    id: id,
                    text: text,
                    custom: stateRedirect
                }
            });
        } else {
            setRedirect({
                pathname: sender.url,
                state: {
                    type: sender.type,
                    moduleid: moduleid,
                    id: record[columnKey],
                    approval: supportApproval
                }
            });
        }
    };

    const onClickAdd = () => {
        setRedirect({
            pathname: actionAdd.url,
            state: {
                type: EActionType.Insert,
                moduleid: moduleid,
                approval: supportApproval
            }
        });
    };

    const onClickDownloadCurrentPage = (page, orderBy, order, search, filter, isUsingAdv, exportType, fields) => {
        if (moduleid) {
            setIsLoading(true);
            APIRequest.Send({
                url: 'api/parameter/ExportModuleData',
                method: 'POST',
                responseType: 'blob',
                data: {
                    ModuleID: moduleid,
                    page: page,
                    orderBy: orderBy,
                    order: order,
                    search: search,
                    filter: filter,
                    isAdv: isUsingAdv,
                    exportAs: exportType ? exportType.Label : '',
                    fields: fields
                }
            }, UserProfile).then((res) => {
                if (res.status === 200) {
                    if (res.data) {
                        if (exportType && exportType.Extension) {
                            FileSaver.saveAs(res.data, moduleLabel + exportType.Extension);
                        } else {
                            FileSaver.saveAs(res.data, moduleLabel + '.xlsx');
                        }
                    }
                } else {
                    showInfoPanel.error('Invalid Credential');
                }
            }).catch((err) => {
                showInfoPanel.error(err.response ? err.response.data : err);
            }).finally(() => setIsLoading(false));
        } else {
            showInfoPanel.error('Module ID not found');
        }
    };

    const onClickDownloadAll = (orderBy, order, search, filter, isUsingAdv, exportType, fields) => {
        if (moduleid) {
            setIsLoading(true);
            APIRequest.Send({
                url: 'api/parameter/ExportModuleDataAll',
                method: 'POST',
                responseType: 'blob',
                data: {
                    ModuleID: moduleid,
                    orderBy: orderBy,
                    order: order,
                    search: search,
                    filter: filter,
                    isAdv: isUsingAdv,
                    exportAs: exportType ? exportType.Label : '',
                    fields: fields
                }
            }, UserProfile).then((res) => {
                if (res.status === 200) {
                    if (res.data) {
                        if (exportType && exportType.Extension) {
                            FileSaver.saveAs(res.data, moduleLabel + exportType.Extension);
                        } else {
                            FileSaver.saveAs(res.data, moduleLabel + '.xlsx');
                        }
                    }
                } else {
                    showInfoPanel.error('Invalid Credential');
                }
            }).catch((err) => {
                showInfoPanel.error(err.response ? err.response.data : err);
            }).finally(() => setIsLoading(false));
        } else {
            showInfoPanel.error('Module ID not found');
        }
    };

    const onClickDownloadSelected = (selected, orderBy, order, exportType, fields) => {
        if (moduleid) {
            setIsLoading(true);
            APIRequest.Send({
                url: 'api/parameter/ExportModuleDataSelected',
                method: 'POST',
                data: {
                    ModuleID: moduleid,
                    SelectedData: selected,
                    orderBy: orderBy,
                    order: order,
                    exportAs: exportType ? exportType.Label : '',
                    fields: fields
                },
                responseType: 'blob'
            }, UserProfile).then((res) => {
                if (res.status === 200) {
                    if (exportType && exportType.Extension) {
                        FileSaver.saveAs(res.data, moduleLabel + exportType.Extension);
                    } else {
                        FileSaver.saveAs(res.data, moduleLabel + '.xlsx');
                    }
                } else {
                    showInfoPanel.error('Invalid Credential');
                }
            }).catch((err) => {
                showInfoPanel.error(err.response ? err.response.data : err);
            }).finally(() => setIsLoading(false));
        } else {
            showInfoPanel.error('Module ID not found');
        }
    };

    useEffect(() => {
        if (tokenState) {
            getModuleDetail();
        }
    }, [props.location.state, clickedCS]);

    // useEffect(() => {
    //     window.location.reload();
    // }, []);

    const handleClicked = () => {
        setClickedCS(!clickedCS);
    };

    return (
        redirect ?
            <Redirect to={redirect} /> :
            <Paper className={classes.root}>
                <NawaTable
                    moduleID={moduleid}
                    rowCount={rowCount}
                    moduleLabel={<Text module-view={true} name={moduleLabel} />}
                    moduleName={moduleName}
                    actionList={actionList}
                    columns={columns}
                    columnKey={columnKey}
                    rows={rows}
                    getModuleData={getModuleData}
                    onClickAction={onClickAction}
                    onClickAdd={addAccess ? onClickAdd : null}
                    onClickDownload={onClickDownloadCurrentPage}
                    onClickDownloadAll={onClickDownloadAll}
                    onClickDownloadSelected={onClickDownloadSelected}
                    isLoading={isLoading}
                    exportTypesList={exportTypesList}
                />
            </Paper>
    );
};