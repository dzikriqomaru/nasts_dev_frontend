import React, { useState, useEffect, useContext } from 'react';
import PropTypes from 'prop-types';

import {
  APIRequest,
  EActionType,
  NawaTable,
  EInputType,
  EDataType,
  UserProfileContext, useInfoPanel,
  SearchBar,
  Constraints,
  Validation,
  DialogMessage as MsgDialog,
  Loader
} from '@vikitheolorado/nawadata-web';

import {
    Card,
    CardContent,
    CardActions,
    CardHeader,
    Typography,
    Toolbar,
    Button,
    Divider,
    Grid,
    Paper,
    Container,
    ExpansionPanel,
    ExpansionPanelSummary,
    ExpansionPanelDetails,
    IconButton,
} from '@material-ui/core';
import { DropzoneDialog } from 'material-ui-dropzone';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Box from '@material-ui/core/Box';

import { KeyboardArrowLeft, ExpandMore, TrendingUpRounded } from '@material-ui/icons';
import DownloadIcon from '@material-ui/icons/CloudDownload';
import UploadIcon from '@material-ui/icons/CloudUpload';
import CheckIcon from '@material-ui/icons/CheckCircle';
import CancelIcon from '@material-ui/icons/Cancel';
import EmptyDetailIcon from '@material-ui/icons/RemoveCircle';

import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';

import FileSaver from 'file-saver';
import { set } from 'date-fns';

import Text from 'react-text';

const controllerUrl = "api/custom/";

const useStyles = makeStyles((theme) => ({
    root: {
        height: `calc(${theme.contentHeight})`,
    },
    toolbar: {
        paddingLeft: theme.spacing(2),
        paddingRight: theme.spacing(1),
    },
    title: {
        flex: '1 1 100%',
        margin: '0% 1%',
    },
    dialogPaper: {
        minHeight: '70vh',
        maxHeight: '70vh',
        minWidth: '70vh',
        maxWidth: '70vh',
        position: 'absolute',
        left: '32%',
        top: '14%',
    },
    container: {
        height: `calc(${theme.contentHeight} - 105px)`,
        overflow: 'hidden',
    },
    tableContainer: {
        height: `calc(${theme.contentHeight} - 200px)`,
        margin: '0px',
        width: '100%',
    },
    heading: {
        fontSize: theme.typography.pxToRem(20),
        fontWeight: theme.typography.fontWeightRegular,
    },
    divider: {
        width: '2%',
        height: 'auto',
        display: 'inline-block',
    },
    upperButton: {
        paddingRight: '3%',
    },
    button: {
        borderRadius: '50px',
    },
    tableButton: {
        borderRadius: '50px',
        right: '20%',
    },
    typography: {
        textAlign: 'center',
        marginTop: '45%',
        fontSize: 24,
    },
    emptyIcon: {
        width: 50,
        height: 50,
    },
    '@media screen and (max-width:1025px)': {
        divider: {
        width: '4%',
        height: 'auto',
        display: 'inline-block',
        },
        dialogPaper: {
        minHeight: '70vh',
        maxHeight: '70vh',
        minWidth: '70vh',
        maxWidth: '70vh',
        position: 'absolute',
        left: '0%',
        top: '10%',
        },
    },
    '@media screen and (max-width:675px)': {
        button: {
        padding: '2% 11%',
        },
        divider: {
        width: '30%',
        height: 'auto',
        display: 'inline-block',
        },
        dialogPaper: {
        minHeight: '70vh',
        maxHeight: '70vh',
        minWidth: '40vh',
        maxWidth: '40vh',
        position: 'absolute',
        left: '0%',
        top: '10%',
        },
        upperButton: {
        paddingRight: '7%',
        },
    },
}));

const TabPanel = (props) => {
    const { children, value, index, ...other } = props;


    return (
        <div
            role='tabpanel'
            hidden={value !== index}
            id={`simple-tabpanel=${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
        {value === index && (
            <Box>
                <Typography>{children}</Typography>
            </Box>
        )}
        </div>
    );
};

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

const tabProps = (index) => {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
};

const PopUpDialog = (props) => {
    const { onClose, open, listModuleDataDetail, validColumnKey } = props;
    const { showInfoPanel } = useInfoPanel();

    const classes = useStyles();

    const handleClose = () => {
        onClose();
    };

    const checkListModuleDataDetail = () => {
        try {
            listModuleDataDetail.map((value) => {
                if (value.ListDataDetails.length === 0) {
                    listModuleDataDetail.splice(listModuleDataDetail.indexOf(value), 1);
                }
            });
        }
        catch (err) {
            showInfoPanel.error(err);
        }
    };

    useEffect(() => {
        const ABORTCONTROLLER = new AbortController();

        checkListModuleDataDetail();

        return () =>{
            ABORTCONTROLLER.abort();
        }
    }, []);

    return (
        <Dialog
            onClose={handleClose}
            aria-labelledby='simple-dialog-title'
            open={open}
            classes={{ paper: classes.dialogPaper }}
        >
            {/* {isLoading ? <Loader /> : null} */}
            {listModuleDataDetail.length > 0 ? (
                listModuleDataDetail.map (
                    (value) => (
                        <div>
                            <NawaTable
                                moduleName={value.ModuleDetailLabel}
                                classContainer={classes.tableContainer}
                                rowCount={value.ListDataDetails.length}
                                actionList={[]}
                                columns={value.ListModuleDetailField.map((value) => {
                                    //if (value.IsPrimaryKey) setValidColumnKey(value.FieldName);
                                    return {
                                    id: value.FieldName,
                                    label: value.FieldLabel,
                                    key: value.IsPrimaryKey,
                                    isShow: value.IsShowInView,
                                    fieldType: value.FK_FieldType_ID,
                                    tableReferenceFieldDisplayName: value.TableReferenceFieldDisplayName
                                    };
                                })}
                                columnKey={validColumnKey}
                                rows={value.ListDataDetails.map((value) => value.Dictionary)}
                            />
                        </div>
                    ),
                    //setIsLoading(false)
                )
            ) : (
                <Typography className={classes.typography}>
                    List of details is empty
                    <br />
                    <br />
                    <EmptyDetailIcon className={classes.emptyIcon} />
                </Typography>
            )}
        </Dialog>
    );
};

PopUpDialog.propTypes = {
    onClose: PropTypes.func.isRequired,
    open: PropTypes.bool.isRequired,
};

export default function ParameterUpload(props) {
    const params = props.location.params;

    const ModuleID = params.find((item) => item.key === 'ModuleID').value;

    const [isLoading, setIsLoading] = useState(false);

    const classes = useStyles();
    const UserProfile = React.useContext(UserProfileContext);

    const [open, setOpen] = useState(false);

    const [moduleName, setModuleName] = React.useState('Module');

    const [validRowCount, setValidRowCount] = React.useState(0);
    const [validColumns, setValidColumns] = React.useState([]);
    const [validColumnKey, setValidColumnKey] = React.useState('');
    const [validRows, setValidRows] = React.useState([]);

    const [invalidRowCount, setInvalidRowCount] = React.useState(0);
    const [invalidColumns, setInvalidColumns] = React.useState([]);
    const [invalidColumnKey, setInvalidColumnKey] = React.useState('');
    const [invalidRows, setInvalidRows] = React.useState([]);

    const [tabValue, setTabValue] = React.useState(0);
    const [openDialogue, setOpenDialogue] = useState(false);
    const [listModuleDetail, setListModuleDetail] = React.useState([]);
    const [listModuleDataDetail, setListModuleDataDetail] = React.useState([]);
    const [maxFileSize, setMaxFileSize] = React.useState(0);
    const { showInfoPanel } = useInfoPanel();

    const openPopUp = () => {
        setOpenDialogue(true);
    };

    const closePopUp = () => {
        setOpenDialogue(false);
    };

    const GetModuleDetail = () => {
        let validateGetModule = Validation(ModuleID,
            {
                Type:Constraints.NotNull,
                errMsg: 'Module ID not found',
        
            }
        );

        if (validateGetModule.length == 0) {
            setIsLoading(true);
            APIRequest.Send(
                {
                    method: 'GET',
                    url: controllerUrl + 'GetModuleDetail?ModuleID=' + ModuleID,
                },
                UserProfile
            ).then(function (res) {
                if (res.status === 200) {
                    if (res.data) {
                        setModuleName(res.data.ModuleLabel);
                        setInvalidRowCount(0);
                        setInvalidRows([]);
                        setInvalidColumns([]);
                        setInvalidColumnKey('');
                        setValidRowCount(0);
                        setValidRows([]);
                        setValidColumns([]);
                        setValidColumnKey('');
                        setListModuleDetail(res.data.ListModuleDetail);
                    } 
                    else {
                        showInfoPanel.error('Invalid Credential');
                    }
                } 
                else {
                    showInfoPanel.error('Invalid Credential');
                }
            });
        }
        else {
            showInfoPanel.error(validateGetModule)
        }
    };

    const GetValidModuleData = (page, orderBy, order, search, filter) => {
        setIsLoading(true);
        APIRequest.Send(
            {
                method: 'GET',
                url: controllerUrl + 'GetValidModuleData',
                params: {
                    ModuleID: ModuleID,
                    page: page,
                    orderBy: orderBy,
                    order: order,
                    search: search,
                    filter: filter,
                }
            },
            UserProfile
        ).then(function (res) {
            if (res.status === 200) {
                if (res.data) {
                    setValidRowCount(res.data.count);
                    if (listModuleDetail.length > 0) {
                        res.data.rows.map((value) =>
                            (value.Dictionary.details = (
                                <Button
                                    className={classes.tableButton}
                                    onClick={() =>
                                        getModuleUploadDetail(value.Dictionary.nawa_recordnumber)
                                    }
                                    color='primary'
                                    variant='contained'
                                >
                                    details
                                </Button>
                            ))
                        );
                    }
                    setValidRows(res.data.rows.map((value) => value.Dictionary));
                    setIsLoading(false);
                    //setTabValue(0);
                }
                else {
                    showInfoPanel.error('Invalid Credential');
                }
            } else {
                showInfoPanel.error('Invalid Credential');
            }
        });
    };

    const GetValidModuleDetail = () => {
        setIsLoading(true);
        APIRequest.Send(
            {
                method: 'GET',
                url: controllerUrl + 'GetValidModuleDetail?ModuleID=' + ModuleID,
            },
            UserProfile
        )
        .then(function (res) {
            if (res.status === 200) {
                if (res.data) {
                    let newColumns = res.data.ModuleFields.map((value) => {
                        if (value.IsPrimaryKey) setValidColumnKey(value.FieldName);
                        return {
                            id: value.FieldName,
                            label: value.FieldLabel,
                            key: value.IsPrimaryKey,
                            isShow: value.IsShowInView,
                            fieldType: value.FK_FieldType_ID,
                            inputType: value.FK_ExtType_ID,
                            tabelReferenceName: value.TabelReferenceName,
                            tableReferenceFieldKey:
                                value.TableReferenceFieldKey,
                            tableReferenceFieldDisplayName:
                                value.TableReferenceFieldDisplayName,
                            sequence: value.Sequence,
                            PK_ModuleField_ID: value.PK_ModuleField_ID
                        };
                    });
                    if(listModuleDetail.length>0){
                        newColumns.push({
                            id: 'details',
                            label: 'details',
                            key: false,
                            isShow: true,
                            fieldType: EDataType.Input,
                            inputType: EInputType.TextField,
                        });
                    }
                    setValidColumns(newColumns);

                    GetValidModuleData(1, '', '', '', '');
                } 
                else {
                    showInfoPanel.error('Invalid Credential');
                }
            } 
            else {
                showInfoPanel.error('Invalid Credential');
            }
        });
    };

    const GetInvalidModuleData = (page, orderBy, order, search, filter) => {
        setIsLoading(true);
        APIRequest.Send(
        {
            method: 'GET',
            url: controllerUrl + 'GetInvalidModuleData',
            params: {
                ModuleID: ModuleID,
                page: page,
                orderBy: orderBy,
                order: order,
                search: search,
                filter: filter,
            }
        }, UserProfile)
        .then(function (res) {
            if (res.status === 200) {
                if (res.data) {
                    setInvalidRowCount(res.data.count);
                    if (listModuleDetail.length > 0) {
                        res.data.rows.map((value) =>
                            (value.Dictionary.details = (
                                <Button
                                    className={classes.tableButton}
                                    onClick={() =>
                                        getModuleUploadDetail(value.Dictionary.nawa_recordnumber)
                                    }
                                    color='primary'
                                    variant='contained'
                                >
                                    details
                                </Button>
                            ))
                        );
                    }
                    setInvalidRows(res.data.rows.map((value) => value.Dictionary));
                    if(res.data.count > 0) setTabValue(1);
                    setIsLoading(false);
                } 
                else {
                    showInfoPanel.error('Invalid Credential');
                }
            }
            else {
                showInfoPanel.error('Invalid Credential');
            }
        });
    };

    const GetInvalidModuleDetail = () => {
        APIRequest.Send(
        {
            method: 'GET',
            url: controllerUrl + 'GetInvalidModuleDetail?ModuleID=' + ModuleID,
        }, UserProfile)
        .then(function (res) {
            if (res.status === 200) {
                if (res.data) {
                    let newColumns = res.data.ModuleFields.map((value) => {
                        if (value.IsPrimaryKey) setInvalidColumnKey(value.FieldName);
                        return {
                            id: value.FieldName,
                            label: value.FieldLabel,
                            key: value.IsPrimaryKey,
                            isShow: value.IsShowInView,
                            fieldType: value.FK_FieldType_ID,
                            inputType: value.FK_ExtType_ID,
                            tabelReferenceName: value.TabelReferenceName,
                            tableReferenceFieldKey:
                                value.TableReferenceFieldKey,
                            tableReferenceFieldDisplayName:
                                value.TableReferenceFieldDisplayName,
                            sequence: value.Sequence,
                            PK_ModuleField_ID: value.PK_ModuleField_ID
                        };
                    });
                    if(listModuleDetail.length>0){
                        newColumns.push({
                            id: 'details',
                            label: 'details',
                            key: false,
                            isShow: true,
                            fieldType: EDataType.Input,
                            inputType: EInputType.TextField,
                        });
                    }
                    
                    setInvalidColumns(newColumns);

                    GetInvalidModuleData(1, '', '', '', '');
                } 
                else {
                    showInfoPanel.error('Invalid Credential');
                }
            } 
            else {
                showInfoPanel.error('Invalid Credential');
            }
        });
    };

    const handleChange = (files) => {
        setIsLoading(true);
        let data = new FormData();
        data.append('ModuleID', ModuleID);
        data.append('ActionType', EActionType.Import);
        data.append('body', files[0]);
        APIRequest.Send(
        {
            method: 'POST',
            url: controllerUrl + 'SaveParameterUpload',
            state: 'success',
            data: data,
            headers: {
            'content-type': 'multipart/form-data',
            },
        }, UserProfile)
        .then(function (res) {
            if (res.status === 200) {
                GetValidModuleDetail();
                GetInvalidModuleDetail();
            } 
            else {
                showInfoPanel.error('Invalid Credential');
            }
            setIsLoading(false);
        })
        .catch(function (res) {
            setIsLoading(false);
            if(res.response.data){
                showInfoPanel.error(res.response.data)
            }else{
                showInfoPanel.error('Upload Failed!')
            }
        });

        setOpen(false);
    };

    const handleOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const commitData = () => {
        setIsLoading(true);
        APIRequest.Send(
        {
            method: 'GET',
            url: controllerUrl + 'CommitModuleUpload?ModuleID=' + ModuleID,
        }, UserProfile)
        .then(function (res) {
            setIsLoading(false);
            if (res.status === 200) {
                if (res.data) {
                    showInfoPanel.success('file has been successfully uploaded')
                    setValidRowCount(0);
                    setValidRows([]);
                    setValidColumns([]);
                    setValidColumnKey('');
                    showInfoPanel.success('Data successfully uploaded')                    
                } 
                else {
                    showInfoPanel.error('Invalid Credential')
                }
            } 
            else {
                showInfoPanel.error('Invalid Credential')
            }
        })
        .catch(function (msg) {
            setIsLoading(false);
            showInfoPanel.error(msg.response.data)
        });
    };

    const onDownloadTemplate = () => {
        APIRequest.Send(
        {
            url: controllerUrl + 'ExportTemplateData?ModuleID=' + ModuleID,
            method: 'GET',
            responseType: 'blob',
        }, UserProfile)
        .then((response) => {
            FileSaver.saveAs(response.data, 'Data.xlsx');
        });
    };

    const exportInvalidModuleDataAll = (orderBy, order, search, filter) => {
        if (ModuleID) {
            setIsLoading(true);
            APIRequest.Send(
                {
                    method: 'GET',
                    url: controllerUrl + 'ExportInvalidModuleDataAll',
                    responseType: 'blob',
                    params: {
                        ModuleID: ModuleID,
                        orderBy: orderBy,
                        order: order,
                        search: search,
                        filter: filter,
                    }
                },UserProfile
            ).then((res) => {
                if (res.status === 200) {
                    FileSaver.saveAs(res.data, 'Data.xlsx');
                }
                else {
                    showInfoPanel.error('Invalid Credential');
                }
            }).finally(() => setIsLoading(false));
        }
        else {
            showInfoPanel.error('Module ID Not Found');
        }
    };

    const getModuleUploadDetail = (RecordID) => {
        setIsLoading(true);
        APIRequest.Send(
        {
            url:
            controllerUrl + 'GetModuleUploadDetail?ModuleID=' +
            ModuleID +
            '&RecordID=' +
            RecordID,
            method: 'GET',
        }, UserProfile)
        .then((res) => {
            setListModuleDataDetail(res.data);
            openPopUp();
        })
        .catch((err) => {
            showInfoPanel.error(err.response.data);
        });
    };

    const getMaxFileSize = () => {
        APIRequest.Send(
            {
                method: 'GET',
                url: controllerUrl + 'getSysParam',
                params: {
                    SysParamID: 8
                }
            },UserProfile
        ).then((res) => {
            if (res.status === 200) {
                setMaxFileSize(res.data.SettingValue);
            }
            else {
                showInfoPanel.error('Invalid Credential');
            }
        })
        .catch((err) => {
            showInfoPanel.error(err.response.data);
        })
    };

    const changeTabs = (event, newValue) => {
        setTabValue(newValue);
    };

    useEffect(() => {
        const ABORTCONTROLLER = new AbortController();

        GetModuleDetail();

        return () =>{
            ABORTCONTROLLER.abort();
        }
    }, [props.location.params]);

    useEffect(() => {
        getMaxFileSize();
    }, []);

    return (
        <div>
            <Paper className={classes.root}>
                {/* {isLoading ? <Loader /> : null} */}
            
                <DropzoneDialog
                    open={open}
                    onSave={handleChange}
                    showPreviews={false}
                    showPreviewsInDropzone={true}
                    showFileNames={true}
                    maxFileSize={maxFileSize}
                    onClose={handleClose}
                    filesLimit={1}
                    acceptedFiles={[
                        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                    ]}
                    clearOnUnmount={true}
                    dropzoneText={'Drag and drop an excel file here or click'}
                />
                <Toolbar className={classes.toolbar}>
                    <Typography
                        className={classes.title}
                        color='inherit'
                        variant='subtitle1'
                    >
                        {moduleName ? <Text module-upload name={moduleName} /> : null}
                    </Typography>
                    <div>
                        <Button
                            startIcon={<DownloadIcon />}
                            onClick={onDownloadTemplate}
                            variant='contained'
                            color='primary'
                            className={classes.button}
                        >
                            <Text download />
                        </Button>
                    </div>
                    <div className={classes.divider} />
                    <div className={classes.upperButton}>
                        <Button
                            startIcon={<UploadIcon />}
                            onClick={handleOpen}
                            variant='contained'
                            color='primary'
                            className={classes.button}
                        >
                            <Text upload />
                        </Button>
                    </div>
                </Toolbar>
                <Container className={classes.container}>
                    <AppBar position='static'>
                        <Tabs
                            variant='fullWidth'
                            TabIndicatorProps={{ style: { background: 'white' } }}
                            value={tabValue}
                            onChange={changeTabs}
                        >
                            <Tab icon={<CheckIcon />} label= 'valid' {...tabProps(0)} />
                            <Tab icon={<CancelIcon />} label='invalid' {...tabProps(1)} />
                        </Tabs>
                    </AppBar>
                    <TabPanel value={tabValue} index={0}>
                        {/* <Paper style={{width: 'auto', paddingBottom: '5px'}}> */}
                        {validColumns.length > 0 ? (
                                <NawaTable
                                    key={'ValidTable'}
                                    classContainer={classes.tableContainer}
                                    rowCount={validRowCount}
                                    actionList={[]}
                                    columns={validColumns}
                                    columnKey={validColumnKey}
                                    rows={validRows}
                                    getModuleData={GetValidModuleData}
                                    isLoading={isLoading}
                                />
                            ) : (
                                <Typography style={{ padding:'16px' }}>
                                    <Text no-data />
                                </Typography>
                            )
                        }
                        {/* </Paper> */}
                    </TabPanel>
                    <TabPanel value={tabValue} index={1}>
                        {/* <Paper style={{width: '100%', paddingBottom: '5px'}}> */}
                        {invalidColumns.length > 0 ? (
                                <NawaTable
                                    key={'InvalidTable'}
                                    classContainer={classes.tableContainer}
                                    rowCount={invalidRowCount}
                                    actionList={[]}
                                    columns={invalidColumns}
                                    columnKey={invalidColumnKey}
                                    rows={invalidRows}
                                    getModuleData={GetInvalidModuleData}
                                    onClickDownloadAll={exportInvalidModuleDataAll}
                                    isLoading={isLoading}
                                />
                            ) : (
                                <Typography style={{ padding:'16px' }}>
                                    <Text no-data />
                                </Typography>
                            )
                        }
                    </TabPanel>
                </Container>
                <Container
                    style={{ width: '100%', display: 'flex', justifyContent: 'center' }}
                >
                    <Button
                        onClick={commitData}
                        className={classes.button}
                        variant='contained'
                        color='primary'
                        disabled={validRowCount === 0 || invalidRowCount > 0}
                    >
                        <Text save />
                    </Button>
                    <PopUpDialog open={openDialogue} onClose={closePopUp} listModuleDataDetail={listModuleDataDetail} validColumnKey={validColumnKey} />
                </Container>
            </Paper>
        </div>
    );
}
