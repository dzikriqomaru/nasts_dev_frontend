import React, { useState, useEffect, useContext, Fragment, useRef } from 'react';
import { Redirect } from 'react-router-dom';
import moment from 'moment';
import { Link as RouterLink } from 'react-router-dom';
import ReactDOMServer from 'react-dom/server';
import useMediaQuery from '@material-ui/core/useMediaQuery';

import {
    APIRequest,
    EActionType,
    EDataType,
    EInputType,
    NawaInput,
    UserProfileContext, useInfoPanel,
    Loader,
    DialogMessage as MsgDialog,
    Constraints,
    Infopanel,
    UnsavedChanges,
    useDateFormatList,
    useTimeFormat,
    ParameterDetailMobile

} from '@vikitheolorado/nawadata-web';

import {
    colors,
    Card,
    CardContent,
    CardHeader,
    Divider,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Text from 'react-text';
import Typography from '@material-ui/core/Typography';
import KeyboardArrowRightIcon from '@material-ui/icons/KeyboardArrowRight';
import FileSaver from 'file-saver';

function LoggingAPI(userProfile, pageRouteName, functionName) {
    const isOnStart = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;
    const moduleid = arguments[4];
    const type = arguments[5];
    const id = arguments[6];
    const approvalType = arguments[7];
    const LogStatus = arguments.length > 8 && arguments[8] !== undefined ? arguments[8] : 'INFO';
    const LogDescription = arguments.length > 9 && arguments[9] !== undefined ? arguments[9] : '';

    APIRequest.Send({
        method: 'POST',
        url: 'api/LogConsoleWeb/CreateLog',
        data: {
            Log_Page: pageRouteName,
            Log_Function: functionName,
            Log_Data: JSON.stringify({ ModuleID: moduleid, ActionType: type, ApprovalType: approvalType, ID: id }),
            Log_Status: LogStatus,
            Log_Message: isOnStart ? 'Start Function' : 'End Function',
            Log_Description: LogDescription,
            CreatedDate: new Date()
        }
    }, userProfile);
}

const styles = require('@material-ui/core/styles');
const core = require('@material-ui/core');

const white = '#FFFFFF';
const black = '#000000';
const palette = {
    //  black,  white,  primary: {    contrastText: white,    dark: colors.lightBlue[900],    main: colors.lightBlue[500],    light: colors.lightBlue[100]  },
    //  secondary: {    contrastText: white,    dark: colors.blue[900],    main: colors.blue['A400'],    light: colors.blue['A400']  },  black,  white,
    black: black,
    white: white,
    primary: {
        light: '#757ce8',
        main: '#3f50b5',
        dark: '#002884',
        contrastText: '#fff'
    },
    secondary: {
        light: '#757ce8',
        main: '#b53f3f',
        dark: '#002884',
        contrastText: '#fff'
    },
    success: {
        contrastText: white,
        dark: colors.green[900],
        main: colors.green[600],
        light: colors.green[400]
    },
    info: {
        contrastText: white,
        dark: colors.blue[900],
        main: colors.blue[600],
        light: colors.blue[400]
    },
    warning: {
        contrastText: white,
        dark: colors.orange[900],
        main: colors.orange[600],
        light: colors.orange[400]
    },
    error: {
        contrastText: white,
        dark: colors.red[900],
        main: colors.red[600],
        light: colors.red[400]
    },
    text: {
        primary: colors.blueGrey[900],
        secondary: colors.blueGrey[600],
        link: colors.blue[600]
    },
    background: {
        default: '#F4F6F8',
        paper: white
    },
    icon: colors.blueGrey[600],
    divider: colors.grey[200]
};

const typography = {
    h1: {
        color: palette.text.primary,
        fontWeight: 500,
        fontSize: '35px',
        letterSpacing: '-0.24px',
        lineHeight: '40px'
    },
    h2: {
        color: palette.text.primary,
        fontWeight: 500,
        fontSize: '29px',
        letterSpacing: '-0.24px',
        lineHeight: '32px'
    },
    h3: {
        color: palette.text.primary,
        fontWeight: 500,
        fontSize: '24px',
        letterSpacing: '-0.06px',
        lineHeight: '28px'
    },
    h4: {
        color: palette.text.primary,
        fontWeight: 500,
        fontSize: '20px',
        letterSpacing: '-0.06px',
        lineHeight: '24px'
    },
    h5: {
        color: palette.text.primary,
        fontWeight: 500,
        fontSize: '16px',
        letterSpacing: '-0.05px',
        lineHeight: '20px'
    },
    h6: {
        color: palette.text.primary,
        fontWeight: 500,
        fontSize: '14px',
        letterSpacing: '-0.05px',
        lineHeight: '20px'
    },
    subtitle1: {
        color: palette.text.primary,
        fontSize: '16px',
        letterSpacing: '-0.05px',
        lineHeight: '25px'
    },
    subtitle2: {
        color: palette.text.secondary,
        fontWeight: 400,
        fontSize: '14px',
        letterSpacing: '-0.05px',
        lineHeight: '21px'
    },
    body1: {
        color: palette.text.primary,
        fontSize: '14px',
        letterSpacing: '-0.05px',
        lineHeight: '21px'
    },
    body2: {
        color: palette.text.secondary,
        fontSize: '12px',
        letterSpacing: '-0.04px',
        lineHeight: '18px'
    },
    button: {
        color: palette.text.primary,
        fontSize: '14px'
    },
    caption: {
        color: palette.text.secondary,
        fontSize: '11px',
        letterSpacing: '0.33px',
        lineHeight: '13px'
    },
    overline: {
        color: palette.text.secondary,
        fontSize: '11px',
        fontWeight: 500,
        letterSpacing: '0.33px',
        lineHeight: '13px',
        textTransform: 'uppercase'
    }
};

const MuiTableCell = {
    root: {
        ...typography.body1,
        borderBottom: '1px solid ' + palette.divider
    }
};


const MuiTableHead = {
    root: {
        backgroundColor: colors.grey[50]
    }
};

const MuiTypography = {
    gutterBottom: {
        marginBottom: 8
    }
};

const MuiButton = {
    contained: {
        boxShadow: '0 1px 1px 0 rgba(0,0,0,0.14), 0 2px 1px -1px rgba(0,0,0,0.12), 0 1px 3px 0 rgba(0,0,0,0.20)',
        backgroundColor: '#FFFFFF'
    }
};

const MuiIconButton = {
    root: {
        color: palette.icon,
        '&:hover': {
            backgroundColor: 'rgba(0, 0, 0, 0.03)'
        }
    }
};

const MuiPaper = {
    elevation1: {
        boxShadow: '0 0 0 1px rgba(63,63,68,0.05), 0 1px 3px 0 rgba(63,63,68,0.15)'
    }
};


const overrides = {
    MuiButton: MuiButton,
    MuiIconButton: MuiIconButton,
    MuiPaper: MuiPaper,
    MuiTableCell: MuiTableCell,
    MuiTableHead: MuiTableHead,
    MuiTypography: MuiTypography
};

const topHeight = '64px';

const theme = styles.createTheme({
    palette: palette,
    typography: typography,
    overrides: overrides,
    zIndex: {
        appBar: 1200
        // drawer: 1100
    },
    topbarHeight: topHeight,
    contentHeight: `calc(100vh - ${topHeight})`,
    headerPrimary: '',
    headerSecondary: ''
});

const useStyles = styles.makeStyles((theme) => ({
    root: {
        height: `calc(${theme.contentHeight})`
    },
    textField: {
        margin: theme.spacing(1),
        width: `calc(100% - 20px)`
    },
    buttonsContainer: {
        display: 'flex',
        justifyContent: 'center',
        padding: '8px',
        background: 'none'
    },
    content: {
        overflowX: 'hidden',
        height: `calc(${theme.contentHeight} - 60px)`,
        overflowY: 'auto'
    },
    headerContainer: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        height: '58px',
        '& button': {
            marginRight: '16px'
        }
    },
    errorBox: {
        backgroundColor: '#ffbbbb',
        border: '2px solid #808080',
        borderRadius: '5px',
        margin: '10px 0px'
    },
    errorHeader: {
        padding: '10px',
        fontWeight: 'bold'
    },
    errorList: {
        color: 'black'
    },
    mobileHeader: {
        display: 'none'
    },
    '@media screen and (max-width: 600px)': {
        mobileHeader: {
            display: 'flex',
            flexDirection: 'column',
            paddingTop: theme.spacing(1),
            paddingLeft: theme.spacing(2)
        },
        backMenu: {
            alignItems: 'center'
        },
        titleDesc: {
            paddingTop: theme.spacing(1),
            fontSize: '13px',
            fontColor: '#8D9DB9'
        },
        content: {
            overflowX: 'hidden'
        },
        saveButton: {
            height: '35px',
            marginTop: '2%',
            position: 'relative',
            boxShadow: 'none',
            width: '100%'
        },
        cancelButton: {
            height: '35px',
            marginTop: '2%',
            position: 'relative',
            border: '2px solid #8D9DB9',
            boxShadow: 'none',
            color: '#465672',
            width: '100%'
        }
    },
    '@media screen and (max-width: 1024px) and (min-width: 601px)': {
        content: {
            backgroundColor: '#F2F4F7'
        },
        tabletInputView: {
            width: '90%',
            margin: 'auto',
            backgroundColor: '#FFFFFF'
        }
    }
}));

export default function ParameterInput(props) {
    const classes = useStyles();
    const { moduleid } = props.location.state;
    const type = 2;
    const id = 1;

    const UserProfile = useContext(UserProfileContext);

    const [isLoading, setIsLoading] = useState(true);
    const [isTableLoading, setIsTableLoading] = useState(true);
    const [moduleLabel, setModuleLabel] = useState('Module');
    const [moduleDbName, setModuleDbName] = useState('Module');
    const [userHasWorkflow, setUserHasWorkflow] = useState(false);
    const [review, setReview] = useState('');
    const [fields, setFields] = useState([]);
    const [columns, setColumns] = useState([]);
    const [detailColumns, setDetailColumns] = useState([]);
    const [columnKey, setColumnKey] = useState(['']);
    const [data, setData] = useState(null);
    const [redirect, setRedirect] = useState(null);
    const [focusOnID, setFocusOnID] = useState(null);
    const [errorList, setErrorList] = useState([]);
    const [listModuleDetail, setListModuleDetail] = useState([]);
    const [sendToggle, setSendToggle] = useState(false);
    const [dataDetail, setDataDetail] = useState([]);
    const [dataInitialized, setDataInitialized] = useState(false);
    const [isDataChanged, setIsDataChanged] = useState(false);
    const [isPrint, setIsPrint] = useState(false);
    const [showDialog, setShowDialog] = useState(false);
    const [paramRedirect, setParamRedirect] = useState('');
    const [isDiscard, setIsDiscard] = useState(false);
    const [approval, setApproval] = useState(false);
    const [urlView, setUrlView] = useState('');
    const [showConfirm, setShowConfirm] = useState(false);
    const [moduleName, setModuleName] = useState('Module');


    const [rowCount, setRowCount] = useState(0);
    const [rows, setRows] = useState([]);

    const isMobile = useMediaQuery('(max-width:600px)');
    const tabletMin = useMediaQuery('(min-width:601px)');
    const tabletMax = useMediaQuery('(max-width:1024px)');
    const isTablet = tabletMin && tabletMax;

    const _useInfoPanel = useInfoPanel(),
        showInfoPanel = _useInfoPanel.showInfoPanel;

    const dateFormatIndex = useDateFormatList().map(function (x) {
        return x.PK_SystemParameter_ID;
    }).indexOf(6);
    const dateFormatList = useDateFormatList()[dateFormatIndex];
    const timeFormatList = useTimeFormat()[0];

    const dateFormat = dateFormatList ? dateFormatList.MomentJSFormat : '';
    const timeFormat = timeFormatList ? timeFormatList.MomentJSFormat : '';

    const onBlock = (tx) => {
        if (isDataChanged && !showDialog) {
            setShowDialog(true);
            setParamRedirect(tx);
        }
        if (!isDataChanged || showDialog) {
            props.history.block(onBlock);
            return true;
        }
        return false;
    };

    const showPrompt = () => {
        UserProfile.ValidateIfNoChange == 1 && isDataChanged === false ? showInfoPanel.error('Please make changes before saving') : setShowConfirm(true);
        setShowDialog(false);
    };

    useEffect(function () {
        props.history.block(onBlock);

        if (isDiscard) {
            if (!isDataChanged) {
                if (paramRedirect) {
                    setRedirect({
                        pathname: paramRedirect.pathname,
                        params: paramRedirect.params,
                        state: paramRedirect.state
                    });
                } else {
                    setRedirect({
                        pathname: urlView,
                        state: {
                            moduleid: moduleid
                        }
                    });
                }
            }
        }

        return function () {
            props.history.block(true);
        };
    }, [isDataChanged]);

    const handleCancel = function handleCancel(state) {
        // setShowConfirm(state)
        setShowDialog(state);
        setParamRedirect('');
    };

    const GetModuleData = function GetModuleData() {
        return APIRequest.Send({
            method: 'GET',
            url: 'api/parameter/GetModuleDataByID',
            params: {
                ModuleID: moduleid,
                ID: id,
                actionType: type
            }
        }, UserProfile);
    };

    const GetModuleDetail = function GetModuleDetail() {
        APIRequest.Send({
            method: 'GET',
            url: 'api/parameter/GetModuleDetail?ModuleID=' + moduleid + (type === EActionType.Update || type === EActionType.Detail ? '&ID=' + id : '')
        }, UserProfile).then(function (res) {
            if (res.status === 200) {
                if (res.data) {
                    setUrlView(res.data.UrlView);
                    setModuleLabel(res.data.ModuleLabel);
                    setModuleDbName(res.data.ModuleName);
                    setModuleName(res.data.ModuleName);
                    setFields(res.data.ModuleFields);
                    setUserHasWorkflow(res.data.UserHasWorkflow);
                    setApproval(res.data.IsUseApproval);

                    const newData = {};
                    const newColumns = res.data.ModuleFields.map(function (value) {
                        if (value.IsPrimaryKey) {
                            setColumnKey(value.FieldName);
                        }
                        newData[value.FieldName] = value.DefaultValue || '';
                        return value;
                    });
                    setColumns(newColumns);
                    if (type != EActionType.Update) setData(newData);
                    setDataInitialized(true);
                    if (type == EActionType.Update || type === EActionType.Detail) {
                        setIsLoading(true);
                        GetModuleData().then(function (res) {
                            if (res.status === 200) {
                                if (res.data) {
                                    if (res.data.validation) {
                                        if (res.data.validation.length > 0) {
                                            const newErrorList = res.data.validation.map(function (item) {
                                                return {
                                                    PK_ModuleField_ID: item.Key,
                                                    message: item.Value
                                                };
                                            });
                                            setErrorList(newErrorList);
                                        }
                                    }
                                    if (res.data.Dictionary) {
                                        const editData = res.data.Dictionary;
                                        Object.keys(newData).forEach(function (key) {
                                            if (editData[key] === undefined && editData[key + 'Name'] === undefined) {
                                                newData[key] = editData[key];
                                                return;
                                            }
                                            if (newColumns.find(function (value) {
                                                return value.FieldName === key;
                                            }).FK_ExtType_ID == EInputType.DateField) {
                                                newData[key] = editData[key] ? new Date(editData[key]) : '';
                                            } else if (newColumns.find(function (value) {
                                                return value.FieldName === key;
                                            }).FK_FieldType_ID == EDataType.BOOLEAN) {
                                                newData[key] = typeof editData[key] == 'boolean' ? editData[key] == 1 ? 'true' : 'false' : '';
                                            } else if (newColumns.find(function (value) {
                                                return value.FieldName === key;
                                            }).FK_FieldType_ID == EDataType.VARBINARY) {
                                                newData[key] = {
                                                    fileName: editData[key + 'Name'],
                                                    fieldName: key,
                                                    id: id,
                                                    moduleid: moduleid
                                                };
                                            } else if (newColumns.find(function (value) {
                                                return value.FieldName === key;
                                            }).FK_ExtType_ID == EInputType.NumberField) {
                                                const hasHypen = false;
                                                const b = String(editData[key]).split('.');
                                                if (b[1]) {
                                                    b[1] = b[1].replaceAll('-', '');
                                                }
                                                const d = b[0].split('');
                                                if (d[0] === '-') {
                                                    hasHypen = true;
                                                }
                                                b[0] = b[0].replaceAll('-', '');
                                                const c = '';
                                                if (UserProfile.DecimalSeparator === ',') {
                                                    b[0] = b[0].replace(/\B(?=(\d{3})+(?!\d))/g, '.');
                                                    c = b.join(',');
                                                } else {
                                                    b[0] = b[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                                                    c = b.join('.');
                                                }
                                                hasHypen ? newData[key] = '-' + c : newData[key] = c;
                                            } else {
                                                newData[key] = editData[key];
                                            }
                                        });
                                        setData({ ...newData });
                                    }
                                }
                            }
                        }).catch(function (err) {
                            showInfoPanel.error(err.response ? err.response.data : err).finally(function () {
                                return setIsLoading(false);
                            });
                        });
                    }

                    //#region Module Detail
                    if (res.data.ListModuleDetail && res.data.ListModuleDetail.length > 0) {
                        const newModuleDetails = res.data.ListModuleDetail.map(function (value) {
                            return {
                                moduleDetailID: value.PK_ModuleDetail_ID
                            };
                        });
                        setListModuleDetail(newModuleDetails);
                    }
                    //#endregion
                } else {
                    showInfoPanel.error('Invalid Credential');
                }
            } else {
                showInfoPanel.error('Invalid Credential');
            }
        }).catch(function (err) {
            return showInfoPanel.error(err.response ? err.response.data : err);
        }).finally(function () {
            return setIsLoading(false);
        });
    };


    const onChangeData = function onChangeData(fieldName, value, firstValue, extType) {
        if (dataInitialized) {
            if (type == 2 && value !== firstValue || type === 1) {
                setIsDataChanged(true);
            }
            const newData = data;
            newData[fieldName] = value;
            setData({ ...newData });
        }
    };

    function isValidInput(value) {
        return value !== '' && value !== null && value !== undefined;
    }

    const validateData = function validateData() {
        let errStatus = false;
        const newErrorList = [];

        fields.forEach(function (item) {
            const value = data[item.FieldName];
            if (value === undefined) return;
            switch (item.FK_ExtType_ID) {
                case EInputType.DateField:
                    const validDate = moment(new Date(value), dateFormat);
                    if (!value && item.Required) {
                        errStatus = true;
                        newErrorList.push({
                            message: item.FieldLabel + ' is required',
                            PK_ModuleField_ID: item.PK_ModuleField_ID
                        });
                    } else if (value && !validDate.isValid()) {
                        errStatus = true;
                        newErrorList.push({
                            message: item.FieldLabel + ' Invalid date format. Please enter the date in the format "DD/MM/YYYY".',
                            PK_ModuleField_ID: item.PK_ModuleField_ID
                        });
                    }
                    break;
                case EInputType.TimeField:
                    const validTime = moment(new Date(value), timeFormat);
                    if (!value && item.Required) {
                        errStatus = true;
                        newErrorList.push({
                            message: item.FieldLabel + ' is required',
                            PK_ModuleField_ID: item.PK_ModuleField_ID
                        });
                    } else if (value && !validTime.isValid()) {
                        errStatus = true;
                        newErrorList.push({
                            message: item.FieldLabel + ' Invalid Time format. Please enter the time in the format "hh(hour)/mm(minutes)/a(AM or PM).',
                            PK_ModuleField_ID: item.PK_ModuleField_ID
                        });
                    }
                    break;
                case EInputType.TextField:
                case EInputType.PasswordField:
                case EInputType.QueryField:
                    if (value && item.SizeField > 0 && value.length > item.SizeField) {
                        errStatus = true;
                        newErrorList.push({
                            message: item.FieldLabel + ' is limited to ' + item.SizeField + ' characters',
                            PK_ModuleField_ID: item.PK_ModuleField_ID
                        });
                    } else if (!value && item.Required && item.FK_FieldType_ID !== EDataType.IDENTITY && item.FK_FieldType_ID !== EDataType.BIGIDENTITY) {
                        errStatus = true;
                        newErrorList.push({
                            message: item.FieldLabel + ' is required',
                            PK_ModuleField_ID: item.PK_ModuleField_ID
                        });
                    } else if (item.ListRegex) {
                        const regexError = false;
                        item.ListRegex.forEach(function (itemRegex) {
                            const reg = new RegExp(itemRegex.Regex);
                            if (!reg.test(value)) {
                                regexError = true;
                            }
                        });
                        if (regexError) {
                            errStatus = true;
                            newErrorList.push({
                                message: item.FieldLabel + ' input format is invalid',
                                PK_ModuleField_ID: item.PK_ModuleField_ID
                            });
                        }
                    }
                    break;
                case EInputType.RichText:
                    if ((!value || value === '<p><br></p>') && item.Required) {
                        errStatus = true;
                        newErrorList.push({
                            message: item.FieldLabel + ' ' + document.getElementById('require').innerHTML,
                            PK_ModuleField_ID: item.PK_ModuleField_ID
                        });
                    } else if (value && item.SizeField > 0 && value.length > item.SizeField) {
                        errStatus = true;
                        newErrorList.push({
                            message: item.FieldLabel + ' is limited to ' + item.SizeField + ' characters',
                            PK_ModuleField_ID: item.PK_ModuleField_ID
                        });
                    }
                    break;
                case EInputType.NumberField:
                    const regex = '';
                    if (value === '-' || value === '.' || value === ',' || value === '-.' || value === '-,' || value[value.length - 1] === '.' || value[value.length - 1] === ',') {
                        errStatus = true;
                        newErrorList.push({
                            message: item.FieldLabel + ' Invalid Format for Numberfield',
                            PK_ModuleField_ID: item.PK_ModuleField_ID
                        });
                    } else if (item.FK_FieldType_ID === EDataType.INT || item.FK_FieldType_ID === EDataType.BIGINT || item.FK_FieldType_ID === EDataType.SMALLINT || item.FK_FieldType_ID === EDataType.TINYINT) {
                        if (UserProfile.DecimalSeparator === '.') {
                            regex = /[^0-9-,]/gi;
                        } else {
                            regex = /[^0-9-.]/gi;
                        }
                        if (regex.test(value)) {
                            errStatus = true;
                            newErrorList.push({
                                message: item.FieldLabel + ' Invalid Format for Numberfield'
                            });
                        }
                    } else if (item.FK_FieldType_ID === EDataType.FLOAT || item.FK_FieldType_ID === EDataType.MONEY || item.FK_FieldType_ID === EDataType.NUMERIC_DECIMAL || item.FK_FieldType_ID === EDataType.REAL) {
                        regex = /[^0-9-,.]/gi;
                        if (regex.test(value)) {
                            errStatus = true;
                            newErrorList.push({
                                message: item.FieldLabel + ' Invalid Format for Numberfield'
                            });
                        }
                    }

                    const val = '';
                    const splitVal = '';
                    if (value) {
                        if (UserProfile.DecimalSeparator === ',') {
                            val = value.replaceAll('.', '').replace(',', '.');
                        } else {
                            val = value.replaceAll(',', '');
                        }
                    } else {
                        val = '';
                    }
                    splitVal = val.replace('-', '').split('.');

                    if (item.FK_FieldType_ID == EDataType.BIGINT && val < Number.MIN_SAFE_INTEGER || item.FK_FieldType_ID == EDataType.BIGINT && val > Number.MAX_SAFE_INTEGER) {
                        errStatus = true;
                        newErrorList.push({
                            message: item.FieldLabel + ' Reach BIGINT Limit from -9,007,199,254,740,991 to 9,007,199,254,740,991',
                            PK_ModuleField_ID: item.PK_ModuleField_ID
                        });
                    } else if (item.FK_FieldType_ID == EDataType.INT && val < -2147483648 || item.FK_FieldType_ID == EDataType.INT && val > 2147483647) {
                        errStatus = true;
                        newErrorList.push({
                            message: item.FieldLabel + ' Reach INT Limit from -2,147,483,648 to 2,147,483,647',
                            PK_ModuleField_ID: item.PK_ModuleField_ID
                        });
                    } else if (item.FK_FieldType_ID == EDataType.SMALLINT && val < -32768 || item.FK_FieldType_ID == EDataType.SMALLINT && val > 32767) {
                        errStatus = true;
                        newErrorList.push({
                            message: item.FieldLabel + ' Reach SMALLINT Limit from -32,768 to 32,767',
                            PK_ModuleField_ID: item.PK_ModuleField_ID
                        });
                    } else if (item.FK_FieldType_ID == EDataType.TINYINT && val < -0 || item.FK_FieldType_ID == EDataType.TINYINT && val > 255) {
                        errStatus = true;
                        newErrorList.push({
                            message: item.FieldLabel + ' Reach TINYINT Limit from 0 to 255',
                            PK_ModuleField_ID: item.PK_ModuleField_ID
                        });
                    } else if (item.FK_FieldType_ID == EDataType.MONEY && val < -922337203685477 || item.FK_FieldType_ID == EDataType.MONEY && val > 922337203685477) {
                        errStatus = true;
                        newErrorList.push({
                            message: item.FieldLabel + ' Reach MONEY Limit from -922337203685477 to 922337203685477',
                            PK_ModuleField_ID: item.PK_ModuleField_ID
                        });
                    } else if ((item.FK_FieldType_ID == EDataType.REAL || item.FK_FieldType_ID == EDataType.FLOAT || item.FK_FieldType_ID == EDataType.NUMERIC_DECIMAL) && val < Number.MIN_SAFE_INTEGER || (item.FK_FieldType_ID == EDataType.REAL || item.FK_FieldType_ID == EDataType.FLOAT || item.FK_FieldType_ID == EDataType.NUMERIC_DECIMAL) && val > Number.MAX_SAFE_INTEGER) {
                        errStatus = true;
                        newErrorList.push({
                            message: item.FieldLabel + ' Reach Limit from Javascript MIN_SAFE_INTEGER to Javascript MAX_SAFE_INTEGER',
                            PK_ModuleField_ID: item.PK_ModuleField_ID
                        });
                    } else if ((item.FK_FieldType_ID === EDataType.MONEY || item.FK_FieldType_ID === EDataType.FLOAT || item.FK_FieldType_ID === EDataType.REAL) && splitVal[1] && item.SizeField > 0 && splitVal[1].length > item.SizeField) {
                        errStatus = true;
                        newErrorList.push({
                            message: item.FieldLabel + ' is limited to ' + item.SizeField + ' characters',
                            PK_ModuleField_ID: item.PK_ModuleField_ID
                        });
                    }
                    break;
                default:
                    if (item.Required && item.FK_FieldType_ID !== EDataType.IDENTITY && item.FK_FieldType_ID !== EDataType.BIGIDENTITY && !isValidInput(value)) {
                        errStatus = true;
                        newErrorList.push({
                            message: item.FieldLabel + ' is required',
                            PK_ModuleField_ID: item.PK_ModuleField_ID
                        });
                    }
                    break;
            }
        });

        setErrorList(newErrorList);
        return errStatus;
    };

    const convertDate = function convertDate(date) {
        const monthTemp = date.getMonth() > 8 ? date.getMonth() + 1 : '0' + (date.getMonth() + 1);
        const dateTemp = date.getDate() > 9 ? date.getDate() : '0' + date.getDate();
        return monthTemp + '/' + dateTemp + '/' + date.getFullYear();
    };

    const onSave = function onSave() {
        LoggingAPI(UserProfile, '/parameterinput', 'onSave', true, moduleid, type, id);
        setShowConfirm(false);
        setIsDataChanged(false);
        setShowDialog(false);
        const arr = {};
        setDataDetail([]);
        setSendToggle(!sendToggle);
        Object.entries(data).forEach(([key, value]) => {
            if (value === undefined) return;

            const field = columns.find((item) => item.FieldName === key);
            const { FK_FieldType_ID, FK_ExtType_ID } = field;

            if (FK_FieldType_ID === EDataType.DATE || FK_FieldType_ID === EDataType.DATETIME) {
                if (FK_ExtType_ID === EInputType.DateField) {
                    arr[key] = value ? convertDate(new Date(value)) : '';
                } else {
                    arr[key] = value ? value.toLocaleString('en-US') : '';
                }
            } else if (
                FK_ExtType_ID === EInputType.FileUpload ||
                (FK_ExtType_ID === EInputType.DisplayField && FK_FieldType_ID === EDataType.VARBINARY)
            ) {
                arr[key] = value ? (value.fileData ? String(value.fileData) : '') : '';
                const columnFileName = key + 'Name';
                arr[columnFileName] = value ? (value.fileName ? String(value.fileName) : '') : '';
            } else if (
                FK_ExtType_ID === EInputType.DropDownField ||
                FK_ExtType_ID === EInputType.PopUpCombobox ||
                FK_ExtType_ID === EInputType.PopUpMultiCheckBox
            ) {
                arr[key] = isValidInput(value) ? String(value) : null;
            } else if (FK_ExtType_ID === EInputType.NumberField) {
                if (value) {
                    if (UserProfile.DecimalSeparator === ',') {
                        const a = value.replaceAll('.', '').replace(',', '.');
                        arr[key] = value ? String(a) : null;
                    } else {
                        const a = value.replaceAll(',', '');
                        arr[key] = value ? String(a) : null;
                    }
                } else {
                    arr[key] = String(value);
                }
            } else {
                arr[key] = String(value);
            }
        });
        if (validateData()) {
            showInfoPanel.error('There are some problems with your input(s). Please check error list on the top of page for more detail.');
            setShowConfirm(false);
            setIsDataChanged(true);
        } else {
            setIsLoading(true);
            APIRequest.Send({
                method: 'POST',
                url: 'api/parameter/SaveParameterInput',
                data: {
                    ModuleID: moduleid,
                    ActionType: type,
                    data: arr,
                    dataDetail: dataDetail,
                    Review: review
                }
            }, UserProfile).then(function (res) {
                if (res.status === 200) {
                    showInfoPanel.success('Data Downloaded');
                    downloadAll()
                } else {
                    setShowConfirm(false);
                    setIsDataChanged(true);
                    showInfoPanel.error('Invalid Credential');
                }
            }).catch(function (err) {
                setShowConfirm(false);
                setIsDataChanged(true);

                if (err.response && err.response.status == 400) {
                    const newErrorList = err.response.data.map(function (item) {
                        return {
                            PK_ModuleField_ID: item.Key,
                            message: item.Value
                        };
                    });
                    setErrorList(newErrorList);
                    showInfoPanel.error('There are some problems with your input(s). Please check error list on the top of page for more detail.');
                } else {
                    showInfoPanel.error(err.response ? err.response.data : err);
                    handleDiscard()
                }
            }).finally(function () {
                return setIsLoading(false);
            });
        }
    };

    const handleDiscard = () => {
        setIsDataChanged(false);
        setIsDiscard(true);
    };

    useEffect(function () {
        GetModuleDetail();
    }, []);

    const downloadAll = () => {
        APIRequest.Send({
            url: 'api/parameter/ExportModuleDetailDataAll',
            method: 'POST',
            responseType: 'blob',
            data: {
                moduleDetailID: 25696,
                orderBy: "",
                order: "",
                search: "",
                filter: "",
                isAdv: false,
                exportAs: "Excel",
                fields: [{"FieldName":"TicketNo","FieldLabel":"Ticket","IsPrimaryKey":true,"IsShowInView":true,"FK_FieldType_ID":15,"FK_ExtType_ID":7,"TabelReferenceName":"","TableReferenceFieldKey":"","TableReferenceFieldDisplayName":"","Sequence":1,"PK_ModuleField_ID":768052},{"FieldName":"IssueStatus","FieldLabel":"Issue Status","IsPrimaryKey":false,"IsShowInView":true,"FK_FieldType_ID":9,"FK_ExtType_ID":5,"TabelReferenceName":"","TableReferenceFieldKey":"","TableReferenceFieldDisplayName":"","Sequence":2,"PK_ModuleField_ID":768053},{"FieldName":"RaiseDate","FieldLabel":"Raise Date","IsPrimaryKey":false,"IsShowInView":true,"FK_FieldType_ID":10,"FK_ExtType_ID":1,"TabelReferenceName":"","TableReferenceFieldKey":"","TableReferenceFieldDisplayName":"","Sequence":3,"PK_ModuleField_ID":768054},{"FieldName":"ClientCode","FieldLabel":"Client Code","IsPrimaryKey":false,"IsShowInView":true,"FK_FieldType_ID":9,"FK_ExtType_ID":5,"TabelReferenceName":"","TableReferenceFieldKey":"","TableReferenceFieldDisplayName":"","Sequence":4,"PK_ModuleField_ID":768055},{"FieldName":"Product_Code","FieldLabel":"Product Code","IsPrimaryKey":false,"IsShowInView":true,"FK_FieldType_ID":9,"FK_ExtType_ID":5,"TabelReferenceName":"","TableReferenceFieldKey":"","TableReferenceFieldDisplayName":"","Sequence":5,"PK_ModuleField_ID":768056},{"FieldName":"SubProduct_Code","FieldLabel":"Sub Product Code","IsPrimaryKey":false,"IsShowInView":true,"FK_FieldType_ID":9,"FK_ExtType_ID":5,"TabelReferenceName":"","TableReferenceFieldKey":"","TableReferenceFieldDisplayName":"","Sequence":6,"PK_ModuleField_ID":768057},{"FieldName":"Severity_Name","FieldLabel":"Severity","IsPrimaryKey":false,"IsShowInView":true,"FK_FieldType_ID":9,"FK_ExtType_ID":5,"TabelReferenceName":"","TableReferenceFieldKey":"","TableReferenceFieldDisplayName":"","Sequence":7,"PK_ModuleField_ID":768058},{"FieldName":"CategoryName","FieldLabel":"Category","IsPrimaryKey":false,"IsShowInView":true,"FK_FieldType_ID":9,"FK_ExtType_ID":5,"TabelReferenceName":"","TableReferenceFieldKey":"","TableReferenceFieldDisplayName":"","Sequence":8,"PK_ModuleField_ID":768059},{"FieldName":"IssueTitle","FieldLabel":"Issue Title","IsPrimaryKey":false,"IsShowInView":true,"FK_FieldType_ID":9,"FK_ExtType_ID":5,"TabelReferenceName":"","TableReferenceFieldKey":"","TableReferenceFieldDisplayName":"","Sequence":9,"PK_ModuleField_ID":768060},{"FieldName":"RootCause","FieldLabel":"Root Cause","IsPrimaryKey":false,"IsShowInView":true,"FK_FieldType_ID":9,"FK_ExtType_ID":5,"TabelReferenceName":"","TableReferenceFieldKey":"","TableReferenceFieldDisplayName":"","Sequence":10,"PK_ModuleField_ID":768061},{"FieldName":"Solution","FieldLabel":"Solution","IsPrimaryKey":false,"IsShowInView":true,"FK_FieldType_ID":9,"FK_ExtType_ID":5,"TabelReferenceName":"","TableReferenceFieldKey":"","TableReferenceFieldDisplayName":"","Sequence":11,"PK_ModuleField_ID":768062},{"FieldName":"RatingName","FieldLabel":"Rating","IsPrimaryKey":false,"IsShowInView":true,"FK_FieldType_ID":9,"FK_ExtType_ID":5,"TabelReferenceName":"","TableReferenceFieldKey":"","TableReferenceFieldDisplayName":"","Sequence":12,"PK_ModuleField_ID":768063},{"FieldName":"SolveDate","FieldLabel":"Solve Date","IsPrimaryKey":false,"IsShowInView":true,"FK_FieldType_ID":10,"FK_ExtType_ID":1,"TabelReferenceName":"","TableReferenceFieldKey":"","TableReferenceFieldDisplayName":"","Sequence":13,"PK_ModuleField_ID":768064}],
                parentID: 1
            }
        }, UserProfile).then(function (res) {
            if (res.status === 200) {
                if (res.data) {
                    FileSaver.saveAs(res.data, 'Monthly Issue Summary ' + `${convertDate(data['FromDate'])} - ${convertDate(data['ToDate'])}` + '.xlsx')
                }
            }
        }).catch(function (err) {
            setShowConfirm(false);
            setIsDataChanged(true);

            if (err.response && err.response.status == 400) {
                const newErrorList = err.response.data.map(function (item) {
                    return {
                        PK_ModuleField_ID: item.Key,
                        message: item.Value
                    };
                });
                setErrorList(newErrorList);
                showInfoPanel.error('There are some problems with your input(s). Please check error list on the top of page for more detail.');
            } else {
                showInfoPanel.error(err.response ? err.response.data : err);
            }
        })
    }

    if (redirect) {
        return <Redirect to={redirect} />;
    }

    return (
        <Card className={classes.root}>
            {isLoading ? <Loader /> : null}
            <div style={{ display: 'none' }}>
                <div id="require">
                    <Text validate-required />
                </div>
            </div>
            <div className={classes.mobileHeader}>
                <div className={classes.titleDesc}>Add New Record</div>
            </div>
            <div className={classes.headerContainer}>
                <CardHeader
                    titleTypographyProps={{ variant: 'h4' }}
                    title={moduleLabel}
                    style={{ minHeight: '56px' }}
                />
                <div className={classes.buttonsContainer}>
                    <core.Button
                        variant="contained"
                        size="small"
                        color="primary"
                        onClick={onSave}
                    >
                        Print
                    </core.Button>
                </div>
            </div>
            <Divider />
            <CardContent className={classes.content}>
                <div className={classes.tabletInputView}>
                    {errorList.length > 0 ? (
                        <div className={classes.errorBox}>
                            <Typography variant="h5" className={classes.errorHeader}>
                                <Text validate-err-title />
                            </Typography>
                            <List dense>
                                {errorList.map((item) => (
                                    <ListItem
                                        key={item.PK_ModuleField_ID}
                                        button
                                        onClick={() => setFocusOnID(item.PK_ModuleField_ID)}
                                    >
                                        <ListItemIcon>
                                            <KeyboardArrowRightIcon style={{ color: 'black' }} />
                                        </ListItemIcon>
                                        <ListItemText
                                            primary={
                                                <Typography variant="h6" style={{ color: 'black' }}>
                                                    {item.message}
                                                </Typography>
                                            }
                                        />
                                    </ListItem>
                                ))}
                            </List>
                        </div>
                    ) : (null)}
                    <Fragment>
                        <div className={classes.tabletInputView}>
                            {columns
                                .filter(
                                    (val) =>
                                        val.IsShowInForm &&
                                        (type === EActionType.Insert ? val.bAdd : val.bEdit)
                                )
                                .map((item, index) => (
                                    <NawaInput
                                        key={index}
                                        name={item.FieldLabel}
                                        type={item.FK_ExtType_ID}
                                        required={item.Required}
                                        IsPrimaryKey={item.IsPrimaryKey}
                                        value={data ? data[item.FieldName] : null}
                                        ParentValue={
                                            item.BCasCade ? (data ? data[item.FieldNameParent] : null) : ''
                                        }
                                        dataType={item.FK_FieldType_ID}
                                        PK_ModuleField_ID={item.PK_ModuleField_ID}
                                        onChange={(value) =>
                                            onChangeData(
                                                item.FieldName,
                                                value,
                                                data ? data[item.FieldName] : null,
                                                item.FK_ExtType_ID
                                            )
                                        }
                                        sizeField={
                                            item.SizeField == null || item.SizeField == undefined
                                                ? 0
                                                : item.SizeField
                                        }
                                        listRegex={item.ListRegex}
                                        isFocus={item.PK_ModuleField_ID === focusOnID}
                                        setFocus={(value) => setFocusOnID(value)}
                                        actionType={type}
                                        moduleName={moduleDbName}
                                        fieldName={item.FieldName}
                                        isDataChanged={isDataChanged}
                                        bCascade={item.BCasCade}
                                    />
                                ))}
                        </div>
                    </Fragment>
                </div>
            </CardContent>
        </Card>
    );
}