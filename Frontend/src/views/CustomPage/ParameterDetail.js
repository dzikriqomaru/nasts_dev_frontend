import React, { useState, useEffect, useContext } from 'react';

import { Redirect } from 'react-router-dom';
import useMediaQuery from '@material-ui/core/useMediaQuery';

import {
	APIRequest,
	EActionType,
	EDataType,
	EInputType,
	NawaInput,
	UserProfileContext,
	useInfoPanel,
	Loader
} from '@vikitheolorado/nawadata-web';

import Text from 'react-text';
import {
	Card,
	CardContent,
	CardActions,
	CardHeader,
	Typography,
	Button,
	Divider,
	Box,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import { KeyboardArrowLeft } from '@material-ui/icons';

const controllerUrl = "api/custom/";

const useStyles = makeStyles((theme) => ({
	root: {
		height: `calc(${theme.contentHeight})`,
	},
	content: {
		height: `calc(${theme.contentHeight} - 103px)`,
		overflow: 'auto',
		padding: '5px',
	},
	contentGrid: {
		height: `calc(${theme.contentHeight} - 103px)`,
		oveflow: 'auto',
	},
	'@media(minWidth: 675)':{
		
	}
}));

export default function ParameterDetail(props) {
	const { type, moduleid, id } = props.location.state;
	const UserProfile = useContext(UserProfileContext);
	const [moduleName, setModuleName] = useState('Module');
	const [columns, setColumns] = useState([]);
	const [columnKey, setColumnKey] = useState(['']);
	const [data, setData] = useState({});
	const [redirect, setRedirect] = useState(null);
	const [listModuleDetail, setListModuleDetail] = useState([]);
    const [approval, setApproval] = useState(false);
    const [urlView, setUrlView] = useState('')
	const [isLoading, setIsLoading] = useState(true);

	const isDesktop = useMediaQuery('(min-width:1025px)');

	const classes = useStyles();

    const { showInfoPanel } = useInfoPanel();

	const GetModuleData = () => {
		return APIRequest.Send(
			{
				method: 'GET',
				url: controllerUrl + 'GetModuleDataByID?ModuleID=' + moduleid + '&ID=' + id,
			},
			UserProfile
		);
	};

	const GetModuleDetail = () => {
		APIRequest.Send(
			{
				method: 'GET',
				url: controllerUrl + 'GetModuleDetail?ModuleID=' + moduleid,
			},
			UserProfile
		).then(function (res) {
			if (res.status === 200) {
				if (res.data) {
                    setUrlView(res.data.UrlView);
					setModuleName(res.data.ModuleLabel);
                    setApproval(res.data.IsUseApproval);

					let newData = {};
					let newColumns = res.data.ModuleFields.map((value) => {
						if (value.IsPrimaryKey) setColumnKey(value.FieldName);
						newData[value.FieldName] = '';
						return value;
					});
					if (type === EActionType.Activation) {
						newData['Active'] = res.data.Active;
						newColumns.push({
							PK_ModuleField_ID: 999999,
							FK_Module_ID: moduleid,
							FieldName: 'Active',
							FieldLabel: 'Active',
							Sequence: 999,
							Required: true,
							IsPrimaryKey: false,
							IsUnik: false,
							IsShowInView: true,
							IsShowInForm: true,
							DefaultValue: true,
							FK_FieldType_ID: EDataType.BOOLEAN,
							SizeField: 0,
							FK_ExtType_ID: EInputType.Radio,
							TabelReferenceName: '',
							TabelReferenceNameAlias: '',
							TableReferenceFieldKey: '',
							TableReferenceFieldDisplayName: '',
							TableReferenceFilter: '',
							IsUseRegexValidation: false,
							TableReferenceAdditonalJoin: '',
							BCasCade: false,
							FieldNameParent: '',
							FilterCascade: '',
						});
					}
					setColumns(newColumns);

					setData(newData);
                    console.log(newData);

					//#region Module Detail
					if (
						res.data.ListModuleDetail &&
						res.data.ListModuleDetail.length > 0
					) {
						let newModuleDetails = res.data.ListModuleDetail.map((value) => {
							return {
								moduleDetailID: value.PK_ModuleDetail_ID,
							};
						});
						setListModuleDetail(newModuleDetails);
                        console.log(newModuleDetails,'List');
                    }
					//#endregion

					GetModuleData().then((res) => {
						if (res.status === 200) {
							if (res.data) {
								if (res.data.Dictionary) {
									let editData = res.data.Dictionary;
									Object.keys(newData).forEach((key) => {
										if (
											newColumns.find((value) => value.FieldName === key)
												.FK_ExtType_ID === 1
										) {
											newData[key] = new Date(editData[key]);
										} else if (
											newColumns.find((value) => value.FieldName === key)
												.FK_FieldType_ID === EDataType.BOOLEAN
										) {
											newData[key] = editData[key] === 1 ||  editData[key]===true ? 'true' : 'false';
										} else if (
											newColumns.find((value) => value.FieldName === key)
                                                .FK_FieldType_ID == EDataType.VARBINARY 
										) {
											newData[key] = {
												fileName: editData[key + 'Name'],
												fileData: editData[key],
											};
										} else {
											newData[key] = editData[key];
										}
									});
									setData({ ...newData });
								}
							}
						}
					});
					setIsLoading(false);
				} else {
                    showInfoPanel.error('Invalid Credential');
				}
			} else {
				showInfoPanel.error('Invalid Credential');
			}
		});
	};

	const onChangeData = (fieldName, value) => {
		let newData = data;
		newData[fieldName] = value;
		setData({ ...newData });
	};

	const onSave = () => {
        setIsLoading(true);
		APIRequest.Send(
			{
				method: 'POST',
				url: controllerUrl +'SaveParameterDetail',
				data: {
					ModuleID: moduleid,
					ActionType: type,
					data: Number(id),
				},
			},
			UserProfile
		).then(function (res) {
			if (res.status === 200) {
                if ( UserProfile.FK_MRole_ID != 1 && approval ) {
                    showInfoPanel.success('Pending To Approval')
                } else {
                    showInfoPanel.success('Data Saved to database')
                }
				setRedirect({
					pathname: urlView,
					params: [{ key: 'ModuleID', value: moduleid,infoPanel:'success' }],
				});
			} else {
                showInfoPanel.error('Invalid Credential');
			}
		})
        .catch((err) => {
            showInfoPanel.error(err.response.data);   
        })
        .finally(() => setIsLoading(false));
	};

	const onCancel = () => {
		setRedirect({
			pathname: urlView,
			params: [{ key: 'ModuleID', value: moduleid }],
		});
	};

	useEffect(() => {
        let abortController = new AbortController();
		GetModuleDetail();

        return () =>{
            abortController.abort();
        }
	}, []);

	if (redirect) return <Redirect to={redirect} />;

	return (
		<Card className={classes.root}>
            {isLoading ? <Loader /> : null}
			<CardHeader
				titleTypographyProps={{ variant: 'h4' }}
				title={moduleName}
				style={{ minHeight: '56px' }}
			/>
			<Divider />
			<CardContent className={classes.content}>
				{columns
					.filter((val) => val.IsShowInForm)
					.map((item, index) => {
						return (
							<div>
								{isDesktop ? ( 
									<NawaInput
                                        key={index}
                                        PK_ModuleField_ID={item.PK_ModuleField_ID}
                                        value={data[item.FieldName]}
                                        ParentValue={item.BCasCade ? data[item.FieldNameParent] : ''}
                                        name={item.FieldLabel}
                                        type={item.FK_ExtType_ID}
                                        dataType={item.FK_FieldType_ID}
                                        required={item.Required}
                                        disabled = { type === EActionType.Activation || type === EActionType.Detail}
                                        // check if actiontype is activation and extType is Radio, the radio button isn't disabled
                                        sizeField={item.SizeField ?? 0}
                                        fontColor={'black'}
								    />
								):(
									<div style = {{backgroundColor:'#F9F9F9'}}>
										{/* <ParameterDetailMobile
                                            key={index}
                                            PK_ModuleField_ID={item.PK_ModuleField_ID}
                                            value={data[item.FieldName]}
                                            ParentValue={item.BCasCade ? data[item.FieldNameParent] : ''}
                                            name={item.FieldLabel}
                                            type={item.FK_ExtType_ID}
                                            dataType={item.FK_FieldType_ID}
                                            required={item.Required}
										
										// check if actiontype is activation and extType is Radio, the radio button isn't disabled
                                            onChange={type===EActionType.Activation && item.FK_ExtType_ID===6?(value) => {
                                                    onChangeData(item.FieldName, value);
                                                }:false
                                            }
										    sizeField={item.SizeField ?? 0}
										/> */}
									</div>
								)}
							</div>
						);
					})}
				{listModuleDetail.map((item, index) => {
					return (
						// <ParameterDetailView
						// 	moduleDetailID={item.moduleDetailID}
						// 	parentID={id ?? 0}
						// 	actionType={type}
						// 	classContainer={classes.container}
						// 	toggleSend={false}
						// 	sendEditedData={() => {}}
                        //     parentData = {data}
						// />
						<></>
					);
				})}
			</CardContent>
			<Divider />
			<CardActions style={{ justifyContent: 'center' }}>
				{type === EActionType.Delete || type === EActionType.Activation ? (
					<Button
						variant='contained'
						size='small'
						color='primary'
						onClick={onSave}
					>
						{type === EActionType.Delete ? <Text delete/> : 
                        (data.Active === 'true' ? <Text deactivate/>:
                        <Text activate/>)
                        }
					</Button>
				) : null}
				<Button
					variant='contained'
					size='small'
					color='default'
					onClick={onCancel}
				>
					<Text cancel/>
				</Button>
			</CardActions>
		</Card>
	);
}
