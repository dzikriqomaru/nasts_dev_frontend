import React, { useState, useEffect, useContext } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import FileSaver from 'file-saver';
import Text from 'react-text';

import { 
    APIRequest, 
    EDataType, 
    EInputType, 
    EActionType, 
    NawaTable 
} from '@vikitheolorado/nawadata-web';
import { 
    UserProfileContext, 
    useInfoPanel, 
    useGetCustomMenuAccess,
    useMenuAccess,
} from '@vikitheolorado/nawadata-web';

import { Redirect } from 'react-router-dom';

const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
    },
    container: {
        height: `calc(${theme.contentHeight} - 105px)`,
    },
    visuallyHidden: {
        border: 0,
        clip: 'rect(0 0 0 0)',
        height: 1,
        margin: -1,
        overflow: 'hidden',
        padding: 0,
        position: 'absolute',
        top: 20,
        width: 1,
    },
}));

export default function CustomCheckOneRowOnly(props) {
    const { 
        moduleid, 
        title, 
        actionID,
    } = props.location.state;

    const { showInfoPanel } = useInfoPanel();
    const { getMenuAccess } = useGetCustomMenuAccess();
    const customMenuAccess = useMenuAccess();

    const classes = useStyles();
    const UserProfile = useContext(UserProfileContext);
    const [rowCount, setRowCount] = useState(0);
    const [moduleName, setModuleName] = useState('');
    const [moduleLabel, setModuleLabel] = useState('');

    const [redirect, setRedirect] = useState(null);
    const [addAccess, setAddAccess] = useState(true);

    const exportTypesList = [
        {
            Label: 'Excel',
            Extension: '.xlsx',
        },
        {
            Label: 'CSV',
            Extension: '.csv',
        },
        {
            Label: 'Text',
            Extension: '.txt',
        },
    ];

    const [actionList, setActionList] = useState([]);
    const [rows, setRows] = useState([]);
    const [isLoading, setIsLoading] = useState(true);

    const [columns, setColumns] = useState([]);

    const [columnKey, setColumnKey] = useState('PK_MUser_ID');

    const [urlPathName,setUrlPathName] = useState({
        Edit     : "",
        Add      : "",
    });

    const updateActionList = (data) => {
        setAddAccess(data.bAdd && data.IsSupportAdd);
    };

    const GetModuleData = (page, orderBy, order, search, filter, isAdv, fields) => {
        setIsLoading(true);
        APIRequest.Send({
            method: 'POST',
            url: 'api/usermanagement/GetModuleData',
            data: {
                moduleId:moduleid,
                page: page,
                orderBy: orderBy,
                order: order,
                search: search,
                filter: filter,
                isAdv: isAdv,
                // fields: fields,
            }
        }, UserProfile)
        .then(function(res){
            if (res.status === 200) {
                if (res.data) {
                    setRowCount(res.data.count);
                    setRows(res.data.rows.map(value => value.Dictionary));
                    setIsLoading(false);
                }
                else {
                    showInfoPanel.error('Invalid Credential');
                    setIsLoading(false);
                }
            }
            else {
                showInfoPanel.error('Invalid Credential');
                setIsLoading(false);
            }
        })
        .catch((err) => {
            showInfoPanel.error(err.response ? err.response.data : err);
            setIsLoading(false);
        });
    } 

    const GetModuleSupportActivation = () => {
        setIsLoading(true);
        APIRequest.Send({
            method: 'GET',
            url: 'api/Common/GetModuleByID',
            params: {
                id: moduleid
            }
        }, UserProfile)
        .then(function(res){
            if (res.status === 200) {
                if (res.data && res.data.IsSupportActivation) {
                    setColumns([
                        {id: 'PK_MUser_ID', label: 'ID', key: true, isShow: true, fieldType: EDataType.IDENTITY, inputType: EInputType.DisplayField, sequence: 1},
                        {id: 'UserID', label: 'User ID', key: false, isShow: true, fieldType: EDataType.VARCHAR, inputType: EInputType.TextField, sequence: 2},
                        {id: 'UserName', label: 'User Name', key: false, isShow: true, fieldType: EDataType.VARCHAR, inputType: EInputType.TextField, sequence: 3},
                        {id: 'RoleName', label: 'Role Name', key: false, isShow: true, fieldType: EDataType.VARCHAR, inputType: EInputType.TextField, sequence: 4},
                        {id: 'GroupMenuName', label: 'Group Menu', key: false, isShow: true, fieldType: EDataType.VARCHAR, inputType: EInputType.TextField, sequence: 5},
                        {id: 'UserEmailAddress', label: 'Email Address', key: false, isShow: true, fieldType: EDataType.VARCHAR, inputType: EInputType.TextField, sequence: 6},
                        {id: 'Active', label: 'Active', key: false, isShow: true, fieldType: EDataType.BOOLEAN, inputType: EInputType.Radio, sequence: 7},
                        {id: 'LastLogin', label: 'Last Login', key: false, isShow: true, fieldType: EDataType.DATETIME, inputType: EInputType.DATETIME, sequence: 8},
                        {id: 'IsUseLDAP', label: 'Use LDAP', key: false, isShow: true, fieldType: EDataType.BOOLEAN, inputType: EInputType.ToggleField, sequence: 9},
                    ]);
                }
            }
            setIsLoading(false);
        })
        .catch((err) => {
            showInfoPanel.error(err.response ? err.response.data : err);
            setIsLoading(false);
        });
    } 

    const GetModuleDetail = () => {
        APIRequest.Send({
            method: 'GET',
            url: 'api/parameter/GetModuleDetail',
            params: {
                ModuleID: moduleid,
                action: EActionType.View,
            }
        }, UserProfile)
        .then(res => {
            if (res.status === 200) {
                if (res.data) {
                    setModuleLabel(res.data.ModuleLabel);
                    setModuleName(res.data.ModuleName);
                    getMenuAccess.get(moduleid, 'api/UserManagement/GetModuleAccess');
                    setColumnKey('PK_MUser_ID');
                    GetModuleSupportActivation();
                    setActionList(res.data.ActionList);
                    setUrlPathName({
                        Edit     : res.data.UrlEdit,
                        Add      : res.data.UrlAdd
                    })
                }
                else {
                    showInfoPanel.error('Invalid Credential');
                }
            } else {
                showInfoPanel.error('Invalid Credential');
            }
        })
        .catch(err => {
            showInfoPanel.error(err.response ? err.response.data : err);
            setRedirect({
                pathname: '/SomethingWrong',
                state: {
                    errorMessage: err.response ? err.response.data : err
                },
            });
        });
    }

    useEffect(() => {
        GetModuleDetail();
    }, []);

    useEffect(() => {
        updateActionList(customMenuAccess);
    }, [customMenuAccess]);

    const onClickAction = (sender, record) => {
        setRedirect({
            pathname: sender.url,
            state: {
                type: sender.type,
                moduleid: moduleid,
                id: record[columnKey]
            }
        });
    }

    const onClickAdd = () => {
        setRedirect({
            pathname: urlPathName.Add,
            state: {
                type: EActionType.Insert,
                moduleid: moduleid
            }
        });
    }

    const onClickDownload = (page, orderBy, order, search, filter, isUsingAdv, exportType, fields) => {
        APIRequest.Send({
            url: 'api/usermanagement/ExportModuleData',
            method: 'POST',
            responseType: 'blob',
            data: {
                ModuleID: moduleid,
                page: page,
                orderBy: orderBy,
                order: order,
                search: search,
                filter: filter,
                isAdv: isUsingAdv,
                exportAs: exportType ? exportType.Label : '',
                fields: fields,
            },
        }, UserProfile)
        .then((res) => {
            if (res.status === 200) {
                if (res.data) {
                    if (exportType && exportType.Extension) {
                        FileSaver.saveAs(res.data, moduleLabel + exportType.Extension);
                    }
                    else {
                        FileSaver.saveAs(res.data, moduleLabel + '.xlsx');
                    }
                }
            }
            else {
                showInfoPanel.error('Invalid Credential');
            }
        })
        .catch((err) => {
            showInfoPanel.error(err.response ? err.response.data : err);
        })
        .finally(() => setIsLoading(false));
    }
  
    const onClickDownloadAll = (orderBy, order, search, filter, isUsingAdv, exportType, fields) => {
        APIRequest.Send({
            url: 'api/usermanagement/ExportModuleDataAll',
            method: 'POST',
            responseType: 'blob',
            data: {
                ModuleID: moduleid,
                orderBy: orderBy,
                order: order,
                search: search,
                filter: filter,
                isAdv: isUsingAdv,
                exportAs: exportType ? exportType.Label : '',
                fields: fields,
            },
        }, UserProfile)
        .then((res) => {
            if (res.status === 200) {
                if (res.data) {
                    if (exportType && exportType.Extension) {
                        FileSaver.saveAs(res.data, moduleLabel + exportType.Extension);
                    }
                    else {
                        FileSaver.saveAs(res.data, moduleLabel + '.xlsx');
                    }
                }
            }
            else {
                showInfoPanel.error('Invalid Credential');
            }
        })
        .catch((err) => {
            showInfoPanel.error(err.response ? err.response.data : err);
        })
        .finally(() => setIsLoading(false));
    }
  
    const onClickDownloadSelected = (selected, orderBy, order, exportType, fields) => {
        APIRequest.Send({
            url: 'api/usermanagement/ExportModuleDataSelected',
            method: 'POST',
            data: {
                ModuleID: 0,
                SelectedData: selected,
                orderBy: orderBy,
                order: order,
                exportAs: exportType ? exportType.Label : '',
                fields: fields,
            },
            responseType: 'blob'
        }, UserProfile)
        .then((res) => {
            if (res.status === 200) {
                if (res.data) {
                    if (exportType && exportType.Extension) {
                        FileSaver.saveAs(res.data, moduleLabel + exportType.Extension);
                    }
                    else {
                        FileSaver.saveAs(res.data, moduleLabel + '.xlsx');
                    }
                }
            }
            else {
                showInfoPanel.error('Invalid Credential');
            }
        })
        .catch((err) => {
            showInfoPanel.error(err.response ? err.response.data : err);
        })
        .finally(() => setIsLoading(false));
    }


    return redirect ? (
        <Redirect to={redirect} />
    ) : (
        <Paper className={classes.root}>
            <NawaTable
                rowCount={rowCount}
                moduleName={moduleName}
                moduleLabel={'MUser tapi cuma bisa select 1 row saja'}
                actionList={actionList}
                columns={columns}
                columnKey={columnKey}
                rows={rows}
                getModuleData={GetModuleData}
                onClickAction={onClickAction}
                onClickAdd={addAccess ? onClickAdd : null}
                onClickDownload={onClickDownload}
                onClickDownloadAll={onClickDownloadAll}
                onClickDownloadSelected={onClickDownloadSelected}
                noAdvFilter={true}
                isLoading={moduleName ? isLoading : true}
                exportTypesList={exportTypesList}
                onlyCheckOneRow
            />
        </Paper>
    );
}
