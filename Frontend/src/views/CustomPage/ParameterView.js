import React, { useState, useEffect, useContext } from 'react';
import { Redirect } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import FileSaver from 'file-saver';

// COMPONENYS
import {
    useInfoPanel,
    APIRequest,
    EActionType,
    NawaTable,
    UserProfileContext,
    DialogMessage as MsgDialog
} from '@vikitheolorado/nawadata-web';

const controllerUrl = "api/custom/";
const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
    },
}));
export default function ParameterView(props) {
    const params = props.location.params;
    const ModuleID = params.find((item) => item.key == 'ModuleID').value;
    const { showInfoPanel } = useInfoPanel();
    const classes = useStyles();
    const UserProfile = useContext(UserProfileContext);
    const tokenState = UserProfile.APIToken
    const [rowCount, setRowCount] = useState(0);
    const [moduleName, setModuleName] = useState('Module');
    const [redirect, setRedirect] = useState(null);
    const [actionList, setActionList] = useState([]);
    const [columns, setColumns] = useState([]);
    const [columnKey, setColumnKey] = useState(['']);
    const [rows, setRows] = useState([]);
    const [addAccess, setAddAccess] = useState(true);
    const [actionAdd, setActionAdd] = useState([]);
    const [supportApproval, setSupportApproval] = useState(false);
    const [isLoading, setIsLoading] = useState(true);
    const [clickedCS, setClickedCS] = useState(false)

 

    const getModuleData = (page, orderBy, order, search, filter, isAdvanced) => {
        setIsLoading(true);
        if( filter ) {
            let filterArr = filter.split(' ');
            if ( filterArr[0] == 'Active' ) {
                let filValue = filterArr[2]
                if ( 
                    filValue == "'%t%'" || filValue == "'%r%'" || filValue == "'%u%'" || 
                    filValue == "'%tr%'" || filValue == "'%ru%'" || filValue == "'%ue%'" ||
                    filValue == "'%tru%'" || filValue == "'%rue%'" || filValue == "'%true%'" ||
                    filValue == "'%1%'" || filValue == 1
                ) {
                    filter = `Active ${filterArr[1]} 1`
                } else if (
                    filValue == "'%f%'" || filValue == "'%a%'" || filValue == "'%l%'" || 
                    filValue == "'%s%'" || filValue == "'%fa%'" || filValue == "'%al%'" || 
                    filValue == "'%ls%'" || filValue == "'%se%'" || filValue == "'%fal%'" || 
                    filValue == "'%als%'" || filValue == "'%lse%'" || filValue == "'%fals%'" || 
                    filValue == "'%alse%'" || filValue == "'%false%'" || filValue == "'%0%'" || 
                    filValue == 0  
                ) {
                    filter = `Active ${filterArr[1]} 0`
                } else if (filValue == "'%e%'") {
                    filter = ""
                } else {
                        filter = "Active = 5"
                    }
                }
            }
        
        if (ModuleID) {
            APIRequest.Send({
                method: 'GET',
                url: controllerUrl + 'GetModuleData',
                params: {
                    ModuleID: ModuleID,
                    page: page,
                    orderBy: orderBy,
                    order: order,
                    search: search,
                    filter: filter,
                    isAdv: isAdvanced,
                }
            },UserProfile)
            .then((res) => {
                if (res.status === 200) {
                    if (res.data) {
                        setRowCount(res.data.count);
                        setRows(res.data.rows.map((value) => value.Dictionary));
                    } else {
                        showInfoPanel.error('Invalid Credential')
                    }
                } else {
                    showInfoPanel.error('Invalid Credential')
                }
            })
            .catch((err) => {
                showInfoPanel.error(err.data+'');
            })
            .finally(() => setIsLoading(false));
        }
        else {
            showInfoPanel.error('Module ID not found');
            setIsLoading(false);
        }
    };

    const getModuleDetail = () => {
        setIsLoading(true);
        if (ModuleID && tokenState) {
            APIRequest.Send({
                method: 'GET',
                url: controllerUrl + 'GetModuleDetail',
                params: {
                    ModuleID: ModuleID,
                }
            },UserProfile)
            .then((res) => {
                if (res.status === 200) {
                    if (res.data) {
                        setSupportApproval(res.data.IsUseApproval)
                        setModuleName(res.data.ModuleLabel);
                        let newColumns = res.data.ModuleFields.map((value) => {
                            if (value.IsPrimaryKey)
                                setColumnKey(value.FieldName);
    
                            return {
                                id: value.FieldName,
                                label: value.FieldLabel,
                                key: value.IsPrimaryKey,
                                isShow: value.IsShowInView,
                                fieldType: value.FK_FieldType_ID,
                                inputType: value.FK_ExtType_ID,
                                tabelReferenceName: value.TabelReferenceName,
                                tableReferenceFieldKey:
                                    value.TableReferenceFieldKey,
                                tableReferenceFieldDisplayName:
                                    value.TableReferenceFieldDisplayName,
                                sequence: value.Sequence,
                                PK_ModuleField_ID: value.PK_ModuleField_ID,
                                active:value.Active
                            };
                        });
                        setColumns(newColumns);
                        setActionList(res.data.ActionList);
                        let actionAdd = res.data.ActionList.filter(
                            (item) => item.type === EActionType.Insert
                        );
                        setAddAccess(actionAdd.length > 0);
                        if (actionAdd.length === 1) {
                            setActionAdd(actionAdd[0]);
                        }
    
                        getModuleData(1, '', '', '', '');
                    } else {
                        showInfoPanel.error('Invalid Credential');
                    }
                } else {
                    showInfoPanel.error('Invalid Credential');
                }
            })
            .catch((err) => {
                showInfoPanel.error(err.data+'');
            })
        }
        else {
            showInfoPanel.error('Module ID not found');
            setIsLoading(false);
        }
    };

    const onClickAction = (sender, record) => {
        for(let data in record){
            if(typeof(record[data])!== 'string'){
               
                record[data] = record[data].toString();
            }
        }
        if (sender.type === EActionType.OthersWithSP) {
            APIRequest.Send(
                {
                    method: 'POST',
                    url: controllerUrl + 'CustomSupportSP',
                    data: {
                        Target: sender.url,
                        Parameter: sender.parameter,
                        ParameterValue: sender.parameterValue,
                        ModuleID: ModuleID,
                        Data: record
                    },
                },
                UserProfile
            )
                .then((res) => {
                    if (res.status === 200) {
                        handleClicked();
                        setIsLoading(false);
                    } else {
                        showInfoPanel.error('Invalid Credential');
                    }
                })
                .catch((err) => {
                    showInfoPanel.error(err.response.data+'');
                })
                .finally(() => setIsLoading(false));
        } 
        else if (sender.type === EActionType.OthersWithPage) {
            let moduleid = ModuleID;
            let type = sender.type;
            let id = record[columnKey];
            let text = sender.text;
            let url = sender.url;

            let parameters = sender.parameter.replace(/[@\s]/g, '');
            let arrParameter = parameters.split(',');
            let arrParameterValue = sender.parameterValue
                .replace(/\s/g, '')
                .split(',');
            let stateRedirect = {};
            for (let i = 0; i < arrParameter.length; i++) {
                let field = columns.find(
                    (item) => item.sequence == arrParameterValue[i]
                );
                if (field != null) {
                    stateRedirect[arrParameter[i]] = record[field.id];

                    if(arrParameter[i].toLowerCase() == "moduleid"){
                        moduleid = record[field.id];
                    }
                    if(arrParameter[i].toLowerCase() == "type"){
                        type = record[field.id];
                    }
                    if(arrParameter[i].toLowerCase() == "id"){
                        id = record[field.id];
                    }
                    if(arrParameter[i].toLowerCase() == "text"){
                        text = record[field.id];
                    }
                }
            }
            if(url.split("?").length > 1){
                let urlParam = url.split("?").slice(1).join("?");
                url = url.split("?")[0];

                let arrParams = urlParam.split("&");
                for (let i = 0; i < arrParams.length; i++) {
                    let paramDetail = arrParams[i].split("=");
                    if (paramDetail.length = 2) {
                        stateRedirect[paramDetail[0]] = paramDetail[1];
                    
                        if(paramDetail[0].toLowerCase() == "moduleid"){
                            moduleid = paramDetail[1];
                        }
                        if(paramDetail[0].toLowerCase() == "type"){
                            type = paramDetail[1];
                        }
                        if(paramDetail[0].toLowerCase() == "id"){
                            id = paramDetail[1];
                        }
                        if(paramDetail[0].toLowerCase() == "text"){
                            text = paramDetail[1];
                        }
                    }
                }
            }

            setRedirect({
                pathname: url,
                state: {
                    type: type,
                    moduleid: moduleid,
                    id: id,
                    text: text,
                    custom: stateRedirect,
                },
            });
        } 
        else {
            setRedirect({
                pathname: sender.url,
                state: {
                    type: sender.type,
                    moduleid: ModuleID,
                    id: record[columnKey],
                    approval:supportApproval,
                },
            });
        }
    };

    const onClickAdd = () => {
        setRedirect({
            pathname: actionAdd.url,
            state: {
                type: EActionType.Insert,
                moduleid: ModuleID,
                approval:supportApproval
            },
        });
    };

    const onClickDownloadCurrentPage = (page, orderBy, order, search, filter, isUsingAdv) => {
        if (ModuleID) {
            setIsLoading(true);
            APIRequest.Send({
                url: controllerUrl + 'ExportModuleData',
                method: 'GET',
                responseType: 'blob',
                params: {
                    ModuleID: ModuleID,
                    page: page,
                    orderBy: orderBy,
                    order: order,
                    search: search,
                    filter: filter,
                    isAdv: isUsingAdv,
                },
            },UserProfile)
            .then((res) => {
                if (res.status === 200) {
                    if (res.data) {
                        FileSaver.saveAs(res.data, moduleName + '.xlsx');
                    }
                }
                else {
                    showInfoPanel.error('Invalid Credential');
                }
            })
            .catch((err) => {
                showInfoPanel.error(err.response.data+'');
            })
            .finally(() => setIsLoading(false));
        }
        else {
            showInfoPanel.error('Module ID not found');
        }
    };

    const onClickDownloadAll = (orderBy, order, search, filter, isUsingAdv) => {
        if (ModuleID) {
            setIsLoading(true);
            APIRequest.Send({
                url:controllerUrl + 'ExportModuleDataAll',
                method: 'GET',
                responseType: 'blob',
                params: {
                    ModuleID: ModuleID,
                    orderBy: orderBy,
                    order: order,
                    search: search,
                    filter: filter,
                    isAdv: isUsingAdv,
                }
            },UserProfile)
            .then((res) => {
                if (res.status === 200) {
                    if (res.data) {
                        FileSaver.saveAs(res.data, moduleName + '.xlsx');
                    }
                }
                else {
                    showInfoPanel.error('Invalid Credential');
                }
            })
            .catch((err) => {
                showInfoPanel.error(err.response.data+'');
            })
            .finally(() => setIsLoading(false));
        }
        else {
            showInfoPanel.error('Module ID not found');
        }
    };

    const onClickDownloadSelected = (selected, orderBy, order) => {
        if (ModuleID) {
            setIsLoading(true);
            APIRequest.Send({
                url: controllerUrl + 'ExportModuleDataSelected',
                method: 'POST',
                data: {
                    ModuleID: ModuleID,
                    SelectedData: selected,
                    orderBy: orderBy,
                    order: order,
                },
                responseType: 'blob',
            },UserProfile)
            .then((res) => {
                if (res.status === 200) {
                    if (res.data) {
                        FileSaver.saveAs(res.data, moduleName + '.xlsx');
                    }
                }
                else {
                    showInfoPanel.error('Invalid Credential');
                }
            })
            .catch((err) => {
                showInfoPanel.error(err.response.data+'');
            })
            .finally(() => setIsLoading(false));
        }
        else {
            showInfoPanel.error('Module ID not found');
        }
    };

    useEffect(() => {
        if(tokenState){
            getModuleDetail();
        }
    }, [props.location.params, clickedCS]);

    const handleClicked = () =>{
        setClickedCS(!clickedCS)
    }

    return redirect 
    ? (
        <Redirect to={redirect} />
    )
    : (
        <Paper className={classes.root}>
            <NawaTable
                rowCount={rowCount}
                moduleName={moduleName}
                actionList={actionList}
                columns={columns}
                columnKey={columnKey}
                rows={rows}
                getModuleData={getModuleData}
                onClickAction={onClickAction}
                onClickAdd={addAccess ? onClickAdd : null}
                onClickDownload={onClickDownloadCurrentPage}
                onClickDownloadAll={onClickDownloadAll}
                onClickDownloadSelected={onClickDownloadSelected}
                isLoading={isLoading}
            />
        </Paper>
    );
}