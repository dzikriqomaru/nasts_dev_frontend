import React, { useState, useEffect, useContext } from 'react';
import moment from 'moment';
import { Redirect } from 'react-router-dom';
import TableMasterDetail from './TableMasterDetail';
import {
    Card,
    CardContent,
    CardActions,
    CardHeader,
    Typography,
    Button,
    Divider,
    Box,
    Grid,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    TextField,
    IconButton,
    Modal
} from '@material-ui/core';
import HistoryIcon from '@material-ui/icons/History';

import { makeStyles } from '@material-ui/core/styles';

import {
    APIRequest,
    EInputType,
    EActionType,
    EDataType,
    NawaInput,
	UserProfileContext, useInfoPanel,
    DialogMessage as MsgDialog,
    Loader,
    NawaTable,
} from '@vikitheolorado/nawadata-web';

const controllerUrl = "api/custom/";

// import { KeyboardArrowLeft } from '@material-ui/icons';

const formatDate = (date) => {
    // dddd = nama hari
    // DD = tanggal (ex: 01, 14, 28)
    // Do = tanggal (ex: 1st, 2nd, 3rd)
    // MM = bulan (angka)
    // MMM = bulan (short)
    // MMMM = bulan (full)
    // YY = tahun (puluhan)
    // YYYY = tahun (full)
    const dateString = 'DD MMMM YYYY';
    const dateFormatted = moment(date).format(dateString);
    return dateFormatted;
};

const useStyles = makeStyles((theme) => ({
    root: {
        height: `calc(${theme.contentHeight})`,
    },
    content: {
        height: `calc(${theme.contentHeight} - 103px)`,
        overflow: 'auto',
        padding: '5px',
    },
    contentGrid: {
        height: `calc(${theme.contentHeight} - 103px)`,
        oveflow: 'auto',
    },
}));

export default function ParameterApprovalDetail(props) {
    const { moduleid, id } = props.location.state;
    const UserProfile = useContext(UserProfileContext);
    const [moduleName, setModuleName] = useState('Module');
    const [haveActiveWorkflow, setHaveActiveWorkflow] = useState(false);
    const [reviewDialog, setReviewDialog] = useState(false);
    const [reviewAction, setReviewAction] = useState(1);
    const [reviewNotes, setReviewNotes] = useState('');
    const [moduleKey, setModuleKey] = useState('');
    const [action, setAction] = useState('');
    const [actionID, setActionID] = useState(0);
    const [createdBy, setCreatedBy] = useState('');
    const [createdDate, setCreatedDate] = useState('');
    const [columns, setColumns] = useState([]);
    const [columnKey, setColumnKey] = useState(['']);
    const [oldData, setOldData] = useState({});
    const [newData, setNewData] = useState({});
    const [redirect, setRedirect] = useState(null);
    const [listModuleDetail, setListModuleDetail] = useState(null);
	const { showInfoPanel } = useInfoPanel();
    const [data, setData] = useState();
    const [activationData, setActivationData] = useState({});

    const [rowCount, setRowCount] = useState(0);
    const [rows, setRows] = useState([]);

    const [isLoading, setIsLoading] = useState(true);
    const [isLoadingTable, setIsLoadingTable] = useState(false);

    const [historyOpen, setHistoryOpen] = useState(false);

    const classes = useStyles();

    const columnsHistory = [
        { id: 'PK_MWorkflow_History_ID', label: 'ID', isShow: true },
        { id: 'intLevel', label: 'Level', isShow: true },
        { id: 'RoleName', label: 'Role', isShow: true },
        { id: 'UserName', label: 'User', isShow: true },
        { id: 'UserNameExecute', label: 'User Execute', isShow: true },
        { id: 'ResponseDate', label: 'Response Date', isShow: true, fieldType: EDataType.DATETIME, inputType: EInputType.DateField },
        { id: 'ApprovalStatusName', label: 'Status', isShow: true },
        { id: 'Notes', label: 'Notes', isShow: true },
        { id: 'CreatedDate', label: 'Created Date', isShow: true, fieldType: EDataType.DATETIME, inputType: EInputType.DateField },
    ]
    const [dataHistory, setDataHistory] = useState([]);
    const [rowCountHistory, setRowCountHistory] = useState(0);

    const getModuleDataHistory = (page, orderBy, order, search, filter) => {
        setIsLoading(true);
        if (id) {
            APIRequest.Send({
                method: 'GET',
                url: controllerUrl + 'GetApprovalDataHistory',
                params: {
                    PK_ModuleApproval_ID: id,
                    page: page,
                    orderBy: orderBy,
                    order: order,
                    search: search,
                    filter: filter,
                }
            },UserProfile)
            .then((res) => {
                if (res.status === 200) {
                    if (res.data) {
                        setRowCountHistory(res.data.count);
                        setDataHistory(res.data.rows.map((value) => value.Dictionary));
                    } else {
                        showInfoPanel.error('Something went wrong');
                        // setMsgDialogTitle('Error');
                        // setMsgDialogContent('Invalid Credential');
                        // dialogHandle(); // waiting for API changes
                    }
                } else {
                    showInfoPanel.error('Something went wrong');
                    // setMsgDialogTitle('Error');
                    // setMsgDialogContent('Invalid Credential');
                    // dialogHandle(); // waiting for API changes
                }
            })
            .catch((err) => {
                showInfoPanel.error(err.data);
                // setMsgDialogTitle('Error');
                // setMsgDialogContent(err.data);
                // dialogHandle();
            })
            .finally(() => setIsLoading(false));
        }
        else {
            showInfoPanel.error('ModuleApproval ID not found');
            // setMsgDialogTitle('Error');
            // setMsgDialogContent('Module ID not found');
            // dialogHandle();
            setIsLoading(false);
        }
    };

    const getApprovalData = () => {
        if (id) {
            return APIRequest.Send(
                {
                    method: 'GET',
                    url: controllerUrl + 'GetModuleApprovalByID',
                    params: {
                        PK_ModuleApproval_ID: id,
                    }
                },
                UserProfile
            );
        }
        else {
            showInfoPanel.error('id not found');
        }
    };

    const getImportApprovalData = (page, orderBy, order, search, filter) => {
        setIsLoadingTable(false);
        if (id) {
            APIRequest.Send(
                {
                    method: 'GET',
                    url: controllerUrl + 'GetImportApprovalByID',
                    params: {
                        PK_ModuleApproval_ID: id,
                        page: page,
                        orderBy: orderBy,
                        order: order,
                        search: search,
                        filter: filter
                    }
                },
                UserProfile
            ).then((res) => {
                if(res?.data){
                    setIsLoadingTable(false);
                    setRowCount(res.data.count);
                    setRows(res.data.rows.map((value) => value.Dictionary));
                }
            })
            .catch((err) => {
                console.log(err.response.statusText);
                showInfoPanel.error(err.response.statusText);
                // setMsgDialogContent(err.response.statusText);
                // setMsgDialogTitle('Error');
                // dialogHandle();
                setIsLoadingTable(false);
            });
        }
        else {
            showInfoPanel.error('id not found');
            // setMsgDialogContent();
            // setMsgDialogTitle('Error');
            // dialogHandle();
            setIsLoadingTable(false);
        }
    };

    // Feel free to delete this function in the future if changes are to be made 
    const setInvalidCredentialDialog = () => {
        setIsLoading(false);
        showInfoPanel.error('Invalid Credentials');
    }

    const getModuleDetail = () => {
        if (id && moduleid) {
            APIRequest.Send(
                {
                    method: 'GET',
                    url: controllerUrl + 'GetModuleDetail',
                    params: {
                        ModuleID: moduleid,
                        PK_ModuleApproval_ID: id,
                    }
                },
                UserProfile
            ).then(function (res) {
                if (res.status === 200) {
                    if (res.data) {
                        setModuleName(res.data.ModuleLabel);
                        setHaveActiveWorkflow(res.data.HaveActiveWorkflow);
                        setListModuleDetail(res.data.ListModuleDetail);
                        let newOldData = {};
                        let newNewData = {};
                        let newColumns = res.data.ModuleFields.map((value) => {
                            if (value.IsPrimaryKey) setColumnKey(value.FieldName);
                            newOldData[value.FieldName] = '';
                            newNewData[value.FieldName] = '';
                            return {
                                id: value.FieldName,
                                label: value.FieldLabel,
                                key: value.IsPrimaryKey,
                                isShow: value.IsShowInView,
                                fieldType: value.FK_FieldType_ID,
                                inputType: value.FK_ExtType_ID,
                                tabelReferenceName: value.TabelReferenceName,
                                tableReferenceFieldKey:
                                    value.TableReferenceFieldKey,
                                tableReferenceFieldDisplayName:
                                    value.TableReferenceFieldDisplayName,
                                sequence: value.Sequence,
                                ...value};
                        });
                        newColumns.push({
                            id: 'nawa_Action',
                            label: 'Action',
                            isShow: true,
                            key: true,
                            fieldType: EDataType.VARCHAR,
                        })
                        setColumns(newColumns);
    
                        if (
                            res.data.PK_ModuleAction_ID == EActionType.Update ||
                            res.data.PK_ModuleAction_ID == EActionType.Delete
                        ) {
                            setOldData(newOldData);
                        } 
                        else {
                            setOldData(null);
                        }
    
                        if (
                            res.data.PK_ModuleAction_ID == EActionType.Update ||
                            res.data.PK_ModuleAction_ID == EActionType.Insert
                        ) {
                            setNewData(newNewData);
                        } 
                        else {
                            setNewData(null);
                        }
    
                        getApprovalData().then(function (res) {
                            if (res.status === 200) {
                                if (res.data) {
                                    setModuleKey(res.data.ModuleKey);
                                    setAction(res.data.ModuleActionName);
                                    setActionID(res.data.PK_ModuleAction_ID);
                                    setCreatedBy(res.data.CreatedBy);
                                    setCreatedDate(
                                        formatDate(new Date(res.data.CreatedDate))
                                    );
                                    if(res.data.PK_ModuleAction_ID == EActionType.Import){
                                        getImportApprovalData(1, '', '', '', '');
                                    }else{
                                        let editOldData =
                                        res.data.PK_ModuleAction_ID ==
                                            EActionType.Update ||
                                        res.data.PK_ModuleAction_ID ==
                                            EActionType.Delete ||
                                        res.data.PK_ModuleAction_ID ==
                                            EActionType.Activation
                                            ? res.data.oldData[0]
                                            : null;
                                        let editNewData =
                                            res.data.PK_ModuleAction_ID ==
                                                EActionType.Update ||
                                            res.data.PK_ModuleAction_ID ==
                                                EActionType.Insert
                                                ? res.data.newData[0]
                                                : null;
                                        newColumns.forEach((col) => {
                                            if (col.FK_ExtType_ID == 1) {
                                                if (
                                                    res.data.PK_ModuleAction_ID ==
                                                        EActionType.Update ||
                                                    res.data.PK_ModuleAction_ID ==
                                                        EActionType.Delete ||
                                                    res.data.PK_ModuleAction_ID ==
                                                        EActionType.Activation
                                                ) {
                                                    newOldData[
                                                        col.FieldName
                                                    ] = new Date(
                                                        editOldData[col.FieldName]
                                                    );
                                                }
                                                if (
                                                    res.data.PK_ModuleAction_ID ==
                                                        EActionType.Update ||
                                                    res.data.PK_ModuleAction_ID ==
                                                        EActionType.Insert
                                                ) {
                                                    newNewData[
                                                        col.FieldName
                                                    ] = new Date(
                                                        editNewData[col.FieldName]
                                                    );
                                                }
                                            } 
                                            else if (col.FK_FieldType_ID == 13) {
                                                if (
                                                    res.data.PK_ModuleAction_ID ==
                                                        EActionType.Update ||
                                                    res.data.PK_ModuleAction_ID ==
                                                        EActionType.Delete ||
                                                    res.data.PK_ModuleAction_ID ==
                                                        EActionType.Activation
                                                ) {
                                                    newOldData[col.FieldName] =
                                                        editOldData[col.FieldName] == 1
                                                            ? 'true'
                                                            : 'false';
                                                }
                                                if (
                                                    res.data.PK_ModuleAction_ID ==
                                                        EActionType.Update ||
                                                    res.data.PK_ModuleAction_ID ==
                                                        EActionType.Insert
                                                ) {
                                                    newNewData[col.FieldName] =
                                                        editNewData[col.FieldName] == 1
                                                            ? 'true'
                                                            : 'false';
                                                }
                                            } 
                                            else if (col.FK_FieldType_ID == 14) {
                                                if (
                                                    res.data.PK_ModuleAction_ID ==
                                                        EActionType.Update ||
                                                    res.data.PK_ModuleAction_ID ==
                                                        EActionType.Delete ||
                                                    res.data.PK_ModuleAction_ID ==
                                                        EActionType.Activation
                                                ) {
                                                    newOldData[col.FieldName] = {
														fileName: editOldData[col.FieldName + 'Name'],
														fileData: editOldData[col.FieldName],
													};
                                                }
                                                if (
                                                    res.data.PK_ModuleAction_ID ==
                                                        EActionType.Update ||
                                                    res.data.PK_ModuleAction_ID ==
                                                        EActionType.Insert
                                                ) {
                                                    newNewData[col.FieldName] = {
														fileName: editNewData[col.FieldName + 'Name'],
														fileData: editNewData[col.FieldName],
													};
                                                }
                                            } 
                                            else {
                                                if (
                                                    res.data.PK_ModuleAction_ID ==
                                                        EActionType.Update ||
                                                    res.data.PK_ModuleAction_ID ==
                                                        EActionType.Delete ||
                                                    res.data.PK_ModuleAction_ID ==
                                                        EActionType.Activation
                                                ) {
                                                    newOldData[col.FieldName] =
                                                        editOldData[col.FieldName];
                                                }
                                                if (
                                                    res.data.PK_ModuleAction_ID ==
                                                        EActionType.Update ||
                                                    res.data.PK_ModuleAction_ID ==
                                                        EActionType.Insert
                                                ) {
                                                    newNewData[col.FieldName] =
                                                        editNewData[col.FieldName];
                                                }
                                            }
                                        });
                                        if (
                                            res.data.PK_ModuleAction_ID ==
                                                EActionType.Activation
                                        ) {
                                            newOldData.Active = res.data.oldData[0].Active ? 'true -> false' : 'false -> true';
                                            newColumns.push({
                                                FieldName: 'Active',
                                                FieldLabel: 'Active',
                                                IsShowInForm: true,
                                                key: false,
                                                FK_ExtType_ID: 5,
                                                FK_FieldType_ID: 9,
                                                fieldType: 9,
                                                id: 'GroupMenuDesciption',
                                                inputType: 5,
                                                isShow: true,
                                                key: false,
                                                label: 'Description',
                                            });
                                            setColumns(newColumns);
                                        }
                                        if (
                                            res.data.PK_ModuleAction_ID ==
                                                EActionType.Update ||
                                            res.data.PK_ModuleAction_ID ==
                                                EActionType.Delete ||
                                            res.data.PK_ModuleAction_ID ==
                                                EActionType.Activation
                                        ) {
                                            setOldData({ ...newOldData });
                                        }
                                        if (
                                            res.data.PK_ModuleAction_ID ==
                                                EActionType.Update ||
                                            res.data.PK_ModuleAction_ID ==
                                                EActionType.Insert
                                        ) {
                                            setNewData({ ...newNewData });
                                        }
                                    }

                                    setIsLoading(false);
                                } 
                                else {
                                    setInvalidCredentialDialog();
                                    setIsLoading(false);
                                }
                            } 
                            else {
                                setInvalidCredentialDialog();
                                setIsLoading(false);
                            }
                        }).catch(err => {
                            showInfoPanel.error(err.response.statusText);
                            setIsLoading(false);
                        });
                    } 
                    else {
                        setInvalidCredentialDialog();
                        setIsLoading(false);
                    }
                } 
                else {
                    setInvalidCredentialDialog();
                    setIsLoading(false);
                }
            });
        }
        else {
            showInfoPanel.error('ID Not Found');
            setIsLoading(false);
        }
    };

    const onChangeData = (fieldName, value) => {
        // let newData = data;
        // newData[fieldName] = value;
        // setData({...newData});
    };

    const onApprove = () => {
        if (haveActiveWorkflow) {
            setReviewAction(1);
            setReviewDialog(true);
        } else {
            setIsLoading(true);
            if (id) {
                APIRequest.Send(
                    {
                        method: 'POST',
                        url: controllerUrl + 'ActionModuleApproval',
                        data: {
                            ActionApproval: 1,
                            PK_ModuleApproval_ID: id,
                            Review: '',
                        },
                    },
                    UserProfile
                ).then(function (res) {
                    if (res.status === 200) {
                        showInfoPanel.success('Data Saved into database')
                        setRedirect({
                            pathname: '/ParameterApproval',
                            params: [{ key: 'ModuleID', value: moduleid }],
                        });
                    } else {
                        setInvalidCredentialDialog();
                    }
                })
                .catch((err)=>{
                    setIsLoading(false);
                    showInfoPanel.error(err.response.data.title);
                });
            }
            else {
                setIsLoading(false);
                showInfoPanel.error('ID Not Found');
            }
        }
    };

    const onReject = () => {
        if (haveActiveWorkflow) {
            setReviewAction(2);
            setReviewDialog(true);
        } 
        else {
            setIsLoading(true);
            if (id) {
                APIRequest.Send(
                    {
                        method: 'POST',
                        url: controllerUrl + 'ActionModuleApproval',
                        data: {
                            ActionApproval: 2,
                            PK_ModuleApproval_ID: id,
                            Review: '',
                        },
                    },
                    UserProfile
                ).then(function (res) {
                    if (res.status === 200) {
                        showInfoPanel.success('Data Rejected')
                        setRedirect({
                            pathname: '/ParameterApproval',
                            params: [{ key: 'ModuleID', value: moduleid }],
                        });
                    } else {
                        setInvalidCredentialDialog();
                    }
                })
                .catch((err)=>{
                    setIsLoading(false);
                    showInfoPanel.error('Error while rejecting')
                });
            }
            else {
                showInfoPanel.error('ID Not Found');
                setIsLoading(false);

            }
        }
    };

    const onRevise = () => {
        setReviewAction(3);
        setReviewDialog(true);
    };

    const handleClose = () => {
        setReviewDialog(false);
    };

    const handleSubmit = () => {
        if (id) {
            setIsLoading(true)
            APIRequest.Send(
                {
                    method: 'POST',
                    url: controllerUrl + 'ActionModuleApproval',
                    data: {
                        ActionApproval: reviewAction,
                        PK_ModuleApproval_ID: id,
                        Review: '',
                    },
                },
                UserProfile
            ).then(function (res) {
                if (res.status === 200) {
                    setRedirect({
                        pathname: '/ParameterApproval',
                        params: [{ key: 'ModuleID', value: moduleid }],
                    });
                    setIsLoading(false)
                    if(reviewAction == 1){
                        showInfoPanel.success('Data Approved');
                    }
                    else if(reviewAction == 2){
                        showInfoPanel.success('Data Rejected');
                    }
                    else if(reviewAction == 3){
                        showInfoPanel.success('Data Revised');
                    }
                } else {
                    setIsLoading(false)
                    setInvalidCredentialDialog();
                }
            });
        }
        else {
            showInfoPanel.error('ID Not Found');
        }
    };

    const onCancel = () => {
        setRedirect({
            pathname: '/ParameterApproval',
            params: [{ key: 'ModuleID', value: moduleid }],
        });
    };

    useEffect(() => {
        getModuleDetail();
    }, []);

    if (redirect) return <Redirect to={redirect} />;

    return (
        <React.Fragment>
            { isLoading ? <Loader /> : null }

            <Dialog open={reviewDialog} onClose={handleClose}>
                <DialogTitle>Notes</DialogTitle>
                <DialogContent>
                    <TextField
                        autoFocus
                        margin='dense'
                        label='Notes'
                        type='text'
                        value={reviewNotes}
                        onChange={(event) => {
                            setReviewNotes(event.target.value);
                        }}
                        fullWidth
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color='secondary'>
                        Cancel
                    </Button>
                    <Button onClick={handleSubmit} color='primary'>
                        Submit
                    </Button>
                </DialogActions>
            </Dialog>
            <Card className={classes.root}>
                <CardHeader
                    titleTypographyProps={{ variant: 'h4' }}
                    title={moduleName + ' Approval Detail'}
                    style={{ minHeight: '56px' }}
                />
                <Divider />
                <CardContent className={classes.content}>
                    <Typography
                        style={{ flex: '1 1 100%' }}
                        color='inherit'
                        variant='subtitle1'
                    >
                        Approval
                        <IconButton color='inherit' onClick={() => {setHistoryOpen(true)}}>
                            <HistoryIcon style={{fontSize: "medium"}} />
                        </IconButton>
                    </Typography>
                    <NawaInput
                        value={moduleName}
                        name={'Module Name'}
                        type={EInputType.TextField}
                        dataType={EDataType.VARCHAR}
                        required={false}
                    />
                    <NawaInput
                        value={moduleKey}
                        name={'Module Key'}
                        type={EInputType.TextField}
                        dataType={EDataType.VARCHAR}
                        required={false}
                    />
                    <NawaInput
                        value={action}
                        name={'Action'}
                        type={EInputType.TextField}
                        dataType={EDataType.VARCHAR}
                        required={false}
                    />
                    <NawaInput
                        value={createdBy}
                        name={'Created By'}
                        type={EInputType.TextField}
                        dataType={EDataType.VARCHAR}
                        required={false}
                    />
                    <NawaInput
                        value={createdDate}
                        name={'Created Date'}
                        type={EInputType.TextField}
                        dataType={EDataType.VARCHAR}
                        required={false}
                    />

                    <Grid container>
                        {actionID == EActionType.Import ?
                        (
                            <React.Fragment>
                                <NawaTable
                                    id='approval-table'
                                    moduleName={"Data Import"}
                                    rowCount={rowCount}
                                    rows={rows}
                                    columns={columns.filter(val => val.FieldName != "CreatedBy" && val.FieldName != "CreatedDate" && val.FieldName != "LastUpdateBy" && val.FieldName != "LastUpdateDate" && val.FieldName != "ApprovedBy" && val.FieldName != "ApprovedDate" && val.FieldName != "Alternateby")}
                                    actionList={[]}
                                    getModuleData={getImportApprovalData}
                                    onClickAction={() => {}}
                                    noAdvFilter={true}
                                    //isLoading={false}
                                />
                            </React.Fragment>
                        )
                        :
                        (
                        <React.Fragment>
                            {oldData ? (
                                <Grid item xs={newData ? 6 : 12}>
                                    <Typography
                                        style={{ flex: '1 1 100%' }}
                                        color='inherit'
                                        variant='subtitle1'
                                    >
                                        {actionID == EActionType.Activation 
                                            ? 'Activation Data' 
                                            : 'Old Data'
                                        }
                                    </Typography>
                                    {columns
                                        .filter((val) => val.IsShowInForm)
                                        .map((item, index) => {
                                            return (
                                                <NawaInput
                                                    key={index}
                                                    PK_ModuleField_ID={
                                                        item.PK_ModuleField_ID
                                                    }
                                                    value={oldData[item.FieldName]}
                                                    ParentValue={
                                                        item.BCasCade
                                                            ? oldData[
                                                                item
                                                                    .FieldNameParent
                                                            ]
                                                            : ''
                                                    }
                                                    name={item.FieldLabel}
                                                    type={item.FK_ExtType_ID}
                                                    dataType={item.FK_FieldType_ID}
                                                    required={item.Required}
                                                    fontColor={'black'}
                                                />
                                            );
                                        })}
                                </Grid>
                            ) : null}

                            {newData ? (
                                <Grid item xs={oldData ? 6 : 12}>
                                    <Typography
                                        style={{ flex: '1 1 100%' }}
                                        color='inherit'
                                        variant='subtitle1'
                                    >
                                        New Data
                                    </Typography>
                                    {columns
                                        .filter((val) => val.IsShowInForm)
                                        .map((item, index) => {
                                            return (
                                                <NawaInput
                                                    key={index}
                                                    PK_ModuleField_ID={
                                                        item.PK_ModuleField_ID
                                                    }
                                                    value={newData[item.FieldName]}
                                                    ParentValue={
                                                        item.BCasCade
                                                            ? newData[
                                                                item
                                                                    .FieldNameParent
                                                            ]
                                                            : ''
                                                    }
                                                    name={item.FieldLabel}
                                                    type={item.FK_ExtType_ID}
                                                    dataType={item.FK_FieldType_ID}
                                                    required={item.Required}
                                                    fontColor={oldData?oldData[item.FieldName] === newData[item.FieldName]?'black':'red':''}
                                                />
                                            );
                                        })}
                                </Grid>
                            ) : null}
                        </React.Fragment>
                        )
                        }
                    </Grid>
                    {listModuleDetail &&
                        listModuleDetail.map((item, index) => (
                            <React.Fragment>
                                <Typography
                                    style={{ flex: '1 1 100%' }}
                                    color='inherit'
                                    variant='subtitle1'
                                >
                                    {item.ModuleDetailLabel} Approval Detail
                                </Typography>
                                <TableMasterDetail
                                    list={listModuleDetail[index]}
                                    PK_ModuleApproval_ID={id}
                                />
                            </React.Fragment>
                        ))}
                </CardContent>
                <Divider />
                <CardActions style={{ justifyContent: 'center' }}>
                    <Button
                        variant='contained'
                        size='small'
                        color='primary'
                        onClick={onApprove}
                    >
                        Approve
                    </Button>
                    <Button
                        variant='contained'
                        size='small'
                        color='secondary'
                        onClick={onReject}
                    >
                        Reject
                    </Button>
                    {haveActiveWorkflow ? (
                        <Button
                            variant='contained'
                            size='small'
                            color='secondary'
                            onClick={onRevise}
                        >
                            Revise
                        </Button>
                    ) : null}
                    <Button
                        variant='contained'
                        size='small'
                        color='default'
                        onClick={onCancel}
                    >
                        Cancel
                    </Button>
                </CardActions>
            </Card>
            <Modal
                open={historyOpen}
                onClose={() => {setHistoryOpen(false)}}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box style={{ position: 'absolute', top: '50%', left: '50%', transform: 'translate(-50%, -50%)', width: '70%', bgcolor: 'background.paper', border: '2px solid #000', boxShadow: 24, p: 4, }}>
                    <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                        <NawaTable
                            rowCount={rowCountHistory}
                            moduleName={"Approval History"}
                            actionList={[]}
                            columns={columnsHistory}
                            columnKey={"PK_MWorkflow_History_ID"}
                            rows={dataHistory}
                            getModuleData={getModuleDataHistory}
                            onClickAction={() => {}}
                            onClickAdd={null}
                            onClickDownload={null}
                            onClickDownloadAll={null}
                            onClickDownloadSelected={null}
                            isLoading={isLoading}
                            noAdvFilter
                            noColumnFilter
                        />
                    </Typography>
                </Box>
            </Modal>
        </React.Fragment>
    );
}
