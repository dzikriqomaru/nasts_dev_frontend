import React, { useState, useEffect, useContext } from 'react';
import { Redirect } from 'react-router-dom';
import moment from 'moment';
import { Link as RouterLink } from 'react-router-dom';
import ReactDOMServer from 'react-dom/server';
import useMediaQuery from '@material-ui/core/useMediaQuery';

import {
	APIRequest,
	EActionType,
	EDataType,
	EInputType,
	NawaInput,
	UserProfileContext, useInfoPanel,
	Loader,
	DialogMessage as MsgDialog,
	Constraints,
	Infopanel,
	UnsavedChanges
} from '@vikitheolorado/nawadata-web';

import {
	Card,
	CardContent,
	CardActions,
	CardHeader,
	Button,
	Divider,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Text from 'react-text';
import Typography from '@material-ui/core/Typography';
import KeyboardArrowRightIcon from '@material-ui/icons/KeyboardArrowRight';

const controllerUrl = "api/custom/";

const useStyles = makeStyles((theme) => ({
	root: {
		height: `calc(${theme.contentHeight})`,
		overflowY:'auto'
	},
	container: {
		height: `25vh`,
	},
	content: {
		height: `calc(${theme.contentHeight} - 103px)`,
		overflow: 'auto',
		padding: '5px',
	},
	contentGrid: {
		height: `calc(${theme.contentHeight} - 103px)`,
		oveflow: 'auto',
	},
	errorBox: {
		backgroundColor: '#ffbbbb',
		border: '2px solid #808080',
		borderRadius: '5px',
		margin: '10px 0px',
	},
	errorHeader: {
		padding: '10px',
		fontWeight: 'bold',
	},
	errorList: {
		color: 'black',
	},
	mobileHeader:{
		display:'none'
	},
	'@media screen and (max-width: 600px)':{
		mobileHeader:{
			display:'flex',
			flexDirection:'column',
			paddingTop: theme.spacing(1),
			paddingLeft: theme.spacing(2),
		},
		backMenu:{
			alignItems:'center'
		},
		titleDesc:{
			paddingTop: theme.spacing(1),
			fontSize: '13px',
			fontColor:'#8D9DB9'
		},
		content:{
			overflowX:'hidden',
		},
		saveButton:{
            height:'35px',
            // borderRadius:'16px',
            marginTop: '2%',
            position: 'relative',
            boxShadow: 'none',
			width:'100%'
        },
		cancelButton:{
            height:'35px',
            // borderRadius:'16px',
            marginTop: '2%',
            position: 'relative',
            border: '2px solid #8D9DB9',
            boxShadow: 'none',
            color: '#465672',
			width:'100%'
        },
	},
	'@media screen and (max-width: 1024px) and (min-width: 601px)':{
		content:{
			backgroundColor:'#F2F4F7'
		},
		tabletInputView:{
			width: '90%',
			margin:'auto',
			backgroundColor:'#FFFFFF'
		}
	}
	
}));

export default function ParameterInput(props) {
	const classes = useStyles();
	const { 
        type, 
        moduleid, 
        id, 
        returnLink, 
        historyData,
        invalidErrorMessage,
    } = props.location.state;
	const UserProfile = useContext(UserProfileContext);
	const [isLoading, setIsLoading] = useState(true);
	const [moduleName, setModuleName] = useState('Module');
	const [moduleDbName, setModuleDbName] = useState('Module');
	const [userHasWorkflow, setUserHasWorkflow] = useState(false);
	const [review, setReview] = useState('');
	const [fields, setFields] = useState([]);
	const [columns, setColumns] = useState([]);
	const [columnKey, setColumnKey] = useState(['']);
	const [data, setData] = useState({});
	const [redirect, setRedirect] = useState(null);
	const [focusOnID, setFocusOnID] = useState(null);
	const [errorList, setErrorList] = useState([]);
	const [listModuleDetail, setListModuleDetail] = useState([]);
	const [sendToggle, setSendToggle] = useState(false);
	const [dataDetail, setDataDetail] = useState([]);
	const [dataInitialized, setDataInitialized] = useState(false);
    const [isDataChanged, setIsDataChanged] = useState(false);
    const [showDialog,setShowDialog] = useState(false)
    const [paramRedirect,setParamRedirect] = useState('')
    const [isDiscard, setIsDiscard] = useState(false);
    const [noSaveChanges, setNoSaveChanges] = useState(false);
    const [approval, setApproval] = useState(false);
    const [urlView, setUrlView] = useState('')

	const isMobile = useMediaQuery('(max-width:600px)');
	const tabletMin = useMediaQuery('(min-width:601px)');
    const tabletMax = useMediaQuery('(max-width:1024px)');
    const isTablet = tabletMin && tabletMax;


	const { showInfoPanel } = useInfoPanel();

  

    const onBlock = (tx) => {
        if (isDataChanged && !showDialog) {
            setShowDialog(true);
            setParamRedirect(tx)
        }
        if (!isDataChanged || showDialog) {
            props.history.block(onBlock);
            return true;
        }
        return false;
      };
      
    useEffect(() => {
        props.history.block(onBlock);
    
        if (isDiscard) {
            if( !isDataChanged ) {
                if( paramRedirect ) {
                    setRedirect({
                        pathname: paramRedirect.pathname,
                        params: paramRedirect.params,
                        state: paramRedirect.state 
                    });
                } else {
                    setRedirect({
                        pathname: urlView,
                        params: [{ key: 'ModuleID', value: moduleid }],
                    });
                }
            }
        }

        return () => {
            props.history.block(onBlock);
        }
    }, [isDataChanged])
    
     const handleCancel = (state) => {
        setShowDialog(state);
        setParamRedirect('');
     } 

     useEffect(()=>{
        if(type === EActionType.Update){
            APIRequest.Send({
                method: 'GET',
                url: controllerUrl + 'getSysParam',
                params: {
                    SysParamID: 34
                }
            }, UserProfile)
            .then(res => {
                if(res.data.SettingValue === "1"){
                    setNoSaveChanges(true)
                }
                else{
                    setNoSaveChanges(false)
                }
            })
            .catch(err => {
                showInfoPanel.error(err+'');
            })
        }
     },[])

     const checkSaveChanges = () =>{
         if(noSaveChanges === true){
            if(!isDataChanged){
                return false
            }
            return true;
         }
         return true;
     }

	const GetModuleData = () => {
		return APIRequest.Send(
			{
				method: 'GET',
				url:
					controllerUrl + 'GetModuleDataByID?ModuleID=' + moduleid + '&ID=' + id,
			},
			UserProfile
		);
	};

	const GetModuleDetail = () => {
		APIRequest.Send(
			{
				method: 'GET',
				url:
					controllerUrl + 'GetModuleDetail?ModuleID=' +
					moduleid +
					(type === EActionType.Update ? '&ID=' + id : ''),
			},
			UserProfile
		)
			.then((res) => {
				if (res.status === 200) {
					if (res.data) {
                        setUrlView(res.data.UrlView)
						setModuleName(res.data.ModuleLabel);
						setModuleDbName(res.data.ModuleName);
						setFields(res.data.ModuleFields);
						setUserHasWorkflow(res.data.UserHasWorkflow);
                        setApproval(res.data.IsUseApproval);

						let newData = {};
						let newColumns = res.data.ModuleFields.map((value) => {
							if (value.IsPrimaryKey){ 
                                setColumnKey(value.FieldName);
                            }
                            if (value.FK_ExtType_ID === EInputType.DateField) {
                                newData[value.FieldName] = ''
                            } else {
                                newData[value.FieldName] = value.DefaultValue || ''
                            }
							return value;
						});
						setColumns(newColumns);
						setData(newData);
                        setDataInitialized(true);
						if (type == EActionType.Update) {
							GetModuleData().then((res) => {
								if (res.status === 200) {
									if (res.data) {
										if(res.data.validation){
											if(res.data.validation.length > 0){
												let newErrorList = res.data.validation.map(item => ({PK_ModuleField_ID: item.Key, message: item.Value}));
												setErrorList(newErrorList);
											}
										}
										if (res.data.Dictionary) {
											let editData = res.data.Dictionary;
											Object.keys(newData).forEach((key) => {
												if (
													newColumns.find((value) => value.FieldName === key)
														.FK_ExtType_ID == EInputType.DateField
												) {
													newData[key] = editData[key] ? new Date(editData[key]) : '';
												} else if (
													newColumns.find((value) => value.FieldName === key)
														.FK_FieldType_ID == EDataType.BOOLEAN
												) {
													newData[key] = editData[key] == 1 ? 'true' : 'false';
												} else if (
													newColumns.find((value) => value.FieldName === key)
														.FK_FieldType_ID == EDataType.VARBINARY 
												) {
													newData[key] = {
														fileName: editData[key + 'Name'],
														fileData: editData[key],
													};
												} else {
													newData[key] = editData[key];
												}
											});
											setData({ ...newData });
										}
									}
								}
							});
						}

						//#region Module Detail
						if (
							res.data.ListModuleDetail &&
							res.data.ListModuleDetail.length > 0
						) {
							let newModuleDetails = res.data.ListModuleDetail.map((value) => {
								return {
									moduleDetailID: value.PK_ModuleDetail_ID,
								};
							});
							setListModuleDetail(newModuleDetails);
						}
						//#endregion

						setIsLoading(false);
					} else {
						showInfoPanel.error('Invalid Credential');
					}
				} else {
					showInfoPanel.error('Invalid Credential');
				}
			})
			.catch((err) => showInfoPanel.error(err.response+''))
			.finally(() => setIsLoading(false));
	};

	const onChangeData = (fieldName, value, firstValue) => {
        if (dataInitialized){
            if ((type == 2 && value !== firstValue) || type === 1 ){
                setIsDataChanged(true);
            }
            let newData = data;
            newData[fieldName] = value;
            setData({ ...newData });
        }
	};

	const validateData = () => {
		let errStatus = false;
		let newErrorList = [];

		fields.map((item) => {
			let value = data[item.FieldName];

			switch (item.FK_ExtType_ID) {
				case EInputType.DateField:
                    let validDate = moment(new Date(value), 'DD/MM/YYYY');
					if (!value && item.Required) {
						errStatus = true;
						newErrorList.push({
							message: item.FieldLabel + ' is required',
							PK_ModuleField_ID: item.PK_ModuleField_ID,
						});
					} else if (value && !validDate.isValid()) {
						errStatus = true;
						newErrorList.push({
							message: item.FieldLabel + ' Invalid date format. Please enter the date in the format "DD/MM/YYYY".',
							PK_ModuleField_ID: item.PK_ModuleField_ID,
						});
					}
					break;
                case EInputType.TimeField:
                    let validTime = moment(new Date(value), 'hh/mm/a');
                    if (!value && item.Required) {
						errStatus = true;
						newErrorList.push({
							message: item.FieldLabel + ' is required',
							PK_ModuleField_ID: item.PK_ModuleField_ID,
						});
					} else if (value && !validTime.isValid()) {
						errStatus = true;
						newErrorList.push({
							message: item.FieldLabel + ' Invalid Time format. Please enter the time in the format "hh(hour)/mm(minutes)/a(AM or PM).',
							PK_ModuleField_ID: item.PK_ModuleField_ID,
						});
					}
                    break;
				case EInputType.TextField || EInputType.PasswordField || EInputType.QueryField:
					if (value && item.SizeField > 0 && value.length > item.SizeField) {
						errStatus = true;
						newErrorList.push({
							message:
								item.FieldLabel +
								' is limited to ' +
								item.SizeField +
								' characters',
							PK_ModuleField_ID: item.PK_ModuleField_ID,
						});
					} else if (
						!value &&
						item.Required &&
						item.FK_FieldType_ID !== EDataType.IDENTITY &&
						item.FK_FieldType_ID !== EDataType.BIGIDENTITY
					) {
						errStatus = true;
						newErrorList.push({
							message: item.FieldLabel + ' is required',
							PK_ModuleField_ID: item.PK_ModuleField_ID,
						});
					} else if (item.ListRegex) {
						let regexError = false;
						item.ListRegex.forEach((itemRegex) => {
							let reg = new RegExp(itemRegex.Regex);
							if (!reg.test(value)) {
								regexError = true;
							}
						});
						if (regexError) {
							errStatus = true;
							newErrorList.push({
								message: item.FieldLabel + ' input format is invalid',
								PK_ModuleField_ID: item.PK_ModuleField_ID,
							});
						}
					}
					break;
                case EInputType.RichText:
                    if (
						(!value || value==='<p><br></p>') &&
						item.Required
					) {
						errStatus = true;
						newErrorList.push({
							message: item.FieldLabel + ' ' + document.getElementById('require').innerHTML,
							PK_ModuleField_ID: item.PK_ModuleField_ID,
						});
                    }
                    break;
				default:
					if (
						item.Required &&
						item.FK_FieldType_ID !== EDataType.IDENTITY &&
						item.FK_FieldType_ID !== EDataType.BIGIDENTITY &&
						!value
					) {
						errStatus = true;
						newErrorList.push({
							message: item.FieldLabel + ' is required',
							PK_ModuleField_ID: item.PK_ModuleField_ID,
						});
					}
                    if ( (item.FK_FieldType_ID == EDataType.BIGINT && value < Number.MIN_SAFE_INTEGER)  || (item.FK_FieldType_ID == EDataType.BIGINT &&  value > Number.MAX_SAFE_INTEGER) ) {
                        errStatus = true;
						newErrorList.push({
							message: item.FieldLabel + ' Reach BIGINT Limit from -9,007,199,254,740,991 to 9,007,199,254,740,991',
							PK_ModuleField_ID: item.PK_ModuleField_ID,
						});
                    } else if ( (item.FK_FieldType_ID == EDataType.INT && value < -2147483648) || (item.FK_FieldType_ID == EDataType.INT && value > 2147483647) ) {
                        errStatus = true;
						newErrorList.push({
							message: item.FieldLabel + ' Reach INT Limit from -2,147,483,648 to 2,147,483,647',
							PK_ModuleField_ID: item.PK_ModuleField_ID,
						});
                    }
                    else if ( (item.FK_FieldType_ID == EDataType.SMALLINT && value < -32768) || (item.FK_FieldType_ID == EDataType.SMALLINT && value > 32767) ){
                        errStatus = true;
						newErrorList.push({
							message: item.FieldLabel + ' Reach SMALLINT Limit from -32,768 to 32,767',
							PK_ModuleField_ID: item.PK_ModuleField_ID,
						});
                    }
                    else if ( (item.FK_FieldType_ID == EDataType.TINYINT && value < -0) || (item.FK_FieldType_ID == EDataType.TINYINT && value > 255) ) {
                        errStatus = true;
						newErrorList.push({
							message: item.FieldLabel + ' Reach TINYINT Limit from 0 to 255',
							PK_ModuleField_ID: item.PK_ModuleField_ID,
						});
                    } 
                    else if ( 
                        item.FK_FieldType_ID === EDataType.BIGINT ||
                        item.FK_FieldType_ID === EDataType.INT ||
                        item.FK_FieldType_ID === EDataType.SMALLINT ||
                        item.FK_FieldType_ID === EDataType.TINYINT
                    ) {
                        let pattern = /^[0-9]*$/;
                        let reg = new RegExp(pattern);
                        if (!reg.test(value)) {
                            errStatus = true;
                            newErrorList.push({
                                message: 'incorrect '+ item.FieldLabel + ' format',
                                PK_ModuleField_ID: item.PK_ModuleField_ID,
                            });
                        }
                    }
				    break;
			}
		});

		setErrorList(newErrorList);
		return errStatus;
	};

	const onSave = () => {
        if(checkSaveChanges()){
            setIsDataChanged(false)
            setShowDialog(false)
            let arr = {};
            setDataDetail([]);
            setSendToggle(!sendToggle);
            Object.entries(data).forEach(([key, value]) => {
                if (
                    columns.find((item) => item.FieldName == key).FK_FieldType_ID == EDataType.DATE ||
                    columns.find((item) => item.FieldName == key).FK_FieldType_ID == EDataType.DATETIME
                ) {
                     arr[key] = value ? new Date(value) : ''
                } 
                else if (
                    columns.find((item) => item.FieldName == key).FK_ExtType_ID == EInputType.FileUpload ||
                    (columns.find((item) => item.FieldName == key).FK_ExtType_ID == EInputType.DisplayField &&
                    columns.find((item) => item.FieldName == key).FK_FieldType_ID == EDataType.VARBINARY) 
                ) {
                    arr[key] = value ? String(value.fileData) : '';
                    let columnFileName = key + 'Name';
                    arr[columnFileName] = value ? String(value.fileName) : '';
                } 

                else if ( columns.find((item) => item.FieldName == key).FK_ExtType_ID == EInputType.DropDownField ) {
                    arr[key] = value?String(value):null
                } 
                
                else {
                    arr[key] = String(value);
                }
            });
            if (validateData()) {
                showInfoPanel.error('There are some problems with your input(s). Please check error list on the top of page for more detail.');
            } else {
                setIsLoading(true);
                APIRequest.Send( 
                    {
                        method: 'POST',
                        url: controllerUrl + 'SaveParameterInput',
                        data: {
                            ModuleID: moduleid,
                            ActionType: type,
                            data: arr,
                            dataDetail: dataDetail,
                        },
                    },
                    UserProfile
                )
                    .then((res) => {
                        if (res.status === 200) {
                            if ( UserProfile.FK_MRole_ID != 1 && approval ) {
                                showInfoPanel.success('Pending To Approval')
                            } else {
                                showInfoPanel.success('Data Saved to database')
                            }
                            setIsLoading(false);
                            if (returnLink) {
                                setRedirect({
                                    pathname: returnLink,
                                    data: historyData || '',
                                });
                            }
                            else {
                                setRedirect({
                                    pathname: urlView,
                                    params: [{ key: 'ModuleID', value: moduleid }],
                                });
                            }
                            
                        } else {
                            showInfoPanel.error('Invalid Credential')
                        }
                    })
                    .catch((err) => {
                        if(err.response.status == 400){
                            let newErrorList = err.response.data.map(item => ({PK_ModuleField_ID: item.Key, message: item.Value}));
                            setErrorList(newErrorList);
                            showInfoPanel.error('There are some problems with your input(s). Please check error list on the top of page for more detail.');
                        }else{
                            showInfoPanel.error(err.response.data);   
                        }
                    })
                    .finally(() => setIsLoading(false));
		    }
        }

        else{
            showInfoPanel.error("please make changes before saving");
        }
	};

	const onCancel = () => {
        if (isDataChanged) {
            setShowDialog(true)
        }
        else {
            if (returnLink) {
                setRedirect({
                    pathname: returnLink,
                    data: historyData || '',
                });
            }
            else {
                setRedirect({
                    pathname: urlView,
                    params: [{ key: 'ModuleID', value: moduleid }],
                });
            }
        }

	};
    const handleDiscard = () => {
        setIsDataChanged(false)
        setIsDiscard(true);
    }
    
	// COLLECT DATA FROM CHILD COMPONENT
	const getFromDetail = (value) => {
		let tempData = dataDetail;
		tempData.push(value);
		setDataDetail(tempData);
	};


	useEffect(() => {
		GetModuleDetail();
        if (invalidErrorMessage) {
            showInfoPanel.error(invalidErrorMessage);
        }
	}, []);

    const showChangesDialog = () => {
       return (  
            <UnsavedChanges
                discard={handleDiscard}
                save={onSave}
                cancel={handleCancel}
            />
        )
    }

	if (redirect) return <Redirect to={redirect} />;
	return (

		<Card className={classes.root}>
			{isLoading ? <Loader /> : null}

			<div style = {{display:'none'}}>
				<div id = 'require'>
				<Text validate-required/>
				</div>
			</div>

			<div className = { classes.mobileHeader }>
				<div className = {classes.titleDesc}>
					Add New Record
				</div>

			</div>

			<CardHeader
				titleTypographyProps={{ variant: 'h4' }}
				title={moduleName}
				style={{ minHeight: '56px' }}
			/>

			<Divider />

			<CardContent className={classes.content}>
				<div className = { classes.tabletInputView }>
				{errorList.length > 0 ? (
					<div className={classes.errorBox}>
						<Typography variant='h5' className={classes.errorHeader}>
							<Text validate-err-title/>
						</Typography>
						<List dense={true}>
							{errorList.map((item) => {
								return (
									<ListItem
										key={item.PK_ModuleField_ID}
										button
										onClick={() => setFocusOnID(item.PK_ModuleField_ID)}
									>
										<ListItemIcon>
											<KeyboardArrowRightIcon style={{ color: 'black' }} />
										</ListItemIcon>
										<ListItemText
											primary={
												<Typography variant='h6' style={{ color: 'black' }}>
													{item.message}
												</Typography>
											}
										/>
									</ListItem>
								);
							})}
						</List>
					</div>
				) : (
					<React.Fragment></React.Fragment>
				)}
				
				<div className = { classes.tabletInputView }>
					{columns
						.filter((val) => val.IsShowInForm)
						.map((item, index) => {
							return (
								<NawaInput
									key={index}
									name={item.FieldLabel}
									type={item.FK_ExtType_ID}
									required={item.Required}
                                    IsPrimaryKey={item.IsPrimaryKey}
									value={data[item.FieldName]}
									ParentValue={item.BCasCade ? data[item.FieldNameParent] : ''}
									dataType={item.FK_FieldType_ID}
									PK_ModuleField_ID={item.PK_ModuleField_ID}
									onChange={(value) => {
										onChangeData(item.FieldName, value, data[item.FieldName]);
									}}
									sizeField={item.SizeField ?? 0}
									listRegex={item.ListRegex}
									isFocus={item.PK_ModuleField_ID === focusOnID}
									setFocus={(value) => setFocusOnID(value)}
                                    actionType={type}
                                    moduleName={moduleDbName}
								/>
							);
						})}
				</div>
				

				{listModuleDetail.map((item, index) => {
					return (
						<React.Fragment>
							{
								isMobile || isTablet ? (
									// <ParameterDetailView
                                    //     moduleDetailID={item.moduleDetailID}
                                    //     parentID={id ?? 0}
                                    //     actionType={type}
                                    //     classContainer={classes.container}
                                    //     toggleSend={sendToggle}
                                    //     sendEditedData={(value) => getFromDetail(value)}
                                    //     parentData = {data}
									// />
									<></>
								):(
									// <ParameterDetailView
                                    //     moduleDetailID={item.moduleDetailID}
                                    //     parentID={id ?? 0}
                                    //     actionType={type}
                                    //     classContainer={classes.container}
                                    //     toggleSend={sendToggle}
                                    //     sendEditedData={(value) => getFromDetail(value)}
                                    //     parentData = {data}
									// />
									<></>
									)
							}
						
						</React.Fragment>
					
					);
				})}

				{userHasWorkflow ? (
					<NawaInput
						name={'Review Notes for Reviewer'}
						type={EInputType.TextField}
						required={false}
						value={review}
						dataType={EDataType.VARCHAR}
						onChange={(value) => {
							setReview(value);
                            setIsDataChanged(true);
						}}
                        moduleName={moduleDbName}
					/>
				) : null}
				</div>
			</CardContent>
			<Divider />
			{
				isMobile || isTablet ? 
				(
					<CardActions >
						<div style={{ display:'flex', flexDirection: 'column', width:'100%' }}>
							<Button
								variant='contained'
								size='small'
								color='primary'
								onClick={onSave}
								className = { classes.saveButton }
							>
								<Text save/>
							</Button>
							<Button
								variant='contained'
								size='small'
								color='default'
								onClick={onCancel}
								className = { classes.cancelButton }
							>
								<Text cancel/>
							</Button>
						</div>
						
						{showDialog?showChangesDialog():null}
					</CardActions>
				):
				(
					<CardActions style={{ justifyContent: 'center'}}>
						<Button
							variant='contained'
							size='small'
							color='primary'
							onClick={onSave}
						>
							<Text save/>
						</Button>
						<Button
							variant='contained'
							size='small'
							color='default'
							onClick={onCancel}
						>
							<Text cancel/>
						</Button>
						{showDialog?showChangesDialog():null}
					</CardActions>
				)
			}
			
		</Card>
	);
}
