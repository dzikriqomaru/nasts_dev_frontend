
-- LDAP Type
IF NOT EXISTS (SELECT 1 FROM [dbo].[SystemParameter] WHERE [PK_SystemParameter_ID] = -30)
BEGIN
    INSERT INTO [dbo].[SystemParameter]
               ([PK_SystemParameter_ID]
               ,[FK_SystemParameterGroup_ID]
               ,[SettingName]
               ,[SettingValue]
               ,[Active]
               ,[Hide]
               ,[fk_MFieldType_ID]
               ,[IsEncript]
               ,[EncriptionKey])
         VALUES (-30, 2, 'LDAP Type', '1', 1, 0, 11, 0, '')
END

IF NOT EXISTS (SELECT 1  FROM SystemParameterPickList where ApplicationAuthentication = 'Normal' AND SettingName = 'LDAP Type')
BEGIN
    INSERT INTO SystemParameterPickList VALUES(1,'Normal','LDAP Type')
END

IF NOT EXISTS (SELECT 1 FROM SystemParameterPickList WHERE ApplicationAuthentication = 'API' AND SettingName = 'LDAP Type')
BEGIN
    INSERT INTO SystemParameterPickList VALUES(2,'API','LDAP Type')
END

-- LDAP API URL
IF NOT EXISTS (SELECT 1 FROM [dbo].[SystemParameter] WHERE [PK_SystemParameter_ID] = -31)
BEGIN
    INSERT INTO [dbo].[SystemParameter]
               ([PK_SystemParameter_ID]
               ,[FK_SystemParameterGroup_ID]
               ,[SettingName]
               ,[SettingValue]
               ,[Active]
               ,[Hide]
               ,[fk_MFieldType_ID]
               ,[IsEncript]
               ,[EncriptionKey])
         VALUES (-31, 2, 'LDAP API URL', 'LDAP API URL', 1, 0, 9, 0, '')
END

-- LDAP API client_Id
IF NOT EXISTS (SELECT 1 FROM [dbo].[SystemParameter] WHERE [PK_SystemParameter_ID] = -32)
BEGIN
    INSERT INTO [dbo].[SystemParameter]
               ([PK_SystemParameter_ID]
               ,[FK_SystemParameterGroup_ID]
               ,[SettingName]
               ,[SettingValue]
               ,[Active]
               ,[Hide]
               ,[fk_MFieldType_ID]
               ,[IsEncript]
               ,[EncriptionKey])
         VALUES (-32, 2, 'LDAP API client_Id', 'LDAP API client_Id', 1, 0, 9, 0, '')
END

-- LDAP API scope
IF NOT EXISTS (SELECT 1 FROM [dbo].[SystemParameter] WHERE [PK_SystemParameter_ID] = -33)
BEGIN
    INSERT INTO [dbo].[SystemParameter]
               ([PK_SystemParameter_ID]
               ,[FK_SystemParameterGroup_ID]
               ,[SettingName]
               ,[SettingValue]
               ,[Active]
               ,[Hide]
               ,[fk_MFieldType_ID]
               ,[IsEncript]
               ,[EncriptionKey])
         VALUES (-33, 2, 'LDAP API scope', 'LDAP API scope', 1, 0, 9, 0, '')
END

-- LDAP API grant_type
IF NOT EXISTS (SELECT 1 FROM [dbo].[SystemParameter] WHERE [PK_SystemParameter_ID] = -34)
BEGIN
    INSERT INTO [dbo].[SystemParameter]
               ([PK_SystemParameter_ID]
               ,[FK_SystemParameterGroup_ID]
               ,[SettingName]
               ,[SettingValue]
               ,[Active]
               ,[Hide]
               ,[fk_MFieldType_ID]
               ,[IsEncript]
               ,[EncriptionKey])
         VALUES (-34, 2, 'LDAP API grant_type', 'LDAP API grant_type', 1, 0, 9, 0, '')
END

-- LDAP API client_secret
IF NOT EXISTS (SELECT 1 FROM [dbo].[SystemParameter] WHERE [PK_SystemParameter_ID] = -35)
BEGIN
    INSERT INTO [dbo].[SystemParameter]
               ([PK_SystemParameter_ID]
               ,[FK_SystemParameterGroup_ID]
               ,[SettingName]
               ,[SettingValue]
               ,[Active]
               ,[Hide]
               ,[fk_MFieldType_ID]
               ,[IsEncript]
               ,[EncriptionKey])
         VALUES (-35, 2, 'LDAP API client_secret', 'LDAP API client_secret', 1, 0, 9, 0, '')
END

-- LDAP API device
IF NOT EXISTS (SELECT 1 FROM [dbo].[SystemParameter] WHERE [PK_SystemParameter_ID] = -36)
BEGIN
    INSERT INTO [dbo].[SystemParameter]
               ([PK_SystemParameter_ID]
               ,[FK_SystemParameterGroup_ID]
               ,[SettingName]
               ,[SettingValue]
               ,[Active]
               ,[Hide]
               ,[fk_MFieldType_ID]
               ,[IsEncript]
               ,[EncriptionKey])
         VALUES (-36, 2, 'LDAP API device', 'LDAP API device', 1, 0, 9, 0, '')
END

SELECT * FROM SystemParameter 
WHERE PK_SystemParameter_ID = -30 
OR PK_SystemParameter_ID = -31 
OR PK_SystemParameter_ID = -32 
OR PK_SystemParameter_ID = -33
OR PK_SystemParameter_ID = -34
OR PK_SystemParameter_ID = -35
OR PK_SystemParameter_ID = -36

SELECT *  FROM SystemParameterPickList 
where SettingName = 'LDAP Type'

GO