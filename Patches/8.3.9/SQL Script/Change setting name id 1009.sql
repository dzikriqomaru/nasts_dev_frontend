
-- Variable to store the count of records to be updated
DECLARE @count INT;

-- Check if the record exists in SystemParameter
SELECT @count = COUNT(*) 
FROM SystemParameter 
WHERE PK_SystemParameter_ID = 1009;

-- Update the record if it exists
IF @count > 0
BEGIN
    UPDATE SystemParameter
    SET SettingName = 'Check IP Address'
    WHERE PK_SystemParameter_ID = 1009;
END
ELSE
BEGIN
    -- Handle the case where the record does not exist
    -- For example, print an error message or log the issue
    PRINT 'Record with PK_SystemParameter_ID = 1009 does not exist in SystemParameter.';
END

-- Check if the record exists in SystemParameterPickList
SELECT @count = COUNT(*)
FROM SystemParameterPickList
WHERE SettingName = 'Allow Multiple Login';

-- Update the record if it exists
IF @count > 0
BEGIN
    UPDATE SystemParameterPickList
    SET SettingName = 'Check IP Address'
    WHERE SettingName = 'Allow Multiple Login';
END
ELSE
BEGIN
    -- Handle the case where the record does not exist
    -- For example, print an error message or log the issue
    PRINT 'Record with SettingName = ''Allow Multiple Login'' does not exist in SystemParameterPickList.';
END
