/****** Object:  StoredProcedure [dbo].[usp_GetListOptions]    Script Date: 05/12/2023 16:19:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROC [dbo].[usp_GetListOptions]
	@PK_ModuleField_ID BIGINT,
	@ParentValue VARCHAR(MAX),
	@UserID VARCHAR(50),
	@SearchKeyword VARCHAR(MAX) = ''
	--05/12/2023 penambahan with no lock di beberapa module terkadang lock
AS
DECLARE @TableReferenceName VARCHAR(250),
		@TableReferenceNameAlias VARCHAR(250),
		@TableReferenceFieldKey VARCHAR(250),
		@TableReferenceFieldDisplayName VARCHAR(250),
		@TableReferenceFilter VARCHAR(1000),
		@TableReferenceAdditonalJoin VARCHAR(1000),
		@FilterCascade VARCHAR(1000),
		@SearchWord	VARCHAR(MAX) = '''%' + REPLACE(@SearchKeyword, '''', '''''') + '%''',
		@SQLQuery NVARCHAR(MAX),
		@Params NVARCHAR(MAX) = '@Parent VARCHAR(MAX)'

SELECT @TableReferenceName = TabelReferenceName,
		@TableReferenceNameAlias = TabelReferenceNameAlias,
		@TableReferenceFieldKey = TableReferenceFieldKey,
		@TableReferenceFieldDisplayName = TableReferenceFieldDisplayName,
		@TableReferenceFilter = TableReferenceFilter,
		@TableReferenceAdditonalJoin = TableReferenceAdditonalJoin,
		@FilterCascade = CASE WHEN BCasCade = 1 THEN FilterCascade ELSE '' END
FROM dbo.ModuleField
WHERE PK_ModuleField_ID = @PK_ModuleField_ID

SET @TableReferenceNameAlias = CASE WHEN ISNULL(@TableReferenceNameAlias, '') = '' THEN @TableReferenceName ELSE @TableReferenceNameAlias END

SET @SQLQuery = 'SELECT ' + @TableReferenceNameAlias + '.' + @TableReferenceFieldKey + ' AS [value],' + CHAR(10) + CHAR(13) +
				'		' + @TableReferenceNameAlias + '.' + @TableReferenceFieldDisplayName + ' AS [text]' + CHAR(10) + CHAR(13) +
				'FROM ' + @TableReferenceName + ' AS ' + @TableReferenceNameAlias + CHAR(10) + ' WITH (NOLOCK) ' + CHAR(13) +

				-- search
				'WHERE (' + @TableReferenceNameAlias + '.' + @TableReferenceFieldKey + ' LIKE ' + @SearchWord + 
				' OR '+ @TableReferenceNameAlias + '.' + @TableReferenceFieldDisplayName + ' LIKE ' + @SearchWord + ') ' +

				-- filter
				CASE WHEN ISNULL(@TableReferenceFilter, '') <> ''
					 THEN 'AND ' + REPLACE(@TableReferenceFilter, '@UserID', @UserID) +
							CASE WHEN ISNULL(@FilterCascade, '') <> ''
								 THEN ' AND ' + @FilterCascade
								 ELSE ''
							END
					 ELSE CASE WHEN ISNULL(@FilterCascade, '') <> ''
							   THEN 'AND ' + @FilterCascade
							   ELSE ''
						  END
				END
				PRINT(@SQLQuery);
EXEC sp_executesql @SQLQuery, @Params, @ParentValue
