ALTER PROCEDURE [dbo].[usp_ValidateLHBUFormData_New]  
/***********************************************************  
* Procedure description:  
* Date:   10/4/2016   
* Author: Hendra  
*  
* Changes  
* Date        Modified By                 Comments  
************************************************************  
*  
************************************************************/  
(  
       @xmlData XML, @moduleID INT ,@userid VARCHAR(50)  
)  
AS  
BEGIN  
         
         
         
         
  
--DECLARE     @xmlData XML, @moduleID INT,@userid VARCHAR(50)  
         
         
/*  
--SET @xmlData=N'<Data><D01_DebiturIndividu><CIF>06867051515810000179</CIF><FlagDetail>D</FlagDetail><JenisIdentitas>1</JenisIdentitas>  
<NikatauPassport>4545</NikatauPassport><NamaSesuaiIdentitas>QUWXV167143</NamaSesuaiIdentitas><NamaLengkap>QUWXV167143</NamaLengkap>  
<FK_KodeStatusatauGelasDebitur>00</FK_KodeStatusatauGelasDebitur><JenisKelamin>P</JenisKelamin><TempatLahirKTP>RYNKB167143</TempatLahirKTP>  
<TanggalLahir>18990101</TanggalLahir><NPWP>63249072020</NPWP><Alamat>LRBVQ167143</Alamat><Kelurahan>QAFNV167143</Kelurahan>  
<Kecamatan>FGRLW167143</Kecamatan><FK_KodeKabatauKota>3396</FK_KodeKabatauKota><KodePos>20231</KodePos><Telepon>62  61453541</Telepon>  
<NomorTeleponGenggam /><AlamatEmail /><FK_KodeNegaraDomisili>ID</FK_KodeNegaraDomisili><FK_KodePekerjaan>014</FK_KodePekerjaan>  
<TempatBekerja>UD MUDA JAYA</TempatBekerja><FK_KodeBidangUsahaTempatBekerjaDebitur>012192</FK_KodeBidangUsahaTempatBekerjaDebitur><AlamatTempatBekerja />  
<PenghasilanKotorPertahun>0</PenghasilanKotorPertahun><FK_KodeSumberPenghasilan /><JumlahTanggungan>0</JumlahTanggungan>  
<FK_KodeHubungandenganLJK>9900</FK_KodeHubungandenganLJK><FK_KodeGolonganDebitur /><StatusPerkawinanDebitur /><NikatauPassportPasangan />  
<NamaPasangan>Nama Pasangan 06867051515810000179</NamaPasangan><TanggalLahirPasangan /><PerjanjianPisahHarta />  
<MelanggarBMPKatauBMPDatauBMPP>T</MelanggarBMPKatauBMPDatauBMPP><MelampauiBMPKatauBMPDatauBMPP>T</MelampauiBMPKatauBMPDatauBMPP>  
<NamaGadisIbuKandung>NQUOI167143</NamaGadisIbuKandung><FK_KodeKantorCabang>124</FK_KodeKantorCabang><OperasiData>C</OperasiData>  
<FK_KodeJenisLJK>0101</FK_KodeJenisLJK><FK_KodeLJK>000045</FK_KodeLJK><Tahun>2016</Tahun><Bulan>05</Bulan><KodeSandiBI />  
<PK_D01_DebiturIndividu>7648</PK_D01_DebiturIndividu><AccessCode /><Tambahan /><CreatedBy>System</CreatedBy><LastUpdateBy>Sysadmin</LastUpdateBy>  
<ApprovedBy>Sysadmin</ApprovedBy><CreatedDate>2016-08-17 18:44:54</CreatedDate><LastUpdateDate>2016-10-05 14:48:23</LastUpdateDate>  
<ApprovedDate>2016-10-05 14:48:23</ApprovedDate></D01_DebiturIndividu></Data>'  
--set @moduleID=3086  
--SET @userid='sysadmin'  
*/  
         
       DECLARE @kolom VARCHAR(MAX)  
       DECLARE @tablename VARCHAR(500)  
         
       DEclare @IsLHBUForm int = 0--(select count(1) from ORS_FormInfo where FK_Module_ID = @moduleID)  
SELECT   
     
  @kolom =ISNULL(  
   STUFF((SELECT ', [' + US.FieldName + '] varchar(8000)'  
          FROM modulefield  US  
          WHERE US.FK_Module_ID= m.PK_Module_ID  
          ORDER BY us.Sequence  
          FOR XML PATH('')), 1, 1, ''),'')  ,@tablename =m.ModuleName  
FROM Module m  
WHERE m.PK_Module_ID=@moduleID  
  
  
  
       DECLARE @kolomIsi VARCHAR(MAX)  
  
SELECT   
     
  @kolomIsi= ISNULL(  
   STUFF((SELECT ', NULLIF([' + US.FieldName + '],''NULL'') [' + US.FieldName + ']'  
          FROM modulefield  US  
          WHERE US.FK_Module_ID= m.PK_Module_ID  
          ORDER BY us.Sequence  
          FOR XML PATH('')), 1, 1, ''),'')  ,@tablename =m.ModuleName  
FROM Module m  
WHERE m.PK_Module_ID=@moduleID  
  
  
  
  
DECLARE @sql VARCHAR(MAX) =''  
DECLARE @sql1 VARCHAR(MAX)=''  
DECLARE @sql2 VARCHAR(MAX)=''  
DECLARE @sql3 VARCHAR(MAX)=''  
DECLARE @sql4 VARCHAR(MAX)=''  
  
  
set @sql =' DECLARE @handle INT ' + CHAR(10)  
          +' DECLARE @PrepareXmlStatus INT  ' + CHAR(10)  
                +' DECLARE @XMLData XML ' + CHAR(10)  
  
set @sql =@sql       +  'SET @XMLDATA='''+CONVERT(VARCHAR(MAX),@xmlData) +''''+CHAR(10)               
  
set @sql =@sql       +  ' EXEC @PrepareXmlStatus= sp_xml_preparedocument @handle OUTPUT, @XMLDATA '+CHAR(10)  
  
  
  
  
SET @sql1 =@sql1+ ' IF OBJECT_ID('''+@tablename+'_'+@userid+''', ''U'') IS NOT NULL  ' + char(10)  
         + '  DROP TABLE '+@tablename+'_'+@userid+';  ' + char(10)  
           
  
set @sql1 =@sql1     +  ' SELECT '+@kolomIsi+' into '+@tablename+'_'+@userid+' FROM    OPENXML(@handle, ''/Data/'+@tablename+''', 2)   '+CHAR(10)  
set @sql1 =@sql1     +  ' WITH ( '+CHAR(10)  
set @sql1 =@sql1     + @kolom +CHAR(10)  
set @sql1 =@sql1     +  ' )  '+CHAR(10)  
  
  
  
  
SET @sql1 =@sql1+ ' IF OBJECT_ID(''ValidationReport_'+ @userid +''', ''U'') IS NOT NULL  ' + char(10)  
         + '  DROP TABLE ValidationReport_'+@userid+';  ' + char(10)  
         + ' SELECT * INTO ValidationReport_'+@userid+' FROM ValidationReport vr WHERE 1=2'  
  
  
  
  
  
        
SET @sql2 = ' ' + char(10)  
         + '  ' + char(10)  
         + '  DECLARE @SQL varchar(max), @ValidationExpression     VARCHAR(MAX), ' + char(10)  
         + '          @ValidationType           VARCHAR(50), ' + char(10)  
         + '          @ValidationMessage        VARCHAR(8000), ' + char(10)  
         + '          @ExpressionType           VARCHAR(50),           ' + char(10)  
         + '          @FieldName                VARCHAR(100), ' + char(10)  
         + '          @ModuleName               VARCHAR(200), ' + char(10)  
         + '          @ModuleLabel              VARCHAR(200), ' + char(10)  
         + '          @ModuleURL                VARCHAR(5000), ' + char(10)  
         + '          @FieldLabel               VARCHAR(200), ' + char(10)  
         + '          @KeyField                 VARCHAR(500), ' + char(10)  
         + '          @KeyValue                 VARCHAR(500), ' + char(10)  
         + '          @ErrorType                VARCHAR(50), ' + char(10)  
         + '          @pk                       VARCHAR(1000) ,' + char(10)  
         + '          @moduleID                       VARCHAR(1000) ' + char(10)  
         + ' set @moduleID = '+ CONVERT(VARCHAR(20),@moduleID) +CHAR(10)                      
           
         + '                ' + char(10)  
         + 'DECLARE my_cursor CURSOR FAST_FORWARD READ_ONLY  ' + char(10)  
         + '  FOR ' + char(10)  
         + '      SELECT vp.PK_ValidationParameter, ' + char(10)  
         + '             vp.ValidationExpression, ' + char(10)  
         + '             vp.ValidationType, ' + char(10)  
         + '             REPLACE(vp.ValidationMessage, '''''''', '''''''''''') ValidationMessage, ' + char(10)  
         + '             vp.ExpressionType, ' + char(10)  
         + '             vp.TableName, ' + char(10)  
         + '             vp.FieldName, ' + char(10)  
         + '             m.ModuleName, ' + char(10)  
         + '             m.ModuleLabel, ' + char(10)  
         + '             ISNULL(m.UrlEdit, '''')         UrlEdit, ' + char(10)  
         + '             vp.FieldName, ' + char(10)  
         + '             ISNULL(mf.FieldName,'''')     KeyField, ' + char(10)  
         + '             ISNULL(mf.FieldName,'''')     KeyValue, ' + char(10)  
         + '             vp.ErrorType ' + char(10)  
         + '      FROM   ValidationParameter        AS vp ' + char(10)  
         + '             LEFT JOIN Module           AS m ' + char(10)  
         + '                  ON  m.PK_Module_ID = vp.TableName ' + char(10)        
         + '             LEFT JOIN ModuleField AS mf ' + char(10)  
         + '                  ON  m.PK_Module_ID = mf.FK_Module_ID ' + char(10)  
         + '					  AND mf.IsPrimaryKey = 1 ' + char(10)  
         + '      WHERE  vp.[Active] = 1 AND vp.TableName=@moduleID ' + char(10)  
         + '  ' + char(10)  
         + '  OPEN my_cursor ' + char(10)  
         + '  ' + char(10)  
         + '  FETCH FROM my_cursor INTO @pk, @ValidationExpression,@ValidationType,@ValidationMessage,@ExpressionType,@ModuleID,@FieldName, ' + char(10)  
         + ' @ModuleName,@ModuleLabel,@ModuleURL,@FieldLabel,@KeyField,@KeyValue,@ErrorType ' + char(10)  
         + '  ' + char(10)  
         + '  WHILE @@FETCH_STATUS = 0 ' + char(10)  
         + '  BEGIN ' + char(10)  
         + '      PRINT @ExpressionType ' + char(10)  
         + '       ' + char(10)  
         + '      IF @ExpressionType = ''EXPRESSION'' ' + char(10)  
         + '      BEGIN ' + char(10)  
         + '           ' + char(10)  
        SET @sql3=   '              SET @SQL = ''INSERT INTO ValidationReport_'+@userid+''' ' + char(10)  
         + '              + CHAR(10) + ' + char(10)  
         + '              ''([SegmentData],[NamaField],[ValidationMessage],[Active],[CreatedBy],[LastUpdateBy],[ApprovedBy]'' ' + char(10)  
         + '              + CHAR(10) + '',[CreatedDate],[LastUpdateDate],[ApprovedDate],TanggalData, [KeyField],[KeyFieldValue]'' ' + char(10)  
         + '              + CHAR(10) + ' + char(10)  
         + '              '',[ModuleURL],[RecordID],[ModuleID],[FK_ValidationType],ErrorType,FieldValue) '' ' + char(10)  
         + '              + CHAR(10) + ''SELECT'' ' + char(10)  
         + '              + CHAR(10) + '''''''' + @ModuleLabel + '''''','''''' + @FieldLabel + '''''', '''''' + @ValidationMessage + ' + char(10)  
         + '              '''''', 1, ''''Sysadmin'''',''''Sysadmin'''',''''Sysadmin'''''' ' + char(10)  
         + '              + CHAR(10) + '',GETDATE(),GETDATE(),GETDATE(),'+case @IsLHBUForm when 0 then '''''1900-01-01''''' else 'Report_Date' end +', '''''' + @KeyField + '''''', '' + REPLACE(@KeyValue, ''cln.'', '''')  ' + char(10)  
         + '              + '','' ' + char(10)  
         + '              + CHAR(10) + '''''''' + @ModuleURL + '''''', '' +  @KeyField + '', '''''' + @ModuleID + '''''','' + @ValidationType ' + char(10)  
         + '              + '','''''' + @ErrorType + '''''', hd.''+ @FieldName +'''' ' + char(10)  
         + '              + CHAR(10) + ''FROM '+@tablename+'_'+@userid+' hd''  ' + char(10)  
         + '              + CHAR(10) + @ValidationExpression ' + char(10)  
         + '                ' + char(10)  
         + '           ' + char(10)  
         + '         PRINT @SQL ' + char(10)  
         + '          EXEC (@SQL) ' + char(10)  
         + '          PRINT ''ID '' + @pk ' + char(10)  
         + '      END ' + char(10)  
         + '      ELSE  ' + char(10)  
         + '      IF @ExpressionType = ''FULL QUERY'' ' + char(10)  
         + '      BEGIN ' + char(10)  
         + '          PRINT ''ENTER FULL QUERY'' ' + char(10)  
         + '          PRINT ''ID '' + @pk ' + char(10)  
         + '           ' + char(10)  
         + ' ' + char(10)  
         + '         set @ValidationExpression =REPLACE(@ValidationExpression,''[dbo].[ValidationReport]'',''ValidationReport_'+@userid+''') ' + char(10)  
         + '         set @ValidationExpression =REPLACE(@ValidationExpression,''dbo.ValidationReport'',''ValidationReport_'+@userid+''') ' + char(10)                    
         + '                          ' + char(10)  
         + '           ' + char(10)  
         + '    --      PRINT @ValidationExpression ' + char(10)  
         + '          EXEC (@ValidationExpression) ' + char(10)  
         + '      END ' + char(10)  
         + '       ' + char(10)  
         + '      FETCH FROM my_cursor INTO @pk, @ValidationExpression,@ValidationType,@ValidationMessage,@ExpressionType,@ModuleID, ' + char(10)  
         + '      @FieldName, ' + char(10)  
         + '      @ModuleName,@ModuleLabel,@ModuleURL,@FieldLabel,@KeyField,@KeyValue,@ErrorType ' + char(10)  
         + '  END ' + char(10)  
         + '  ' + char(10)  
         + '  CLOSE my_cursor ' + char(10)  
         + '  DEALLOCATE my_cursor'  
                
SET @sql4 = ' ' + char(10)  
         + 'SELECT us.NamaField FieldName,   ' + char(10)  
         + '       US.ValidationMessage  ' + char(10)  
         + ' FROM ValidationReport_' + @userid + ' US  ' + char(10)  
         + '      INNER JOIN ValidationType vt  ' + char(10)  
         + '      ON  us.FK_ValidationType = vt.PK_ValidationType_ID  ' + char(10)  
         + '      INNER JOIN ModuleField mf ON us.ModuleId = mf.Fk_Module_Id AND mf.FieldName = us.NamaField ' + char(10)  
         + ' ORDER BY  ' + char(10)  
         + '      mf.Sequence, us.SegmentData,  ' + char(10)  
         + '      us.RecordID'  
           
-- PRINT @sql PRINT @sql1 PRINT @sql2 PRINT @sql3 PRINT @sql4          
EXEC(@sql +@sql1+ @sql2 + @sql3 + @sql4)  
  
END 