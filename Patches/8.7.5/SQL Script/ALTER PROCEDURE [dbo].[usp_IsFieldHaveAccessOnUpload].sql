ALTER PROCEDURE [dbo].[usp_IsFieldHaveAccessOnUpload] 
/* =============================================
-- Author:		Dandi Juliandi
-- Create date: 19 April 2024
-- Description:	Check Field Access on Upload
-- =============================================
*
SP ini Digunakan di:
1. usp_SaveUploadApproval
2. usp_SaveUploadData

* Changes
* Date		Modified By			Comments
************************************************************
* 
************************************************************/
	@FieldName VARCHAR(250) = '',
    @FK_Module_ID BIGINT = 0,
	@userid VARCHAR(250) ='',
	@bAdd  bit = 0 Output ,
	@bEdit bit = 0 Output
AS
BEGIN
	SELECT 
		@bAdd = ISNULL(mga.bAdd, 1), 
		@bEdit = ISNULL(mga.bEdit, 1)
	FROM MGroupMenuAccessField mga
		JOIN MUser mus on mga.FK_MGroupMenu_ID = mus.FK_MGroupMenu_ID and mus.UserID = @userid
		RIGHT JOIN ModuleField mf ON (mf.FieldName = mga.ModuleField AND mf.FK_Module_ID = mga.FK_Module_ID AND mga.FK_MGroupMenu_ID = mus.FK_MGroupMenu_ID) 
	WHERE mf.FK_Module_ID = @FK_Module_ID AND(mga.FK_MGroupMenu_ID = mus.FK_MGroupMenu_ID OR mga.FK_MGroupMenu_ID IS NULL)
		AND mf.FieldName = @FieldName
END
