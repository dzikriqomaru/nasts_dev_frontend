
ALTER VIEW [dbo].[VW_MUser]
AS
SELECT
    PK_MUser_ID, 
  UserID, 
  UserName, 
  RoleName, 
  GroupMenuName, 
  UserEmailAddress, 
  MUser.Active,
  MUser.LastLogin,
  MUser.IsUseLDAP
FROM dbo.MUser
JOIN dbo.MRole
     ON FK_MRole_ID = PK_MRole_ID
JOIN dbo.MGroupMenu
     ON FK_MGroupMenu_ID = PK_MGroupMenu_ID
GO
