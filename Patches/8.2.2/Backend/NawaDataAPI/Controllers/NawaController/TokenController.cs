﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using NawaDataBLL;
using Microsoft.AspNetCore.Authorization;
using ITfoxtec.Identity.Saml2;
using Microsoft.Extensions.Options;
using ITfoxtec.Identity.Saml2.MvcCore;
using ITfoxtec.Identity.Saml2.Schemas;
using System.Security.Authentication;
using NawaDataDAL.Models;
using Scorpion;
using Microsoft.Data.SqlClient;
using System.Data;
using NawaDataDAL;

namespace NawaDataAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TokenController : ControllerBase
    {
        const string relayStateReturnUrl = "ReturnUrl";
        private readonly Saml2Configuration config;
        public IConfiguration _configuration;

        public TokenController(IConfiguration conf, IOptions<Saml2Configuration> configAccessor)
        {
            _configuration = conf;

            try
            {
                config = configAccessor.Value;

            }
            catch
            { 
                
            }
        }

        [HttpGet("LoginSAML")]
        public IActionResult Login()
        {
            try
            {
                var binding = new Saml2RedirectBinding();
                binding.SetRelayStateQuery(new Dictionary<string, string> { { relayStateReturnUrl, Url.Content("~/") } });

                return binding.Bind(new Saml2AuthnRequest(config)).ToActionResult();
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("AssertionConsumerService")]
        public IActionResult AssertionConsumerService()
        {
            try
            {
                var binding = new Saml2PostBinding();
                var saml2AuthnResponse = new Saml2AuthnResponse(config);

                binding.ReadSamlResponse(Request.ToGenericHttpRequest(), saml2AuthnResponse);
                if (saml2AuthnResponse.Status != Saml2StatusCodes.Success)
                {
                    throw new AuthenticationException($"SAML Response status: {saml2AuthnResponse.Status}");
                }
                //binding.Unbind(Request.ToGenericHttpRequest(), saml2AuthnResponse);
                //await saml2AuthnResponse.CreateSession(HttpContext, claimsTransform: (claimsPrincipal) => ClaimsTransform.Transform(claimsPrincipal));

                //var relayStateQuery = binding.GetRelayStateQuery();
                //var returnUrl = relayStateQuery.ContainsKey(relayStateReturnUrl) ? relayStateQuery[relayStateReturnUrl] : Url.Content("~/");
                //return Redirect(returnUrl);

                #region Get Client's IP Address
                string ipString = "";
                try
                {
                    System.Net.IPAddress remoteIpAddress = Request.HttpContext.Connection.RemoteIpAddress;
                    if (remoteIpAddress != null)
                    {
                        // If we got an IPV6 address, then we need to ask the network for the IPV4 address 
                        // This usually only happens when the browser is on the same machine as the server.
                        if (remoteIpAddress.AddressFamily == System.Net.Sockets.AddressFamily.InterNetworkV6)
                        {
                            remoteIpAddress = System.Net.Dns.GetHostEntry(remoteIpAddress).AddressList
                                .First(x => x.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork);
                        }

                        ipString = remoteIpAddress.ToString();
                    }
                }
                catch (Exception ex)
                {
                    ElmahErrorBLL.Log(ex, "");
                }
                #endregion

                MUserView userValidation = UserBLL.GetUserByUserID(saml2AuthnResponse.ClaimsIdentity.Name);

                UserBLL.ChangeUserUsedStatus(userValidation.PK_MUser_ID, true);
                AuditTrailBLL.PostAuditTrailUserLogin(userValidation.UserID, "Login", ipString, "Login Success");

                Claim[] claims = new Claim[]
                {
                new Claim(JwtRegisteredClaimNames.Sub, _configuration["Jwt:Subject"]),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.Iat, DateTime.Now.ToString()),
                new Claim("PK_MUser_ID", userValidation.PK_MUser_ID.ToString()),
                new Claim("UserID", userValidation.UserID),
                new Claim("UserName", userValidation.UserName),
                new Claim("FK_MRole_ID", userValidation.FK_MRole_ID.ToString()),
                new Claim("FK_MGroupMenu_ID", userValidation.FK_MGroupMenu_ID.ToString()),
                new Claim("UserEmailAddress", userValidation.UserEmailAddress),
                new Claim("AlternateUser", "")
                };

                //Get session expiration time
                NawaDataDAL.Models.SystemParameter expSysParam = SystemParameterBLL.GetSystemParameterValue(17);
                int tokenExpiration = expSysParam == null ? 60 :
                    string.IsNullOrEmpty(expSysParam.SettingValue) ? 60 : Convert.ToInt32(expSysParam.SettingValue);

                SymmetricSecurityKey key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]));
                SigningCredentials signIn = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
                JwtSecurityToken token = new JwtSecurityToken(_configuration["Jwt:Issuer"], _configuration["Jwt:Issuer"], claims, expires: DateTime.Now.AddMinutes(tokenExpiration), signingCredentials: signIn);

                string finalToken = new JwtSecurityTokenHandler().WriteToken(token);
                //string webURL = SystemParameterBLL.GetSystemParameterValue(-59).SettingValue;
                string webURL = "http://localhost:3000";

                return Redirect(webURL + "?url=/&token=" + finalToken);
                //return Ok(new JwtSecurityTokenHandler().WriteToken(token));
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPost]
        public async Task<ActionResult> Post(UserInfo _userData)
        {
            try
            {

                if (_userData != null && _userData.UserName != null && _userData.Password != null)
                {
                    #region Get Client's IP Address
                    string ipString = "";
                    try
                    {
                        System.Net.IPAddress remoteIpAddress = Request.HttpContext.Connection.RemoteIpAddress;
                        if (remoteIpAddress != null)
                        {
                            // If we got an IPV6 address, then we need to ask the network for the IPV4 address 
                            // This usually only happens when the browser is on the same machine as the server.
                            if (remoteIpAddress.AddressFamily == System.Net.Sockets.AddressFamily.InterNetworkV6)
                            {
                                remoteIpAddress = System.Net.Dns.GetHostEntry(remoteIpAddress).AddressList
                                    .First(x => x.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork);
                            }

                            ipString = remoteIpAddress.ToString();
                        }
                    }
                    catch(Exception ex)
                    {
                        ElmahErrorBLL.Log(ex,"");
                    }
                    #endregion

                    _userData.Password = EncryptionBLL.DecryptStringAES(_userData.Password);
                    NawaDataDAL.Models.UserLoginValidation userValidation = await UserBLL.Login(_userData.UserName, _userData.Password, ipString);

                    if (userValidation != null && userValidation.IsValid && userValidation.User != null)
                    {
                        if (!(UserBLL.IsEnableOTP() && UserBLL.IsRoleNotInExcludeOTP(userValidation.User.FK_MRole_ID)))
                        {
                            DateTime now = DateTime.Now.Date;
                            DateTime last = userValidation.User.LastChangePassword.Date;
                            double totalDay = (now - last).Days;
                            int expiredDay = Convert.ToInt32(SystemParameterBLL.GetAppSystemParameter(1000).SettingValue);

                            //unchange inused if user expired = true
                            if (Convert.ToInt32(totalDay) <= expiredDay || Convert.ToString(userValidation.User.LastChangePassword) == "01/01/1753 01:01:01")
                            {
                                UserBLL.ChangeUserUsedStatus(userValidation.User.PK_MUser_ID, true);
                            }
                        }
                        UserBLL.ChangeUserIP(userValidation.User.PK_MUser_ID, ipString);
                        AuditTrailBLL.PostAuditTrailUserLogin(userValidation.User.UserID, "Login", ipString, "Login Success");
                        
                        //Create claims details based on the user information
                        Claim[] claims = new Claim[]
                        {
                            new Claim(JwtRegisteredClaimNames.Sub, _configuration["Jwt:Subject"]),
                            new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                            new Claim(JwtRegisteredClaimNames.Iat, DateTime.Now.ToString()),
                            new Claim("PK_MUser_ID", userValidation.User.PK_MUser_ID.ToString()),
                            new Claim("UserID", userValidation.User.UserID),
                            new Claim("UserName", userValidation.User.UserName),
                            new Claim("FK_MRole_ID", userValidation.User.FK_MRole_ID.ToString()),
                            new Claim("FK_MGroupMenu_ID", userValidation.User.FK_MGroupMenu_ID.ToString()),
                            new Claim("UserEmailAddress", userValidation.User.UserEmailAddress),
                            new Claim("AlternateUser", "")
                        };

                        //Get session expiration time
                        NawaDataDAL.Models.SystemParameter expSysParam = SystemParameterBLL.GetSystemParameterValue(17);
                        int tokenExpiration = expSysParam == null ? 60 :
                            string.IsNullOrEmpty(expSysParam.SettingValue) ? 60 : Convert.ToInt32(expSysParam.SettingValue);

                        SymmetricSecurityKey key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]));
                        SigningCredentials signIn = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
                        JwtSecurityToken token = new JwtSecurityToken(_configuration["Jwt:Issuer"], _configuration["Jwt:Issuer"], claims, expires: DateTime.Now.AddMinutes(tokenExpiration), signingCredentials: signIn);
                   

                        return Ok(new JwtSecurityTokenHandler().WriteToken(token));
                    }
                    else
                    {
                        string errFrontend = "";
                        string errBackend = "";
                        if (userValidation != null)
                        {
                            if ((userValidation.Message == null) && (userValidation.User != null))
                            {
                                SystemParameter parameter = SystemParameterBLL.GetSystemParameterValue(9004);
                                errBackend = parameter.SettingName;
                                errFrontend = parameter.SettingValue;
                            }
                            else if ((userValidation.Message != null) && (userValidation.User == null))
                            {
                                SystemParameter parameter = SystemParameterBLL.GetSystemParameterValue(9005);
                                errBackend = parameter.SettingName;
                                errFrontend = parameter.SettingValue;
                            }
                            else
                            {
                                errBackend = userValidation.Message;
                                errFrontend = userValidation.Message;
                            }
                        }
                        AuditTrailBLL.PostAuditTrailUserLogin(_userData.UserName, "Try Login", ipString, errBackend);
                        return BadRequest(errFrontend);
                    }
                }
                else
                {
                    return BadRequest("Invalid Credential");
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult Get(bool IsLogin = false)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                int mUser_ID = Convert.ToInt32(identity.FindFirst("PK_MUser_ID").Value);
                int mGroupMenu_ID = Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value);
                int mRole_ID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value);
                string userID = identity.FindFirst("UserID").Value;

                MUserView user = UserBLL.GetUserByID(mUser_ID, mGroupMenu_ID, mRole_ID);
                int expiredDay = Convert.ToInt32(SystemParameterBLL.GetAppSystemParameter(1000).SettingValue);
                DateTime now = DateTime.Now.Date;
                DateTime last = user.LastChangePassword.Date;
                double totalDay = (now - last).Days;
                List<NawaIcon> iconList = CommonBLL.GetNawaIcon();
                int validateNoChange = Convert.ToInt32(SystemParameterBLL.GetAppSystemParameter(34).SettingValue);
                if ((SystemParameterBLL.GetSystemParameterValue(3).SettingValue == "2" || SystemParameterBLL.GetSystemParameterValue(3).SettingValue == "1" && !user.IsUseLDAP)
                && mRole_ID != 1 && Convert.ToString(user.LastChangePassword) == "01/01/1753 01:01:01")
                {
                    return Ok(new { user, status = "newUser", iconList = iconList });
                }
                else if ((SystemParameterBLL.GetSystemParameterValue(3).SettingValue == "2" || SystemParameterBLL.GetSystemParameterValue(3).SettingValue == "1" 
                && !user.IsUseLDAP) && mRole_ID != 1 && Convert.ToInt32(totalDay) >= expiredDay)
                {
                    return Ok(new { user, status = "expiredUser", iconList = iconList });
                }
                else
                {
                    OTPUser otpUser = null;
                    if (IsLogin)
                        otpUser = UserBLL.GetOTP(mRole_ID, userID, mUser_ID);

                    List<TreeMenu> treeMenus = UserBLL.GetTreeMenu(mGroupMenu_ID);
                    List<Alternate> listAlternate = UserBLL.GetAlternateUsers(user.UserID);

                    if (otpUser != null)
                        return Ok(new { otpUser, user, status = "validOTP", iconList = iconList, menus = treeMenus, listAlternate = listAlternate, validateNoChange = validateNoChange });
                    else
                        return Ok(new { user, status = "valid", iconList = iconList, menus = treeMenus, listAlternate = listAlternate, validateNoChange = validateNoChange });
                }
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("Mobile")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetMobile()
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                int mUser_ID = Convert.ToInt32(identity.FindFirst("PK_MUser_ID").Value);
                int mGroupMenu_ID = Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value);
                int mRole_ID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value);

                NawaDataDAL.Models.MUserView user = UserBLL.GetUserByID(mUser_ID, mGroupMenu_ID, mRole_ID);

                List<NawaDataDAL.Models.TreeMenu> treeMenus = UserBLL.GetTreeMenuMobile(mGroupMenu_ID);


                return Ok(new { user = user, menus = treeMenus });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("Logout")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult Logout()
        {
            try
            {

                #region Get Client's IP Address
                System.Net.IPAddress remoteIpAddress = Request.HttpContext.Connection.RemoteIpAddress;
                string ipString = "";

                if (remoteIpAddress != null)
                {
                    // If we got an IPV6 address, then we need to ask the network for the IPV4 address 
                    // This usually only happens when the browser is on the same machine as the server.
                    if (remoteIpAddress.AddressFamily == System.Net.Sockets.AddressFamily.InterNetworkV6)
                    {
                        remoteIpAddress = System.Net.Dns.GetHostEntry(remoteIpAddress).AddressList
                            .First(x => x.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork);
                    }

                    ipString = remoteIpAddress.ToString();
                }
                #endregion


                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                //get api token on header
                string token = Request.Headers["Authorization"].ToString().Split(' ')[1];
                DateTime validTo = new JwtSecurityTokenHandler().ReadJwtToken(token).ValidTo;
                string userID = identity.FindFirst("UserID").Value;
                int pkMUserID = Convert.ToInt32(identity.FindFirst("PK_MUser_ID").Value);

                

                #region Log Out Alternate User
                if (identity.FindFirst("AlternateUser") != null)
                {
                    if (!string.IsNullOrEmpty(identity.FindFirst("AlternateUser").Value))
                    {
                        userID = identity.FindFirst("AlternateUser").Value;
                        MUserView muserview = UserBLL.GetUserByUserID(userID);
                        if (muserview != null)
                            pkMUserID = muserview.PK_MUser_ID;
                    }
                }
                #endregion

                if (userID != null)
                {
                    UserBLL.ChangeUserUsedStatus(pkMUserID, false);
                    AuditTrailBLL.PostAuditTrailUserLogin(userID, "Logout", ipString, "Logout Success");
                    NawaDataBLL.UserBLL.InsertUsedToken(Convert.ToInt32(identity.FindFirst("PK_MUser_ID").Value), userID, token, validTo);
                }


                var result = new { Success = "True" };
                return Ok(result);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetBanner")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetBanner()
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                List<NawaDataDAL.Models.Banner> listBanner = UserBLL.GetBanner(userID);

                return Ok(listBanner);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetSessionList")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetSessionList()
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                int roleID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value);
                int groupID = Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value);
                List<NawaDataDAL.Models.MultiGroupRole> sessionList = UserBLL.GetSessionList(userID, groupID, roleID);

                return Ok(sessionList);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ChangeSession")]
        [Authorize(Policy="ApplicationLock")]
        [HttpPost]
        public ActionResult ChangeSession(UserInfoWithGroup _userData)
        {
            try
            {
                #region Get Client's IP Address
                System.Net.IPAddress remoteIpAddress = Request.HttpContext.Connection.RemoteIpAddress;
                string ipString = "";

                if (remoteIpAddress != null)
                {
                    // If we got an IPV6 address, then we need to ask the network for the IPV4 address 
                    // This usually only happens when the browser is on the same machine as the server.
                    if (remoteIpAddress.AddressFamily == System.Net.Sockets.AddressFamily.InterNetworkV6)
                    {
                        remoteIpAddress = System.Net.Dns.GetHostEntry(remoteIpAddress).AddressList
                            .First(x => x.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork);
                    }

                    ipString = remoteIpAddress.ToString();
                }
                #endregion


                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;

                if (_userData != null && userID != null && _userData.FK_MGroupMenu_ID != 0 && _userData.FK_MRole_ID != 0)
                {
                    MUser user = UserBLL.ChangeSession(userID, _userData.FK_MGroupMenu_ID, _userData.FK_MRole_ID);
                    if (user == null)
                    {
                        AuditTrailBLL.PostAuditTrailUserLogin(userID, "Change Session", ipString, "Change Session Failed");
                        return BadRequest("Change Session Not Allowed");
                    }
                    else
                    {
                        AuditTrailBLL.PostAuditTrailUserLogin(userID, "Change Session", ipString, $"Change Session Success to Group Menu ID ${_userData.FK_MGroupMenu_ID} and Role ID ${_userData.FK_MRole_ID}");
                        //create claims details based on the user information
                        Claim[] claims = new Claim[]
                        {
                        new Claim(JwtRegisteredClaimNames.Sub, _configuration["Jwt:Subject"]),
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                        new Claim(JwtRegisteredClaimNames.Iat, DateTime.UtcNow.ToString()),
                        new Claim("PK_MUser_ID", user.PK_MUser_ID.ToString()),
                        new Claim("UserID", userID),
                        new Claim("UserName", user.UserName),
                        new Claim("FK_MRole_ID", user.FK_MRole_ID.ToString()),
                        new Claim("FK_MGroupMenu_ID", user.FK_MGroupMenu_ID.ToString()),
                        new Claim("UserEmailAddress", user.UserEmailAddress),
                        new Claim("AlternateUser", userID)
                        };

                        SymmetricSecurityKey key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]));

                        SigningCredentials signIn = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                        JwtSecurityToken token = new JwtSecurityToken(_configuration["Jwt:Issuer"], _configuration["Jwt:Issuer"], claims, expires: DateTime.UtcNow.AddDays(1), signingCredentials: signIn);

                        return Ok(new JwtSecurityTokenHandler().WriteToken(token));
                    }
                }
                else
                {
                    return BadRequest("Invalid Credential");
                }
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GoogleAuthenticate")]
        [HttpPost]
        public async Task<ActionResult> GoogleAuthenticate(GoogleSignIn googleToken)
        {
            try
            {
                #region Get Client's IP Address
                System.Net.IPAddress remoteIpAddress = Request.HttpContext.Connection.RemoteIpAddress;
                string ipString = "";

                if (remoteIpAddress != null)
                {
                    // If we got an IPV6 address, then we need to ask the network for the IPV4 address 
                    // This usually only happens when the browser is on the same machine as the server.
                    if (remoteIpAddress.AddressFamily == System.Net.Sockets.AddressFamily.InterNetworkV6)
                    {
                        remoteIpAddress = System.Net.Dns.GetHostEntry(remoteIpAddress).AddressList
                            .First(x => x.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork);
                    }

                    ipString = remoteIpAddress.ToString();
                }
                #endregion

                if (!string.IsNullOrEmpty(googleToken.tokenID))
                {
                    NawaDataDAL.Models.MUser user = await UserBLL.GoogleAuthenticate(googleToken.tokenID);

                    AuditTrailBLL.PostAuditTrailUserLogin(user.UserID, "Login Google",ipString, "Login Success");
                    //create claims details based on the user information
                    Claim[] claims = new Claim[]
                    {
                        new Claim(JwtRegisteredClaimNames.Sub, _configuration["Jwt:Subject"]),
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                        new Claim(JwtRegisteredClaimNames.Iat, DateTime.UtcNow.ToString()),
                        new Claim("PK_MUser_ID", user.PK_MUser_ID.ToString()),
                        new Claim("UserID", user.UserID),
                        new Claim("UserName", user.UserName),
                        new Claim("FK_MRole_ID", user.FK_MRole_ID.ToString()),
                        new Claim("FK_MGroupMenu_ID", user.FK_MGroupMenu_ID.ToString()),
                        new Claim("UserEmailAddress", user.UserEmailAddress)
                    };

                    SymmetricSecurityKey key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]));

                    SigningCredentials signIn = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                    JwtSecurityToken token = new JwtSecurityToken(_configuration["Jwt:Issuer"], _configuration["Jwt:Issuer"], claims, expires: DateTime.UtcNow.AddDays(1), signingCredentials: signIn);

                    return Ok(new JwtSecurityTokenHandler().WriteToken(token));
                }
                else
                {
                    return BadRequest("Invalid Credential");
                }
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("FacebookAuthenticate")]
        [HttpPost]
        public ActionResult FacebookAuthenticate(NawaDataDAL.Models.FacebookModel facebookToken)
        {
            try
            {
                #region Get Client's IP Address
                System.Net.IPAddress remoteIpAddress = Request.HttpContext.Connection.RemoteIpAddress;
                string ipString = "";

                if (remoteIpAddress != null)
                {
                    // If we got an IPV6 address, then we need to ask the network for the IPV4 address 
                    // This usually only happens when the browser is on the same machine as the server.
                    if (remoteIpAddress.AddressFamily == System.Net.Sockets.AddressFamily.InterNetworkV6)
                    {
                        remoteIpAddress = System.Net.Dns.GetHostEntry(remoteIpAddress).AddressList
                            .First(x => x.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork);
                    }

                    ipString = remoteIpAddress.ToString();
                }
                #endregion

                if (!string.IsNullOrEmpty(facebookToken.accessToken))
                {
                    NawaDataDAL.Models.MUser user = UserBLL.FacebookAuthenticate(facebookToken.accessToken);

                    AuditTrailBLL.PostAuditTrailUserLogin(user.UserID, "Login Facebook",ipString, "Login Success");
                    //create claims details based on the user information
                    Claim[] claims = new Claim[]
                    {
                        new Claim(JwtRegisteredClaimNames.Sub, _configuration["Jwt:Subject"]),
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                        new Claim(JwtRegisteredClaimNames.Iat, DateTime.UtcNow.ToString()),
                        new Claim("PK_MUser_ID", user.PK_MUser_ID.ToString()),
                        new Claim("UserID", user.UserID),
                        new Claim("UserName", user.UserName),
                        new Claim("FK_MRole_ID", user.FK_MRole_ID.ToString()),
                        new Claim("FK_MGroupMenu_ID", user.FK_MGroupMenu_ID.ToString()),
                        new Claim("UserEmailAddress", user.UserEmailAddress)
                    };

                    SymmetricSecurityKey key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]));

                    SigningCredentials signIn = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                    JwtSecurityToken token = new JwtSecurityToken(_configuration["Jwt:Issuer"], _configuration["Jwt:Issuer"], claims, expires: DateTime.UtcNow.AddDays(1), signingCredentials: signIn);

                    return Ok(new JwtSecurityTokenHandler().WriteToken(token));
                }
                else
                {
                    return BadRequest("Invalid Credential");
                }
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetAutoLoginRedirect")]
        [HttpGet]
        [Authorize(Policy="ApplicationLock")]
        public ActionResult GetAutoLoginRedirect()
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;

                string linkURL = SystemParameterBLL.GetAppSystemParameter(-23).SettingValue;
                string encryptionKey = SystemParameterBLL.GetAppSystemParameter(-22).SettingValue;

                linkURL = linkURL.Replace("{UserID}", NawaEncryption.Common.EncryptRijndael(userID, encryptionKey));

                return Ok(linkURL);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("RedirectAuth")]
        [HttpGet]
        public ActionResult RedirectAuth(string userID)
        {
            try
            {
                UserLoginValidation userValidation = UserBLL.RedirectAuth(userID);

                if (userValidation != null && userValidation.IsValid && userValidation.User != null)
                {
                    //Create claims details based on the user information
                    Claim[] claims = new Claim[]
                    {
                        new Claim(JwtRegisteredClaimNames.Sub, _configuration["Jwt:Subject"]),
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                        new Claim(JwtRegisteredClaimNames.Iat, DateTime.Now.ToString()),
                        new Claim("PK_MUser_ID", userValidation.User.PK_MUser_ID.ToString()),
                        new Claim("UserID", userValidation.User.UserID),
                        new Claim("UserName", userValidation.User.UserName),
                        new Claim("FK_MRole_ID", userValidation.User.FK_MRole_ID.ToString()),
                        new Claim("FK_MGroupMenu_ID", userValidation.User.FK_MGroupMenu_ID.ToString()),
                        new Claim("UserEmailAddress", userValidation.User.UserEmailAddress)
                    };

                    //Get session expiration time
                    SystemParameter expSysParam = SystemParameterBLL.GetSystemParameterValue(17);
                    int tokenExpiration = expSysParam == null ? 60 :
                        string.IsNullOrEmpty(expSysParam.SettingValue) ? 60 : Convert.ToInt32(expSysParam.SettingValue);

                    SymmetricSecurityKey key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]));
                    SigningCredentials signIn = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
                    JwtSecurityToken token = new JwtSecurityToken(_configuration["Jwt:Issuer"], _configuration["Jwt:Issuer"], claims, expires: DateTime.Now.AddMinutes(tokenExpiration), signingCredentials: signIn);

                    return Ok(new JwtSecurityTokenHandler().WriteToken(token));
                }
                else
                {
                    string errString = userValidation == null ? "Redirect feature is disabled at the moment" :
                        string.IsNullOrEmpty(userValidation.Message) ? "Redirect feature is disabled at the moment" : userValidation.Message;

                    return StatusCode(500, errString);
                }
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string stringUserID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, stringUserID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ChangeAlternate")]
        [Authorize(Policy="ApplicationLock")]
        [HttpPost]
        public async Task<ActionResult> ChangeAlternate(UserInfoWithGroup _userData)
        //public async Task<ActionResult> Post(UserInfo _userData)

        {
            try
            {
                #region Get Client's IP Address
                System.Net.IPAddress remoteIpAddress = Request.HttpContext.Connection.RemoteIpAddress;
                string ipString = "";

                if (remoteIpAddress != null)
                {
                    // If we got an IPV6 address, then we need to ask the network for the IPV4 address 
                    // This usually only happens when the browser is on the same machine as the server.
                    if (remoteIpAddress.AddressFamily == System.Net.Sockets.AddressFamily.InterNetworkV6)
                    {
                        remoteIpAddress = System.Net.Dns.GetHostEntry(remoteIpAddress).AddressList
                            .First(x => x.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork);
                    }

                    ipString = remoteIpAddress.ToString();
                }
                #endregion


                if (_userData != null && _userData.UserID != null)
                {

                    ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                    string userIDLogin = identity.FindFirst("UserID").Value;
                    MUserView user = UserBLL.ChangeAlternate(_userData.UserID, userIDLogin);

                    


                    if (user == null)
                    {
                        AuditTrailBLL.PostAuditTrailUserLogin(userIDLogin, "Change Alternate", ipString, "Change Alternate Failed");
                        return BadRequest("Invalid credentials");
                    }
                    else
                    {
                        NawaDataDAL.Models.UserLoginValidation userValidation = await UserBLL.ValidateAlternateUser(_userData.UserID, ipString);
                        if (userValidation != null && userValidation.IsValid && userValidation.User != null)
                        {

                            AuditTrailBLL.PostAuditTrailUserLogin(userIDLogin, "Change Alternate", ipString, $"Change Alternate Success to User ID ${_userData.UserID}");
                            Claim[] claims = new Claim[]
                            {
                            new Claim(JwtRegisteredClaimNames.Sub, _configuration["Jwt:Subject"]),
                            new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                            new Claim(JwtRegisteredClaimNames.Iat, DateTime.UtcNow.ToString()),
                            new Claim("PK_MUser_ID", user.PK_MUser_ID.ToString()),
                            new Claim("UserID", user.UserID),
                            new Claim("UserName", user.UserName),
                            new Claim("FK_MRole_ID", user.FK_MRole_ID.ToString()),
                            new Claim("FK_MGroupMenu_ID", user.FK_MGroupMenu_ID.ToString()),
                            new Claim("UserEmailAddress", user.UserEmailAddress),
                            new Claim("AlternateUser", userIDLogin)
                            };
                            SymmetricSecurityKey key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]));
                            SigningCredentials signIn = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
                            JwtSecurityToken token = new JwtSecurityToken(_configuration["Jwt:Issuer"], _configuration["Jwt:Issuer"], claims, expires: DateTime.UtcNow.AddDays(1), signingCredentials: signIn);
                            return Ok(new JwtSecurityTokenHandler().WriteToken(token));
                        } else
                        {
                            string errFrontend = "";
                            string errBackend = "";
                            if (userValidation != null)
                            {
                                if ((userValidation.Message == null) && (userValidation.User != null))
                                {
                                    SystemParameter parameter = SystemParameterBLL.GetSystemParameterValue(9004);
                                    errBackend = parameter.SettingName;
                                    errFrontend = parameter.SettingValue;
                                }
                                else if ((userValidation.Message != null) && (userValidation.User == null))
                                {
                                    SystemParameter parameter = SystemParameterBLL.GetSystemParameterValue(9005);
                                    errBackend = parameter.SettingName;
                                    errFrontend = parameter.SettingValue;
                                }
                                else
                                {
                                    errBackend = userValidation.Message;
                                    errFrontend = userValidation.Message;
                                }
                            }
                            AuditTrailBLL.PostAuditTrailUserLogin(_userData.UserID, "Try Login", ipString, errBackend);
                            return BadRequest(errFrontend);
                        }

                    }
                }
                else
                {
                    return BadRequest("Invalid Credential");
                }
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string stringUserID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, stringUserID);
                return StatusCode(500, ex.Message);
            }
        }


        [Route("ResendOTPUser")]
        [HttpGet]
        [Authorize(Policy = "ApplicationLock")]
        public ActionResult ResendOTPUser()
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                int mRole_ID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value);
                int mUserID = Convert.ToInt32(identity.FindFirst("PK_MUser_ID").Value);

                OTPUser otpUser = UserBLL.ResendOTP(mRole_ID, userID, mUserID);
                return Ok(new { otpUser });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("CheckOTP")]
        [HttpGet]
        [Authorize(Policy = "ApplicationLock")]
        public ActionResult SubmitOTP(string OTPCode)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;

                string isOTPValid = UserBLL.SubmitOTP(userID, OTPCode);
                return Ok(isOTPValid);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        public class UserInfo
        {
            public string UserName { get; set; }
            public string Password { get; set; }
        }

        public class UserInfoWithGroup
        {
            public string UserID { get; set; }
            public int FK_MRole_ID { get; set; }
            public int FK_MGroupMenu_ID { get; set; }
        }

        public class GoogleSignIn 
        {
            public string tokenID { get; set; }
        }
    }
}