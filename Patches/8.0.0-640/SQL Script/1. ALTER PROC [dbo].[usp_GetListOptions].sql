GO
/****** Object:  StoredProcedure [dbo].[usp_GetListOptions]    Script Date: 2/5/2024 11:30:58 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROC [dbo].[usp_GetListOptions]
	@PK_ModuleField_ID BIGINT,
	@ParentValue VARCHAR(MAX),
	@UserID VARCHAR(50),
	@SearchKeyword VARCHAR(MAX) = ''
	--05/12/2023 penambahan with no lock di beberapa module terkadang lock
	--12/12/2023 penambahan bracket saat pakai table alias
AS
DECLARE @TableReferenceName VARCHAR(250),
		@TableReferenceNameAlias VARCHAR(250),
		@TableReferenceFieldKey VARCHAR(250),
		@TableReferenceFieldDisplayName VARCHAR(250),
		@TableReferenceFilter VARCHAR(1000),
		@TableReferenceAdditonalJoin VARCHAR(1000),
		@FilterCascade VARCHAR(1000),
		@SearchWord	VARCHAR(MAX) = '''%' + REPLACE(@SearchKeyword, '''', '''''') + '%''',
		@SQLQuery NVARCHAR(MAX),
		@Params NVARCHAR(MAX) = '@Parent VARCHAR(MAX)'

SELECT @TableReferenceName = mf1.TabelReferenceName,
		@TableReferenceNameAlias = mf1.TabelReferenceNameAlias,
		@TableReferenceFieldKey = mf1.TableReferenceFieldKey,
		@TableReferenceFieldDisplayName = mf1.TableReferenceFieldDisplayName,
		@TableReferenceFilter = mf1.TableReferenceFilter,
		@TableReferenceAdditonalJoin = mf1.TableReferenceAdditonalJoin,
		@FilterCascade = CASE
			WHEN mf1.BCasCade = 1
				--ignore FilterCascade when the parent field is FileUpload or RichText
				AND (mf2.FK_ExtType_ID <> 8 AND mf2.FK_ExtType_ID <> 16)
			THEN mf1.FilterCascade
			ELSE ''
		END
FROM dbo.ModuleField mf1
LEFT JOIN dbo.ModuleField mf2 ON mf1.FieldNameParent = mf2.FieldName AND mf1.FK_Module_ID = mf2.FK_Module_ID
WHERE mf1.PK_ModuleField_ID = @PK_ModuleField_ID;

if @TableReferenceNameAlias <> ''
	begin
		set @TableReferenceFilter=replace(@TableReferenceFilter,@TableReferenceNameAlias+'.','['+@TableReferenceNameAlias+'].')
		set @FilterCascade=replace(@FilterCascade,@TableReferenceNameAlias+'.','['+@TableReferenceNameAlias+'].')
		set @TableReferenceNameAlias='['+@TableReferenceNameAlias+']'
	end


SET @TableReferenceNameAlias = CASE WHEN ISNULL(@TableReferenceNameAlias, '') = '' THEN @TableReferenceName ELSE @TableReferenceNameAlias END

SET @SQLQuery = 'SELECT ' + @TableReferenceNameAlias + '.' + @TableReferenceFieldKey + ' AS [value],' + CHAR(10) + CHAR(13) +
				'		' + @TableReferenceNameAlias + '.' + @TableReferenceFieldDisplayName + ' AS [text]' + CHAR(10) + CHAR(13) +
				'FROM ' + @TableReferenceName + ' AS ' + @TableReferenceNameAlias + CHAR(10) + ' WITH (NOLOCK) ' + CHAR(13) +

				-- search
				'WHERE (' + @TableReferenceNameAlias + '.' + @TableReferenceFieldKey + ' LIKE ' + @SearchWord + 
				' OR '+ @TableReferenceNameAlias + '.' + @TableReferenceFieldDisplayName + ' LIKE ' + @SearchWord + ') ' +

				-- filter
				CASE WHEN ISNULL(@TableReferenceFilter, '') <> ''
					 THEN 'AND ' + REPLACE(@TableReferenceFilter, '@UserID', @UserID) +
							CASE WHEN ISNULL(@FilterCascade, '') <> ''
								 THEN ' AND ' + @FilterCascade
								 ELSE ''
							END
					 ELSE CASE WHEN ISNULL(@FilterCascade, '') <> ''
							   THEN ' AND ' + @FilterCascade
							   ELSE ''
						  END
				END
EXEC sp_executesql @SQLQuery, @Params, @ParentValue
