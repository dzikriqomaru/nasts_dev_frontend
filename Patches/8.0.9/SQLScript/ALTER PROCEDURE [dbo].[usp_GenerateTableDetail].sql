ALTER PROCEDURE [dbo].[usp_GenerateTableDetail]
/***********************************************************
* Procedure description:
* Date:   02/12/2020 
* Author: Fauzan
*
* Changes
* 
* Date		Modified By			Comments
************************************************************
* 21 Sept 2023 Davin			Nullable update in Information Schema for save draft feature
* 2 Nov 2023 Davin				Update sizeFieldCurrent Samakan dengan usp_generatetable
* 13 Dec 2023  apabila alter/update dan belum ada draft maka akan ditambahkan
************************************************************/
(@PKModuleDetailID INT)
AS
BEGIN
	--DECLARE @PKModuleDetailID INT
	--SET @PKModuleDetailID =9846
	DECLARE @ModuleDetailName				VARCHAR(250),
	        @ModuleDetailLabel				VARCHAR(250),
	        @ModuleDetailDescription		VARCHAR(500),
	        @IsUseDesigner					BIT,
	        @IsUseApproval					BIT,
	        @IsUseStoreProcedureValidation	BIT
	
	SELECT @ModuleDetailName				= ModuleDetailName,
	       @ModuleDetailLabel				= ModuleDetailLabel,
	       @ModuleDetailDescription			= ModuleDetailDescription
	FROM   dbo.ModuleDetail
	WHERE  PK_ModuleDetail_ID = @PKModuleDetailID

	IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES t WHERE t.TABLE_NAME = @ModuleDetailName)
	BEGIN
	    -- ADD NEW TABLE
	    DECLARE @sql VARCHAR(8000)
	    SET @sql = '	CREATE TABLE dbo. ' + @ModuleDetailName + CHAR(10)
	        + '	( ' + CHAR(10)
	    
	    DECLARE @PK_ModuleField_ID				BIGINT,
	            @FK_Module_ID					INT,
	            @FieldName						VARCHAR(250),
	            @FieldLabel						VARCHAR(250),
	            @Sequence						INT,
	            @Required						BIT,
	            @IsPrimaryKey					BIT,
	            @IsUnik						BIT,
	            @FK_FieldType_ID				INT,
	            @SizeField						INT,
	            @FK_ExtType_ID					INT,
	            @TabelReferenceName				VARCHAR(250),
	            @TableReferenceFieldKey			VARCHAR(250),
	            @TableReferenceFieldDisplayName	VARCHAR(250),
	            @TableReferenceFilter			VARCHAR(550),
	            @IsUseRegexValidation			BIT
	    
	    DECLARE my_cursor CURSOR FAST_FORWARD READ_ONLY 
	    FOR
	        SELECT PK_ModuleDetailField_ID,
	               FK_ModuleDetail_ID,
	               FieldName,
	               FieldLabel,
	               Sequence,
	               [Required],
	               IsPrimaryKey,
	               IsUnik,
	               FK_FieldType_ID,
	               SizeField,
	               FK_ExtType_ID,
	               TabelReferenceName,
	               TableReferenceFieldKey,
	               TableReferenceFieldDisplayName,
	               TableReferenceFilter,
	               IsUseRegexValidation
	        FROM   dbo.ModuleDetailField mf WHERE mf.FK_ModuleDetail_ID = @PKModuleDetailID 	        
	        UNION all
			SELECT a.PK_ModuleField_ID, @PKModuleDetailID, REPLACE(a.FieldName,'[FieldName]',b.FieldName) AS FieldNAme, REPLACE(a.FieldLabel,'[FieldName]',b.FieldLabel) AS FieldLabel, a.Sequence, a.[Required],
					a.IsPrimaryKey, a.IsUnik, a.FK_FieldType_ID, a.SizeField,
					a.FK_ExtType_ID , NULL, NULL, NULL, NULL, NULL
			FROM ModuleFieldDefaultFileUpload a
			CROSS JOIN dbo.ModuleDetailField b 
			WHERE b.FK_ModuleDetail_ID = @PKModuleDetailID AND b.FK_ExtType_ID IN (7, 8) AND b.FK_FieldType_ID = 14
   
	        ORDER BY Sequence
	    
	    OPEN my_cursor
	    
	    FETCH FROM my_cursor INTO @PK_ModuleField_ID, @FK_Module_ID, @FieldName,
	    @FieldLabel, @Sequence, @Required, @IsPrimaryKey,
	    @IsUnik, @FK_FieldType_ID, @SizeField, @FK_ExtType_ID,
	    @TabelReferenceName, @TableReferenceFieldKey,
	    @TableReferenceFieldDisplayName,
	    @TableReferenceFilter, @IsUseRegexValidation
	    
	    WHILE @@FETCH_STATUS = 0
	    BEGIN
	        DECLARE @datatype VARCHAR(30)=''
	        
	        IF @FK_FieldType_ID<>11 
	        begin
				SELECT @datatype = mt.FieldTypeSQLName
				FROM   MFieldType mt
				WHERE  mt.PK_FieldType_ID = @FK_FieldType_ID
	        END 
			--tambahan FK_FieldType_ID=16 parent reference
	        IF @FK_FieldType_ID=11 OR @FK_FieldType_ID=16
	        BEGIN
	        	
	        	IF @FK_ExtType_ID=15 or @FK_ExtType_ID=21
	        	BEGIN
	        		--karena checkbox, maka tipedatanya varchar(max)
	        		set @datatype='varchar(max)'
	        	END
	        	ELSE
	        		BEGIN
	        				SELECT @datatype=c.DATA_TYPE+ case when c.CHARACTER_MAXIMUM_LENGTH IS NULL THEN '' WHEN c.CHARACTER_MAXIMUM_LENGTH=-1 THEN '(max)' ELSE '('+ CONVERT(VARCHAR(20), c.CHARACTER_MAXIMUM_LENGTH) +')' END   
	        	  FROM INFORMATION_SCHEMA.[COLUMNS] c WHERE c.TABLE_NAME=@TabelReferenceName AND c.COLUMN_NAME=@TableReferenceFieldKey
	        	    
	        		END
	        END 

	        DECLARE @strsizefield VARCHAR(30)=''
	        SET @strsizefield = '' 
	        IF @FK_FieldType_ID = 9
				IF @SizeField<>-1 
				BEGIN
					SET @strsizefield = ' (' + CONVERT(VARCHAR(50), @SizeField) +')'	
				END 	
				ELSE
					SET @strsizefield = ' (max)'
	        ELSE IF @FK_FieldType_ID =14
				SET @strsizefield = ' (max)'	
	            
	        
	        DECLARE @strreq AS VARCHAR(30)=''
	        IF @Required = 1  And @IsPrimaryKey = 1
	            SET @strreq = ' Not Null '
	        ELSE
	            SET @strreq = ' NULL ' 
	        
	        
	        DECLARE @identity VARCHAR(30)
	        SET @identity = ''
	        IF @FK_FieldType_ID = 12 OR @FK_FieldType_ID = 15
	            SET @identity = ' IDENTITY (1, 1) '	
	        
	        SET @sql = @sql + ' [' + @FieldName + ']  ' + @datatype + @strsizefield + ' ' + @strreq + @identity + ',' +CHAR(10)+CHAR(13)
	        
	        
	        FETCH FROM my_cursor INTO @PK_ModuleField_ID, @FK_Module_ID, @FieldName,
	        @FieldLabel, @Sequence, @Required, @IsPrimaryKey,
	        @IsUnik, @FK_FieldType_ID, @SizeField,
	        @FK_ExtType_ID, @TabelReferenceName,
	        @TableReferenceFieldKey,
	        @TableReferenceFieldDisplayName,
	        @TableReferenceFilter, @IsUseRegexValidation
	    END
	    
	    CLOSE my_cursor
	    DEALLOCATE my_cursor
	    
	    
	    
	    
	    
	    
	    
	    DECLARE @PK_ModuleField_IDdefault bigint, @FieldNameDefault varchar(250),
        @FieldLabelDefault varchar(250), @SequenceDefault int, @Requireddefault bit,
        @IsPrimaryKeyDefault bit, @IsUnikDefault bit, @FK_FieldType_IDDefault int, @SizeFieldDefault int,
        @FK_ExtType_IDDefault int
	    
	    DECLARE my_cursor CURSOR FAST_FORWARD READ_ONLY FOR
	    SELECT PK_ModuleField_ID, FieldName, FieldLabel, Sequence, [Required],
	           IsPrimaryKey, IsUnik, FK_FieldType_ID, SizeField, FK_ExtType_ID
	    FROM dbo.ModuleFieldDefault 
	    
	    OPEN my_cursor
	    
	    FETCH FROM my_cursor INTO @PK_ModuleField_IDdefault, @FieldNamedefault, @FieldLabeldefault,
	                              @Sequencedefault, @Requireddefault, @IsPrimaryKeydefault, @IsUnikdefault,
	                              @FK_FieldType_IDdefault, @SizeFielddefault, @FK_ExtType_IDdefault
	    
	    WHILE @@FETCH_STATUS = 0
	    BEGIN
	    
	     DECLARE @datatypedefault VARCHAR(30)=''
	        SELECT @datatypedefault= mt.FieldTypeSQLName
	        FROM   MFieldType mt
	        WHERE  mt.PK_FieldType_ID = @FK_FieldType_IDDefault
	        
	        DECLARE @strsizefieldDefault VARCHAR(30)=''
	        SET @strsizefielddefault = '' 
	        IF @FK_FieldType_IDDefault = 9
	            SET @strsizefieldDefault = ' (' + CONVERT(VARCHAR(50), @SizeFieldDefault) +
	                ')'
	        
	        DECLARE @strreqDefault AS VARCHAR(30)=''
	        IF @Requireddefault = 1
	            SET @strreqDefault = ' Not Null '
	        ELSE
	            SET @strreqDefault = ' NULL ' 
	        
	        
	        DECLARE @identityDefault VARCHAR(30)
	        SET @identityDefault = ''
	        IF @FK_FieldType_IDDefault = 12 OR @FK_FieldType_IDDefault = 15
	            SET @identityDefault = ' IDENTITY (1, 1) '	
	        
	        
	    SET @sql = @sql + ' [' + @FieldNamedefault + ']  ' + @datatypeDefault + @strsizefielddefault	 + ' ' + @strreqDefault + @identitydefault + ','+CHAR(10)+CHAR(13)
	    
	    	FETCH FROM my_cursor INTO @PK_ModuleField_IDdefault, @FieldNamedefault,
	    	                          @FieldLabeldefault, @Sequencedefault, @Requireddefault,
	    	                          @IsPrimaryKeydefault, @IsUnikdefault, @FK_FieldType_IDdefault,
	    	                          @SizeFielddefault, @FK_ExtType_IDdefault
	    END
	    
	    CLOSE my_cursor
	    DEALLOCATE my_cursor
	    
	    
	    
	    IF LEN(@sql) > 0
	        SET @sql = SUBSTRING(@sql, 0, LEN(@sql) -3)    
	    
	    SET @sql = @sql + ')  ON [PRIMARY]'    
	    
	    
		--PRINT @sql
	    EXEC (@sql)
	    
	    DECLARE @keyprimary VARCHAR(50)
	    SELECT @keyprimary = mf.FieldName
	    FROM   ModuleDetailField mf
	    WHERE  mf.FK_ModuleDetail_ID = @PKModuleDetailID
	           AND mf.IsPrimaryKey = 1
	    
	    SET @sql = 'ALTER TABLE dbo.' + @ModuleDetailName + ' ADD CONSTRAINT ' + CHAR(10)
	        + '	PK_' + @ModuleDetailName + ' PRIMARY KEY CLUSTERED  ' + CHAR(10)
	        + '	( ' + CHAR(10)
	        + '	' + @keyprimary + ' ' + CHAR(10)
	        +
	        ') WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY] ' 
	        + CHAR(10)
	    
		--PRINT (@sql)
	    EXEC (@sql)
	END
	ELSE
	BEGIN
	-------------
	-- ADD COLUMN
	-------------
	--cek dulu ada Field Draft atau tidak
		DECLARE @TempResDetail TABLE(Res INT)
		DECLARE @sqlQuery varchar(1000) = 'SELECT COUNT (COLUMN_NAME) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = ''' + @ModuleDetailName +''' and COLUMN_NAME =''Draft''' 
		INSERT INTO @TempResDetail EXEC (@sqlQuery)
		--cek dulu ada Field Draft atau tidak

	DECLARE  @FieldNameCek varchar(250),
         @FieldTypeSQLNameCek varchar(50),  @SizeFieldCek INT,@datatypecek VARCHAR(50)
        
        
	DECLARE my_cursor CURSOR FAST_FORWARD READ_ONLY FOR
	-- cari yg ada di setting dan tidak ada di table

	SELECT mf.FieldName ,
	CASE WHEN mf.FK_ExtType_ID<>15 AND mf.FK_ExtType_ID<>21 THEN  mt.FieldTypeSQLName ELSE 'varchar' end AS FieldTypeSQLName ,
	CASE WHEN mf.FK_ExtType_ID<>15 AND mf.FK_ExtType_ID<>21 THEN mf.SizeField ELSE -1 END AS SizeField  , 
	CASE WHEN mf.FK_ExtType_ID<>15 AND mf.FK_ExtType_ID<>21 THEN  mt.FieldTypeSQLName ELSE 'varchar' end  FieldTypeSQLName
		  FROM 
		  (
			SELECT PK_ModuleDetailField_ID, FK_ModuleDetail_ID, FieldName, FK_FieldType_ID, SizeField,FK_ExtType_ID, [Required] 
			FROM  ModuleDetailField
			WHERE FK_ModuleDetail_ID=@PKModuleDetailID 
			UNION ALL
			SELECT a.PK_ModuleField_ID, @PKModuleDetailID, 
			REPLACE(a.FieldName,'[FieldName]',b.FieldName) , a.FK_FieldType_ID, 
			a.SizeField,a.FK_ExtType_ID, b.[Required] 
			FROM  ModuleFieldDefaultFileUpload a
			CROSS JOIN dbo.ModuleDetailField b 
			WHERE b.FK_ModuleDetail_ID=@PKModuleDetailID AND b.FK_ExtType_ID IN (7, 8) AND b.FK_FieldType_ID=14  
			UNION ALL
			--penambahan draft apabila belum ada
			SELECT a.PK_ModuleField_ID, @PKModuleDetailID, a.FieldName , a.FK_FieldType_ID,a.SizeField,a.FK_ExtType_ID,a.[Required]
			FROM  ModuleFieldDefault a
			CROSS JOIN @TempResDetail b
			where a.PK_ModuleField_ID=9 and b.Res =0
		  )mf
	INNER JOIN MFieldType mt ON mt.PK_FieldType_ID = mf.FK_FieldType_ID 
	INNER JOIN ModuleDetail x ON x.PK_ModuleDetail_ID = mf.FK_ModuleDetail_ID
	LEFT JOIN INFORMATION_SCHEMA.[COLUMNS] c ON mf.FieldName=c.COLUMN_NAME AND c.TABLE_NAME = x.ModuleDetailName
	WHERE mf.FK_ModuleDetail_ID = @PKModuleDetailID
	AND c.COLUMN_NAME IS NULL

	-- add field yg kurang
	OPEN my_cursor
	FETCH FROM my_cursor INTO @FieldNameCek,@FieldTypeSQLNameCek,@SizeFieldCek,@datatypecek
	WHILE @@FETCH_STATUS = 0
	BEGIN
	
	IF @SizeFieldCek=-1 
	BEGIN
		IF @datatypecek='varchar' OR @datatypecek='varbinary' SET @datatypecek=@datatypecek + '(max)'
	END
	ELSE
		BEGIN
			IF @datatypecek='varchar' SET @datatypecek=@datatypecek + '('+ CONVERT(VARCHAR(20), @SizeFieldCek)	 +')'
			ELSE IF @datatypecek='varbinary' SET @datatypecek=@datatypecek + '(max)'
		END
		 
	
	set @sql=' Alter table '+ @ModuleDetailName +' add  ['+ @FieldNameCek +'] '+@datatypecek
	
	--PRINT @sql
	EXEC(@sql)
	FETCH FROM my_cursor INTO  @FieldNamecek,@FieldTypeSQLNamecek,@SizeFieldcek,@datatypecek
	END
	CLOSE my_cursor
	DEALLOCATE my_cursor
	-----------------
	-- DELETE COLUMN
	-----------------
	DECLARE  @FieldNamebuang varchar(250),
			 @FieldTypeSQLNamebuang varchar(50),  @SizeFieldbuang int
	DECLARE my_cursor CURSOR FAST_FORWARD READ_ONLY FOR
	--cari yg ada di table dan tidak ada di setting


	--declare @PKModuleDetailID INT
	--SET @PKModuleDetailID =9846
	--declare @ModuleName              VARCHAR(250)='cobaidentitybigint'

	SELECT c.COLUMN_NAME , c.DATA_TYPE,isnull(c.CHARACTER_MAXIMUM_LENGTH,0) AS maxpanjang
	  FROM 
	  (
	  SELECT PK_ModuleDetailField_ID, FK_ModuleDetail_ID, FieldName, FK_FieldType_ID, SizeField
		FROM  ModuleDetailField
	  WHERE FK_ModuleDetail_ID = @PKModuleDetailID 
		 UNION ALL
		  SELECT a.PK_ModuleField_ID, @PKModuleDetailID, REPLACE(a.FieldName,'[FieldName]',b.FieldName) , a.FK_FieldType_ID, a.SizeField
		FROM  ModuleFieldDefaultFileUpload a
		 CROSS JOIN dbo.ModuleDetailField  b 
	   WHERE b.FK_ModuleDetail_ID = @PKModuleDetailID AND b.FK_ExtType_ID IN (7, 8) AND b.FK_FieldType_ID=14   
  
	  ) mf
	INNER JOIN MFieldType mt ON mt.PK_FieldType_ID = mf.FK_FieldType_ID 
	INNER JOIN ModuleDetail x ON x.PK_ModuleDetail_ID = mf.FK_ModuleDetail_ID
	right JOIN INFORMATION_SCHEMA.[COLUMNS] c ON mf.FieldName=c.COLUMN_NAME AND c.TABLE_NAME = x.ModuleDetailName
	WHERE c.TABLE_NAME = @ModuleDetailName
	AND mf.PK_ModuleDetailField_ID IS NULL
	AND c.COLUMN_NAME NOT IN (
	SELECT mfd.FieldName FROM ModuleFieldDefault mfd	
	)

	--buang field yg lebih
	OPEN my_cursor
	FETCH FROM my_cursor INTO @FieldNamebuang,@FieldTypeSQLNamebuang,@SizeFieldbuang
	WHILE @@FETCH_STATUS = 0
	BEGIN
		/*{ ... Cursor logic here ... }*/
		set @sql=' Alter table '+ @ModuleDetailName +' drop column ['+ @FieldNamebuang + ']'
	 
		--PRINT @sql 
		EXEC(@sql)
		FETCH FROM my_cursor INTO  @FieldNamebuang,@FieldTypeSQLNamebuang,@SizeFieldbuang
	END
	CLOSE my_cursor
	DEALLOCATE my_cursor

	-----------------
	-- UPDATE COLUMN
	-----------------
	DECLARE  @FieldNameubah varchar(250),
			 @FieldTypeSQLNameubah varchar(50),  @SizeFieldubah INT,@tipedataubah VARCHAR(50),@Requiredubah BIT
	DECLARE my_cursor CURSOR FAST_FORWARD READ_ONLY FOR

	SELECT xx.FieldName, xx.FieldTypeSQLName, xx.SizeFieldcurrent, xx.DATA_TYPE , xx.[Required] 
	FROM (
	SELECT mf.FieldName,
		   CASE 
				WHEN mf.FK_FieldType_ID <> 11  AND mf.FK_FieldType_ID <> 16 AND mf.FK_ExtType_ID <> 21 THEN mt.FieldTypeSQLName
				WHEN (mf.FK_FieldType_ID = 11 AND mf.FK_ExtType_ID<>15 AND mf.FK_ExtType_ID<>21) OR mf.FK_FieldType_ID=16 THEN (
						 SELECT c.DATA_TYPE 
						 FROM   INFORMATION_SCHEMA.[COLUMNS] c
						 WHERE  c.TABLE_NAME = mf.TabelReferenceName
								AND c.COLUMN_NAME = mf.TableReferenceFieldKey
				)   
				WHEN mf.FK_FieldType_ID=11 AND (mf.FK_ExtType_ID=15 OR mf.FK_ExtType_ID=21) THEN 'varchar'
       
		   END AS FieldTypeSQLName ,  
            
				  CASE 
					WHEN mf.FK_FieldType_ID=11  AND (mf.FK_ExtType_ID = 15 OR mf.FK_ExtType_ID = 21) THEN -1
					WHEN mf.FK_FieldType_ID <> 11 AND mf.FK_ExtType_ID <> 21 THEN mf.SizeField
					WHEN mf.FK_FieldType_ID = 11 AND mf.FK_ExtType_ID<>15 AND mf.FK_ExtType_ID <> 21 THEN 
					(
						 SELECT 						  
							 case 
							 when c.CHARACTER_MAXIMUM_LENGTH IS NULL THEN '' 
							 WHEN c.CHARACTER_MAXIMUM_LENGTH=-1 THEN -1 
							 ELSE CONVERT(VARCHAR(20), c.CHARACTER_MAXIMUM_LENGTH) 
						 END                      
						 FROM   INFORMATION_SCHEMA.[COLUMNS] c
						 WHERE  c.TABLE_NAME = mf.TabelReferenceName
								AND c.COLUMN_NAME = mf.TableReferenceFieldKey
				) 
				 END  AS SizeFieldcurrent
				 ,CASE WHEN mf.FK_ExtType_ID<>15  THEN  c.DATA_TYPE ELSE 'varchar' end AS Data_type, 
				 CASE WHEN mf.FK_ExtType_ID<> 15  THEN  isnull(c.CHARACTER_MAXIMUM_LENGTH,'') ELSE '-1' END  AS sizeFieldexisting,
				 mf.[Required],
				c.IS_NULLABLE AS RequiredExisting
		FROM  (
			SELECT PK_ModuleDetailField_ID, FK_ModuleDetail_ID, FieldName, FK_FieldType_ID, SizeField,TabelReferenceName,TableReferenceFieldKey,FK_ExtType_ID, [Required]
			FROM  ModuleDetailField
			WHERE FK_ModuleDetail_ID=@PKModuleDetailID 
			UNION ALL
			SELECT a.PK_ModuleField_ID, @PKModuleDetailID, REPLACE(a.FieldName,'[FieldName]',b.FieldName) , a.FK_FieldType_ID, a.SizeField,NULL,NULL,a.FK_ExtType_ID, b.[Required]
			FROM  ModuleFieldDefaultFileUpload a
			CROSS JOIN dbo.ModuleDetailField b 
			WHERE b.FK_ModuleDetail_ID=@PKModuleDetailID AND b.FK_ExtType_ID IN (7, 8) AND b.FK_FieldType_ID=14     
	  ) mf
		   INNER JOIN MFieldType mt
				ON  mt.PK_FieldType_ID = mf.FK_FieldType_ID
		   INNER JOIN INFORMATION_SCHEMA.[COLUMNS] c
				ON  (
						mf.FieldName = c.COLUMN_NAME
						--AND mt.FieldTypeSQLName = c.DATA_TYPE
				) 
		WHERE  mf.FK_ModuleDetail_ID = @PKModuleDetailID
		   AND c.TABLE_NAME = @ModuleDetailName
	)xx 
	WHERE xx.FieldTypeSQLName<>xx.DATA_TYPE OR xx.SizeFieldcurrent<>xx.sizeFieldexisting  OR (CASE WHEN xx.[Required] = 1 THEN 'NO' ELSE 'YES' END) <> xx.RequiredExisting  

	OPEN my_cursor
	FETCH FROM my_cursor INTO @FieldNameubah,@FieldTypeSQLNameubah,@SizeFieldubah,@tipedataubah, @RequiredUbah
	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF @SizeFieldubah=-1
		BEGIN
			IF @FieldTypeSQLNameubah='varchar' OR @FieldTypeSQLNameubah='varbinary' SET @FieldTypeSQLNameubah=@FieldTypeSQLNameubah + '(max)'
		END
		ELSE
		BEGIN
			IF @FieldTypeSQLNameubah='varchar' SET @FieldTypeSQLNameubah=@FieldTypeSQLNameubah + '('+ convert(VARCHAR(20), @SizeFieldubah)	 +')'
			ELSE IF @FieldTypeSQLNameubah='varbinary' SET @FieldTypeSQLNameubah=@FieldTypeSQLNameubah + '(max)'
		END
	
	
		set @sql=' Alter table '+ @ModuleDetailName +' alter column ['+ @FieldNameubah  + ']  ' +@FieldTypeSQLNameubah
	
		EXEC(@sql)
		FETCH FROM my_cursor INTO  @FieldNameubah,@FieldTypeSQLNameubah,@SizeFieldubah,@tipedataubah, @RequiredUbah
	END
	CLOSE my_cursor
	DEALLOCATE my_cursor
	    
	END
END