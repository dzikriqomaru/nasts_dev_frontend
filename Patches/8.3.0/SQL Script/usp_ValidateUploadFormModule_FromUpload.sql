
/****** Object:  StoredProcedure [dbo].[usp_ValidateUploadFormModule_FromUpload]    Script Date: 5/13/2024 19:51:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER   PROCEDURE [dbo].[usp_ValidateUploadFormModule_FromUpload]  

/***********************************************************  
* Procedure description:  
* Date:   10/4/2016   
* Author: -  
*  
* Changes  
* Date        Modified By                 Comments  
************************************************************  
*  13 Mei 2024	Davin Julian	Add: Replacer @userid in validation expression
************************************************************/  
 @Moduleid INT ,@userid VARCHAR(50)   
 --* 18/12/2023 cek dulu sebelum penambahan kolom draft
AS        
BEGIN             
       DECLARE @dateformat VARCHAR(50)            
       SELECT @dateformat = sp.SettingValue        
       FROM   SystemParameter sp        
       WHERE  sp.PK_SystemParameter_ID = 14           
               
       DECLARE @PK_Module_ID            INT,        
               @ModuleName              VARCHAR(250),        
               @ModuleLabel             VARCHAR(250),        
               @ModuleDescription       VARCHAR(500),        
               @IsUseDesigner           BIT,        
               @IsUseApproval           BIT,        
               @IsSupportAdd            BIT,        
               @IsSupportEdit           BIT,        
               @IsSupportDelete         BIT,        
               @IsSupportActivation     BIT,        
               @IsSupportView           BIT,        
               @IsSupportUpload         BIT,        
               @UrlAdd                  VARCHAR(550),        
               @UrlEdit                 VARCHAR(550),        
               @UrlDelete               VARCHAR(550),        
               @UrlActivation           VARCHAR(550),        
               @UrlView                 VARCHAR(550),        
               @UrlUpload               VARCHAR(550),        
               @UrlApproval             VARCHAR(550),        
               @UrlApprovalDetail       VARCHAR(550),        
               @IsUseStoreProcedureValidation BIT,        
               @Active                  BIT,        
               @CreatedBy               VARCHAR(50),        
               @LastUpdateBy            VARCHAR(50),        
               @ApprovedBy              VARCHAR(50),        
               @CreatedDate             DATETIME,        
               @LastUpdateDate          DATETIME,        
               @ApprovedDate            DATETIME            
               
       SELECT @PK_Module_ID = PK_Module_ID,        
              @ModuleName              = ModuleName,        
              @ModuleLabel             = ModuleLabel,        
              @ModuleDescription       = ModuleDescription,        
              @IsUseDesigner           = IsUseDesigner,        
              @IsUseApproval           = IsUseApproval,        
              @IsSupportAdd            = IsSupportAdd,        
              @IsSupportEdit           = IsSupportEdit,        
              @IsSupportDelete         = IsSupportDelete,        
              @IsSupportActivation     = IsSupportActivation,        
              @IsSupportView           = IsSupportView,        
              @IsSupportUpload         = IsSupportUpload,        
              @UrlAdd                  = UrlAdd,        
              @UrlEdit                 = UrlEdit,        
              @UrlDelete               = UrlDelete,        
              @UrlActivation           = UrlActivation,        
              @UrlView                 = UrlView,        
              @UrlUpload               = UrlUpload,        
              @UrlApproval             = UrlApproval,        
              @UrlApprovalDetail       = UrlApprovalDetail,        
              @IsUseStoreProcedureValidation = IsUseStoreProcedureValidation,        
              @Active                  = [Active],        
              @CreatedBy               = CreatedBy,        
              @LastUpdateBy            = LastUpdateBy,        
              @ApprovedBy              = ApprovedBy,        
              @CreatedDate             = CreatedDate,        
              @LastUpdateDate          = LastUpdateDate,        
              @ApprovedDate            = ApprovedDate        
       FROM   dbo.Module m        
       WHERE  m.PK_Module_ID = @moduleid           
               
       DECLARE @sqldroptable VARCHAR(MAX) = ''        
               
       SET @sqldroptable = 'DROP TABLE IF EXISTS ' + @modulename + '_' + @userid + ' ' + CHAR(10)        
        + 'select * INTO  ' + @modulename + '_' + @userid + ' From ' + @modulename + ' WHERE 1=2 ' + CHAR(10)        
           + '   ALTER TABLE ' + @modulename + '_' + @userid + ' ADD  PK_upload_ID bigint '        
               
     EXEC (@sqldroptable)        
               
       DECLARE @roleid INT             
       DECLARE @filterPersonal VARCHAR(8000) = ''         
       DECLARE @filteraccess VARCHAR(8000) = ''          
       DECLARE @filterCombine VARCHAR(8000) = ''            
               
       SELECT @filterPersonal = dbo.ufn_GetFilterAdditionalPerUser(@userid, @Moduleid)          
               
       SELECT @roleid = m.FK_MRole_ID        
       FROM   MUser m        
       WHERE  m.UserID = @userid            
               
       IF EXISTS (        
              SELECT 1        
              FROM   INFORMATION_SCHEMA.TABLES        
              WHERE  TABLE_TYPE         = 'BASE TABLE'        
                     AND TABLE_NAME     = 'DataAccess'        
          )        
           SELECT TOP 1 @filteraccess = xx.FilterData        
           FROM   (        
                      SELECT REPLACE(da.FilterData, '@userid', @userid) AS FilterData,        
                             da.FK_Role_ID        
                      FROM   DataAccess da        
                      WHERE  da.FK_Role_ID = @roleid        
                             AND da.FK_Module_ID = @Moduleid        
                      UNION        
                      SELECT REPLACE(da.FilterData, '@userid', @userid) AS FilterData,        
                             da.FK_Role_ID        
                      FROM   DataAccess da        
                      WHERE  da.FK_Role_ID = 0        
                             AND da.FK_Module_ID = @Moduleid        
                  )xx        
           ORDER BY        
                  FK_Role_ID DESC        
       ELSE        
           SELECT @filteraccess = '';            
                 
     --IF len(@filteraccess) >0        
     --set @filteraccess ='  WHERE   ( '+ @filteraccess+' ) '            
                 
       IF LEN(@filteraccess) > 0        
          OR LEN(@filterPersonal) > 0        
           SET @filterCombine = ' where '           
               
       IF LEN(@filteraccess) > 0        
           SET @filterCombine = @filterCombine + ' ' + @filteraccess          
               
       IF LEN(@filterPersonal) > 0        
          AND LEN(@filteraccess) = 0        
           SET @filterCombine = @filterCombine + ' ' + @filterPersonal          
               
       IF LEN(@filterPersonal) > 0        
          AND LEN(@filteraccess) > 0        
           SET @filterCombine = @filterCombine + ' and ' + @filterPersonal          
               
       DECLARE @FK_Module_ID               INT,        
               @FieldName                  VARCHAR(250),        
               @FieldLabel                 VARCHAR(250),        
               @Sequence                   INT,        
               @Required                   BIT,        
               @IsPrimaryKey               BIT,        
               @IsUnik                     BIT,        
               @IsShowInView               BIT,        
               @FK_FieldType_ID            INT,        
               @SizeField                  INT,        
               @FK_ExtType_ID              INT,        
               @TabelReferenceName         VARCHAR(250),        
               @TableReferenceFieldKey     VARCHAR(250),        
               @TableReferenceFieldDisplayName VARCHAR(250),        
               @TableReferenceFilter       VARCHAR(550),        
               @IsUseRegexValidation       BIT,        
               @fields VARCHAR(MAX),        
               @fieldDefault               VARCHAR(MAX),        
               @fieldactive                VARCHAR(100),        
               @fieldvaluedefault          VARCHAR(MAX),        
               @FieldUpdate                VARCHAR(MAX),        
 @fieldupdatedefault         VARCHAR(MAX),        
               @filedupdateactive          VARCHAR(100),        
               @fieldprimarykey            VARCHAR(500),        
               @fieldInsertValue           VARCHAR(MAX)   ,@FieldIdentityon VARCHAR(8000),@FieldIdentityoff VARCHAR(8000)         
               
               
       SET @fieldInsertValue = ''                       
       SET @filedupdateactive = ''             
       SET @fields = ''            
       SET @fieldDefault = ''            
       SET @fieldactive = ''            
       SET @fieldvaluedefault = ''              
       SET @FieldUpdate = ''            
       SET @fieldupdatedefault = ''            
       SET @fieldprimarykey = ''     
       set @FieldIdentityon=''    
       set @FieldIdentityoff=''           
       DECLARE my_cursor CURSOR FAST_FORWARD READ_ONLY         
       FOR        
           SELECT FK_Module_ID,        
                  FieldName,        
               FieldLabel,        
                  Sequence,        
                  [Required],        
                  IsPrimaryKey,        
                  IsUnik,        
                  IsShowInView,        
                  FK_FieldType_ID,        
                  SizeField,        
                  FK_ExtType_ID,        
                  TabelReferenceName,        
                  TableReferenceFieldKey,        
                  TableReferenceFieldDisplayName,        
                  TableReferenceFilter,        
                  IsUseRegexValidation        
           FROM   dbo.ModuleField mf        
           WHERE  mf.FK_Module_ID = @moduleid        
                  AND mf.FK_FieldType_ID <> 14         
                      --AND mf.IsShowInForm=1        
           ORDER BY        
                  mf.Sequence          
               
       OPEN my_cursor            
               
       FETCH FROM my_cursor INTO @FK_Module_ID, @FieldName,            
       @FieldLabel, @Sequence, @Required, @IsPrimaryKey,            
       @IsUnik, @IsShowInView, @FK_FieldType_ID, @SizeField,            
       @FK_ExtType_ID, @TabelReferenceName,            
       @TableReferenceFieldKey,            
       @TableReferenceFieldDisplayName,            
       @TableReferenceFilter, @IsUseRegexValidation            
               
       WHILE @@FETCH_STATUS = 0        
       BEGIN        
           /*{ ... Cursor logic here ... }*/            
           IF @FK_ExtType_ID = 11        
               CONTINUE            
                   
                   
           IF @IsPrimaryKey = 1        
               SET @fieldprimarykey = @FieldName            
                   
           --IF @FK_FieldType_ID <> 12        
           --   AND @FK_FieldType_ID <> 15        
           --BEGIN        
           --    SET @fields = @fields + @FieldName + ',' + CHAR(10) + CHAR(13)        
           --END            
                   
                SET @fields = @fields + @FieldName + ',' + CHAR(10) + CHAR(13)       
                   
                   
           IF     @FK_FieldType_ID <> 11  AND @FK_FieldType_ID <> 10                        
           BEGIN        
               SET @fieldInsertValue = @fieldInsertValue + @FieldName + ',' + CHAR(10) + CHAR(13)        
           END            
               
           IF @FK_FieldType_ID=12 OR @FK_FieldType_ID=15     
           BEGIN    
             SET @FieldIdentityon=' set identity_insert '+  @ModuleName + '_' + @userid +' on'    
             SET @FieldIdentityoff=' set identity_insert '+  @ModuleName + '_' + @userid +' off'    
           END    
               
           --IF @FK_FieldType_ID <> 12        
           --   AND @FK_FieldType_ID <> 11        
           --   AND @FK_FieldType_ID <> 10        
           --   AND @FK_FieldType_ID <> 15        
           --BEGIN        
           --    SET @fieldInsertValue = @fieldInsertValue + @FieldName + ',' + CHAR(10) + CHAR(13)        
           --END            
                   
       
                   
                   
           IF @FK_FieldType_ID BETWEEN 1 AND 9 --varchar        
           BEGIN        
               SET @FieldUpdate = @FieldUpdate + @FieldName + '=' + @ModuleName + '_upload.' + @FieldName + ',' + CHAR(10)        
                   + CHAR(13)        
           END            
                   
           IF @FK_FieldType_ID = 10        
           BEGIN        
               SET @FieldUpdate = @FieldUpdate + @FieldName + '=dbo.ufn_GetDateValue(' + @ModuleName + '_upload.' +         
                   @FieldName + ',''' + @dateformat + '''),' + CHAR(10) + CHAR(13)            
                       
               IF @Required = 1        
               BEGIN        
                   SET @fieldInsertValue = @fieldInsertValue + @FieldName + ',' + CHAR(10) + CHAR(13)        
               END        
               ELSE        
               BEGIN        
                   SET @fieldInsertValue = @fieldInsertValue + 'CASE WHEN  ' + @FieldName + '='''' or ' + @FieldName +         
                       ' =''NULL'' then null else ' + @FieldName + ' end as ' + @FieldName + ' ,' + CHAR(10) + CHAR(13)        
               END        
           END            
                   
           IF @FK_FieldType_ID = 11        
           BEGIN        
               SET @FieldUpdate = @FieldUpdate + @FieldName + '=SUBSTRING( ' + @ModuleName + '_upload.' + @FieldName +         
                   ', CHARINDEX(''('', ' + @ModuleName + '_upload.' + @FieldName + ',1)+1,  case when CHARINDEX('')'', '        
                   + @ModuleName + '_upload.' + @FieldName + ',1)-2 >0 then CHARINDEX('')'', ' + @ModuleName +         
                   '_upload.' + @FieldName + ',1)-2 ELSE 0 END ),' + CHAR(10) + CHAR(13)            
                       
               SET @fieldInsertValue = @fieldInsertValue + 'SUBSTRING( ' + @ModuleName + '_upload.' + @FieldName +         
                   ', CHARINDEX(''('', ' + @ModuleName + '_upload.' + @FieldName + ',1)+1,  case when CHARINDEX('')'', '        
                   + @ModuleName + '_upload.' + @FieldName + ',1)-2 >0 then CHARINDEX('')'', ' + @ModuleName +         
                   '_upload.' + @FieldName + ',1)-2 ELSE 0 END ),' + CHAR(10) + CHAR(13)        
           END             
                   
           IF @FK_FieldType_ID = 13--varchar        
           BEGIN        
               SET @FieldUpdate = @FieldUpdate + @FieldName + '=' + @ModuleName + '_upload.' + @FieldName + ',' + CHAR(10)        
                   + CHAR(13)        
           END         
                   
           FETCH FROM my_cursor INTO @FK_Module_ID, @FieldName,         
           @FieldLabel, @Sequence, @Required, @IsPrimaryKey,         
           @IsUnik, @IsShowInView, @FK_FieldType_ID,         
           @SizeField, @FK_ExtType_ID, @TabelReferenceName,         
           @TableReferenceFieldKey,         
           @TableReferenceFieldDisplayName,         
           @TableReferenceFilter, @IsUseRegexValidation        
       END            
               
       CLOSE my_cursor            
       DEALLOCATE my_cursor            
               
       SET @fields=@fields +'PK_upload_ID,'  + CHAR(10) + CHAR(13)        
        SET @fieldInsertValue=@fieldInsertValue +'PK_upload_ID,'  + CHAR(10) + CHAR(13)        
               
       IF LEN(@fields) > 0        
           SET @fields = SUBSTRING(@fields, 1, LEN(@fields) -3)        
               
       IF LEN(@fieldInsertValue) > 0        
           SET @fieldInsertValue = SUBSTRING(@fieldInsertValue, 1, LEN(@fieldInsertValue) -3)    
		   
		
		 --cek dulu ada Field Draft atau tidak
		declare @sqlQuery nvarchar(200)
		DECLARE @rcount NUMERIC(18,0)   
		set @sqlQuery = 'SELECT COUNT (COLUMN_NAME) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = ''' + @ModuleName +''' and COLUMN_NAME =''Draft''' 
		EXECUTE @rcount=sp_executesql @sqlQuery
               
               
       DECLARE @PK_ModuleField_ID_Default BIGINT,        
               @FieldName_Default           VARCHAR(250),        
               @FieldLabel_Default          VARCHAR(250),        
               @Sequence_Default            INT,        
               @Required_Default            BIT,        
               @IsPrimaryKey_Default        BIT,        
   @IsUnik_Default              BIT,        
               @FK_FieldType_ID_Default     INT,        
               @SizeField_Default           INT,        
               @FK_ExtType_ID_Default       INT            
               
       DECLARE my_cursor CURSOR FAST_FORWARD READ_ONLY         
       FOR       
           SELECT PK_ModuleField_ID,        
                  FieldName,        
                  FieldLabel,        
                  Sequence,        
                  [Required],        
                  IsPrimaryKey,        
                  IsUnik,        
                  FK_FieldType_ID,        
                  SizeField,        
                  FK_ExtType_ID        
           FROM   dbo.ModuleFieldDefault
		    WHERE FieldName <> iif(@rcount > 0 ,'', 'Draft')
               
       OPEN my_cursor            
               
       FETCH FROM my_cursor INTO @PK_ModuleField_ID_Default, @FieldName_Default, @FieldLabel_Default,            
       @Sequence_Default, @Required_Default, @IsPrimaryKey_Default, @IsUnik_Default,            
       @FK_FieldType_ID_Default, @SizeField_Default, @FK_ExtType_ID_Default            
               
       WHILE @@FETCH_STATUS = 0        
       BEGIN        
           /*{ ... Cursor logic here ... }*/            
                   
           SET @fieldDefault = @fieldDefault + @FieldName_Default + ',' + CHAR(10) + CHAR(13)            
                   
           IF @FK_FieldType_ID_Default = 13        
           BEGIN        
               SET @fieldactive = 'isnull(' + @FieldName_Default + ',1),' + CHAR(10) + CHAR(13)                       
               SET @filedupdateactive = @filedupdateactive + @FieldName_Default + '=isnull(' + @ModuleName + '_upload.'         
                   + @FieldName_Default + ',1),' + CHAR(10) + CHAR(13)        
           END         
                   
           --cek tipenya varchar(percaya kalau cuma isinya createdby,lastupdateby,approveby)            
           IF @FK_FieldType_ID_Default = 9        
           BEGIN        
               SET @fieldvaluedefault = @fieldvaluedefault + '''' + @userid + ''',' + CHAR(10) + CHAR(13)        
           END        
                   
           IF @PK_ModuleField_ID_Default = 3        
              OR @PK_ModuleField_ID_Default = 4        
               SET @fieldupdatedefault = @fieldupdatedefault + @FieldName_Default + '=''' + @userid + ''',' + CHAR(10) +         
                   CHAR(13)         
                   
                   
                   
           --cek tipenya date(percaya kalau isinya createdate,lastupdatedate,approveate)            
           IF @FK_FieldType_ID_Default = 10        
               SET @fieldvaluedefault = @fieldvaluedefault + '''' + CONVERT(VARCHAR(20), GETDATE(), 120) + ''',' + CHAR(10)         
                   + CHAR(13)            
                   
           IF @PK_ModuleField_ID_Default = 6        
              OR @PK_ModuleField_ID_Default = 7        
               SET @fieldupdatedefault = @fieldupdatedefault + @FieldName_Default + '=''' + CONVERT(VARCHAR(20), GETDATE(), 120)         
                   + ''',' + CHAR(10) + CHAR(13)         
                   
                   
           FETCH FROM my_cursor INTO @PK_ModuleField_ID_Default, @FieldName_Default,         
           @FieldLabel_Default,    
           @Sequence_Default, @Required_Default, @IsPrimaryKey_Default, @IsUnik_Default,         
           @FK_FieldType_ID_Default, @SizeField_Default, @FK_ExtType_ID_Default        
       END            
               
       CLOSE my_cursor            
       DEALLOCATE my_cursor            
               
               
       IF LEN(@fieldDefault) > 0        
           SET @fieldDefault = SUBSTRING(@fieldDefault, 1, LEN(@fieldDefault) -3)            
               
       IF LEN(@fieldvaluedefault) > 0        
           SET @fieldvaluedefault = SUBSTRING(@fieldvaluedefault, 1, LEN(@fieldvaluedefault) -3)            
               
               
       DECLARE @strinsert VARCHAR(MAX) = ''            
               
               
       SET @strinsert = @FieldIdentityon +' INSERT INTO ' + @ModuleName + '_' + @userid + ' ' + '(' + CHAR(10) + CHAR(13)              
       SET @strinsert += @fields             
       IF LEN(@fieldDefault) > 0        
       BEGIN        
           SET @strinsert += ',' + CHAR(10) + CHAR(13)            
           SET @strinsert += @fieldDefault + CHAR(10) + CHAR(13)        
       END            
               
      SET @strinsert += ')' + CHAR(10) + CHAR(13)            
       SET @strinsert += ' SELECT ' + CHAR(10) + CHAR(13)            
       SET @strinsert += @fieldInsertValue             
       IF LEN(@fieldactive) > 0        
       BEGIN        
           SET @strinsert += ',' + CHAR(10) + CHAR(13)            
           SET @strinsert += @fieldactive + CHAR(10) + CHAR(13)        
       END            
               
       SET @strinsert += @fieldvaluedefault + CHAR(10) + CHAR(13)            
               
               
       SET @strinsert += ' FROM ' + @ModuleName +        
           '_Upload  WHERE nawa_userid=''' + @userid + ''''            
               
       SET @strinsert += ' AND (nawa_Action=''INSERT'' or nawa_Action=''Update'') AND KeteranganError='''' ' +@FieldIdentityoff             
           
       EXEC (@strinsert)            
                   
        
        
        
        
--Indra, 23 Mei 2019:        
--Ganti proses validasi Upload, dilakukan bukan perbaris tapi langsung 1 table,        
--Disamakan seperti sp usp_ExecuteValidationBySegmentData        
        
 DECLARE @SQL                      VARCHAR(MAX),            
   @PK_ValidationParameter BIGINT,        
         @ValidationExpression     VARCHAR(MAX),            
         @ValidationType           VARCHAR(50),            
         @ValidationMessage        VARCHAR(8000),            
         @ExpressionType           VARCHAR(50),            
         --@ModuleID                 VARCHAR(100),            
         @FieldNamevalidation                VARCHAR(250),            
         @ModuleNamevalidation               VARCHAR(200),            
         @ModuleLabelvalidation              VARCHAR(200),            
         @ModuleURL                VARCHAR(5000),            
         @FieldLabelvalidation               VARCHAR(200),            
         @KeyField                 VARCHAR(500),            
         @KeyValue                 VARCHAR(500),            
         @ErrorType                VARCHAR(50) ,        
         @fkfieldtypeid int           
             
 DECLARE my_cursor CURSOR FAST_FORWARD READ_ONLY             
 FOR            
SELECT vp.PK_ValidationParameter,         
   vp.ValidationExpression,            
            vt.ValidationType,            
            vp.ValidationMessage,            
            vp.ExpressionType,            
            vp.TableName,            
            vp.FieldName,            
            m.ModuleName,            
            m.ModuleLabel,        
            m.UrlEdit,        
            mf.FieldLabel,        
            IsNull(mfPK.FieldName,''),        
            IsNull(mfPK.FieldName,''),        
            vp.ErrorType,mf.FK_FieldType_ID        
     FROM   ValidationParameter    AS vp        
    INNER JOIN ValidationType vt        
    ON vp.ValidationType = vt.PK_ValidationType_ID        
      LEFT JOIN Module       AS m            
                 ON  m.PK_Module_ID = vp.TableName            
            LEFT JOIN ModuleField  AS mf            
                 ON  mf.FieldName = vp.FieldName AND mf.FK_Module_ID=m.PK_Module_ID            
            LEFT JOIN ModuleField  AS mfPK
                 ON  mfPK.IsPrimaryKey = 1 AND mfPK.FK_Module_ID=m.PK_Module_ID            
     WHERE  vp.[Active] = 1            
     AND mf.FieldLabel IS NOT NULL            
            AND m.PK_Module_ID = @Moduleid        
            
                        
             
 OPEN my_cursor            
             
FETCH FROM my_cursor INTO @PK_ValidationParameter, @ValidationExpression, @ValidationType, @ValidationMessage, @ExpressionType,         
 @ModuleID, @FieldNamevalidation, @ModuleNamevalidation, @ModuleLabelvalidation, @ModuleURL, @FieldLabelvalidation, @KeyField, @KeyValue, @ErrorType  ,@fkfieldtypeid        
             
 WHILE @@FETCH_STATUS = 0            
 BEGIN            
	--Replacer @userid in validation expression
 	SET @ValidationExpression=REPLACE(@ValidationExpression,'@userid','_'+@Userid)
     IF @ExpressionType = 'EXPRESSION' AND @ValidationExpression NOT LIKE '%SubmitStatus%'        
     BEGIN            
  DECLARE @StrQuery AS VARCHAR(MAX)        
        
        
        
          
 SET @StrQuery = ' ' + char(10)        
    + '        ' + char(10)        
    + ' UPDATE '+@ModuleName+'_Upload ' + char(10)        
    + ' SET    KeteranganError = KeteranganError + ''</br>'' + CHAR(13) + CHAR(10) +  ' + char(10)        
    + '        '' '+@ErrorType+' - '+@ValidationType+' - '+ REPLACE(@ValidationMessage, '''', '''''') +''' ' + char(10)        
    + ' FROM  '+@ModuleName+'_Upload ' + char(10)        
    + ' INNER JOIN  ' + char(10)        
    + ' ( ' + char(10)        
    + '  ' + char(10)        
    + ' SELECT pk_upload_id FROM '+@ModuleName+'_'+@Userid+' hd    ' + char(10)        
    + '  '+ @ValidationExpression +' ' + char(10)        
    + '        ' + char(10)        
    + '               ' + char(10)        
    + ' )xx ' + char(10)        
    + ' ON '+@ModuleName+'_Upload.pk_upload_id= xx.pk_upload_id ' + char(10)        
    + ' where nawa_userid =''' + @userid + ''''        
        
        insert into AuditTrailLog
	(LogDescription) values(@StrQuery)
        
  EXEC(@StrQuery)        
        
     END            
        
        
     FETCH FROM my_cursor INTO @PK_ValidationParameter, @ValidationExpression, @ValidationType, @ValidationMessage, @ExpressionType,         
   @ModuleID, @FieldNamevalidation, @ModuleNamevalidation, @ModuleLabelvalidation, @ModuleURL, @FieldLabelvalidation, @KeyField, @KeyValue, @ErrorType    ,@fkfieldtypeid        
 END            
             
 CLOSE my_cursor            
 DEALLOCATE my_cursor    
     
END
