INSERT INTO systemDictionary (
    Keywords, LanguageType, TextDefinition, Active,
    CreatedBy, LastUpdateBy, ApprovedBy,
    CreatedDate, LastUpdateDate, ApprovedDate
)
VALUES (
    'edit-text', 'en', 'Ubah', 1,
    'sysadmin', 'sysadmin', 'sysadmin',
    GETDATE(), GETDATE(), GETDATE()
), (
    'edit-text', 'id', 'Mengubah', 1,
    'sysadmin', 'sysadmin', 'sysadmin',
    GETDATE(), GETDATE(), GETDATE()
), (
    'confirm-text', 'en', '`Are you sure you want to ${name} this data ?`', 1,
    'sysadmin', 'sysadmin', 'sysadmin',
    GETDATE(), GETDATE(), GETDATE()
), (
    'confirm-text', 'id', '`Apakah Anda yakin ingin ${name} data ini ?`', 1,
    'sysadmin', 'sysadmin', 'sysadmin',
    GETDATE(), GETDATE(), GETDATE()
), (
    'deactive-text', 'en', 'Deactive', 1,
    'sysadmin', 'sysadmin', 'sysadmin',
    GETDATE(), GETDATE(), GETDATE()
), (
    'deactive-text', 'id', 'Menonaktifkan', 1,
    'sysadmin', 'sysadmin', 'sysadmin',
    GETDATE(), GETDATE(), GETDATE()
), (
    'active-text', 'en', 'Active', 1,
    'sysadmin', 'sysadmin', 'sysadmin',
    GETDATE(), GETDATE(), GETDATE()
), (
    'active-text', 'id', 'Mengaktifkan', 1,
    'sysadmin', 'sysadmin', 'sysadmin',
    GETDATE(), GETDATE(), GETDATE()
), (
    'delete-text', 'en', 'Delete', 1,
    'sysadmin', 'sysadmin', 'sysadmin',
    GETDATE(), GETDATE(), GETDATE()
), (
    'delete-text', 'id', 'Menghapus', 1,
    'sysadmin', 'sysadmin', 'sysadmin',
    GETDATE(), GETDATE(), GETDATE()
), (
    'insert-text', 'en', 'Insert', 1,
    'sysadmin', 'sysadmin', 'sysadmin',
    GETDATE(), GETDATE(), GETDATE()
), (
    'insert-text', 'id', 'Menambahkan', 1,
    'sysadmin', 'sysadmin', 'sysadmin',
    GETDATE(), GETDATE(), GETDATE()
), (
    'save-changes-text-2', 'en', 'You can save your changes, discard your changes, or cancel to continue editing', 1,
    'sysadmin', 'sysadmin', 'sysadmin',
    GETDATE(), GETDATE(), GETDATE()
), (
    'save-changes-text-2', 'id', 'Anda dapat menyimpan perubahan, membuang perubahan, atau membatalkan untuk melanjutkan pengeditan', 1,
    'sysadmin', 'sysadmin', 'sysadmin',
    GETDATE(), GETDATE(), GETDATE()
), (
    'save-changes-text-1', 'en', 'You have unsaved changes. Do you want to continue ?', 1,
    'sysadmin', 'sysadmin', 'sysadmin',
    GETDATE(), GETDATE(), GETDATE()
), (
    'save-changes-text-1', 'id', 'Anda memiliki perubahan yang belum disimpan. Apakah Anda ingin melanjutkan ?', 1,
    'sysadmin', 'sysadmin', 'sysadmin',
    GETDATE(), GETDATE(), GETDATE()
), (
    'save-changes', 'en', 'Save Changes', 1,
    'sysadmin', 'sysadmin', 'sysadmin',
    GETDATE(), GETDATE(), GETDATE()
), (
    'save-changes', 'id', 'Simpan Perubahan', 1,
    'sysadmin', 'sysadmin', 'sysadmin',
    GETDATE(), GETDATE(), GETDATE()
), (
    'discard', 'en', 'Discard', 1,
    'sysadmin', 'sysadmin', 'sysadmin',
    GETDATE(), GETDATE(), GETDATE()
), (
    'discard', 'id', 'Buang', 1,
    'sysadmin', 'sysadmin', 'sysadmin',
    GETDATE(), GETDATE(), GETDATE()
), (
    'approve', 'en', 'Approve', 1,
    'sysadmin', 'sysadmin', 'sysadmin',
    GETDATE(), GETDATE(), GETDATE()
), (
    'approve', 'id', 'Setujui', 1,
    'sysadmin', 'sysadmin', 'sysadmin',
    GETDATE(), GETDATE(), GETDATE()
), (
    'reject', 'en', 'Reject', 1,
    'sysadmin', 'sysadmin', 'sysadmin',
    GETDATE(), GETDATE(), GETDATE()
), (
    'reject', 'id', 'Tolak', 1,
    'sysadmin', 'sysadmin', 'sysadmin',
    GETDATE(), GETDATE(), GETDATE()
), (
    'approve-current', 'en', 'Approve Current Page', 1,
    'sysadmin', 'sysadmin', 'sysadmin',
    GETDATE(), GETDATE(), GETDATE()
), (
    'approve-current', 'id', 'Setujui Halaman Ini', 1,
    'sysadmin', 'sysadmin', 'sysadmin',
    GETDATE(), GETDATE(), GETDATE()
), (
    'reject-current', 'en', 'Reject Current Page', 1,
    'sysadmin', 'sysadmin', 'sysadmin',
    GETDATE(), GETDATE(), GETDATE()
), (
    'reject-current', 'id', 'Tolak Halaman Ini', 1,
    'sysadmin', 'sysadmin', 'sysadmin',
    GETDATE(), GETDATE(), GETDATE()
), (
    'approve-selected', 'en', 'Approve Selected', 1,
    'sysadmin', 'sysadmin', 'sysadmin',
    GETDATE(), GETDATE(), GETDATE()
), (
    'approve-selected', 'id', 'Setujui Yang Terpilih', 1,
    'sysadmin', 'sysadmin', 'sysadmin',
    GETDATE(), GETDATE(), GETDATE()
), (
    'reject-selected', 'en', 'Reject Selected', 1,
    'sysadmin', 'sysadmin', 'sysadmin',
    GETDATE(), GETDATE(), GETDATE()
), (
    'reject-selected', 'id', 'Tolak yang Terpilih', 1,
    'sysadmin', 'sysadmin', 'sysadmin',
    GETDATE(), GETDATE(), GETDATE()
)


INSERT INTO SystemDictionaryDetail (Keywords, ParamName) VALUES ('confirm-text', 'name="Module"')