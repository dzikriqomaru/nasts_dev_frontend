INSERT INTO SystemParameter(PK_SystemParameter_ID, FK_SystemParameterGroup_ID, SettingName, SettingValue, Active, Hide, fk_MFieldType_ID, IsEncript, EncriptionKey)
VALUES(-140, 2, 'Fallback Login with SHA1', '1', 1, 0, 11, 0, '');

INSERT INTO SystemParameterPickList(Pk_ApplicationAuthentication_ID, ApplicationAuthentication, SettingName)
VALUES(0, 'False', 'Fallback Login with SHA1');
INSERT INTO SystemParameterPickList(Pk_ApplicationAuthentication_ID, ApplicationAuthentication, SettingName)
VALUES(1, 'True', 'Fallback Login with SHA1');