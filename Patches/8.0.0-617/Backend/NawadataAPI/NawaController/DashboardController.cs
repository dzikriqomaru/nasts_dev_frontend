﻿using DevExpress.DashboardWeb;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using NawaDataAPI.Services;
using NawaDataBLL;
using NawaDataDAL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Claims;
using System.Xml.Linq;

namespace NawaDataAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DashboardController : ControllerBase
    {
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        [Route("GetDashboardChart")]
        public ActionResult GetDashboardChart(int chartId)
        {
            List<CChartSetting> Data = new List<CChartSetting>();
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string CurrentUser = identity.FindFirst("UserID").Value; 
                int GroupMenuID = Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value); 
                int MRoleID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value);
                Data = DashboardBLL.GetDashboardChart(CurrentUser, GroupMenuID, MRoleID,chartId);
                return Ok(Data);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("devExpressDashboardView")]
        [HttpGet]
        public XDocument LoadDashboard(string dashboardID)
        {
            IHttpContextAccessor contextAccessor = (IHttpContextAccessor)HttpContext;

            DatabaseEditableDashboardStorage dashboardView = new DatabaseEditableDashboardStorage(contextAccessor);

            return dashboardView.LoadDashboard(dashboardID);
        }

        [Route("GetDashboardList")]
        [HttpGet]

        public ActionResult GetDashboardList(int page, string search)
        {
            try
            {
                string query = "SELECT COUNT(13) FROM DevExpDashboard WHERE DevExpDashboard_Caption LIKE @search ";

                SqlParameter[] prms = new SqlParameter[]
                {
                    new SqlParameter("@search", SqlDbType.VarChar){ Value = "%" + search + "%" },
                };

                int rowCount = Convert.ToInt32(NawaDataDAL.NawaDAO.ExecuteScalar(query, prms));

                AppSystemParameter pageSizeSysParam = SystemParameterBLL.GetAppSystemParameter(7);

                int pageSize = pageSizeSysParam != null ? Convert.ToInt32(pageSizeSysParam.SettingValue) : 12;
                if (pageSize <= 0)
                    pageSize = 1;

                int totalPage = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(rowCount) / pageSize));

                query = "SELECT DevExpDashboard_ID AS ID ,DevExpDashboard_Caption AS Name FROM DevExpDashboard WHERE DevExpDashboard_Caption LIKE @search ORDER BY DevExpDashboard_ID OFFSET(@PageIndex - 1)" +
                    " * @PageSize ROWS FETCH NEXT @PageSize ROWS ONLY";

                prms = new SqlParameter[]
               {
                    new SqlParameter("@search", SqlDbType.VarChar){  Value = "%" + search + "%" },
                    new SqlParameter("@PageIndex", SqlDbType.Int){ Value = page },
                    new SqlParameter("@PageSize", SqlDbType.Int){ Value = pageSize }
               };

                DataTable dt = NawaDataDAL.NawaDAO.ExecuteTable(query,prms);

                List<DashboardInfo> list = NawaDataDAL.NawaDAO.ConvertDataTable<DashboardInfo>(dt);
                return Ok(new { count = rowCount, rows = list, totalPage = totalPage });

            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

    }
}
