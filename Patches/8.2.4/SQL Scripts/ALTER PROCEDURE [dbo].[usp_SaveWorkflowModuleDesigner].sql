USE [NawadataTemplateNF8AlphaRelease]
GO
/****** Object:  StoredProcedure [dbo].[usp_SaveWorkflowModuleDesigner]    Script Date: 5/2/2024 12:05:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[usp_SaveWorkflowModuleDesigner]
	@FK_ModuleApproval_ID BIGINT,
	@pkmoduleid INT,
	@pkunik VARCHAR(MAX),
	@pkuseridexecute INT,
	@notes VARCHAR(MAX),
	@intworkflowstatus int
AS
BEGIN
SET NOCOUNT ON
-- 14/07/2022 Firman Nugraha, Fix Workflow comment 20220714

 --20220714 Update declare untuk mWorkflow email ---                
  DECLARE @ReqEmail INT,                
    @RevEmail INT,                
    @ApprovedEmail INT,                
    @RejectEmail INT,                
    @EmailAddress VARCHAR(MAX),                
    @UserNameEmail VARCHAR(MAX),                
    @RoleIDEmail INT,                
    @UserIDEmail VARCHAR(MAX),                
    @PKUserIDPrev INT,                
    @IntLevelPrev INT,                
    @UserTypePrev INT
--End 20220714 Update declare untuk mWorkflow email ---

 DECLARE @userexecute AS VARCHAR(500)
 SELECT @userexecute = muser.UserName
 FROM muser
 WHERE PK_MUser_ID = @pkuseridexecute
 
 
 UPDATE MWorkFlow_History
 SET
 FK_MUserId=@pkuseridexecute,UserName=@userexecute, -- 20220714
 UserNameExecute = @userexecute,
 ResponseDate = GETDATE(),
 FK_MWorkflow_ApprovalStatus_ID = @intworkflowstatus,
 Notes = @notes
 FROM MWorkFlow_History x
 WHERE x.FK_Module_ID = @pkmoduleid
	   AND x.FK_Unik_ID = @pkunik
	   AND x.FK_MWorkflow_ApprovalStatus_ID IS NULL

 DECLARE @pkworkflowid INT,
		 @intlevel INT,
		 @previoususerid int
 
 SELECT @pkworkflowid = FK_MWorkflow_ID,
		@intlevel = WorkflowLevel,
		@previoususerid = FK_MWorkflowUser_ID
 FROM MWorkFlow_Progress
 WHERE FK_Module_ID = @pkmoduleid
	   AND FK_Unik_ID = @pkunik

 IF @intworkflowstatus = 1 OR @intworkflowstatus = 4 --accept
 BEGIN
	DECLARE @jmlnextworkflow INT,
			@usertype int
	
	SELECT @jmlnextworkflow = COUNT(1)
	FROM MWorkFlowDetail AS mfd
	WHERE mfd.FK_WorkFlow = @pkworkflowid
		  AND mfd.[Level] = @intlevel + 1
		  
	SELECT @usertype = mfd.UserType
	FROM MWorkFlowDetail AS mfd
	WHERE mfd.FK_WorkFlow = @pkworkflowid
		  AND mfd.[Level] = @intlevel + 1
	
	DECLARE @pknextuserid INT,
			@usernamenext VARCHAR(500)
	
	IF @usertype = 2
	BEGIN
		SELECT @pknextuserid = ms.FK_Parent_ID
		FROM MUserStructure AS ms
		WHERE ms.FK_User_ID = @previoususerid

		SELECT @usernamenext = m.UserName
		FROM MUser AS m
		WHERE m.PK_MUser_ID = @pknextuserid
	END
	IF @usertype = 4
		SET @pknextuserid=null 
 
	IF @jmlnextworkflow > 0 AND ((@usertype = 2 AND @pknextuserid IS NOT NULL) OR @usertype = 4)
	BEGIN
		--ada NEXTworkflow
		DELETE FROM MWorkFlow_Progress
		WHERE FK_Module_ID = @pkmoduleid
			AND FK_Unik_ID = @pkunik

		INSERT INTO MWorkFlow_Progress(
			-- PK_MWorkflow_Progress_ID -- this column value is auto-generated
			FK_Module_ID,
			FK_Unik_ID,
			FK_MWorkflow_ID,
			WorkflowLevel,
			FK_MWorkFlowUserType_ID,
			FK_MWorkflowRole_ID,
			FK_MWorkflowUser_ID,
			FK_MWorkflow_Status_ID,
			SLAType,
			SLAValue,
			BreachSLAAction,
			BreachSLAActionLevelTo,
			RevisedActionLevelTo
		) 
		SELECT @pkmoduleid, @pkunik, mfd.FK_WorkFlow, mfd.[Level], mfd.UserType, mfd.RoleID, @pknextuserid,
			   3, mfd.SLAType, mfd.SLAValue, mfd.BreachSLAAction, mfd.BreachSLAActionTo, mfd.RevisedActionTo
		FROM MWorkFlowDetail AS mfd
		WHERE mfd.FK_WorkFlow = @pkworkflowid
			  AND mfd.[Level] = @intlevel + 1
		INSERT INTO MWorkFlow_History(
			-- PK_MWorkflow_History_ID -- this column value is auto-generated 
			FK_ModuleApproval_ID, 
			FK_Module_ID,
			FK_Unik_ID,
			FK_MUserId,
			FK_MRoleId,
			intLevel,
			RoleName,
			UserName,
			UserNameExecute,
			CreatedDate,
			ResponseDate,
			FK_MWorkflow_ApprovalStatus_ID,
			Notes
		)
		SELECT @FK_ModuleApproval_ID, @pkmoduleid, @pkunik, @pknextuserid, mfd.RoleID,
			   mfd.[Level], m.RoleName, @usernamenext, NULL, GETDATE(), NULL, NULL, NULL
		FROM MWorkFlowDetail AS mfd
			 INNER JOIN MRole AS m
			 ON mfd.RoleID = m.PK_MRole_ID
		WHERE mfd.FK_WorkFlow = @pkworkflowid
			  AND mfd.[Level] = @intlevel + 1
		-- 20220714 Update Email Template Request ---                
                 
		 SELECT @ReqEmail = ISNULL(ReqEmailTemplateID,0)                
		 FROM MWorkFlow WHERE PK_WorkFlow = @pkworkflowid                
                
		IF @ReqEmail > 0                
			BEGIN                
				IF @usertype=2                 
					BEGIN         
						SELECT @EmailAddress = UserEmailAddress, @UserNameEmail = UserName, @UserIDEmail = UserID                
						FROM Muser WHERE PK_MUser_ID = @pknextuserid                
                  
						INSERT INTO MWorkflow_Email (FK_Module_ID,FK_Unik_ID,UserID,EmailAddress,IsSendEmail,FK_MWorkflow_ApprovalStatus_ID,UserName,Active,CreatedBy,CreatedDate, FK_ModuleApproval_ID)                
						SELECT @pkmoduleid, @pkunik, @UserIDEmail, @EmailAddress, 0, @intworkflowstatus, @UserNameEmail,1,@UserNameEmail,GETDATE(),@FK_ModuleApproval_ID              
					END                
				ELSE IF @usertype=4                
					BEGIN              
						SELECT @RoleIDEmail = RoleID                
						FROM MWorkFlowDetail WHERE FK_WorkFlow=@pkworkflowid AND [Level]=@intlevel+1                   
                   
						INSERT INTO MWorkflow_Email (FK_Module_ID,FK_Unik_ID,UserID,EmailAddress,IsSendEmail,FK_MWorkflow_ApprovalStatus_ID,UserName,Active,CreatedBy,CreatedDate,FK_ModuleApproval_ID)                
						SELECT @pkmoduleid, @pkunik, UserID, UserEmailAddress, 0, @intworkflowstatus, UserName,1,UserName,GETDATE(),@FK_ModuleApproval_ID                
						FROM MUser WHERE FK_MRole_ID = @RoleIDEmail                 
						--EXEC usp_EMailScheduler_Create @ReqEmail, @pkunik, 0          
					END                
				EXEC usp_EMailScheduler_Create @ReqEmail, @FK_ModuleApproval_ID, 0         

				UPDATE MWorkflow_Email SET IsSendEmail = 1 WHERE FK_Module_ID = @pkmoduleid AND FK_Unik_ID = @pkunik                
			END          
		-- 20220714 End Update Email Template Request  ---        

		SELECT CAST(0 AS BIT) FinalAccept
	 END
	ELSE
	BEGIN
	--sudah ngak ada lagi,maka sudah selesai
	UPDATE MWorkFlow_Progress
	SET FK_MWorkflow_Status_ID = 4
	WHERE FK_Module_ID = @pkmoduleid
			AND FK_Unik_ID = @pkunik
	-- 20220714 Update Email Template Approved  ---         
	SELECT @IntLevelPrev = xx.Level, @UserTypePrev = xx.UserType  FROM                
	(                
		SELECT ROW_NUMBER() over (partition by FK_WorkFlow order by [Level] ASC) as rn, *                
		FROM MWorkFlowDetail WHERE FK_WorkFlow = @pkworkflowid                
	)xx                
	WHERE xx.rn = 1;                
                   
	IF @UserTypePrev = 1                 
		BEGIN                
			SELECT @PKUserIDPrev = FK_MUserId                
			FROM MWorkFlow_History WHERE FK_Module_ID = @pkmoduleid AND FK_Unik_ID = @pkunik AND intLevel = @IntLevelPrev                
                 
			SELECT @ApprovedEmail = ISNULL(ApprovedEmailTemplateID,0)                
			FROM MWorkFlow WHERE PK_WorkFlow = @pkworkflowid                
                
			IF @ApprovedEmail > 0                
			BEGIN                
				SELECT @EmailAddress = UserEmailAddress, @UserNameEmail = UserName, @UserIDEmail = UserID                
				FROM Muser WHERE PK_MUser_ID = @PKUserIDPrev                
                  
				INSERT INTO MWorkflow_Email (FK_Module_ID,FK_Unik_ID,UserID,EmailAddress,IsSendEmail,FK_MWorkflow_ApprovalStatus_ID,UserName,Active,CreatedBy,CreatedDate,FK_ModuleApproval_ID)                
				SELECT @pkmoduleid, @pkunik, @UserIDEmail, @EmailAddress, 0, @intworkflowstatus, @UserNameEmail,1,@UserNameEmail,GETDATE(),@FK_ModuleApproval_ID                
                   
				EXEC usp_EMailScheduler_Create @ApprovedEmail, @FK_ModuleApproval_ID, 0                
                 
				UPDATE MWorkflow_Email SET IsSendEmail = 1 WHERE FK_Module_ID = @pkmoduleid AND FK_Unik_ID = @pkunik                
			END                
		END                
		-- 20220714 End Update Email Template Approved  ---      
	SELECT CAST(1 AS BIT) FinalAccept
	END
 END
 IF @intworkflowstatus = 2 --reject
 BEGIN
	UPDATE MWorkFlow_Progress
	SET FK_MWorkflow_Status_ID = 5
	WHERE FK_Module_ID = @pkmoduleid
		  AND FK_Unik_ID = @pkunik

	-- 20220714 Update Email Template Rejected ---                
                   
		SELECT @IntLevelPrev = xx.Level, @UserTypePrev = xx.UserType  FROM                
		(                
			SELECT ROW_NUMBER() over (partition by FK_WorkFlow order by [Level] ASC) as rn, *                
			FROM MWorkFlowDetail WHERE FK_WorkFlow = @pkworkflowid                
		)xx                
		WHERE xx.rn = 1                
                   
		IF @UserTypePrev = 1                 
			BEGIN                
				SELECT @PKUserIDPrev = FK_MUserId                
				FROM MWorkFlow_History WHERE FK_Module_ID = @pkmoduleid AND FK_Unik_ID = @pkunik AND intLevel = @IntLevelPrev                
                     
				SELECT @RejectEmail = ISNULL(RejectEmailTemplateID,0)                
				FROM MWorkFlow WHERE PK_WorkFlow = @pkworkflowid                
                
				IF @RejectEmail > 0                
					BEGIN                
						SELECT @EmailAddress = UserEmailAddress, @UserNameEmail = UserName, @UserIDEmail = UserID                
						FROM Muser WHERE PK_MUser_ID = @PKUserIDPrev                
                  
						INSERT INTO MWorkflow_Email (FK_Module_ID,FK_Unik_ID,UserID,EmailAddress,IsSendEmail,FK_MWorkflow_ApprovalStatus_ID,UserName,Active,CreatedBy,CreatedDate, FK_ModuleApproval_ID)                
						SELECT @pkmoduleid, @pkunik, @UserIDEmail, @EmailAddress, 0, @intworkflowstatus, @UserNameEmail,1,@UserNameEmail,GETDATE(), @FK_ModuleApproval_ID                
                   
						EXEC usp_EMailScheduler_Create @RejectEmail, @FK_ModuleApproval_ID, 0                
                 
						UPDATE MWorkflow_Email SET IsSendEmail = 1 WHERE FK_Module_ID = @pkmoduleid AND FK_Unik_ID = @pkunik                
					END                
                     
			END                
                   
		--20220714  End Update Email Template Rejected  ---   

	SELECT CAST(0 AS BIT) FinalAccept
 END
 IF @intworkflowstatus= 3 --revise
 BEGIN
	DECLARE @intlevelrevise INT,
			@pkworkflowidrevise INT,
			@usertyperevise INT
	
	SELECT @intlevelrevise = mfp.RevisedActionLevelTo,
		   @pkworkflowidrevise = mfp.FK_MWorkflow_ID
	FROM MWorkFlow_Progress AS mfp
	WHERE mfp.FK_Module_ID = @pkmoduleid
		  AND mfp.FK_Unik_ID = @pkunik

	SELECT @usertyperevise = mfd.UserType
	FROM MWorkFlowDetail AS mfd
	WHERE mfd.FK_WorkFlow = @pkworkflowidrevise
		  AND mfd.[Level] = @intlevelrevise

	DELETE FROM MWorkFlow_Progress
	WHERE FK_Module_ID = @pkmoduleid
		  AND FK_Unik_ID = @pkunik 

	SELECT @previoususerid = mfh.FK_MUserId
	FROM MWorkFlow_History AS mfh
	WHERE mfh.FK_Module_ID = @pkmoduleid
		  AND mfh.FK_Unik_ID = @pkunik
		  AND mfh.intLevel = @intlevelrevise

	IF @usertyperevise=2
	BEGIN
		DECLARE @twoPreviousUserID INT
		SELECT @twoPreviousUserID = mfh.FK_MUserId
		FROM MWorkFlow_History AS mfh
		WHERE mfh.FK_Module_ID = @pkmoduleid
			  AND mfh.FK_Unik_ID = @pkunik
			  AND mfh.intLevel = @intlevelrevise - 2
		
		SET @pknextuserid = @previoususerid

		SELECT @usernamenext = m.UserName
		FROM MUser AS m
		WHERE m.PK_MUser_ID = @pknextuserid 
	END
	IF @usertyperevise = 4
		SET @pknextuserid = NULL
	IF @usertyperevise = 1
	BEGIN
		SET @pknextuserid = NULL
		SELECT TOP 1 @pknextuserid = mfh.FK_MUserId
		FROM MWorkFlow_History AS mfh
		WHERE mfh.FK_Module_ID = @pkmoduleid
			  AND mfh.FK_Unik_ID = @pkunik
			  AND mfh.intLevel = @intlevelrevise
		ORDER BY mfh.PK_MWorkflow_History_ID DESC
		SELECT @usernamenext = m.UserName
		FROM MUser AS m
		WHERE m.PK_MUser_ID = @pknextuserid
	END

	INSERT INTO MWorkFlow_Progress(
		-- PK_MWorkflow_Progress_ID -- this column value is auto-generated
		FK_Module_ID,
		FK_Unik_ID,
		FK_MWorkflow_ID,
		WorkflowLevel,
		FK_MWorkFlowUserType_ID,
		FK_MWorkflowRole_ID,
		FK_MWorkflowUser_ID,
		FK_MWorkflow_Status_ID,
		SLAType,
		SLAValue,
		BreachSLAAction,
		BreachSLAActionLevelTo,
		RevisedActionLevelTo
	)
	SELECT @pkmoduleid, @pkunik, mfd.FK_WorkFlow, mfd.[Level], mfd.UserType, mfd.RoleID,
		   @pknextuserid, 6, mfd.SLAType, mfd.SLAValue, mfd.BreachSLAAction,
		   mfd.BreachSLAActionTo, mfd.RevisedActionTo
	FROM MWorkFlowDetail AS mfd
	WHERE mfd.FK_WorkFlow = @pkworkflowidrevise
		  AND mfd.[Level] = @intlevelrevise

	INSERT INTO MWorkFlow_History(
		-- PK_MWorkflow_History_ID -- this column value is auto-generated
		FK_ModuleApproval_ID,
		FK_Module_ID,
		FK_Unik_ID,
		FK_MUserId,
		FK_MRoleId,
		intLevel,
		RoleName,
		UserName,
		UserNameExecute,
		CreatedDate,
		ResponseDate,
		FK_MWorkflow_ApprovalStatus_ID,
		Notes
	)
	SELECT @FK_ModuleApproval_ID, @pkmoduleid, @pkunik, @pknextuserid,
		   mfd.RoleID, mfd.[Level], m.RoleName, @usernamenext,
		   NULL, GETDATE(), NULL, NULL, NULL
	FROM MWorkFlowDetail AS mfd 
		 INNER JOIN MRole AS m
		 ON mfd.RoleID=m.PK_MRole_ID
	WHERE mfd.FK_WorkFlow = @pkworkflowid
		  AND mfd.[Level] = @intlevelrevise
	
	-- 20220714 Update Email Template Revise ---                
                   
		SELECT @IntLevelPrev = xx.Level, @UserTypePrev = xx.UserType  FROM                
		(                
			SELECT ROW_NUMBER() over (partition by FK_WorkFlow order by [Level] ASC) as rn, *                
			FROM MWorkFlowDetail WHERE FK_WorkFlow = @pkworkflowid                
		)xx                
		WHERE xx.rn = 1                
                   
		IF @UserTypePrev = 1                 
			BEGIN                
				SELECT @PKUserIDPrev = FK_MUserId                
				FROM MWorkFlow_History WHERE FK_Module_ID = @pkmoduleid AND FK_Unik_ID = @pkunik AND intLevel = @IntLevelPrev                
                     
				SELECT @RevEmail = ISNULL(RevEmailTemplateID,0)                
				FROM MWorkFlow WHERE PK_WorkFlow = @pkworkflowid                
                
				IF @RevEmail > 0                
					BEGIN                
						SELECT @EmailAddress = UserEmailAddress, @UserNameEmail = UserName, @UserIDEmail = UserID                
						FROM Muser WHERE PK_MUser_ID = @PKUserIDPrev                
                  
						INSERT INTO MWorkflow_Email (FK_Module_ID,FK_Unik_ID,UserID,EmailAddress,IsSendEmail,FK_MWorkflow_ApprovalStatus_ID,UserName,Active,CreatedBy,CreatedDate,FK_ModuleApproval_ID)                
						SELECT @pkmoduleid, @pkunik, @UserIDEmail, @EmailAddress, 0, @intworkflowstatus, @UserNameEmail,1,@UserNameEmail,GETDATE(),@FK_ModuleApproval_ID                
                   
						EXEC usp_EMailScheduler_Create @RevEmail, @FK_ModuleApproval_ID, 0                
                 
						UPDATE MWorkflow_Email SET IsSendEmail = 1 WHERE FK_Module_ID = @pkmoduleid AND FK_Unik_ID = @pkunik                
					END                
                     
			END                
                   
  -- 20220714 End Update Email Template Revise  ---  

	SELECT CAST(0 AS BIT) FinalAccept
 END
END

