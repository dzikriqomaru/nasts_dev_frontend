CREATE OR ALTER   PROCEDURE [dbo].[usp_IsFieldHaveAccessOnUpload] 
/* =============================================
-- Author:		Dandi Juliandi
-- Create date: 19 April 2024
-- Description:	Check Field Access on Upload
-- =============================================
*
SP ini Digunakan di:
1. usp_SaveUploadApproval
2. usp_SaveUploadData

* Changes
* Date		Modified By			Comments
************************************************************
* 
************************************************************/
	@FieldName VARCHAR(250) = '',
    @FK_Module_ID BIGINT = 0,
	@userid VARCHAR(250) ='',
	@bAdd  bit = 0 Output ,
	@bEdit bit = 0 Output
AS
BEGIN
	SELECT 
	@bAdd = ISNULL(bAdd,0) , 
	@bEdit = ISNULL(bEdit,0)  
	FROM MGroupMenuAccessField mga
	join muser mus on mga.FK_MGroupMenu_ID = mus.FK_MGroupMenu_ID and mus.UserID = @userid
	WHERE mga.FK_Module_ID = @FK_Module_ID 
	AND mga.ModuleField = @FieldName
END
