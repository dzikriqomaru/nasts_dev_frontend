CREATE OR ALTER PROCEDURE [dbo].[usp_ApproveUploadData]
/***********************************************************
* Procedure description:
* Date:   12/2/2015 
* Author: Hendra
*
* Changes
* Date		Modified By			Comments
************************************************************
** 18/12/2023 cek dulu sebelum penambahan kolom draft
* 19 Apr 2024 Davin				Field Access Implementation on Insert and Update
************************************************************/
(@pkapprovalid BIGINT,@userid VARCHAR(50))
AS
BEGIN


--DECLARE @pkapprovalid BIGINT,@userid VARCHAR(50)
--SET @pkapprovalid =36389
--SET @userid='supervisor1'	
--================================================================================================
-- Comment Coding ambil data moduleapproval by pk
--================================================================================================
	  DECLARE @sqlmajorversion INT  
    SELECT @sqlmajorversion= CONVERT(INT, SERVERPROPERTY('ProductMajorVersion'))


	DECLARE @isdebug BIT =0
	DECLARE @PK_ModuleApproval_ID bigint, @ModuleNameApproval varchar(max),
        @ModuleKey varchar(1000), @ModuleField varchar, @ModuleFieldBefore VARCHAR(MAX),
        @PK_ModuleAction_ID int, @CreatedDate datetime, @CreatedBy varchar(50)
	
	SELECT
		@PK_ModuleApproval_ID = ma.PK_ModuleApproval_ID, @ModuleNameApproval = ma.ModuleName,
		@ModuleKey = ma.ModuleKey, @ModuleField = ma.ModuleField,
		@ModuleFieldBefore = ma.ModuleFieldBefore,
		@PK_ModuleAction_ID = ma.PK_ModuleAction_ID, @CreatedDate = ma.CreatedDate,
		@CreatedBy = ma.CreatedBy
	FROM dbo.ModuleApproval ma
	WHERE ma.PK_ModuleApproval_ID=@pkapprovalid

	
	DECLARE @bIsCreateTableUploadForEachUser AS BIT = 0
	DECLARE @strTableUploadName AS VARCHAR(500) = ''
	SELECT @bIsCreateTableUploadForEachUser = sp.SettingValue
	FROM   SystemParameter AS sp
	WHERE  sp.PK_SystemParameter_ID = 53
	
	
	IF @bIsCreateTableUploadForEachUser = 1
	BEGIN
	    SET @strTableUploadName = @ModuleNameApproval + '_Upload_data_' + @CreatedBy + '_approval'
	END
	ELSE
	BEGIN
	    SET @strTableUploadName = @ModuleNameApproval + '_Upload_approval'
	END
	DECLARE @pkmoduleid INT,@modulelabelapproval VARCHAR(500)
	SELECT @pkmoduleid =PK_Module_ID,@modulelabelapproval=ModuleLabel FROM dbo.Module WHERE ModuleName= @ModuleNameApproval
	
--================================================================================================
-- Comment Coding :insert audittrailheadernya dan ambil pkaudittrailheadernya
--================================================================================================	
	DECLARE @pkaudittrailheader BIGINT=0
	INSERT dbo.AuditTrailHeader
	(
	    CreatedDate,
	    CreatedBy,
	    ApproveBy,
	    ModuleLabel,
	    FK_ModuleAction_ID,
	    FK_AuditTrailStatus_ID,
	    PK_ModuleApproval_ID
	)
	VALUES
	(   GETDATE(), -- CreatedDate - datetime
	    @CreatedBy,        -- CreatedBy - varchar(50)
	    @userid,        -- ApproveBy - varchar(50)
	    @modulelabelapproval,        -- ModuleLabel - varchar(100)
	    7,         -- FK_ModuleAction_ID - int
	    1,         -- FK_AuditTrailStatus_ID - int
	    @pkapprovalid          -- PK_ModuleApproval_ID - bigint
	    )

		SET @pkaudittrailheader=SCOPE_IDENTITY()

DECLARE @LimitRecordNotInsertAuditTrail BIGINT=0
  SELECT @LimitRecordNotInsertAuditTrail=sp.SettingValue
    FROM SystemParameter AS sp WHERE sp.PK_SystemParameter_ID=80

	DECLARE @jmlUploadRecord BIGINT
	DECLARE @tblcountupload TABLE(jml BIGINT)
	
	DECLARE @sqlcount VARCHAR(MAX)
	SET @sqlcount = 'SELECT  COUNT(1) FROM '+ @strTableUploadName +' WHERE FK_ModuleApproval_ID = ' + CAST(@pkapprovalid AS VARCHAR)
	INSERT INTO @tblcountupload (jml)
	EXEC(@sqlcount)
	
	 SELECT @jmlUploadRecord=jml FROM @tblcountupload 

--================================================================================================
-- Comment Coding audittrail beforenya
--================================================================================================


 DECLARE @modulename VARCHAR(500)  
 DECLARE @primarykeyfield VARCHAR(500)  
 SELECT @modulename=ModuleName FROM module WHERE PK_Module_ID=@pkmoduleid  
   
 SELECT @primarykeyfield=mf.FieldName FROM ModuleField AS mf WHERE mf.FK_Module_ID=@pkmoduleid AND mf.IsPrimaryKey=1  
   
   
	--DECLARE @sqlfirst VARCHAR(MAX)
	--SET @sqlfirst = ' ' + CHAR(10)
	--    + ' DROP TABLE IF EXISTS ' + @modulename + '_upload_' + @userid + '   ' + CHAR(10)
	--    + 'DECLARE @xmldata xml ' + CHAR(10)
	--    + 'SELECT @xmldata=dataxml FROM ( ' + CHAR(10)
	--    + 'SELECT CAST( ModuleField AS XML) dataxml FROM  moduleapproval  WHERE PK_ModuleApproval_ID= ' + CONVERT(VARCHAR(50), @PK_ModuleApproval_ID)
	--    + ' ' + CHAR(10)
	--    + ')xx  ' + CHAR(10)
	--    + 'SELECT  ' + CHAR(10)
	--    + '	  tbl.col.value(''nawa_Action[1]'',''varchar(50)'') AS nawa_Action, ' + CHAR(10)
	--    + '	  tbl.col.value(''' + @primarykeyfield + '[1]'',''varchar(8000)'') AS '+@primarykeyfield+' ' + CHAR(10)
	--    + '	INTO ' + @modulename + '_upload_' + @userid + '   ' + CHAR(10)
	--    + ' FROM @xmldata.nodes(''/DataUpload/' + @modulename + '_Upload'') tbl(col)'
	----EXEC (@sqlfirst)
	
   
   --================================================================================================
   -- Comment Coding table cobabigdata_upload akan terisi ketika sp usp_Getdataupload. jadi tinggal pake
   --================================================================================================

DECLARE @sqldropfirst AS VARCHAR(MAX)

IF @sqlmajorversion>=13
BEGIN
SET @sqldropfirst='Drop table if EXISTS '+@ModuleName+'_upload_'+@userid+'  '	
END
ELSE
	BEGIN

		set @sqldropfirst = ' IF OBJECT_ID('+@ModuleName+'_upload_'+@userid+', ''U'') IS NOT NULL  DROP TABLE '+@ModuleName+'_upload_'+@userid+' '

	END
EXEC (@sqldropfirst)

DECLARE @sqlfirst VARCHAR(max)

SET @sqlfirst = ' ' + char(10)
         + ' SELECT nawa_Action,'+ @primarykeyfield +'  ' + char(10)
         + ' INTO '+@ModuleName+'_upload_'+@userid+' ' + char(10)
         + ' FROM dbo.'+ @strTableUploadName+'  WHERE FK_ModuleApproval_ID = ' + CAST(@pkapprovalid AS VARCHAR)
EXEC(@sqlfirst)

   

 
 DECLARE @sqlbefore VARCHAR(MAX) = ''  
 IF @sqlmajorversion>=13
 BEGIN
 	SET @sqlbefore = 'DROP  TABLE IF EXISTS ' + @modulename + '_AuditTrail_' + @userid + CHAR(13)
 END
 ELSE
 	BEGIN

 		set @sqlbefore = ' IF OBJECT_ID('+ @modulename + '_AuditTrail_' + @userid+', ''U'') IS NOT NULL  DROP TABLE '+ @modulename + '_AuditTrail_' + @userid+' '+ CHAR(13)

 	END
	SET @sqlbefore = @sqlbefore + ' select ' + @modulename + '.* into ' + @modulename + '_AuditTrail_' + @userid + 
	    '  from ' + @modulename + CHAR(13)
	
	SET @sqlbefore = @sqlbefore + ' inner join ' + @modulename + '_upload_' + @userid + ' ON ' + @modulename + 
	    '_Upload_' + @userid + '.' + @primarykeyfield + ' = CONVERT(VARCHAR(8000), ' + @modulename + '.' + 
	    @primarykeyfield + ')' + CHAR(13)
	
	SET @sqlbefore = @sqlbefore + ' WHERE nawa_Action IN (''update'',''delete'')'  

	EXEC (@sqlbefore) 


--================================================================================================
-- Comment Coding :simpan data uudittrailbefore
--================================================================================================
	
 DECLARE @sqlcrossApply VARCHAR(MAX)=''    
  
 IF @sqlmajorversion>=13
 BEGIN
 	SET @sqlcrossApply = '	DROP TABLE IF EXISTS  AuditTrail_CrossApply_'+@userid + char(10)
         + '	SELECT * INTO AuditTrail_CrossApply_'+@userid+' FROM AuditTrail_CrossApply WHERE 1=2 ' + char(10)         
         + '	CREATE INDEX IdxAuditTrail_CrossApply_'+@userid+'  ON AuditTrail_CrossApply_'+@userid+'  ' + char(10)
         + '	(FK_Module_ID,UserID) INCLUDE (primarykeydata)'
         
            
 END
 ELSE
 BEGIN
 	
 		SET @sqlcrossApply = ' IF OBJECT_ID(AuditTrail_CrossApply_'+@userid+', ''U'') IS NOT NULL  DROP TABLE AuditTrail_CrossApply_'+@userid+' ' + char(10)
         + '	SELECT * INTO AuditTrail_CrossApply_'+@userid+' FROM AuditTrail_CrossApply WHERE 1=2 ' + char(10)         
         + '	CREATE INDEX IdxAuditTrail_CrossApply_'+@userid+'  ON AuditTrail_CrossApply_'+@userid+'  ' + char(10)
         + '	(FK_Module_ID,UserID) INCLUDE (primarykeydata)'
 	END
 exec(@sqlcrossApply)    
  DECLARE @sqlcreatecrossapply varchar(max)  
  SET @sqlcreatecrossapply ='insert into AuditTrail_CrossApply_'+@userid+'(primarykeydata,FieldName,OldValue,FK_Module_ID,UserID)'    
      + ' SELECT xx.'+@primarykeyfield +' AS Primarykeydata, ' + char(10)    
         + '        ca.* ,'+ CONVERT(VARCHAR(50), @pkmoduleid ) +','''+@userid+'''  ' + char(10)    
          
         --+ ' FROM   ( ' + char(10)    
         --+ '            SELECT rc.* ' + char(10)    
         --+ '            FROM   '+@modulename+' AS rc ' + char(10)    
         --+ '                   INNER JOIN '+@modulename+'_AuditTrail_'+@userid+' ' + char(10)    
         --+ ' ON  rc.'+@primarykeyfield+' = '+@modulename+'_AuditTrail_'+@userid+'.'+@primarykeyfield+' ' + char(10)    
         --+ '        )xx ' + char(10)  
         + ' FROM   '+@modulename+'_AuditTrail_'+@userid+' xx ' + char(10)      
         + '        CROSS APPLY( ' + char(10)    
         + '     VALUES'    


--================================================================================================
-- Comment Coding:generate statement insert ,insertdetail
--================================================================================================	


	    	DECLARE @fields VARCHAR(MAX)=''
	    	DECLARE @fieldinsert VARCHAR(MAX)=''
	    	DECLARE @fieldinsertdetail VARCHAR(MAX)=''
	
	
	
	
	SET @fields= '  myTempTable.XmlCol.query(''./nawa_userid'').value(''.'', ''varchar(8000)'') AS nawa_userid , ' + char(10)
         + '  myTempTable.XmlCol.query(''./nawa_recordnumber'').value(''.'', ''varchar(8000)'') AS nawa_recordnumber , ' + char(10)
         + '  myTempTable.XmlCol.query(''./KeteranganError'').value(''.'', ''varchar(8000)'') AS KeteranganError , ' + char(10)
         + '  myTempTable.XmlCol.query(''./nawa_Action'').value(''.'', ''varchar(8000)'') AS nawa_Action ,'+char(10)
		 + '  myTempTable.XmlCol.query(''./Active'').value(''.'', ''varchar(8000)'') AS Active ,'+char(10)
		 
set @fieldinsert='nawa_userid,'+CHAR(10) 		 
               +'nawa_recordnumber,'+CHAR(10)
               +'KeteranganError,'+CHAR(10)
               +'nawa_Action,'+CHAR(10)
          +'Active,'+CHAR(10)  	

--set @fieldinsertdetail='nawa_userid,'+CHAR(10)  		 
--               +'nawa_recordnumber,'+CHAR(10)
--               +'KeteranganError,'+CHAR(10)
--               +'nawa_Action,'+CHAR(10)
--               +'Active,'+CHAR(10)  	

               
               DECLARE @dateformat VARCHAR(50)
	select @dateformat=sp.SettingValue
	  FROM dbo.SystemParameter sp WHERE sp.PK_SystemParameter_ID =14
               

DECLARE @queryfieldinsertupdate VARCHAR(MAX)=''
	DECLARE @queryfield VARCHAR(MAX)=''
	DECLARE @PK_ModuleField_ID bigint, @FK_Module_ID int, @FieldName varchar(250),
        @FieldLabel varchar(250), @Sequence int, @Required bit,
        @IsPrimaryKey bit, @IsUnik bit, @IsShowInView bit, @FK_FieldType_ID int,
        @SizeField int, @FK_ExtType_ID int, @TabelReferenceName varchar(250),
        @TableReferenceFieldKey varchar(250),
        @TableReferenceFieldDisplayName varchar(250),
        @TableReferenceFilter varchar(550), @IsUseRegexValidation bit
	
	DECLARE my_cursor CURSOR FAST_FORWARD READ_ONLY FOR
	SELECT mf.PK_ModuleField_ID, mf.FK_Module_ID, mf.FieldName, mf.FieldLabel, mf.Sequence,
	       mf.Required, mf.IsPrimaryKey, mf.IsUnik, mf.IsShowInView, mf.FK_FieldType_ID,
	       mf.SizeField, mf.FK_ExtType_ID, mf.TabelReferenceName, mf.TableReferenceFieldKey,
	       mf.TableReferenceFieldDisplayName, mf.TableReferenceFilter,
	       mf.IsUseRegexValidation
	FROM dbo.ModuleField mf
	WHERE mf.FK_Module_ID=@pkmoduleid  and mf.FK_FieldType_ID<>14 
		  AND (mf.IsShowInForm = 1 OR mf.IsPrimaryKey = 1)
	ORDER BY mf.Sequence
	
	OPEN my_cursor
	
	FETCH FROM my_cursor INTO @PK_ModuleField_ID, @FK_Module_ID, @FieldName,
	                          @FieldLabel, @Sequence, @Required, @IsPrimaryKey,
	                          @IsUnik, @IsShowInView, @FK_FieldType_ID,
	                          @SizeField, @FK_ExtType_ID, @TabelReferenceName,
	                          @TableReferenceFieldKey,
	                          @TableReferenceFieldDisplayName,
	                          @TableReferenceFilter, @IsUseRegexValidation
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
		/*{ ... Cursor logic here ... }*/
	
	SET @fields=@fields +'myTempTable.XmlCol.query(''./'+@FieldName+''').value(''.'', ''varchar(8000)'') AS '+ @FieldName +' ,' +char(10)
	SET @fieldinsert=@fieldinsert+ @FieldName +','+CHAR(10)
	

	   /*{ ... Cursor logic here ... }*/    
   IF @FK_FieldType_ID=9    
   BEGIN    
     set @queryfieldinsertupdate=@queryfieldinsertupdate + ' ('''+@FieldLabel+''', '+@FieldName+'),'    
   END    
       
        
   IF  (@FK_FieldType_ID  IN  ( 1,2,3,4,5,6,7,8,12,13))    
    SET @queryfieldinsertupdate=@queryfieldinsertupdate + ' ('''+@FieldLabel+''', TRY_CONVERT(VARCHAR(8000), '+@FieldName+')),'    
  if (@FK_FieldType_ID =10)    
   SET @queryfieldinsertupdate=@queryfieldinsertupdate + ' ('''+@FieldLabel+''', TRY_CONVERT(VARCHAR(8000), '+@FieldName+')),'    

	 IF (@FK_FieldType_ID=11)   
	 BEGIN
	SET @queryfieldinsertupdate=@queryfieldinsertupdate + ' ('''+@FieldLabel+''', TRY_CONVERT(VARCHAR(8000), '+@FieldName+')),' 
	 -- SET @queryfieldinsertupdate=@queryfieldinsertupdate +' ('''+@FieldLabel +''', try_convert(varchar(8000), SUBSTRING('+ @FieldName +', PATINDEX(''%(%'', '+@FieldName+')+1, CASE WHEN PATINDEX(''%)%'','+@FieldName+')=0 THEN LEN('+@FieldName+') else PATINDEX(''%)%'','+@FieldName+')-2 END ))), ' + char(10)
    
     end
	

	--IF @FK_FieldType_ID<>10 
	--BEGIN
	--	SET @fieldinsertdetail=@fieldinsertdetail+ @FieldName +','+CHAR(10)
	--END
	--ELSE
	--	BEGIN
	--SET @fieldinsertdetail=@fieldinsertdetail+ 'FORMAT( CONVERT(DATETIME,' + @FieldName +') ,'''+ @dateformat +''') '+','+CHAR(10)		
	--	END
	
	
	  
		FETCH FROM my_cursor INTO @PK_ModuleField_ID, @FK_Module_ID, @FieldName,
		                          @FieldLabel, @Sequence, @Required,
		                          @IsPrimaryKey, @IsUnik, @IsShowInView,
		                          @FK_FieldType_ID, @SizeField, @FK_ExtType_ID,
		                          @TabelReferenceName, @TableReferenceFieldKey,
		                          @TableReferenceFieldDisplayName,
		                          @TableReferenceFilter, @IsUseRegexValidation
	END
	
	CLOSE my_cursor
	DEALLOCATE my_cursor
	

IF LEN(@fields)>0 SET @fields=SUBSTRING(@fields,1,LEN(@fields)-2)
IF LEN(@fieldinsert)>0 SET @fieldinsert=SUBSTRING(@fieldinsert,1,LEN(@fieldinsert)-2)
IF LEN(@fieldinsertdetail)>0 SET @fieldinsertdetail=SUBSTRING(@fieldinsertdetail,1,LEN(@fieldinsertdetail)-2) 	 	
 IF LEN(@queryfieldinsertupdate)>0 SET @queryfieldinsertupdate=SUBSTRING(@queryfieldinsertupdate,1,LEN(@queryfieldinsertupdate)-1)     
	
 SET @queryfield=@queryfieldinsertupdate+' ) AS CA(FieldName, OldValue)'     
 set @sqlcreatecrossapply=@sqlcreatecrossapply + @queryfield  

 IF @LimitRecordNotInsertAuditTrail<=0
BEGIN
exec (@sqlcreatecrossapply)  

END
ELSE
	BEGIN
		IF   @jmlUploadRecord <= @LimitRecordNotInsertAuditTrail
		BEGIN
				exec (@sqlcreatecrossapply)  
		END 
		
	END
 

 DECLARE @sqlaudituploadperuser varchar(max)
 
 
 IF @sqlmajorversion>=13
 BEGIN
 	SET @sqlaudituploadperuser = ' DROP TABLE IF EXISTS AuditTrailDetailUpload_'+@userid+' ' + char(10)
         + ' SELECT * INTO AuditTrailDetailUpload_'+@userid+' FROM AuditTrailDetailUpload WHERE 1=2 ' + char(10)
         + ' CREATE INDEX IdxAuditTrailDetailUpload_'+@userid+' ON AuditTrailDetailUpload_'+@userid+' ' + char(10)
         + ' (nawa_action,FK_Module_ID,UserID) INCLUDE (PrimarykeyData)'
 END
 ELSE
  begin
 SET @sqlaudituploadperuser = ' IF OBJECT_ID(audittraildetailupload_'+@userid+', ''U'') IS NOT NULL  DROP TABLE audittraildetailupload_'+@userid+'  ' + char(10)
         + ' SELECT * INTO AuditTrailDetailUpload_'+@userid+' FROM AuditTrailDetailUpload WHERE 1=2 ' + char(10)
         + ' CREATE INDEX IdxAuditTrailDetailUpload_'+@userid+' ON AuditTrailDetailUpload_'+@userid+' ' + char(10)
         + ' (nawa_action,FK_Module_ID,UserID) INCLUDE (PrimarykeyData)'
 
 	END 
 

EXEC(@sqlaudituploadperuser)

--================================================================================================
-- Comment Coding : getdata want to save and save audittraildetailupload
--================================================================================================

DECLARE @sqlupdatedataexisting varchar(max)=''
SET @sqlupdatedataexisting = 'INSERT into dbo.AuditTrailDetailUpload_'+@userid+' ' + char(10)
         + '( ' + char(10)
         + '    FK_AuditTrailHeader_ID, ' + char(10)
         + '    FieldName, ' + char(10)
         + '    OldValue, ' + char(10)
         + '    NewValue, ' + char(10)
         + '    PrimarykeyData, ' + char(10)
         + '    nawa_action, ' + char(10)
         + '    FK_Module_ID, ' + char(10)
         + '    UserID ' + char(10)
         + ') ' + char(10)
         + ' ' + char(10)
         + 'SELECT '+CONVERT(VARCHAR(50), @pkaudittrailheader )+', Fieldname,'''' AS OldValue, NewValue ,xx.'+ @primarykeyfield +' AS primarykeydata,nawa_Action,'+CONVERT(VARCHAR(50), @pkmoduleid )+','''+@userid+''' FROM ( ' + char(10)
         + 'SELECT * FROM '+ @strTableUploadName+' AS cbdu WHERE cbdu.FK_ModuleApproval_ID = ' + CAST(@pkapprovalid AS VARCHAR) + char(10)
         + ')xx ' + char(10)
         + 'CROSS APPLY( ' + char(10)
         + 'values'


		 
DECLARE @queryfieldaudittraildelete AS VARCHAR(MAX)=@queryfieldinsertupdate
		 
SET @queryfieldinsertupdate=@queryfieldinsertupdate+' ) AS CA(FieldName, NewValue) WHERE xx.nawa_Action=''update'' OR xx.nawa_Action=''insert'' OR xx.nawa_Action='''''
SET @queryfieldaudittraildelete  =@queryfieldaudittraildelete +' ) AS CA(FieldName, OldValue) WHERE xx.nawa_Action=''delete'' '
set @sqlupdatedataexisting= @sqlupdatedataexisting+@queryfieldinsertupdate  

 IF @LimitRecordNotInsertAuditTrail<=0
BEGIN
EXEC(@sqlupdatedataexisting) 

END
ELSE
	BEGIN
		IF   @jmlUploadRecord <= @LimitRecordNotInsertAuditTrail
		BEGIN
				EXEC(@sqlupdatedataexisting)
		END 
		
	END

--================================================================================================
-- Comment Coding:insert audittrail delete
--================================================================================================


DECLARE @sqlaudittraildetailuploaddelete varchar(max)=''
SET @sqlaudittraildetailuploaddelete = ' ' + char(10)
         + 'INSERT into dbo.AuditTrailDetailUpload_'+@userid+' ' + char(10)
         + '( ' + char(10)
         + '    FK_AuditTrailHeader_ID, ' + char(10)
         + '    FieldName, ' + char(10)
         + '    OldValue, ' + char(10)
         + '    NewValue, ' + char(10)
         + '    PrimarykeyData, ' + char(10)
         + '    nawa_action, ' + char(10)
         + '    FK_Module_ID, ' + char(10)
         + '    UserID ' + char(10)
         + ') ' + char(10)
         + ' ' + char(10)
         
         + 'SELECT '+CONVERT(VARCHAR(50), @pkaudittrailheader )+', Fieldname,OldValue, '''' NewValue ,xx.'+ @primarykeyfield +' AS primarykeydata,nawa_Action,'+CONVERT(VARCHAR(50), @pkmoduleid )+','''+@userid+''' FROM ( ' + char(10)         
          + 'SELECT * FROM '+ @strTableUploadName +' AS cbdu WHERE cbdu.FK_ModuleApproval_ID = ' + CAST(@pkapprovalid AS VARCHAR) + char(10)
         + ')xx ' + char(10)
         + 'CROSS APPLY( ' + char(10)
         + 'values	'


	SET @sqlaudittraildetailuploaddelete	=@sqlaudittraildetailuploaddelete+@queryfieldaudittraildelete
	
 IF @LimitRecordNotInsertAuditTrail<=0
BEGIN
EXEC(@sqlaudittraildetailuploaddelete)

END
ELSE
	BEGIN
		IF   @jmlUploadRecord <= @LimitRecordNotInsertAuditTrail
		BEGIN
				EXEC(@sqlaudittraildetailuploaddelete)
		END 
		
	END

--================================================================================================
-- Comment Coding:insert audittraildetail untuk mode update dan delete
--================================================================================================



DECLARE @sqlauditdetailupdatedelete varchar(max)
SET @sqlauditdetailupdatedelete = 'INSERT INTO AuditTrailDetail     ' + char(10)
         + ' (       ' + char(10)
         + '  FK_AuditTrailHeader_ID,     ' + char(10)
         + '  FieldName,     ' + char(10)
         + '  OldValue,     ' + char(10)
         + '  NewValue     ' + char(10)
         + ' )SELECT      ' + char(10)
         + ' newtable.FK_AuditTrailHeader_ID,     ' + char(10)
         + '  newtable.FieldName,     ' + char(10)
         + '  oldtable.OldValue,     ' + char(10)
      + ' newtable.NewValue     ' + char(10)
         + '  FROM AuditTrailDetailUpload_'+@userid+' newtable     ' + char(10)
         + '  LEFT JOIN AuditTrail_CrossApply_'+@userid+' oldtable     ' + char(10)
         + '  ON newtable.primarykeydata=     ' + char(10)
         + '  oldtable.primarykeydata  AND newtable.Fieldname =oldtable.FieldName  AND  oldtable.FK_Module_ID=newtable.FK_Module_ID AND oldtable.userid=newtable.UserID   ' + char(10)
         + '  WHERE (nawa_Action=''update'' OR nawa_Action=''Insert'')     ' + char(10)
         + '     AND newtable.FK_Module_ID='+ CONVERT(VARCHAR(50), @pkmoduleid)+' AND newtable.userid='''+@userid+'''   '
    
      
  
 IF @LimitRecordNotInsertAuditTrail<=0
BEGIN
EXEC(@sqlauditdetailupdatedelete)

END
ELSE
	BEGIN
		IF   @jmlUploadRecord <= @LimitRecordNotInsertAuditTrail
		BEGIN
				EXEC(@sqlauditdetailupdatedelete)
		END 
		
	END
 --================================================================================================
 -- Comment Coding insert audittraildetail untuk mode delete
 --================================================================================================     
 
 DECLARE @sqlaudittraildelete varchar(max)
SET @sqlaudittraildelete = ' INSERT INTO AuditTrailDetail     ' + char(10)
         + ' (       ' + char(10)
         + '  FK_AuditTrailHeader_ID,     ' + char(10)
         + '  FieldName,     ' + char(10)
         + '  OldValue,     ' + char(10)
         + '  NewValue     ' + char(10)
         + ' )     ' + char(10)
         + '  select     ' + char(10)
         + '   newtable.FK_AuditTrailHeader_ID,     ' + char(10)
         + '  newtable.FieldName,     ' + char(10)
         + '  newtable.OldValue,     ' + char(10)
         + '  newtable.NewValue     ' + char(10)
         + '  FROM AuditTrailDetailUpload_'+@userid+' newtable     ' + char(10)
         + '  LEFT JOIN AuditTrail_CrossApply_'+@userid+' oldtable     ' + char(10)
         + '  ON newtable.primarykeydata=     ' + char(10)
         + '  oldtable.primarykeydata  AND newtable.Fieldname =oldtable.FieldName  AND newtable.FK_Module_ID=oldtable.FK_Module_ID  ' + char(10)
         + '  AND oldtable.UserID = newtable.UserID ' + char(10)
         + '  WHERE nawa_Action=''delete''      ' + char(10)
         + '   AND oldtable.FK_Module_ID='+CONVERT(VARCHAR(50),@pkmoduleid)+' AND oldtable.userid='''+@userid+'''  '




 IF @LimitRecordNotInsertAuditTrail<=0
BEGIN
EXEC(@sqlaudittraildelete)

END
ELSE
	BEGIN
		IF   @jmlUploadRecord <= @LimitRecordNotInsertAuditTrail
		BEGIN
				EXEC(@sqlaudittraildelete)
		END 
		
	END



  

IF @sqlmajorversion>=13
BEGIN
EXEC('DROP  TABLE IF EXISTS '+@modulename+'_AuditTrail_'+@userid)  
EXEC('DROP TABLE IF EXISTS AuditTrail_CrossApply_'+@userid+'')
EXEC('DROP TABLE IF exists audittraildetailupload_'+@userid+'')
END
ELSE
	BEGIN
			EXEC( ' IF OBJECT_ID('+@modulename+'_AuditTrail_'+@userid+', ''U'') IS NOT NULL  DROP TABLE '+@modulename+'_AuditTrail_'+@userid+' ')
			EXEC( ' IF OBJECT_ID(AuditTrail_CrossApply_'+@userid+', ''U'') IS NOT NULL  DROP TABLE AuditTrail_CrossApply_'+@userid+' ')
			EXEC( ' IF OBJECT_ID(audittraildetailupload_'+@userid+', ''U'') IS NOT NULL  DROP TABLE audittraildetailupload_'+@userid+' ')
			

	END





	--WHERE  m.PK_Module_ID = @pkmoduleid
	
	
	DECLARE @sql VARCHAR(MAX)=''
	IF @ModuleKey = 1
	BEGIN
	    SET @sql = 'DELETE FROM ' + @ModuleName
	    exec  (@sql)
	END
	
	
	
	
	DECLARE 
	       
	        @fieldDefault               VARCHAR(MAX),
	        @fieldactive                VARCHAR(100),
	        @fieldvaluedefault          VARCHAR(MAX),@FieldUpdate VARCHAR(MAX),
	        @fieldupdatedefault VARCHAR(MAX),
	        @filedupdateactive VARCHAR(100),@fieldprimarykey VARCHAR(500),@fieldInsertValue VARCHAR(MAX)
	        
	        
	SET @fieldInsertValue=''	        
	set @filedupdateactive='' 
	SET @fields = ''
	SET @fieldDefault = ''
	SET @fieldactive = ''
	SET @fieldvaluedefault = ''  
	set @FieldUpdate=''
	set @fieldupdatedefault =''
	set @fieldprimarykey=''
	DECLARE my_cursor CURSOR FAST_FORWARD READ_ONLY 
	FOR
	    SELECT mf.FK_Module_ID,
	           mf.FieldName,
	           mf.FieldLabel,
	           mf.Sequence,
	           mf.Required,
	           mf.IsPrimaryKey,
	           mf.IsUnik,
	           mf.IsShowInView,
	           mf.FK_FieldType_ID,
	           mf.SizeField,
	           mf.FK_ExtType_ID,
	           mf.TabelReferenceName,
	           mf.TableReferenceFieldKey,
	           mf.TableReferenceFieldDisplayName,
	           mf.TableReferenceFilter,
	           mf.IsUseRegexValidation
	    FROM   dbo.ModuleField mf
	    WHERE  mf.FK_Module_ID = @pkmoduleid AND mf.FK_FieldType_ID<>14
			   AND (mf.IsShowInForm = 1 OR mf.IsPrimaryKey = 1)

	OPEN my_cursor
	
	FETCH FROM my_cursor INTO @FK_Module_ID, @FieldName,
	@FieldLabel, @Sequence, @Required, @IsPrimaryKey,
	@IsUnik, @IsShowInView, @FK_FieldType_ID, @SizeField,
	@FK_ExtType_ID, @TabelReferenceName,
	@TableReferenceFieldKey,
	@TableReferenceFieldDisplayName,
	@TableReferenceFilter, @IsUseRegexValidation
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
	/*{ ... Cursor logic here ... }*/    
			IF @FK_ExtType_ID=11 CONTINUE    
               
			IF @IsPrimaryKey=1 SET @fieldprimarykey = @FieldName    
			
			/*Cek Akses Field*/
			DECLARE @bAdd bit, @bEdit bit
			EXEC usp_IsFieldHaveAccessOnUpload @FieldName,@FK_Module_ID,@CreatedBy,@bAdd = @bAdd OUTPUT,@bEdit = @bEdit  OUTPUT

			/*Query Builder For Insert*/
			--Set Field to Insert
			IF @bAdd = 1
			BEGIN
				IF @FK_FieldType_ID <> 12 AND @FK_FieldType_ID <> 14 AND @FK_FieldType_ID <> 15 --Exclude Identity, Varbinary, Big Indentity
				BEGIN    
					SET @fields = @fields + @FieldName + ',' + CHAR(10) + CHAR(13)    
				END    
            
				--Set Value to Insert
				IF @FK_FieldType_ID <> 10 AND @FK_FieldType_ID <> 11 AND @FK_FieldType_ID <> 12 AND @FK_FieldType_ID <> 14 AND @FK_FieldType_ID <> 15 --Exclude DateTime, Reference Table, Identity, Varbinary, Big Identity
				BEGIN    
					SET @fieldInsertValue= @fieldInsertValue + @FieldName + ',' + CHAR(10) + CHAR(13)    
				END 
				ELSE IF @FK_FieldType_ID = 10 --DATETIME
				BEGIN
					IF @Required=1    
					BEGIN
						SET @fieldInsertValue= @fieldInsertValue + @FieldName + ',' + CHAR(10) + CHAR(13)    
					END     
					ELSE
					BEGIN    
						SET @fieldInsertValue= @fieldInsertValue + 'CASE WHEN  '+ @FieldName +'='''' or '+  @FieldName  +' =''NULL'' then null else '+ @FieldName +' end as '+ @FieldName  +' ,' + CHAR(10) + CHAR(13)                              
					END
				END
				ELSE IF @FK_FieldType_ID = 11 --Reference Table
				BEGIN
					IF @FK_ExtType_ID = 15
					BEGIN
						--Tipe Multiple
						SET @fieldInsertValue = @fieldInsertValue + @FieldName + ',' + CHAR(10) + CHAR(13)    
					END
					ELSE
					BEGIN
						--Tipe Combo
						SET @fieldInsertValue = @fieldInsertValue + 'SUBSTRING( ' + @strTableUploadName +'.' + @FieldName +
							', CHARINDEX(''('', ' + @strTableUploadName +'.' + @FieldName +
							',1)+1,  case when CHARINDEX('')'', ' + @strTableUploadName +'.' + @FieldName +
							',1)-2 >0 then CHARINDEX('')'', ' + @strTableUploadName +'.' + @FieldName + ',1)-2 ELSE 0 END ),' +
							CHAR(10) + CHAR(13)
					END
				END
			END

			
			/*Query Builder For Update*/
			IF @bEdit = 1
			BEGIN
				IF @FK_FieldType_ID <> 10 AND @FK_FieldType_ID <> 11 AND @FK_FieldType_ID <> 12 AND @FK_FieldType_ID <> 14 AND @FK_FieldType_ID <> 15 --Exclude DateTime, Reference Table, Identity, Varbinary, Big Identity
				BEGIN    
					SET @FieldUpdate = @FieldUpdate + @FieldName + '=' + @strTableUploadName +'.' + @FieldName + ',' + CHAR(10)+ CHAR(13)
				END
				ELSE IF @FK_FieldType_ID=10         
				BEGIN    
					SET @FieldUpdate = @FieldUpdate + @FieldName + '=dbo.ufn_GetDateValue(' + @strTableUploadName +'.' +
						@FieldName + ',''' + @dateformat + '''),' + CHAR(10) + CHAR(13) 
				END    
				ELSE IF @FK_FieldType_ID = 11    
				BEGIN
					IF @FK_ExtType_ID = 15
					BEGIN
						--Tipe Multiple
						SET @FieldUpdate = @FieldUpdate + @FieldName + '=' + @strTableUploadName +'.' + @FieldName + ',' + CHAR(10)
							+ CHAR(13)
					END
					ELSE
					BEGIN
						--Tipe Combo
						SET @FieldUpdate = @FieldUpdate + @FieldName + '=SUBSTRING( ' + @strTableUploadName +'.' + @FieldName +
							', CHARINDEX(''('', ' + @strTableUploadName +'.' + @FieldName +
							',1)+1,  case when CHARINDEX('')'', ' + @strTableUploadName +'.' + @FieldName +
							',1)-2 >0 then CHARINDEX('')'', ' + @strTableUploadName +'.' + @FieldName + ',1)-2 ELSE 0 END ),' +
							CHAR(10) + CHAR(13)
					END
				END
			END
		
		FETCH FROM my_cursor INTO @FK_Module_ID, @FieldName,
			@FieldLabel, @Sequence, @Required, @IsPrimaryKey,
			@IsUnik, @IsShowInView, @FK_FieldType_ID,
			@SizeField, @FK_ExtType_ID, @TabelReferenceName,
			@TableReferenceFieldKey,
			@TableReferenceFieldDisplayName,
			@TableReferenceFilter, @IsUseRegexValidation
		END
	
	CLOSE my_cursor
	DEALLOCATE my_cursor
	
	
	
	IF LEN(@fields) > 0 SET @fields = SUBSTRING(@fields, 1, LEN(@fields) -3)
	IF LEN(@fieldInsertValue) > 0 SET @fieldInsertValue= SUBSTRING(@fieldInsertValue, 1, LEN(@fieldInsertValue) -3)

	 --cek dulu ada Field Draft atau tidak
		declare @sqlQuery nvarchar(200)
		DECLARE @rcount NUMERIC(18,0)   
		set @sqlQuery = 'SELECT COUNT (COLUMN_NAME) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = ''' + @ModuleName +''' and COLUMN_NAME =''Draft''' 
		EXECUTE @rcount=sp_executesql @sqlQuery
	
	
	DECLARE @PK_ModuleField_ID_Default   BIGINT,
	  @FieldName_Default             VARCHAR(250),
	        @FieldLabel_Default            VARCHAR(250),
	        @Sequence_Default              INT,
	        @Required_Default              BIT,
	        @IsPrimaryKey_Default          BIT,
	        @IsUnik_Default               BIT,
	        @FK_FieldType_ID_Default       INT,
	        @SizeField_Default             INT,
	        @FK_ExtType_ID_Default         INT
	
	DECLARE my_cursor CURSOR FAST_FORWARD READ_ONLY 
	FOR
	    SELECT PK_ModuleField_ID,
	           FieldName,
	           FieldLabel,
	           Sequence,
	           [Required],
	           IsPrimaryKey,
	           IsUnik,
	           FK_FieldType_ID,
	           SizeField,
	           FK_ExtType_ID
	    FROM   dbo.ModuleFieldDefault 
		 WHERE FieldName <> iif(@rcount > 0 ,'', 'Draft')
	
	OPEN my_cursor
	
	FETCH FROM my_cursor INTO @PK_ModuleField_ID_Default, @FieldName_Default, @FieldLabel_Default,
	@Sequence_Default, @Required_Default, @IsPrimaryKey_Default, @IsUnik_Default,
	@FK_FieldType_ID_Default, @SizeField_Default, @FK_ExtType_ID_Default
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
	    /*{ ... Cursor logic here ... }*/
	    
	    SET @fieldDefault = @fieldDefault + @FieldName_Default + ',' + CHAR(10) +CHAR(13)

	    IF @FK_FieldType_ID_Default = 13 and @FieldName_Default = 'Active'
	    BEGIN
			SET @fieldactive = @FieldName_Default + ',' + CHAR(10) + CHAR(13)	    
			set @filedupdateactive=@filedupdateactive +@FieldName_Default +'='+ @strTableUploadName+'.' +@FieldName_Default +','+ CHAR(10) + CHAR(13)
	    	
	    END
	     
	    --cek tipenya varchar(percaya kalau cuma isinya createdby,lastupdateby,approveby)
	    --IF @FK_FieldType_ID_Default = 9
	    --BEGIN
	    --	SET @fieldvaluedefault = @fieldvaluedefault + '''' + @CreatedBy + ''','+ CHAR(10) + CHAR(13)


	    --END
	    
	     IF @PK_ModuleField_ID_Default= 2
	    begin
			
			SET @fieldvaluedefault = @fieldvaluedefault + '''' + @CreatedBy + ''','+ CHAR(10) + CHAR(13)
		END 
	    
	    IF @PK_ModuleField_ID_Default= 3 
	    begin
			SET @fieldupdatedefault = @fieldupdatedefault +@FieldName_Default+ '=''' + @CreatedBy + ''','+ CHAR(10) + CHAR(13)
			SET @fieldvaluedefault = @fieldvaluedefault + '''' + @CreatedBy + ''','+ CHAR(10) + CHAR(13)
		END 
	    IF  @PK_ModuleField_ID_Default=4
	    BEGIN
	    	SET @fieldupdatedefault = @fieldupdatedefault +@FieldName_Default+ '=''' + @userid + ''','+ CHAR(10) + CHAR(13)
	    	SET @fieldvaluedefault = @fieldvaluedefault + '''' + @userid + ''','+ CHAR(10) + CHAR(13)
	    END
			
    IF  @PK_ModuleField_ID_Default=8
	    BEGIN
	    	SET @fieldupdatedefault = @fieldupdatedefault +@FieldName_Default+ '='''','+ CHAR(10) + CHAR(13)
	    	
	    	
	    	SET @fieldvaluedefault = @fieldvaluedefault + ''''','+ CHAR(10) + CHAR(13)
	    END
	    --cek tipenya date(percaya kalau isinya createdate,lastupdatedate,approveate)
	    IF @FK_FieldType_ID_Default = 10
	        SET @fieldvaluedefault = @fieldvaluedefault + '''' + CONVERT(VARCHAR(20), GETDATE(), 120)+ ''',' + CHAR(10) + CHAR(13)
	    
	     IF @PK_ModuleField_ID_Default= 6 or @PK_ModuleField_ID_Default=7
			SET @fieldupdatedefault = @fieldupdatedefault +@FieldName_Default+ '=''' +  CONVERT(VARCHAR(20), GETDATE(), 120) + ''','+ CHAR(10) + CHAR(13)
		
		IF @FK_FieldType_ID_Default = 13 and @FieldName_Default = 'Draft'
	    BEGIN
	    	SET @fieldupdatedefault = @fieldupdatedefault +@FieldName_Default+ '=1 ,'+ CHAR(10) + CHAR(13)
	    	SET @fieldvaluedefault = @fieldvaluedefault + '1' + ','+ CHAR(10) + CHAR(13)
	    	
	    END
	    
	    FETCH FROM my_cursor INTO @PK_ModuleField_ID_Default, @FieldName_Default, 
	    @FieldLabel_Default,
	    @Sequence_Default, @Required_Default, @IsPrimaryKey_Default, @IsUnik_Default,
	    @FK_FieldType_ID_Default, @SizeField_Default, @FK_ExtType_ID_Default
	END
	
	CLOSE my_cursor
	DEALLOCATE my_cursor
	
	
	IF LEN(@fieldDefault) > 0
	    SET @fieldDefault = SUBSTRING(@fieldDefault, 1, LEN(@fieldDefault) -3)
	
	IF LEN(@fieldvaluedefault) > 0
	    SET @fieldvaluedefault = SUBSTRING(@fieldvaluedefault, 1, LEN(@fieldvaluedefault) -3)
	

	
	
	--insert mode
	
	DECLARE @strinsert VARCHAR(MAX) = ''
	
	SET @strinsert = 'INSERT INTO ' + @ModuleName + '(' + CHAR(10) + CHAR(13)	
	SET @strinsert += @fields 
	IF LEN(@fieldDefault) > 0
	BEGIN
	    SET @strinsert += ',' + CHAR(10) + CHAR(13)
	    SET @strinsert += @fieldDefault + CHAR(10) + CHAR(13)
	END
	
	SET @strinsert += ')' + CHAR(10) + CHAR(13)
	SET @strinsert += ' SELECT ' + CHAR(10) + CHAR(13)
	SET @strinsert += @fieldInsertValue 
	IF LEN(@fieldactive) > 0
	BEGIN
	    SET @strinsert += ',' + CHAR(10) + CHAR(13)
	    SET @strinsert += @fieldactive + CHAR(10) + CHAR(13)
	END
	
	SET @strinsert += @fieldvaluedefault + CHAR(10) + CHAR(13)
	
	
	SET @strinsert += ' FROM ' + @strTableUploadName+'  WHERE FK_ModuleApproval_ID = ' + CAST(@pkapprovalid AS VARCHAR)
	
	SET @strinsert += ' AND nawa_Action=''INSERT'' AND KeteranganError='''' order by nawa_recordnumber'
	
	exec ( @strinsert)
	
	
	--upate  mode
	
	
		
	
	DECLARE @strupdate VARCHAR(MAX) = ''
		IF LEN(@fieldupdatedefault) > 0
	    SET @fieldupdatedefault = SUBSTRING(@fieldupdatedefault, 1, LEN(@fieldupdatedefault) -3)
	
	
	
SET @strupdate = 'UPDATE '+ @ModuleName +' SET ' + char(10)
		 + @FieldUpdate
		 + @filedupdateactive
       +@fieldupdatedefault
		 +' from '+@ModuleName
         + '	INNER JOIN '+@strTableUploadName+' ON '+@ModuleName+'.'+@fieldprimarykey +'='+@strTableUploadName+'.'+@fieldprimarykey +'' + char(10)
         + '	WHERE FK_ModuleApproval_ID = ' + CAST(@pkapprovalid AS VARCHAR) + ' AND '+@strTableUploadName+'.nawa_Action=''Update'' ' + char(10)
         + '	 '
                
EXEC(@strupdate)




		--delete mode
		
		DECLARE @sqlDelete varchar(max)
SET @sqlDelete = '	DELETE '+@ModuleName+'		 ' + char(10)
         + '		FROM '+@ModuleName+' ' + char(10)
         + '		INNER JOIN '+@strTableUploadName+' ON '+@ModuleName+'.'+@fieldprimarykey+'='+@strTableUploadName+'.'+@fieldprimarykey +'		  ' + char(10)
         + '		WHERE '+@strTableUploadName+'.nawa_Action=''Delete'' ' + char(10)
         + '		AND FK_ModuleApproval_ID = ' + CAST(@pkapprovalid AS VARCHAR)
EXEC(@sqlDelete )



--DECLARE @sqlclear varchar(4000)
--SET @sqlclear = 'DELETE FROM '+@strTableUploadName+' WHERE nawa_userid='''+@CreatedBy+''' AND FK_ModuleApproval_ID = ' + CAST(@pkapprovalid AS VARCHAR)
----SELECT (@sqlclear)
--EXEC(@sqlclear)

--DECLARE @sqlclear1 VARCHAR(MAX)


--IF @sqlmajorversion>=13
--BEGIN
--SET @sqlclear1='DROP TABLE IF EXISTS '+ @modulename +'_ApprovalDetail_'+ @userid +''	
--END
--ELSE
--	BEGIN

--		set @sqlclear1 = ' IF OBJECT_ID('+@modulename +'_ApprovalDetail_'+ @userid +', ''U'') IS NOT NULL  DROP TABLE '+@modulename +'_ApprovalDetail_'+ @userid +' '


--	END


--EXEC (@sqlclear1)		



--DECLARE @sqlclear2 VARCHAR(MAX)

--IF @sqlmajorversion>=13
--BEGIN
--SET @sqlclear2='DROP TABLE IF EXISTS '+ @modulename +'_upload_'+ @userid +''	
--END
--ELSE
--	BEGIN

--		set @sqlclear2 = ' IF OBJECT_ID('+@modulename +'_upload_'+ @userid +', ''U'') IS NOT NULL  DROP TABLE '+@modulename +'_upload_'+ @userid +' '

--	END

--EXEC (@sqlclear2)		
				
END
