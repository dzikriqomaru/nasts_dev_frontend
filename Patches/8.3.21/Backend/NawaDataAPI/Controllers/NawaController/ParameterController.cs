﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Data.SqlClient;
using System.Security.Claims;
using System.IO;
using System.Text.Json;
using NawaDataBLL;
using NawaDataDAL;
using NawaDataDAL.Models;

namespace NawaDataAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParameterController : ControllerBase
    {
        [Route("ExportTemplateData")]
        [HttpGet]
        [Authorize(Policy="ApplicationLock")]
        public IActionResult ExportModuleData(int ModuleID)
        {
            ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
            try
            {
                int groupMenuId = Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value);
                if (CommonBLL.CheckMenuAccess(groupMenuId, ModuleID, EActionType.Import))
                {
                    MemoryStream ms = ModuleBLL.ExportTemplateData(ModuleID, identity.FindFirst("UserID").Value, Convert.ToInt32(identity.FindFirst("PK_MUser_ID").Value), Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), groupMenuId);
                    ms.Position = 0;

                    return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                }
                else
                {
                    return StatusCode(500, "You don't have authorization to access this menu.");
                }
            }
            catch (Exception ex)
            {
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ExportModuleDataWithoutData")]
        [HttpGet]
        [Authorize(Policy = "ApplicationLock")]
        public IActionResult ExportModuleDataWithoutData(int ModuleID)
        {
            ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
            try
            {
                int groupMenuId = Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value);
                if (CommonBLL.CheckMenuAccess(groupMenuId, ModuleID, EActionType.Import))
                {
                    MemoryStream ms = ModuleBLL.ExportModuleDataWithoutData(ModuleID, identity.FindFirst("UserID").Value, Convert.ToInt32(identity.FindFirst("PK_MUser_ID").Value), Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), groupMenuId);
                    ms.Position = 0;

                    return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                }
                else
                {
                    return StatusCode(500, "You don't have authorization to access this menu.");
                }
            }
            catch (Exception ex)
            {
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("CheckExistModuleByName")]
        [HttpPost]
        [Authorize(Policy = "ApplicationLock")]
        public ActionResult CheckExistModuleByName([FromBody] RequestBodyListString body)
        {
            ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
            try
            {
                string modNameExist = "";
                return ModuleBLL.CheckExistModuleByName(body.listModuleName,out modNameExist) ? StatusCode(500, modNameExist + " already exists") : Ok("Module is not exist");
            }
            catch (Exception ex)
            {
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("SaveParameterUpload")]
        [Authorize(Policy="ApplicationLock")]
        [HttpPost]
        public ActionResult SaveParameterUpload([FromForm] CParameterUpload parameterUpload)
        {
            ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
            string userID = identity.FindFirst("UserID").Value;
            int groupMenuID = Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value);
            try
            {
                if (CommonBLL.CheckMenuAccess(groupMenuID, parameterUpload.ModuleID, EActionType.Import))
                {
                    ModuleBLL.SaveParameterUpload(parameterUpload.ModuleID, parameterUpload.body.OpenReadStream(), identity.FindFirst("UserID").Value, parameterUpload.correctionID, groupMenuID);

                    return Ok("OK");
                }
                else
                {
                    return StatusCode(500, "You don't have authorization to access this menu.");
                }
            }
            catch (Exception ex)
            {
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("CommitModuleUpload")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult CommitModuleUpload(int ModuleID, int correctionID, string reviewNotes)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;

                if (CommonBLL.CheckMenuAccess(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), ModuleID, EActionType.Import))
                {
                    String msg = "";
                    ModuleBLL.CommitParameterUpload(ModuleID, identity.FindFirst("UserID").Value, Convert.ToInt32(identity.FindFirst("PK_MUser_ID").Value), Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), correctionID, reviewNotes, out msg);

                    return Ok(msg);
                }
                else
                {
                    return StatusCode(500, "You don't have authorization to access this menu.");
                }
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex,userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetValidModuleDetail")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetValidModuleDetail(int ModuleID)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;

                if (CommonBLL.CheckMenuAccess(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), ModuleID, EActionType.Import))
                {
                    ModuleView moduleView = ModuleBLL.GetValidModuleDetail(ModuleID);

                    return Ok(moduleView);
                }
                else
                {
                    return StatusCode(500, "You don't have authorization to access this menu.");
                }
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex,userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetValidModuleData")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetValidModuleData(int ModuleID, int page, string orderBy, string order, string search, string filter)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;

                if (CommonBLL.CheckMenuAccess(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), ModuleID, EActionType.Import))
                {
                    int rowCount = 0;
                    List<NawaDataDAL.DynamicDictionary> moduleData = ModuleBLL.GetValidModuleData(
                        ModuleID, 
                        identity.FindFirst("UserID").Value, 
                        page, 
                        orderBy, 
                        order, 
                        search, 
                        filter, 
                        out rowCount, 
                        null
                    );

                    return Ok(new { count = rowCount, rows = moduleData });
                }
                else
                {
                    return StatusCode(500, "You don't have authorization to access this menu.");
                }
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex,userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetValidModuleData")]
        [Authorize(Policy="ApplicationLock")]
        [HttpPost]
        public ActionResult GetValidModuleData([FromBody] GetModuleDataPostBody body)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;

                if (CommonBLL.CheckMenuAccess(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), body.ModuleID, EActionType.Import))
                {
                    int rowCount = 0;
                    List<NawaDataDAL.DynamicDictionary> moduleData = ModuleBLL.GetValidModuleData(
                        body.ModuleID, 
                        identity.FindFirst("UserID").Value, 
                        body.page, body.orderBy, 
                        body.order,
                        body.search, 
                        "", 
                        out rowCount, 
                        body.jsonFilter
                    );

                    return Ok(new { count = rowCount, rows = moduleData });
                }
                else
                {
                    return StatusCode(500, "You don't have authorization to access this menu.");
                }
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetInvalidModuleDetail")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetInvalidModuleDetail(int ModuleID)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;

                if (CommonBLL.CheckMenuAccess(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), ModuleID, EActionType.Import))
                {
                    ModuleView moduleView = ModuleBLL.GetInvalidModuleDetail(ModuleID);

                    return Ok(moduleView);
                }
                else
                {
                    return StatusCode(500, "You don't have authorization to access this menu.");
                }
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex,userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetInvalidModuleData")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetInvalidModuleData(int ModuleID, int page, string orderBy, string order, string search, string filter)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;

                if (CommonBLL.CheckMenuAccess(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), ModuleID, EActionType.Import))
                {
                    int rowCount = 0;
                    List<NawaDataDAL.DynamicDictionary> moduleData = ModuleBLL.GetInvalidModuleData(
                        ModuleID, 
                        identity.FindFirst("UserID").Value, 
                        page, 
                        orderBy, 
                        order, 
                        search, 
                        filter, 
                        out rowCount,
                        null
                    );

                    return Ok(new { count = rowCount, rows = moduleData });
                }
                else
                {
                    return StatusCode(500, "You don't have authorization to access this menu.");
                }
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex,userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetInvalidModuleData")]
        [Authorize(Policy="ApplicationLock")]
        [HttpPost]
        public ActionResult GetInvalidModuleData([FromBody] GetModuleDataPostBody body)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;

                if (CommonBLL.CheckMenuAccess(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), body.ModuleID, EActionType.Import))
                {
                    int rowCount = 0;
                    List<NawaDataDAL.DynamicDictionary> moduleData = ModuleBLL.GetInvalidModuleData(
                        body.ModuleID, 
                        identity.FindFirst("UserID").Value, 
                        body.page, 
                        body.orderBy, 
                        body.order,
                        body.search, 
                        "", 
                        out rowCount,
                        body.jsonFilter
                    );

                    return Ok(new { count = rowCount, rows = moduleData });
                }
                else
                {
                    return StatusCode(500, "You don't have authorization to access this menu.");
                }
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetModuleDetail")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetModuleDetail(int ModuleID, string ID = "", long PK_ModuleApproval_ID = 0, EActionType action = 0)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                int mGroupMenuID = Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value);
                if (action != 0)
                {
                    if (CommonBLL.CheckMenuAccess(mGroupMenuID, ModuleID, action)) 
                    {
                        ModuleView moduleView = ModuleBLL.GetModuleDetail(ModuleID, ID, PK_ModuleApproval_ID, identity.FindFirst("UserID").Value, mGroupMenuID);

                        return Ok(moduleView);
                    }
                    else
                    {
                        return StatusCode(500, "You don't have authorization to access this menu.");
                    }
                }
                else
                {
                    if (CommonBLL.CheckMenuAccess(mGroupMenuID, ModuleID, EActionType.View)
                    || CommonBLL.CheckMenuAccess(mGroupMenuID, ModuleID, EActionType.Insert)
                    || CommonBLL.CheckMenuAccess(mGroupMenuID, ModuleID, EActionType.Import)
                    || CommonBLL.CheckMenuAccess(mGroupMenuID, ModuleID, EActionType.Detail)
                    || CommonBLL.CheckMenuAccess(mGroupMenuID, ModuleID, EActionType.Approval))
                    {
                        ModuleView moduleView = ModuleBLL.GetModuleDetail(ModuleID, ID, PK_ModuleApproval_ID, identity.FindFirst("UserID").Value, mGroupMenuID);

                        return Ok(moduleView);
                    }
                    else
                    {
                        return StatusCode(500, "You don't have authorization to access this menu.");
                    }
                }
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex,userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetModuleDetailDetail")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetModuleDetailDetail(int ModuleDetailID, string ID = "", long PK_ModuleApproval_ID = 0)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                int mGroupMenuID = Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value);

                if (CommonBLL.CheckMenuAccessModuleDetail(mGroupMenuID, ModuleDetailID, EActionType.View))
                {
                    ModuleDetailView moduleView = ModuleBLL.GetModuleDetailDetail(ModuleDetailID, ID, PK_ModuleApproval_ID, identity.FindFirst("UserID").Value, mGroupMenuID);

                    return Ok(moduleView);
                }
                else
                {
                    return StatusCode(500, "You don't have authorization to access this menu.");
                }
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex,userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetModuleData")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetModuleData(int ModuleID, int page, string orderBy, string order, string search, string filter, bool isAdv, int rowPerPage)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                int rowCount = 0;
                List<ResponseSummary> listsummary = new List<ResponseSummary>();

                if (!CommonBLL.CheckMenuAccess(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), ModuleID, EActionType.View))
                    return StatusCode(500, "You don't have authorization to access this menu.");

                List<NawaDataDAL.DynamicDictionary> moduleData = ModuleBLL.GetModuleData(
                    ModuleID, 
                    page, 
                    orderBy, 
                    order, 
                    search, 
                    filter, 
                    isAdv, 
                    out rowCount, 
                    identity.FindFirst("FK_MRole_ID").Value, 
                    identity.FindFirst("UserID").Value,
                    out listsummary,
                    null,
                    rowPerPage
                );

                return Ok(new { count = rowCount, rows = moduleData, listsummary = listsummary });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex,userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetModuleData")]
        [Authorize(Policy = "ApplicationLock")]
        [HttpPost]
        public ActionResult GetModuleData([FromBody] GetModuleDataPostBody body)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                int rowCount = 0;
                List<ResponseSummary> listsummary = new List<ResponseSummary>();

                if (!CommonBLL.CheckMenuAccess(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), body.ModuleID, EActionType.View))
                    return StatusCode(500, "You don't have authorization to access this menu.");

                List<DynamicDictionary> moduleData = ModuleBLL.GetModuleData(
                    body.ModuleID,
                    body.page,
                    body.orderBy,
                    body.order,
                    body.search,
                    body.filter,
                    body.isAdv,
                    out rowCount,
                    identity.FindFirst("FK_MRole_ID").Value,
                    identity.FindFirst("UserID").Value,
                    listsummary: out listsummary,
                    body.jsonFilter
                );

                return Ok(new { count = rowCount, rows = moduleData, listsummary = listsummary });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetApprovalDataHistory")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetApprovalDataHistory(long PK_ModuleApproval_ID, int page, string orderBy, string order, string search, string filter, long moduleid)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                int rowCount = 0;
                int rowCountCheck = 0;

                List<NawaDataDAL.DynamicDictionary> moduleData = ModuleBLL.GetApprovalDataHistory(
                    PK_ModuleApproval_ID, 
                    page, 
                    orderBy,
                    order, 
                    search, 
                    filter, 
                    out rowCount, 
                    identity.FindFirst("FK_MRole_ID").Value, 
                    identity.FindFirst("UserID").Value,
                    moduleid,
                    out rowCountCheck
                );
                if (rowCountCheck > 0)
                {
                    return Ok(new { count = rowCount, rows = moduleData , isHistory = true });
                }
                else
                {
                    return Ok(new { count = rowCount, rows = moduleData,isHistory=false });
                }
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex,userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetModuleDetailDataView")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetModuleDetailDataView(int ModuleDetailID, int id, int page, string orderBy, string order, string search, string filter, bool isAdv, string detailType, EActionType actionType)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;

                if (CommonBLL.CheckMenuAccessModuleDetail(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), ModuleDetailID, actionType))
                {
                    int rowCount = 0;
                    List<ResponseSummary> listsummary = new List<ResponseSummary>();
                    string userID = identity.FindFirst("UserID").Value;
                    List<NawaDataDAL.DynamicDictionary> moduleData = ModuleBLL.GetModuleDetailDataByFkId(ModuleDetailID, id, page, orderBy, order, search, filter, isAdv, detailType, out rowCount, listsummary: out listsummary, userID);
                    return Ok(new { count = rowCount, rows = moduleData, listsummary = listsummary });
                }
                else
                {
                    return StatusCode(500, "You don't have authorization to access this menu.");
                }
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex,userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetModuleDetailDataViewApproval")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetModuleDetailDataViewApproval(int ModuleDetailID, long ModuleApprovalID)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;

                if (CommonBLL.CheckMenuAccessModuleDetail(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), ModuleDetailID, EActionType.Insert))
                {
                    int rowCount = 0;
                    List<NawaDataDAL.DynamicDictionary> moduleData = ModuleBLL.GetModuleDetailDataByFkApprovalId(ModuleDetailID, ModuleApprovalID, out rowCount);

                    return Ok(new { count = rowCount, rows = moduleData });
                }
                else
                {
                    return StatusCode(500, "You don't have authorization to access this menu.");
                }
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex,userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetModuleApproval")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetModuleApproval(int ModuleID, int page, string orderBy, string order, string search, string filterKey, string filterValue)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MUser currentUser = new MUser()
                {
                    UserID = identity.FindFirst("UserID").Value,
                    FK_MRole_ID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value),
                    FK_MGroupMenu_ID = Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value)
                };

                if (!CommonBLL.CheckMenuAccess(currentUser.FK_MGroupMenu_ID, ModuleID, EActionType.Approval))
                    return StatusCode(500, "You don't have authorization to access this menu.");

                int rowCount = 0;
                List<ModuleApprovalView> moduleApproval = ModuleBLL.GetModuleApproval(ModuleID, identity.FindFirst("UserID").Value, page, orderBy, order, search, filterKey, filterValue, out rowCount, currentUser.FK_MRole_ID);

                return Ok(new { count = rowCount, rows = moduleApproval });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex,userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetCustomModuleApprovalByID")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetCustomModuleApprovalByID(long PK_ModuleApproval_ID)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MUser currentUser = new MUser()
                {
                    UserID = identity.FindFirst("UserID").Value,
                    FK_MRole_ID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value),
                    FK_MGroupMenu_ID = Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value)
                };

                if (!ModuleBLL.CheckModuleApprovalByID(Convert.ToInt32(PK_ModuleApproval_ID)))
                    return StatusCode(500, "Data already on process approval.");

                if (!CommonBLL.CheckMenuAccessByApprovalID(currentUser.FK_MGroupMenu_ID, Convert.ToInt32(PK_ModuleApproval_ID), EActionType.Approval))
                    return StatusCode(500, "You don't have authorization to access this menu.");

                ModuleApprovalView moduleAccessApproval = ModuleBLL.GetCustomModuleApprovalByID(PK_ModuleApproval_ID);

                return Ok(moduleAccessApproval);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetModuleCorrection")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetModuleCorrection(int ModuleID, int page, string orderBy, string order, string search, string filterKey, string filterValue)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MUser currentUser = new MUser()
                {
                    UserID = identity.FindFirst("UserID").Value,
                    FK_MRole_ID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value),
                    FK_MGroupMenu_ID = Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value)
                };

                if (!CommonBLL.CheckMenuAccess(currentUser.FK_MGroupMenu_ID, ModuleID, EActionType.Approval))
                    return StatusCode(500, "You don't have authorization to access this menu.");

                int rowCount = 0;
                List<ModuleApprovalView> moduleApproval = ModuleBLL.GetModuleCorrection(ModuleID, identity.FindFirst("UserID").Value, page, orderBy, order, search, filterKey, filterValue, out rowCount, currentUser.FK_MRole_ID);

                return Ok(new { count = rowCount, rows = moduleApproval });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex,userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetModuleUploadDetail")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetModuleUploadDetailByID(int page, string orderBy, string order, int ModuleID, int RecordID, string type, string search, string moduleDetailID, string filter)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                if (CommonBLL.CheckMenuAccess(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), ModuleID, EActionType.Import)
                    || CommonBLL.CheckMenuAccess(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), ModuleID, EActionType.Approval))
                {
                    List<ModuleUploadDetailView> uploadDetail = ModuleBLL.GetModuleUploadDetail( page,  orderBy,  order, ModuleID, RecordID, identity.FindFirst("UserID").Value, search, filter, moduleDetailID, type);

                    return Ok(uploadDetail);
                }
                else
                {
                    return StatusCode(500, "You don't have authorization to access this menu.");
                }
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetModuleApprovalByID")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetModuleApprovalByID(long PK_ModuleApproval_ID)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MUser currentUser = new MUser()
                {
                    UserID = identity.FindFirst("UserID").Value,
                    FK_MRole_ID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value),
                    PK_MUser_ID = Convert.ToInt32(identity.FindFirst("PK_MUser_ID").Value),
                    FK_MGroupMenu_ID = Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value)
                };

                if (!ModuleBLL.CheckApprovalAccessByID(Convert.ToInt32(PK_ModuleApproval_ID), currentUser.UserID, currentUser.FK_MRole_ID))
                    return StatusCode(500, "You don't have authorization to access this data.");

                if (!ModuleBLL.CheckModuleApprovalByID(Convert.ToInt32(PK_ModuleApproval_ID), currentUser.PK_MUser_ID))
                    return StatusCode(500, "Data already on process approval.");

                if (!CommonBLL.CheckMenuAccessByApprovalID(currentUser.FK_MGroupMenu_ID, Convert.ToInt32(PK_ModuleApproval_ID), EActionType.Approval))
                    return StatusCode(500, "You don't have authorization to access this menu.");

                ModuleApprovalView moduleApproval = ModuleBLL.GetModuleApprovalByID(PK_ModuleApproval_ID);

                return Ok(moduleApproval);

            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }


        [Route("GetImportApprovalByID")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetImportApprovalByID(long PK_ModuleApproval_ID, int page, string orderBy, string order, string search, string filter)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MUser currentUser = new MUser()
                {
                    UserID = identity.FindFirst("UserID").Value,
                    FK_MRole_ID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value),
                    FK_MGroupMenu_ID = Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value)
                };


                if (!CommonBLL.CheckMenuAccessByApprovalID(currentUser.FK_MGroupMenu_ID, Convert.ToInt32(PK_ModuleApproval_ID), EActionType.Approval))
                    return StatusCode(500, "You don't have authorization to access this menu.");

                int rowCount = 0;

                List<NawaDataDAL.DynamicDictionary> moduleData = ModuleBLL.GetImportApprovalByID(
                    PK_ModuleApproval_ID, 
                    page, 
                    orderBy, 
                    order, 
                    search, 
                    filter, 
                    out rowCount,
                    null
                );

                return Ok(new { count = rowCount, rows = moduleData });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetImportApprovalByID")]
        [Authorize(Policy="ApplicationLock")]
        [HttpPost]
        public ActionResult GetImportApprovalByID([FromBody] GetApprovalDataHistoryPostBody body)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MUser currentUser = new MUser()
                {
                    UserID = identity.FindFirst("UserID").Value,
                    FK_MRole_ID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value),
                    FK_MGroupMenu_ID = Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value)
                };


                if (!CommonBLL.CheckMenuAccessByApprovalID(currentUser.FK_MGroupMenu_ID, Convert.ToInt32(body.PK_ModuleApproval_ID), EActionType.Approval))
                    return StatusCode(500, "You don't have authorization to access this menu.");

                int rowCount = 0;

                List<NawaDataDAL.DynamicDictionary> moduleData = ModuleBLL.GetImportApprovalByID(
                    body.PK_ModuleApproval_ID,
                    body.page,
                    body.orderBy,
                    body.order,
                    body.search,
                    "",
                    out rowCount,
                    body.jsonFilter
                );

                return Ok(new { count = rowCount, rows = moduleData });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetModuleDetailApprovalByFkID")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetModuleDetailApprovalByFkID(long PK_ModuleApproval_ID, long ModuleDetailID)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MUser currentUser = new MUser()
                {
                    UserID = identity.FindFirst("UserID").Value,
                    FK_MRole_ID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value),
                    FK_MGroupMenu_ID = Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value)
                };

                if (!CommonBLL.CheckMenuAccessByApprovalID(currentUser.FK_MGroupMenu_ID, Convert.ToInt32(PK_ModuleApproval_ID), EActionType.Approval))
                    return StatusCode(500, "You don't have authorization to access this menu.");

                List<ModuleDetailApprovalView> moduleDetailApproval = ModuleBLL.GetModuleDetailApprovalByFkID(PK_ModuleApproval_ID, ModuleDetailID);

                return Ok(moduleDetailApproval);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetModuleDetailUploadApprovalByFkID")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetModuleDetailUploadApprovalByFkID(long PK_ModuleApproval_ID, long ModuleDetailID)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MUser currentUser = new MUser()
                {
                    UserID = identity.FindFirst("UserID").Value,
                    FK_MRole_ID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value),
                    FK_MGroupMenu_ID = Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value)
                };


                if (!CommonBLL.CheckMenuAccessByApprovalID(currentUser.FK_MGroupMenu_ID, Convert.ToInt32(PK_ModuleApproval_ID), EActionType.Approval))
                    return StatusCode(500, "You don't have authorization to access this menu.");

                List<ModuleDetailApprovalView> moduleDetailApproval = ModuleBLL.GetModuleDetailUploadApprovalByFkID(PK_ModuleApproval_ID, ModuleDetailID);

                return Ok(moduleDetailApproval);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetModuleWorkflowHistory")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetModuleWorkflowHistory(long PK_ModuleApproval_ID, int page, string orderBy, string order, string search)
        {
            try
            {
                int rowCount = 0;
                List<MWorkFlow_HistoryView> workflowHistories = ModuleBLL.GetModuleWorkflowHistory(PK_ModuleApproval_ID, page, orderBy, order, search, out rowCount);

                return Ok(new { count = rowCount, rows = workflowHistories });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ActionModuleApproval")]
        [Authorize(Policy="ApplicationLock")]
        [HttpPost]
        public ActionResult ActionModuleApproval(CParameterApproval parameterApproval)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MUser currentUser = new MUser()
                {
                    UserID = identity.FindFirst("UserID").Value,
                    FK_MRole_ID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value),
                    FK_MGroupMenu_ID = Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value)
                };
                string alternateUser = identity.FindFirst("AlternateUser").Value;

                if (parameterApproval.PK_ModuleApproval_IDSelected != null 
                    && parameterApproval.PK_ModuleApproval_IDSelected.Length > 0)
                {
                    foreach (long pk in parameterApproval.PK_ModuleApproval_IDSelected)
                    {
                        if (!CommonBLL.CheckMenuAccessByApprovalID(currentUser.FK_MGroupMenu_ID, Convert.ToInt32(pk), EActionType.Approval))
                            return StatusCode(500, "You don't have authorization to access this menu.");
                        if (parameterApproval.ActionApproval == EActionApprovalType.Approve)
                        {
                            ModuleBLL.ApproveModuleApproval(pk, identity.FindFirst("UserID").Value, Convert.ToInt32(identity.FindFirst("PK_MUser_ID").Value), Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), parameterApproval.Review, alternateUser);
                        }
                        else if (parameterApproval.ActionApproval == EActionApprovalType.Reject)
                        {
                            ModuleBLL.RejectModuleApproval(pk, Convert.ToInt32(identity.FindFirst("PK_MUser_ID").Value), parameterApproval.Review, identity.FindFirst("UserID").Value, alternateUser);
                        }
                    }
                }
                else
                {
                    if (!CommonBLL.CheckMenuAccessByApprovalID(currentUser.FK_MGroupMenu_ID, Convert.ToInt32(parameterApproval.PK_ModuleApproval_ID), EActionType.Approval))
                        return StatusCode(500, "You don't have authorization to access this menu.");
                    if (parameterApproval.ActionApproval == EActionApprovalType.Approve)
                    {
                        ModuleBLL.ApproveModuleApproval(parameterApproval.PK_ModuleApproval_ID, identity.FindFirst("UserID").Value, Convert.ToInt32(identity.FindFirst("PK_MUser_ID").Value), Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), parameterApproval.Review, alternateUser);
                    }
                    else if (parameterApproval.ActionApproval == EActionApprovalType.Reject)
                    {
                        ModuleBLL.RejectModuleApproval(parameterApproval.PK_ModuleApproval_ID, Convert.ToInt32(identity.FindFirst("PK_MUser_ID").Value), parameterApproval.Review, identity.FindFirst("UserID").Value, alternateUser);
                    }
                    else
                    {
                        ModuleBLL.ReviseModuleApproval(parameterApproval.PK_ModuleApproval_ID, Convert.ToInt32(identity.FindFirst("PK_MUser_ID").Value), parameterApproval.Review, identity.FindFirst("UserID").Value, currentUser.FK_MRole_ID);
                    }
                }
                
                return Ok();
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ExportModuleData")]
        [HttpGet]
        [Authorize(Policy="ApplicationLock")]
        public IActionResult ExportModuleData(int ModuleID, int page, string orderBy, string order, string search, string filter, bool isAdv, string exportAs, ModuleField fields)
        {
            ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;

            try
            {
                if (!CommonBLL.CheckMenuAccess(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), ModuleID, EActionType.View))
                    return StatusCode(500, "You don't have authorization to access this menu.");

                MemoryStream ms = ModuleBLL.ExportModuleData(
                    ModuleID,
                    page,
                    orderBy,
                    order,
                    search,
                    filter,
                    isAdv,
                    exportAs,
                    null,
                    identity.FindFirst("UserID").Value,
                    Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value),
                    null
                );
                ms.Position = 0;

                string mediaType;

                if (exportAs == "Text")
                {
                    mediaType = "text/plain";
                }
                else if (exportAs == "CSV")
                {
                    mediaType = "text/csv";
                }
                else
                {
                    mediaType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                }

                return File(ms, mediaType);
            }
            catch (Exception ex)
            {
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ExportModuleData")]
        [HttpPost]
        [Authorize(Policy="ApplicationLock")]
        public IActionResult ExportModuleData([FromBody] GetModuleDataPostBody body)
        {
            ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
            try
            {
                if (!CommonBLL.CheckMenuAccess(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), body.ModuleID, EActionType.View))
                    return StatusCode(500, "You don't have authorization to access this menu.");

                MemoryStream ms = ModuleBLL.ExportModuleData(
                    body.ModuleID,
                    body.page,
                    body.orderBy,
                    body.order,
                    body.search,
                    body.filter,
                    body.isAdv,
                    body.exportAs,
                    body.fields,
                    identity.FindFirst("UserID").Value,
                    Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value),
                    body.jsonFilter,
                    body.rowPerPage

                );
                ms.Position = 0;

                string mediaType;

                if (body.exportAs == "Text")
                {
                    mediaType = "text/plain";
                }
                else if (body.exportAs == "CSV")
                {
                    mediaType = "text/csv";
                }
                else
                {
                    mediaType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                }

                return File(ms, mediaType);
            }
            catch (Exception ex)
            {
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ExportModuleDataAll")]
        [HttpGet]
        [Authorize(Policy="ApplicationLock")]
        public IActionResult ExportModuleDataAll(int ModuleID, string orderBy, string order, string search, string filter, bool isAdv, string exportAs)
        {
            ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
            try
            {
                if (!CommonBLL.CheckMenuAccess(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), ModuleID, EActionType.View))
                    return StatusCode(500, "You don't have authorization to access this menu.");

                MemoryStream ms = ModuleBLL.ExportModuleDataAll(
                    ModuleID,
                    orderBy,
                    order,
                    search,
                    filter,
                    isAdv,
                    exportAs,
                    null,
                    identity.FindFirst("UserID").Value,
                    Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value),
                    null
                );
                ms.Position = 0;

                string mediaType;

                if (exportAs == "Text")
                {
                    mediaType = "text/plain";
                }
                else if (exportAs == "CSV")
                {
                    mediaType = "text/csv";
                }
                else
                {
                    mediaType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                }

                return File(ms, mediaType);
            }
            catch (Exception ex)
            {
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ExportModuleDataAll")]
        [HttpPost]
        [Authorize(Policy="ApplicationLock")]
        public IActionResult ExportModuleDataAll([FromBody] GetModuleDataPostBody body)
        {
            ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
            try
            {
                if (!CommonBLL.CheckMenuAccess(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), body.ModuleID, EActionType.View))
                    return StatusCode(500, "You don't have authorization to access this menu.");

                MemoryStream ms = ModuleBLL.ExportModuleDataAll(
                    body.ModuleID,
                    body.orderBy,
                    body.order,
                    body.search,
                    body.filter,
                    body.isAdv,
                    body.exportAs,
                    body.fields,
                    identity.FindFirst("UserID").Value,
                    Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value),
                    body.jsonFilter
                );
                ms.Position = 0;

                string mediaType;

                if (body.exportAs == "Text")
                {
                    mediaType = "text/plain";
                }
                else if (body.exportAs == "CSV")
                {
                    mediaType = "text/csv";
                }
                else
                {
                    mediaType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                }

                return File(ms, mediaType);
            }
            catch (Exception ex)
            {
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ExportModuleDataSelected")]
        [HttpPost]
        [Authorize(Policy="ApplicationLock")]
        public IActionResult ExportModuleDataSelected(CSelectedData selectedData)
        {
            ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
            try
            {
                if (!CommonBLL.CheckMenuAccess(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), selectedData.ModuleID, EActionType.View))
                    return StatusCode(500, "You don't have authorization to access this menu.");

                string userID = identity.FindFirst("UserID").Value;
                MemoryStream ms = ModuleBLL.ExportModuleDataSelected(selectedData.ModuleID, selectedData.SelectedData, selectedData.orderBy, selectedData.order, selectedData.exportAs, selectedData.fields, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), userID);
                ms.Position = 0;

                string mediaType;

                if (selectedData.exportAs == "Text")
                {
                    mediaType = "text/plain";
                }
                else if (selectedData.exportAs == "CSV")
                {
                    mediaType = "text/csv";
                }
                else
                {
                    mediaType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                }

                return File(ms, mediaType);
            }
            catch (Exception ex)
            {
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ExportModuleDataSelectedMobile")]
        [HttpPost]
        [Authorize(Policy="ApplicationLock")]
        public IActionResult ExportUploadModuleDataSelectedMobile([FromBody] string data)
        {
            try
            {
                var selectedData = JsonSerializer.Deserialize<CSelectedData>(data);

                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MemoryStream ms = ModuleBLL.ExportUploadModuleDataSelected(selectedData.ModuleID, selectedData.SelectedData, selectedData.orderBy, selectedData.order, identity.FindFirst("UserID").Value);

                ms.Position = 0;
                return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Data.xlsx");
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ExportInvalidModuleDataAll")]
        [HttpGet]
        [Authorize(Policy="ApplicationLock")]
        public IActionResult ExportInvalidModuleDataAll(int moduleID, string orderBy, string order, string search, string filter, bool isAdv, string exportAs)
        {
            ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
            try
            {
                if (!CommonBLL.CheckMenuAccess(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), moduleID, EActionType.Import))
                    return StatusCode(500, "You don't have authorization to access this menu.");

                MemoryStream ms = ModuleBLL.ExportInvalidModuleDataAll(
                    moduleID,
                    orderBy,
                    order,
                    search,
                    filter,
                    isAdv,
                    exportAs,
                    null,
                    identity.FindFirst("UserID").Value,
                    Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value),
                    null
                );
                ms.Position = 0;

                return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Data.xlsx");
            }
            catch (Exception ex)
            {
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ExportInvalidModuleDataAll")]
        [HttpPost]
        [Authorize(Policy="ApplicationLock")]
        public IActionResult ExportInvalidModuleDataAll([FromBody] ExportInvalidModuleDataAllPostBody body)
        {
            ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;

            try
            {
                if (!CommonBLL.CheckMenuAccess(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), body.moduleID, EActionType.Import))
                    return StatusCode(500, "You don't have authorization to access this menu.");

                MemoryStream ms = ModuleBLL.ExportInvalidModuleDataAll(
                    body.moduleID,
                    body.orderBy,
                    body.order,
                    body.search,
                    body.filter,
                    body.isAdv,
                    body.exportAs,
                    body.fields,
                    identity.FindFirst("UserID").Value,
                    Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value),
                    body.jsonFilter
                );
                ms.Position = 0;

                return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Data.xlsx");
            }
            catch (Exception ex)
            {
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ExportModuleDetailData")]
        [HttpGet]
        [Authorize]
        public IActionResult ExportModuleDetailData(int moduleDetailID, int page, string orderBy, string order, string search, string filter, bool isAdv, string exportAs, int parentID)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MemoryStream ms = ModuleBLL.ExportModuleDetailData(moduleDetailID, page, orderBy, order, search, filter, isAdv, exportAs, null, identity.FindFirst("UserID").Value, parentID);
                ms.Position = 0;

                return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Data.xlsx");
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ExportModuleDetailData")]
        [HttpPost]
        [Authorize]
        public IActionResult ExportModuleDetailData([FromBody] ExportModuleDetailData body)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MemoryStream ms = ModuleBLL.ExportModuleDetailData(body.moduleDetailID, body.page, body.orderBy, body.order, body.search, body.filter, body.isAdv, body.exportAs, body.fields, identity.FindFirst("UserID").Value, body.parentID);
                ms.Position = 0;

                return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Data.xlsx");
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ExportModuleDetailDataAll")]
        [HttpGet]
        [Authorize]
        public IActionResult ExportModuleDetailDataAll(int moduleDetailID, string orderBy, string order, string search, string filter, bool isAdv, string exportAs, int parentID)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MemoryStream ms = ModuleBLL.ExportModuleDetailDataAll(moduleDetailID, orderBy, order, search, filter, isAdv, exportAs, null, identity.FindFirst("UserID").Value, parentID);
                ms.Position = 0;

                return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Data.xlsx");
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ExportModuleDetailDataAll")]
        [HttpPost]
        [Authorize]
        public IActionResult ExportModuleDetailDataAll([FromBody] ExportModuleDetailData body)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MemoryStream ms = ModuleBLL.ExportModuleDetailDataAll(body.moduleDetailID, body.orderBy, body.order, body.search, body.filter, body.isAdv, body.exportAs, body.fields, identity.FindFirst("UserID").Value, body.parentID);
                ms.Position = 0;

                return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Data.xlsx");
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ExportModuleDetailDataSelected")]
        [HttpPost]
        [Authorize(Policy="ApplicationLock")]
        public IActionResult ExportModuleDetailDataSelected(CSelectedData selectedData)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                MemoryStream ms = ModuleBLL.ExportModuleDetailDataSelected(selectedData.ModuleDetailID, selectedData.SelectedData, selectedData.orderBy, selectedData.order, selectedData.exportAs, selectedData.fields, selectedData.ParentID, userID);
                ms.Position = 0;

                return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Data.xlsx");
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetModuleDataByID")]
        [HttpGet]
        [Authorize(Policy="ApplicationLock")]
        public ActionResult GetModuleDataByID(int ModuleID, int id, EActionType actionType)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;

                if (CommonBLL.CheckMenuAccess(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), ModuleID, actionType))
                {
                    NawaDataDAL.Models.CDataWithValidation moduleData = ModuleBLL.GetModuleData(ModuleID, id);

                    return Ok(moduleData);
                }
                else
                {
                    return StatusCode(500, "You don't have authorization to access this menu.");
                }
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetListOptions")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetListOptions(long PK_ModuleField_ID, string ParentValue, string search="", string actionType="NawaInput")
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                List<COption> listOptions = ModuleBLL.GetListOptions(PK_ModuleField_ID, ParentValue, identity.FindFirst("UserID").Value, search, actionType);
                return Ok(listOptions);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetPopupBoxValue")]
        [Authorize(Policy = "ApplicationLock")]
        [HttpGet]
        public ActionResult GetPopupBoxValue(long PK_ModuleField_ID, string Value, bool isDetail = false)
        {
            string resCatch = "";
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                List<COption> listOptions = ModuleBLL.GetPopupBoxValue(PK_ModuleField_ID, Value, isDetail);
                return Ok(listOptions);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetCustomModuleListOptions")]
        [Authorize(Policy = "ApplicationLock")]
        [HttpGet]
        public ActionResult GetCustomModuleListOptions(string tabelReferenceName, string tableReferenceFieldDisplayName, string tableReferenceFieldKey)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                List<COption> listOptions = ModuleBLL.GetCustomModuleListOptions(tabelReferenceName, tableReferenceFieldDisplayName, tableReferenceFieldKey);
                return Ok(listOptions);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetListOptionsWithPage")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetListOptionsWithPage(long PK_ModuleField_ID, string ParentValue, int page = 1, string search = "")
        {
            try
            {
                int rowCount = 0;
                int pageSize = 10;
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;

                List<COption> listOptions = ModuleBLL.GetListOptionsWithPage(
                    PK_ModuleField_ID, 
                    ParentValue, 
                    identity.FindFirst("UserID").Value,
                    out rowCount,
                    out pageSize,
                    page, 
                    search 
                );

                return Ok(new { countPerPage = pageSize, count = rowCount, rows = listOptions });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetListOptionsByID")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetListOptionsByID(long PK_ModuleField_ID, string ParentValue, string FieldKey = "")
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                List<COption> listOptions = ModuleBLL.GetListOptionsByID(PK_ModuleField_ID, ParentValue, identity.FindFirst("UserID").Value, FieldKey);

                return Ok(listOptions);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetListOptionsModuleDetail")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetListOptionsModuleDetail(long PK_ModuleDetailField_ID, string ParentValue, string search="", string actionType = "NawaInput")
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                List<COption> listOptions = ModuleBLL.GetListOptionsModuleDetail(PK_ModuleDetailField_ID, ParentValue, search, actionType, userID);

                return Ok(listOptions);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetListOptionsModuleDetailWithPage")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetListOptionsModuleDetailWithPage(long PK_ModuleDetailField_ID, string ParentValue, int page = 1, string search = "")
        {
            try
            {
                int rowCount = 0;
                int pageSize = 10;
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;

                List<COption> listOptions = ModuleBLL.GetListOptionsModuleDetailWithPage(
                    PK_ModuleDetailField_ID,
                    ParentValue,
                    out rowCount,
                    out pageSize,
                    page,
                    search
                );

                return Ok(new { countPerPage = pageSize, count = rowCount, rows = listOptions });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetListOptionsModuleDetailByID")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetListOptionsModuleDetailByID(long PK_ModuleDetailField_ID, string ParentValue, string FieldKey = "")
        {
            try
            {
                List<COption> listOptions = ModuleBLL.GetListOptionsModuleDetailByID(PK_ModuleDetailField_ID, ParentValue, FieldKey);

                return Ok(listOptions);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("SaveParameterInput")]
        [Authorize(Policy="ApplicationLock")]
        [HttpPost]
        public ActionResult SaveParameterInput(CParameterInput parameterInput)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;

                if (CommonBLL.CheckMenuAccess(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), parameterInput.ModuleID, parameterInput.ActionType))
                {
                    List<KeyValuePair<long, string>> validationMessage = ModuleBLL.SaveData(parameterInput, identity.FindFirst("UserID").Value, Convert.ToInt32(identity.FindFirst("PK_MUser_ID").Value), Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("AlternateUser").Value, parameterInput.isSaveAsDraft);

                    if (validationMessage.Count > 0)
                    {
                        return StatusCode(400, validationMessage);
                    }
                    else
                    {
                        return Ok("Data has been saved");
                    }
                }
                else
                {
                    return StatusCode(500, "You don't have authorization to access this menu.");
                }
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("SaveParameterCorrection")]
        [Authorize(Policy="ApplicationLock")]
        [HttpPost]
        public ActionResult SaveParameterCorrection(CParameterCorrection parameterCorrection)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                List<KeyValuePair<long, string>> validationMessage = ModuleBLL.SaveCorrection(parameterCorrection, identity.FindFirst("UserID").Value, Convert.ToInt32(identity.FindFirst("PK_MUser_ID").Value), Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value));
                if (validationMessage.Count > 0)
                {
                    return StatusCode(400, validationMessage);
                }
                else
                {
                    return Ok("Data has been saved");
                }
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("SaveParameterDetail")]
        [Authorize(Policy="ApplicationLock")]
        [HttpPost]
        public ActionResult SaveParameterDetail(CParameterDetail parameterDetail)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;

                if (CommonBLL.CheckMenuAccess(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), parameterDetail.ModuleID, parameterDetail.ActionType))
                {
                    ModuleBLL.SaveData(parameterDetail, identity.FindFirst("UserID").Value, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), Convert.ToInt32(identity.FindFirst("PK_MUser_ID").Value), identity.FindFirst("AlternateUser").Value);

                    return Ok("Data has been saved");
                }
                else
                {
                    return StatusCode(500, "You don't have authorization to access this menu.");
                }
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("CustomSupportSP")]
        [Authorize(Policy="ApplicationLock")]
        [HttpPost]
        public ActionResult CustomSupportSP(CustomSupportSPRequest request)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;

                if (!CommonBLL.CheckMenuAccess(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), request.ModuleID, EActionType.View))
                    return StatusCode(500, "You don't have authorization to access this menu.");

                ModuleBLL.CustomSupportSP(request);

                return Ok("Action Complete");
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("RunStoredProcedure")]
        [Authorize(Policy = "ApplicationLock")]
        [HttpPost]
        public ActionResult RunStoredProcedure(CustomSupportSPRequest request)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;

                if (!CommonBLL.CheckMenuAccess(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), request.ModuleID, EActionType.View))
                    return StatusCode(500, "You don't have authorization to access this menu.");

                ModuleBLL.RunStoredProcedure(request);

                return Ok("Action Complete");
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        //[Route("GetModuleDetailData")]
        //[Authorize]
        //[HttpGet]
        //public ActionResult GetModuleDetailData(int ModuleID, int page, string orderBy, string order, string search, string filter, bool isAdv)
        //{
        //    try
        //    {
        //        ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
        //        int rowCount = 0;

        //        List<NawaDataDAL.DynamicDictionary> moduleData = ModuleBLL.GetModuleDetailData(
        //            ModuleID,
        //            page,
        //            orderBy,
        //            order,
        //            search,
        //            filter,
        //            isAdv,
        //            out rowCount,
        //            identity.FindFirst("FK_MRole_ID").Value,
        //            identity.FindFirst("UserID").Value,
        //            null
        //        );

        //        return Ok(new { count = rowCount, rows = moduleData });
        //    }
        //    catch (Exception ex)
        //    {
        //        ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
        //        string userID = identity.FindFirst("UserID").Value;
        //        ElmahErrorBLL.Log(ex, userID);
        //        return StatusCode(500, ex.Message);
        //    }
        //}

    }

}
