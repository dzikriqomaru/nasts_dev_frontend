INSERT INTO
    "SystemParameter" (
        "PK_SystemParameter_ID",
        "FK_SystemParameterGroup_ID",
        "SettingName",
        "fk_MFieldType_ID",
        "SettingValue",
        "Active",
        "Hide",
        "IsEncript"
    )
VALUES (
        -130,
        1,
        'Hide And Show Module Key', -- Use single quotes for string literals
        11,
        1,
        CAST(1 AS BIT),
        CAST(0 AS BIT),
        CAST(0 AS BIT)
    );

INSERT INTO
    "SystemParameterPickList" (
        "Pk_ApplicationAuthentication_ID",
        "ApplicationAuthentication",
        "SettingName"
    )
VALUES (
        1,
        'True',
        'Hide And Show Module Key'
    )

INSERT INTO
    "SystemParameterPickList" (
        "Pk_ApplicationAuthentication_ID",
        "ApplicationAuthentication",
        "SettingName"
    )
VALUES (
        2,
        'False',
        'Hide And Show Module Key'
    )