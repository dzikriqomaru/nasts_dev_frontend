CREATE OR ALTER PROCEDURE [dbo].[usp_ValidateUploadFormModule]
/*********************
* Procedure description:
* Date:   12/1/2015
* Author: Hendra
*
* Changes
* Date        Modified By                Comments
5 Oct 2022	Davin Julian	Add: ' and a.PK_ModuleAction_ID <> 7' in already exists on pending approval
17 Jan 2023	Davin Julian	Add: @FieldTypeSQLName
02 May 2023 Randi Imansyah  Add: ' and a.PK_ModuleAction_ID <> 1' in already exists on pending approval
05 May 2023 Davin Julian	Add: Try Cast field from table upload when check already exist in database & Cast a number to match the rounding of the data type
06 Jun 2023 Dandi Juliandi	Remove: RichTextField from upload feature
03 Jul 2023 Alhan Saha S	Add: 'LEN(LTRIM(RTRIM(' + @FieldName + '))) > 0' in cast a number to match the rounding of the data type
16 Jan 2024 Alhan Saha S	Remove: 'trim from LTRIM(RTRIM(' + @TableReferenceFieldDisplayName + ')) in ReferenceTable'
02 Feb 2024 Alhan Saha S	Add: '@TableReferenceFilter with @userid replacer in ReferenceTable'
07 Feb 2024 A I Humam		Add: check whether the inputted data is valid in database based on filter cascade
08 May 2024 Dandi Juliandi	Add: (8288) Change VARCHAR to VARCHAR(MAX) because string is splited and unique validaiotn is not working --> Tambahan Davin untuk issue (8682) 
10 Mei 2024	Davin Julian	Add: Checking column is exists in table Upload Approval before validate
13 May 2024 Dicco Suryo K   Add: (8226) fix upload for approval data when there is a special character in the field
13 May 2024 Davin Julian	Add: check same id in 1 file
16 May 2024 Davin Julian	Add: @FK_FieldType_ID in usp_ValidateUploadFormModule_CheckUploadPendingApproval
21 Mei 2024 Davin Julian	Add: Filter PK_Upload_ID in Cascade filter
30 Mei 2024 Davin Julian	Add: (8659) Validation for last field in json or middle field in json
06 Jun 2024 Dandi Juliandi	Add: (8752) Validation primary value in [Module]_upload_approval
11 Jun 2024 Dandi Juliandi 	Add: (8835) Validattion same Primary value can't work when Primary Key is not identity
********************
*
********************/
(
    @Moduleid INT,
    @userid VARCHAR(50),
    @intMode INT,
	@correctionID INT = 0,
	@groupMenuID INT = 0
)
AS
BEGIN
	--DECLARE @Moduleid INT, @userid VARCHAR(50),@intMode INT
	--   SET @Moduleid = 301114
	--   SET @userid = 'sysadmin'
	--   SET @intMode = 0

	DECLARE @Parent VARCHAR(255);
	DECLARE @sql VARCHAR(MAX)
	DECLARE @bIsCreateTableUploadForEachUser AS BIT=0
	DECLARE @strTableUploadName AS VARCHAR(500)=''
	SELECT @bIsCreateTableUploadForEachUser =sp.SettingValue
	  FROM SystemParameter AS sp WHERE sp.PK_SystemParameter_ID=53

	DECLARE @delimiter VARCHAR(MAX)
	SELECT @delimiter = SettingValue
	FROM SystemParameter
	WHERE PK_SystemParameter_ID = 39

    DECLARE @defaulttimeformat VARCHAR(50) = 'HH:mm';
    SELECT @defaulttimeformat = sp.SettingValue
    FROM SystemParameter AS sp
    WHERE sp.PK_SystemParameter_ID = 33;

	DECLARE @activationAccess INT
	SELECT @activationAccess = bActivation FROM MUser
	JOIN MGroupMenuAccess
	ON MUser.FK_MGroupMenu_ID = MGroupMenuAccess.FK_GroupMenu_ID
	where MUser.UserID = @userid AND MGroupMenuAccess.FK_Module_ID = @Moduleid

	--SELECT @defaulttimeformat
	DECLARE @dateformat INT
	SELECT @dateformat = CASE mdf.SQLFormat
		WHEN 'mmm dd yyyy hh:mm AM/PM' THEN 100
		WHEN 'mm/dd/yy' THEN 1
		WHEN 'mm/dd/yyyy' THEN 101
		WHEN 'yy.mm.dd' THEN 2
		WHEN 'dd/mm/yy' THEN 3
		WHEN 'dd.mm.yy' THEN 4
		WHEN 'dd-mm-yy' THEN 5
		WHEN 'dd Mmm yy' THEN 6
		WHEN 'Mmm dd, yy' THEN 7
		WHEN 'hh:mm:ss' THEN 8
		WHEN 'yyyy.mm.dd' THEN 102
		WHEN 'dd/mm/yyyy' THEN 103
		WHEN 'dd.mm.yyyy' THEN 104
		WHEN 'dd-mm-yyyy' THEN 105
		WHEN 'dd Mmm yyyy' THEN 106
		WHEN 'Mmm dd, yyyy' THEN 107
		WHEN 'Mmm dd yyyy hh:mm:ss:ms AM/PM' THEN 9
		WHEN 'Mmm dd yyyy hh:mi:ss:mmm AM/PM' THEN 9
		WHEN 'Mmm dd yy hh:mm:ss:ms AM/PM' THEN 109
		WHEN 'mm-dd-yy' THEN 10
		WHEN 'mm-dd-yyyy' THEN 110
		WHEN 'yy/mm/dd' THEN 11
		WHEN 'yyyy/mm/dd' THEN 111
		WHEN 'yymmdd' THEN 12
		WHEN 'yyyymmdd' THEN 112
		WHEN 'dd Mmm yyyy hh:mm:ss:Ms' THEN 113
		WHEN 'hh:mm:ss:Ms' THEN 14
		WHEN 'yyyy-mm-dd hh:mm:ss' THEN 120
		WHEN 'yyyy-mm-dd hh:mm:ss.Ms' THEN 121
		WHEN 'yyyy-mm-ddThh:mm:ss.Ms' THEN 126
		WHEN 'dd Mmm yyyy hh:mm:ss:ms AM/PM' THEN 130
		WHEN 'dd/mm/yy hh:mm:ss:ms AM/PM' THEN 131
		WHEN 'RFC822' THEN 2
		WHEN 'dd Mmm yyyy hh:mm' THEN 4
		ELSE 1 END
	FROM   SystemParameter sp
	INNER JOIN MDateFormat mdf
	ON sp.SettingValue = mdf.PK_DateFormat_ID 
	WHERE  sp.PK_SystemParameter_ID = 14

    DECLARE @strmodulename VARCHAR(250);
    SELECT @strmodulename = m.ModuleName
    FROM Module m
    WHERE m.PK_Module_ID = @Moduleid;

	IF @bIsCreateTableUploadForEachUser = 1
	BEGIN
	    SET @strTableUploadName = @strmodulename + '_Upload_data_' + dbo.StripTableName(@userid)
	END
	ELSE
	BEGIN
	    SET @strTableUploadName = @strmodulename + '_Upload'
	END

	SET @sql = 'UPDATE ' + @strTableUploadName + ' SET nawa_Action = ''Insert'' WHERE nawa_Action IS NULL OR nawa_Action = '''' AND nawa_userid=''' + @userid + ''''
    EXEC (@sql);

	IF (@activationAccess = 1)
	BEGIN
		SET @sql = 'UPDATE ' + @strTableUploadName + ' SET Active = 1 WHERE Active IS NULL AND nawa_userid=''' + @userid + ''''
		EXEC (@sql);
	END

	--Commented for regex validation in backend
    SET @sql = 'UPDATE ' + @strTableUploadName +' SET KeteranganError = '''' WHERE KeteranganError IS NULL AND nawa_userid=''' + @userid + ''''
    --PRINT (@sql);
    EXEC (@sql);

    DECLARE @PK_ModuleField_ID BIGINT,
            @FK_Module_ID INT,
            @FieldName VARCHAR(250),
            @FieldLabel VARCHAR(250),
            @Sequence INT,
            @Required BIT,
            @IsPrimaryKey BIT,
            @IsUnik BIT,
            @IsShowInView BIT,
            @FK_FieldType_ID INT,
            @SizeField INT,
            @FK_ExtType_ID INT,
            @TabelReferenceName VARCHAR(250),
            @TabelReferenceNameAlias VARCHAR(50),
            @TableReferenceFieldKey VARCHAR(250),
            @TableReferenceFieldDisplayName VARCHAR(250),
            @TableReferenceFilter VARCHAR(550),
            @IsUseRegexValidation BIT,
			@FieldNameParent VARCHAR(250),
			@FilterCascade VARCHAR(250),
            @Modulename VARCHAR(250),
            @FieldTypeDescription VARCHAR(255),
			@FieldTypeSQLName VARCHAR(250),
			@PK_FieldType_ID INT
    DECLARE @listFieldatas VARCHAR(MAX) = '';
    DECLARE @listfieldbawah VARCHAR(MAX) = '';

	DECLARE @primaryKeyFieldName VARCHAR(MAX) = (SELECT TOP 1 FieldName FROM ModuleField WHERE FK_Module_ID = @Moduleid AND IsPrimaryKey = 1);

    DECLARE my_cursor CURSOR FAST_FORWARD READ_ONLY FOR
    SELECT PK_ModuleField_ID,
           FK_Module_ID,
           FieldName,
           FieldLabel,
           Sequence,
           [Required],
           IsPrimaryKey,
           IsUnik,
           IsShowInView,
           FK_FieldType_ID,
           SizeField,
           FK_ExtType_ID,
           TabelReferenceName,
           TabelReferenceNameAlias,
           TableReferenceFieldKey,
           TableReferenceFieldDisplayName,
           TableReferenceFilter,
           IsUseRegexValidation,
		   FieldNameParent,
		   FilterCascade,
           m.ModuleName,
           mt.FieldTypeDescription,	
		   mt.FieldTypeSQLName,
		   mt.PK_FieldType_ID
    FROM dbo.ModuleField mf
        INNER JOIN Module m
            ON mf.FK_Module_ID = m.PK_Module_ID
        INNER JOIN MFieldType mt
            ON mt.PK_FieldType_ID = mf.FK_FieldType_ID
    WHERE mf.FK_Module_ID = @Moduleid
          AND (mf.IsShowInForm = 1 OR mf.IsPrimaryKey = 1)
		  AND FieldName IN (SELECT ModuleField FROM MGroupMenuAccessField mg WHERE FK_Module_ID = @Moduleid AND FK_MGroupMenu_ID = @groupMenuID AND bUpload = '1')
    ORDER BY mf.Sequence;

    OPEN my_cursor;

    FETCH FROM my_cursor
	INTO @PK_ModuleField_ID,
		 @FK_Module_ID,
		 @FieldName,
		 @FieldLabel,
		 @Sequence,
		 @Required,
		 @IsPrimaryKey,
		 @IsUnik,
		 @IsShowInView,
		 @FK_FieldType_ID,
		 @SizeField,
		 @FK_ExtType_ID,
		 @TabelReferenceName,
		 @TabelReferenceNameAlias,
		 @TableReferenceFieldKey,
		 @TableReferenceFieldDisplayName,
		 @TableReferenceFilter,
		 @IsUseRegexValidation,
		 @FieldNameParent,
		 @FilterCascade,
		 @Modulename,
		 @FieldTypeDescription,	
		 @FieldTypeSQLName,
		 @PK_FieldType_ID

    WHILE @@FETCH_STATUS = 0
    BEGIN
		

		-- Cek Akses Field	
		EXEC usp_ValidateWarningTableUpload @FieldName, @FK_Module_ID, @strTableUploadName, @FieldLabel, @userid, @IsPrimaryKey, @Required

		-- Check when primary key value is exist in [Module]_Upload_Approval table
		IF @IsPrimaryKey = 1
		BEGIN
			SET @sql=
				'IF EXISTS(
					select 1 from sys.columns c
					JOIN sys.objects obj on c.object_id = obj.object_id
					where obj.name = ''' + @strTableUploadName + '_approval'' and c.name in ('''+@FieldName+''')
				)
				BEGIN
				EXEC ('' UPDATE U SET U.KeteranganError = U.KeteranganError + ''''Line '''' + convert(Varchar(20), U.nawa_recordnumber) + '''' : '
		            + @FieldLabel + ' already exist in pending approval'''' ' +
					'FROM ' + @strTableUploadName + ' U JOIN ' + @strTableUploadName + '_approval UA ' + 
					'ON U.' + @primaryKeyFieldName + ' = UA.' + @primaryKeyFieldName + ' ' + 
					'and U.nawa_userid=''''' + @userid + ''''''') '
				+ 'END'
			EXEC (@sql);
		END;
		

		--Cast a number to match the rounding of the data type
        IF @FK_FieldType_ID = 5
           OR @FK_FieldType_ID = 6
           OR @FK_FieldType_ID = 7
           OR @FK_FieldType_ID = 8
        BEGIN
			Set	@sql =  'UPDATE ' + @strTableUploadName +' ' + CHAR(10)
	            + 'SET    '+@FieldName+' = TRY_CAST(' + @FieldName + ' as ' +@FieldTypeSQLName + ')'  +
				' WHERE LEN(LTRIM(RTRIM(' + @FieldName + '))) > 0 AND TRY_CAST(' + @FieldName + 
				' as ' +@FieldTypeSQLName + ') IS NOT NULL and nawa_userid=''' + @userid +
	            '''  and nawa_Action <> ''Delete'' '
			EXEC(@sql);
		END;

        --Cek required dan kosong maka invalid
        IF @Required = 1
           AND NOT (
                       @FK_FieldType_ID = 12
                       OR @FK_FieldType_ID = 15
                   ) -- identity
        BEGIN
	        SET @sql = ' UPDATE ' + @strTableUploadName +' SET KeteranganError =KeteranganError + ''Line ''+ convert(Varchar(20),nawa_recordnumber) +'' : '
	            + @FieldLabel + ' is required  '' WHERE (' + @FieldName +
	            '='''' or ' + @FieldName + ' is null )  and nawa_userid=''' + @userid +
	            '''  and nawa_Action <> ''Delete'' '
            --PRINT (@sql);
            EXEC (@sql);
        END;

		--Reference table
        IF @FK_FieldType_ID = 11
        BEGIN
			DECLARE @ParentExtType INT, @ParentFieldType INT
            IF @FK_ExtType_ID <> 15 AND @FK_ExtType_ID <> 21 AND @FK_ExtType_ID <> 7 
            BEGIN
	            SET @sql = 'update ' + @strTableUploadName +' set ' + @FieldName + '= ''(''+' + @FieldName + ' from  ' +
	                @strTableUploadName +' where charindex(''('',' + @FieldName + ') =0 and '+ @FieldName + ' is not null and '+ @FieldName + ' != ''''';

                --PRINT (@sql);
                EXEC (@sql);

	            SET @sql = 'update ' + @strTableUploadName +' set ' + @FieldName
	                + '= ' + @FieldName + '+'')'' from  ' +
	                @strTableUploadName +' where charindex('')'',' + @FieldName + ') =0 and '+ @FieldName + ' is not null and '+ @FieldName + ' != ''''';

                --PRINT (@sql);
                EXEC (@sql);

                DECLARE @alias VARCHAR(50) = @TabelReferenceNameAlias;
                IF @alias = ''
                    SET @alias = @TabelReferenceName;

                SET @listFieldatas += ' ' + @Modulename + '.' + @FieldName + 'Name,' + @Modulename + '.' + @FieldName
                                      + ',';

                SET @listfieldbawah += '  ' + @FieldName + ' AS ' + @FieldName + 'Name , REPLACE( REPLACE( SUBSTRING( '
                                       + @FieldName + ', charindex(''('',' + @FieldName + '),charindex('')'','
                                       + @FieldName + ')),''('',''''),'')'','''') ' + @FieldName + ',';

				-- Dandi Juliandi 24/01/2024 : Remove trim for dropdown have multiple space
				SET @sql = 'UPDATE tulv SET KeteranganError = KeteranganError + ''Line '' + CONVERT(VARCHAR(20), nawa_recordnumber) + '' : '
	                + @FieldLabel + ' not exist in database '' + CHAR(13) + CHAR(10) FROM ' + @strTableUploadName + ' tulv LEFT JOIN ( SELECT '
					+ '''('' + CONVERT(VARCHAR(8000), ' + @alias + '.' + @TableReferenceFieldKey + ') +'')'' + '' '' + CAST(' + @alias + '.' 
					+ @TableReferenceFieldDisplayName + ' AS VARCHAR(255)) AS DisplayField, '
					+ '''(''+ CONVERT(VARCHAR(MAX), ' + @alias + '.' + @TableReferenceFieldKey + ')' + ' +'')'' AS DisplayKey '

				--add cascade filter
				IF @FieldNameParent <> ''
				BEGIN
					SET @sql += ' ,tulv.PK_UPLOAD_ID ' 
					SET @sql += 'FROM ' + @TabelReferenceName + '  ' + @alias

					--Get Parent Field
					SELECT @ParentExtType = FK_ExtType_ID, @ParentFieldType = FK_FieldType_ID
					FROM ModuleField
					WHERE FieldName = @FieldNameParent AND FK_Module_ID = @Moduleid
					----
					--If ExtType is not FileUpload AND RichText
					IF @ParentExtType <> 8 AND @ParentExtType <> 16
					BEGIN
						IF @ParentFieldType = 11
						BEGIN
							SET @sql += ' INNER JOIN ' + @strTableUploadName + ' tulv ON ' + REPLACE(@FilterCascade, '@Parent', 
								'SUBSTRING(tulv.' + @FieldNameParent + ', CHARINDEX(''('',tulv.' + @FieldNameParent + ')+1, CHARINDEX('')'',tulv.' + @FieldNameParent + ')-2)'
							);
						END
						ELSE
						BEGIN
							SET @sql += ' INNER JOIN ' + @strTableUploadName + ' tulv ON ' + REPLACE(@FilterCascade, '@Parent', 
								'tulv.' + @FieldNameParent
							);
						END
					END
				END
				ELSE
				BEGIN
					SET @sql += 'FROM ' + @TabelReferenceName + '  ' + @alias
				END;

				--add table reference filter
				IF @TableReferenceFilter <> ''
                BEGIN
                    SET @sql += ' WHERE ' + REPLACE(LOWER(@TableReferenceFilter), '@userid', @userid);
                END;

	            SET @sql += ' )xx ON tulv.' + @FieldName + ' IN (xx.DisplayField, xx.DisplayKey) '

	            IF @FieldNameParent <> ''
				BEGIN
					 SET @sql += ' and tulv.PK_UPLOAD_ID = XX.PK_UPLOAD_ID ' -- Add Filter PK_Upload_ID
				END

				 SET @sql += ' WHERE tulv.nawa_userid = ''' + @userid
	                + ''' AND xx.DisplayField IS NULL AND xx.DisplayKey IS NULL AND tulv.' + @FieldName 
					+ ' <> '''' and nawa_Action <> ''Delete'' '
				--PRINT('sql : ' + @sql);
                EXEC (@sql);
            END;
            ELSE IF @FK_ExtType_ID = 15 OR @FK_ExtType_ID = 21
            BEGIN
                --kalau tipenya checkbox


                --UPDATE cobaidentitybigint_Upload SET KeteranganError = KeteranganError + 'Line ' +  CONVERT(VARCHAR(20), nawa_recordnumber)  + ' Field Checkboxcombo tidak '
                --FROM cobaidentitybigint_Upload  INNER JOIN (
                -- SELECT * FROM ( SELECT PK_upload_ID,(
                --SELECT ISNULL(
                --           STUFF(
                --               (
                --                   SELECT ';' + CONVERT(VARCHAR(1000), MUser.PK_MUser_ID) + ' - ' +
                --                          MUser.UserID
                --                   FROM   dbo.[Split](cobapopupusercombocheckbox, ';') AS s
                --                          INNER JOIN MUser
                --                               ON  CONVERT(VARCHAR(50), MUser.PK_MUser_ID) = CONVERT(VARCHAR(50), s.val)
                --                                   FOR XML PATH('')),1,1,''),'')
                --        )fieldcheckboxtovalidate    from cobaidentitybigint_Upload
                --  WHERE ISNULL( cobapopupusercombocheckbox,'')<>''
                --  )xx WHERE xx.fieldcheckboxtovalidate=''
  --    )yy ON  cobaidentitybigint_Upload.PK_upload_ID=yy.PK_upload_ID
				
				--DAVIN 20230321 -- Add Alias and Filter Reference in combo
                DECLARE @aliasCombo VARCHAR(50) = @TabelReferenceNameAlias;
                IF @aliasCombo = ''
                    SET @aliasCombo = @TabelReferenceName;

				DECLARE @FilterReferenceCombo VARCHAR(500)= '';
                IF @TableReferenceFilter <> ''
                BEGIN
                    SET @FilterReferenceCombo = ' AND ' + @TableReferenceFilter;
                END;

				--Humam 20240112 -- Add Filter Cascade in combo
				DECLARE @FilterCascadeCombo VARCHAR(500)= '';
				IF @FieldNameParent <> ''
				BEGIN
					--Get Parent Field
					SELECT @ParentExtType = FK_ExtType_ID, @ParentFieldType = FK_FieldType_ID
					FROM ModuleField
					WHERE FieldName = @FieldNameParent AND FK_Module_ID = @Moduleid
					----
					--If ExtType is not FileUpload and RichText
					IF @ParentExtType <> 8 AND @ParentExtType <> 16
					BEGIN
						--SET @FilterCascadeCombo = ' AND ' + REPLACE(@FilterCascade, '@Parent', 'tulv.' + @FieldNameParent);
						IF @ParentFieldType = 11
						BEGIN
							SET @FilterCascadeCombo = ' AND ' + REPLACE(@FilterCascade, '@Parent', 
								'SUBSTRING(' + @FieldNameParent + ', CHARINDEX(''('', ' + @FieldNameParent + ')+1, CHARINDEX('')'', ' + @FieldNameParent + ')-2)'
							);
						END
						ELSE
						BEGIN
							SET @FilterCascadeCombo = ' AND ' + REPLACE(@FilterCascade, '@Parent', @FieldNameParent);
						END
					END
				END;

                SET @sql
                    = 'UPDATE ' + @strTableUploadName
                      + ' SET KeteranganError = KeteranganError + ''Line '' +  CONVERT(VARCHAR(20), nawa_recordnumber)  + '' : '
                      + @FieldLabel + ' ('' + fieldcheckboxtovalidate + '') not exist in database '' ' + CHAR(10) + ' FROM ' + @strTableUploadName
                      + '  INNER JOIN ( ' + CHAR(10) + '  SELECT * FROM ( SELECT PK_upload_ID,(        ' + CHAR(10)
                      + ' SELECT ISNULL( ' + CHAR(10) + '            STUFF( ' + CHAR(10) + '                ( '
                      --+ CHAR(10) + '                    SELECT '';'' + CONVERT(VARCHAR(1000), ' + @TabelReferenceName
                      --+ '.' + @TableReferenceFieldKey + ') + '' - '' + ' + CHAR(10) + '                           '
                      --+ @TabelReferenceName + '.' + @TableReferenceFieldDisplayName + ' ' + CHAR(10)
					  + CHAR(10) + '         SELECT '''+@delimiter+''' + CONVERT(VARCHAR(1000), s.val) ' + CHAR(10)
                      + '                    FROM   dbo.[Split](' + @FieldName + ', '''+@delimiter+''') AS s ' + CHAR(10)
                      + '                           LEFT JOIN ' + @TabelReferenceName + ' ' + @aliasCombo + ' ' + CHAR(10)
					  + ' ON  CONVERT(VARCHAR(1000), ' + @aliasCombo + '.'
                      + @TableReferenceFieldKey + ') = CONVERT(VARCHAR(1000), s.val)  ' + @FilterReferenceCombo + @FilterCascadeCombo + CHAR(10)
					  + '					WHERE ' + @aliasCombo + '.' + @TableReferenceFieldKey + ' IS NULL' + CHAR(10)
                      + '                   FOR XML PATH('''')),1,1,''''),'''')  ' + CHAR(10)
                      + '         )fieldcheckboxtovalidate    from ' + @strTableUploadName + '  ' + CHAR(10)
                      + '   WHERE ISNULL( ' + @FieldName + ','''')<>''''  ' + CHAR(10)
                      --+ '   )xx WHERE xx.fieldcheckboxtovalidate='''' ' + CHAR(10) + '     )yy ON  '
                      + '   )xx ' + CHAR(10) + '     )yy ON  '
                      + @strTableUploadName + '.PK_upload_ID=yy.PK_upload_ID      ' + CHAR(10)
                      + 'WHERE ISNULL(fieldcheckboxtovalidate, '''') <> '''' ' + CHAR(10)
                EXEC (@sql);
            END;
        END;
        ELSE
        BEGIN
			SET @listFieldatas += ' ' + @Modulename + '.' + @FieldName + ','
			SET @listfieldbawah += ' ' + @strTableUploadName +'.' + @FieldName + ','
        END;

        --Jika primary key maka Untuk update dan delete harus di pastikan data tersebut tidak sedang menuggu approval
        IF @IsPrimaryKey = 1
        BEGIN
	        SET @sql = 'update ' + @strTableUploadName + ' ' + CHAR(13)
	            + ' set KeteranganError = KeteranganError + ''Line '' + CONVERT(VARCHAR(20), nawa_recordnumber) + '' : '
	            + @FieldLabel + ' already exists on pending approval  '' ' + CHAR(13)
	            + 'from ModuleApproval a' + CHAR(13)
	            + 'inner join ' + @strTableUploadName +' b on a.ModuleKey = b. ' + @FieldName + ' and a.PK_ModuleAction_ID <> 7 ' + CHAR(13)
	            + 'where a.ModuleName =''' + @Modulename + ''' and b.nawa_userid =''' + @userid
	            + ''' and nawa_Action in (''Delete'',''Update'')' + CHAR(13)
	            + '' + CHAR(13)
            --PRINT (@sql);
			EXEC (@sql);

			--20221005 comment
			--SET @sql = 'UPDATE ' + @strTableUploadName + ' ' + CHAR(13)
	  --          + ' SET ' + @FieldName + ' = ''''' + CHAR(13)
	  --          + ' WHERE nawa_userid = ''' + @userid + ''' and nawa_Action = ''Insert''' + CHAR(13)
   --         PRINT (@sql);
			--EXEC (@sql);
            --if (@Moduleid not in (8769))
            EXEC usp_ValidateUploadFormModule_CheckUploadPendingApproval @userid,
                                                                         @FieldLabel,
                                                                         @FieldName,
                                                                         @Modulename,
																		 @correctionID,
																		 @FK_FieldType_ID
        END;

		--Cek unik
        IF @IsUnik = 1 OR @IsPrimaryKey = 1
        BEGIN
            -- Jika identity : Jika terisi selain numeric maka diupdate jadi kosong.
            IF @FK_FieldType_ID = 12 OR @FK_FieldType_ID = 15
            BEGIN
	            SET @sql = 'UPDATE ' + @strTableUploadName +' set ' + @FieldName + ' = '''' where isnumeric(' +
	                @FieldName + ') = 0 '
                EXEC (@sql);

				SET @sql = 'UPDATE ' + @strTableUploadName + ' ' + char(10)
						 + 'SET KeteranganError = KeteranganError + '' Field ' + @FieldLabel + ' is required '' ' + char(10)
						 + 'WHERE ISNULL(' + @FieldName + ','''') = ''''  ' + char(10)
						 + 'AND nawa_userid=''' + @userid + ''' AND (nawa_action=''Update'' OR nawa_Action=''delete'')'
				EXEC(@sql)

				-- check same id in 1 file 
				SET @sql = 'UPDATE A SET A.KeteranganError = A.KeteranganError + ''Line '' + CONVERT(VARCHAR(20), A.nawa_recordnumber) + '' : '
							+ @FieldLabel + ' already exist in row ('' + CONVERT(VARCHAR(20), B.nawa_recordnumber) + '') '' FROM ' + @strTableUploadName
							+ ' A INNER JOIN ' + @strTableUploadName + ' B ON A.' + @FieldName + ' =  B.' + @FieldName
							+ ' WHERE A.nawa_Action IN (''UPDATE'', ''INSERT'', ''DELETE'') AND A.nawa_userid=''' + @userid
							+ ''' AND B.nawa_Action IN (''UPDATE'', ''INSERT'', ''DELETE'') AND B.nawa_userid=''' + @userid
							+ ''' AND A.nawa_recordnumber <> B.nawa_recordnumber'
							+ ' AND LEN(A.' + @FieldName + ') != 0'
							+ ' AND LEN(B.' + @FieldName + ') != 0'
				EXEC (@sql);
            END;
            IF @intMode = 0
            BEGIN
                IF @FK_FieldType_ID != 12 AND @FK_FieldType_ID != 15
				BEGIN
					IF @FK_FieldType_ID = 10 AND @FK_ExtType_ID = 1
					BEGIN
					DECLARE @Date VARCHAR(500) = 'CONCAT(FORMAT(DATEPART(YEAR,A.'+@FieldName+'), ''0000'') , ''-'' , FORMAT(DATEPART(MONTH,A.'+@FieldName+'), ''00'') , ''-'',FORMAT(DATEPART(Day,A.'+@FieldName+'), ''00''), ''T00:00:00'') '
							--Checking table ModuleApproval
							SET @sql =
								'UPDATE ' + @strTableUploadName + ' SET KeteranganError = KeteranganError + ''Line '' + CONVERT(VARCHAR(20), nawa_recordnumber) + '' : '
								+ @FieldLabel + ' already exist in pending approval '' + CHAR(13) + CHAR(10) FROM ' + @strTableUploadName +
								' A INNER JOIN ModuleApproval B ON B.ModuleField LIKE ''%"' + @FieldName  + '":"'' + ' + @Date  + ' + ''"%'' OR B.ModuleFieldBefore LIKE ''%"'
								+ @FieldName  + '":"'' + ' + @Date  + ' + ''"%'' WHERE nawa_userid = ''' + @userid + '''AND B.FK_Module_ID = ' + CONVERT(VARCHAR(50), @Moduleid)
								EXEC (@sql);

					END
					ELSE IF @FK_FieldType_ID = 10 AND @FK_ExtType_ID = 12
					BEGIN
						DECLARE @Time VARCHAR(500) = 'CONCAT(''1900-01-01T'',FORMAT(DATEPART(HOUR,A.'+@FieldName+'), ''00''),'':'',FORMAT(DATEPART(MINUTE,A.'+@FieldName+'), ''00''),'':'',FORMAT(DATEPART(SECOND,A.'+@FieldName+'), ''00'')) '
							--Checking table ModuleApproval
							SET @sql =
								'UPDATE ' + @strTableUploadName + ' SET KeteranganError = KeteranganError + ''Line '' + CONVERT(VARCHAR(20), nawa_recordnumber) + '' : '
								+ @FieldLabel + ' already exist in pending approval '' + CHAR(13) + CHAR(10) FROM ' + @strTableUploadName +
								' A INNER JOIN ModuleApproval B ON B.ModuleField LIKE ''%"' + @FieldName  + '":"'' + ' + @Date  + ' + ''"%'' OR B.ModuleFieldBefore LIKE ''%"'
								+ @FieldName  + '":"'' + ' + @Date  + ' + ''"%'' WHERE nawa_userid = ''' + @userid + '''AND B.FK_Module_ID = ' + CONVERT(VARCHAR(50), @Moduleid)
								EXEC (@sql);
					END
					--Davin 10 Mei 2024 -> Checking column is exists in table Upload Approval before validate
						--Checking table [ModuleName]_upload_approval
							SET @sql =
							'IF EXISTS(
									select 1 from sys.columns c
									JOIN sys.objects obj on c.object_id = obj.object_id
									where obj.name = ''' + @strTableUploadName + '_approval'' and c.name in ('''+@FieldName+''')
								)
								BEGIN
								EXEC (''UPDATE ' + @strTableUploadName + ' 
									SET KeteranganError = KeteranganError + ''''Line '''' + CONVERT(VARCHAR(20), nawa_recordnumber)  + '''' : '
															+ @FieldLabel + ' already exist in pending approval '''' + CHAR(13) + CHAR(10) '
									+ ' WHERE nawa_userid = ''''' + @userid +
									''''' AND ' + @FieldName  + ' IN (SELECT ' + @FieldName  + ' FROM ' + @strTableUploadName + '_approval)'') '
								+'END'
							EXEC ( @sql);
							---End Davin

						--Checking table ModuleApproval
						IF @FK_FieldType_ID = 1
						   OR @FK_FieldType_ID = 2
						   OR @FK_FieldType_ID = 3
						   OR @FK_FieldType_ID = 4
						   OR @FK_FieldType_ID = 5
						   OR @FK_FieldType_ID = 6
						   OR @FK_FieldType_ID = 7
						   OR @FK_FieldType_ID = 8
						   BEGIN
							SET @sql =
							'UPDATE ' + @strTableUploadName + ' SET KeteranganError = KeteranganError + ''Line '' + CONVERT(VARCHAR(20), nawa_recordnumber) + '' : '
							+ @FieldLabel + ' already exist in pending approval '' + CHAR(13) + CHAR(10) FROM ' + @strTableUploadName +
							' A INNER JOIN ModuleApproval B ON B.ModuleField LIKE ''%"' + @FieldName  + '":'' + A.' + @FieldName  + ' + '',%'' ' + ' OR B.ModuleField LIKE ''%"' + @FieldName  + '":'' + A.' + @FieldName  + ' + ''}%'' ' --ADD Validation for last field in json or middle field in json
							--+ ' OR B.ModuleFieldBefore LIKE ''%"' + @FieldName  + '":"'' + A.' + @FieldName  + ' + ''"%'' ' 
							+ ' WHERE nawa_userid = ''' + @userid + '''AND B.FK_Module_ID = ' + CONVERT(VARCHAR(50), @Moduleid)
							
						   END
						   -- Dandi Juliandi 24/01/2024 : Add validation in unique approval for Dropdown
						   ELSE IF (@FK_FieldType_ID = 11 AND @FK_ExtType_ID = 2)
						   BEGIN
								SET @sql =
								'UPDATE ' + @strTableUploadName + ' SET KeteranganError = KeteranganError + ''Line '' + CONVERT(VARCHAR(20), nawa_recordnumber) + '' : '
								+ @FieldLabel + ' already exist in pending approval '' + CHAR(13) + CHAR(10) FROM ' + @strTableUploadName +
								' A INNER JOIN ModuleApproval B ON B.ModuleField LIKE ''%"' + @FieldName  + '":"'' + SUBSTRING(A.' + @FieldName  + ', CHARINDEX(''('', A.' + @FieldName + ') + 1, CHARINDEX('')'', A.' + @FieldName + ') - CHARINDEX(''('', A.' + @FieldName + ') - 1) + ''"%'' '
								+ ' WHERE nawa_userid = ''' + @userid + '''AND B.FK_Module_ID = ' + CONVERT(VARCHAR(50), @Moduleid);
						   END
						   ELSE
						   BEGIN
						   -- Dicco 08/05/2024 :8226 fix upload for approval data
								SET @sql =
								'UPDATE ' + @strTableUploadName + ' SET KeteranganError = KeteranganError + ''Line '' + CONVERT(VARCHAR(20), nawa_recordnumber) + '' : '
								+ @FieldLabel + ' already exist in pending approval '' + CHAR(13) + CHAR(10) FROM ' + @strTableUploadName +
								' A INNER JOIN ModuleApproval B ON B.ModuleField LIKE ''%"' + @FieldName + '":"'' + REPLACE(REPLACE(REPLACE(REPLACE(A.' + @FieldName + ', ''\'', ''\\''), ''['', ''[[]''), ''"'', ''\"''), '''', '''''''') + ''"%'' '
								+ ' WHERE nawa_userid = ''' + @userid + ''' AND B.FK_Module_ID = ' + CONVERT(VARCHAR(50), @Moduleid);
						   END

						   EXEC (@sql);

						--Checking other row
						SET @sql = 'UPDATE A SET A.KeteranganError = A.KeteranganError + ''Line '' + CONVERT(VARCHAR(20), A.nawa_recordnumber) + '' : '
							+ @FieldLabel + ' already exist in row ('' + CONVERT(VARCHAR(20), B.nawa_recordnumber) + '') '' FROM ' + @strTableUploadName
							+ ' A INNER JOIN ' + @strTableUploadName + ' B ON A.' + @FieldName + ' =  B.' + @FieldName
							+ ' WHERE A.nawa_Action IN (''UPDATE'', ''INSERT'', ''DELETE'') AND A.nawa_userid=''' + @userid
							+ ''' AND B.nawa_Action IN (''UPDATE'', ''INSERT'', ''DELETE'') AND B.nawa_userid=''' + @userid
							+ ''' AND A.nawa_recordnumber <> B.nawa_recordnumber'
						EXEC (@sql);
				END
				IF @FK_FieldType_ID <> 11 or  @FK_ExtType_ID = 14 or @FK_ExtType_ID = 15
                BEGIN
					IF(@IsPrimaryKey = 1)
					BEGIN
						SET @sql = 'UPDATE ' + @strTableUploadName + ' SET KeteranganError = KeteranganError + ''Line '' + CONVERT(VARCHAR(20), nawa_recordnumber) + '' : '
							+ @FieldLabel + ' Not Exist in database  '' + CHAR(13) + CHAR(10) FROM ' + @strTableUploadName + ' LEFT JOIN (SELECT A.PK_upload_ID FROM '
							+ @strTableUploadName + ' A INNER JOIN ' + @Modulename + ' B ON  A.' + @FieldName + ' = b.' + @FieldName
							+ ' WHERE a.nawa_Action in (''Delete'',''Update'') AND a.nawa_userid=''' + @userid + ''') notvalid ON notvalid.PK_upload_ID = ' +
							@strTableUploadName + '.PK_upload_ID WHERE nawa_Action in (''Delete'',''Update'') AND nawa_userid=''' + @userid +
							''' AND notvalid.PK_upload_ID is null'
						EXEC (@sql);
					END;
					ELSE
					BEGIN
						IF @PK_FieldType_ID = 9
						BEGIN
							SET @FieldTypeSQLName = @FieldTypeSQLName + '(MAX)';
						END
						SET @sql =
							'UPDATE ' + @strTableUploadName + ' SET KeteranganError = KeteranganError + ''Line '' + CONVERT(VARCHAR(20), nawa_recordnumber) + '' : '
							+ @FieldLabel + ' already exist in database '' + CHAR(13) + CHAR(10) FROM ' + @strTableUploadName + ' A INNER JOIN ' + @Modulename + ' B ON TRY_CAST(A.'
							+ @FieldName + ' as '+@FieldTypeSQLName+') =  b.' + @FieldName  + ' WHERE A.nawa_Action = ''Insert'' AND A.nawa_userid=''' + @userid + ''''
						EXEC (@sql);

						IF @FK_FieldType_ID = 10
						BEGIN
							SET @sql =
							'UPDATE ' + @strTableUploadName + ' SET KeteranganError = KeteranganError + ''Line '' + CONVERT(VARCHAR(20), nawa_recordnumber) + '' : '
							+ @FieldLabel + ' already exist in database '' + CHAR(13) + CHAR(10) FROM ' + @strTableUploadName + ' A INNER JOIN ' + @Modulename + ' B ON TRY_CONVERT(DATETIME, A.'
							+ @FieldName + ', ' + CAST(@dateformat as VARCHAR) + ') =  TRY_CONVERT(DATETIME, b.' + @FieldName  + ', ' + CAST(@dateformat as VARCHAR) + ') WHERE A.nawa_Action = ''Update'' AND A.nawa_userid=''' + @userid + ''' AND A.'
							+ @primaryKeyFieldName + ' != B.' + @primaryKeyFieldName
						END
						ELSE 
						BEGIN
							SET @sql =
							'UPDATE ' + @strTableUploadName + ' SET KeteranganError = KeteranganError + ''Line '' + CONVERT(VARCHAR(20), nawa_recordnumber) + '' : '
							+ @FieldLabel + ' already exist in database '' + CHAR(13) + CHAR(10) FROM ' + @strTableUploadName + ' A INNER JOIN ' + @Modulename + ' B ON TRY_CAST(A.'
							+ @FieldName + ' as '+@FieldTypeSQLName+') =  b.' + @FieldName  + ' WHERE A.nawa_Action = ''Update'' AND A.nawa_userid=''' + @userid + ''' AND A.'
							+ @primaryKeyFieldName + ' != B.' + @primaryKeyFieldName
							
						END
						
						--PRINT (@sql);
						EXEC (@sql);
						
						--Randi 04/05/2022 Same with keterangan error "already exist in Row(x)"
						--SET @sql =
						--	'UPDATE A SET KeteranganError = A.KeteranganError + ''Line '' + CONVERT(VARCHAR(20), A.nawa_recordnumber) + '' : '
						--	+ @FieldLabel + ' already exist in this excel '' + CHAR(13) + CHAR(10) FROM ' + @strTableUploadName + ' A INNER JOIN ' + @strTableUploadName + ' B ON A.'
						--	+ @FieldName + ' =  b.' + @FieldName  + ' WHERE A.nawa_Action IN (''Insert'', ''Update'') AND A.nawa_userid=''' + @userid + ''' AND B.nawa_Action IN (''Insert'', ''Update'') AND B.nawa_userid=''' + @userid + ''' AND A.PK_upload_ID != B.PK_upload_ID'
						--PRINT (@sql);
						--EXEC (@sql);

					END;
                END;
                ELSE
                BEGIN
				-- Dandi Juliandi 25/03/2024 : fix(S065) : Duplicate message when dropdown is unique
	     --           SET @sql = 'UPDATE ' + @strTableUploadName +' SET    KeteranganError = KeteranganError + ''Line '' + CONVERT(VARCHAR(20), nawa_recordnumber)         + '' : '
	     --               + @FieldLabel +
	     --               ' Already Exist in database  '' + CHAR(13) + CHAR(10) FROM   ' +
	     --               @strTableUploadName +'        INNER JOIN (                 SELECT A.PK_upload_ID                 FROM   '
						--+ @strTableUploadName +' A                        INNER JOIN ' +
	     --               @Modulename + ' B                             ON  SUBSTRING( A.' + @FieldName +
	     --               ',CHARINDEX(''('',A.' + @FieldName + ') +1 ,CHARINDEX('')'',A.' + @FieldName + ')-2)'
	     --      + ' = CONVERT(VARCHAR(MAX), b.' + @FieldName + ')' +
	     --               '           WHERE  a.nawa_Action = ''Insert''    AND a.nawa_userid='''
	     --               + @userid +
	     --               '''         )notvalid             ON  notvalid.PK_upload_ID = ' +
	     --               @strTableUploadName +'.PK_upload_ID'
      --              EXEC (@sql);


					-- Dandi Juliandi 24/01/2024 : Comment Code for fix error when upload data using Update or Delete Type
					-- Notes : Validasi ini berfungsi untuk mengecek nilai pada dropdown jika ada di database
	    --            SET @sql = 'UPDATE ' + @strTableUploadName +' SET    KeteranganError = KeteranganError + ''Line '' + CONVERT(VARCHAR(20), nawa_recordnumber)         + '' : '
	    --                + @FieldLabel +
	    --                ' Not Exist in database  '' + CHAR(13) + CHAR(10) FROM   ' +
	    --                @strTableUploadName +'        LEFT JOIN (                 SELECT A.PK_upload_ID                 FROM   '
	    --                + @strTableUploadName +' A                        LEFT JOIN ' +
	    --                @Modulename + ' B                             ON  SUBSTRING( A.' + @FieldName +
	    --                ',CHARINDEX(''('',A.' + @FieldName + ') +1 ,CHARINDEX('')'',A.' + @FieldName + ')-2)'
	    --                + ' = b.' + @FieldName +
	    --                '                 WHERE  a.nawa_Action in (''Delete'',''Update'')     AND a.nawa_userid='''
	    --                + @userid +
	    --                '''         )notvalid             ON  notvalid.PK_upload_ID <> ' +
	    --                @strTableUploadName +'.PK_upload_ID' +
	    --                ' WHERE  nawa_Action in (''Delete'',''Update'') AND nawa_userid=''' + @userid +
					--	''' and notvalid.PK_upload_ID is null '
     --               EXEC (@sql);  
			

					IF(@IsPrimaryKey != 1)
					BEGIN
					--  Dandi Juliandi 24/01/2024 : Add TRY_CAST for dropdown have have invalid value and sitring type
						SET @sql =
							'UPDATE ' + @strTableUploadName + ' SET KeteranganError = KeteranganError + ''Line '' + CONVERT(VARCHAR(20), nawa_recordnumber) + '' : '
							+ @FieldLabel + ' already exist in database '' + CHAR(13) + CHAR(10) FROM ' + @strTableUploadName + ' A INNER JOIN ' + @Modulename + ' B ON SUBSTRING( A.' + @FieldName +
							',CHARINDEX(''('',A.' + @FieldName + ') +1 ,CHARINDEX('')'',A.' + @FieldName + ')-2)' + ' =  TRY_CAST(b.' + @FieldName  + ' AS VARCHAR(MAX)) WHERE A.nawa_Action IN (''Insert'', ''Update'') AND A.nawa_userid=''' + @userid + ''' AND A.'
							+ @primaryKeyFieldName + ' != B.' + @primaryKeyFieldName
						EXEC (@sql);
					END;
                END;
            END;
        END;

		--Datetime
        IF @FK_FieldType_ID = 10
        BEGIN
            IF @Required = 1
            BEGIN
	            --SET @sql = 'UPDATE ' + @strTableUploadName +' SET KeteranganError = KeteranganError + ''Line '' + CONVERT(VARCHAR(20), nawa_recordnumber) + '' : '
	            --    + @FieldLabel + ' is not a valid date  '' + CHAR(13) + CHAR(10) FROM ' + @strTableUploadName +' INNER JOIN ( SELECT A.PK_upload_ID FROM ' + @strTableUploadName +' A WHERE (a.nawa_Action = ''Insert'' OR a.nawa_Action=''Update'' ) AND a.nawa_userid=''' +
	            --    @userid + ''' AND dbo.[ufn_GetDateValue](' + @FieldName + ',''' + @dateformat +
	            --    ''') is null  )notvalid ON notvalid.PK_upload_ID = ' + @strTableUploadName +'.PK_upload_ID'

				SET @sql = 'UPDATE ' + @strTableUploadName +' SET KeteranganError = KeteranganError + ''Line '' + CONVERT(VARCHAR(20), nawa_recordnumber) + '' : '
	                + @FieldLabel + ' is not a valid date  '' + CHAR(13) + CHAR(10) WHERE (nawa_Action = ''Insert'' OR nawa_Action=''Update'' ) AND nawa_userid=''' +
	                @userid + ''' AND TRY_CONVERT(DATETIME, ' + @FieldName + ',' + CAST(@dateformat AS VARCHAR) + ') is null'
            END;
            ELSE
            BEGIN
                --ada update kalau null dianggap valid kalau ngak required
	            --SET @sql = 'UPDATE ' + @strTableUploadName +' SET KeteranganError = KeteranganError + ''Line '' + CONVERT(VARCHAR(20), nawa_recordnumber) + '' : '
	            --    + @FieldLabel + ' is not a valid date  '' + CHAR(13) + CHAR(10) FROM ' + @strTableUploadName +' INNER JOIN ( SELECT A.PK_upload_ID FROM ' + @strTableUploadName +' A WHERE (a.nawa_Action = ''Insert'' OR a.nawa_Action=''Update'' ) AND a.nawa_userid=''' +
	            --    @userid + ''' AND dbo.[ufn_GetDateValue](' + @FieldName + ',''' + @dateformat +
	            --    ''') is null  AND ( a.' + @FieldName
	            --    + '<>''null'' and a.' + @FieldName + '<>'''' ))notvalid ON notvalid.PK_upload_ID = ' + @strTableUploadName +'.PK_upload_ID'

				SET @sql = 'UPDATE ' + @strTableUploadName +' SET KeteranganError = KeteranganError + ''Line '' + CONVERT(VARCHAR(20), nawa_recordnumber) + '' : '
	                + @FieldLabel + ' is not a valid date  '' + CHAR(13) + CHAR(10) WHERE (nawa_Action = ''Insert'' OR nawa_Action=''Update'' ) AND nawa_userid=''' +
	                @userid + ''' AND TRY_CONVERT(DATETIME, ' + @FieldName + ',' + CAST(@dateformat AS VARCHAR) + ') is null AND ( ' + @FieldName + '<>''null'' and ' + @FieldName + '<>'''' )'
            END;
			--PRINT 'StartDate';
            --PRINT (@sql);
            EXEC (@sql);
        END;

		--Varchar
        IF @FK_FieldType_ID = 9
        BEGIN
			-- jika tipenya RihTExt datanya dijadikan NULL
			IF @FK_ExtType_ID = 16
			BEGIN
				SET @sql = 'UPDATE ' + @strTableUploadName + ' SET ' + @FieldName + '= NULL ' + ' WHERE (nawa_Action = ''Insert'' OR nawa_Action=''Update'' ) AND nawa_userid=''' + @userid + ''''
                EXEC (@sql);
			END
            --cek maxlength jika terisi
            IF @SizeField > 0
            BEGIN
	    SET @sql = 'UPDATE ' + @strTableUploadName +' SET KeteranganError = KeteranganError + ''Line '' + CONVERT(VARCHAR(20), nawa_recordnumber) + '' : '
	                + @FieldLabel + ' Max Length is ' + CONVERT(VARCHAR(20), @SizeField) +
	                ' char   '' + CHAR(13)  + CHAR(10) FROM ' + @strTableUploadName +' INNER JOIN ( SELECT A.PK_upload_ID FROM ' + @strTableUploadName +' A WHERE (a.nawa_Action = ''Insert'' OR a.nawa_Action=''Update'' ) AND a.nawa_userid=''' +
	                @userid + ''' AND len(LTRIM(rtrim(a.' + @FieldName + ')))>' + CONVERT(VARCHAR(20), @SizeField) +
	                ' )notvalid ON notvalid.PK_upload_ID = ' + @strTableUploadName +'.PK_upload_ID'

                EXEC (@sql);
            END;
        END;

		--Boolean
		IF @FK_FieldType_ID = 13
		BEGIN
			SET @sql = 'UPDATE ' + @strTableUploadName +' SET KeteranganError = KeteranganError + ''Line '' + CONVERT(VARCHAR(20), nawa_recordnumber) + '' : '
	            + @FieldLabel + ' can only be True or False  '' + CHAR(13) + CHAR(10) WHERE (nawa_Action = ''Insert'' OR nawa_Action=''Update'' ) AND nawa_userid=''' +
	            @userid + ''' AND ' + @FieldName + ' != ''True'' AND ' + @FieldName + ' != ''False'' AND ' +  @FieldName + ' != '''' '

            EXEC (@sql);
		END;

		--Numeric
        IF @FK_FieldType_ID = 1
           OR @FK_FieldType_ID = 2
           OR @FK_FieldType_ID = 3
           OR @FK_FieldType_ID = 4
           OR @FK_FieldType_ID = 5
           OR @FK_FieldType_ID = 6
           OR @FK_FieldType_ID = 7
           OR @FK_FieldType_ID = 8
        BEGIN
            --            UPDATE Sales_upload
            --SET    KeteranganError = KeteranganError + 'Line ' + CONVERT(VARCHAR(20), nawa_recordnumber)
            --       + ' : Sales Name range must  Huge Fixed Number data with range from -2^63 (-9.223.372.036.854.775.808) through 2^63-1 (9.223.372.036.854.775.807).' + CHAR(13) + CHAR(10)
            --FROM   Sales_upload
            --       INNER JOIN (
            --                SELECT A.PK_upload_ID
            --                FROM   Sales_Upload A

            --                WHERE  (a.nawa_Action = 'Insert' OR a.nawa_Action='Update' ) AND a.nawa_userid='adm01'
            --                AND  try_cast(a.SalesName AS DECIMAL(28,2)) IS NULL
            --                AND a.SalesName IS NOT null
            --            )notvalid
            --            ON  notvalid.PK_upload_ID = Sales_upload.PK_upload_ID
	        SET @sql = 'UPDATE ' + @strTableUploadName +' ' + CHAR(10)
	            + 'SET    KeteranganError = KeteranganError + ''Line '' + CONVERT(VARCHAR(20), nawa_recordnumber)  ' +
	            CHAR(10)
	            + '       + '' : ' + @FieldLabel + ' range must  ' + @FieldTypeDescription +
	            '   '' + CHAR(13) + CHAR(10) ' + CHAR(10)
	            + 'FROM   ' + @strTableUploadName +' ' + CHAR(10)
	            + '       INNER JOIN ( ' + CHAR(10)
	            + '                SELECT A.PK_upload_ID ' + CHAR(10)
	            + '                FROM   ' + @strTableUploadName +' A ' + CHAR(10)
	            + '                        ' + CHAR(10)
	            + '                WHERE  (a.nawa_Action = ''Insert'' OR a.nawa_Action=''Update'' ) AND a.nawa_userid='''
	            + @userid + ''' ' + CHAR(10)
				+ '                AND  try_cast(a.' + @FieldName + ' AS '+ @FieldTypeSQLName +') IS NULL  ' + CHAR(10)
	            + '       AND a.' + @FieldName + ' IS NOT null    ' + CHAR(10)
	            + '            )notvalid ' + CHAR(10)
	      + '            ON  notvalid.PK_upload_ID = ' + @strTableUploadName +'.PK_upload_ID'

            --PRINT (@sql);
            EXEC (@sql);
        END;

		--TimeField
		IF @FK_ExtType_ID = 12
        BEGIN
	        SET @sql = 'UPDATE ' + @strTableUploadName +' ' + CHAR(10)
	            + 'SET    KeteranganError = KeteranganError + ''Line '' + CONVERT(VARCHAR(20), nawa_recordnumber)  ' +
	           CHAR(10)
	            + '       + '' : ' + @FieldLabel + ' Format must  ' + @defaulttimeformat +
	            '   '' + CHAR(13) + CHAR(10) ' + CHAR(10)
	            + 'FROM   ' + @strTableUploadName +' ' + CHAR(10)
	            + '       INNER JOIN ( ' + CHAR(10)
	            + '            SELECT A.PK_upload_ID ' + CHAR(10)
	            + '                FROM   ' + @strTableUploadName +' A ' + CHAR(10)
	            + '                        ' + CHAR(10)
	            +
	            '                WHERE  (a.nawa_Action = ''Insert'' OR a.nawa_Action=''Update'' ) AND a.nawa_userid='''
	            +
	            @userid + ''' ' + CHAR(10)
	            + '                AND  try_cast(a.' + @FieldName + ' AS tIME ) IS NULL  ' + CHAR(10)
	            + '                AND a.' + @FieldName + ' IS NOT null    ' + CHAR(10)
	            + '            )notvalid ' + CHAR(10)
	            + '            ON  notvalid.PK_upload_ID = ' + @strTableUploadName +'.PK_upload_ID'
            --EXEC (@sql);
        END;

        FETCH FROM my_cursor
        INTO @PK_ModuleField_ID,
             @FK_Module_ID,
             @FieldName,
             @FieldLabel,
             @Sequence,
             @Required,
             @IsPrimaryKey,
             @IsUnik,
             @IsShowInView,
             @FK_FieldType_ID,
             @SizeField,
             @FK_ExtType_ID,
             @TabelReferenceName,
             @TabelReferenceNameAlias,
             @TableReferenceFieldKey,
             @TableReferenceFieldDisplayName,
             @TableReferenceFilter,
             @IsUseRegexValidation,
			 @FieldNameParent,
			 @FilterCascade,
             @Modulename,
             @FieldTypeDescription,
			 @FieldTypeSQLName,
			 @PK_FieldType_ID
    END;

    IF LEN(@listFieldatas) > 0
        SET @listFieldatas = SUBSTRING(@listFieldatas, 1, LEN(@listFieldatas) - 1);

    IF LEN(@listfieldbawah) > 0
        SET @listfieldbawah = SUBSTRING(@listfieldbawah, 1, LEN(@listfieldbawah) - 1);

    DECLARE @roleid INT;
    DECLARE @filteraccess VARCHAR(8000);

    SELECT @roleid = m.FK_MRole_ID
    FROM MUser m
    WHERE m.UserID = @userid;

    IF EXISTS
    (
        SELECT 1
        FROM INFORMATION_SCHEMA.TABLES
        WHERE TABLE_TYPE = 'BASE TABLE'
              AND TABLE_NAME = 'DataAccess'
    )
        SELECT TOP 1
               @filteraccess = xx.FilterData
        FROM
        (
            SELECT REPLACE(da.FilterData, '@userid', @userid) AS FilterData,
                   da.FK_Role_ID
            FROM DataAccess da
            WHERE da.FK_Role_ID = @roleid
                  AND da.FK_Module_ID = @Moduleid
                  AND da.FK_ModuleAction = 7
            UNION
            SELECT REPLACE(da.FilterData, '@userid', @userid) AS FilterData,
                   da.FK_Role_ID
            FROM DataAccess da
            WHERE da.FK_Role_ID = 0
                  AND da.FK_Module_ID = @Moduleid
                  AND da.FK_ModuleAction = 7
        ) xx
        ORDER BY FK_Role_ID DESC;
    ELSE
        SELECT @filteraccess = '';

    IF LEN(@filteraccess) > 0
    BEGIN
        SET @filteraccess = '  WHERE  NOT ( ' + @filteraccess + ' ) ';

	    SET @listfieldbawah += ',nawa_userid as Createdby,nawa_userid as LastUpdateBy'
	    SET @sql = ' ' + CHAR(10)
	        + 'UPDATE ' + @strTableUploadName +' ' + CHAR(10)
	        + 'SET    KeteranganError = KeteranganError + ''Line '' + CONVERT(VARCHAR(20), nawa_recordnumber)  ' + CHAR(10)
	        +
	        '       + '' : You dont have Right to Add or Edit this Record.Please Contact Your Administrator  '' + CHAR(13) + CHAR(10) '
	        + CHAR(10)
	        + 'FROM   ' + @strTableUploadName +' ' + CHAR(10)
	        + 'INNER JOIN ( ' + CHAR(10)
	        + '  SELECT ' + @strmodulename + '.PK_upload_ID , ' + @listFieldatas + CHAR(10)
	        + ' FROM ( ' + CHAR(10)
	        + '  SELECT PK_upload_ID, ' + @listfieldbawah + CHAR(10)
	        + '  FROM ' + @strTableUploadName +'  ' + CHAR(10)
	        + ' )' + @strmodulename + @filteraccess + CHAR(10)
	        + '  )invalid  ON invalid.PK_upload_ID = ' + @strTableUploadName +'.PK_upload_ID ' + CHAR(10)
	        + '  '
        EXEC (@sql);
    END;

    CLOSE my_cursor;
    DEALLOCATE my_cursor;

	--Cek Akses Module
	EXEC usp_validateMenuAccess @FK_Module_ID,@strTableUploadName,@groupMenuID, @userid

    --tambah validasi untuk onereporting kalau mengandung ors_forminfo dan actionnya delete, maka statussubmittednya harus blank

    DECLARE @sqlvalidatedeletebefore VARCHAR(MAX);
    /*SET @sqlvalidatedeletebefore = ' ' + char(10)
 + 'if ( (SELECT COUNT(1) FROM INFORMATION_SCHEMA.TABLES AS t WHERE t.TABLE_NAME=''ORS_FormInfo'')=1) ' + char(10)
 + 'begin  ' + char(10)
 + 'DECLARE @JmlInOrsFormInfo INT  ' + char(10)
 + 'SELECT @JmlInOrsFormInfo=COUNT(1) FROM ORS_FormInfo AS ofi WHERE ofi.FK_Module_ID= '+CONVERT(VARCHAR(50),@Moduleid) + char(10)
 + 'IF @JmlInOrsFormInfo=1  ' + char(10)
 + ' ' + char(10)
 + 'BEGIN ' + char(10)
 + '   ' + char(10)
 + 'UPDATE '+@Modulename+'_upload  ' + char(10)
 + 'SET  KeteranganError = KeteranganError + ''Line '' + CONVERT(VARCHAR(20), nawa_recordnumber) + '' : Delete is not allowed for summited data  '' + CHAR(13) + CHAR(10) ' + char(10)
 + 'WHERE nawa_Action=''delete'' AND nawa_userid=''sysadmin''   ' + char(10)
 + 'AND isnull(SubmitStatus,'''')<>'''' ' + char(10)
 + '   ' + char(10)
 + 'END  ' + char(10)
 + 'END '
 EXEC(@sqlvalidatedeletebefore)  */

    --IF EXISTS
    --(
    --    SELECT 1
    --    FROM INFORMATION_SCHEMA.TABLES AS t
    --    WHERE t.TABLE_NAME = 'ORS_FormInfo'
    --)
    --BEGIN
    --    IF EXISTS (SELECT 1 FROM ORS_FormInfo fi WHERE fi.FK_Module_ID = @Moduleid)
    --    BEGIN
    --        SET @sqlvalidatedeletebefore
    --            = +'UPDATE ' + @Modulename + '_upload  ' + CHAR(10)
    --              + 'SET  KeteranganError = KeteranganError + ''Line '' + CONVERT(VARCHAR(20), nawa_recordnumber) + '' : Delete is not allowed for submitted data  '' + CHAR(13) + CHAR(10) '
    --              + CHAR(10) + 'WHERE nawa_Action=''delete'' AND nawa_userid=''sysadmin''   ' + CHAR(10)
    --              + 'AND isnull(SubmitStatus,'''')<>'''' ' + CHAR(10);
    --        EXEC (@sqlvalidatedeletebefore);

    --        DECLARE @checkreportdate VARCHAR(5) = '',
    --                @fielnameuniqant VARCHAR(500) = '';


    --        SELECT @fielnameuniqant = fieldname,
    --               @checkreportdate = acuk.CheckReportDate
    --        FROM ANT_CheckUniqueKey AS acuk
    --        WHERE acuk.TableName = @Modulename;

    --        IF @checkreportdate = 'No'
    --           AND @fielnameuniqant <> ''
    --        BEGIN
    --            DECLARE @sqlant VARCHAR(MAX);
    --            SET @sqlant
    --                = ' ' + CHAR(10) + '        UPDATE ' + @Modulename + '_upload ' + CHAR(10)
    --                  + '   SET    KeteranganError = KeteranganError + '' LIne '' + CONVERT(VARCHAR(20), nawa_recordnumber) + '' Field '
    --                  + @fielnameuniqant + ' must be unique '' ' + CHAR(10) + '          FROM ' + @Modulename
    --                  + '_upload xx  ' + CHAR(10) + '          INNER JOIN ( ' + CHAR(10) + '              ' + CHAR(10)
    --                  + '   select COUNT(1) jml,' + @fielnameuniqant + '  ' + CHAR(10) + '     From ' + @Modulename
    --                  + '_upload     ' + CHAR(10) + '   WHERE   nawa_userid=''' + @userid + ''' ' + CHAR(10)
    --                  + '   GROUP BY ' + @fielnameuniqant + ' ' + CHAR(10) + '   HAVING COUNT(1)>1 ' + CHAR(10)
    --                  + '    ' + CHAR(10) + '          )yy ON xx.' + @fielnameuniqant + '=yy.' + @fielnameuniqant
    --                  + '  ' + CHAR(10) + '            WHERE xx.nawa_userid=''' + @userid + '''';
    --            --PRINT '@sqlant'+ @sqlant
    --            EXEC (@sqlant);
    --        END;
    --        ELSE
    --        BEGIN
    --            IF @fielnameuniqant <> ''
    --            BEGIN
    --                DECLARE @sqlntuniqwithreportdate VARCHAR(MAX);
    --                SET @sqlntuniqwithreportdate
    --                    = '   UPDATE ' + @Modulename + '_upload ' + CHAR(10)
    --                      + '           SET    KeteranganError = KeteranganError + '' LIne '' + CONVERT(VARCHAR(20), nawa_recordnumber) +  '
    --                      + CHAR(10) + '                  '' Field ' + @fielnameuniqant
    --                      + ' and Report_Date must be unique '' ' + CHAR(10) + '           FROM   ' + @Modulename
    --                      + '_upload xx ' + CHAR(10) + '                  INNER JOIN ( ' + CHAR(10)
    --                      + '                           SELECT COUNT(1) jml, ' + CHAR(10)
    --                      + '       ' + @fielnameuniqant + ', ' + CHAR(10)
    --                      + '                                  Report_Date ' + CHAR(10)
    --                      + '                           FROM   ' + @Modulename + '_upload ' + CHAR(10)
    --                      + '                           WHERE  nawa_userid = ''' + @userid + ''' ' + CHAR(10)
    --                      + '                           GROUP BY ' + CHAR(10) + '                                  '
    --                      + @fielnameuniqant + ', ' + CHAR(10) + '                                  Report_Date '
    --                      + CHAR(10) + '                           HAVING COUNT(1) > 1 ' + CHAR(10)
    --                      + '                       )yy ' + CHAR(10) + '                       ON  xx.'
    --                      + @fielnameuniqant + ' = yy.' + @fielnameuniqant + ' ' + CHAR(10)
    --                      + '                       AND xx.Report_Date = yy.Report_Date ' + CHAR(10)
    --                      + '           WHERE  xx.nawa_userid = ''' + @userid + '''';
    --                --PRINT  @sqlntuniqwithreportdate
    --                EXEC (@sqlntuniqwithreportdate);
    --            END;


    --        END;
    --    END;
    --END;

    /* Tambah untuk Insert jika NULL, ID Identity generate otomatis*/
    IF EXISTS
    (
        SELECT 1
        FROM ModuleField mf
        WHERE mf.FK_Module_ID = @FK_Module_ID
              AND mf.IsPrimaryKey = 1
              AND
              (
                  mf.FK_FieldType_ID = 12
                  OR mf.FK_FieldType_ID = 15
              )
    )
    BEGIN
        DECLARE @FieldPrimary AS VARCHAR(MAX) = '';
		--DECLARE @RandNumber AS INT(250) = select ( 0 - floor(rand(checksum(newid())) * 2432) );
        SELECT TOP 1
               @FieldPrimary = mf.FieldName
        FROM ModuleField mf
        WHERE mf.FK_Module_ID = @FK_Module_ID
              AND mf.IsPrimaryKey = 1
              AND
              (
                  mf.FK_FieldType_ID = 12
                  OR mf.FK_FieldType_ID = 15
              );

	   -- SET @sql = 'UPDATE ' + @strTableUploadName +' SET ' + @FieldPrimary + ' = (SELECT ISNULL(MAX(' + @FieldPrimary +
	     --   '), 0) FROM ' + @strmodulename +
	       -- ') + ISNULL(nawa_recordnumber, 0)
			SET @sql = 'UPDATE ' + @strTableUploadName +' SET ' + @FieldPrimary + ' = (select ( 0 - floor(rand(checksum(newid())) * 2432) ))
			WHERE nawa_Action=''insert'' AND nawa_userid=''' + @userid + '''
			AND ISNULL('  + @FieldPrimary + ', '''') = ''''';
        --PRINT (@sql)
        EXEC (@sql);
    END;

    --if (@Moduleid not in (8769))
    DECLARE @IntCount AS INT;
    SELECT @IntCount = COUNT(1)
    FROM ValidationParameter vp
    WHERE vp.TableName = @Moduleid
          AND vp.Active = 1;

    IF @IntCount > 0
    BEGIN
        EXEC [usp_ValidateUploadFormModule_FromUpload] @Moduleid, @userid;
    END;
END;