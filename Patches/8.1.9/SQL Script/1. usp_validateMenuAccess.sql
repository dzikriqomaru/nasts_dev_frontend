CREATE OR ALTER PROCEDURE [dbo].[usp_validateMenuAccess]
@FK_Module_ID BIGINT = 0,
@strTableUploadName VARCHAR(250),
@FK_MGroupMenu_ID BIGINT = 0,
@userID VARCHAR(250)
AS
BEGIN
	DECLARE @bAdd bit,
			@bEdit bit,
			@bDelete bit ,
			@sql VARCHAR(MAX) = ''
	
	SELECT 
	@bAdd = bAdd,
	@bEdit = bEdit,
	@bDelete = bDelete
	FROM MGroupMenuAccess WHERE FK_GroupMenu_ID = @FK_MGroupMenu_ID AND FK_Module_ID = @FK_Module_ID
	
	IF @bAdd = 0
	BEGIN
		SET @sql = 'UPDATE ' + @strTableUploadName + ' SET KeteranganError =KeteranganError + ''Line ''+ convert(Varchar(20),nawa_recordnumber) +'' : '+
		@userID + ' does not have access to insert on this module'' WHERE nawa_userid =''' + @userID + ''' and nawa_Action = ''Insert'' '
		EXEC(@sql)
	END

	IF @bEdit = 0
	BEGIN
		SET @sql = 'UPDATE ' + @strTableUploadName + ' SET KeteranganError =KeteranganError + ''Line ''+ convert(Varchar(20),nawa_recordnumber) +'' : '+
		@userID + ' does not have access to update on this module''  WHERE nawa_userid =''' + @userID + ''' and nawa_Action = ''Update'' '
		EXEC(@sql)
	END

	
	IF @bDelete = 0
	BEGIN
		SET @sql = 'UPDATE ' + @strTableUploadName + ' SET KeteranganError =KeteranganError + ''Line ''+ convert(Varchar(20),nawa_recordnumber) +'' : '+
		@userID + ' does not have access to delete on this module'' WHERE nawa_userid =''' + @userID + ''' and nawa_Action = ''Delete'' '
		EXEC (@sql)
	END
END