﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NawaDataDAL;
using NawaDataDAL.Models;
using NawaDataBLL;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;

namespace NawaDataAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SystemParameterController : ControllerBase
    {
        [Route("getSysParam")]
        [HttpGet]
        public ActionResult GetSysParam(int SysParamID)
        {
            try
            {
                AppSystemParameter sysParam = SystemParameterBLL.GetAppSystemParameter(SysParamID);
                return Ok(sysParam);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [Route("GetMultipleSysParam")]
        [HttpGet]
        public ActionResult GetMultipleSysParam(string sysParamID)
        {
            try
            {
                //Converted into int array to prevent query injection
                int[] idArray = sysParamID.Split(',').Select(n => Convert.ToInt32(n)).ToArray();
                List<AppSystemParameter> listSysParam = SystemParameterBLL.GetMultipleAppSystemParameter(idArray);

                return Ok(listSysParam);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("getSysParamPublic")]
        [HttpGet]
        public ActionResult GetSysParamPublic(int SysParamID)
        {
            try
            {
                AppSystemParameter sysParam = SystemParameterBLL.GetAppSystemParameterPublic(SysParamID);
                return Ok(sysParam);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetMultipleSysParamPublic")]
        [HttpGet]
        public ActionResult GetMultipleSysParamPublic(string sysParamID)
        {
            try
            {
                //Converted into int array to prevent query injection
                int[] idArray = sysParamID.Split(',').Select(n => Convert.ToInt32(n)).ToArray();
                List<AppSystemParameter> listSysParam = SystemParameterBLL.GetMultipleAppSystemParameterPublic(idArray);

                return Ok(listSysParam);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [Route("getSysParamPickList")]
        [HttpGet]
        public ActionResult GetDataSysParamPickList(string SettingName)
        {
            try
            {
                List<SystemParameterPickList> sysParamPickList = SystemParameterBLL.GetSystemParameterPickList(SettingName);
                return Ok(sysParamPickList);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [Route("postSysParam")]
        [HttpPost]
        public ActionResult PostSysParam(SystemParameterBLL.SaveParameter SysParam)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MUser currentUser = new MUser()
                {
                    UserID = identity.FindFirst("UserID").Value,
                    FK_MRole_ID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value),
                    PK_MUser_ID = Convert.ToInt32(identity.FindFirst("PK_MUser_ID").Value)
                };

                #region Check if Exists in module approval
                Module mod = ModuleBLL.GetModuleByName("systemparameter");
                if (SysParam.Mode != EActionType.Insert)
                {
                    bool IsExistsInModuleApproval = ModuleBLL.IsExistsInModuleApproval(SysParam.PK_SystemParameter_ID, mod.PK_Module_ID);
                    if (IsExistsInModuleApproval)
                        return StatusCode(500, "Data Already Exists in Pending Approval");
                }
                #endregion

                string successMessage = "";
                if (currentUser.FK_MRole_ID == 1)
                {
                    SystemParameterBLL.SaveSystemParameterWithoutApproval(SysParam, currentUser.UserID);
                    successMessage = "System Parameter Data is saved into Database successfully.";
                }
                else
                {
                    SystemParameterBLL.SaveSystemParameterWithApproval(SysParam, currentUser);
                    successMessage = "System Parameter Data is saved into pending approval.";
                }

                return Ok(successMessage);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetModuleData")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetModuleData(int page, string orderBy, string order, string search, string filter, bool isAdv)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;                

                int rowCount = 0;
                List<NawaDataDAL.DynamicDictionary> moduleData = NawaDataBLL.SystemParameterBLL.GetModuleData(
                    page, 
                    orderBy, 
                    order, 
                    search, 
                    filter,
                    isAdv,
                    null,
                    out rowCount, 
                    identity.FindFirst("FK_MRole_ID").Value, 
                    identity.FindFirst("UserID").Value,
                    null
                );

                return Ok(new { count = rowCount, rows = moduleData });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetModuleData")]
        [Authorize(Policy="ApplicationLock")]
        [HttpPost]
        public ActionResult GetModuleData([FromBody] GenericPagingFilterPostBody body)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;

                int rowCount = 0;
                List<NawaDataDAL.DynamicDictionary> moduleData = NawaDataBLL.SystemParameterBLL.GetModuleData(
                    body.page,
                    body.orderBy,
                    body.order,
                    body.search,
                    body.filter,
                    body.isAdv,
                    body.fields,
                    out rowCount,
                    identity.FindFirst("FK_MRole_ID").Value,
                    identity.FindFirst("UserID").Value,
                    body.jsonFilter
                );

                return Ok(new { count = rowCount, rows = moduleData });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        //[Authorize(Policy="ApplicationLock")]
        [HttpGet]
        [Route("getDictionary")]
        public ActionResult GetSystemDictionary()
        {
            try
            {
                List<SystemDictionary> listDictionary = SystemParameterBLL.GetSystemDictionary();
                //(for multi language) temporary solution to adapt to db changes (if it changes at all)
                HelperFunctions.clearDictionaryCache();
                return Ok(listDictionary);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetSysParamDateTimeFormat")]
        [HttpGet]
        public ActionResult GetSysParamDateTimeFormat(string multiSysParamID, string parseAs)
        {
            try
            {
                List<SysParamDateTimeFormat> formatList = SystemParameterBLL.GetSysParamDateTimeFormat(multiSysParamID, parseAs);
                return Ok(formatList);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetModuleAccess")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetModuleAccess(int moduleID)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MGroupMenuAccess menuAccess = CommonBLL.GetModuleAccess(moduleID, identity.FindFirst("FK_MGroupMenu_ID").Value);

                return Ok(menuAccess);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ActionSystemParameterApproval")]
        [Authorize(Policy="ApplicationLock")]
        [HttpPost]
        public ActionResult ActionSystemParameterApproval(CParameterApproval objParam)
        {
            try
            {
                string msg = "";
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MUser currentUser = new MUser()
                {
                    PK_MUser_ID = Convert.ToInt32(identity.FindFirst("PK_MUser_ID").Value),
                    UserID = identity.FindFirst("UserID").Value,
                    FK_MGroupMenu_ID = Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value)
                };

                
                if (objParam.PK_ModuleApproval_IDSelected != null
                   && objParam.PK_ModuleApproval_IDSelected.Length > 0)
                {
                    foreach (long pk in objParam.PK_ModuleApproval_IDSelected)
                    {
                        if (!CommonBLL.CheckMenuAccessByApprovalID(currentUser.FK_MGroupMenu_ID, pk, EActionType.Approval))
                        {
                            return StatusCode(500, "You don't have authorization to access this menu.");
                        }

                        if (objParam.ActionApproval == EActionApprovalType.Approve)
                        {
                            SystemParameterBLL.ApproveApproval(pk, currentUser.UserID, currentUser.PK_MUser_ID);
                            msg = "Data approved successfully";
                        }
                        else if (objParam.ActionApproval == EActionApprovalType.Reject)
                        {
                            SystemParameterBLL.RejectApproval(pk, currentUser.UserID, currentUser.PK_MUser_ID);
                            msg = "Data rejected successfully";
                        }
                    }
                }
                else
                {
                    if (!CommonBLL.CheckMenuAccessByApprovalID(currentUser.FK_MGroupMenu_ID, objParam.PK_ModuleApproval_ID, EActionType.Approval))
                    {
                        return StatusCode(500, "You don't have authorization to access this menu.");
                    }

                    if (objParam.ActionApproval == EActionApprovalType.Approve)
                    {
                        SystemParameterBLL.ApproveApproval(objParam.PK_ModuleApproval_ID, currentUser.UserID, currentUser.PK_MUser_ID);
                        msg = "Data approved successfully";
                    }
                    else if (objParam.ActionApproval == EActionApprovalType.Reject)
                    {
                        SystemParameterBLL.RejectApproval(objParam.PK_ModuleApproval_ID, currentUser.UserID, currentUser.PK_MUser_ID);
                        msg = "Data rejected successfully";
                    }
                }

                return Ok(msg);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }
    }
}