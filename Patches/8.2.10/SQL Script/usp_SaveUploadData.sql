SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[usp_SaveUploadData]    
/***********************************************************    
* Procedure description:    
* Date:   12/2/2015     
* Author: Hendra    
*    
* Changes    
* Date        Modified By                Comments    
************************************************************    
* 03/03/2021  Muhammad Fauzan Girindra   Adding ISNULL for KeteranganError
* 04/12/2023 cek dulu sebelum penambahan kolom draft
* 19 Apr 2024 Davin				Field Access Implementation on Insert and Update
************************************************************/    
(@intmode INT, @moduleid INT, @userid VARCHAR(50))    
AS    
BEGIN
	DECLARE @bIsCreateTableUploadForEachUser AS BIT = 0
	DECLARE @strTableUploadName AS VARCHAR(500) = ''
	SELECT @bIsCreateTableUploadForEachUser = sp.SettingValue
	FROM   SystemParameter AS sp
	WHERE  sp.PK_SystemParameter_ID = 53

	DECLARE @activationAccess INT
	SELECT @activationAccess = bActivation FROM MUser
	JOIN MGroupMenuAccess
	ON MUser.FK_MGroupMenu_ID = MGroupMenuAccess.FK_GroupMenu_ID
	where MUser.UserID = @userid AND MGroupMenuAccess.FK_Module_ID = @moduleid

    DECLARE @dateformat VARCHAR(50)    
    SELECT @dateformat= mdf.SQLFormat
        FROM   SystemParameter sp
	INNER JOIN MDateFormat mdf
	ON sp.SettingValue = mdf.PK_DateFormat_ID 
	WHERE  sp.PK_SystemParameter_ID = 14

    DECLARE @PK_Module_ID            INT,    
            @ModuleName              VARCHAR(250),    
            @ModuleLabel             VARCHAR(250),    
            @ModuleDescription       VARCHAR(500),    
            @IsUseDesigner           BIT,    
            @IsUseApproval           BIT,    
            @IsSupportAdd            BIT,    
            @IsSupportEdit           BIT,    
            @IsSupportDelete         BIT,    
            @IsSupportActivation     BIT,    
            @IsSupportView           BIT,    
            @IsSupportUpload         BIT,    
            @UrlAdd                  VARCHAR(550),    
            @UrlEdit                 VARCHAR(550),    
            @UrlDelete               VARCHAR(550),    
            @UrlActivation           VARCHAR(550),    
            @UrlView                 VARCHAR(550),    
            @UrlUpload               VARCHAR(550),    
            @UrlApproval             VARCHAR(550),    
            @UrlApprovalDetail       VARCHAR(550),    
            @IsUseStoreProcedureValidation BIT,    
            @Active                  BIT,    
            @CreatedBy               VARCHAR(50),    
            @LastUpdateBy            VARCHAR(50),    
            @ApprovedBy              VARCHAR(50),    
            @CreatedDate             DATETIME,    
            @LastUpdateDate          DATETIME,    
            @ApprovedDate            DATETIME    
           
    SELECT @PK_Module_ID = PK_Module_ID,    
            @ModuleName              = ModuleName,    
            @ModuleLabel             = ModuleLabel,    
            @ModuleDescription       = ModuleDescription,    
            @IsUseDesigner           = IsUseDesigner,    
            @IsUseApproval           = IsUseApproval,    
            @IsSupportAdd            = IsSupportAdd,    
            @IsSupportEdit           = IsSupportEdit,    
            @IsSupportDelete         = IsSupportDelete,    
            @IsSupportActivation     = IsSupportActivation,    
            @IsSupportView           = IsSupportView,    
            @IsSupportUpload         = IsSupportUpload,    
            @UrlAdd                  = UrlAdd,    
            @UrlEdit                 = UrlEdit,    
            @UrlDelete               = UrlDelete,    
            @UrlActivation           = UrlActivation,    
            @UrlView                 = UrlView,    
            @UrlUpload               = UrlUpload,    
            @UrlApproval             = UrlApproval,    
            @UrlApprovalDetail       = UrlApprovalDetail,    
            @IsUseStoreProcedureValidation = IsUseStoreProcedureValidation,    
            @Active                  = [Active],    
            @CreatedBy               = CreatedBy,    
            @LastUpdateBy            = LastUpdateBy, 
            @ApprovedBy              = ApprovedBy,    
            @CreatedDate             = CreatedDate,    
            @LastUpdateDate          = LastUpdateDate,    
            @ApprovedDate            = ApprovedDate    
    FROM   dbo.Module m    
    WHERE  m.PK_Module_ID = @moduleid    
           
	IF @bIsCreateTableUploadForEachUser = 1
	BEGIN
	    SET @strTableUploadName = @ModuleName + '_Upload_data_' + @userid
	END
	ELSE
	BEGIN
	    SET @strTableUploadName = @ModuleName + '_Upload'
	END
	DECLARE @roleid INT     
	DECLARE @filterPersonal VARCHAR(8000)=''  
	DECLARE @filteraccess VARCHAR(8000)=''  
	DECLARE @filterCombine VARCHAR(8000)=''    
    
	SELECT @filterPersonal=dbo.ufn_GetFilterAdditionalPerUser(@userid, @PK_Module_ID)  
    SELECT @roleid=m.FK_MRole_ID FROM MUser m WHERE m.UserID=@userid    
    
	IF EXISTS (    
        SELECT 1    
        FROM   INFORMATION_SCHEMA.TABLES    
        WHERE  TABLE_TYPE         = 'BASE TABLE'    
               AND TABLE_NAME     = 'DataAccess'    
    )    
		SELECT TOP 1 @filteraccess=xx.FilterData    
		FROM   (    
				SELECT REPLACE(da.FilterData, '@userid', @userid) AS FilterData,    
						da.FK_Role_ID    
				FROM   DataAccess da    
				WHERE  da.FK_Role_ID = @roleid    
						AND da.FK_Module_ID = @Moduleid    
				UNION    
				SELECT REPLACE(da.FilterData, '@userid', @userid) AS FilterData,    
						da.FK_Role_ID    
				FROM   DataAccess da    
				WHERE  da.FK_Role_ID = 0    
						AND da.FK_Module_ID = @Moduleid    
			)xx    
		ORDER BY    
			FK_Role_ID DESC    
	ELSE    
		SELECT @filteraccess= '' ;    
         
	IF LEN(@filteraccess)>0 OR LEN(@filterPersonal) >0   
	SET @filterCombine =' where '   
         
	IF LEN(@filteraccess)>0    
	SET @filterCombine =@filterCombine +' '+@filteraccess  
         
	IF LEN(@filterPersonal)>0  AND LEN(@filteraccess)=0  
	SET @filterCombine =@filterCombine +' '+@filterPersonal  
         
		IF LEN(@filterPersonal)>0  AND LEN(@filteraccess)>0  
	SET @filterCombine =@filterCombine +' and '+@filterPersonal  
           
	DECLARE @sql VARCHAR(MAX)    
	IF @intmode = 1    
	BEGIN    
		SET @sql = 'DELETE FROM ' + @ModuleName + @filterCombine    
		EXEC (@sql)    
	END    
                 
    DECLARE @FK_Module_ID               INT,    
            @FieldName                  VARCHAR(250),    
            @FieldLabel                 VARCHAR(250),    
            @Sequence                   INT,    
            @Required                   BIT,    
            @IsPrimaryKey               BIT,    
            @IsUnik                     BIT,    
            @IsShowInView               BIT,    
            @FK_FieldType_ID            INT,    
            @SizeField                  INT,    
            @FK_ExtType_ID              INT,    
            @TabelReferenceName         VARCHAR(250),    
            @TableReferenceFieldKey     VARCHAR(250),    
            @TableReferenceFieldDisplayName VARCHAR(250),    
            @TableReferenceFilter       VARCHAR(550),    
            @IsUseRegexValidation       BIT,    
            @fields                     VARCHAR(MAX),    
            @fieldDefault               VARCHAR(MAX),    
            @fieldactive                VARCHAR(100),    
            @fieldvaluedefault          VARCHAR(MAX),@FieldUpdate VARCHAR(MAX),    
            @fieldupdatedefault VARCHAR(MAX),    
            @filedupdateactive VARCHAR(100),@fieldprimarykey VARCHAR(500),@fieldInsertValue VARCHAR(MAX)    
                   
                   
		SET @fieldInsertValue=''               
		set @filedupdateactive=''     
		SET @fields = ''    
		SET @fieldDefault = ''    
		SET @fieldactive = ''    
		SET @fieldvaluedefault = ''      
		set @FieldUpdate=''    
		set @fieldupdatedefault =''    
		set @fieldprimarykey=''    
		DECLARE my_cursor CURSOR FAST_FORWARD READ_ONLY     
      FOR    
           SELECT FK_Module_ID,    
                  FieldName,    
                  FieldLabel,    
                  Sequence,    
                  [Required],    
                  IsPrimaryKey,    
                  IsUnik,    
                  IsShowInView,    
                  FK_FieldType_ID,    
                  SizeField,    
                  FK_ExtType_ID,    
                  TabelReferenceName,    
                  TableReferenceFieldKey,    
                  TableReferenceFieldDisplayName,    
                  TableReferenceFilter,    
                  IsUseRegexValidation    
           FROM   dbo.ModuleField mf    
           WHERE  mf.FK_Module_ID = @moduleid  AND mf.FK_FieldType_ID<>14   
				  AND (mf.IsShowInForm=1 OR mf.IsPrimaryKey = 1)
           ORDER BY mf.Sequence  
           
       OPEN my_cursor    
           
       FETCH FROM my_cursor INTO @FK_Module_ID, @FieldName,    
       @FieldLabel, @Sequence, @Required, @IsPrimaryKey,    
       @IsUnik, @IsShowInView, @FK_FieldType_ID, @SizeField,    
       @FK_ExtType_ID, @TabelReferenceName,    
       @TableReferenceFieldKey,    
       @TableReferenceFieldDisplayName,    
	   @TableReferenceFilter, @IsUseRegexValidation    
           
       WHILE @@FETCH_STATUS = 0    
       BEGIN    
          	/*{ ... Cursor logic here ... }*/    
			IF @FK_ExtType_ID=11 CONTINUE    
               
			IF @IsPrimaryKey=1 SET @fieldprimarykey = @FieldName    
			
			/*Cek Akses Field*/
			DECLARE @bAdd bit, @bEdit bit
			EXEC usp_IsFieldHaveAccessOnUpload @FieldName,@FK_Module_ID,@userid,@bAdd = @bAdd OUTPUT,@bEdit = @bEdit  OUTPUT

			/* Set Fields */
			IF @FK_FieldType_ID <> 12 AND @FK_FieldType_ID <> 14 AND @FK_FieldType_ID <> 15 --Exclude Identity, Varbinary, Big Indentity
			BEGIN    
				SET @fields = @fields + @FieldName + ',' + CHAR(10) + CHAR(13)    
			END  

			/*Query Builder For Insert*/
			--Set Field to Insert
			IF @bAdd = 1
			BEGIN
				--Set Value to Insert
				IF @FK_FieldType_ID <> 10 AND @FK_FieldType_ID <> 11 AND @FK_FieldType_ID <> 12 AND @FK_FieldType_ID <> 14 AND @FK_FieldType_ID <> 15 --Exclude DateTime, Reference Table, Identity, Varbinary, Big Identity
				BEGIN    
					SET @fieldInsertValue= @fieldInsertValue + @FieldName + ',' + CHAR(10) + CHAR(13)    
				END 
				ELSE IF @FK_FieldType_ID = 10 --DATETIME
				BEGIN
					IF @Required=1    
					BEGIN
						SET @fieldInsertValue= @fieldInsertValue + @FieldName + ',' + CHAR(10) + CHAR(13)    
					END     
					ELSE
					BEGIN    
						SET @fieldInsertValue= @fieldInsertValue + 'CASE WHEN  '+ @FieldName +'='''' or '+  @FieldName  +' =''NULL'' then null else '+ @FieldName +' end as '+ @FieldName  +' ,' + CHAR(10) + CHAR(13)                              
					END
				END
				ELSE IF @FK_FieldType_ID = 11 --Reference Table
				BEGIN
					IF @FK_ExtType_ID = 15
					BEGIN
						--Tipe Multiple
						SET @fieldInsertValue = @fieldInsertValue + @FieldName + ',' + CHAR(10) + CHAR(13)    
					END
					ELSE
					BEGIN
						--Tipe Combo
						SET @fieldInsertValue = @fieldInsertValue + 'SUBSTRING( ' + @strTableUploadName +'.' + @FieldName +
							', CHARINDEX(''('', ' + @strTableUploadName +'.' + @FieldName +
							',1)+1,  case when CHARINDEX('')'', ' + @strTableUploadName +'.' + @FieldName +
							',1)-2 >0 then CHARINDEX('')'', ' + @strTableUploadName +'.' + @FieldName + ',1)-2 ELSE 0 END ),' +
							CHAR(10) + CHAR(13)
					END
				END
			END

			
			/*Query Builder For Update*/
			IF @bEdit = 1
			BEGIN
				IF @FK_FieldType_ID <> 10 AND @FK_FieldType_ID <> 11 AND @FK_FieldType_ID <> 12 AND @FK_FieldType_ID <> 14 AND @FK_FieldType_ID <> 15 --Exclude DateTime, Reference Table, Identity, Varbinary, Big Identity
				BEGIN    
					SET @FieldUpdate = @FieldUpdate + @FieldName + '=' + @strTableUploadName +'.' + @FieldName + ',' + CHAR(10)+ CHAR(13)
				END
				ELSE IF @FK_FieldType_ID=10         
				BEGIN    
					SET @FieldUpdate = @FieldUpdate + @FieldName + '=dbo.ufn_GetDateValue(' + @strTableUploadName +'.' +
						@FieldName + ',''' + @dateformat + '''),' + CHAR(10) + CHAR(13) 
				END    
				ELSE IF @FK_FieldType_ID = 11    
				BEGIN
					IF @FK_ExtType_ID = 15
					BEGIN
						--Tipe Multiple
						SET @FieldUpdate = @FieldUpdate + @FieldName + '=' + @strTableUploadName +'.' + @FieldName + ',' + CHAR(10)
							+ CHAR(13)
					END
					ELSE
					BEGIN
						--Tipe Combo
						SET @FieldUpdate = @FieldUpdate + @FieldName + '=SUBSTRING( ' + @strTableUploadName +'.' + @FieldName +
							', CHARINDEX(''('', ' + @strTableUploadName +'.' + @FieldName +
							',1)+1,  case when CHARINDEX('')'', ' + @strTableUploadName +'.' + @FieldName +
							',1)-2 >0 then CHARINDEX('')'', ' + @strTableUploadName +'.' + @FieldName + ',1)-2 ELSE 0 END ),' +
							CHAR(10) + CHAR(13)
					END
				END
			END

			FETCH FROM my_cursor INTO @FK_Module_ID, @FieldName,    
			@FieldLabel, @Sequence, @Required, @IsPrimaryKey,    
			@IsUnik, @IsShowInView, @FK_FieldType_ID,    
			@SizeField, @FK_ExtType_ID, @TabelReferenceName,    
			@TableReferenceFieldKey,    
			@TableReferenceFieldDisplayName,    
			@TableReferenceFilter, @IsUseRegexValidation     
       END    
           
       CLOSE my_cursor    
       DEALLOCATE my_cursor    
           
           
           
       IF LEN(@fields) > 0 SET @fields = SUBSTRING(@fields, 1, LEN(@fields) -3)    
       IF LEN(@fieldInsertValue) > 0 SET @fieldInsertValue= SUBSTRING(@fieldInsertValue, 1, LEN(@fieldInsertValue) -3)    

	    --cek dulu ada Field Draft atau tidak
		declare @sqlQuery nvarchar(200)
		DECLARE @rcount NUMERIC(18,0)   
		set @sqlQuery = 'SELECT COUNT (COLUMN_NAME) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = ''' + @ModuleName +''' and COLUMN_NAME =''Draft''' 
		EXECUTE @rcount=sp_executesql @sqlQuery
           
           
       DECLARE @PK_ModuleField_ID_Default     BIGINT,    
               @FieldName_Default             VARCHAR(250),    
               @FieldLabel_Default            VARCHAR(250),    
               @Sequence_Default           INT,    
               @Required_Default              BIT,    
               @IsPrimaryKey_Default          BIT,    
               @IsUnik_Default                BIT,    
               @FK_FieldType_ID_Default       INT,    
               @SizeField_Default             INT,    
               @FK_ExtType_ID_Default         INT    
           
       DECLARE my_cursor CURSOR FAST_FORWARD READ_ONLY     
       FOR    
           SELECT PK_ModuleField_ID,    
                  FieldName,    
                  FieldLabel,    
                  Sequence,    
                  [Required],    
                  IsPrimaryKey,    
                  IsUnik,    
                  FK_FieldType_ID,    
                  SizeField,    
                  FK_ExtType_ID    
           FROM   dbo.ModuleFieldDefault   
		   WHERE FieldName <> iif(@rcount > 0 ,'', 'Draft')
           
       OPEN my_cursor    
           
       FETCH FROM my_cursor INTO @PK_ModuleField_ID_Default, @FieldName_Default, @FieldLabel_Default,    
       @Sequence_Default, @Required_Default, @IsPrimaryKey_Default, @IsUnik_Default,    
       @FK_FieldType_ID_Default, @SizeField_Default, @FK_ExtType_ID_Default    
           
       WHILE @@FETCH_STATUS = 0    
       BEGIN    
           /*{ ... Cursor logic here ... }*/    
               
           SET @fieldDefault = @fieldDefault + @FieldName_Default + ',' + CHAR(10) +CHAR(13)    
               
           IF @FK_FieldType_ID_Default = 13 And @PK_ModuleField_ID_Default <> 9
           BEGIN    
                SET @fieldactive ='isnull('+ @FieldName_Default + ',1),' + CHAR(10) + CHAR(13)  
				
				IF ((@activationAccess = 1 AND @PK_ModuleField_ID_Default = 1) OR @PK_ModuleField_ID_Default <> 1)
				BEGIN
					SET @filedupdateactive = @filedupdateactive + @FieldName_Default + '=isnull(' + @strTableUploadName +'.' +
					@FieldName_Default + ',1),' + CHAR(10) + CHAR(13)
				END
           END     
                
           --cek tipenya varchar(percaya kalau cuma isinya createdby,lastupdateby,approveby)    
           IF @FK_FieldType_ID_Default = 9    
           BEGIN    
              SET @fieldvaluedefault = @fieldvaluedefault + '''' + @userid + ''','+ CHAR(10) + CHAR(13)    
                  
                       
           END    

           IF @PK_ModuleField_ID_Default= 3 or @PK_ModuleField_ID_Default=4    
                     SET @fieldupdatedefault = @fieldupdatedefault +@FieldName_Default+ '=''' + @userid + ''','+ CHAR(10) + CHAR(13)    
                         
               
                   
           --cek tipenya date(percaya kalau isinya createdate,lastupdatedate,approveate)    
           IF @FK_FieldType_ID_Default = 10    
               SET @fieldvaluedefault = @fieldvaluedefault + '''' + CONVERT(VARCHAR(20), GETDATE(), 120)+ ''',' + CHAR(10) + CHAR(13)    
               
            IF @PK_ModuleField_ID_Default= 6 or @PK_ModuleField_ID_Default=7    
                     SET @fieldupdatedefault = @fieldupdatedefault +@FieldName_Default+ '=''' +  CONVERT(VARCHAR(20), GETDATE(), 120) + ''','+ CHAR(10) + CHAR(13)   
					 
			--Tambah Field Draft
            IF @PK_ModuleField_ID_Default= 9 AND @rcount > 0  
			BEGIN
                     SET @fieldupdatedefault = @fieldupdatedefault +@FieldName_Default+ '=1,'+ CHAR(10) + CHAR(13)  
					 SET @fieldvaluedefault = @fieldvaluedefault + '1,'+ CHAR(10) + CHAR(13)    
			END
                         
               
           FETCH FROM my_cursor INTO @PK_ModuleField_ID_Default, @FieldName_Default,     
           @FieldLabel_Default,    
           @Sequence_Default, @Required_Default, @IsPrimaryKey_Default, @IsUnik_Default,    
           @FK_FieldType_ID_Default, @SizeField_Default, @FK_ExtType_ID_Default    
       END    
           
       CLOSE my_cursor    
       DEALLOCATE my_cursor    
           
           
       IF LEN(@fieldDefault) > 0    
           SET @fieldDefault = SUBSTRING(@fieldDefault, 1, LEN(@fieldDefault) -3)    
           
       IF LEN(@fieldvaluedefault) > 0    
           SET @fieldvaluedefault = SUBSTRING(@fieldvaluedefault, 1, LEN(@fieldvaluedefault) -3)    
           
        
       --insert mode
	   IF LEN(@fieldInsertValue) > 0
		   BEGIN
			DECLARE @strinsert VARCHAR(MAX) = ''    
           
			SET @strinsert = 'INSERT INTO ' + @ModuleName + '(' + CHAR(10) + CHAR(13)      
			SET @strinsert += @fields  
			IF LEN(@fieldDefault) > 0    
			BEGIN    
				SET @strinsert += ',' + CHAR(10) + CHAR(13)    
				SET @strinsert += @fieldDefault + CHAR(10) + CHAR(13)    
			END    
           
			SET @strinsert += ')' + CHAR(10) + CHAR(13)    
			SET @strinsert += ' SELECT ' + CHAR(10) + CHAR(13)    
			SET @strinsert += @fieldInsertValue     
			IF LEN(@fieldactive) > 0    
			BEGIN    
				SET @strinsert += ',' + CHAR(10) + CHAR(13)    
				SET @strinsert += @fieldactive + CHAR(10) + CHAR(13)    
			END    
           
			SET @strinsert += @fieldvaluedefault + CHAR(10) + CHAR(13)    
			SET @strinsert += ' FROM ' + @strTableUploadName +'  WHERE nawa_userid=''' + @userid + ''''    
			SET @strinsert +=     
				' AND nawa_Action=''INSERT'' AND ISNULL(KeteranganError, '''') = '''' order by nawa_recordnumber'
			EXEC (@strinsert)    
		END
           
           
		--upate  mode   
		DECLARE @strupdate VARCHAR(MAX) = ''    
		IF LEN(@fieldupdatedefault) > 0    
		SET @fieldupdatedefault = SUBSTRING(@fieldupdatedefault, 1, LEN(@fieldupdatedefault) -3)    

		SET @strupdate = 'UPDATE '+ @ModuleName +' SET ' + char(10)    
		+ @FieldUpdate    
		+ @filedupdateactive    
		+@fieldupdatedefault    
		+' from '+@ModuleName    
		+ '  INNER JOIN ' + @strTableUploadName +' ON ' + @ModuleName + '.' + @fieldprimarykey + '=' + @strTableUploadName +'.' + @fieldprimarykey + '' + CHAR(10) 
		+ '  WHERE ' + @strTableUploadName +'.nawa_userid=''' + @userid + ''' AND ' + @strTableUploadName +'.KeteranganError='''' AND ' + @strTableUploadName +'.nawa_Action=''Update'' ' + CHAR(10) 
		+ '  '         
		EXEC(@strupdate)    
    
        --delete mode
		DECLARE @sqlDelete varchar(max)
		SET @sqlDelete = '   DELETE ' + @ModuleName + '            ' + CHAR(10)
		+ '         FROM ' + @ModuleName + ' ' + CHAR(10)
		+ '         INNER JOIN ' + @strTableUploadName + ' ON ' + @ModuleName + '.' + @fieldprimarykey + '=' +
		@strTableUploadName + '.' + @fieldprimarykey + '          ' + CHAR(10)
		+ '         WHERE ' + @strTableUploadName + '.nawa_Action=''Delete'' AND ' + @strTableUploadName + '.KeteranganError='''' ' + CHAR(10)
		+ '         AND ' + @strTableUploadName + '.nawa_userid=''' + @userid + ''''
		EXEC (@sqlDelete)

END
