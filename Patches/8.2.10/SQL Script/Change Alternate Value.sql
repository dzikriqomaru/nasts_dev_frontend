/* INSERT QUERY */
DECLARE @PK_Module_ID INT;
SELECT @PK_Module_ID = PK_Module_ID FROM Module WHERE ModuleName = 'Alternate';

IF NOT EXISTS(SELECT 1 FROM ModuleValidation WHERE StoreProcedureName = 'usp_changeAlternateValue' and FK_Module_ID = @PK_Module_ID)
BEGIN
INSERT INTO ModuleValidation (
	FK_Module_ID,
	FK_ModuleAction_ID,
	FK_ModuleTime_ID,
	StoreProcedureName,
	StoreProcedureParameter,
	StoreProcedureParameterValueFieldSequence
) VALUES (
	@PK_Module_ID, 1, 2, 'usp_changeAlternateValue', '@PK_Alternate_ID,@EndDate', '1,5'
), (
	@PK_Module_ID, 2, 2, 'usp_changeAlternateValue', '@PK_Alternate_ID,@EndDate', '1,5'
)

/* UPDATE QUERY */
UPDATE Module SET IsUseStoreProcedureValidation = 1 WHERE PK_Module_ID = @PK_Module_ID;
END

/* CREATE SP QUERY */
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE OR ALTER PROCEDURE [dbo].[usp_changeAlternateValue]
(
    @PK_Alternate_ID INT,
    @EndDate DATETIME
)
AS
BEGIN
	/*
	| Dandi Juliandi at 25 April 2024
	| Fix 7863 NF8
	*/

    UPDATE Alternate 
	SET EndDate = DATEADD(MINUTE, -1, DATEADD(DAY, 1, @EndDate))
    WHERE PK_Alternate_ID = @PK_Alternate_ID;
END