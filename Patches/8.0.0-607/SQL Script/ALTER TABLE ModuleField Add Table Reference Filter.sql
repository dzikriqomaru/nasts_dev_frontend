DECLARE @module_id INT
SET @module_id = (SELECT PK_Module_ID FROM Module WHERE ModuleName = 'DataAccessReplacer')
IF NOT EXISTS(SELECT * from ModuleField where FK_Module_ID = @module_id AND FieldName ='FilterData')
BEGIN 
	INSERT INTO ModuleField(FK_Module_ID, FieldName, FieldLabel, Sequence, Required, IsPrimaryKey, IsUnik, 
	IsShowInView, IsShowInForm, FK_FieldType_ID, SizeField, FK_ExtType_ID, TabelReferenceName, TabelReferenceNameAlias,
	TableReferenceFieldKey, TableReferenceFieldDisplayName, TableReferenceFilter) 
	VALUES(@module_id, 'TableReferenceFilter', 'Table Reference Filter', 8, 0, 0, 0, 1, 1, 9, 250, 5, '', '', '', '', '')
END