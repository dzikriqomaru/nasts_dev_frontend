UPDATE ModuleField SET 
	[Sequence] = 9,
	Required = 1,
	FK_ExtType_ID = 2, 
	FK_FieldType_ID  = 11,
	SizeField = 0,
	TabelReferenceName = 'ModuleField', 
	TabelReferenceNameAlias = 'ModuleField', 
	TableReferenceFieldKey = 'PK_ModuleField_ID',
	TableReferenceFieldDisplayName = 'FieldName',
	BCasCade = 1,
	FieldNameParent = 'TableReferenceName',
	FilterCascade = 'FK_Module_ID = @Parent'
WHERE
	FK_Module_ID = 309234
AND 
	FieldName = 'TableReferenceKey'