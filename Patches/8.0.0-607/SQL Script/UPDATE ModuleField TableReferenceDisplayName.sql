UPDATE ModuleField SET 
	[Sequence] = 10,
	Required = 1,
	FK_ExtType_ID = 2, 
	FK_FieldType_ID  = 11,
	SizeField = 0,
	TabelReferenceName = 'ModuleField', 
	TabelReferenceNameAlias = 'ModuleFieldName', 
	TableReferenceFieldKey = 'PK_ModuleField_ID',
	TableReferenceFieldDisplayName = 'FieldLabel',
	BCasCade = 1,
	FieldNameParent = 'TableReferenceKey',
	FilterCascade = 'ModuleFieldName.PK_ModuleField_ID = @Parent'
WHERE
	FK_Module_ID = 309234
AND 
	FieldName = 'TableReferenceDisplayName'