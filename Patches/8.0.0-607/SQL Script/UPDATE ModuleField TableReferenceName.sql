UPDATE ModuleField SET 
	FK_ExtType_ID = 2, 
	FK_FieldType_ID  = 11,
	SizeField = 0,
	TabelReferenceName = 'Module', 
	TabelReferenceNameAlias = 'Module', 
	TableReferenceFieldKey = 'PK_Module_ID', 
	TableReferenceFieldDisplayName = 'ModuleName'
WHERE
	FK_Module_ID = 309234
AND 
	FieldName = 'TableReferenceName'