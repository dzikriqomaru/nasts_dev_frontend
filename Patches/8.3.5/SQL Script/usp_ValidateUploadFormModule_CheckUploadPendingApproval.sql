/****** Object:  StoredProcedure [dbo].[usp_ValidateUploadFormModule_CheckUploadPendingApproval]    Script Date: 5/16/2024 13:48:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER procedure [dbo].[usp_ValidateUploadFormModule_CheckUploadPendingApproval]
/*********************
* Changes
* Date        Modified By                Comments
********************
* 20230327	Davin Julian	Add Filter @PK_ModuleApproval_ID
* 16 Mei 2024 Davin Julian	Add b.nawa_action != insert and If else
********************/
		 @userid VARCHAR(50)='sysadmin'
		,@FieldLabel varchar(100)= 'ID'
		,@FieldName varchar(100)= 'PK_F204_ID'
		,@Modulename varchar(100) ='F204'
		,@correctionID varchar(100) = '0'
		,@FK_FieldType_ID INT =0
as 
BEGIN

		--,@xmlData varchar(max)= '<DataUpload><F204_Upload><PK_upload_ID>1</PK_upload_ID><nawa_userid>operator1</nawa_userid><nawa_recordnumber>1</nawa_recordnumber><KeteranganError></KeteranganError><nawa_Action>Insert</nawa_Action><PK_F204_ID>26</PK_F204_ID><TanggalData>21-Sep-2018</TanggalData><PosisiAkhirHariTransaksiDerivatifJualBukanInvestasiDenganPihakAsing>675.87</PosisiAkhirHariTransaksiDerivatifJualBukanInvestasiDenganPihakAsing><AdaTransaksiDerivatifValasHariIni>(1) Ada transaksi</AdaTransaksiDerivatifValasHariIni><AdaTransaksiDerivatifValasJualBukanInvestasiDenganPihakAsing>(4) Tidak ada transaksi</AdaTransaksiDerivatifValasJualBukanInvestasiDenganPihakAsing></F204_Upload></DataUpload>'
	

DECLARE @bIsCreateTableUploadForEachUser AS BIT=0
DECLARE @strTableUploadName AS VARCHAR(500)=''
DECLARE @strtableuploadnamecreatedby VARCHAR(500)=''
SELECT @bIsCreateTableUploadForEachUser =sp.SettingValue	
FROM SystemParameter AS sp WHERE sp.PK_SystemParameter_ID=53

DECLARE @kolom VARCHAR(MAX)
DECLARE @kolomIsi VARCHAR(MAX)
DECLARE @tablename VARCHAR(500)

SELECT 
  @kolom =ISNULL(
   STUFF((SELECT ', [' + US.FieldName + '] varchar(8000)'
          FROM modulefield  US
          WHERE US.FK_Module_ID= m.PK_Module_ID
          ORDER BY us.Sequence
          FOR XML PATH('')), 1, 1, ''),'')  ,@tablename =m.ModuleName
FROM Module m
WHERE m.ModuleName=@Modulename

SELECT 
   
  @kolomIsi= ISNULL(
   STUFF((SELECT ', NULLIF([' + US.FieldName + '],''NULL'') [' + US.FieldName + ']'
          FROM modulefield  US
          WHERE US.FK_Module_ID= m.PK_Module_ID
          ORDER BY us.Sequence
          FOR XML PATH('')), 1, 1, ''),'')  ,@tablename =m.ModuleName
FROM Module m
WHERE m.ModuleName=@Modulename

DECLARE @sql VARCHAR(MAX)


DECLARE @PK_ModuleApproval_ID BIGINT,  @xmlData VARCHAR(max),@createdby VARCHAR(50)
DECLARE my_cursor2 CURSOR FAST_FORWARD READ_ONLY 
FOR	
	SELECT PK_ModuleApproval_ID,ModuleField,CreatedBy from ModuleApproval 
	WHERE ModuleName =@Modulename and 
		  PK_ModuleAction_ID  = (select PK_ModuleAction_ID from ModuleAction where ModuleActionName ='Import')
OPEN my_cursor2 FETCH FROM my_cursor2 INTO @PK_ModuleApproval_ID,@xmlData,@createdby
WHILE @@FETCH_STATUS = 0
BEGIN
	IF @bIsCreateTableUploadForEachUser = 1
	BEGIN
		SET @strTableUploadName = @Modulename + '_Upload_data_' + @userid
		SET  @strtableuploadnamecreatedby = @Modulename + '_Upload_data_' + @createdby + '_approval'
	END
	ELSE
	BEGIN
		SET @strTableUploadName = @Modulename + '_Upload'
		 SET  @strtableuploadnamecreatedby  = @Modulename + '_Upload_approval'
	END

	IF OBJECT_ID(@strtableuploadnamecreatedby, 'U') IS NULL
	BEGIN
		FETCH FROM my_cursor2 INTO @PK_ModuleApproval_ID,@xmlData,@createdby
		CONTINUE
	END

	set @sql ='' + CHAR(10)
	--set @sql ='DECLARE @handle INT ' + CHAR(10)
	--		 +'DECLARE @PrepareXmlStatus INT  ' + CHAR(10)
	--		 +'DECLARE @XMLData XML ' + CHAR(10)

	--set @sql =@sql	+  'SET @XMLDATA='''+CONVERT(VARCHAR(MAX),Replace(@xmlData,'''','''''')) +''''+CHAR(10)		  

	--set @sql =@sql	+  'EXEC @PrepareXmlStatus= sp_xml_preparedocument @handle OUTPUT, @XMLDATA '+CHAR(10)+CHAR(13)
	--SET @sql =@sql+ 'IF OBJECT_ID('''+@tablename+'_Upload_'+@userid+''', ''U'') IS NOT NULL  ' + char(10)
	--			  + '	DROP TABLE '+@tablename+'_Upload_'+@userid+';  ' + char(10)+CHAR(13)
         

	--set @sql =@sql	+ 'SELECT '+@kolomIsi+' '+ CHAR(10)
	--				+ 'INTO '+@tablename+'_Upload_'+@userid+' '+ CHAR(10)
	--				+ 'FROM    OPENXML(@handle, ''/DataUpload/'+@tablename+'_Upload'', 2)   '+CHAR(10)
	--				+ 'WITH ( '+ @kolom +' )  '+CHAR(10)+CHAR(13)

	--Davin 16 Mei 2024 Add b.nawa_action != insert and If else
	 IF @FK_FieldType_ID = 12 OR @FK_FieldType_ID = 15
	 BEGIN
	set @sql =@sql	+ 'update a'+ CHAR(10)
					+ '	set KeteranganError  = a.KeteranganError +''Line '' + CONVERT(VARCHAR(20), a.nawa_recordnumber)         + '' : '+ @FieldLabel +' already exists on upload pending approval '' '+ char(13)
					+ 'from '+@strTableUploadName+' a'+ CHAR(10)
					--+'inner join '+@tablename+'_Upload_'+@userid+' b on a.'+@FieldName+' = b.'+@FieldName+' and a.nawa_Action !=''Insert'' and a.nawa_userid ='''+@userid+''' ' + CHAR(10)+CHAR(13)
					+'inner join '+@strtableuploadnamecreatedby+' b on a.'+@FieldName+' = b.'+@FieldName+' and a.nawa_Action !=''Insert'' and a.nawa_userid ='''+@userid+''' and b.FK_ModuleApproval_ID = ' + CAST( @PK_ModuleApproval_ID AS VARCHAR)
					+ ' AND b.FK_ModuleApproval_ID <> ' + @correctionID + CHAR(10)+CHAR(13) 
					+' and b.nawa_Action !=''Insert'' '
	END
	ELSE
	BEGIN
		set @sql =@sql	+ 'update a'+ CHAR(10)
					+ '	set KeteranganError  = a.KeteranganError +''Line '' + CONVERT(VARCHAR(20), a.nawa_recordnumber)         + '' : '+ @FieldLabel +' already exists on upload pending approval '' '+ char(13)
					+ 'from '+@strTableUploadName+' a'+ CHAR(10)
					--+'inner join '+@tablename+'_Upload_'+@userid+' b on a.'+@FieldName+' = b.'+@FieldName+' and a.nawa_Action !=''Insert'' and a.nawa_userid ='''+@userid+''' ' + CHAR(10)+CHAR(13)
					+'inner join '+@strtableuploadnamecreatedby+' b on a.'+@FieldName+' = b.'+@FieldName+' and a.nawa_Action !=''Insert'' and a.nawa_userid ='''+@userid+''' and b.FK_ModuleApproval_ID = ' + CAST( @PK_ModuleApproval_ID AS VARCHAR)
					+ ' AND b.FK_ModuleApproval_ID <> ' + @correctionID + CHAR(10)+CHAR(13) 
	END

	--set @sql=@sql +'drop table '+@tablename+'_Upload_'+@userid
	--print @sql
	exec (@sql)

	FETCH FROM my_cursor2 INTO @PK_ModuleApproval_ID,@xmlData,@createdby
END

CLOSE my_cursor2
DEALLOCATE my_cursor2
END
