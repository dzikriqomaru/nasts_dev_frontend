import React, { Component, useState, useEffect } from "react";
import { MemoryRouter, BrowserRouter } from "react-router-dom";
import { Chart } from "react-chartjs-2";
import { ThemeProvider } from "@material-ui/styles";
import validate from "validate.js";
import Routes from "./Routes";

import "react-perfect-scrollbar/dist/css/styles.css";
import "./index.scss";
import "./App.css";
import Text from "react-text";
import { withRouter } from "react-router-dom";

// COMPONENYS
import {
  theme,
  chartjs,
  LoginSkeleton,
  validators,
  UserProfileContextProvider,
  InfoPanel,
  DateTimeFormat,
  ExportDialog,
  GetMenuAccess,
  useInfoPanel,
  Encryption,
  APIRequest,
  StaticData,
  DialogMessage as MsgDialog,
  useBeforeUnload,
} from "@vikitheolorado/nawadata-web";

Chart.helpers.extend(Chart.elements.Rectangle.prototype, {
  draw: chartjs.draw,
});

validate.validators = {
  ...validate.validators,
  ...validators,
};

export default function App(props) {
  const [mainColor, setMainColor] = useState(
    StaticData.Get("ThemeColor", "#3f50b5")
  );
  const [secColor, setSecColor] = useState(
    StaticData.Get("SecThemeColor", "#f44336")
  );
  const [headerPrimary, setHeaderPrimary] = useState(
    StaticData.Get("headerPrimary", "")
  );
  const [headerSecondary, setHeaderSecondary] = useState(
    StaticData.Get("headerSecondary", "")
  );
  const [appLanguage, setAppLanguage] = useState(
    StaticData.Get("Language", "en")
  );
  const [hasError, setHasError] = useState(false);
  const [appTitle, setAppTitle] = useState(StaticData.Get("title", ""));
  const [appDictionary, setAppDictionary] = useState(null);
  const [msgDialogTitle, setMsgDialogTitle] = useState("");
  const [msgDialogContent, setMsgDialogContent] = useState("");
  const [msgDialogOpen, setMsgDialogOpen] = useState(false);
  const [apiUrl, setApiUrl] = useState(localStorage.getItem("APIUrl"));
  const { showInfoPanel } = useInfoPanel();

  const setStateOfParent = (errorState) => {
    setHasError(errorState);
  };

  const dialogHandle = () => {
    setMsgDialogOpen(!msgDialogOpen);
  };

  const getSystemParameterPublic = (id) => {
    APIRequest.Send(
      {
        method: "GET",
        url: `api/systemParameter/getSysParamPublic?SysParamID=${id}`,
      },
      null,
      "",
      apiUrl
    )
      .then(function (res) {
        if (res.status == 200) {
          if (id == 50) {
            StaticData.Set("GoogleToken", res.data.SettingValue);
          } else if (id === 56) {
            StaticData.Set("FacebookToken", res.data.SettingValue);
          } else if (id === 1052) {
            StaticData.Set("DecimalSeparator", res.data.SettingValue);
          } else if (id === 1051) {
            StaticData.Set("ThousandSeparator", res.data.SettingValue);
          } else if (id === 39) {
            StaticData.Set(
              "PopUpMultiComboCheckBoxDelimiter",
              res.data.SettingValue
            );
          } else if (id === 7) {
            StaticData.Set("PageSize", res.data.SettingValue);
          }
        } else {
          showInfoPanel.error("Error When Get Key From DataBase");
        }
      })
      .catch(() => {
        setHasError(true);
      });
  };

  const getColor = () => {
    APIRequest.Send(
      {
        method: "GET",
        url: "api/Theme",
      },
      null,
      "",
      apiUrl
    )
      .then(function (res) {
        if (res.status == 200) {
          StaticData.Set("title", res.data.title);
          setAppTitle(res.data.title);
          if (StaticData.Get("ThemeColor") !== res.data.themeColor) {
            StaticData.Set("ThemeColor", res.data.themeColor);
            setMainColor(res.data.themeColor);
          }
          if (StaticData.Get("SeCThemeColor") !== res.data.secThemeColor) {
            StaticData.Set("SecThemeColor", res.data.secThemeColor);
            setSecColor(res.data.secThemeColor);
          }
          if (StaticData.Get("Language") !== res.data.language) {
            StaticData.Set("Language", res.data.language);
            setAppLanguage(res.data.language);
          }
          if (StaticData.Get("headerPrimary") !== res.data.headerPrimary) {
            StaticData.Set("headerPrimary", res.data.headerPrimary);
            setHeaderPrimary(res.data.headerPrimary);
          }
          if (StaticData.Get("headerSecondary") !== res.data.headerSecondary) {
            StaticData.Set("headerSecondary", res.data.headerSecondary);
            setHeaderSecondary(res.data.headerSecondary);
          }
          StaticData.Set(
            "EnableCaptcha",
            res.data.EnableCaptchaWeb === true ? "true" : "false"
          );
          StaticData.Set(
            "EnableGoogleLogin",
            res.data.EnableGoogleLogin === true ? "true" : "false"
          );
          StaticData.Set(
            "EnableFacebookLogin",
            res.data.EnableFacebookLogin === true ? "true" : "false"
          );
        } else {
          setMsgDialogContent("Invalid Credential");
          setMsgDialogTitle("Error");
          setMsgDialogOpen(true);
        }

        getDictionary();
        getSystemParameterPublic(-50);
        getSystemParameterPublic(-56);
        getSystemParameterPublic(1051);
        getSystemParameterPublic(1052);
        getSystemParameterPublic(7);
        getSystemParameterPublic(39);
      })
      .catch(() => {
        setHasError(true);
      });
  };

  const setDictionary = (data) => {
    let helper = {};
    let dictionary = data
      .map((x) => ({
        keywords: x.Keywords,
        language: x.LanguageType,
        text: x.TextDefinition,
      }))
      .reduce((acc, obj) => {
        let key = obj.keywords + "-" + obj.language;
        if (!helper[key]) {
          helper[key] = Object.assign({}, obj); // create a copy of o
          acc.push(helper[key]);
        }

        return acc;
      }, []);

    helper = {};

    let dictionaryDetail = data
      .filter((x) => x.ParamName != "")
      .map((x) => ({
        keywords: x.Keywords,
        parameter: x.ParamName,
      }))
      .reduce((acc, obj) => {
        let key = obj.keywords + "-" + obj.parameter;

        if (!helper[key]) {
          helper[key] = Object.assign({}, obj); // create a copy of o
          acc.push(helper[key]);
        }

        return acc;
      }, []);

    let groupDict = dictionary.reduce((acc, obj) => {
      const key = obj["keywords"];
      if (!acc[key]) {
        acc[key] = {};
      }

      // Add object to list for given key's value
      let paramDict = dictionaryDetail
        .filter((x) => x.keywords == key)
        .map((dictDetail) => dictDetail.parameter);
      if (paramDict.length > 0) {
        acc[key][obj["language"]] = eval(
          "({" + paramDict.join(", ") + "}) => " + obj["text"]
        );
      } else {
        acc[key][obj["language"]] = obj["text"];
      }
      return acc;
    }, {});

    localStorage.setItem("dictObj", JSON.stringify(groupDict));
    setAppDictionary(groupDict);
  };

  const getDictionary = () => {
    APIRequest.Send(
      {
        method: "GET",
        url: "api/systemparameter/getDictionary",
      },
      null,
      "",
      apiUrl
    )
      .then(function (res) {
        if (res.status === 200) {
          setDictionary(res.data);
          localStorage.setItem("appDictionary", JSON.stringify(res.data));
        } else {
          setMsgDialogContent("Page not found");
          setMsgDialogTitle("Error");
          setMsgDialogOpen(true);
          return {};
        }
      })
      .catch(() => {
        setHasError(true);
      });
  };

  const getResetPasswordURL = (type) => {
    const urlParams = new URLSearchParams(window.location.search);

    if (window.location.search && urlParams.has("token")) {
      let urlParam = window.location.search.replace("?", "");
      let urlSplit = "";
      if (urlParam.includes("&")) {
        urlSplit = urlParam.split("&");
      } else if (urlParam.includes(",")) {
        urlSplit = urlParam.split(",");
      }

      if (urlParam.length) {
        if (type === "url") {
          return urlSplit[0].split("=")[1];
        } else if (type === "token") {
          return urlSplit[1].split("=")[1];
        } else {
          return "";
        }
      } else {
        return "";
      }
    } else {
      return "";
    }
  };

  useEffect(() => {
    let dictionary = localStorage.getItem("appDictionary");

    if (dictionary) {
      setDictionary(JSON.parse(dictionary));
    }

    getColor();
    window.onerror = (message, url, line, column, error) => {
      console.table({
        message,
        url,
        line,
        column,
        error,
      });
    };
    document.title = appTitle;
  }, []);

  useBeforeUnload();

  const themeColor = { ...theme };
  themeColor.palette.primary.main = mainColor ? mainColor : null;
  themeColor.palette.secondary.main = secColor ? secColor : null;
  themeColor.headerPrimary = headerPrimary || null;
  themeColor.headerSecondary = headerSecondary || null;

  if (appDictionary) {
    return hasError ? (
      <h2>Something went wrong!!</h2>
    ) : (
      <InfoPanel>
        <ExportDialog>
          <UserProfileContextProvider APIUrl={apiUrl}>
            <DateTimeFormat>
              <GetMenuAccess>
                <Encryption>
                  <Text language={appLanguage} dictionary={appDictionary}>
                    <ThemeProvider theme={themeColor}>
                      <MemoryRouter>
                        {/*  For Use Prompt react-router-dom, Declare Inside MemoryRouter 
                                                        getUserConfirmation = {(message, callback) => {
                                                            const allowTransition = window.confirm(message);
                                                            callback(allowTransition);
                                                        } } 
                                                    */}
                        <Routes
                          resetPasswordURL={getResetPasswordURL("url")}
                          resetPasswordToken={getResetPasswordURL("token")}
                        />
                        <MsgDialog
                          isOpen={msgDialogOpen}
                          msgTitle={msgDialogTitle}
                          message={msgDialogContent}
                          handleDialog={dialogHandle}
                        />
                      </MemoryRouter>
                    </ThemeProvider>
                  </Text>
                </Encryption>
              </GetMenuAccess>
            </DateTimeFormat>
          </UserProfileContextProvider>
        </ExportDialog>
      </InfoPanel>
    );
  } else {
    return (
      <React.Fragment>
        <LoginSkeleton />
        <MsgDialog
          isOpen={msgDialogOpen}
          msgTitle={msgDialogTitle}
          message={msgDialogContent}
          handleDialog={dialogHandle}
        />
      </React.Fragment>
    );
  }
}
