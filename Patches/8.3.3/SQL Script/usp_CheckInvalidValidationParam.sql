/****** Object:  StoredProcedure [dbo].[usp_CheckInvalidValidationParam]    Script Date: 5/15/2024 20:00:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[usp_CheckInvalidValidationParam]  
	@ModuleID AS VARCHAR(MAX),  
	@ExpressionType AS VARCHAR(MAX),
	@Expression AS VARCHAR(MAX) 
AS  
BEGIN  

 	SET @Expression=REPLACE(@Expression,'@userid','')
	IF @ExpressionType='EXPRESSION'
	BEGIN
		DECLARE @SQL AS VARCHAR(MAX) = '',  
		@ModuleName AS VARCHAR(MAX) = (SELECT ModuleName FROM Module WHERE PK_Module_ID = @ModuleID);  
		SET @SQL = 'IF EXISTS(SELECT 1 FROM (SELECT *, '''' Nawa_Action FROM ' + @ModuleName + ') hd ' + @Expression + ')  
			BEGIN   
				RETURN   
			END';
		BEGIN TRY  
			EXEC(@SQL)  
		END TRY  
			BEGIN CATCH  
				SELECT 'Please check the validation expression.<br> Error found: ' + ERROR_MESSAGE()  
			END CATCH  
	END
END