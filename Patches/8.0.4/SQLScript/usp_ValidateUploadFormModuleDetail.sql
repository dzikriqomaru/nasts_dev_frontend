---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
ALTER PROCEDURE [dbo].[usp_ValidateUploadFormModuleDetail]
/*********************
* Procedure description:
* Date:   12/03/2021
* Author: Muhammad Fauzan Girindra
*
* Changes
* Date        Modified By                Comments
17 Jan 2023	Davin Julian	Add: @FieldTypeSQLName
********************
05 May 2023 Davin Julian		Add: Cast a number to match the rounding of the data type 
07 Feb 2024 A I Humam			Add: check whether the inputted data is valid in database based on filter cascade
25 Maret 2024 Davin Julian		Add: usp_DeleteRelatedChildDataFromUpload 
********************/
(
    @moduleDetailID INT,
    @userID VARCHAR(50),
    @intMode INT
)
AS
BEGIN
    DECLARE @sql VARCHAR(MAX);

    --DECLARE @moduleDetailID INT = 8946,
    --@userID VARCHAR(50) ='sysadmin',
    --@intMode INT = 0

    DECLARE @delimiter VARCHAR(MAX)
	SELECT @delimiter = SettingValue
	FROM SystemParameter
	WHERE PK_SystemParameter_ID = 39

    DECLARE @defaultTimeFormat VARCHAR(50) = 'HH:mm';
    SELECT @defaultTimeFormat = sp.SettingValue
    FROM SystemParameter AS sp
    WHERE sp.PK_SystemParameter_ID = 33;

   DECLARE @dateformat INT
	SELECT @dateformat = CASE mdf.SQLFormat
		WHEN 'mmm dd yyyy hh:mm AM/PM' THEN 100
		WHEN 'mm/dd/yy' THEN 1
		WHEN 'mm/dd/yyyy' THEN 101
		WHEN 'yy.mm.dd' THEN 2
		WHEN 'dd/mm/yy' THEN 3
		WHEN 'dd.mm.yy' THEN 4
		WHEN 'dd-mm-yy' THEN 5
		WHEN 'dd Mmm yy' THEN 6
		WHEN 'Mmm dd, yy' THEN 7
		WHEN 'hh:mm:ss' THEN 8
		WHEN 'yyyy.mm.dd' THEN 102
		WHEN 'dd/mm/yyyy' THEN 103
		WHEN 'dd.mm.yyyy' THEN 104
		WHEN 'dd-mm-yyyy' THEN 105
		WHEN 'dd Mmm yyyy' THEN 106
		WHEN 'Mmm dd, yyyy' THEN 107
		WHEN 'Mmm dd yyyy hh:mm:ss:ms AM/PM' THEN 9
		WHEN 'Mmm dd yyyy hh:mi:ss:mmm AM/PM' THEN 9
		WHEN 'Mmm dd yy hh:mm:ss:ms AM/PM' THEN 109
		WHEN 'mm-dd-yy' THEN 10
		WHEN 'mm-dd-yyyy' THEN 110
		WHEN 'yy/mm/dd' THEN 11
		WHEN 'yyyy/mm/dd' THEN 111
		WHEN 'yymmdd' THEN 12
		WHEN 'yyyymmdd' THEN 112
		WHEN 'dd Mmm yyyy hh:mm:ss:Ms' THEN 113
		WHEN 'hh:mm:ss:Ms' THEN 14
		WHEN 'yyyy-mm-dd hh:mm:ss' THEN 120
		WHEN 'yyyy-mm-dd hh:mm:ss.Ms' THEN 121
		WHEN 'yyyy-mm-ddThh:mm:ss.Ms' THEN 126
		WHEN 'dd Mmm yyyy hh:mm:ss:ms AM/PM' THEN 130
		WHEN 'dd/mm/yy hh:mm:ss:ms AM/PM' THEN 131
		WHEN 'RFC822' THEN 2
		WHEN 'dd Mmm yyyy hh:mm' THEN 4
		ELSE 1 END
	FROM   SystemParameter sp
	INNER JOIN MDateFormat mdf
	ON sp.SettingValue = mdf.PK_DateFormat_ID 
	WHERE  sp.PK_SystemParameter_ID = 14

    DECLARE @moduleId int;
    DECLARE @strModuleDetailName VARCHAR(250);
    SELECT 
	@strModuleDetailName = ModuleDetailName,
	@moduleId = FK_Module_ID
	FROM ModuleDetail
	WHERE PK_ModuleDetail_ID = @moduleDetailID;

	DECLARE @FKFieldName VARCHAR(250)
	SELECT @FKFieldName = FieldName
	FROM ModuleDetailField
	WHERE FK_ModuleDetail_ID = @moduleDetailID AND FK_FieldType_ID = 16;

	DECLARE @primaryKeyFieldName VARCHAR(MAX) = (SELECT TOP 1 FieldName FROM ModuleDetailField WHERE FK_ModuleDetail_ID = @moduleDetailID AND IsPrimaryKey = 1);
	
    DECLARE @moduleName VARCHAR(250);
    DECLARE @fieldFK VARCHAR(250);
    DECLARE @fieldParent VARCHAR(250);


    SET @sql = 'UPDATE ' + @strModuleDetailName + '_Upload SET nawa_Action = ''Insert'' WHERE nawa_Action IS NULL AND nawa_userid=''' + @userID + '''';
    EXEC (@sql);
	
	SET @sql = 'UPDATE ' + @strModuleDetailName + '_Upload SET Active = 1 WHERE Active IS NULL AND nawa_userid=''' + @userID + '''';
    EXEC (@sql);

	--Commented for regex validation in backend
    SET @sql = 'UPDATE ' + @strModuleDetailName + '_Upload SET KeteranganError = '''' WHERE KeteranganError IS NULL AND nawa_userid=''' + @userID + '''';
    EXEC (@sql);

    DECLARE @PK_ModuleDetailField_ID BIGINT,
            @FK_ModuleDetail_ID INT,
            @FieldName VARCHAR(250),
            @FieldLabel VARCHAR(250),
            @Sequence INT,
            @Required BIT,
            @IsPrimaryKey BIT,
            @IsUnik BIT,
            @IsShowInView BIT,
            @FK_FieldType_ID INT,
            @SizeField INT,
            @FK_ExtType_ID INT,
            @TabelReferenceName VARCHAR(250),
            @TabelReferenceNameAlias VARCHAR(50),
            @TableReferenceFieldKey VARCHAR(250),
            @TableReferenceFieldDisplayName VARCHAR(250),
            @TableReferenceFilter VARCHAR(550),
            @IsUseRegexValidation BIT,
			@FieldNameParent VARCHAR(250),
			@FilterCascade VARCHAR(250),
            @ModuleDetailName VARCHAR(250),
            @FieldTypeDescription VARCHAR(255),
			@FieldTypeSQLName VARCHAR(250)

    DECLARE my_cursor CURSOR FAST_FORWARD READ_ONLY FOR
    SELECT PK_ModuleDetailField_ID,
           FK_ModuleDetail_ID,
           FieldName,
           FieldLabel,
           Sequence,
           [Required],
           IsPrimaryKey,
           IsUnik,
           IsShowInView,
           FK_FieldType_ID,
           SizeField,
           FK_ExtType_ID,
           TabelReferenceName,
           TabelReferenceNameAlias,
           TableReferenceFieldKey,
           TableReferenceFieldDisplayName,
           TableReferenceFilter,
           IsUseRegexValidation,
		   FieldNameParent,
		   FilterCascade,
           m.ModuleDetailName,
           mt.FieldTypeDescription,
		   mt.FieldTypeSQLName
    FROM dbo.ModuleDetailField mf
        INNER JOIN ModuleDetail m
            ON mf.FK_ModuleDetail_ID = m.PK_ModuleDetail_ID
        INNER JOIN MFieldType mt
            ON mt.PK_FieldType_ID = mf.FK_FieldType_ID
    WHERE mf.FK_ModuleDetail_ID = @moduleDetailID
          --AND mf.IsShowInForm = 1
    ORDER BY mf.Sequence;

    OPEN my_cursor;

    FETCH FROM my_cursor
    INTO @PK_ModuleDetailField_ID,
         @FK_ModuleDetail_ID,
         @FieldName,
         @FieldLabel,
         @Sequence,
         @Required,
         @IsPrimaryKey,
         @IsUnik,
         @IsShowInView,
         @FK_FieldType_ID,
         @SizeField,
         @FK_ExtType_ID,
         @TabelReferenceName,
         @TabelReferenceNameAlias,
         @TableReferenceFieldKey,
         @TableReferenceFieldDisplayName,
         @TableReferenceFilter,
         @IsUseRegexValidation,
		 @FieldNameParent,
		 @FilterCascade,
         @ModuleDetailName,
         @FieldTypeDescription,
		 @FieldTypeSQLName

    WHILE @@FETCH_STATUS = 0
    BEGIN
		--Cast a number to match the rounding of the data type
        IF @FK_FieldType_ID = 5
           OR @FK_FieldType_ID = 6
           OR @FK_FieldType_ID = 7
           OR @FK_FieldType_ID = 8
        BEGIN
			Set	@sql =  'UPDATE ' +  @ModuleDetailName + '_upload ' + CHAR(10)
	            + 'SET    '+@FieldName+' = TRY_CAST(' + @FieldName + ' as ' +@FieldTypeSQLName + ')'  +
				' WHERE TRY_CAST(' + @FieldName + ' as ' +@FieldTypeSQLName + ') IS NOT NULL and nawa_userid=''' + @userid +
	            '''  and nawa_Action <> ''Delete'' '
			EXEC(@sql);
		END;

        --mfieldtype
        --cek required dan kosong maka invalid
        IF @Required = 1
           AND NOT (
                       @FK_FieldType_ID = 12
                       OR @FK_FieldType_ID = 15
                   ) -- identity
        BEGIN
            SET @sql
                = ' UPDATE ' + @ModuleDetailName
                  + '_Upload SET KeteranganError =KeteranganError + ''Line ''+ convert(Varchar(20),nawa_recordnumber) +'' : '
                  + @FieldLabel + ' is required'' WHERE (' + @FieldName + '='''' or ' + @FieldName
                  + ' is null )  and nawa_userid=''' + @userID + '''  and nawa_Action <> ''Delete'' ';
			
			PRINT (@FieldLabel);
            PRINT (@sql);
            EXEC (@sql);
        END;
		
		IF @FK_FieldType_ID = 16
		BEGIN
			SET @moduleName = @TabelReferenceName ;
			SET @fieldFK = @FieldName;
			SET @fieldParent = @TableReferenceFieldKey;
		END;
		

        IF @FK_FieldType_ID = 11 --reference tabel
        BEGIN
     IF @FK_ExtType_ID <> 15 AND @FK_ExtType_ID <> 21 AND @FK_ExtType_ID <> 7
            BEGIN
                SET @sql
                    = 'update ' + @ModuleDetailName + '_upload set ' + @FieldName + '= ''(''+' + @FieldName + ' from  '
                      + @ModuleDetailName + '_upload where charindex(''('',' + @FieldName + ') =0 and '+ @FieldName + ' is not null and '+ @FieldName + ' != ''''';

                PRINT (@FieldLabel);
				PRINT (@sql);
                EXEC (@sql);

                SET @sql
                    = 'update ' + @ModuleDetailName + '_upload set ' + @FieldName + '= ' + @FieldName + '+'')'' from  '
                      + @ModuleDetailName + '_upload where charindex('')'',' + @FieldName + ') =0 and '+ @FieldName + ' is not null and '+ @FieldName + ' != ''''';

                PRINT (@FieldLabel);
				PRINT (@sql);
                EXEC (@sql);

                DECLARE @alias VARCHAR(50) = @TabelReferenceNameAlias;
                IF @alias = ''
                    SET @alias = @TabelReferenceName;

                SET @sql
                    = 'UPDATE ' + @ModuleDetailName
                      + '_upload SEt KeteranganError = KeteranganError + ''Line '' + CONVERT(VARCHAR(20), nawa_recordnumber) + '' : '
                      + @FieldLabel + ' not exist in database'' + CHAR(13) + CHAR(10) FROM ' + @ModuleDetailName
                      + '_upload INNER JOIN ( SELECT tulv.PK_upload_ID FROM ' + @ModuleDetailName
                      + '_upload tulv LEFT JOIN ( SELECT ''('' + CONVERT(VARCHAR(8000), ltrim(rtrim( ' + @alias + '.'
                      + @TableReferenceFieldKey + '))) +'')'' + '' '' + ltrim(rtrim( ' + @alias + '.'
                      + @TableReferenceFieldDisplayName + '))' + ' AS DisplayField, '
					  + '''(''+ CONVERT(VARCHAR(MAX), ' + @alias + '.' + @TableReferenceFieldKey + ')' + ' +'')'' AS DisplayKey '
					  + 'FROM ' + @TabelReferenceName + ' '+ @alias;
				
				--add cascade filter
				IF @FieldNameParent <> ''
				BEGIN
					--Get Parent Field
					DECLARE @ParentExtType INT, @ParentFieldType INT
					SELECT @ParentExtType = FK_ExtType_ID, @ParentFieldType = FK_FieldType_ID
					FROM ModuleDetailField
					WHERE FieldName = @FieldNameParent AND FK_ModuleDetail_ID = @moduleDetailID
					----
					--If ExtType is not FileUpload or RichText
					IF @ParentExtType <> 8 AND @ParentExtType <> 16
					BEGIN
						IF @ParentFieldType = 11
						BEGIN
							SET @sql += ' INNER JOIN ' + @ModuleDetailName+'_upload' + ' tulv ON ' + REPLACE(@FilterCascade, '@Parent', 
								'SUBSTRING(tulv.' + @FieldNameParent + ', CHARINDEX(''('',tulv.' + @FieldNameParent + ')+1, CHARINDEX('')'',tulv.' + @FieldNameParent + ')-2)'
							);
						END
						ELSE
						BEGIN
							SET @sql += ' INNER JOIN ' + @ModuleDetailName+'_upload' + ' tulv ON ' + REPLACE(@FilterCascade, '@Parent', 
								'tulv.' + @FieldNameParent
							);
						END
					END
				END;

				--add table reference filter
                IF @TableReferenceFilter <> ''
                BEGIN
                    SET @sql += ' WHERE ' + @TableReferenceFilter;
                END;

                SET @sql += ' )xx ON tulv.' + @FieldName + ' IN (xx.DisplayField, xx.DisplayKey) ' 
							+ ' WHERE tulv.nawa_userid = ''' + @userID
                            + ''' AND xx.DisplayField IS NULL AND xx.DisplayKey IS NULL ' 
                            + 'AND tulv.' + @FieldName + ' <> '''' )notvalid ON notvalid.PK_upload_ID = '
                            + @ModuleDetailName + '_upload.PK_upload_ID and nawa_Action <> ''Delete'' ';

                PRINT (@FieldLabel);
				PRINT (@sql);
                EXEC (@sql);
            END;
            ELSE IF @FK_ExtType_ID = 15 OR @FK_ExtType_ID = 21
            BEGIN
				--DAVIN 20230321 -- Add Alias and Filter Reference in combo
                DECLARE @aliasCombo VARCHAR(50) = @TabelReferenceNameAlias;
         IF @aliasCombo = ''
                    SET @aliasCombo = @TabelReferenceName;

				DECLARE @FilterReferenceCombo VARCHAR(500)= '';
                IF @TableReferenceFilter <> ''
                BEGIN
                    SET @FilterReferenceCombo = ' AND ' + @TableReferenceFilter;
                END;

				--Humam 20240112 -- Add Filter Cascade in combo
				DECLARE @FilterCascadeCombo VARCHAR(500)= '';
				IF @FieldNameParent <> ''
				BEGIN
					--Get Parent Field
					SELECT @ParentExtType = FK_ExtType_ID, @ParentFieldType = FK_FieldType_ID
					FROM ModuleDetailField
					WHERE FieldName = @FieldNameParent AND FK_ModuleDetail_ID = @moduleDetailID
					----
					--If ExtType is not FileUpload and RichText
					IF @ParentExtType <> 8 AND @ParentExtType <> 16
					BEGIN
						--SET @FilterCascadeCombo = ' AND ' + REPLACE(@FilterCascade, '@Parent', 'tulv.' + @FieldNameParent);
						IF @ParentFieldType = 11
						BEGIN
							SET @FilterCascadeCombo = ' AND ' + REPLACE(@FilterCascade, '@Parent', 
								'SUBSTRING(' + @FieldNameParent + ', CHARINDEX(''('', ' + @FieldNameParent + ')+1, CHARINDEX('')'', ' + @FieldNameParent + ')-2)'
							);
						END
						ELSE
						BEGIN
							SET @FilterCascadeCombo = ' AND ' + REPLACE(@FilterCascade, '@Parent', @FieldNameParent);
						END
					END
				END;

                --kalau tipenya checkbox
                SET @sql
                    = 'UPDATE ' + @ModuleDetailName
                      + '_Upload SET KeteranganError = KeteranganError + ''Line '' +  CONVERT(VARCHAR(20), nawa_recordnumber)  + '' '
                      + @FieldLabel + ' ('' + fieldcheckboxtovalidate + '') not exist in database '' ' + CHAR(10) + ' FROM ' + @ModuleDetailName
                      + '_Upload  INNER JOIN ( ' + CHAR(10) + '  SELECT * FROM ( SELECT PK_upload_ID,(        '
                      + CHAR(10) + ' SELECT ISNULL( ' + CHAR(10) + '            STUFF( ' + CHAR(10)
                      + '                ( ' 
					  + CHAR(10) + '         SELECT '''+@delimiter+''' + CONVERT(VARCHAR(1000), s.val) ' + CHAR(10)
                      + ' ' + CHAR(10) + '                    FROM   dbo.[Split](' + @FieldName + ', '''+ @delimiter +''') AS s '
                      + CHAR(10) + '                           LEFT JOIN ' + @TabelReferenceName + ' ' + @aliasCombo + ' ' + CHAR(10)
                      + '                                ON  CONVERT(VARCHAR(1000), ' + @aliasCombo + '.'
                      + @TableReferenceFieldKey + ') = CONVERT(VARCHAR(1000), s.val)  ' + @FilterReferenceCombo + @FilterCascadeCombo + CHAR(10)
					  + '					WHERE ' + @aliasCombo + '.' + @TableReferenceFieldKey + ' IS NULL' + CHAR(10)
                      + '                                    FOR XML PATH('''')),1,1,''''),'''')  ' + CHAR(10)
                      + '         )fieldcheckboxtovalidate    from ' + @ModuleDetailName + '_Upload  ' + CHAR(10)
                      + '   WHERE ISNULL( ' + @FieldName + ','''')<>''''  ' + CHAR(10)
                      + '   )xx  )yy ON  ' + @ModuleDetailName
                      + '_Upload.PK_upload_ID=yy.PK_upload_ID      ' + CHAR(10) 
                      + 'WHERE ISNULL(fieldcheckboxtovalidate, '''') <> '''' ' + CHAR(10)

                PRINT (@FieldLabel);
				PRINT ( @sql);
                EXEC (@sql);
            END;
			ELSE 
			BEGIN
			SET @sql
                    = 'update ' + @ModuleDetailName + '_upload set ' + @FieldName + '= ''(''+' + @FieldName + ' from  '
                      + @ModuleDetailName + '_upload where ( '+@FieldName+' <> '''' AND charindex(''('',' + @FieldName + ') =0)';

                PRINT (@FieldLabel);
				PRINT (@sql);
                EXEC (@sql);

                SET @sql
                    = 'update ' + @ModuleDetailName + '_upload set ' + @FieldName + '= ' + @FieldName + '+'')'' from  '
                      + @ModuleDetailName + '_upload where ( '+@FieldName+' <> '''' AND  charindex('')'',' + @FieldName + ') =0)';

                PRINT (@FieldLabel);
				PRINT (@sql);
                EXEC (@sql);
			END;
        END;

        IF @IsUnik = 1
           OR @IsPrimaryKey = 1
        BEGIN
            -- Jika identity : Jika terisi selain numeric maka diupdate jadi kosong.
            IF @FK_FieldType_ID = 12
               OR @FK_FieldType_ID = 15
            BEGIN
                SET @sql
                    = 'UPDATE ' + @ModuleDetailName + '_upload set ' + @FieldName + ' = '''' where isnumeric(' + @FieldName
                      + ') =0 ';
                EXEC (@sql);

				SET @sql = 'UPDATE ' + @ModuleDetailName + '_upload  ' + char(10)
						 + 'SET KeteranganError = KeteranganError + '' Field ' + @FieldLabel + ' is required '' ' + char(10)
						 + 'WHERE ISNULL(' + @FieldName + ','''') = ''''  ' + char(10)
						 + 'AND nawa_userid=''' + @userid + ''' AND (nawa_action=''Update'' OR nawa_Action=''delete'')'
				
				PRINT (@FieldLabel);
				PRINT (@sql);
				EXEC(@sql)
            END;

            IF @intMode = 0
            BEGIN
				IF @FK_FieldType_ID != 12 AND @FK_FieldType_ID != 15
				BEGIN
					IF @FK_FieldType_ID = 10 AND @FK_ExtType_ID = 1
					BEGIN
					DECLARE @Date VARCHAR(500) = 'CONCAT(FORMAT(DATEPART(YEAR,A.'+@FieldName+'), ''0000'') , ''-'' , FORMAT(DATEPART(MONTH,A.'+@FieldName+'), ''00'') , ''-'',FORMAT(DATEPART(Day,A.'+@FieldName+'), ''00''), ''T00:00:00'') '
							--Checking table ModuleDetailApproval
							SET @sql =
								'UPDATE ' +  @ModuleDetailName + '_upload ' + ' SET KeteranganError = KeteranganError + ''Line '' + CONVERT(VARCHAR(20), nawa_recordnumber) + '' : '
								+ @FieldLabel + ' already exist in pending approval '' + CHAR(13) + CHAR(10) FROM ' + @ModuleDetailName + '_upload '  +
								' A INNER JOIN ModuleDetailApproval B ON ( B.ModuleDetailField LIKE ''%"' + @FieldName  + '":"'' + ' + @Date  + ' + ''"%'' OR B.ModuleDetailFieldBefore LIKE ''%"'
								+ @FieldName  + '":"'' + ' + @Date  + ' + ''"%'') AND ( B.ModuleDetailField LIKE ''%"' + @FKFieldName  + '":"'' + A.' + @FKFieldName  + ' + ''"%'' OR B.ModuleDetailFieldBefore LIKE ''%"'
								+ @FKFieldName  + '":"'' + A.' + @FKFieldName  + ' + ''"%'') WHERE nawa_userid = ''' + @userid + '''AND B.FK_ModuleDetail_ID = ' + CONVERT(VARCHAR(50), @moduleDetailID) 
								
								PRINT (@FieldLabel);
								PRINT (@sql);
								EXEC (@sql);

					END
					ELSE IF @FK_FieldType_ID = 10 AND @FK_ExtType_ID = 12
					BEGIN
						DECLARE @Time VARCHAR(500) = 'CONCAT(''1900-01-01T'',FORMAT(DATEPART(HOUR,A.'+@FieldName+'), ''00''),'':'',FORMAT(DATEPART(MINUTE,A.'+@FieldName+'), ''00''),'':'',FORMAT(DATEPART(SECOND,A.'+@FieldName+'), ''00'')) '
							--Checking table ModuleApproval
							SET @sql =
								'UPDATE ' + @ModuleDetailName + '_upload ' + ' SET KeteranganError = KeteranganError + ''Line '' + CONVERT(VARCHAR(20), nawa_recordnumber) + '' : '
								+ @FieldLabel + ' already exist in pending approval '' + CHAR(13) + CHAR(10) FROM ' + @ModuleDetailName + '_upload ' +
								' A INNER JOIN ModuleDetailApproval B ON ( B.ModuleDetailField LIKE ''%"' + @FieldName  + '":"'' + ' + @Date  + ' + ''"%'' OR B.ModuleDetailFieldBefore LIKE ''%"'
								+ @FieldName  + '":"'' + ' + @Date  + ' + ''"%'')  AND ( B.ModuleDetailField LIKE ''%"' + @FKFieldName  + '":"'' + A.' + @FKFieldName  + ' + ''"%'' OR B.ModuleDetailFieldBefore LIKE ''%"'
								+ @FKFieldName  + '":"'' + A.' + @FKFieldName  + ' + ''"%'') WHERE nawa_userid = ''' + @userid + '''AND B.FK_ModuleDetail_ID = ' + CONVERT(VARCHAR(50), @moduleDetailID)
								
								PRINT (@FieldLabel);
								PRINT (@sql);
								EXEC (@sql);
					END

					--Checking table [ModuleDetailName]_upload_approval
					SET @sql =
						'IF OBJECT_ID(''' + @ModuleDetailName + '_Upload_approval'', ''U'') IS NOT NULL AND COL_LENGTH(''' + @ModuleDetailName + '_Upload_approval'', '''+ @FieldName 
						+''') IS NOT NULL BEGIN UPDATE ' + @ModuleDetailName + '_upload ' +
						' SET KeteranganError = KeteranganError + ''Line '' + CONVERT(VARCHAR(20), nawa_recordnumber) + '' : '
						+ @FieldLabel + ' already exist in pending approval '' + CHAR(13) + CHAR(10) WHERE nawa_userid = ''' + @userid +
						''' AND ' + @FieldName  + ' IN (SELECT a.' + @FieldName  + ' FROM ' + @ModuleDetailName + '_Upload_approval a ' +
						'INNER JOIN ' +  @ModuleDetailName + '_Upload b ON a.' + @FieldName + ' = b.' + @FieldName + 
						' WHERE a.' +  @FKFieldName + ' = b.' +  @FKFieldName + ' ) END'
						
						PRINT (@FieldLabel);
						PRINT (@sql);
						EXEC (@sql);

					--Checking table ModuleDetailApproval
					SET @sql =
						'UPDATE ' +  @ModuleDetailName + '_upload ' + ' SET KeteranganError = KeteranganError + ''Line '' + CONVERT(VARCHAR(20), nawa_recordnumber) + '' : '
						+ @FieldLabel + ' already exist in pending approval '' + CHAR(13) + CHAR(10) FROM ' + @ModuleDetailName + '_upload ' +
						' A INNER JOIN ModuleDetailApproval B ON ( B.ModuleDetailField LIKE ''%"' + @FieldName  + '":"'' + A.' + @FieldName  + ' + ''"%'' OR B.ModuleDetailFieldBefore LIKE ''%"'
						+ @FieldName  + '":"'' + A.' + @FieldName  + ' + ''"%'') AND ( B.ModuleDetailField LIKE ''%"' + @FKFieldName  + '":"'' + A.' + @FKFieldName  + ' + ''"%'' OR B.ModuleDetailFieldBefore LIKE ''%"'
						+ @FKFieldName  + '":"'' + A.' + @FKFieldName  + ' + ''"%'') WHERE nawa_userid = ''' + @userid + '''AND B.FK_ModuleDetail_ID = ' + CONVERT(VARCHAR(50), @moduleDetailID) 
						
						PRINT (@FieldLabel);
						PRINT (@sql);
						EXEC (@sql);

					--Checking other row
					SET @sql = 'UPDATE A SET A.KeteranganError = A.KeteranganError + ''Line '' + CONVERT(VARCHAR(20), A.nawa_recordnumber) + '' : '
						+ @FieldLabel + ' already exist in row ('' + CONVERT(VARCHAR(20), B.nawa_recordnumber) + '')'' FROM ' + @ModuleDetailName
						+ '_Upload A INNER JOIN ' + @ModuleDetailName + '_Upload B ON A.' + @FieldName + ' =  B.' + @FieldName
						+ ' WHERE A.nawa_Action IN (''UPDATE'', ''INSERT'') AND A.nawa_userid=''' + @userid
						+ ''' AND B.nawa_Action IN (''UPDATE'', ''INSERT'') AND B.nawa_userid=''' + @userid
						+ ''' AND A.nawa_recordnumber <> B.nawa_recordnumber AND A.' + @FKFieldName + ' = B.' + @FKFieldName
				
					PRINT (@FieldLabel);
					PRINT (@sql);
					EXEC (@sql);
				END

                IF @FK_FieldType_ID <> 11 or  @FK_ExtType_ID = 14 or @FK_ExtType_ID = 15
                BEGIN
					IF(@IsPrimaryKey = 1)
					BEGIN
						SET @sql
							= 'UPDATE ' + @ModuleDetailName
							  + '_upload SET    KeteranganError = KeteranganError + ''Line '' + CONVERT(VARCHAR(20), nawa_recordnumber)         + '' : '
							  + @FieldLabel + ' Not Exist in database'' + CHAR(13) + CHAR(10) FROM   ' + @ModuleDetailName
							  + '_upload        LEFT JOIN (                 SELECT A.PK_upload_ID                 FROM   '
							  + @ModuleDetailName + '_Upload A                        INNER JOIN ' + @ModuleDetailName
							  + ' B                             ON  A.' + @FieldName + ' = b.' + @FieldName
							  + '                 WHERE  a.nawa_Action in (''Delete'',''Update'')     AND a.nawa_userid='''
							  + @userID + '''         )notvalid             ON  notvalid.PK_upload_ID = ' + @ModuleDetailName
							  + '_upload.PK_upload_ID'
							  + ' WHERE  nawa_Action in (''Delete'',''Update'') and notvalid.PK_upload_ID is null AND nawa_userid= '''
							  + @userID + ''' ';

						PRINT (@FieldLabel);
						PRINT (@sql);
						EXEC (@sql);

					END
					ELSE 
					BEGIN
						SET @sql
							= 'UPDATE ' + @ModuleDetailName
							  + '_upload SET    KeteranganError = KeteranganError + ''Line '' + CONVERT(VARCHAR(20), nawa_recordnumber)         + '' : '
							  + @FieldLabel + ' Already Exist in database'' + CHAR(13) + CHAR(10) FROM   '
							  + @ModuleDetailName
							  + '_upload        INNER JOIN (                 SELECT A.PK_upload_ID                 FROM   '
							  + @ModuleDetailName + '_Upload A                        INNER JOIN ' + @ModuleDetailName
							  + ' B                             ON  A.' + @FieldName + ' = CONVERT(VARCHAR(MAX), b.'
							  + @FieldName + ')'
							  + '                 WHERE  a.nawa_Action IN (''INSERT'')   AND a.nawa_userid=''' + @userID
							  + '''  AND a.' + @FKFieldName + ' = b.' + @FKFieldName + '    )notvalid             ON  notvalid.PK_upload_ID = ' + @ModuleDetailName
							  + '_upload.PK_upload_ID';

						PRINT (@FieldLabel);
						PRINT (@sql);
						EXEC (@sql);

						-- Cek Insert Data Type Date
						IF @FK_FieldType_ID = 10
						BEGIN
							SET @sql
							= 'UPDATE ' + @ModuleDetailName
							  + '_upload SET    KeteranganError = KeteranganError + ''Line '' + CONVERT(VARCHAR(20), nawa_recordnumber)         + '' : '
							  + @FieldLabel + ' Already Exist in database'' + CHAR(13) + CHAR(10) FROM   '
							  + @ModuleDetailName
							  + '_upload        INNER JOIN (                 SELECT A.PK_upload_ID                 FROM   '
							  + @ModuleDetailName + '_Upload A                        INNER JOIN ' + @ModuleDetailName
							  + ' B                             ON  A.' + @FieldName + ' = b.'
							  + @FieldName + 
							  + '                 WHERE  a.nawa_Action IN (''INSERT'')   AND a.nawa_userid=''' + @userID
							  + '''  AND a.' + @FKFieldName + ' = b.' + @FKFieldName + '    )notvalid             ON  notvalid.PK_upload_ID = ' + @ModuleDetailName
							  + '_upload.PK_upload_ID';

							PRINT (@FieldLabel);
							PRINT (@sql);
							EXEC (@sql);
						END

						-- Cek Update
						IF @FK_FieldType_ID = 10
						BEGIN
							SET @sql
								= 'UPDATE ' + @ModuleDetailName
								  + '_upload SET    KeteranganError = KeteranganError + ''Line '' + CONVERT(VARCHAR(20), nawa_recordnumber) + '' : '
								  + @FieldLabel + ' Already Exist in database'' + CHAR(13) + CHAR(10) FROM   '
								  + @ModuleDetailName
								  + '_upload        INNER JOIN ( SELECT A.PK_upload_ID FROM   '
								  + @ModuleDetailName + '_Upload A INNER JOIN ' + @ModuleDetailName
								  + ' B ON TRY_CONVERT(Datetime, A.' + @FieldName + ', ' +  CAST(@dateformat as VARCHAR) + ') = TRY_CONVERT(Datetime, B.' + @FieldName + ', ' +  CAST(@dateformat as VARCHAR) + ')'
								  + '  WHERE  a.nawa_Action IN (''UPDATE'')   AND a.nawa_userid=''' + @userID
								  + '''  AND a.' + @FKFieldName + ' = b.' + @FKFieldName + '  AND a.' + @primaryKeyFieldName + ' != b.' + @primaryKeyFieldName + '   )notvalid '              
								  + ' ON  notvalid.PK_upload_ID = ' + @ModuleDetailName
								  + '_upload.PK_upload_ID';	
						END						
						ELSE
						BEGIN
							SET @sql
								= 'UPDATE ' + @ModuleDetailName
								  + '_upload SET    KeteranganError = KeteranganError + ''Line '' + CONVERT(VARCHAR(20), nawa_recordnumber)         + '' : '
								  + @FieldLabel + ' Already Exist in database'' + CHAR(13) + CHAR(10) FROM   '
								  + @ModuleDetailName
								  + '_upload        INNER JOIN (                 SELECT A.PK_upload_ID                 FROM   '
								  + @ModuleDetailName + '_Upload A                        INNER JOIN ' + @ModuleDetailName
								  + ' B                             ON  A.' + @FieldName + ' = CONVERT(VARCHAR(MAX), b.'
								  + @FieldName + ')'
								  + '                 WHERE  a.nawa_Action IN (''UPDATE'')   AND a.nawa_userid=''' + @userID
								  + '''  AND a.' + @FKFieldName + ' = b.' + @FKFieldName + '  AND a.' + @primaryKeyFieldName + ' != b.' + @primaryKeyFieldName + '   )notvalid ' 
								  + '            ON  notvalid.PK_upload_ID = ' + @ModuleDetailName
								  + '_upload.PK_upload_ID';	
						END

						PRINT (@FieldLabel);
						PRINT (@sql);
						EXEC (@sql);
					END

                END;
        ELSE
                BEGIN
                    SET @sql
                        = 'UPDATE ' + @ModuleDetailName
                          + '_upload SET    KeteranganError = KeteranganError + ''Line '' + CONVERT(VARCHAR(20), nawa_recordnumber)         + '' : '
                          + @FieldLabel + ' Already Exist in database'' + CHAR(13) + CHAR(10) FROM   '
                          + @ModuleDetailName
                          + '_upload        INNER JOIN (                 SELECT A.PK_upload_ID                 FROM   '
                          + @ModuleDetailName + '_Upload A                        INNER JOIN ' + @ModuleDetailName
                          + ' B                             ON  SUBSTRING( A.' + @FieldName + ',CHARINDEX(''('',A.'
                          + @FieldName + ') +1 ,CHARINDEX('')'',A.' + @FieldName + ')-2)'
                          + ' = CONVERT(VARCHAR(MAX), b.' + @FieldName + ')'
                          + '                 WHERE  a.nawa_Action = ''Insert''    AND a.nawa_userid=''' + @userID
                          + '''  AND a.' + @FKFieldName + ' = b.' + @FKFieldName + '       )notvalid   ON  notvalid.PK_upload_ID = ' + @ModuleDetailName
                          + '_upload.PK_upload_ID';

                    PRINT (@FieldLabel);
					PRINT (@sql);
                    EXEC (@sql);

                    SET @sql = 'UPDATE ' + @ModuleDetailName +'_Upload SET    KeteranganError = KeteranganError + ''Line '' + CONVERT(VARCHAR(20), nawa_recordnumber)         + '' : '
	                    + @FieldLabel +
	                    ' Already Exist in database  '' + CHAR(13) + CHAR(10) FROM   ' +
	                    @ModuleDetailName +'_Upload        INNER JOIN (                 SELECT A.PK_upload_ID                 FROM   '
	                    + @ModuleDetailName +'_Upload A                        INNER JOIN ' +
	                    @ModuleDetailName + ' B                             ON  SUBSTRING( A.' + @FieldName +
	                    ',CHARINDEX(''('',A.' + @FieldName + ') +1 ,CHARINDEX('')'',A.' + @FieldName + ')-2)'
	                    + ' = b.' + @FieldName +
	                    '                 WHERE  a.nawa_Action in (''Delete'',''Update'')     AND a.nawa_userid='''
	                    + @userid +
	                    ''' AND a.' + @FKFieldName + ' = b.' + @FKFieldName + ' AND a.' + @primaryKeyFieldName + ' != b.' + @primaryKeyFieldName + ' )notvalid             ON  notvalid.PK_upload_ID = ' +
	                    @ModuleDetailName +'_Upload.PK_upload_ID' +
	                    ' WHERE  nawa_Action in (''Delete'',''Update'') AND nawa_userid=''' + @userid +
						''''
                    PRINT (@FieldLabel);
					PRINT (@sql);
                    EXEC (@sql);
                END;
            END;
        END;

        IF @FK_FieldType_ID = 10
        --datetime
        BEGIN
            IF @Required = 1
            BEGIN
                SET @sql
                    = 'UPDATE ' + @ModuleDetailName
                      + '_upload SET KeteranganError = KeteranganError + ''Line '' + CONVERT(VARCHAR(20), nawa_recordnumber) + '' : '
                      + @FieldLabel + ' is not a valid date'' + CHAR(13) + CHAR(10) FROM ' + @ModuleDetailName
                      + '_upload INNER JOIN ( SELECT A.PK_upload_ID FROM ' + @ModuleDetailName
                      + '_Upload A WHERE (a.nawa_Action = ''Insert'' OR a.nawa_Action=''Update'' ) AND a.nawa_userid='''
                      + @userID + ''' AND TRY_CONVERT(DATETIME, ' + @FieldName + ',' + CAST(@dateformat AS VARCHAR) + ' ) is null  )notvalid ON notvalid.PK_upload_ID = ' + @ModuleDetailName + '_upload.PK_upload_ID';
				
            END;
            ELSE
            BEGIN
                --ada update kalau null dianggap valid kalau ngak required
                SET @sql
                    = 'UPDATE ' + @ModuleDetailName
                      + '_upload SET KeteranganError = KeteranganError + ''Line '' + CONVERT(VARCHAR(20), nawa_recordnumber) + '' : '
                      + @FieldLabel + ' is not a valid date'' + CHAR(13) + CHAR(10) FROM ' + @ModuleDetailName
                      + '_upload INNER JOIN ( SELECT A.PK_upload_ID FROM ' + @ModuleDetailName
                      + '_Upload A WHERE (a.nawa_Action = ''Insert'' OR a.nawa_Action=''Update'' ) AND a.nawa_userid='''
                      + @userID + ''' AND TRY_CONVERT(DATETIME, ' + @FieldName + ',' + CAST(@dateformat AS VARCHAR) + ') is null  AND ( a.' + @FieldName + '<>''null'' and a.' + @FieldName
                      + '<>'''' ))notvalid ON notvalid.PK_upload_ID = ' + @ModuleDetailName + '_upload.PK_upload_ID';
			
            END;
            PRINT (@FieldLabel);
								PRINT (@sql);
            EXEC (@sql);
        END;

        IF @FK_FieldType_ID = 9 -- varchar
        BEGIN
            --cek maxlength jika terisi
            IF @SizeField > 0
            BEGIN
                SET @sql
                    = 'UPDATE ' + @ModuleDetailName
                      + '_upload SET KeteranganError = KeteranganError + ''Line '' + CONVERT(VARCHAR(20), nawa_recordnumber) + '' : '
                      + @FieldLabel + ' Max Length is ' + CONVERT(VARCHAR(20), @SizeField)
                      + ' char'' + CHAR(13)  + CHAR(10) FROM ' + @ModuleDetailName
                      + '_upload INNER JOIN ( SELECT A.PK_upload_ID FROM ' + @ModuleDetailName
                      + '_Upload A WHERE (a.nawa_Action = ''Insert'' OR a.nawa_Action=''Update'' ) AND a.nawa_userid='''
                      + @userID + ''' AND len(LTRIM(rtrim(a.' + @FieldName + ')))>' + CONVERT(VARCHAR(20), @SizeField)
                      + ' )notvalid ON notvalid.PK_upload_ID = ' + @ModuleDetailName + '_upload.PK_upload_ID';
				PRINT (@FieldLabel);
								PRINT (@sql);
                EXEC (@sql);
            END;
        END;

		--Boolean
		IF @FK_FieldType_ID = 13
		BEGIN
			SET @sql = 'UPDATE ' + @ModuleDetailName + '_upload SET KeteranganError = KeteranganError + ''Line '' + CONVERT(VARCHAR(20), nawa_recordnumber) + '' : '
	            + @FieldLabel + ' can only be True or False  '' + CHAR(13) + CHAR(10) WHERE (nawa_Action = ''Insert'' OR nawa_Action=''Update'' ) AND nawa_userid=''' +
	            @userid + ''' AND ' + @FieldName + ' != ''True'' AND ' + @FieldName + ' != ''False'' AND ' +  @FieldName + ' != '''' '

				PRINT (@FieldLabel);
								PRINT (@sql);
            EXEC (@sql);
		END;

        IF @FK_FieldType_ID = 1
           OR @FK_FieldType_ID = 2
           OR @FK_FieldType_ID = 3
           OR @FK_FieldType_ID = 4
           OR @FK_FieldType_ID = 5
           OR @FK_FieldType_ID = 6
           OR @FK_FieldType_ID = 7
           OR @FK_FieldType_ID = 8
        BEGIN
            SET @sql
                = 'UPDATE ' + @ModuleDetailName + '_upload ' + CHAR(10)
                  + 'SET    KeteranganError = KeteranganError + ''Line '' + CONVERT(VARCHAR(20), nawa_recordnumber)  '
                  + CHAR(10) + '       + '' : ' + @FieldLabel + ' range must  ' + @FieldTypeDescription
                  + ''' + CHAR(13) + CHAR(10) ' + CHAR(10) + 'FROM   ' + @ModuleDetailName + '_upload ' + CHAR(10)
                  + '       INNER JOIN ( ' + CHAR(10) + '                SELECT A.PK_upload_ID ' + CHAR(10)
                  + '                FROM   ' + @ModuleDetailName + '_Upload A ' + CHAR(10) + '                        '
                  + CHAR(10)
                  + '                WHERE  (a.nawa_Action = ''Insert'' OR a.nawa_Action=''Update'' ) AND a.nawa_userid='''
                  + @userID + ''' ' + CHAR(10) + '                AND  try_cast(a.' + @FieldName
                  + ' AS  '+ @FieldTypeSQLName +') IS NULL  ' + CHAR(10) + '       AND a.' + @FieldName + ' IS NOT null    '
                  + CHAR(10) + '            )notvalid ' + CHAR(10) + '            ON  notvalid.PK_upload_ID = '
                  + @ModuleDetailName + '_upload.PK_upload_ID';

            PRINT (@FieldLabel);
								PRINT (@sql);
            EXEC (@sql);
        END;

        IF @FK_ExtType_ID = 12
        BEGIN
            SET @sql
                = 'UPDATE ' + @ModuleDetailName + '_upload ' + CHAR(10)
                  + 'SET    KeteranganError = KeteranganError + ''Line '' + CONVERT(VARCHAR(20), nawa_recordnumber)  '
                  + CHAR(10) + '       + '' : ' + @FieldLabel + ' Format must  ' + @defaultTimeFormat
                  + ''' + CHAR(13) + CHAR(10) ' + CHAR(10) + 'FROM   ' + @ModuleDetailName + '_upload ' + CHAR(10)
                  + '       INNER JOIN ( ' + CHAR(10) + '            SELECT A.PK_upload_ID ' + CHAR(10)
                  + '                FROM   ' + @ModuleDetailName + '_Upload A ' + CHAR(10) + '                        '
                  + CHAR(10)
                  + '                WHERE  (a.nawa_Action = ''Insert'' OR a.nawa_Action=''Update'' ) AND a.nawa_userid='''
                  + @userID + ''' ' + CHAR(10) + '                AND  try_cast(a.' + @FieldName
                  + ' AS tIME ) IS NULL  ' + CHAR(10) + '                AND a.' + @FieldName + ' IS NOT null    '
                  + CHAR(10) + '            )notvalid ' + CHAR(10) + '            ON  notvalid.PK_upload_ID = '
                  + @ModuleDetailName + '_upload.PK_upload_ID';

				  PRINT (@FieldLabel);
								PRINT (@sql);
            EXEC (@sql);
        END;


        FETCH FROM my_cursor
        INTO @PK_ModuleDetailField_ID,
             @FK_ModuleDetail_ID,
             @FieldName,
             @FieldLabel,
             @Sequence,
             @Required,
             @IsPrimaryKey,
             @IsUnik,
             @IsShowInView,
             @FK_FieldType_ID,
             @SizeField,
             @FK_ExtType_ID,
             @TabelReferenceName,
             @TabelReferenceNameAlias,
             @TableReferenceFieldKey,
             @TableReferenceFieldDisplayName,
             @TableReferenceFilter,
             @IsUseRegexValidation,
			 @FieldNameParent,
			 @FilterCascade,
             @ModuleDetailName,
             @FieldTypeDescription,
			 @FieldTypeSQLName
    END;
    CLOSE my_cursor;
    DEALLOCATE my_cursor;

	/*Cek Action Delete Parent*/
	EXEC usp_DeleteRelatedChildDataFromUpload @modulename, @userid

	SET @sql = 'UPDATE ' + @modulename + '_upload'
				+ ' SET KeteranganError = KeteranganError + ''There is an error on module detail: '+@strModuleDetailName+'.''+ CHAR(13) + CHAR(10) ' 
				+ ' WHERE PK_UPLOAD_ID IN (' 
				+ ' SELECT HeadTableUpload.PK_upload_ID ' 
				+ ' FROM ' + @strModuleDetailName + '_upload' + ' DetTableUpload ' 
				+ ' JOIN ' + @modulename + '_upload' + ' HeadTableUpload ON DetTableUpload.' + @fieldFK + ' = HeadTableUpload.' + @fieldParent
				+ ' AND HeadTableUpload.nawa_userid = DetTableUpload.nawa_userid'
				+ ' WHERE DetTableUpload.KeteranganError <> '''' AND HeadTableUpload.nawa_userid = ''' + @userID + ''' ' 
				+ ' GROUP BY HeadTableUpload.PK_upload_ID )';
    EXEC (@sql);
	------------------------------------------------------------------------

    /* Tambah untuk Insert jika NULL, ID Identity generate otomatis*/
    IF EXISTS
    (
        SELECT 1
        FROM ModuleDetailField mf
        WHERE mf.FK_ModuleDetail_ID = @FK_ModuleDetail_ID
              AND mf.IsPrimaryKey = 1
              AND
              (
                  mf.FK_FieldType_ID = 12
                  OR mf.FK_FieldType_ID = 15
              )
    )
    BEGIN
        DECLARE @FieldPrimary AS VARCHAR(MAX) = '';
        SELECT TOP 1
               @FieldPrimary = mf.FieldName
        FROM ModuleDetailField mf
        WHERE mf.FK_ModuleDetail_ID = @FK_ModuleDetail_ID
              AND mf.IsPrimaryKey = 1
              AND
              (
                  mf.FK_FieldType_ID = 12
                  OR mf.FK_FieldType_ID = 15
              );
        SET @sql
            = 'UPDATE ' + @ModuleDetailName + '_upload SET ' + @FieldPrimary + ' = (SELECT ISNULL(MAX(' + @FieldPrimary
              + '), 0) FROM ' + @strModuleDetailName
              + ') + ISNULL(nawa_recordnumber, 0)
    WHERE nawa_Action=''insert'' AND nawa_userid=''' + @userID + '''
     AND ISNULL('  + @FieldPrimary + ', '''') = ''''';
        --PRINT (@sql)
        EXEC (@sql);
    END;
	
    --This part has to be discussed further.
    --DECLARE @IntCount AS INT;
    --SELECT @IntCount = COUNT(1)
    --FROM ValidationParameter vp
    --WHERE vp.TableName = @moduleDetailID
    --      AND vp.Active = 1;

    --IF @IntCount > 0
    --BEGIN
    --    EXEC [usp_ValidateUploadFormModule_FromUpload] @moduleDetailID, @userID;
    --END;
END;
