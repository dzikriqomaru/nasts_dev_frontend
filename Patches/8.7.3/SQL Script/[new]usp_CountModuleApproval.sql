-- Active: 1708397783688@@ndsrndserver.southeastasia.cloudapp.azure.comSQL2016@1333@NawadataTemplateNF8AlphaRelease@dbo
ALTER
PROCEDURE [dbo].[usp_CountModuleApproval] @ModuleKey VARCHAR(1000),
@UserID VARCHAR(50),
@Search VARCHAR(MAX),
@filterKey VARCHAR(50),
@filterValue VARCHAR(MAX),
@RoleID INT,
@dataAccess VARCHAR(MAX) AS DECLARE @PK_MUser_ID INT
SELECT @PK_MUser_ID = PK_MUser_ID
FROM MUser
WHERE
    UserID = @UserID

DECLARE @query VARCHAR(MAX) DECLARE @DateFormat VARCHAR(MAX) DECLARE @DateFormatSysParam VARCHAR(MAX)
SELECT @DateFormatSysParam = SettingValue
FROM SystemParameter
WHERE
    PK_SystemParameter_ID = 6
SELECT @DateFormat = SQLFormat
FROM MDateFormat
WHERE
    PK_DateFormat_ID = CAST(@DateFormatSysParam AS bigint)

DECLARE @TimeFormat VARCHAR(MAX) DECLARE @TimeFormatSysParam VARCHAR(MAX)

DECLARE @ISShowModuleKey VARCHAR(MAX)
SELECT @ISShowModuleKey = SettingValue
FROM SystemParameter
WHERE
    PK_SystemParameter_ID = -130

SELECT @TimeFormatSysParam = SettingValue
FROM SystemParameter
WHERE
    PK_SystemParameter_ID = 33
SELECT @TimeFormat = SQLFormat
FROM MTimeFormat
WHERE
    PK_TimeFormat_ID = CAST(@TimeFormatSysParam AS bigint)

DECLARE @DateTimeFormat VARCHAR(MAX) = @DateFormat + ' ' + @TimeFormat

IF (
    len(@Search) > 0
    OR len(@filterKey) <= 0
) BEGIN IF EXISTS (
    SELECT 13
    FROM Module_TR_WorkFlow
    WHERE
        FK_ModuleID = @ModuleKey
        AND Active = 1
) BEGIN
SET
    @query = '
		SELECT COUNT(13) NeedApproval
		FROM ModuleApproval
		JOIN Module
			ON ModuleApproval.ModuleName = Module.ModuleName
		JOIN ModuleAction
			ON ModuleApproval.PK_ModuleAction_ID = ModuleAction.PK_ModuleAction_ID
		INNER JOIN MWorkFlow_Progress Workflow
			ON Workflow.FK_Module_ID = Module.PK_Module_ID
			AND (
				Workflow.FK_Unik_ID = ModuleApproval.ModuleKey AND ModuleApproval.PK_ModuleAction_ID <> 7
				OR Workflow.FK_Unik_ID = CONVERT(VARCHAR(50), ModuleApproval.PK_ModuleApproval_ID) + ''Import'' AND ModuleApproval.PK_ModuleAction_ID = 7
			)
		WHERE 
			PK_Module_ID = ' + CAST(@ModuleKey AS VARCHAR(MAX)) + '
			AND (
				(
					(
						FK_MWorkFlowUserType_ID = 1
						OR FK_MWorkFlowUserType_ID = 2
					)
					AND FK_MWorkflowUser_ID in (
						--get all parent user from previous user upline (user structure) by userid of current user
						SELECT muse2.FK_Parent_ID 
						FROM MUserStructure muse1
						join MUserStructure muse2 on muse1.FK_User_ID = muse2.FK_User_ID
						where muse1.FK_Parent_ID = ' + CAST(@PK_MUser_ID AS VARCHAR(MAX)) + '
					)
					AND FK_MWorkflow_Status_ID = 3
				)
				OR (
					FK_MWorkFlowUserType_ID = 4
					AND FK_MWorkflowRole_ID = ' + CAST(@RoleID AS VARCHAR(MAX)) + '
					AND FK_MWorkflow_Status_ID = 3
				)
			)
			AND (
				PK_ModuleApproval_ID LIKE ''%' + CAST(@Search AS VARCHAR(MAX)) + '%''
				OR ModuleApproval.ModuleName LIKE ''%' + CAST(@Search AS VARCHAR(MAX)) + '%''
				-- OR ModuleApproval.ModuleField LIKE ''%' + CAST(@Search AS VARCHAR(MAX)) + '%''
				-- OR ModuleApproval.ModuleFieldBefore LIKE ''%' + CAST(@Search AS VARCHAR(MAX)) + '%''
                
                ' + CASE
        WHEN @ISShowModuleKey = '2' THEN 'OR ModuleKey LIKE ''%' + CAST(@Search AS VARCHAR(MAX)) + '%'''
        ELSE ''
    END + '

				OR ModuleActionName LIKE ''%' + CAST(@Search AS VARCHAR(MAX)) + '%''
				OR FORMAT(ModuleApproval.CreatedDate, ''' + CAST(
        @DateTimeFormat AS VARCHAR(MAX)
    ) + ''') LIKE ''%' + CAST(@Search AS VARCHAR(MAX)) + '%''
				OR ModuleApproval.CreatedBy LIKE ''%' + CAST(@Search AS VARCHAR(MAX)) + '%''
			)
			AND NOT EXISTS (
				--exclude users who have previously processed approval
				SELECT 13
				FROM MWorkFlow_History AS mfh 
					JOIN MWorkflow_ApprovalStatus AS mas ON mfh.FK_MWorkflow_ApprovalStatus_ID=mas.PK_MWorkflow_ApprovalStatus_ID
				WHERE mfh.FK_ModuleApproval_ID = CAST(ModuleApproval.PK_ModuleApproval_ID AS VARCHAR(MAX))
					AND mfh.FK_MWorkflow_ApprovalStatus_ID IN (1,4)
					AND mfh.FK_MUserId = ' + CAST(@PK_MUser_ID AS VARCHAR(MAX)) + '
			)';

IF (@dataAccess <> '') BEGIN
SET
    @query = @query + ' AND ' + @dataAccess END

EXEC (@query);

END ELSE BEGIN
SET
    @query = '
		SELECT COUNT(13) NeedApproval
		FROM ModuleApproval
		JOIN Module
			ON ModuleApproval.ModuleName = Module.ModuleName
		JOIN ModuleAction
			ON ModuleApproval.PK_ModuleAction_ID = ModuleAction.PK_ModuleAction_ID
		JOIN MUser UserCreator
			ON ModuleApproval.CreatedBy = UserCreator.UserID
		WHERE 
			PK_Module_ID = ' + CAST(@ModuleKey AS VARCHAR(MAX)) + '
			AND ModuleApproval.CreatedBy <> ''' + CAST(@UserID AS VARCHAR(MAX)) + '''
			AND (
				PK_ModuleApproval_ID LIKE ''%' + CAST(@Search AS VARCHAR(MAX)) + '%''
				OR ModuleApproval.ModuleName LIKE ''%' + CAST(@Search AS VARCHAR(MAX)) + '%''
				-- OR ModuleApproval.ModuleField LIKE ''%' + CAST(@Search AS VARCHAR(MAX)) + '%''
				-- OR ModuleApproval.ModuleFieldBefore LIKE ''%' + CAST(@Search AS VARCHAR(MAX)) + '%''
				-- OR ModuleKey LIKE ''%' + CAST(@Search AS VARCHAR(MAX)) + '%''

                ' + CASE
        WHEN @ISShowModuleKey = '2' THEN 'OR ModuleKey LIKE ''%' + CAST(@Search AS VARCHAR(MAX)) + '%'''
        ELSE ''
    END + '

				OR ModuleActionName LIKE ''%' + CAST(@Search AS VARCHAR(MAX)) + '%''
				OR FORMAT(ModuleApproval.CreatedDate, ''' + CAST(
        @DateTimeFormat AS VARCHAR(MAX)
    ) + ''') LIKE ''%' + CAST(@Search AS VARCHAR(MAX)) + '%''
				OR ModuleApproval.CreatedBy LIKE ''%' + CAST(@Search AS VARCHAR(MAX)) + '%''
			)
			AND ISNULL(ModuleApproval.FK_MRole_ID, UserCreator.FK_MRole_ID) = ' + CAST(@RoleID AS VARCHAR(MAX));

IF (@dataAccess <> '') BEGIN
SET
    @query = @query + ' AND ' + @dataAccess END

EXEC (@query);

END END ELSE BEGIN IF EXISTS (
    SELECT 13
    FROM Module_TR_WorkFlow
    WHERE
        FK_ModuleID = @ModuleKey
        AND Active = 1
) BEGIN
SET
    @query = '
			SELECT COUNT(13) NeedApproval
			FROM ModuleApproval
			JOIN Module
				ON ModuleApproval.ModuleName = Module.ModuleName
			JOIN ModuleAction
				ON ModuleApproval.PK_ModuleAction_ID = ModuleAction.PK_ModuleAction_ID
			INNER JOIN MWorkFlow_Progress Workflow
				ON Workflow.FK_Module_ID = Module.PK_Module_ID
				AND (
					Workflow.FK_Unik_ID = ModuleApproval.ModuleKey AND ModuleApproval.PK_ModuleAction_ID <> 7
					OR Workflow.FK_Unik_ID = CONVERT(VARCHAR(50), ModuleApproval.PK_ModuleApproval_ID) + ''Import'' AND ModuleApproval.PK_ModuleAction_ID = 7
				)
			WHERE 
				PK_Module_ID = ' + CAST(@ModuleKey AS VARCHAR(MAX)) + '
				AND (
					(
						(
							FK_MWorkFlowUserType_ID = 1
							OR FK_MWorkFlowUserType_ID = 2
						)
						AND FK_MWorkflowUser_ID in (
							--get all parent user from previous user upline (user structure) by userid of current user
							SELECT muse2.FK_Parent_ID 
							FROM MUserStructure muse1
							join MUserStructure muse2 on muse1.FK_User_ID = muse2.FK_User_ID
							where muse1.FK_Parent_ID = ' + CAST(@PK_MUser_ID AS VARCHAR(MAX)) + '
						)
						AND FK_MWorkflow_Status_ID = 3
					)
					OR (
						FK_MWorkFlowUserType_ID = 4
						AND FK_MWorkflowRole_ID = ' + CAST(@RoleID AS VARCHAR(MAX)) + '
						AND FK_MWorkflow_Status_ID = 3
					)
				)
				AND (
					(''' + CAST(@filterKey AS VARCHAR(MAX)) + ''' = ''PK_ModuleApproval_ID'' AND PK_ModuleApproval_ID LIKE ''%' + CAST(@filterValue AS VARCHAR(MAX)) + '%'')
					OR (''' + CAST(@filterKey AS VARCHAR(MAX)) + ''' = ''ModuleApproval.ModuleName'' AND ModuleApproval.ModuleName LIKE ''%' + CAST(@filterValue AS VARCHAR(MAX)) + '%'')
					-- OR (''' + CAST(@filterKey AS VARCHAR(MAX)) + ''' = ''ModuleApproval.ModuleField'' AND ModuleApproval.ModuleField LIKE ''%' + CAST(@filterValue AS VARCHAR(MAX)) + '%'')
					-- OR (''' + CAST(@filterKey AS VARCHAR(MAX)) + ''' = ''ModuleApproval.ModuleFieldBefore'' AND ModuleApproval.ModuleFieldBefore LIKE ''%' + CAST(@filterValue AS VARCHAR(MAX)) + '%'')
					-- OR (''' + CAST(@filterKey AS VARCHAR(MAX)) + ''' = ''ModuleKey'' AND ModuleKey LIKE ''%' + CAST(@filterValue AS VARCHAR(MAX)) + '%'')

                    ' + CASE
        WHEN @ISShowModuleKey = '2' THEN 'OR ModuleKey LIKE ''%' + CAST(@filterValue AS VARCHAR(MAX)) + '%'''
        ELSE ''
    END + '

					OR (''' + CAST(@filterKey AS VARCHAR(MAX)) + ''' = ''ModuleActionName'' AND ModuleActionName LIKE ''%' + CAST(@filterValue AS VARCHAR(MAX)) + '%'' )
					OR (''' + CAST(@filterKey AS VARCHAR(MAX)) + ''' = ''ModuleApproval.CreatedDate'' AND FORMAT(ModuleApproval.CreatedDate, ''' + CAST(
        @DateTimeFormat AS VARCHAR(MAX)
    ) + ''') LIKE ''%' + CAST(@filterValue AS VARCHAR(MAX)) + '%'' )
					OR (''' + CAST(@filterKey AS VARCHAR(MAX)) + ''' = ''ModuleApproval.CreatedBy'' AND ModuleApproval.CreatedBy LIKE ''%' + CAST(@filterValue AS VARCHAR(MAX)) + '%'' )
				)
				AND NOT EXISTS (
					--exclude users who have previously processed approval
					SELECT 13
					FROM MWorkFlow_History AS mfh 
						JOIN MWorkflow_ApprovalStatus AS mas ON mfh.FK_MWorkflow_ApprovalStatus_ID=mas.PK_MWorkflow_ApprovalStatus_ID
					WHERE mfh.FK_ModuleApproval_ID = CAST(ModuleApproval.PK_ModuleApproval_ID AS VARCHAR(MAX))
						AND mfh.FK_MWorkflow_ApprovalStatus_ID IN (1,4)
						AND mfh.FK_MUserId = ' + CAST(@PK_MUser_ID AS VARCHAR(MAX)) + '
				)';

PRINT (@query);

IF (@dataAccess <> '') BEGIN
SET
    @query = @query + ' AND ' + @dataAccess END

EXEC (@query);

END ELSE BEGIN
SET
    @query = '
		SELECT COUNT(13) NeedApproval
		FROM ModuleApproval
		JOIN Module
			ON ModuleApproval.ModuleName = Module.ModuleName
		JOIN ModuleAction
			ON ModuleApproval.PK_ModuleAction_ID = ModuleAction.PK_ModuleAction_ID
		JOIN MUser UserCreator
			ON ModuleApproval.CreatedBy = UserCreator.UserID
		WHERE 
			PK_Module_ID = ' + CAST(@ModuleKey AS VARCHAR(MAX)) + '
			AND ModuleApproval.CreatedBy <> ''' + CAST(@UserID AS VARCHAR(MAX)) + '''
			AND (
				(''' + CAST(@filterKey AS VARCHAR(MAX)) + ''' = ''PK_ModuleApproval_ID'' AND PK_ModuleApproval_ID LIKE ''%' + CAST(@filterValue AS VARCHAR(MAX)) + '%'')
				OR (''' + CAST(@filterKey AS VARCHAR(MAX)) + ''' = ''ModuleApproval.ModuleName'' AND ModuleApproval.ModuleName LIKE ''%' + CAST(@filterValue AS VARCHAR(MAX)) + '%'')
				-- OR (''' + CAST(@filterKey AS VARCHAR(MAX)) + ''' = ''ModuleApproval.ModuleField'' AND ModuleApproval.ModuleField LIKE ''%' + CAST(@filterValue AS VARCHAR(MAX)) + '%'')
				-- OR (''' + CAST(@filterKey AS VARCHAR(MAX)) + ''' = ''ModuleApproval.ModuleFieldBefore'' AND ModuleApproval.ModuleFieldBefore LIKE ''%' + CAST(@filterValue AS VARCHAR(MAX)) + '%'')
				-- OR (''' + CAST(@filterKey AS VARCHAR(MAX)) + ''' = ''ModuleKey'' AND ModuleKey LIKE ''%' + CAST(@filterValue AS VARCHAR(MAX)) + '%'')

                ' + CASE
        WHEN @ISShowModuleKey = '2' THEN 'OR ModuleKey LIKE ''%' + CAST(@filterValue AS VARCHAR(MAX)) + '%'''
        ELSE ''
    END + '

				OR (''' + CAST(@filterKey AS VARCHAR(MAX)) + ''' = ''ModuleActionName'' AND ModuleActionName LIKE ''%' + CAST(@filterValue AS VARCHAR(MAX)) + '%'')
				OR (''' + CAST(@filterKey AS VARCHAR(MAX)) + ''' = ''ModuleApproval.CreatedDate'' AND FORMAT(ModuleApproval.CreatedDate, ''' + CAST(
        @DateTimeFormat AS VARCHAR(MAX)
    ) + ''') LIKE ''%' + CAST(@filterValue AS VARCHAR(MAX)) + '%'' )
				OR (''' + CAST(@filterKey AS VARCHAR(MAX)) + ''' = ''ModuleApproval.CreatedBy'' AND ModuleApproval.CreatedBy LIKE ''%' + CAST(@filterValue AS VARCHAR(MAX)) + '%'' )
			)
			AND ISNULL(ModuleApproval.FK_MRole_ID, UserCreator.FK_MRole_ID) = ' + CAST(@RoleID AS VARCHAR(MAX));

IF (@dataAccess <> '') BEGIN
SET
    @query = @query + ' AND ' + @dataAccess END

EXEC (@query);

END END