IF NOT EXISTS (SELECT 1 FROM SystemParameter WHERE SettingName = 'SizeTooltip')

BEGIN
   INSERT INTO SystemParameter
( -- columns to insert data into
 [PK_SystemParameter_ID],[FK_SystemParameterGroup_ID], [SettingName], [SettingValue],[Active],[Hide],[fk_MFieldType_ID],[IsEncript],[EncriptionKey]
)
VALUES
( -- first row: values for the columns in the list above
 90,5, 'SizeTooltip', 700, 1,0,2,0,NULL
)

END
