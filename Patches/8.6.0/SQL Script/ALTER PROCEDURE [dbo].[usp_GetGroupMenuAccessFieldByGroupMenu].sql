ALTER PROCEDURE [dbo].[usp_GetGroupMenuAccessFieldByGroupMenu]    
(@GroupMenuID INT)    
AS    
BEGIN    
	SELECT 
		ISNULL(Field.PK_MGroupMenuAccessField_ID, 0) AS PK_MGroupMenuAccessField_ID,
		@GroupMenuID AS FK_MGroupMenu_ID,
		Mo.PK_Module_ID AS FK_Module_ID,
		MF.FieldName AS ModuleField,
		CASE WHEN isnull(Mo.IsSupportAdd, 0) = 1 THEN isnull(Field.bAdd, 1) ELSE 0 END bAdd,    
		CASE WHEN isnull(Mo.IsSupportEdit, 0) = 1 THEN isnull(Field.bEdit, 1) ELSE 0 END bEdit,    
		CASE WHEN isnull(Mo.IsSupportDelete, 0) = 1 THEN isnull(Field.bDelete, 1) ELSE 0 END bDelete,    
		CASE WHEN isnull(Mo.IsSupportActivation, 0) = 1 THEN isnull(Field.bActivation, 1) ELSE 0 END bActivation,    
		CASE WHEN isnull(Mo.IsSupportView, 0) = 1 THEN isnull(Field.bView, 1) ELSE 0 END bView,    
		CASE WHEN isnull(Mo.IsUseApproval, 0) = 1 THEN isnull(Field.bApproval, 1) ELSE 0 END bApproval,    
		CASE WHEN isnull(Mo.IsSupportUpload, 0) = 1 THEN isnull(Field.bUpload, 1) ELSE 0 END bUpload,    
		CASE WHEN isnull(Mo.IsSupportDetail, 0) = 1 THEN isnull(Field.bDetail, 1) ELSE 0 END bDetail
	FROM Module Mo
	INNER JOIN ModuleField MF 
		ON Mo.PK_Module_ID = MF.FK_Module_ID
	LEFT JOIN MGroupMenuAccessField Field 
		ON Field.FK_Module_ID = Mo.PK_Module_ID
		AND Field.ModuleField = MF.FieldName
		AND Field.FK_MGroupMenu_ID = @GroupMenuID
	--WHERE 
	--	Mo.IsUseDesigner = 1
END

