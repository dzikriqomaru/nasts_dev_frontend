BEGIN TRAN

BEGIN TRY

SET IDENTITY_INSERT Module ON

INSERT INTO Module (PK_Module_ID,ModuleName,ModuleLabel,ModuleDescription,IsUseDesigner,IsUseApproval,IsSupportAdd,IsSupportEdit,IsSupportDelete,IsSupportActivation,IsSupportView,IsSupportUpload,IsSupportDetail,UrlAdd,UrlEdit,UrlDelete,UrlActivation,UrlView,UrlUpload,UrlApproval,UrlApprovalDetail,UrlDetail,IsUseStoreProcedureValidation,Active,CreatedBy,LastUpdateBy,ApprovedBy,CreatedDate,LastUpdateDate,ApprovedDate) 
VALUES (1050,'ModuleHideApprovalAll','Module Hide Approval All','',1,1,1,1,1,1,1,1,1,'/ParameterInput','/ParameterInput','/ParameterDetail','/ParameterDetail','/ParameterView','/ParameterUpload','/ParameterApproval','/??','/ParameterDetail',0,1,'sysadmin','sysadmin','sysadmin',GETDATE(),GETDATE(),GETDATE())

SET IDENTITY_INSERT Module OFF

INSERT INTO ModuleField(FK_Module_ID,FieldName,FieldLabel,Sequence,Required,IsPrimaryKey,IsUnik,IsShowInView,IsShowInForm,DefaultValue,FK_FieldType_ID,SizeField,FK_ExtType_ID,TabelReferenceName,TabelReferenceNameAlias,TableReferenceFieldKey,TableReferenceFieldDisplayName,TableReferenceFilter,IsUseRegexValidation,TableReferenceAdditonalJoin,BCasCade,FieldNameParent,FilterCascade) Values(1050,'PK_ModuleHideApprovalAll_ID','PK_ModuleHideApprovalAll_ID',1,1,1,1,1,1,'',15,0,7,'','','','','',0,'',0,'','')
INSERT INTO ModuleField(FK_Module_ID,FieldName,FieldLabel,Sequence,Required,IsPrimaryKey,IsUnik,IsShowInView,IsShowInForm,DefaultValue,FK_FieldType_ID,SizeField,FK_ExtType_ID,TabelReferenceName,TabelReferenceNameAlias,TableReferenceFieldKey,TableReferenceFieldDisplayName,TableReferenceFilter,IsUseRegexValidation,TableReferenceAdditonalJoin,BCasCade,FieldNameParent,FilterCascade) Values(1050,'FK_Module_ID','FK_Module_ID',2,1,0,0,1,1,'',11,0,2,'Module','','PK_Module_ID','ModuleName','',0,'',0,'','')
EXEC usp_generateTable 1050
EXEC usp_generateTableUpload 1050
COMMIT

PRINT 'SUCCESS'

END TRY

BEGIN CATCH

ROLLBACK

PRINT 'FAILED'

PRINT ERROR_MESSAGE()

END CATCH