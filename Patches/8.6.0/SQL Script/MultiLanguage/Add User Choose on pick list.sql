IF NOT EXISTS (SELECT 1 FROM [dbo].[SystemParameterPickList] 
WHERE [Pk_ApplicationAuthentication_ID] = 10 
and [ApplicationAuthentication] = 'User Choosen'
and [SettingName] = 'Language')
BEGIN
INSERT INTO [dbo].[SystemParameterPickList]
           ([Pk_ApplicationAuthentication_ID]
           ,[ApplicationAuthentication]
           ,[SettingName])
     VALUES
           (10
           ,'User Choosen'
           ,'Language')
END
ELSE
BEGIN
    PRINT 'Data already exists in the system parameter pick list.'
END


