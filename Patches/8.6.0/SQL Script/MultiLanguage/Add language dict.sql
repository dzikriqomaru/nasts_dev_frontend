IF NOT EXISTS (SELECT 1 FROM [dbo].[systemDictionary] WHERE [Keywords] = 'Language')
BEGIN
    INSERT INTO [dbo].[systemDictionary]
           ([Keywords]
           ,[LanguageType]
           ,[TextDefinition]
           ,[Active]
           ,[CreatedBy]
           ,[LastUpdateBy]
           ,[ApprovedBy]
           ,[CreatedDate]
           ,[LastUpdateDate]
           ,[ApprovedDate]
           ,[Alternateby])
     VALUES
           ('Language'
           ,'id'
           ,'Bahasa'
           ,1
           ,'sysadmin'
           ,'sysadmin'
           ,''
           ,GETDATE()
           ,GETDATE()
           ,GETDATE()
           ,'')

    INSERT INTO [dbo].[systemDictionary]
           ([Keywords]
           ,[LanguageType]
           ,[TextDefinition]
           ,[Active]
           ,[CreatedBy]
           ,[LastUpdateBy]
           ,[ApprovedBy]
           ,[CreatedDate]
           ,[LastUpdateDate]
           ,[ApprovedDate]
           ,[Alternateby])
     VALUES
           ('Language'
           ,'en'
           ,'Language'
           ,1
           ,'sysadmin'
           ,'sysadmin'
           ,''
           ,GETDATE()
           ,GETDATE()
           ,GETDATE()
           ,'')
END
ELSE
BEGIN
    PRINT 'Keyword "Language" already exists in the system dictionary.'
END
GO
