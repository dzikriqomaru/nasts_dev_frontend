INSERT INTO systemDictionary (
	Keywords, LanguageType, TextDefinition, Active,
	CreatedBy, LastUpdateBy, ApprovedBy,
	CreatedDate, LastUpdateDate, ApprovedDate
)
VALUES (
	'insert', 'en', 'Insert', 1,
	'sysadmin', 'sysadmin', 'sysadmin',
	GETDATE(), GETDATE(), GETDATE()
), (
	'insert', 'id', 'Tambah', 1,
	'sysadmin', 'sysadmin', 'sysadmin',
	GETDATE(), GETDATE(), GETDATE()
), (
	'approval', 'en', 'Approval', 1,
	'sysadmin', 'sysadmin', 'sysadmin',
	GETDATE(), GETDATE(), GETDATE()
), (
	'approval', 'id', 'Persetujuan', 1,
	'sysadmin', 'sysadmin', 'sysadmin',
	GETDATE(), GETDATE(), GETDATE()
)