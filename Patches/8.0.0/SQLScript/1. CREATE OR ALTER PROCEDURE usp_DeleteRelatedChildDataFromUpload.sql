/****** Object:  StoredProcedure [dbo].[usp_DeleteRelatedChildDataFromUpload]    Script Date: 3/25/2024 13:51:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Davin Julian
-- Create date: 24 Maret 2024
-- Description:	Delete related child data from parent 
-- =============================================
CREATE OR ALTER PROCEDURE [dbo].[usp_DeleteRelatedChildDataFromUpload] 
	@ModuleName VARCHAR(250),
	@userid VARCHAR(50)
AS
BEGIN
	DECLARE @ParentPrimarykeyFieldName Varchar(500);
	DECLARE @Sql VARCHAR(MAX); 

	SET @ParentPrimarykeyFieldName = (
		SELECT TOP 1 FieldName 
		FROM ModuleField mf 
		WHERE mf.IsPrimaryKey = 1 
			AND mf.FK_Module_ID IN (
			SELECT PK_MODULE_ID 
			FROM MODULE 
			WHERE ModuleName = @ModuleName)
		)

	DECLARE @FK_Module_ID INT,    
            @PK_ModuleDetail_ID INT,    
            @ModuleDetailName VARCHAR(250),    
            @ChildFieldNameKey VARCHAR(250),
			@ParentModuleName VARCHAR(250),
			@ParentFieldNameKey VARCHAR(250)

	DECLARE my_cursor CURSOR FAST_FORWARD READ_ONLY     
	FOR    
        SELECT 
			md.FK_Module_ID, 
			md.PK_ModuleDetail_ID ,
			md.ModuleDetailName , 
			mdf.FieldName,
			mdf.TabelReferenceName,
			mdf.TableReferenceFieldKey
		FROM ModuleDetail md
		JOIN ModuleDetailField mdf on md.PK_ModuleDetail_ID = mdf.FK_ModuleDetail_ID and mdf.FK_FieldType_ID = 16
		WHERE FK_Module_ID IN (
			SELECT PK_MODULE_ID 
			FROM Module 
			WHERE ModuleName = @ModuleName AND FK_Parent_ID IS NULL)
	OPEN my_cursor    
           
    FETCH FROM my_cursor 
	INTO @FK_Module_ID, @PK_ModuleDetail_ID , @ModuleDetailName, @ChildFieldNameKey, @ParentModuleName, @ParentFieldNameKey
           
    WHILE @@FETCH_STATUS = 0    
    BEGIN
		DECLARE @ParentTableUploadName VARCHAR(500);
		SET @ParentTableUploadName = @ModuleName + '_Upload';
		
		--Update nawaaction update to delete if parent delete
		SET @Sql = ' UPDATE '+@ModuleDetailName+'_Upload ' + CHAR(13) + CHAR(10)
			+ ' SET nawa_Action =''Delete'' ' + CHAR(13) + CHAR(10)
			+ ' WHERE '+ @ChildFieldNameKey + CHAR(13) + CHAR(10) 
			+ ' IN ( SELECT ' +@ParentTableUploadName+'.'+ @ParentFieldNameKey + ' FROM ' + @ModuleName + ' ' + CHAR(10) 
			+ ' INNER JOIN ' + @ParentTableUploadName + ' ON ' + @ModuleName + '.' + @ParentPrimarykeyFieldName + '=' + @ParentTableUploadName + '.' + @ParentPrimarykeyFieldName + ' ' + CHAR(10)
			+ ' WHERE ' + @ParentTableUploadName + '.nawa_Action=''Delete''' + CHAR(10)
			+ ' AND ' + @ParentTableUploadName + '.KeteranganError='''' ' + CHAR(10)
			+ ' AND ' + @ParentTableUploadName + '.nawa_userid=''' + @userid + ''')' + CHAR(10) 
			+ ' AND ' + @ModuleDetailName+'_Upload.nawa_userid = ''' + @userid + ''''+ CHAR(10) 
			+ ' AND ' + @ModuleDetailName+'_Upload.KeteranganError='''' ' + CHAR(10)
			+ ' AND ' + @ModuleDetailName+'_Upload.nawa_Action = ''Update'''
		EXEC(@sql)

		--Update keterangan error if child action insert and parent delete 
		SET @Sql = ' UPDATE '+@ModuleDetailName+'_Upload ' + CHAR(13) + CHAR(10)
			+ ' SET KeteranganError = KeteranganError + ''Line '' + CONVERT(VARCHAR(20), nawa_recordnumber) + '' : ' + CHAR(13) + CHAR(10)
			+ ' Cannot insert when parent Delete  '' + CHAR(13) + CHAR(10)'
			+ ' WHERE '+ @ChildFieldNameKey + CHAR(13) + CHAR(10) 
			+ ' IN ( SELECT ' +@ParentTableUploadName+'.'+ @ParentFieldNameKey + ' FROM ' + @ModuleName + ' ' + CHAR(10) 
			+ ' INNER JOIN ' + @ParentTableUploadName + ' ON ' + @ModuleName + '.' + @ParentPrimarykeyFieldName + '=' + @ParentTableUploadName + '.' + @ParentPrimarykeyFieldName + ' ' + CHAR(10)
			+ ' WHERE ' + @ParentTableUploadName + '.nawa_Action=''Delete''' + CHAR(10)
			+ ' AND ' + @ParentTableUploadName + '.KeteranganError='''' ' + CHAR(10)
			+ ' AND ' + @ParentTableUploadName + '.nawa_userid=''' + @userid + ''')' + CHAR(10) 
			+ ' AND ' + @ModuleDetailName+'_Upload.nawa_userid = ''' + @userid + ''''+ CHAR(10)
			+ ' AND ' + @ModuleDetailName+'_Upload.nawa_Action = ''Insert'''
		EXEC(@sql)

		FETCH FROM my_cursor 
		INTO  @FK_Module_ID, @PK_ModuleDetail_ID , @ModuleDetailName, @ChildFieldNameKey, @ParentModuleName, @ParentFieldNameKey    
	END
    CLOSE my_cursor    
    DEALLOCATE my_cursor    

END
