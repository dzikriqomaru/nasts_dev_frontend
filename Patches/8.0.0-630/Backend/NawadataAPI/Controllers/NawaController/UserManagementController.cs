﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using NawaDataBLL;
using NawaDataDAL;
using NawaDataDAL.Models;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using System.IO;

namespace NawaDataAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserManagementController : ControllerBase
    {

        [Route("getMRoleListPublic")]
        [HttpGet]
        public ActionResult getMRoleListPublic()
        {
            try
            {
                if (SystemParameterBLL.GetSystemParameterValue(-12).SettingValue == "1")
                {
                    List<MRole> MRoleList = UserBLL.GetMRoleList();
                    return Ok(MRoleList);
                }
                return StatusCode(404, "404 not found error");
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string stringUserID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, stringUserID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("getMGroupMenuListPublic")]
        [HttpGet]
        public ActionResult getMGroupMenuListPublic()
        {
            try
            {
                if (SystemParameterBLL.GetSystemParameterValue(-12).SettingValue == "1")
                {
                    List<MGroupMenu> MGroupMenuList = UserBLL.GetMGroupMenuList();
                    return Ok(MGroupMenuList);
                }

                return StatusCode(404, "404 not found error");

            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string stringUserID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, stringUserID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy = "ApplicationLock")]
        [Route("getMRoleList")]
        [HttpGet]
        public ActionResult getMRoleList()
        {
            try
            {
                List<MRole> MRoleList = UserBLL.GetMRoleList();
                return Ok(MRoleList);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string stringUserID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, stringUserID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy = "ApplicationLock")]
        [Route("getMGroupMenuList")]
        [HttpGet]
        public ActionResult getMGroupMenuList()
        {
            try
            {
                List<MGroupMenu> MGroupMenuList = UserBLL.GetMGroupMenuList();
                return Ok(MGroupMenuList);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string stringUserID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, stringUserID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [Route("approveUser")]
        [HttpGet]
        public ActionResult approveUser(int approvalID)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MUser CurrentUser = new MUser
                {
                    UserID = identity.FindFirst("UserID").Value,
                    PK_MUser_ID = Convert.ToInt32(identity.FindFirst("PK_MUser_ID").Value)
                };
                int MRoleID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value);

                UserBLL.SaveUserApprove(approvalID, CurrentUser);
                return Ok("Data Saved Into Database");

            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string stringUserID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, stringUserID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [Route("approveUserLock")]
        [HttpGet]
        public ActionResult approveUserLock(int approvalID)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MUser CurrentUser = new MUser
                {
                    UserID = identity.FindFirst("UserID").Value,
                    PK_MUser_ID = Convert.ToInt32(identity.FindFirst("PK_MUser_ID").Value)
                };
                int MRoleID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value);

                UserBLL.SaveUserApproveLock(approvalID, CurrentUser);
                return Ok("Data Saved Into Database");

            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string stringUserID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, stringUserID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [Route("rejectUser")]
        [HttpGet]
        public ActionResult rejectUser(int approvalID)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MUser CurrentUser = new MUser
                {
                    UserID = identity.FindFirst("UserID").Value,
                    PK_MUser_ID = Convert.ToInt32(identity.FindFirst("PK_MUser_ID").Value)
                };
                int MRoleID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value);

                UserBLL.SaveUserReject(approvalID, CurrentUser);
                return Ok("Data Rejected");

            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string stringUserID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, stringUserID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [Route("rejectUserLock")]
        [HttpGet]
        public ActionResult rejectUserLock(int approvalID)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MUser CurrentUser = new MUser
                {
                    UserID = identity.FindFirst("UserID").Value,
                    PK_MUser_ID = Convert.ToInt32(identity.FindFirst("PK_MUser_ID").Value)
                };
                int MRoleID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value);

                UserBLL.SaveUserLockReject(approvalID, CurrentUser);
                return Ok("Data Rejected");

            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string stringUserID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, stringUserID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [Route("postMUser")]
        [HttpPost]
        public ActionResult postMUser(UserBLL.SaveMUser User)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string CurrentUser = identity.FindFirst("UserID").Value;
                int MRoleID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value);
                int pkMUserID = Convert.ToInt32(identity.FindFirst("PK_MUser_ID").Value);
                string errorMessage;

                //decrypt user password
                User.UserPasword = EncryptionBLL.DecryptStringAES(User.UserPasword);

                if (UserBLL.ValidatePredictablePassword(User.UserID, User.UserPasword))
                {
                    
                    if (MRoleID == 1)
                    {
                        errorMessage = UserBLL.SaveUserWithoutApproval(User, CurrentUser);

                        if (string.IsNullOrEmpty(errorMessage))
                        {
                            return Ok("Data has been saved");
                        }
                        else
                        {
                            return StatusCode(400, errorMessage);
                        }
                    }
                    else
                    {
                        errorMessage = UserBLL.SaveUserWithApproval(User, CurrentUser, pkMUserID, MRoleID);

                        if (string.IsNullOrEmpty(errorMessage))
                        {
                            return Ok("Pending to approval");
                        }
                        else
                        {
                            return StatusCode(400, errorMessage);
                        }
                    }
                }
                else
                {
                    return BadRequest("Password is too predictable");
                }
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [Route("getMUser")]
        [HttpGet]
        public ActionResult getMUser(int UserID)
        {
            try
            {
                MUserView User = UserBLL.GetUserByID(UserID);
                return Ok(User);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string stringUserID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, stringUserID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [Route("getUserApprovalByID")]
        [HttpGet]
        public ActionResult getUserApprovalByID(long UserApprovalID)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MUser currentUser = new MUser()
                {
                    FK_MGroupMenu_ID = Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value)
                };

                if (!ModuleBLL.CheckModuleApprovalByID(Convert.ToInt32(UserApprovalID)))
                    return StatusCode(500, "Data already on process approval.");

                if (!CommonBLL.CheckMenuAccessByApprovalID(currentUser.FK_MGroupMenu_ID, Convert.ToInt32(UserApprovalID), EActionType.Approval))
                    return StatusCode(500, "You don't have authorization to access this menu.");

                UserApprovalView userApproval = UserBLL.GetUserApprovalByID(UserApprovalID);

                return Ok(userApproval);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string stringUserID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, stringUserID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [Route("getUserLockApprovalByID")]
        [HttpGet]
        public ActionResult getUserLockApprovalByID(long UserApprovalID)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MUser currentUser = new MUser()
                {
                    FK_MGroupMenu_ID = Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value)
                };

                int mode = 1;
                string userRequest = "";
                MUserView oldData = new MUserView { };
                DateTime dateRequest = DateTime.Now;
                MUserView User = UserBLL.GetUserLockApprovalByID(UserApprovalID, out mode, out userRequest, out dateRequest, out oldData);

                if (!ModuleBLL.CheckModuleApprovalByID(Convert.ToInt32(UserApprovalID)))
                    return StatusCode(500, "Data already on process approval.");

                if (!CommonBLL.CheckMenuAccessByApprovalID(currentUser.FK_MGroupMenu_ID, Convert.ToInt32(UserApprovalID), EActionType.Approval))
                    return StatusCode(500, "You don't have authorization to access this menu.");

                return Ok(new { Mode = mode, UserRequest = userRequest, DateRequest = dateRequest.ToString("dd MMM yyyy HH:mm:ss"), User = User, OldData = oldData });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string stringUserID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, stringUserID);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [Route("getUserLDAP")]
        [HttpGet]
        public ActionResult getUserLDAP(string UserID)
        {
            try
            {
                string userid = UserID;
                if (UserID == "")
                {
                    return Ok("Please Enter User ID");
                }
                DataTable dtresult = UserBLL.CheckUserLDAP(UserID);
                return Ok(new { displayname = Convert.ToString(dtresult.Rows[0]["displayname"]), Mail = Convert.ToString(dtresult.Rows[0]["Mail"]) });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string stringUserID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, stringUserID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetModuleData")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetModuleData(int moduleId, int page, string orderBy, string order, string search, string filter)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;

                int rowCount = 0;
                List<DynamicDictionary> moduleData = UserBLL.GetModuleData(
                    moduleId, page, orderBy, order, search, filter, out rowCount, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value, null);

                return Ok(new { count = rowCount, rows = moduleData });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string stringUserID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, stringUserID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetModuleData")]
        [Authorize(Policy="ApplicationLock")]
        [HttpPost]
        public ActionResult GetModuleData([FromBody] GenericPagingFilterPostBody body)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;

                int rowCount = 0;
                List<DynamicDictionary> moduleData = UserBLL.GetModuleData(
                    body.ModuleID, body.page, body.orderBy, body.order, body.search, body.filter, out rowCount, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value, body.jsonFilter);

                return Ok(new { count = rowCount, rows = moduleData });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string stringUserID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, stringUserID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetModuleDataUserLock")]
        [Authorize(Policy = "ApplicationLock")]
        [HttpGet]
        public ActionResult GetModuleDataUserLock(int moduleId, int page, string orderBy, string order, string search, string filter)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;

                int rowCount = 0;
                List<DynamicDictionary> moduleData = UserBLL.GetModuleDataUserLock(
                    moduleId, page, orderBy, order, search, filter, out rowCount, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value, null);

                return Ok(new { count = rowCount, rows = moduleData });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string stringUserID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, stringUserID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetModuleDataUserLock")]
        [Authorize(Policy = "ApplicationLock")]
        [HttpPost]
        public ActionResult GetModuleDataUserLock([FromBody] GenericPagingFilterPostBody body)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;

                int rowCount = 0;
                List<DynamicDictionary> moduleData = UserBLL.GetModuleDataUserLock(
                    body.ModuleID, body.page, body.orderBy, body.order, body.search, body.filter, out rowCount, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value, body.jsonFilter);

                return Ok(new { count = rowCount, rows = moduleData });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string stringUserID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, stringUserID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ExportModuleData")]
        [HttpGet]
        [Authorize(Policy="ApplicationLock")]
        public IActionResult ExportModuleData(int ModuleID, int page, string orderBy, string order, string search, string filter, bool isAdv, string exportAs)
        {
            ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
            try
            {
                MemoryStream ms = UserBLL.ExportModuleData(
                    ModuleID, page, orderBy, order, search, filter, isAdv, exportAs, null, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value, null);

                string mediaType;

                ms.Position = 0;

                if (exportAs == "Text")
                {
                    mediaType = "text/plain";
                }
                else if (exportAs == "CSV")
                {
                    mediaType = "text/csv";
                }
                else
                {
                    mediaType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                }

                return File(ms, mediaType);
            }
            catch (Exception ex)
            {
                string stringUserID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, stringUserID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ExportModuleData")]
        [HttpPost]
        [Authorize(Policy="ApplicationLock")]
        public IActionResult ExportModuleData([FromBody] GenericPagingFilterPostBody body)
        {
            ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
            try
            {
                MemoryStream ms = NawaDataBLL.UserBLL.ExportModuleData(body.ModuleID, body.page, body.orderBy, body.order, body.search, body.filter, 
                    body.isAdv, body.exportAs, body.fields, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value, body.jsonFilter);

                ms.Position = 0;
                return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Data.xlsx");
            }
            catch (Exception ex)
            {
                string stringUserID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, stringUserID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ExportModuleDataUserLock")]
        [HttpGet]
        [Authorize(Policy = "ApplicationLock")]
        public IActionResult ExportModuleDataUserLock(int ModuleID, int page, string orderBy, string order, string search, string filter, bool isAdv, string exportAs)
        {
            ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
            try
            {
                MemoryStream ms = UserBLL.ExportModuleDataUserLock(
                    ModuleID, page, orderBy, order, search, filter, isAdv, exportAs, null, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value, null);

                string mediaType;

                ms.Position = 0;

                if (exportAs == "Text")
                {
                    mediaType = "text/plain";
                }
                else if (exportAs == "CSV")
                {
                    mediaType = "text/csv";
                }
                else
                {
                    mediaType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                }

                return File(ms, mediaType);
            }
            catch (Exception ex)
            {
                string stringUserID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, stringUserID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ExportModuleDataUserLock")]
        [HttpPost]
        [Authorize(Policy = "ApplicationLock")]
        public IActionResult ExportModuleDataUserLock([FromBody] GenericPagingFilterPostBody body)
        {
            ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
            try
            {
                MemoryStream ms = NawaDataBLL.UserBLL.ExportModuleDataUserLock(body.ModuleID, body.page, body.orderBy, body.order, body.search, body.filter,
                    body.isAdv, body.exportAs, body.fields, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value, body.jsonFilter);

                ms.Position = 0;
                return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Data.xlsx");
            }
            catch (Exception ex)
            {
                string stringUserID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, stringUserID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ExportModuleDataAll")]
        [HttpGet]
        [Authorize(Policy="ApplicationLock")]
        public IActionResult ExportModuleDataAll(int ModuleID, string orderBy, string order, string search, string filter, bool isAdv, string exportAs)
        {
            ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
            try
            {
                MemoryStream ms = UserBLL.ExportModuleDataAll(
                    ModuleID, orderBy, order, search, filter, isAdv, exportAs, null, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value, null);

                ms.Position = 0;
                string mediaType;

                if (exportAs == "Text")
                {
                    mediaType = "text/plain";
                }
                else if (exportAs == "CSV")
                {
                    mediaType = "text/csv";
                }
                else
                {
                    mediaType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                }

                return File(ms, mediaType);
            }
            catch (Exception ex)
            {
                string stringUserID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, stringUserID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ExportModuleDataAll")]
        [HttpPost]
        [Authorize(Policy="ApplicationLock")]
        public IActionResult ExportModuleDataAll([FromBody] GenericPagingFilterPostBody body)
        {
            ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
            try
            {
                MemoryStream ms = UserBLL.ExportModuleDataAll(
                    body.ModuleID, body.orderBy, body.order, body.search, body.filter, body.isAdv, body.exportAs, body.fields, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value, body.jsonFilter);

                ms.Position = 0;
                return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Data.xlsx");
            }
            catch (Exception ex)
            {
                string stringUserID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, stringUserID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ExportModuleDataAllUserLock")]
        [HttpGet]
        [Authorize(Policy = "ApplicationLock")]
        public IActionResult ExportModuleDataAllUserLock(int ModuleID, string orderBy, string order, string search, string filter, bool isAdv, string exportAs)
        {
            ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
            try
            {
                MemoryStream ms = UserBLL.ExportModuleDataAllUserLock(
                    ModuleID, orderBy, order, search, filter, isAdv, exportAs, null, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value, null);

                ms.Position = 0;
                string mediaType;

                if (exportAs == "Text")
                {
                    mediaType = "text/plain";
                }
                else if (exportAs == "CSV")
                {
                    mediaType = "text/csv";
                }
                else
                {
                    mediaType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                }

                return File(ms, mediaType);
            }
            catch (Exception ex)
            {
                string stringUserID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, stringUserID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ExportModuleDataAllUserLock")]
        [HttpPost]
        [Authorize(Policy = "ApplicationLock")]
        public IActionResult ExportModuleDataAllUserLock([FromBody] GenericPagingFilterPostBody body)
        {
            ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
            try
            {
                MemoryStream ms = UserBLL.ExportModuleDataAllUserLock(
                    body.ModuleID, body.orderBy, body.order, body.search, body.filter, body.isAdv, body.exportAs, body.fields, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value), identity.FindFirst("UserID").Value, body.jsonFilter);

                ms.Position = 0;
                return File(ms, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Data.xlsx");
            }
            catch (Exception ex)
            {
                string stringUserID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, stringUserID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ExportModuleDataSelected")]
        [HttpPost]
        [Authorize(Policy="ApplicationLock")]
        public IActionResult ExportModuleDataSelected(NawaDataDAL.Models.CSelectedData selectedData)
        {
            ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
            try
            {
                MemoryStream ms = UserBLL.ExportModuleDataSelected(selectedData.SelectedData, selectedData.orderBy, selectedData.order, selectedData.exportAs, selectedData.fields, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value));

                ms.Position = 0;
                string mediaType;

                if (selectedData.exportAs == "Text")
                {
                    mediaType = "text/plain";
                }
                else if (selectedData.exportAs == "CSV")
                {
                    mediaType = "text/csv";
                }
                else
                {
                    mediaType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                }

                return File(ms, mediaType);
            }
            catch (Exception ex)
            {
                string stringUserID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, stringUserID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ExportModuleDataSelectedUserLock")]
        [HttpPost]
        [Authorize(Policy = "ApplicationLock")]
        public IActionResult ExportModuleDataSelectedUserLock(NawaDataDAL.Models.CSelectedData selectedData)
        {
            ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
            try
            {
                MemoryStream ms = UserBLL.ExportModuleDataSelectedUserLock(selectedData.SelectedData, selectedData.orderBy, selectedData.order, selectedData.exportAs, selectedData.fields, Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value));

                ms.Position = 0;
                string mediaType;

                if (selectedData.exportAs == "Text")
                {
                    mediaType = "text/plain";
                }
                else if (selectedData.exportAs == "CSV")
                {
                    mediaType = "text/csv";
                }
                else
                {
                    mediaType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                }

                return File(ms, mediaType);
            }
            catch (Exception ex)
            {
                string stringUserID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, stringUserID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("RegisterUser")]
        [HttpPost]
        public ActionResult RegisterUser(MUser user)
        {
            try
            {
                if(SystemParameterBLL.GetSystemParameterValue(-12).SettingValue == "1")
                {
                    user.UserPasword = EncryptionBLL.DecryptStringAES(user.UserPasword);

                    if (UserBLL.ValidatePredictablePassword(user.UserID, user.UserPasword))
                    {
                        UserBLL.RegisterUser(user);

                        return Ok("Registration success, waiting for approval");
                    }
                    else
                    {
                        return BadRequest("Password is too predictable");
                    }
                }

                return StatusCode(404, "404 not found error");
               
            }
            catch (Exception ex)
            {
                ElmahErrorBLL.Log(ex);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ActionMUserApproval")]
        [HttpPost]
        [Authorize(Policy="ApplicationLock")]
        public ActionResult ActionMUserApproval(CParameterApproval parameterApproval)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                Module moduleMUser = ModuleBLL.GetModuleByName("MUser");

                if (CommonBLL.CheckMenuAccess(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), moduleMUser.PK_Module_ID, EActionType.Approval))
                {
                    string currentUser = identity.FindFirst("UserID").Value;

                    
                    if (parameterApproval.PK_ModuleApproval_IDSelected != null && parameterApproval.PK_ModuleApproval_IDSelected.Length > 0)
                    {
                        foreach (long pk in parameterApproval.PK_ModuleApproval_IDSelected)
                        {
                            CParameterApproval approvalData = new CParameterApproval
                            {
                                PK_ModuleApproval_ID = pk,
                                ActionApproval = parameterApproval.ActionApproval
                            };
                            UserBLL.ActionMUserApproval(approvalData, currentUser);
                        }
                    }
                    else
                    {
                        UserBLL.ActionMUserApproval(parameterApproval, currentUser);
                    }

                    if (parameterApproval.ActionApproval == EActionApprovalType.Approve)
                    {
                        return Ok("User Approved");
                    }
                    else if (parameterApproval.ActionApproval == EActionApprovalType.Reject)
                    {
                        return Ok("User Rejected");
                    }
                    else
                    {
                        return Ok("User Revised");
                    }
                }
                else
                {
                    return StatusCode(500, "You don't have authorization to access this menu.");
                }
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        #region ActionUserApproval
        [Route("ActionUserApproval")]
        [HttpPost]
        [Authorize(Policy = "ApplicationLock")]
        public ActionResult ActionUserApproval(CParameterApproval parameterApproval)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MUser CurrentUser = new MUser
                {
                    UserID = identity.FindFirst("UserID").Value,
                    PK_MUser_ID = Convert.ToInt32(identity.FindFirst("PK_MUser_ID").Value)
                };
                if (parameterApproval.PK_ModuleApproval_IDSelected != null && parameterApproval.PK_ModuleApproval_IDSelected.Length > 0)
                {
                    if (parameterApproval.ActionApproval == EActionApprovalType.Approve)
                    {
                        foreach (long pk in parameterApproval.PK_ModuleApproval_IDSelected)
                        {
                            UserBLL.SaveUserApprove(pk, CurrentUser);
                        }
                    }
                    else
                    {
                        foreach (long pk in parameterApproval.PK_ModuleApproval_IDSelected)
                        {
                            UserBLL.SaveUserReject(pk, CurrentUser);
                        }
                    }
                }
                return Ok("Success");
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }
        #endregion


        #region Action User Lock Approval
        [Route("ActionUserLockApproval")]
        [HttpPost]
        [Authorize(Policy = "ApplicationLock")]
        public ActionResult ActionUserLockApproval(CParameterApproval parameterApproval)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MUser CurrentUser = new MUser
                {
                    UserID = identity.FindFirst("UserID").Value,
                    PK_MUser_ID = Convert.ToInt32(identity.FindFirst("PK_MUser_ID").Value)
                };

                if (parameterApproval.PK_ModuleApproval_IDSelected != null && parameterApproval.PK_ModuleApproval_IDSelected.Length > 0)
                {
                    foreach (long pk in parameterApproval.PK_ModuleApproval_IDSelected)
                    {
                        CParameterApproval approvalData = new CParameterApproval
                        {
                            PK_ModuleApproval_ID = pk,
                            ActionApproval = parameterApproval.ActionApproval
                        };
                        if (parameterApproval.ActionApproval == EActionApprovalType.Approve)
                        {
                            UserBLL.SaveUserApproveLock(approvalData.PK_ModuleApproval_ID, CurrentUser);
                        }
                        else
                        {
                            UserBLL.SaveUserReject(approvalData.PK_ModuleApproval_ID, CurrentUser);
                        }
                    }
                }
                else
                {
                    if (parameterApproval.ActionApproval == EActionApprovalType.Approve)
                    {
                        UserBLL.SaveUserApproveLock(parameterApproval.PK_ModuleApproval_ID, CurrentUser);
                    }
                    else
                    {
                        UserBLL.SaveUserReject(parameterApproval.PK_ModuleApproval_ID, CurrentUser);
                    }
                }
                return Ok("Success");
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }
        #endregion


        [Route("GetUserApproval")]
        [HttpGet]
        [Authorize(Policy="ApplicationLock")]
        public ActionResult GetUserApproval(int page, string orderBy, string order, string search, string filter)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                int rowCount = 0;
                Module moduleMUser = ModuleBLL.GetModuleByName("MUser");

                if (CommonBLL.CheckMenuAccess(Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value), moduleMUser.PK_Module_ID, EActionType.Approval))
                {
                    List<ModuleApproval> data = UserBLL.GetUserApproval(page, orderBy, order, search, filter, out rowCount);

                    return Ok(new { count = rowCount, rows = data });
                }
                else
                {
                    return StatusCode(500, "You don't have authorization to access this menu.");
                }
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string stringUserID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, stringUserID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetUserLockApproval")]
        [HttpGet]
        [Authorize(Policy="ApplicationLock")]
        public ActionResult GetUserLockApproval(int page, string orderBy, string order, string search, string filter)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MUser currentUser = new MUser()
                {
                    UserID = identity.FindFirst("UserID").Value,
                    FK_MRole_ID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value),
                    FK_MGroupMenu_ID = Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value)
                };

                if (!CommonBLL.CheckMenuAccess(currentUser.FK_MGroupMenu_ID, 1034, EActionType.Approval))
                    return StatusCode(500, "You don't have authorization to access this menu.");

                int rowCount = 0;
                List<ModuleApprovalView> moduleApproval = ModuleBLL.GetModuleApproval(1034, identity.FindFirst("UserID").Value, page, orderBy, order, search, null, null, out rowCount, currentUser.FK_MRole_ID);

                return Ok(new { count = rowCount, rows = moduleApproval });
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ForgotPassword")]
        [HttpPost]
        public ActionResult ForgotPassword(string data)
        {
            try
            {
                UserBLL.ForgotPassword(data);

                return Ok("Please check your email for changing your password");
            }
            catch (Exception ex)
            {
                // dont use identity.FindFirst("UserID").Value here, because at this moment the frontend doesnt have valid jwt token yet (havent login),
                // so the identity parser will throw object reference is null exception, rather than exception from BLL  
                ElmahErrorBLL.Log(ex);
                return StatusCode(500, ex.Message);
            }
        }
        [Route("WrongPasswordAttempted")]
        [HttpPost]
        public ActionResult WrongPasswordAttempted(string userID)
        {
            try
            {
                //UserBLL.WrongPasswordAttempted(userID);

                return Ok("2 Wrong Password Attempted");
            }
            catch (Exception ex)
            {
                // dont use identity.FindFirst("UserID").Value here, because at this moment the frontend doesnt have valid jwt token yet (havent login),
                // so the identity parser will throw object reference is null exception, rather than exception from BLL  
                ElmahErrorBLL.Log(ex);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ValidateResetToken")]
        [HttpPost]
        public ActionResult ValidateResetToken(string token)
        {
            try
            {
                UserBLL.ValidateResetToken(token);

                return Ok("Token is valid");
            }
            catch (Exception ex)
            {
                // dont use identity.FindFirst("UserID").Value here, because at this moment the frontend doesnt have valid jwt token yet (havent login),
                // so the identity parser will throw object reference is null exception, rather than exception from BLL                
                ElmahErrorBLL.Log(ex);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("ChangeForgotPassword")]
        [HttpPost]
        public ActionResult ChangeForgotPassword(PasswordReset passwordInfo)
        {
            try
            {
                passwordInfo.newPassword = EncryptionBLL.DecryptStringAES(passwordInfo.newPassword);
                UserBLL.ChangeForgotPassword(passwordInfo.newPassword, passwordInfo.token);

                return Ok("Password has been saved");
            }
            catch (Exception ex)
            {
                ElmahErrorBLL.Log(ex);
                return StatusCode(500, ex.Message);
            }
        }

        [Authorize(Policy="ApplicationLock")]
        [Route("ChangeExpiredPassword")]
        [HttpPost]
        public ActionResult ChangeExpiredPassword(PasswordReset passwordInfo)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                int mUser_ID = Convert.ToInt32(identity.FindFirst("PK_MUser_ID").Value);
                //int mGroupMenu_ID = Convert.ToInt32(identity.FindFirst("FK_MGroupMenu_ID").Value);
                //int mRole_ID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value);

                passwordInfo.newPassword = EncryptionBLL.DecryptStringAES(passwordInfo.newPassword);

                if (UserBLL.ValidatePredictablePassword(identity.FindFirst("UserID").Value, passwordInfo.newPassword))
                {
                    UserBLL.ChangeExpiredPassword(passwordInfo.newPassword, mUser_ID);

                    return Ok("Password has been changed, please log in with the new password");
                }
                else
                {
                    return BadRequest("Password is too predictable");
                }
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string stringUserID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, stringUserID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("DisableUser")]
        [HttpPost]
        public ActionResult DisableUser(string userID)
        {
            try
            {
                string message = UserBLL.DisableUser(userID);

                return Ok(message);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string stringUserID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, stringUserID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("HandleUserLock")]
        [Authorize(Policy="ApplicationLock")]
        [HttpPost]

        public ActionResult HandleUserLock(UserBLL.SaveMUser User)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string CurrentUser = identity.FindFirst("UserID").Value;
                int pkMuserID = Convert.ToInt32(identity.FindFirst("PK_MUser_ID").Value);
                int MRoleID = Convert.ToInt32(identity.FindFirst("FK_MRole_ID").Value);
                string errorMessage;

                if (MRoleID == 1)
                {
                    errorMessage = UserBLL.handleUserLock(User, CurrentUser);
                    if (string.IsNullOrEmpty(errorMessage))
                    {
                        return Ok("Data Saved Into Database");
                    } else
                    {
                        return StatusCode(400, errorMessage);
                    }
                }
                else
                {
                    errorMessage = UserBLL.handleUserLockWithApproval(User, CurrentUser, pkMuserID, MRoleID);

                    if (string.IsNullOrEmpty(errorMessage))
                    {
                        return Ok("Pending To Approval");
                    }
                    else
                    {
                        return StatusCode(400, errorMessage);
                    }
                }


            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string stringUserID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, stringUserID);
                return StatusCode(500, ex.Message);
            }

        }

        [Route("GetModuleAccess")]
        [Authorize(Policy="ApplicationLock")]
        [HttpGet]
        public ActionResult GetModuleAccess(int moduleID)
        {
            try
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                MGroupMenuAccess menuAccess = CommonBLL.GetModuleAccess(moduleID, identity.FindFirst("FK_MGroupMenu_ID").Value);

                return Ok(menuAccess);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string userID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("CheckUserPasswordExpiredRange")]
        [HttpGet]

        public ActionResult CheckUserPasswordExpiredRange(int PK_User_ID)
        {
            try
            {
                int daysRemaining = UserBLL.CheckPasswordExpiredRange(PK_User_ID);
                return Ok(daysRemaining);
            }
            catch (Exception ex)
            {
                ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
                string stringUserID = identity.FindFirst("UserID").Value;
                ElmahErrorBLL.Log(ex, stringUserID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetDataAccessReplacer")]
        [HttpGet]
        [Authorize]
        public ActionResult GetDataAccessReplacer()
        {
            ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
            string userID = identity.FindFirst("UserID").Value;
            try
            {
                List<DataAccessReplacer> dataAccessReplacer = UserBLL.GetDataAccessReplacer(userID);
                return Ok(dataAccessReplacer);
            }
            catch (Exception ex)
            {
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("GetSettingUser")]
        [HttpGet]
        [Authorize]
        public ActionResult GetSettingUser()
        {
            ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
            string userID = identity.FindFirst("UserID").Value;

            try
            {
                List<SettingUser> settingUser = UserBLL.GetSettingUser(userID);
                return Ok(settingUser);
            }
            catch (Exception ex)
            {
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        [Route("SaveSettingUser")]
        [HttpPost]
        [Authorize]
        public ActionResult SaveSettingUser([FromBody] PostSettingUser body)
        {
            ClaimsIdentity identity = HttpContext.User.Identity as ClaimsIdentity;
            string userID = identity.FindFirst("UserID").Value;

            try
            {
                string status = UserBLL.PostSettingUser(body.settingUser);
                return Ok(status);
            }
            catch (Exception ex)
            {
                ElmahErrorBLL.Log(ex, userID);
                return StatusCode(500, ex.Message);
            }
        }

        public class PasswordReset
        {
            public string newPassword { get; set; }
            public string token { get; set; }
        }
    }
}