ALTER PROCEDURE [dbo].[usp_GetModuleApproval]
	@ModuleKey VARCHAR(1000),
	@UserID VARCHAR(50),
	@Search VARCHAR(MAX),
	@OrderBy VARCHAR(MAX),
	@PageIndex INTEGER,
	@PageSize INTEGER,
	@filterKey VARCHAR(50),
	@filterValue VARCHAR(MAX),
	@RoleID INT,
	@DataAccess VARCHAR(MAX)
AS

DECLARE @PK_MUser_ID INT
SELECT @PK_MUser_ID = PK_MUser_ID
FROM MUser
WHERE UserID = @UserID

DECLARE @DateFormat VARCHAR(MAX)
DECLARE @DateFormatSysParam VARCHAR(MAX)
SELECT @DateFormatSysParam = SettingValue FROM SystemParameter WHERE PK_SystemParameter_ID = 6
SELECT @DateFormat = SQLFormat FROM MDateFormat WHERE PK_DateFormat_ID = CAST(@DateFormatSysParam AS bigint)

DECLARE @TimeFormat VARCHAR(MAX)
DECLARE @TimeFormatSysParam VARCHAR(MAX)
SELECT @TimeFormatSysParam = SettingValue FROM SystemParameter WHERE PK_SystemParameter_ID = 33
SELECT @TimeFormat = SQLFormat FROM MTimeFormat WHERE PK_TimeFormat_ID = CAST(@TimeFormatSysParam AS bigint)

DECLARE @DateTimeFormat VARCHAR(MAX) = @DateFormat + ' ' + @TimeFormat

DECLARE @SQL VARCHAR(MAX)

IF @OrderBy = '' SET @OrderBy = '1'

IF len(@filterKey)<=0
	BEGIN
		IF EXISTS(SELECT 13 FROM Module_TR_WorkFlow WHERE FK_ModuleID = @ModuleKey AND Active = 1)
		BEGIN
			SET @SQL =		  'SELECT ModuleApproval.*, ModuleActionName' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + 'FROM ModuleApproval' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '		JOIN Module' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '		ON ModuleApproval.ModuleName = Module.ModuleName' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '		JOIN ModuleAction' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '		ON ModuleApproval.PK_ModuleAction_ID = ModuleAction.PK_ModuleAction_ID' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '		INNER JOIN MWorkFlow_Progress Workflow' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '		ON Workflow.FK_Module_ID = Module.PK_Module_ID' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '		   AND (' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '				Workflow.FK_Unik_ID = ModuleApproval.ModuleKey AND ModuleApproval.PK_ModuleAction_ID <> 7' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '				OR Workflow.FK_Unik_ID = Convert(VARCHAR(50), ModuleApproval.PK_ModuleApproval_ID) + ''Import'' AND ModuleApproval.PK_ModuleAction_ID = 7' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '			)' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + 'WHERE PK_Module_ID = ' + @ModuleKey + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '		 AND (' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '				(' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '					(' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '						FK_MWorkFlowUserType_ID = 1' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '						OR FK_MWorkFlowUserType_ID = 2' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '					)' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '					AND FK_MWorkflowUser_ID IN ('
													--get all parent user from previous user upline (user structure) by userid of current user
			SET @SQL = @SQL + '						SELECT muse2.FK_PARENT_ID ' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '						FROM  MUserStructure muse1 JOIN MUserStructure muse2 on muse1.fk_user_ID = muse2.fk_user_ID '  + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '						WHERE muse1.FK_PARENT_ID = ' + CAST(@PK_MUser_ID AS VARCHAR) + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '					)' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '					AND FK_MWorkflow_Status_ID = 3' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '				)' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '				OR (' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '					FK_MWorkFlowUserType_ID = 4' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '					AND FK_MWorkflowRole_ID = ' + CAST(@RoleID AS VARCHAR) + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '					AND FK_MWorkflow_Status_ID = 3' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '				)' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '			)' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '		 AND (ModuleApproval.ModuleName LIKE ''%' + @Search + '%''' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '			  OR PK_ModuleApproval_ID LIKE ''%' + @Search + '%''' + CHAR(10) + CHAR(13)
			-- SET @SQL = @SQL + '			  OR ModuleApproval.ModuleField LIKE ''%' + @Search + '%''' + CHAR(10) + CHAR(13)
			-- SET @SQL = @SQL + '			  OR ModuleApproval.ModuleFieldBefore LIKE ''%' + @Search + '%''' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '			  OR ModuleKey LIKE ''%' + @Search + '%''' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '			  OR FORMAT(ModuleApproval.CreatedDate, ''' + @DateTimeFormat + ''') LIKE ''%' + @Search + '%''' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '			  OR ModuleApproval.CreatedBy LIKE ''%' + @Search + '%''' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '			  OR ModuleActionName LIKE ''%' + @Search + '%'')' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '		 AND NOT EXISTS (' + CHAR(10) + CHAR(13)
											--exclude users who have previously processed approval
			SET @SQL = @SQL + '				SELECT 13' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '				FROM MWorkFlow_History AS mfh' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '					JOIN MWorkflow_ApprovalStatus AS mas ON mfh.FK_MWorkflow_ApprovalStatus_ID=mas.PK_MWorkflow_ApprovalStatus_ID' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '				WHERE mfh.FK_ModuleApproval_ID = CAST(ModuleApproval.PK_ModuleApproval_ID AS VARCHAR(MAX))' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '					AND mfh.FK_MWorkflow_ApprovalStatus_ID IN (1,4)' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '					AND mfh.FK_MUserId = ' + CAST(@PK_MUser_ID AS VARCHAR) + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '		 )' + CHAR(10) + CHAR(13)
			IF @DataAccess != ''
				BEGIN
					SET @SQL = @SQL + '		 AND ('+ @DataAccess + ')' + CHAR(10) + CHAR(13)
				END
			SET @SQL = @SQL + 'ORDER BY ' + @OrderBy + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + 'OFFSET ' + CAST((@PageIndex - 1) * @PageSize AS VARCHAR) + ' ROWS' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + 'FETCH NEXT ' + CAST(@PageSize AS VARCHAR) + ' ROWS ONLY' + CHAR(10) + CHAR(13)
		END
		ELSE
		BEGIN
			SET @SQL =		  'SELECT ModuleApproval.*, ModuleActionName' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + 'FROM ModuleApproval' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '		JOIN Module' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '		ON ModuleApproval.ModuleName = Module.ModuleName' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '		JOIN ModuleAction' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '		ON ModuleApproval.PK_ModuleAction_ID = ModuleAction.PK_ModuleAction_ID' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '		JOIN MUser UserCreator' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '		ON ModuleApproval.CreatedBy = UserCreator.UserID' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + 'WHERE PK_Module_ID = ' + @ModuleKey + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '		 AND ModuleApproval.CreatedBy <> ''' + @UserID + '''' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '		 AND (ModuleApproval.ModuleName LIKE ''%' + @Search + '%''' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '			  OR PK_ModuleApproval_ID LIKE ''%' + @Search + '%''' + CHAR(10) + CHAR(13)
			-- SET @SQL = @SQL + '			  OR ModuleApproval.ModuleField LIKE ''%' + @Search + '%''' + CHAR(10) + CHAR(13)
			-- SET @SQL = @SQL + '			  OR ModuleApproval.ModuleFieldBefore LIKE ''%' + @Search + '%''' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '			  OR ModuleKey LIKE ''%' + @Search + '%''' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '			  OR FORMAT(ModuleApproval.CreatedDate, ''' + @DateTimeFormat + ''') LIKE ''%' + @Search + '%''' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '			  OR ModuleApproval.CreatedBy LIKE ''%' + @Search + '%''' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '			  OR ModuleActionName LIKE ''%' + @Search + '%'')' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '		 AND ISNULL(ModuleApproval.FK_MRole_ID, UserCreator.FK_MRole_ID) = ' + CAST(@RoleID AS VARCHAR) + CHAR(10) + CHAR(13)
			IF @DataAccess != ''
				BEGIN
					SET @SQL = @SQL + '		 AND ('+ @DataAccess + ')' + CHAR(10) + CHAR(13)
				END
			SET @SQL = @SQL + 'ORDER BY ' + @OrderBy + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + 'OFFSET ' + CAST((@PageIndex - 1) * @PageSize AS VARCHAR) + ' ROWS' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + 'FETCH NEXT ' + CAST(@PageSize AS VARCHAR) + ' ROWS ONLY' + CHAR(10) + CHAR(13)
		END

		EXEC(@SQL)
		--PRINT(@SQL)
	END
ELSE
	BEGIN
		IF EXISTS(SELECT 13 FROM Module_TR_WorkFlow WHERE FK_ModuleID = @ModuleKey AND Active = 1)
		BEGIN
			SET @SQL =		  'SELECT ModuleApproval.*, ModuleActionName' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + 'FROM ModuleApproval' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '		JOIN Module' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '		ON ModuleApproval.ModuleName = Module.ModuleName' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '		JOIN ModuleAction' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '		ON ModuleApproval.PK_ModuleAction_ID = ModuleAction.PK_ModuleAction_ID' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '		INNER JOIN MWorkFlow_Progress Workflow' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '		ON Workflow.FK_Module_ID = Module.PK_Module_ID' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '		   AND (' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '				Workflow.FK_Unik_ID = ModuleApproval.ModuleKey AND ModuleApproval.PK_ModuleAction_ID <> 7' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '				OR Workflow.FK_Unik_ID = Convert(VARCHAR(50), ModuleApproval.PK_ModuleApproval_ID) + ''Import'' AND ModuleApproval.PK_ModuleAction_ID = 7' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '			)' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + 'WHERE PK_Module_ID = ' + @ModuleKey + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '		 AND (' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '				(' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '					(' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '						FK_MWorkFlowUserType_ID = 1' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '						OR FK_MWorkFlowUserType_ID = 2' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '					)' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '					AND FK_MWorkflowUser_ID IN ('
													--get all parent user from previous user upline (user structure) by userid of current user
			SET @SQL = @SQL + '						SELECT muse2.FK_PARENT_ID ' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '						FROM  MUserStructure muse1 JOIN MUserStructure muse2 on muse1.fk_user_ID = muse2.fk_user_ID '  + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '						WHERE muse1.FK_PARENT_ID = ' + CAST(@PK_MUser_ID AS VARCHAR) + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '					)' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '					AND FK_MWorkflow_Status_ID = 3' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '				)' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '				OR (' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '					FK_MWorkFlowUserType_ID = 4' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '					AND FK_MWorkflowRole_ID = ' + CAST(@RoleID AS VARCHAR) + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '					AND FK_MWorkflow_Status_ID = 3' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '				)' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '			)' + CHAR(10) + CHAR(13)

			SET @SQL = @SQL + '		 AND (( '''+ @filterKey +''' = ''ModuleApproval.ModuleName'' AND ModuleApproval.ModuleName LIKE ''%' + @filterValue + '%'')' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '		 OR ( '''+ @filterKey +''' = ''PK_ModuleApproval_ID'' AND PK_ModuleApproval_ID LIKE ''%' + @filterValue + '%'')' + CHAR(10) + CHAR(13)
			-- SET @SQL = @SQL + '		 OR ( '''+ @filterKey +''' = ''ModuleApproval.ModuleField'' AND ModuleApproval.ModuleField LIKE ''%' + @filterValue + '%'')' + CHAR(10) + CHAR(13)
			-- SET @SQL = @SQL + '		 OR ( '''+ @filterKey +''' = ''ModuleApproval.ModuleFieldBefore'' AND ModuleApproval.ModuleFieldBefore LIKE ''%' + @filterValue + '%'')' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '		 OR ( '''+ @filterKey +''' = ''ModuleKey'' AND ModuleKey LIKE ''%' + @filterValue + '%'')' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '		 OR ( '''+ @filterKey +''' = ''ModuleApproval.CreatedDate'' AND FORMAT(ModuleApproval.CreatedDate, ''' + @DateTimeFormat + ''') LIKE ''%' + @filterValue + '%'')' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '		 OR ( '''+ @filterKey +''' = ''ModuleApproval.CreatedBy'' AND ModuleApproval.CreatedBy LIKE ''%' + @filterValue + '%'')' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '		 OR ( '''+ @filterKey +''' = ''ModuleActionName'' AND ModuleActionName LIKE ''%' + @filterValue + '%''))' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '		 AND NOT EXISTS (' + CHAR(10) + CHAR(13)
											--exclude users who have previously processed approval
			SET @SQL = @SQL + '				SELECT 13' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '				FROM MWorkFlow_History AS mfh' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '					JOIN MWorkflow_ApprovalStatus AS mas ON mfh.FK_MWorkflow_ApprovalStatus_ID=mas.PK_MWorkflow_ApprovalStatus_ID' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '				WHERE mfh.FK_ModuleApproval_ID = CAST(ModuleApproval.PK_ModuleApproval_ID AS VARCHAR(MAX))' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '					AND mfh.FK_MWorkflow_ApprovalStatus_ID IN (1,4)' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '					AND mfh.FK_MUserId = ' + CAST(@PK_MUser_ID AS VARCHAR) + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '		 )' + CHAR(10) + CHAR(13)
			IF @DataAccess != ''
				BEGIN
					SET @SQL = @SQL + '		 AND ('+ @DataAccess + ')' + CHAR(10) + CHAR(13)
				END
			SET @SQL = @SQL + 'ORDER BY ' + @OrderBy + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + 'OFFSET ' + CAST((@PageIndex - 1) * @PageSize AS VARCHAR) + ' ROWS' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + 'FETCH NEXT ' + CAST(@PageSize AS VARCHAR) + ' ROWS ONLY' + CHAR(10) + CHAR(13)
		END
		ELSE
		BEGIN
			SET @SQL =		  'SELECT ModuleApproval.*, ModuleActionName' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + 'FROM ModuleApproval' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '		JOIN Module' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '		ON ModuleApproval.ModuleName = Module.ModuleName' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '		JOIN ModuleAction' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '		ON ModuleApproval.PK_ModuleAction_ID = ModuleAction.PK_ModuleAction_ID' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '		JOIN MUser UserCreator' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '		ON ModuleApproval.CreatedBy = UserCreator.UserID' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + 'WHERE PK_Module_ID = ' + @ModuleKey + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '		 AND ModuleApproval.CreatedBy <> ''' + @UserID + '''' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '		 AND (( '''+ @filterKey +''' = ''ModuleApproval.ModuleName'' AND ModuleApproval.ModuleName LIKE ''%' + @filterValue + '%'')' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '		 OR ( '''+ @filterKey +''' = ''PK_ModuleApproval_ID'' AND PK_ModuleApproval_ID LIKE ''%' + @filterValue + '%'')' + CHAR(10) + CHAR(13)
			-- SET @SQL = @SQL + '		 OR ( '''+ @filterKey +''' = ''ModuleApproval.ModuleField'' AND ModuleApproval.ModuleField LIKE ''%' + @filterValue + '%'')' + CHAR(10) + CHAR(13)
			-- SET @SQL = @SQL + '		 OR ( '''+ @filterKey +''' = ''ModuleApproval.ModuleFieldBefore'' AND ModuleApproval.ModuleFieldBefore LIKE ''%' + @filterValue + '%'')' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '		 OR ( '''+ @filterKey +''' = ''ModuleKey'' AND ModuleKey LIKE ''%' + @filterValue + '%'')' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '		 OR ( '''+ @filterKey +''' = ''ModuleApproval.CreatedDate'' AND FORMAT(ModuleApproval.CreatedDate, ''' + @DateTimeFormat + ''') LIKE ''%' + @filterValue + '%'')' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '		 OR ( '''+ @filterKey +''' = ''ModuleApproval.CreatedBy'' AND ModuleApproval.CreatedBy LIKE ''%' + @filterValue + '%'')' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '		 OR ( '''+ @filterKey +''' = ''ModuleActionName'' AND ModuleActionName LIKE ''%' + @filterValue + '%''))' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + '		 AND ISNULL(ModuleApproval.FK_MRole_ID, UserCreator.FK_MRole_ID) = ' + CAST(@RoleID AS VARCHAR) + CHAR(10) + CHAR(13)
			IF @DataAccess != ''
				BEGIN
					SET @SQL = @SQL + '		 AND ('+ @DataAccess + ')' + CHAR(10) + CHAR(13)
				END
			SET @SQL = @SQL + 'ORDER BY ' + @OrderBy + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + 'OFFSET ' + CAST((@PageIndex - 1) * @PageSize AS VARCHAR) + ' ROWS' + CHAR(10) + CHAR(13)
			SET @SQL = @SQL + 'FETCH NEXT ' + CAST(@PageSize AS VARCHAR) + ' ROWS ONLY' + CHAR(10) + CHAR(13)
		END

		EXEC(@SQL)
		PRINT(@SQL)
	END
