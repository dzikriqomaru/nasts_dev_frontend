DECLARE @moduleID INT; 
SELECT TOP 1 @moduleID = PK_Module_ID 
		FROM Module m
		WHERE m.ModuleName = 'DataAccessReplacer'

IF NOT EXISTS (
		SELECT 1 
		FROM ModuleValidation mv 
		WHERE mv.FK_Module_ID = @moduleID
	)
BEGIN
	INSERT INTO ModuleValidation 
	(FK_Module_ID, FK_ModuleAction_ID, FK_ModuleTime_ID, StoreProcedureName, StoreProcedureParameter, StoreProcedureParameterValueFieldSequence)
	VALUES 
	(@moduleID, 2, 2, 'usp_HandleAfterSaveSettingUserReplacer', '', ''),
	(@moduleID, 3, 2, 'usp_HandleAfterSaveSettingUserReplacer', '', '')
END


UPDATE Module
SET IsUseStoreProcedureValidation = 1
WHERE ModuleName = 'DataAccessReplacer'