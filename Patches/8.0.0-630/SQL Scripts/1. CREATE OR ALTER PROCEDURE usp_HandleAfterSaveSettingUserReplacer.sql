SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Davin julian
-- Create date: 2 Maret 2024
-- Description:	Delete setting user when update or delete replacer
-- =============================================
CREATE OR ALTER PROCEDURE usp_HandleAfterSaveSettingUserReplacer
AS
BEGIN
	--Delete user setting if not exists in data access replacer
	DELETE SettingUser 
	WHERE PK_Setting_ID IN (
		SELECT su.PK_Setting_ID 
		FROM DataAccessReplacer dar 
		RIGHT JOIN SettingUser su ON dar.Replacer_Name = su.SettingName
		WHERE dar.PK_Replacer_ID IS NULL 
	)
END
