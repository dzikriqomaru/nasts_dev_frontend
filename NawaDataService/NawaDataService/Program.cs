using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using NawaDataDAL;

namespace NawaDataService
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseWindowsService()
                .ConfigureServices((hostContext, services) =>
                {
                    string server = hostContext.Configuration.GetValue<string>("Connection:Server");
                    string database = hostContext.Configuration.GetValue<string>("Connection:Database");
                    string user = hostContext.Configuration.GetValue<string>("Connection:User");
                    string password = hostContext.Configuration.GetValue<string>("Connection:Password");
                    //if (!string.IsNullOrEmpty(password))
                    //{
                    //    string key = hostContext.Configuration.GetValue<string>("PrivateKey");
                    //    password = NawaEncryption.Common.DecryptRijndael(password, key);
                    //}
                    int timeout = Convert.ToInt32(hostContext.Configuration.GetValue<string>("Connection:CommandTimeout"));

                    NawaDAO.Config(NawaDAO.DBType.SQLServer, server, database, user, password, String.Empty, timeout);

                    string DTExec = hostContext.Configuration.GetValue<string>("DTExec");
                    EODSchedulerSatuanBLL.DTExec = DTExec;

                    //services.AddHostedService<Worker>();

                    WorkerOptions options = hostContext.Configuration.GetSection("ReportHandler").Get<WorkerOptions>();

                    services.AddSingleton(options);

                    services.AddSingleton<IHostedService, WorkerEOD>();
                    services.AddSingleton<IHostedService, WorkerEmail>();
                });
    }
}
