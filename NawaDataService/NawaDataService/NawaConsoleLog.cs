﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Data.SqlClient;
using NawaDataDAL;

namespace NawaDataService
{
    public class NawaConsoleLog
    {
        public void LogInfo(object objmessage)
        {
            string query = "INSERT INTO LogConsoleService(LogStatus, LogInfo, LogDescription, LogCreatedDate) VALUES(@LogStatus, @LogInfo, @LogDescription, @LogCreatedDate)";
            SqlParameter[] sqlParams = new SqlParameter[]
            {
                new SqlParameter("@LogStatus", System.Data.SqlDbType.VarChar) {Value = "INFO"},
                new SqlParameter("@LogInfo", System.Data.SqlDbType.VarChar) {Value = objmessage.ToString()},
                new SqlParameter("@LogDescription", System.Data.SqlDbType.VarChar) {Value = objmessage.ToString()},
                new SqlParameter("@LogCreatedDate", System.Data.SqlDbType.DateTime) {Value = DateTime.Now}
            };

            NawaDAO.ExecuteNonQuery(query, sqlParams);
        }
        public void LogError(object objmessage, Exception objexception)
        {
            string query = "INSERT INTO LogConsoleService(LogStatus, LogInfo, LogDescription, LogCreatedDate) VALUES(@LogStatus, @LogInfo, @LogDescription, @LogCreatedDate)";
            SqlParameter[] sqlParams = new SqlParameter[]
            {
                new SqlParameter("@LogStatus", System.Data.SqlDbType.VarChar) {Value = "ERROR"},
                new SqlParameter("@LogInfo", System.Data.SqlDbType.VarChar) {Value = objmessage.ToString()},
                new SqlParameter("@LogDescription", System.Data.SqlDbType.VarChar) {Value = objexception.ToString()},
                new SqlParameter("@LogCreatedDate", System.Data.SqlDbType.DateTime) {Value = DateTime.Now}
            };

            NawaDAO.ExecuteNonQuery(query, sqlParams);
        }
    }
}
