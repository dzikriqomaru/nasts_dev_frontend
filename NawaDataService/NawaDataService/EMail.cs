﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace NawaDataService
{
    public class EMail
    {
        private string m_sSender;
        private string m_sRecipient;
        private string m_sRecipientCC;
        private string m_sRecipientBCC;
        private string m_sServer;
        private string m_sSubject;
        private string m_sBody;
        private MailPriority m_mPriority;
        private string m_sMailAttachment;
        private string m_sLastErrorMessage;
        private bool m_IsBodyHtml;
        private bool m_bLastSendState;
        private Dictionary<string, string> m_HeaderList;
        private NawaConsoleLog mylog = new NawaConsoleLog();
        private Dictionary<string, Stream> m_MailAttachmentFromStream;
        // Private Shared ReadOnly myLog As log4net.ILog = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)

        /// <summary>
        ///     ''' Get or set Subject of email
        ///     ''' </summary>
        ///     ''' <value></value>
        ///     ''' <returns></returns>
        ///     ''' <remarks></remarks>
        ///     ''' <history>
        ///     ''' [Johan Peterson] 2007-04-30	Created
        ///     ''' </history>
        public string Subject
        {
            get
            {
                return m_sSubject;
            }
            set
            {
                m_sSubject = value;
            }
        }

        /// <summary>
        ///     ''' Get or set Body of email
        ///     ''' </summary>
        ///     ''' <value></value>
        ///     ''' <returns></returns>
        ///     ''' <remarks></remarks>
        public string Body
        {
            get
            {
                return m_sBody;
            }
            set
            {
                m_sBody = value;
            }
        }

        /// <summary>
        ///     ''' Get or set Sender of email
        ///     ''' </summary>
        ///     ''' <value></value>
        ///     ''' <returns></returns>
        ///     ''' <remarks></remarks>
        public string Sender
        {
            get
            {
                return m_sSender;
            }
            set
            {
                m_sSender = value;
            }
        }

        /// <summary>
        ///     ''' Get or set Recipient of email. For multiple recipient, separate it using ";"
        ///     ''' </summary>
        ///     ''' <value></value>
        ///     ''' <returns></returns>
        ///     ''' <remarks></remarks>
        public string Recipient
        {
            get
            {
                return m_sRecipient;
            }
            set
            {
                m_sRecipient = value;
            }
        }

        /// <summary>
        ///     ''' Get or set Recipient CC of email. For multiple recipient, separate it using ";"
        ///     ''' </summary>
        ///     ''' <value></value>
        ///     ''' <returns></returns>
        ///     ''' <remarks></remarks>
        public string RecipientCC
        {
            get
            {
                return m_sRecipientCC;
            }
            set
            {
                m_sRecipientCC = value;
            }
        }

        /// <summary>
        ///     ''' Get or set Recipient BCC of email. For multiple recipient, separate it using ";"
        ///     ''' </summary>
        ///     ''' <value></value>
        ///     ''' <returns></returns>
        ///     ''' <remarks></remarks>
        public string RecipientBCC
        {
            get
            {
                return m_sRecipientBCC;
            }
            set
            {
                m_sRecipientBCC = value;
            }
        }

        /// <summary>
        ///     ''' Get or set value of SMTP Server
        ///     ''' </summary>
        ///     ''' <value></value>
        ///     ''' <returns></returns>
        ///     ''' <remarks></remarks>
        public string Server
        {
            get
            {
                return m_sServer;
            }
            set
            {
                m_sServer = value;
            }
        }

        /// <summary>
        ///     ''' Get or set priority of the email
        ///     ''' </summary>
        ///     ''' <value></value>
        ///     ''' <returns></returns>
        ///     ''' <remarks></remarks>
        public MailPriority Priority
        {
            get
            {
                return m_mPriority;
            }
            set
            {
                m_mPriority = value;
            }
        }

        public Dictionary<string, string> HeaderList
        {
            get
            {
                return m_HeaderList;
            }

            set
            {
                m_HeaderList = value;
            }
        }

        /// <summary>
        ///     ''' Get or set mail attachment with fullpath of filename. For multiple attachment, separate it using ";"
        ///     ''' </summary>
        ///     ''' <value></value>
        ///     ''' <returns></returns>
        ///     ''' <remarks></remarks>
        public string MailAttachment
        {
            get
            {
                return m_sMailAttachment;
            }
            set
            {
                m_sMailAttachment = value;
            }
        }

        /// <summary>
        ///     ''' Get or set mail attachment with fullpath of filename. For multiple attachment, separate it using ";"
        ///     ''' </summary>
        ///     ''' <value></value>
        ///     ''' <returns></returns>
        ///     ''' <remarks></remarks>
        public Dictionary<string, Stream> MailAttachmentFromStream
        {
            get
            {
                return m_MailAttachmentFromStream;
            }
            set
            {
                m_MailAttachmentFromStream = value;
            }
        }

        /// <summary>
        ///     ''' Get or set last send state.
        ///     ''' </summary>
        ///     ''' <value></value>
        ///     ''' <returns></returns>
        ///     ''' <remarks></remarks>
        public bool LastSendState
        {
            get
            {
                return m_bLastSendState;
            }
            set
            {
                m_bLastSendState = value;
            }
        }

        /// <summary>
        ///     ''' Get or set mail body type is HTML or not
        ///     ''' </summary>
        ///     ''' <value></value>
        ///     ''' <returns></returns>
        ///     ''' <remarks></remarks>
        public bool IsBodyHtml
        {
            get
            {
                return m_IsBodyHtml;
            }
            set
            {
                m_IsBodyHtml = value;
            }
        }

        /// <summary>
        ///     ''' Get last error message
        ///     ''' </summary>
        ///     ''' <value></value>
        ///     ''' <returns></returns>
        ///     ''' <remarks></remarks>
        public string LastErrorMessage
        {
            get
            {
                return m_sLastErrorMessage;
            }
        }

        /// <summary>
        ///     ''' Initial email
        ///     ''' </summary>
        ///     ''' <remarks></remarks>
        private void Init()
        {
            m_sBody = "";
            m_sSubject = "";
            m_sSender = "";
            m_sRecipient = "";
            m_sRecipientCC = "";
            m_sRecipientBCC = "";
            m_sLastErrorMessage = "";

            m_sServer = SystemParameterBLL.GetSystemParameter(2000);
            
            m_mPriority = MailPriority.Normal;

            m_IsBodyHtml = Convert.ToBoolean(SystemParameterBLL.GetSystemParameter(2011));
            
            m_bLastSendState = false;
        }

        /// <summary>
        ///     ''' New implementation of Email class
        ///     ''' </summary>
        ///     ''' <remarks></remarks>
        public EMail()
        {
            Init();
        }

        /// <summary>
        ///     ''' Dispose implementation of Email class
        ///     ''' </summary>
        ///     ''' <remarks></remarks>
        public void Dispose()
        {
        }

        /// <summary>
        ///     ''' To initial email with default initial value
        ///     ''' </summary>
        ///     ''' <remarks></remarks>
        public void Clear()
        {
            Init();
        }


        /// <summary>
        ///     ''' Check if recipient and sender is valid
        ///     ''' </summary>
        ///     ''' <returns></returns>
        ///     ''' <remarks></remarks>
        private bool isValidMail()
        {
            if (m_sSender.Trim().Length > 0)
            {
                if (m_sRecipient.Trim().Length > 0)
                    return true;
                else
                {
                    m_sLastErrorMessage = "Invalid Recipient Address";
                    return false;
                }
            }
            else
            {
                m_sLastErrorMessage = "Invalid Sender Address";
                return false;
            }
        }

        /// <summary>
        ///     ''' 'Send Email Synchronously with all properties.
        ///     ''' </summary>
        ///     ''' <returns>Return TRUE if send mail done successfully, otherwise return FALSE</returns>
        ///     ''' <remarks></remarks>
        public bool SendEmail()
        {
            // For a Windows Forms Application, add a reference to 
            // System.Web.dll using [Project Add References]


            MailMessage email;
            char chrDelimiter = ';';
            string[] arrFileName;
            int intFileNameCounter;
            Attachment att;
            bool blnSend;
            SmtpClient smtpMail;
            int Counter;
            string[] arrMailAddress;
            try
            {
                smtpMail = new SmtpClient(m_sServer);
                if (isValidMail())
                {
                    smtpMail.DeliveryMethod = SmtpDeliveryMethod.Network;

                    email = new MailMessage();
                    // The email address of the sender
                    email.From = new MailAddress(m_sSender);

                    // The email address of the recipient. 
                    // Seperate multiple email adresses with a comma seperator
                    m_sRecipient = m_sRecipient.Replace(",", ";");
                    arrMailAddress = m_sRecipient.Split(chrDelimiter);
                    for (Counter = 0; Counter <= arrMailAddress.Length - 1; Counter++)
                        email.To.Add(arrMailAddress[Counter]);

                    if (m_sRecipientCC.Trim().Length > 0)
                    {
                        m_sRecipientCC = m_sRecipientCC.Replace(",", ";");
                        arrMailAddress = m_sRecipientCC.Split(chrDelimiter);
                        for (Counter = 0; Counter <= arrMailAddress.Length - 1; Counter++)
                            email.CC.Add(arrMailAddress[Counter]);
                    }

                    if (m_sRecipientBCC.Trim().Length > 0)
                    {
                        m_sRecipientBCC = m_sRecipientBCC.Replace(",", ";");
                        arrMailAddress = m_sRecipientBCC.Split(chrDelimiter);
                        for (Counter = 0; Counter <= arrMailAddress.Length - 1; Counter++)
                            email.Bcc.Add(arrMailAddress[Counter]);
                    }

                    // The subject of the email
                    email.Subject = m_sSubject;

                    if (m_HeaderList.Count > 0)
                    {
                        foreach (KeyValuePair<string, string> item in m_HeaderList)
                            email.Headers[item.Key] = item.Value;
                    }

                    // The Priority attached and displayed for the email
                    email.Priority = m_mPriority;

                    // The format of the contents of the email
                    // The email format can either be MailFormat.Html or MailFormat.Text
                    // MailFormat.Html : Html Content
                    // MailFormat.Text : Text Message
                    email.IsBodyHtml = m_IsBodyHtml;

                    // The contents of the email 
                    email.Body = m_sBody;

                    // EMail Attachment 
                    // Build an IList of mail attachments using the files named in the string.
                    if (!String.IsNullOrEmpty(m_sMailAttachment))
                    {
                        if (m_sMailAttachment.Trim().Length > 0)
                        {
                            arrFileName = m_sMailAttachment.Split(chrDelimiter);
                            for (intFileNameCounter = 0; intFileNameCounter <= arrFileName.Length - 1; intFileNameCounter++)
                            {
                                att = new Attachment(arrFileName[intFileNameCounter].ToString());
                                email.Attachments.Add(att);

                                att = null;
                            }
                        }
                    }

                    // The name of the smtp server to use for sending emails
                    // SmtpMail.SmtpServer is commonly ignored in many applications 


                    // Send the email and handle any error that occurs
                    try
                    {
                        string strpassword;

                        try
                        {
                            strpassword = SystemParameterBLL.GetSystemParameter(2003);
                        }
                        catch(Exception ex)
                        {
                            strpassword = "";
                        }

                        string struserEmail;

                        try
                        {
                            struserEmail = SystemParameterBLL.GetSystemParameter(2002);
                        }
                        catch (Exception ex)
                        {
                            struserEmail = "";
                        }

                        string strport = "";

                        try
                        {
                            strport = SystemParameterBLL.GetSystemParameter(2001);
                        }
                        catch (Exception ex)
                        {
                            strport = "";
                        }

                        bool bssl = false;

                        try
                        {
                            bssl = Convert.ToBoolean(SystemParameterBLL.GetSystemParameter(2004));
                        }
                        catch (Exception ex)
                        {
                            bssl = false;
                        }

                        smtpMail.EnableSsl = bssl;
                        smtpMail.Port = Convert.ToInt32(strport);

                        smtpMail.Credentials = new System.Net.NetworkCredential(struserEmail, strpassword);
                        smtpMail.Send(email);
                        blnSend = true;
                    }
                    catch (Exception ex)
                    {
                        mylog.LogError("Failed send email to " + m_sRecipient + " with subject " + m_sSubject + " cause: " + ex.Message, ex);

                        m_sLastErrorMessage = ex.Message;
                        blnSend = false;
                        throw;
                    }
                }
                else
                    blnSend = false;
                m_bLastSendState = blnSend;
                return blnSend;
            }
            catch (Exception ex)
            {
                m_sLastErrorMessage = ex.Message;
                blnSend = false;
                throw ex;
            }
            finally
            {
                smtpMail = null;
            }
        }

        /// <summary>
        ///     ''' 'Send Email Synchronously with all properties.
        ///     ''' </summary>
        ///     ''' <returns>Return TRUE if send mail done successfully, otherwise return FALSE</returns>
        ///     ''' <remarks></remarks>
        public bool SendEmail(Dictionary<string, Stream> DicAttachment, int sensitivity, int importance, bool flagDelivery, bool flagRead)
        {
            // For a Windows Forms Application, add a reference to 
            // System.Web.dll using [Project Add References]


            MailMessage email;
            char chrDelimiter = ';';
            string[] arrFileName;
            int intFileNameCounter;
            Attachment att;
            bool blnSend;
            SmtpClient smtpMail;
            int Counter;
            string[] arrMailAddress;
            try
            {
                smtpMail = new SmtpClient(m_sServer);
                if (isValidMail())
                {
                    smtpMail.DeliveryMethod = SmtpDeliveryMethod.Network;

                    email = new MailMessage();
                    // The email address of the sender
                    email.From = new MailAddress(m_sSender);

                    // The email address of the recipient. 
                    // Seperate multiple email adresses with a comma seperator
                    m_sRecipient = m_sRecipient.Replace(",", ";");
                    arrMailAddress = m_sRecipient.Split(chrDelimiter);
                    for (Counter = 0; Counter <= arrMailAddress.Length - 1; Counter++)
                        email.To.Add(arrMailAddress[Counter]);

                    if (m_sRecipientCC.Trim().Length > 0)
                    {
                        m_sRecipientCC = m_sRecipientCC.Replace(",", ";");
                        arrMailAddress = m_sRecipientCC.Split(chrDelimiter);
                        for (Counter = 0; Counter <= arrMailAddress.Length - 1; Counter++)
                            email.CC.Add(arrMailAddress[Counter]);
                    }

                    if (m_sRecipientBCC.Trim().Length > 0)
                    {
                        m_sRecipientBCC = m_sRecipientBCC.Replace(",", ";");
                        arrMailAddress = m_sRecipientBCC.Split(chrDelimiter);
                        for (Counter = 0; Counter <= arrMailAddress.Length - 1; Counter++)
                            email.Bcc.Add(arrMailAddress[Counter]);
                    }

                    // The subject of the email
                    email.Subject = m_sSubject;

                    if (m_HeaderList.Count > 0)
                    {
                        foreach (KeyValuePair<string, string> item in m_HeaderList)
                            email.Headers[item.Key] = item.Value;
                    }

                    //for read receipt
                    if (flagRead)
                    {
                        email.Headers.Add("Disposition-Notification-To", "email@gmail.com");
                    }

                    //for delivery receipt
                    if (flagDelivery)
                    {
                        email.DeliveryNotificationOptions = DeliveryNotificationOptions.OnSuccess;
                    }

                    if(sensitivity > 1)
                    {
                        switch (sensitivity)
                        {
                            case 2:
                                email.Headers.Add("Sensitivity", "Personal");
                                break;
                            case 3:
                                email.Headers.Add("Sensitivity", "Private");
                                break;
                            case 4:
                                email.Headers.Add("Sensitivity", "Company-Confidential");
                                break;
                        }
                    }

                    if (importance != 2)
                    {
                        switch (importance)
                        {
                            case 1:
                                email.Priority = MailPriority.Low;
                                break;
                            case 3:
                                email.Priority = MailPriority.High;
                                break;
                        }
                    }

                    // The Priority attached and displayed for the email
                    email.Priority = m_mPriority;

                    // The format of the contents of the email
                    // The email format can either be MailFormat.Html or MailFormat.Text
                    // MailFormat.Html : Html Content
                    // MailFormat.Text : Text Message
                    email.IsBodyHtml = m_IsBodyHtml;

                    // The contents of the email 
                    email.Body = m_sBody;

                    // EMail Attachment 
                    // Build an IList of mail attachments using the files named in the string.

                    if (DicAttachment != null)
                    {
                        for (int IntCounter = 0; IntCounter <= DicAttachment.Count - 1; IntCounter++)
                        {
                            att = new Attachment(DicAttachment.ElementAt(IntCounter).Value, DicAttachment.ElementAt(IntCounter).Key);
                            email.Attachments.Add(att);
                            att = null;
                        }
                    }

                    // The name of the smtp server to use for sending emails
                    // SmtpMail.SmtpServer is commonly ignored in many applications 


                    // Send the email and handle any error that occurs
                    try
                    {
                        string strpassword;

                        try
                        {
                            strpassword = SystemParameterBLL.GetSystemParameter(2003);
                        }
                        catch (Exception ex)
                        {
                            strpassword = "";
                        }

                        string struserEmail;

                        try
                        {
                            struserEmail = SystemParameterBLL.GetSystemParameter(2002);
                        }
                        catch (Exception ex)
                        {
                            struserEmail = "";
                        }

                        string strport = "";

                        try
                        {
                            strport = SystemParameterBLL.GetSystemParameter(2001);
                        }
                        catch (Exception ex)
                        {
                            strport = "";
                        }

                        bool bssl = false;

                        try
                        {
                            bssl = Convert.ToBoolean(SystemParameterBLL.GetSystemParameter(2004));
                        }
                        catch (Exception ex)
                        {
                            bssl = false;
                        }

                        smtpMail.EnableSsl = bssl;
                        smtpMail.Port = Convert.ToInt32(strport);

                        smtpMail.Credentials = new System.Net.NetworkCredential(struserEmail, strpassword);
                        smtpMail.Send(email);
                        blnSend = true;
                    }
                    catch (Exception ex)
                    {
                        mylog.LogError("Failed send email to " + m_sRecipient + " with subject " + m_sSubject + " cause: " + ex.Message, ex);

                        m_sLastErrorMessage = ex.Message;
                        blnSend = false;
                        throw;
                    }
                }
                else
                    blnSend = false;
                m_bLastSendState = blnSend;
                return blnSend;
            }
            catch (Exception ex)
            {
                m_sLastErrorMessage = ex.Message;
                blnSend = false;
                throw ex;
            }
            finally
            {
                smtpMail = null;
            }
        }
    }
}
