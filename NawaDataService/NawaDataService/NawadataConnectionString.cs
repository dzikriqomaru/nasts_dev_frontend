﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NawaDataService
{
    using System;

    public class NawadataConnectionString
    {
        public int PK_NawaDataConnectionString_ID { get; set; }
        public string ConnecitonName { get; set; }
        public string ConnectionValue { get; set; }
        public string PasswordValue { get; set; }
        public bool Active { get; set; }
        public string CreatedBy { get; set; }
        public string LastUpdateBy { get; set; }
        public string ApprovedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastUpdateDate { get; set; }
        public DateTime ApprovedDate { get; set; }
        public string Alternateby { get; set; }
        public string PasswordValue1 { get; set; }
        public string PasswordEncript { get; set; }
        public byte[] PasswordBinary { get; set; }
        public string PasswordBinaryName { get; set; }
    }

}
