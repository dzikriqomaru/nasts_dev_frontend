using ICSharpCode.SharpZipLib.Zip;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NawaDataDAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NawaDataService
{
    public class WorkerEmail : BackgroundService
    {
        private readonly ILogger<WorkerEmail> _logger;
        private readonly WorkerOptions options;
        private NawaConsoleLog mylog = new NawaConsoleLog();

        public WorkerEmail(ILogger<WorkerEmail> logger, WorkerOptions options)
        {
            _logger = logger;
            this.options = options;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            mylog.LogInfo("Service Email Started");

            while (!stoppingToken.IsCancellationRequested)
            {
                _logger.LogInformation("Worker Email running at: {time}", DateTimeOffset.Now);

                Run();

                await Task.Delay(1000, stoppingToken);
            }

            mylog.LogInfo("Service Email Stopped");
        }

        protected void Run()
        {
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[] { };
                NawaDAO.ExecuteNonQuery("EXEC usp_SchedulerEmail");

                DataTable dt = NawaDAO.ExecuteTable("SELECT * FROM EmailTemplateScheduler ets INNER JOIN EmailTemplate et ON ets.PK_EmailTemplate_ID=et.PK_EmailTemplate_ID WHERE ets.FK_EmailStatus_ID=1");

                foreach (DataRow item in dt.Rows)
                {
                    try
                    {
                        DataTable dtTablePrimary = NawaDAO.ExecuteTable("SELECT eta.NamaTable, eta.QueryData, eta.FieldUnikPrimaryTable  FROM EmailTemplateAdditional eta WHERE eta.FK_EmailTableType_ID=1 AND eta.FK_EmailTemplate_ID=" + item["PK_EmailTemplate_ID"]);

                        foreach (DataRow item1 in dtTablePrimary.Rows)
                        {
                            sqlParameters = new SqlParameter[]
                            {
                                new SqlParameter("@tablename", item1["NamaTable"]),
                                new SqlParameter("@querydata", item1["QueryData"])
                            };

                            NawaDAO.ExecuteNonQuery("exec usp_CreateTableEmailPrimary @tablename,@querydata", sqlParameters);

                            string sql = "";
                            sql = "INSERT INTO EmailTemplateSchedulerDetail ";
                            sql += " ( ";
                            sql += " 	 ";
                            sql += " 	FK_EmailTEmplateScheduler_ID, ";
                            sql += " 	UnikFieldTablePrimary, ";
                            sql += " 	EmailTo, ";
                            sql += " 	EmailCC, ";
                            sql += " 	EmailBCC, ";
                            sql += " 	EmailSubject, ";
                            sql += " 	EmailBody, ";
                            sql += " 	ProcessDate, ";
                            sql += " 	SendEmailDate, ";
                            sql += " 	FK_EmailStatus_ID, ";
                            sql += " 	ErrorMessage, ";
                            sql += " 	retrycount, ";
                            sql += " 	AttachmentPassword, ";
                            sql += " 	AttachmentPasswordSalt ";
                            sql += " ) ";
                            sql += " select  ";
                            sql += " '" + item["PK_EmailTemplateScheduler_ID"] + "' ,";
                            sql += " " + item1["FieldUnikPrimaryTable"] + " ,";
                            sql += " '" + item["EmailTo"] + "' ,";
                            sql += " '" + item["EmailCC"] + "' ,";
                            sql += " '" + item["EmailBCC"] + "' ,";
                            sql += " '" + item["EmailSubject"] + "' ,";
                            sql += " '" + item["EmailBody"] + "' ,";
                            sql += " '" + Convert.ToDateTime(item["ProcessDate"]).ToString("yyyy-MM-dd HH:mm:ss") + "' ,";
                            sql += " null ,";
                            sql += " '1' ,";
                            sql += " '' ,";
                            sql += " '0', ";
                            sql += " '@password', ";
                            sql += " '' ";
                            sql += " from __" + item1["NamaTable"];

                            NawaDAO.ExecuteNonQuery(sql);

                            DataTable dtschedulerdetail = NawaDAO.ExecuteTable("SELECT * FROM EmailTemplateSchedulerDetail WHERE FK_EmailTEmplateScheduler_ID=" + item["PK_EmailTemplateScheduler_ID"]);
                            DataTable dtemaildetail = NawaDAO.ExecuteTable("SELECT etd.Replacer , SUBSTRING(etd.FieldReplacer,1,1)+'__'+SUBSTRING(etd.FieldReplacer,2,LEN(etd.FieldReplacer)-1) AS FieldReplacer  FROM EmailTemplateDetail etd WHERE etd.FK_EmailTemplate_ID=" + item["PK_EmailTemplate_ID"]);

                            foreach (DataRow rowschedulerdetail in dtschedulerdetail.Rows)
                            {

                                //loooping untuk setiap emailtemplatescheduleradditional
                                DataTable dtTableAdditional = NawaDAO.ExecuteTable("SELECT eta.NamaTable, eta.QueryData, eta.FieldUnikPrimaryTable ,eta.FK_EmailTableType_ID FROM EmailTemplateAdditional eta WHERE (eta.FK_EmailTableType_ID=2 or eta.FK_EmailTableType_ID=3) AND eta.FK_EmailTemplate_ID=" + item["PK_EmailTemplate_ID"]);
                                foreach (DataRow item2 in dtTableAdditional.Rows)
                                {
                                    //generate table additional detail
                                    string strquery = Convert.ToString(item2["QueryData"]); //System.Net.WebUtility.HtmlDecode(item2("QueryData"))
                                    strquery = strquery.Replace("@ID", "'" + rowschedulerdetail["UnikFieldTablePrimary"] + "'");
                                    sqlParameters = new SqlParameter[]
                                    {
                                        new SqlParameter("@tablename", item2["NamaTable"]),
                                        new SqlParameter("@querydata", strquery)
                                    };
                                    NawaDAO.ExecuteNonQuery("exec usp_CreateTableEmailPrimary @tablename,@querydata", sqlParameters);
                                }




                                //replace isi tabledetail
                                foreach (DataRow rowreplacer in dtemaildetail.Rows)
                                {
                                    sqlParameters = new SqlParameter[]
                                    {
                                        new SqlParameter("@PK_EmailTemplateSchedulerDetail_ID", rowschedulerdetail["PK_EmailTemplateSchedulerDetail_ID"]),
                                        new SqlParameter("@Replacer", "'" + rowreplacer["Replacer"] + "'"),
                                        new SqlParameter("@FieldReplacer", rowreplacer["FieldReplacer"]),
                                        new SqlParameter("@pk_emailitemplate_id", item["PK_EmailTemplate_ID"])
                                    };
                                    NawaDAO.ExecuteNonQuery("exec usp_replaceEmailSchedulerDetail @PK_EmailTemplateSchedulerDetail_ID,@Replacer,@FieldReplacer,@pk_emailitemplate_id", sqlParameters);
                                }

                                //replace  emailid ke field emailid kalau memang ada replacer $emailid$
                                //kalau kosong emailidnya, maka generate emailidnya supaya
                                Int32 intJmlReplaceremailid = dtemaildetail.Select("replacer='$emailid$'").Count();
                                if (intJmlReplaceremailid > 0)
                                {
                                    sqlParameters = new SqlParameter[]
                                    {
                                        new SqlParameter("@PK_EmailTemplateSchedulerDetail_ID", rowschedulerdetail["PK_EmailTemplateSchedulerDetail_ID"]),
                                        new SqlParameter("@Replacer", "$EmailId$"),
                                        new SqlParameter("@FieldReplacer", rowschedulerdetail["EmailID"]),
                                        new SqlParameter("@pk_emailitemplate_id", item["PK_EmailTemplate_ID"]),
                                    };
                                    NawaDAO.ExecuteNonQuery("exec usp_replaceEmailSchedulerDetail @PK_EmailTemplateSchedulerDetail_ID,@Replacer,@FieldReplacer,@pk_emailitemplate_id", sqlParameters);
                                }

                                //Update EmailIDReference with EmailID because of System Initiate
                                NawaDAO.ExecuteNonQuery(string.Format("UPDATE EmailTemplateSchedulerDetail SET EmailIDReference=EmailID WHERE PK_EmailTemplateSchedulerDetail_ID = {0} AND (EmailIDReference IS NULL OR EmailIDReference='')", rowschedulerdetail["PK_EmailTemplateSchedulerDetail_ID"].ToString()));
                            }
                        }

                        #region Attachment
                        //attachment
                        string query = @"SELECT PK_EmailTemplateAttachment_ID, COALESCE(UseAttachment, '') UseAttachment, FK_EmailAttachmentType_ID, NamaReport
                                            FROM EmailTemplateAttachment
	                                                JOIN EmailTemplateScheduler
	                                                ON EmailTemplateAttachment.FK_EmailTemplate_ID = EmailTemplateScheduler.PK_EmailTemplate_ID
                                         WHERE EmailTemplateScheduler.PK_EmailTemplateScheduler_ID = " + item["PK_EmailTemplateScheduler_ID"].ToString();
                        DataTable listAttachmentTemplate = NawaDAO.ExecuteTable(query);
                        //loop listAttachmentTemplate
                        foreach (DataRow attachmentTemplate in listAttachmentTemplate.Rows)
                        {
                            //if binary type
                            if (attachmentTemplate["FK_EmailAttachmentType_ID"] is 5)
                            {
                                foreach (DataRow item1 in dtTablePrimary.Rows)
                                {
                                    string strtablename = Convert.ToString(item1["NamaTable"]);
                                    if (strtablename.Contains(" "))
                                    {
                                        strtablename = "\"__" + strtablename + "\"";
                                    }
                                    else
                                    {
                                        strtablename = "__" + strtablename;
                                    }

                                    string fieldName = attachmentTemplate["NamaReport"].ToString();

                                    //Insert Table Email Attachment based on setting tipe 5
                                    string StrSQLAtttachment = $@"INSERT INTO EmailTemplateSchedulerDetailAttachment (FK_EmailTEmplateSchedulerDetail_ID, FK_EmailAttachmentType_ID, NamaReport, IsiFile, FileName) 
                                                                Select EmailTemplateSchedulerDetail.PK_EmailTemplateSchedulerDetail_ID, 5, '{fieldName}' as NamaReport, x.{fieldName}, x.{fieldName}Name
                                                                From EmailTemplateSchedulerDetail INNER Join
                                                                EmailTemplateScheduler On EmailTemplateSchedulerDetail.FK_EmailTEmplateScheduler_ID = EmailTemplateScheduler.PK_EmailTemplateScheduler_ID
                                                                JOIN {strtablename} X ON EmailTemplateSchedulerDetail.UnikFieldTablePrimary = X.{item1["FieldUnikPrimaryTable"]}
                                                                Where EmailTemplateScheduler.PK_EmailTemplateScheduler_ID = {item["PK_EmailTemplateScheduler_ID"].ToString()}
                                                                AND X.{item1["FieldUnikPrimaryTable"]} = EmailTemplateSchedulerDetail.UnikFieldTablePrimary";

                                    NawaDAO.ExecuteNonQuery(StrSQLAtttachment);
                                }
                            }

                            else
                            {
                                //Insert Table Email Attachment based on setting tipe 1 (fix file)dan tipe 2 (ssrs) dan 4,
                                string StrSQLAtttachment = "INSERT INTO [dbo].[EmailTemplateSchedulerDetailAttachment] ([FK_EmailTEmplateSchedulerDetail_ID], [FK_EmailAttachmentType_ID], [NamaReport], [ParameterReport], [IsiFile], [FileName], EmailRenderAsName, FileExtension, [MimeType]) " + Environment.NewLine +
                                            "Select EmailTemplateSchedulerDetail.PK_EmailTemplateSchedulerDetail_ID, EmailTemplateAttachment.FK_EmailAttachmentType_ID, EmailTemplateAttachment.NamaReport, EmailTemplateAttachment.ParameterReport, EmailTemplateAttachment.IsiFile, EmailTemplateAttachment.NamaFile, EmailRenderAs.EmailRenderAsName, EmailRenderAs.FileExtension, EmailRenderAs.MimeType  " + Environment.NewLine +
                                            "From EmailTemplateSchedulerDetail INNER Join " + Environment.NewLine +
                                            "EmailTemplateScheduler On EmailTemplateSchedulerDetail.FK_EmailTEmplateScheduler_ID = EmailTemplateScheduler.PK_EmailTemplateScheduler_ID INNER Join " + Environment.NewLine +
                                            "EmailTemplateAttachment On EmailTemplateScheduler.PK_EmailTemplate_ID = EmailTemplateAttachment.FK_EmailTemplate_ID " + Environment.NewLine +
                                            "LEFT JOIN EmailRenderAs on EmailTemplateAttachment.FK_EmailRenderAs_Id=EmailRenderAs.PK_EmailRenderAs_ID " + Environment.NewLine +
                                            "Where EmailTemplateScheduler.PK_EmailTemplateScheduler_ID = {0} ";
                                StrSQLAtttachment = string.Format(StrSQLAtttachment, item["PK_EmailTemplateScheduler_ID"].ToString());

                                NawaDAO.ExecuteNonQuery(StrSQLAtttachment);
                            }
                        }
                        #endregion


                        //update status scheduler ready to kirim
                        sqlParameters = new SqlParameter[]
                        {
                            new SqlParameter("@PK_EmailTemplateScheduler_ID", item["PK_EmailTemplateScheduler_ID"])
                        };
                        NawaDAO.ExecuteNonQuery("exec usp_updateEmailStatusScheduler @PK_EmailTemplateScheduler_ID", sqlParameters);
                    }
                    catch (Exception ex)
                    {
                        mylog.LogError("An error has been occurred on RunEODProcess, Run EOD Email Template ", ex);
                        throw;
                    }
                }

                //Update Generate Email Attachment Status
                NawaDAO.ExecuteNonQuery("EXEC usp_UpdateEmailGenerateAttachmentStatus");

                //Generate Email Attachment From SSRS
                string StrSql = "";
                //StrSql = "SELECT FK_EmailTEmplateSchedulerDetail_ID, FK_EmailAttachmentType_ID,   " & vbCrLf _
                //        & " NamaReport, ParameterReport, IsiFile, [FileName], EmailRenderAsName, FileExtension, MimeType " & vbCrLf _
                //        & " FROM EmailTemplateSchedulerDetailAttachment " & vbCrLf _
                //        & " WHERE        (EmailTemplateSchedulerDetailAttachment.FK_EmailGenerateAttachmentStatus_ID = 2)"
                StrSql = "SELECT a.PK_EmailTemplateSchedulerDetailAttachment_Id, FK_EmailTEmplateSchedulerDetail_ID, FK_EmailAttachmentType_ID,   " + Environment.NewLine
                                + " NamaReport, ParameterReport, IsiFile, [FileName], EmailRenderAsName, FileExtension, MimeType " + Environment.NewLine
                                + " FROM EmailTemplateSchedulerDetailAttachment a " + Environment.NewLine
                                + " LEFT join EmailTemplateSchedulerDetail b" + Environment.NewLine
                                + " ON a.FK_EmailTEmplateSchedulerDetail_ID = b.PK_EmailTEmplateSchedulerDetail_ID" + Environment.NewLine
                                + " WHERE        (a.FK_EmailGenerateAttachmentStatus_ID = 2)" + Environment.NewLine
                                + " AND FK_EmailStatus_ID NOT IN ('4','5') AND FK_EmailAttachmentType_ID IN (2,4) ";

                DataTable DtNeedGenerateEmailAttachment = NawaDAO.ExecuteTable(StrSql);
                foreach (DataRow DrRow in DtNeedGenerateEmailAttachment.Rows)
                {
                    int FK_EmailTEmplateSchedulerDetail_ID = Convert.ToInt32(DrRow["FK_EmailTEmplateSchedulerDetail_ID"]);
                    string NamaReport = DrRow["NamaReport"].ToString();
                    string StrReportParameter = DrRow["ParameterReport"].ToString();
                    string StrRenderAs = DrRow["EmailRenderAsName"].ToString();
                    string strFileExtention = DrRow["FileExtension"].ToString();
                    int FK_EmailAttachmentType_ID = Convert.ToInt32(DrRow["FK_EmailAttachmentType_ID"]);
                    long PK_EmailTemplateSchedulerDetailAttachment_Id = Convert.ToInt64(DrRow["PK_EmailTemplateSchedulerDetailAttachment_Id"]);
                    if (FK_EmailAttachmentType_ID == 2)
                    {
                        ReportingServiceAttachment.GenerateEmailAttachmentFromSSRS(FK_EmailTEmplateSchedulerDetail_ID, strFileExtention, NamaReport, StrReportParameter, StrRenderAs, options);
                    }
                    else
                    {
                        ReportDesignerDevExpress.GenerateEmailAttachmentFromReportDesingerDevExpress(PK_EmailTemplateSchedulerDetailAttachment_Id, FK_EmailTEmplateSchedulerDetail_ID, strFileExtention, NamaReport, StrReportParameter, StrRenderAs);
                    }
                }

                //send email
                int intretrycounttosend = Convert.ToInt32(SystemParameterBLL.GetSystemParameter(2012));

                //Dim dtEmail As DataTable = SQLHelper.ExecuteTable(SQLHelper.strConnectionString, CommandType.Text, "SELECT * FROM EmailTemplateScheduler a INNER JOIN EmailTemplateSchedulerDetail b ON a.PK_EmailTemplateScheduler_ID=b.FK_EmailTEmplateScheduler_ID WHERE (b.FK_EmailStatus_ID=2 OR b.FK_EmailStatus_ID=3 or b.FK_EmailStatus_ID=5 )  and b.retrycount<" & intretrycounttosend, Nothing)

                DataTable dtEmail = NawaDAO.ExecuteTable(@"SELECT b.PK_EmailTemplateSchedulerDetail_ID, b.EmailID,
                                                                  b.EmailTo, b.EmailCC, b.EmailBCC, b.EmailSubject, b.EmailBody,
                                                                  b.AttachmentPassword,
                                                                  ISNULL(c.FK_EmailTemplateImportance_ID, 2) FK_EmailTemplateImportance_ID, ISNULL(c.FK_EmailTemplateSensitivity_ID, 1) FK_EmailTemplateSensitivity_ID,
                                                                  ISNULL(c.DeliveryReceipt, 0) DeliveryReceipt, ISNULL(c.ReadReceipt, 0) ReadReceipt
                                                           FROM EmailTemplateScheduler a
                                                                INNER JOIN EmailTemplateSchedulerDetail b
                                                                ON a.PK_EmailTemplateScheduler_ID=b.FK_EmailTEmplateScheduler_ID
                                                                INNER JOIN EmailTemplate c
                                                                ON a.PK_EmailTemplate_ID = c.PK_EmailTemplate_ID
                                                           WHERE (b.FK_EmailStatus_ID=2 OR b.FK_EmailStatus_ID=3 or b.FK_EmailStatus_ID=5 )
                                                                 and b.retrycount<" + intretrycounttosend);

                string strsender = SystemParameterBLL.GetSystemParameter(2005);

                foreach (DataRow item in dtEmail.Rows)
                {
                    try
                    {
                        //update status 3(inprogress)
                        sqlParameters = new SqlParameter[]
                        {
                            new SqlParameter("@PK_EmailTemplateSchedulerDetail_ID", item["PK_EmailTemplateSchedulerDetail_ID"]),
                            new SqlParameter("@FkStatusemailid", 3),
                            new SqlParameter("@errorMessage", "")
                        };
                        NawaDAO.ExecuteNonQuery("exec usp_updateEmailStatusSchedulerProcess @PK_EmailTemplateSchedulerDetail_ID,@FkStatusemailid,@errorMessage", sqlParameters);

                        Dictionary<string, string> Header = new Dictionary<string, string>();
                        Header.Add("EmailID", Convert.ToString(item["EmailID"]));

                        //item("PK_EmailTemplateSchedulerDetail_ID")
                        //Add Attachment
                        DataTable DtEmailAttachment = NawaDAO.ExecuteTable("SELECT FK_EmailTEmplateSchedulerDetail_ID, IsiFile, FileName, fk_emailattachmenttype_id FROM EmailTemplateSchedulerDetailAttachment WHERE FK_EmailTEmplateSchedulerDetail_ID=" + item["PK_EmailTemplateSchedulerDetail_ID"]);
                        Dictionary<string, System.IO.Stream> DicAttachment = new Dictionary<string, System.IO.Stream>();
                        System.IO.Stream objstream = new System.IO.MemoryStream();
                        long CounterKb = 0;
                        string fileName = "";
                        List<InMemoryFile> files = new List<InMemoryFile>();
                        string firstFileName = "";
                        foreach (DataRow DrEmailAttachment in DtEmailAttachment.Rows)
                        {
                            byte[] FileAttachmentEmailRaw;
                            if (DrEmailAttachment["fk_emailattachmenttype_id"] is 5)
                            {
                                if (DrEmailAttachment["IsiFile"] is not DBNull)
                                {
                                    FileAttachmentEmailRaw = (byte[])DrEmailAttachment["IsiFile"];
                                    if (FileAttachmentEmailRaw != null)
                                    {
                                        System.IO.Stream MsDataToAttach = new System.IO.MemoryStream(FileAttachmentEmailRaw);
                                        string StrFileName = DrEmailAttachment["FileName"].ToString();
                                        DicAttachment.Add(StrFileName, MsDataToAttach);
                                        objstream = DicAttachment.ElementAt(0).Value;
                                        CounterKb = objstream.Length / 1024;
                                        string[] arrayName = DicAttachment.ElementAt(0).Key.Split('.');
                                        fileName = arrayName[0] == null ? DrEmailAttachment["FileName"].ToString() : arrayName[0];
                                    }
                                }
                            }
                            else
                            {
                                FileAttachmentEmailRaw = (byte[])DrEmailAttachment["IsiFile"];
                                if (FileAttachmentEmailRaw != null)
                                {
                                    string StrFileName = DrEmailAttachment["FileName"].ToString();
                                    if (String.IsNullOrEmpty(Convert.ToString(item["AttachmentPassword"])) || Convert.ToString(item["AttachmentPassword"]).ToLower() == "@password")
                                    {
                                        System.IO.Stream MsDataToAttach = new System.IO.MemoryStream(FileAttachmentEmailRaw);
                                        DicAttachment.Add(StrFileName, MsDataToAttach);
                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(firstFileName)) firstFileName = StrFileName;
                                        InMemoryFile file = new InMemoryFile() { FileName = StrFileName, Content = FileAttachmentEmailRaw };
                                        files.Add(file);
                                    }
                                }
                            }

                        }

                        if (files.Count > 0)
                        {
                            Stream zipFile = GetZipArchive(files, Convert.ToString(item["AttachmentPassword"]));
                            DicAttachment.Add(firstFileName, zipFile);
                        }

                        if (!String.IsNullOrEmpty(Convert.ToString(item["AttachmentPassword"])))
                        {
                            string password = Convert.ToString(item["AttachmentPassword"]);
                            string passwordSalt = Guid.NewGuid().ToString();

                            if (password.ToLower() == "@password")
                            {
                                password = "";
                                passwordSalt = "";
                            }
                            else
                            {
                                password = NawaEncryption.Common.EncryptRijndael(password, passwordSalt);
                            }

                            sqlParameters = new SqlParameter[]
                            {
                                new SqlParameter("@PK_EmailTemplateSchedulerDetail_ID", item["PK_EmailTemplateSchedulerDetail_ID"]),
                                new SqlParameter("@Password", password),
                                new SqlParameter("@PasswordSalt", passwordSalt)
                            };
                            NawaDAO.ExecuteNonQuery("UPDATE EmailTemplateSchedulerDetail SET AttachmentPassword = @Password, AttachmentPasswordSalt = @PasswordSalt WHERE PK_EmailTemplateSchedulerDetail_ID = @PK_EmailTemplateSchedulerDetail_ID", sqlParameters);
                        }

                        SendEmail(strsender, Header, Convert.ToString(item["EmailTo"]), Convert.ToString(item["EmailCC"]), Convert.ToString(item["EmailBCC"]), Convert.ToString(item["EmailSubject"]), Convert.ToString(item["EmailBody"]), DicAttachment, Convert.ToInt32(item["FK_EmailTemplateSensitivity_ID"]), Convert.ToInt32(item["FK_EmailTemplateImportance_ID"]), Convert.ToBoolean(item["DeliveryReceipt"]), Convert.ToBoolean(item["ReadReceipt"]));

                        //update status 4(done / sucesssend)
                        sqlParameters = new SqlParameter[]
                        {
                            new SqlParameter("@PK_EmailTemplateSchedulerDetail_ID", item["PK_EmailTemplateSchedulerDetail_ID"]),
                            new SqlParameter("@FkStatusemailid", 4),
                            new SqlParameter("@errorMessage", "")
                        };

                        NawaDAO.ExecuteNonQuery("exec usp_updateEmailStatusSchedulerProcess @PK_EmailTemplateSchedulerDetail_ID,@FkStatusemailid,@errorMessage", sqlParameters);
                    }
                    catch (Exception ex)
                    {
                        //update status 5(failtosend)
                        sqlParameters = new SqlParameter[]
                        {
                            new SqlParameter("@PK_EmailTemplateSchedulerDetail_ID", item["PK_EmailTemplateSchedulerDetail_ID"]),
                            new SqlParameter("@errorMessage", ex.Message),
                        };
                        NawaDAO.ExecuteNonQuery("exec usp_updateLogErrorEmail @PK_EmailTemplateSchedulerDetail_ID,@errorMessage", sqlParameters);

                        throw;
                    }
                }
            }
            catch (Exception ex)
            {
                mylog.LogError("An Error has been occurred on WorkerEmail: ", ex);
            }
        }

        public static Stream GetZipArchive(List<InMemoryFile> files, string password)
        {
            var archiveStream = new MemoryStream();

            var zipStream = new ZipOutputStream(archiveStream);
            zipStream.SetLevel(9);

            if (!string.IsNullOrEmpty(password))
            {
                zipStream.Password = password;
            }

            foreach (var file in files)
            {
                var newEntry = new ZipEntry(file.FileName) { DateTime = DateTime.Now };
                zipStream.PutNextEntry(newEntry);

                zipStream.Write(file.Content, 0, file.Content.Length);
                zipStream.CloseEntry();
            }

            archiveStream.Position = 0;
            return archiveStream;
        }

        public class InMemoryFile
        {
            public string FileName { get; set; }
            public byte[] Content { get; set; }
        }

        private bool SendEmail(string sender, Dictionary<string, string> strHeaderLIst, string StrRecipientTo, string StrRecipientCC, string StrRecipientBCC, string StrSubject, string strbody, Dictionary<string, System.IO.Stream> DicAttachment, int sensitivity, int importance, bool flagDelivery, bool flagRead)
        {
            EMail oEmail;

            try
            {
                oEmail = new EMail();
                oEmail.Sender = sender;
                oEmail.HeaderList = strHeaderLIst;
                oEmail.Recipient = StrRecipientTo;
                oEmail.RecipientCC = StrRecipientCC;
                oEmail.RecipientBCC = StrRecipientBCC;

                oEmail.Subject = StrSubject;
                oEmail.Body = strbody.Replace(Environment.NewLine, "<br>");
                if (oEmail.SendEmail(DicAttachment, sensitivity, importance, flagDelivery, flagRead))
                {
                    return true;
                }

                return false;
            }
            catch
            {
                throw;
            }
        }
    }
}
