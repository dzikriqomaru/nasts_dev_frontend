﻿using Microsoft.Data.SqlClient;
using System;
using System.Data;
using System.Data.Common;
using System.Linq;

namespace NawaDataService
{
    public class SQLHelper : IDBHelper
    {
        private string connectionString { get; set; }

        public SQLHelper(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public DbConnection CreateConnection()
        {
            try
            {
                SqlConnection connection = new SqlConnection(this.connectionString);
                connection.Open();

                return connection;
            }
            catch
            {
                throw;
            }
        }

        public int ExecuteNonQuery(string cmdText, IDataParameter[] cmdParams, int commandTimeout, NawaConnection connection, CommandType commandType)
        {
            try
            {
                int result = -1;

                SqlConnection conn;

                if (connection == null) conn = (SqlConnection)CreateConnection();
                else conn = (SqlConnection)connection.conn;

                using (SqlCommand comm = new SqlCommand(cmdText, conn))
                {
                    if (connection != null && connection.trans != null) comm.Transaction = (SqlTransaction)connection.trans;
                    comm.CommandType = commandType;
                    if(cmdParams != null) comm.Parameters.AddRange(cmdParams);

                    result = comm.ExecuteNonQuery();

                    if (cmdParams != null) comm.Parameters.Clear();
                }

                if (connection == null)
                {
                    conn.Close();
                    conn.Dispose();
                }

                return result;
            }
            catch(Exception ex)
            {
                throw;
            }
        }

        public DataRow ExecuteRow(string cmdText, IDataParameter[] cmdParams, int commandTimeout, NawaConnection connection, CommandType commandType)
        {
            try
            {
                DataRow result = null;

                SqlConnection conn;

                if (connection == null) conn = (SqlConnection)CreateConnection();
                else conn = (SqlConnection)connection.conn;

                using (SqlCommand comm = new SqlCommand(cmdText, conn))
                {
                    if (connection != null && connection.trans != null) comm.Transaction = (SqlTransaction)connection.trans;
                    comm.CommandType = commandType;
                    if (cmdParams != null) comm.Parameters.AddRange(cmdParams);

                    using (SqlDataAdapter da = new SqlDataAdapter(comm))
                    {
                        DataTable dt = new DataTable();
                        da.Fill(dt);
                        if (dt.Rows.Count > 0)
                        {
                            result = dt.Rows[0];
                        }
                    }

                    if (cmdParams != null) comm.Parameters.Clear();
                }

                if (connection == null)
                {
                    conn.Close();
                    conn.Dispose();
                }

                return result;
            }
            catch
            {
                throw;
            }
        }

        public object ExecuteScalar(string cmdText, IDataParameter[] cmdParams, int commandTimeout, NawaConnection connection, CommandType commandType)
        {
            try
            {
                object result = null;

                SqlConnection conn;

                if (connection == null) conn = (SqlConnection)CreateConnection();
                else conn = (SqlConnection)connection.conn;

                using (SqlCommand comm = new SqlCommand(cmdText, conn))
                {
                    if (connection != null && connection.trans != null) comm.Transaction = (SqlTransaction)connection.trans;
                    comm.CommandType = commandType;
                    if (cmdParams != null) comm.Parameters.AddRange(cmdParams);

                    result = comm.ExecuteScalar();

                    if (cmdParams != null) comm.Parameters.Clear();
                }

                if (connection == null)
                {
                    conn.Close();
                    conn.Dispose();
                }

                return result;
            }
            catch
            {
                throw;
            }
        }

        public DataTable ExecuteTable(string cmdText, IDataParameter[] cmdParams, int commandTimeout, NawaConnection connection, CommandType commandType)
        {
            try
            {
                DataTable result = null;

                SqlConnection conn;

                if (connection == null) conn = (SqlConnection)CreateConnection();
                else conn = (SqlConnection)connection.conn;

                using (SqlCommand comm = new SqlCommand(cmdText, conn))
                {
                    if (connection != null && connection.trans != null) comm.Transaction = (SqlTransaction)connection.trans;
                    comm.CommandType = commandType;
                    if (cmdParams != null) comm.Parameters.AddRange(cmdParams);

                    using (SqlDataAdapter da = new SqlDataAdapter(comm))
                    {
                        result = new DataTable();
                        da.Fill(result);
                    }

                    if (cmdParams != null) comm.Parameters.Clear();
                }

                if (connection == null)
                {
                    conn.Close();
                    conn.Dispose();
                }

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable ExecuteTable(string table, string fields, string filters, string sorts, int pageIndex, int pageSize, out int count, IDataParameter[] cmdParams, int commandTimeout, NawaConnection connection)
        {
            try
            {
                DataTable result = null;

                SqlConnection conn;

                if (connection == null) conn = (SqlConnection)CreateConnection();
                else conn = (SqlConnection)connection.conn;

                string cmdText;

                cmdText = "SELECT COUNT(13) TotalCount" + Environment.NewLine +
                          "FROM " + table + Environment.NewLine +
                          "WHERE " + (String.IsNullOrEmpty(filters) ? "1=1" : filters) + Environment.NewLine; 

                using (SqlCommand comm = new SqlCommand(cmdText, conn))
                {
                    if (connection != null && connection.trans != null) comm.Transaction = (SqlTransaction)connection.trans;
                    comm.CommandType = CommandType.Text;
                    if (cmdParams != null) comm.Parameters.AddRange(cmdParams);

                    count = Convert.ToInt32(comm.ExecuteScalar());

                    if (cmdParams != null) comm.Parameters.Clear();
                }

                cmdText = "SELECT " + fields + Environment.NewLine +
                          "FROM " + table + Environment.NewLine +
                          "WHERE " + (String.IsNullOrEmpty(filters) ? "1=1" : filters) + Environment.NewLine +
                          "ORDER BY " + (String.IsNullOrEmpty(sorts) ? "1" : sorts) + Environment.NewLine +
                          "OFFSET (" + ((pageIndex - 1) * pageSize).ToString() + ") ROWS" + Environment.NewLine +
                          "FETCH NEXT " + pageSize.ToString() + " ROWS ONLY";

                using (SqlCommand comm = new SqlCommand(cmdText, conn))
                {
                    comm.CommandType = CommandType.Text;
                    if (cmdParams != null) comm.Parameters.AddRange(cmdParams);

                    using (SqlDataAdapter da = new SqlDataAdapter(comm))
                    {
                        result = new DataTable();
                        da.Fill(result);
                    }

                    if (cmdParams != null) comm.Parameters.Clear();
                }

                if (connection == null)
                {
                    conn.Close();
                    conn.Dispose();
                }

                return result;
            }
            catch
            {
                throw;
            }
        }

        public bool InsertBulkCopy(DataTable data, string destTable, NawaConnection connection)
        {
            try
            {
                using (SqlBulkCopy objBulkCopy = new SqlBulkCopy((SqlConnection)connection.conn, SqlBulkCopyOptions.Default, (SqlTransaction)connection.trans))
                {
                    objBulkCopy.ColumnMappings.Clear();
                    objBulkCopy.BatchSize = data.Rows.Count;
                    objBulkCopy.DestinationTableName = destTable;
                    objBulkCopy.BulkCopyTimeout = 3600;
                    data.Columns.Cast<DataColumn>().Select(col => col.ColumnName).ToList()
                        .ForEach(x => objBulkCopy.ColumnMappings.Add(x, x));

                    objBulkCopy.WriteToServer(data);
                    objBulkCopy.Close();
                }

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
