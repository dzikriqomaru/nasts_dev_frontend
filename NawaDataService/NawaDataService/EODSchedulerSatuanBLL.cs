﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic;
using Microsoft.Data.SqlClient;
using System.Threading;
using Microsoft.SqlServer.Dts.Runtime;
using System.Xml;
using NawaDataDAL;
using Google.Apis.Auth.OAuth2;
using System.Net.Http;
using Newtonsoft.Json;

namespace NawaDataService
{
    public class EODSchedulerSatuanBLL
    {
        enum EODTaskDetailType
        {
            SSIS = 1,
            StoreProcedure,
            SSISinSQLagent,
            API,
            DataFusion
        }

        enum MsEODStatus
        {
            OnQueue = 1,
            Inprogress,
            Sucess,
            ErrorProcess,
            TryCanceling,
            Canceled
        }

        private NawaConsoleLog mylog = new NawaConsoleLog();
        public static string DTExec = "";

        public EODSchedulerSatuanBLL()
        {
        }

        public void run(string strFilter)
        {
            while (true)
            {
                try
                {
                    Thread.Sleep(1000);

                    List<EODSchedulerLog> objListEODSchedulerLog;
                    if (strFilter == "0")
                        objListEODSchedulerLog = NawaDAO.ConvertDataTable<EODSchedulerLog>(NawaDAO.ExecuteTable("SELECT TOP 1 xx.* FROM dbo.EODSchedulerLog xx LEFT JOIN ( SELECT a.FK_Process_ID FROM dbo.NDSMapProcessQueue a INNER JOIN dbo.NDSProcessQueue b ON a.FK_NDSQueue_ID = b.PK_NDSProcess_ID WHERE a.Active = 1 AND b.Active = 1 ) yy ON xx.FK_EODSchedulerID = yy.FK_Process_ID WHERE yy.FK_Process_ID IS NULL AND xx.FK_MsEODStatus_ID=1 AND DATEDIFF(SECOND,xx.ProcessDate,CAST(GETDATE() AS DATETIME2 (3)))>0 ORDER BY xx.PK_EODSchedulerLog_ID;"));
                    else
                        objListEODSchedulerLog = NawaDAO.ConvertDataTable<EODSchedulerLog>(NawaDAO.ExecuteTable("SELECT xx.* FROM dbo.EODSchedulerLog xx INNER JOIN ( SELECT a.FK_Process_ID FROM dbo.NDSMapProcessQueue a INNER JOIN dbo.NDSProcessQueue b ON a.FK_NDSQueue_ID = b.PK_NDSProcess_ID WHERE a.Active = 1 AND b.QueueNo = " + strFilter + " AND b.Active = 1 ) yy ON xx.FK_EODSchedulerID = yy.FK_Process_ID WHERE xx.FK_MsEODStatus_ID=1 AND DATEDIFF(SECOND,xx.ProcessDate,CAST(GETDATE() AS DATETIME2 (3)))>0 ORDER BY xx.PK_EODSchedulerLog_ID;"));

                    foreach (EODSchedulerLog item in objListEODSchedulerLog)
                    {
                        long lngPK_EODSchedulerLog_ID = item.PK_EODSchedulerLog_ID;
                        string strkodecabang = item.KodeCabang;

                        // nawadataetlconfiguration untuk datadate,PK_EODTaskDetailLog_ID,Report_Date,Kode_Cabang mau ngak di pake lagi,jadi langsung di set dari ssisnya
                        // ExecUpdateParamModifiedDate(item.DataDate.GetValueOrDefault(DateTime.Now), strFilter)
                        // If Not strkodecabang Is Nothing Then
                        // ExecUpdateKodeCabang(strkodecabang, strFilter)
                        // End If
                        RunEODScheduler(lngPK_EODSchedulerLog_ID, strFilter);
                    }
                }
                catch (Exception ex)
                {
                    mylog.LogError("An Error has been occurred on RunEODScheduler: ", ex);
                }
            }
        }

        public void EODSchedulerStart(long lngPK_EODSchedulerLog_ID)
        {
            SqlParameter[] sqlParams = new SqlParameter[]
            {
                new SqlParameter("@PK_EODSchedulerLog_ID", System.Data.SqlDbType.BigInt) {Value = lngPK_EODSchedulerLog_ID},
                new SqlParameter("@StartDate", System.Data.SqlDbType.DateTime) {Value = DateTime.Now},
                new SqlParameter("@FK_MsEODStatus_ID", System.Data.SqlDbType.Int) {Value = MsEODStatus.Inprogress}
            };
            NawaDAO.ExecuteNonQuery("UPDATE EODSchedulerLog SET StartDate = @StartDate, FK_MsEODStatus_ID = @FK_MsEODStatus_ID WHERE PK_EODSchedulerLog_ID = @PK_EODSchedulerLog_ID", sqlParams);
        }

        public void EODTaskStart(long lngPK_EODTaskLog_ID)
        {
            SqlParameter[] sqlParams = new SqlParameter[]
            {
                new SqlParameter("@PK_EODTaskLog_ID", System.Data.SqlDbType.BigInt) {Value = lngPK_EODTaskLog_ID},
                new SqlParameter("@StartDate", System.Data.SqlDbType.DateTime) {Value = DateTime.Now},
                new SqlParameter("@FK_MsEODStatus_ID", System.Data.SqlDbType.Int) {Value = MsEODStatus.Inprogress}
            };
            NawaDAO.ExecuteNonQuery("UPDATE EODTaskLog SET StartDate = @StartDate, FK_MsEODStatus_ID = @FK_MsEODStatus_ID WHERE PK_EODTaskLog_ID = @PK_EODTaskLog_ID", sqlParams);
        }

        public void EODTaskDetailStart(long lngPK_EODTaskDetailLog_ID)
        {
            SqlParameter[] sqlParams = new SqlParameter[]
            {
                new SqlParameter("@PK_EODTaskDetailLog_ID", System.Data.SqlDbType.BigInt) {Value = lngPK_EODTaskDetailLog_ID},
                new SqlParameter("@StartDate", System.Data.SqlDbType.DateTime) {Value = DateTime.Now},
                new SqlParameter("@FK_MsEODStatus_ID", System.Data.SqlDbType.Int) {Value = MsEODStatus.Inprogress}
            };
            NawaDAO.ExecuteNonQuery("UPDATE EODTaskDetailLog SET StartDate = @StartDate, FK_MsEODStatus_ID = @FK_MsEODStatus_ID WHERE PK_EODTaskDetailLog_ID = @PK_EODTaskDetailLog_ID", sqlParams);
        }
        public void DeleteSSISConfiguration(ref Package pkg, Dictionary<string, string> objparam)
        {
            if (pkg.Configurations.Count > 0)
            {
                foreach (Configuration item in pkg.Configurations)
                {
                    if (item.ConfigurationType == DTSConfigurationType.SqlServer)
                    {
                        string[] arrsetting = item.ConfigurationString.Split(";");
                        if (arrsetting.Length >= 2)
                        {
                            string strconfigurationfilter = arrsetting[2];
                            strconfigurationfilter = strconfigurationfilter.Replace("\"", "");

                            bool boolResult = objparam.Keys.Any(x => x.Equals(strconfigurationfilter, StringComparison.OrdinalIgnoreCase));
                            if (boolResult)
                                pkg.Configurations.Remove(item);
                        }
                    }
                }
            }
        }
        public void SetSSISVariableValue(ref Package pkg, Dictionary<string, string> objparam)
        {
            foreach (Variable item in pkg.Variables)
            {
                string strToFind = item.Name.ToLower();

                KeyValuePair<string, string> objFindResult = objparam.FirstOrDefault(x => x.Key.ToLower() == strToFind);
                int i = 0;
                if (!string.IsNullOrEmpty(objFindResult.Key))
                    item.Value = objFindResult.Value;
            }
        }
        private bool ExecuteDTSX(string strFullFileDirectory, string strPackageName, Dictionary<string, string> objparam)
        {
            string dtexec = DTExec;

            string strPkgLocation = strFullFileDirectory;

            string dtsxContent = File.ReadAllText(strPkgLocation);

            List<string> connectionList = new List<string>();

            List<string> variableList = new List<string>();

            using (TextReader sr = new StringReader(dtsxContent))
            {
                XmlReaderSettings settings = new XmlReaderSettings();
                settings.IgnoreWhitespace = true;

                using (XmlReader reader = XmlReader.Create(sr, settings))
                {
                    while (reader.Read())
                    {
                        switch (reader.NodeType)
                        {
                            case XmlNodeType.Element:
                                if (reader.Name == "DTS:ConnectionManager")
                                {
                                    string connectionName = reader.GetAttribute("DTS:ObjectName");

                                    if (!string.IsNullOrEmpty(connectionName))
                                    {
                                        connectionList.Add(connectionName);
                                    }
                                }
                                else if (reader.Name == "DTS:Variable")
                                {
                                    string variableName = reader.GetAttribute("DTS:ObjectName");

                                    if (!string.IsNullOrEmpty(variableName))
                                    {
                                        variableList.Add(variableName);
                                    }
                                }
                                break;
                            default:
                                break;
                        }
                    }
                }
            }

            string arguments = "/FILE \"" + strPkgLocation + "\"";

            List<NawadataConnectionString> objConnectionString = NawaDAO.ConvertDataTable<NawadataConnectionString>(NawaDAO.ExecuteTable("SELECT * FROM NawadataConnectionString WHERE Active = 1"));

            foreach (string connectionName in connectionList)
            {
                NawadataConnectionString objFind = objConnectionString.Find(x => x.ConnecitonName.ToLower() == connectionName.ToLower());

                if(objFind != null)
                {
                    string connectionstring = GetConnectionStringByPk(objFind.PK_NawaDataConnectionString_ID);
                    arguments += " /CONN " + connectionName + ";\"" + connectionstring + "\"";
                }
            }

            foreach (KeyValuePair<string, string> dictParam in objparam)
            {
                if (!string.IsNullOrEmpty(dictParam.Value))
                {
                    string varFind = variableList.Find(x => x == dictParam.Key);

                    if (varFind != null)
                    {
                        arguments += " /SET \\Package.Variables[User::" + dictParam.Key + "].Value;\"" + dictParam.Value + "\"";
                    }
                }
            }

            using (Process p = new Process())
            {
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.RedirectStandardOutput = true;
                p.StartInfo.FileName = dtexec;
                p.StartInfo.Arguments = arguments;
                p.Start();
                string output = p.StandardOutput.ReadToEnd();
                p.WaitForExit();

                using (StringReader sr = new StringReader(output))
                {
                    string line = sr.ReadLine();
                    while (line != null)
                    {
                        if (line.Contains("DTExec: The package execution returned"))
                        {
                            if (line.Contains("DTSER_SUCCESS"))
                            {
                                return true;
                            }
                            else
                            {
                                throw new Exception(output);
                            }
                        }

                        line = sr.ReadLine();
                    }
                }
            }

            return false;

            //Package pkg = new Package();
            //Application app = new Application();
            //DTSExecResult pkgResults = new DTSExecResult();

            //try
            //{
            //    strPkgLocation = strFullFileDirectory;

            //    pkg = app.LoadPackage(strPkgLocation, null/* TODO Change to default(_) if this is not a reference type */);

            //    List<NawadataConnectionString> objConnectionString = NawaDAO.ConvertDataTable<NawadataConnectionString>(NawaDAO.ExecuteTable("SELECT * FROM NawadataConnectionString WHERE Active = 1"));

            //    if (pkg != null)
            //    {
            //        for (int i = 0; i <= pkg.Connections.Count - 1; i++)
            //        {
            //            NawadataConnectionString objFind = objConnectionString.Find(x => x.ConnecitonName.ToLower() == pkg.Connections[i].Name.ToLower());
            //            if (objFind != null)
            //            {
            //                string connectionstring = GetConnectionStringByPk(objFind.PK_NawaDataConnectionString_ID);
            //                pkg.Connections[i].ConnectionString = connectionstring;
            //            }
            //        }
            //    }

            //    DeleteSSISConfiguration(ref pkg, objparam);
            //    SetSSISVariableValue(ref pkg, objparam);

            //    // Execute
            //    pkgResults = pkg.Execute(null/* TODO Change to default(_) if this is not a reference type */, null/* TODO Change to default(_) if this is not a reference type */, null/* TODO Change to default(_) if this is not a reference type */, null/* TODO Change to default(_) if this is not a reference type */, null/* TODO Change to default(_) if this is not a reference type */);

            //    switch (pkgResults)
            //    {
            //        case DTSExecResult.Canceled:
            //            throw new Exception((Convert.ToString("Dtsx Package ") + strPackageName) + " is canceled");
            //        case DTSExecResult.Success:
            //            return true;
            //        case DTSExecResult.Failure:
            //            string strDTSXErrorMessage = "";
            //            for (int i = 0; i <= pkg.Errors.Count - 1; i++)
            //                strDTSXErrorMessage += pkg.Errors[i].Description + ", ";
            //            throw new Exception(Convert.ToString((Convert.ToString("Error running package ") + strPackageName) + ": ") + strDTSXErrorMessage);
            //        case DTSExecResult.Completion:
            //            return true;
            //    }
            //    return false;
            //}
            //catch (Exception ex)
            //{
            //    throw;
            //}
            //finally
            //{
            //    pkg = null/* TODO Change to default(_) if this is not a reference type */;
            //    //pkgResults = null/* TODO Change to default(_) if this is not a reference type */;
            //    app = null/* TODO Change to default(_) if this is not a reference type */;
            //}
        }

        public bool RunEodTaskDetailSSIS(EODTaskDetail objEODTaskDetail, EODTaskDetailLog objEODTaskLogDetail, Dictionary<string, string> objparam)
        {
            if (objEODTaskDetail.SSISFIle.Length > 0)
            {
                string strFullFileDirectory = "";
                strFullFileDirectory = @"EODSSIS\" + objEODTaskDetail.SSISName;

                if (!Directory.Exists("EODSSIS"))
                {
                    Directory.CreateDirectory("EODSSIS");
                }

                System.IO.File.WriteAllBytes(strFullFileDirectory, objEODTaskDetail.SSISFIle);

                return (ExecuteDTSX(strFullFileDirectory, objEODTaskDetail.SSISName, objparam));
            }
            else
                return false;
        }

        public bool RunEodTaskDetailStoreProcedure(EODTaskDetail objEODTaskDetail, EODTaskDetailLog objEODTaskLogDetail, EODSchedulerLog objSchedulerLog)
        {
            if (objEODTaskDetail.StoreProcedureName.Length > 0)
            {
                string paramStr = " @PK_EODTaskDetailLog_ID";
                SqlParameter objParamTaskID = new SqlParameter("@PK_EODTaskDetailLog_ID", objEODTaskLogDetail.PK_EODTaskDetailLog_ID);
                SqlParameter objParamDate = new SqlParameter("@DataDate", objSchedulerLog.DataDate);
                SqlParameter objParamBranch = new SqlParameter("@KodeCabang", objSchedulerLog.KodeCabang);

                if (objEODTaskDetail.IsUseParameterProcessDate)
                    paramStr += ", @DataDate";
                if (objEODTaskDetail.IsUseParameterBranch)
                    paramStr += ", @KodeCabang";

                if ((!objEODTaskDetail.IsUseParameterProcessDate) && (objEODTaskDetail.IsUseParameterBranch))
                    NawaDAO.ExecuteNonQuery("EXEC " + objEODTaskDetail.StoreProcedureName + paramStr, new SqlParameter[] { objParamTaskID, objParamBranch });

                if ((!objEODTaskDetail.IsUseParameterProcessDate) && (!objEODTaskDetail.IsUseParameterBranch))
                    NawaDAO.ExecuteNonQuery("EXEC " + objEODTaskDetail.StoreProcedureName + paramStr, new SqlParameter[] { objParamTaskID });
                if ((objEODTaskDetail.IsUseParameterProcessDate) && (!objEODTaskDetail.IsUseParameterBranch))
                    NawaDAO.ExecuteNonQuery("EXEC " + objEODTaskDetail.StoreProcedureName + paramStr, new SqlParameter[] { objParamTaskID, objParamDate });

                if ((objEODTaskDetail.IsUseParameterProcessDate) && (objEODTaskDetail.IsUseParameterBranch))
                    NawaDAO.ExecuteNonQuery("EXEC " + objEODTaskDetail.StoreProcedureName + paramStr, new SqlParameter[] { objParamTaskID, objParamDate, objParamBranch });

                return true;
            }

            return false;
        }

        public bool RunEodTaskDetailDataFusion(EODTaskDetail objEODTaskDetail, EODTaskDetailLog objEODTaskLogDetail, EODSchedulerLog objSchedulerLog)
        {
            if (objEODTaskDetail.SSISName != "")
            {
                string base64AuthFile = SystemParameterBLL.GetSystemParameter(-13);

                if (base64AuthFile != "")
                {
                    string onlyBase64 = base64AuthFile.Substring(base64AuthFile.LastIndexOf(',') + 1);
                    var authByte = Convert.FromBase64String(onlyBase64);
                    var authStream = new MemoryStream(authByte);

                    // Google Auth
                    string scope = "https://www.googleapis.com/auth/cloud-platform";
                    GoogleCredential credential = GoogleCredential.FromStream(authStream);
                    string access_token = credential.CreateScoped(scope).UnderlyingCredential.GetAccessTokenForRequestAsync().Result;

                    // Initialization
                    string instanceName = objEODTaskDetail.SSISName;
                    string pipelineName = objEODTaskDetail.StoreProcedureName;
                    string region = objEODTaskDetail.Region;

                    // Get API Endpoint
                    string instanceDetail = getInstanceDetail(access_token, instanceName, region);
                    dynamic detail = JsonConvert.DeserializeObject(instanceDetail);
                    string apiEndpoint = detail.apiEndpoint + "/";

                    // Parsing Argument
                    String str = objEODTaskDetail.Argument;
                    Dictionary<string, string> parameter = new Dictionary<string, string>();

                    if (str != "" && str != null)
                    {
                        String spearator = "&";
                        String[] strlist = str.Split(spearator);

                        foreach (String s in strlist)
                        {
                            String[] value = s.Split("=");
                            parameter.Add(value[0], value[1]);
                        }
                    }
                    

                    if (!parameter.ContainsKey("DataDate"))
                    {
                        parameter.Add("DataDate", Convert.ToDateTime(objSchedulerLog.DataDate).ToString("yyyy-MM-dd"));
                    }

                    if (!parameter.ContainsKey("Report_Date"))
                    {
                        parameter.Add("Report_Date", Convert.ToDateTime(objSchedulerLog.DataDate).ToString("yyyy-MM-dd"));
                    }

                    if (!parameter.ContainsKey("Kode_Cabang"))
                    {
                        parameter.Add("Kode_Cabang", objSchedulerLog.KodeCabang == null ? "" : objSchedulerLog.KodeCabang);
                    }

                    string json = JsonConvert.SerializeObject(parameter);
                    var contentData = new StringContent(json, Encoding.UTF8, "application/json");

                    // RUN PIPELINE
                    HttpClient client = new HttpClient();
                    client.BaseAddress = new Uri(apiEndpoint);
                    client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue($"Bearer", $"{access_token}");
                    HttpResponseMessage res = client.PostAsync("v3/namespaces/default/apps/" + pipelineName + "/workflows/DataPipelineWorkflow/start", contentData).Result;

                    if (res.IsSuccessStatusCode)
                    {
                        return true;
                    }
                    else
                    {
                        throw new Exception(res.Content.ReadAsStringAsync().Result);
                    }
                } else
                {
                    throw new Exception("Invalid google cloud auth file");
                }
            }

            return false;
        }

        public static string getInstanceDetail(string access_token, string instanceName, string region)
        {
            try
            {
                string projectID = SystemParameterBLL.GetSystemParameter(-14);

                if (projectID != "")
                {

                    string URL = "https://datafusion.googleapis.com/v1/";

                    HttpClient client = new HttpClient();
                    client.BaseAddress = new Uri(URL);
                    client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue($"Bearer", $"{access_token}");
                    HttpResponseMessage response = client.GetAsync("projects/" + projectID + "/locations/" + region + "/instances/" + instanceName).Result;

                    string data = response.Content.ReadAsStringAsync().Result;

                    return data;
                }
                else
                {
                    throw new Exception("Invalid google cloud project id or project region");
                }
            }
            catch
            {
                throw;
            }
        }

        public void EODTaskDetailEnd(long lngPK_EODTaskDetailLog_ID, int intEODStatus, string strErrorMessage)
        {
            SqlParameter[] sqlParams = new SqlParameter[]
            {
                new SqlParameter("@PK_EODTaskDetailLog_ID", System.Data.SqlDbType.BigInt) {Value = lngPK_EODTaskDetailLog_ID},
                new SqlParameter("@Enddate", System.Data.SqlDbType.DateTime) {Value = DateTime.Now},
                new SqlParameter("@FK_MsEODStatus_ID", System.Data.SqlDbType.Int) {Value = intEODStatus},
                new SqlParameter("@ErrorMessage", System.Data.SqlDbType.VarChar) {Value = strErrorMessage}
            };
            NawaDAO.ExecuteNonQuery("UPDATE EODTaskDetailLog SET Enddate = @Enddate, FK_MsEODStatus_ID = @FK_MsEODStatus_ID, ErrorMessage = @ErrorMessage WHERE PK_EODTaskDetailLog_ID = @PK_EODTaskDetailLog_ID", sqlParams);
        }

        public long GetLastStatusEODSchedulerLog(long lngPK_EODSchedulerLog_ID)
        {
            return Convert.ToInt64(NawaDAO.ExecuteScalar("SELECT el.FK_MsEODStatus_ID FROM EODSchedulerLog AS el WHERE el.PK_EODSchedulerLog_ID=" + lngPK_EODSchedulerLog_ID));
        }

        public bool RunEODTaskDetailSSISSQLAgent(EODTaskDetail objEODTaskDetail, EODTaskDetailLog objEODTaskLogDetail)
        {
            if (objEODTaskDetail.SSISFIle.Length > 0)
            {
                string strFullFileDirectory = "";
                strFullFileDirectory = @"EODSSIS\" + objEODTaskDetail.SSISName;

                if (!Directory.Exists("EODSSIS"))
                {
                    Directory.CreateDirectory("EODSSIS");
                }

                System.IO.File.WriteAllBytes(strFullFileDirectory, objEODTaskDetail.SSISFIle);

                return (ExecuteSSISBySQLAgent(strFullFileDirectory, System.IO.Path.GetFileNameWithoutExtension(objEODTaskDetail.SSISName)));
            }
            else
                return false;
        }

        private bool ExecuteSSISBySQLAgent(string strFullFileDirectory, string strPackageName)
        {
            string strPkgLocation;
            Package pkg = new Package();
            Application app = new Application();

            try
            {
                strPkgLocation = strFullFileDirectory;
                pkg = app.LoadPackage(strPkgLocation, null/* TODO Change to default(_) if this is not a reference type */);

                string strListConnectionString = "";
                string strCommand = @"/FILE ""\""" + strFullFileDirectory + @"\"" ";

                    List<NawadataConnectionString> objConnectionString = NawaDAO.ConvertDataTable<NawadataConnectionString>(NawaDAO.ExecuteTable("SELECT * FROM NawadataConnectionString WHERE Active = 1"));

                    if (pkg != null)
                    {
                        for (int i = 0; i <= pkg.Connections.Count - 1; i++)
                        {
                            NawadataConnectionString objFind = objConnectionString.Find(x => x.ConnecitonName.ToLower() == pkg.Connections[i].Name.ToLower());
                            if (objFind != null)
                            {
                                string connectionstring = GetConnectionStringByPk(objFind.PK_NawaDataConnectionString_ID);
                                strListConnectionString += " /CONNECTION " + pkg.Connections[i].Name + @";""\"" " + connectionstring + @"\"" ";
                            }
                        }
                    }

                strCommand += strListConnectionString + " /CHECKPOINTING OFF /REPORTING E";

                string sql;
                sql = " " + Constants.vbCrLf
        + "DECLARE @nawajobname VARCHAR(200)= " + Constants.vbCrLf
        + "DECLARE @nawaloginname VARCHAR(200)= " + Constants.vbCrLf
        + "DECLARE @nawacommand VARCHAR(8000)= " + Constants.vbCrLf
        + " " + Constants.vbCrLf
        + " " + Constants.vbCrLf
        + "/****** Object:  Job [test12]    Script Date: 9/2/2017 4:08:18 PM ******/ " + Constants.vbCrLf
        + "EXEC msdb.dbo.sp_delete_job @job_name=@nawajobname , @delete_unused_schedule=1 " + Constants.vbCrLf
        + " " + Constants.vbCrLf
        + " " + Constants.vbCrLf
        + "/****** Object:  Job [test12]    Script Date: 9/2/2017 4:08:18 PM ******/ " + Constants.vbCrLf
        + "BEGIN TRANSACTION " + Constants.vbCrLf
        + "DECLARE @ReturnCode INT " + Constants.vbCrLf
        + "SELECT @ReturnCode = 0 " + Constants.vbCrLf
        + "/****** Object:  JobCategory [[Uncategorized (Local)]]    Script Date: 9/2/2017 4:08:19 PM ******/ " + Constants.vbCrLf
        + "IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1) " + Constants.vbCrLf
        + "BEGIN " + Constants.vbCrLf
        + "EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]' " + Constants.vbCrLf
        + "IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback " + Constants.vbCrLf
        + " " + Constants.vbCrLf
        + "END " + Constants.vbCrLf
        + " " + Constants.vbCrLf
        + "DECLARE @jobId BINARY(16) " + Constants.vbCrLf
        + "EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=@nawajobname,  " + Constants.vbCrLf
        + "		@enabled=1,  " + Constants.vbCrLf
        + "		@notify_level_eventlog=0,  " + Constants.vbCrLf
        + "		@notify_level_email=0,  " + Constants.vbCrLf
        + "		@notify_level_netsend=0,  " + Constants.vbCrLf
        + "		@notify_level_page=0,  " + Constants.vbCrLf
        + "		@delete_level=0,  " + Constants.vbCrLf
        + "		@description=N'No description available.',  " + Constants.vbCrLf
        + "		@category_name=N'[Uncategorized (Local)]',  " + Constants.vbCrLf
        + "		@owner_login_name=@nawaloginname , @job_id = @jobId OUTPUT " + Constants.vbCrLf
        + "IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback " + Constants.vbCrLf
        + "/****** Object:  Step [test12]    Script Date: 9/2/2017 4:08:19 PM ******/ " + Constants.vbCrLf
        + "EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=@nawajobname,  " + Constants.vbCrLf
        + "		@step_id=1,  " + Constants.vbCrLf
        + "		@cmdexec_success_code=0,  " + Constants.vbCrLf
        + "		@on_success_action=1,  " + Constants.vbCrLf
        + "		@on_success_step_id=0,  " + Constants.vbCrLf
        + "		@on_fail_action=2,  " + Constants.vbCrLf
        + "		@on_fail_step_id=0,  " + Constants.vbCrLf
        + "		@retry_attempts=0,  " + Constants.vbCrLf
        + "		@retry_interval=0,  " + Constants.vbCrLf
        + "		@os_run_priority=0, @subsystem=N'SSIS',  " + Constants.vbCrLf
        + "		@command=@nawacommand,  " + Constants.vbCrLf
        + "		@database_name=N'master',  " + Constants.vbCrLf
        + "		@flags=0 " + Constants.vbCrLf
        + "IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback " + Constants.vbCrLf
        + "EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1 " + Constants.vbCrLf
        + "IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback " + Constants.vbCrLf
        + "EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)' " + Constants.vbCrLf
        + "IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback " + Constants.vbCrLf
        + "COMMIT TRANSACTION " + Constants.vbCrLf
        + "GOTO EndSave " + Constants.vbCrLf
        + "QuitWithRollback: " + Constants.vbCrLf
        + "    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION " + Constants.vbCrLf
        + "EndSave: " + Constants.vbCrLf
        + " " + Constants.vbCrLf
        + "EXEC msdb.dbo.sp_start_job @nawajobname " + Constants.vbCrLf
        + "";

                // Execute

                return (false);
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                pkg = null/* TODO Change to default(_) if this is not a reference type */;

                app = null/* TODO Change to default(_) if this is not a reference type */;
            }
        }

        public string GetConnectionStringByPk(int pk)
        {
            SqlParameter[] objparam = new SqlParameter[1];
            objparam[0] = new SqlParameter();
            objparam[0].ParameterName = "@pkid";
            objparam[0].Value = pk;

            return Convert.ToString(NawaDAO.ExecuteScalar("Nawa_usp_GetConnectionStringByPK", objparam, null, System.Data.CommandType.StoredProcedure));
        }

        public void RunEODScheduler(long lngPK_EODSchedulerLog_ID, string filterantrian)
        {
            bool IsprocessValid = true;
            string strErrorMessage = "";
            long lngTaskDetailProcess;

            try
            {
                EODSchedulerLog objEodSchedulerLog = NawaDAO.ConvertDataTable<EODSchedulerLog>(NawaDAO.ExecuteRow("SELECT TOP 1 * FROM EODSchedulerLog WHERE PK_EODSchedulerLog_ID = " + lngPK_EODSchedulerLog_ID));

                Dictionary<string, string> objParamSSIS = new Dictionary<string, string>();

                if (objParamSSIS.ContainsKey("DataDate"))
                    objParamSSIS.Remove("DataDate");
                objParamSSIS.Add("DataDate", Convert.ToDateTime(objEodSchedulerLog.DataDate).ToString("yyyy-MM-dd"));

                if (objParamSSIS.ContainsKey("Report_Date"))
                    objParamSSIS.Remove("Report_Date");
                objParamSSIS.Add("Report_Date", Convert.ToDateTime(objEodSchedulerLog.DataDate).ToString("yyyy-MM-dd"));

                if (objParamSSIS.ContainsKey("Kode_Cabang"))
                    objParamSSIS.Remove("Kode_Cabang");
                objParamSSIS.Add("Kode_Cabang", objEodSchedulerLog.KodeCabang == null ? "" : objEodSchedulerLog.KodeCabang);


                long EODSchedulerID = 0;
                if (objEodSchedulerLog != null)
                    EODSchedulerID = objEodSchedulerLog.FK_EODSchedulerID;

                // udpate status jadi inprogress (2)
                EODSchedulerStart(lngPK_EODSchedulerLog_ID);

                foreach (EODTaskLog itemTaskLog in NawaDAO.ConvertDataTable<EODTaskLog>(NawaDAO.ExecuteTable("SELECT * FROM EODTaskLog WHERE EODSchedulerLogID = " + lngPK_EODSchedulerLog_ID)))
                {
                    // update status task log  jadi inprogress (2)
                    EODTaskStart(itemTaskLog.PK_EODTaskLog_ID);

                    foreach (EODTaskDetailLog itemTaskDetailLog in NawaDAO.ConvertDataTable<EODTaskDetailLog>(NawaDAO.ExecuteTable("SELECT * FROM EODTaskDetailLog WHERE EODTaskLogID = " + itemTaskLog.PK_EODTaskLog_ID)))
                    {
                        if (IsprocessValid)
                        {
                            try
                            {
                                // update status jadi inprocess (2)
                                EODTaskDetailStart(itemTaskDetailLog.PK_EODTaskDetailLog_ID);

                                EODTaskDetail objEOdTaskDetail = NawaDAO.ConvertDataTable<EODTaskDetail>(NawaDAO.ExecuteRow("SELECT * FROM EODTaskDetail WHERE PK_EODTaskDetail_ID = " + itemTaskDetailLog.FK_EODTAskDetail_ID));

                                if (objEOdTaskDetail != null)
                                {
                                    if (objEOdTaskDetail.FK_EODTaskDetailType_ID == (int)EODTaskDetailType.SSIS)
                                    {
                                        DateTime datdate = objEodSchedulerLog.DataDate;
                                        NawaDAO.ExecuteNonQuery("update SystemParameter set SettingValue='" + datdate.ToString("yyyyMMdd") + "' where PK_SystemParameter_ID=3003");
                                        NawaDAO.ExecuteNonQuery("update SystemParameter set SettingValue='" + datdate.ToString("yyyyMMdd") + "' where SettingName='FTP_DATAPERIODE'");

                                        // objDb.Database.ExecuteSqlCommand("exec Usp_Console_UpdateParamEODTaskDetailLogIDQueue @PK_EODTaskDetailLog_ID,@queue",
                                        // New Data.SqlClient.SqlParameter("@PK_EODTaskDetailLog_ID", itemTaskDetailLog.PK_EODTaskDetailLog_ID), New Data.SqlClient.SqlParameter("@queue", filterantrian))

                                        if (objParamSSIS.ContainsKey("PK_EODTaskDetailLog_ID"))
                                            objParamSSIS.Remove("PK_EODTaskDetailLog_ID");
                                        objParamSSIS.Add("PK_EODTaskDetailLog_ID", Convert.ToString(itemTaskDetailLog.PK_EODTaskDetailLog_ID));


                                        if (RunEodTaskDetailSSIS(objEOdTaskDetail, itemTaskDetailLog, objParamSSIS))
                                            EODTaskDetailEnd(itemTaskDetailLog.PK_EODTaskDetailLog_ID, Convert.ToInt32(MsEODStatus.Sucess), "");
                                        else
                                            EODTaskDetailEnd(itemTaskDetailLog.PK_EODTaskDetailLog_ID, Convert.ToInt32(MsEODStatus.ErrorProcess), "Error Running SSIS " + objEOdTaskDetail.SSISName);

                                        if (GetLastStatusEODSchedulerLog(lngPK_EODSchedulerLog_ID) == (long)MsEODStatus.TryCanceling)
                                            IsprocessValid = false;
                                    }
                                    else if (objEOdTaskDetail.FK_EODTaskDetailType_ID == (int)EODTaskDetailType.StoreProcedure)
                                    {
                                        RunEodTaskDetailStoreProcedure(objEOdTaskDetail, itemTaskDetailLog, objEodSchedulerLog);

                                        EODTaskDetailEnd(itemTaskDetailLog.PK_EODTaskDetailLog_ID, Convert.ToInt32(MsEODStatus.Sucess), "");
                                        if (GetLastStatusEODSchedulerLog(lngPK_EODSchedulerLog_ID) == (long)MsEODStatus.TryCanceling)
                                            IsprocessValid = false;
                                    }
                                    else if (objEOdTaskDetail.FK_EODTaskDetailType_ID == (int)EODTaskDetailType.SSISinSQLagent)
                                    {
                                        NawaDAO.ExecuteNonQuery("exec Usp_Console_UpdateParamEODTaskDetailLogID @PK_EODTaskDetailLog_ID", new SqlParameter[] { new SqlParameter("@PK_EODTaskDetailLog_ID", itemTaskDetailLog.PK_EODTaskDetailLog_ID) });

                                        if (RunEODTaskDetailSSISSQLAgent(objEOdTaskDetail, itemTaskDetailLog))
                                            EODTaskDetailEnd(itemTaskDetailLog.PK_EODTaskDetailLog_ID, Convert.ToInt32(MsEODStatus.Sucess), "");
                                        else
                                            EODTaskDetailEnd(itemTaskDetailLog.PK_EODTaskDetailLog_ID, Convert.ToInt32(MsEODStatus.ErrorProcess), "Error Running SSIS " + objEOdTaskDetail.SSISName);
                                        if (GetLastStatusEODSchedulerLog(lngPK_EODSchedulerLog_ID) == (long)MsEODStatus.TryCanceling)
                                            IsprocessValid = false;
                                    }
                                    else if (objEOdTaskDetail.FK_EODTaskDetailType_ID == (int)EODTaskDetailType.API)
                                    {
                                        string strpathddl = "";
                                        string objparam = SystemParameterBLL.GetSystemParameter(4004);
                                        if (!string.IsNullOrEmpty(objparam))
                                            strpathddl = objparam;

                                        try
                                        {
                                            ExecuteAPI(strpathddl, objEOdTaskDetail.SSISName, objEOdTaskDetail.StoreProcedureName, itemTaskDetailLog.PK_EODTaskDetailLog_ID);
                                            EODTaskDetailEnd(itemTaskDetailLog.PK_EODTaskDetailLog_ID, Convert.ToInt32(MsEODStatus.Sucess), "");
                                            if (GetLastStatusEODSchedulerLog(lngPK_EODSchedulerLog_ID) == (long)MsEODStatus.TryCanceling)
                                                IsprocessValid = false;
                                        }
                                        catch (Exception ex)
                                        {
                                            // Dim errormessage As String = ex.Message + " " + ex.StackTrace
                                            string errormessage = "";
                                            if (ex.InnerException != null)
                                                errormessage += " " + ex.InnerException.Message + " " + ex.InnerException.StackTrace;
                                            IsprocessValid = false;
                                            EODTaskDetailEnd(itemTaskDetailLog.PK_EODTaskDetailLog_ID, Convert.ToInt32(MsEODStatus.ErrorProcess), "Error Running API " + objEOdTaskDetail.SSISName + " " + errormessage);
                                        }
                                    }
                                    else if (objEOdTaskDetail.FK_EODTaskDetailType_ID == (int)EODTaskDetailType.DataFusion)
                                    {
                                        // Ini ganti jadi data fusion execute
                                        RunEodTaskDetailDataFusion(objEOdTaskDetail, itemTaskDetailLog, objEodSchedulerLog);
                                        EODTaskDetailEnd(itemTaskDetailLog.PK_EODTaskDetailLog_ID, Convert.ToInt32(MsEODStatus.Sucess), "");
                                        if (GetLastStatusEODSchedulerLog(lngPK_EODSchedulerLog_ID) == (long)MsEODStatus.TryCanceling)
                                            IsprocessValid = false;
                                    }
                                }
                                else
                                    EODTaskDetailEnd(itemTaskDetailLog.PK_EODTaskDetailLog_ID, Convert.ToInt32(MsEODStatus.ErrorProcess), "There is no task detail " + objEOdTaskDetail.StoreProcedureName);
                            }
                            catch (Exception ex)
                            {
                                strErrorMessage = ex.Message;
                                EODTaskDetailEnd(itemTaskDetailLog.PK_EODTaskDetailLog_ID, Convert.ToInt32(MsEODStatus.ErrorProcess), strErrorMessage);
                                IsprocessValid = false;
                                lngTaskDetailProcess = itemTaskDetailLog.PK_EODTaskDetailLog_ID;

                                EODTaskDetail objEOdTaskDetail = NawaDAO.ConvertDataTable<EODTaskDetail>(NawaDAO.ExecuteRow("SELECT * FROM EODTaskDetail WHERE PK_EODTaskDetail_ID = " + itemTaskDetailLog.FK_EODTAskDetail_ID));

                                if (objEOdTaskDetail.FK_EODTaskDetailType_ID == (int)EODTaskDetailType.SSIS)
                                    mylog.LogError("An error has been occurred on RunEODProcess, package " + objEOdTaskDetail.SSISName, ex);
                                else
                                    mylog.LogError("An error has been occurred on RunEODProcess, Store Procedure " + objEOdTaskDetail.StoreProcedureName, ex);
                            }
                        }
                        else
                        {

                            // //Kalo sebelumnya ada process package yg gagal,
                            // //Maka gak usah dijalankan, sisanya lgsg dianggap gagal aja

                            // //Update Status Log Task Detail jadi In Progress
                            EODTaskDetailStart(itemTaskDetailLog.PK_EODTaskDetailLog_ID);

                            // //Update Status Log Package jadi Error
                            EODTaskDetailEnd(itemTaskDetailLog.PK_EODTaskDetailLog_ID, Convert.ToInt32(MsEODStatus.Canceled), "Task Detail is canceled");
                        }
                    }

                    if (IsprocessValid)
                        EODTaskEnd(itemTaskLog.PK_EODTaskLog_ID, Convert.ToInt32(MsEODStatus.Sucess), "");
                    else
                        EODTaskEnd(itemTaskLog.PK_EODTaskLog_ID, Convert.ToInt32(MsEODStatus.Canceled), "Task Detail is canceled");
                }

                if (IsprocessValid)
                    EodSchedulerEnd(lngPK_EODSchedulerLog_ID, Convert.ToInt32(MsEODStatus.Sucess), "");
                else
                    EodSchedulerEnd(lngPK_EODSchedulerLog_ID, Convert.ToInt32(MsEODStatus.Canceled), "Task Detail is canceled");
            }
            catch (Exception ex)
            {
                mylog.LogError("An error has been occurred on RunEODProcess, Run EOD Scheduler", ex);
            }
        }

        public void ExecuteAPI(string PathDLL, string Classname, string methodName, long PK_EODTaskDetail_ID)
        {
            Assembly assembly__1 = Assembly.LoadFile(PathDLL);
            Type magicType = assembly__1.GetType(Classname);

            ConstructorInfo magicConstructor = magicType.GetConstructor(Type.EmptyTypes);
            object magicClassObject = magicConstructor.Invoke(new object[] { });

            // Get the ItsMagic method and invoke with a parameter value of 100

            MethodInfo magicMethod = magicType.GetMethod(methodName);
            System.Reflection.ParameterInfo[] objListSystemParameter = magicMethod.GetParameters();
            if (objListSystemParameter.Length > 0)
            {
                object[] objparam = new object[] { PK_EODTaskDetail_ID };
                object magicValue = magicMethod.Invoke(magicClassObject, objparam);
                magicValue = null;
                objparam = null;
            }
            else
            {
                object magicValue = magicMethod.Invoke(magicClassObject, null);
                magicValue = null;
            }

            magicMethod = null;
            magicClassObject = null;
            magicType = null;
            assembly__1 = null;
        }

        public void EodSchedulerEnd(long lngPK_EODSchedulerLog_ID, int intEODStatus, string strErrorMessage)
        {
            SqlParameter[] sqlParams = new SqlParameter[]
            {
                new SqlParameter("@PK_EODSchedulerLog_ID", System.Data.SqlDbType.BigInt) {Value = lngPK_EODSchedulerLog_ID},
                new SqlParameter("@Enddate", System.Data.SqlDbType.DateTime) {Value = DateTime.Now},
                new SqlParameter("@FK_MsEODStatus_ID", System.Data.SqlDbType.Int) {Value = intEODStatus},
                new SqlParameter("@ErrorMessage", System.Data.SqlDbType.VarChar) {Value = strErrorMessage}
            };
            NawaDAO.ExecuteNonQuery("UPDATE EODSchedulerLog SET Enddate = @Enddate, FK_MsEODStatus_ID = @FK_MsEODStatus_ID, ErrorMessage = @ErrorMessage WHERE PK_EODSchedulerLog_ID = @PK_EODSchedulerLog_ID", sqlParams);
        }

        public void EODTaskEnd(long lngPK_EODTaskLog_ID, int intEODStatus, string strErrorMessage)
        {
            SqlParameter[] sqlParams = new SqlParameter[]
            {
                new SqlParameter("@PK_EODTaskLog_ID", System.Data.SqlDbType.BigInt) {Value = lngPK_EODTaskLog_ID},
                new SqlParameter("@Enddate", System.Data.SqlDbType.DateTime) {Value = DateTime.Now},
                new SqlParameter("@FK_MsEODStatus_ID", System.Data.SqlDbType.Int) {Value = intEODStatus},
                new SqlParameter("@ErrorMessage", System.Data.SqlDbType.VarChar) {Value = strErrorMessage}
            };
            NawaDAO.ExecuteNonQuery("UPDATE EODTaskLog SET Enddate = @Enddate, FK_MsEODStatus_ID = @FK_MsEODStatus_ID, ErrorMessage = @ErrorMessage WHERE PK_EODTaskLog_ID = @PK_EODTaskLog_ID", sqlParams);
        }

        public void ExecUpdateKodeCabang(string strkodecabang, string strqueue)
        {
            SqlParameter objkodecabang = new SqlParameter("@kodecabang", strkodecabang);
            SqlParameter objparamqueue = new SqlParameter("@queue", strqueue);

            NawaDAO.ExecuteNonQuery("exec Usp_Console_UpdateParamBranchCodequeue @kodecabang,@queue", new SqlParameter[] { objkodecabang, objparamqueue });
        }

        public void ExecUpdateParamModifiedDate(DateTime processdate, string strqueue)
        {
            SqlParameter objparamdate = new SqlParameter("@ProcessDate", processdate.ToString("yyyy-MM-dd"));
            SqlParameter objparamqueue = new SqlParameter("@queue", strqueue);

            NawaDAO.ExecuteNonQuery("exec Usp_Console_UpdateParamModifiedDateQueue @ProcessDate,@queue", new SqlParameter[] { objparamdate, objparamqueue });
        }
    }
}