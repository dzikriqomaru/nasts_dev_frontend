﻿using DevExpress.DataAccess.ConnectionParameters;
using DevExpress.DataAccess.Sql;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports;
using DevExpress.XtraReports.Parameters;
using DevExpress.XtraReports.UI;
using NawaDataDAL;
using System;
using System.Collections.Generic;
using System.Data;
using Microsoft.Data.SqlClient;
using System.IO;
using System.Linq;
using Microsoft.Extensions.Logging;
using System.Drawing.Imaging;

namespace NawaDataService
{
    public class ReportDesignerDevExpress
    {

        private readonly ILogger<WorkerEmail> _logger;
        private static NawaConsoleLog mylog = new NawaConsoleLog();

        public static bool GenerateEmailAttachmentFromReportDesingerDevExpress(long PK_EmailTemplateSchedulerDetailAttachment_Id, int FK_EmailTEmplateSchedulerDetail_ID, string strFileExtention, string StrReportName, string strParameter, string StrFormat)
        {
            try
            {
                mylog.LogInfo("GenerateEmailAttachmentFromReportDesingerDevExpress constructor ");

                byte[] reportTemplate = null;

                SqlParameter[] SqlParameter = new SqlParameter[]
                      {
                              new SqlParameter("@displayname", StrReportName)
                      };

                reportTemplate = (byte[])NawaDAO.ExecuteScalar("SELECT LayoutData FROM ES_ReportLayout WHERE DisplayName = @displayname", SqlParameter);

                if (reportTemplate != null && reportTemplate.Length > 0)
                {

                    mylog.LogInfo("GenerateEmailAttachmentFromReportDesingerDevExpress report exist  ");

                    using (MemoryStream msReport = new MemoryStream(reportTemplate))
                    {
                        XtraReport report = XtraReport.FromStream(msReport, true);

                        //XtraReport reportInternal = report;

                        try
                        {
                            SqlDataSource[] sqlDataSources = DataSourceManager.GetDataSources<SqlDataSource>(report, true).ToArray();

                            foreach (var item in sqlDataSources)
                            {
                                mylog.LogInfo("GenerateEmailAttachmentFromReportDesingerDevExpress cek sqldatasources " + item.Name);

                            }

                            string queryRemoteConnection = "select servername, dbname, user AS username, password, connectionname, connectionType from remoteConnection where active = 1";
                            DataTable dtRemoteConnection = NawaDAO.ExecuteTable(queryRemoteConnection);
                            List<DataSourceHelper> dtSourceHelpers = new List<DataSourceHelper>();


                            mylog.LogInfo("GenerateEmailAttachmentFromReportDesingerDevExpress after  queryRemoteConnection ");

                            foreach (DataRow dr in dtRemoteConnection.Rows)
                            {

                                mylog.LogInfo("GenerateEmailAttachmentFromReportDesingerDevExpress connection exist  ");

                                string queryDecrypt = "EXEC usp_DecryptReportConnectionData @encryptedPass";

                                SqlParameter[] prms = new SqlParameter[]
                                 {
                                        new SqlParameter("@encryptedPass", SqlDbType.VarChar){ Value = Convert.ToString(dr["password"]) },
                                 };

                                string decryptedPassword = Convert.ToString(NawaDAO.ExecuteScalar(queryDecrypt, prms));


                                DataSourceHelper dtSource = new DataSourceHelper();
                                dtSource.servername = Convert.ToString(dr["servername"]);
                                dtSource.dbname = Convert.ToString(dr["dbname"]);
                                dtSource.username = Convert.ToString(dr["username"]);
                                dtSource.fk_connectiontype_id = Convert.ToInt64(dr["connectionType"]);
                                dtSource.connectionname = Convert.ToString(dr["connectionname"]);
                                dtSource.password = decryptedPassword;
                                dtSourceHelpers.Add(dtSource);
                            }



                            foreach (SqlDataSource sqlDataSource in sqlDataSources)
                            {


                                mylog.LogInfo("GenerateEmailAttachmentFromReportDesingerDevExpress data source exist  ");

                                //query = "SELECT * FROM remoteConnection WHERE dbName = '" + sqlDataSource.Name + "'";
                                //string query = "SELECT connectionname, fk_databasename_id, servername, port, username, password, database, bq_project_id, bq_keyfile, bq_serviceaccountemail, bq_datasetid FROM es_reportconnection where connectionname = '" + sqlDataSource.Name + "'";



                                //DataTable dt = NawaDAO.ExecuteTable(query);



                                //foreach (DataRow dr in dt.Rows)
                                foreach (DataSourceHelper dtSourceHelper in dtSourceHelpers)
                                {
                                    if (sqlDataSource.Name == dtSourceHelper.connectionname)
                                    {
                                        switch (dtSourceHelper.fk_connectiontype_id)
                                        {
                                            case 1: //PostgreSQL
                                                PostgreSqlConnectionParameters parametersPostgre = new PostgreSqlConnectionParameters(dtSourceHelper.servername, dtSourceHelper.dbname, dtSourceHelper.username, dtSourceHelper.password);
                                                sqlDataSource.ConnectionParameters = parametersPostgre;
                                                break;
                                            case 2: //SQLServer
                                                MsSqlConnectionParameters parametersSQL = new MsSqlConnectionParameters(dtSourceHelper.servername, dtSourceHelper.dbname, dtSourceHelper.username, dtSourceHelper.password, MsSqlAuthorizationType.SqlServer);
                                                sqlDataSource.ConnectionParameters = parametersSQL;
                                                break;
                                            case 3: //BigQuery
                                                BigQueryConnectionParameters parametersBQ = new BigQueryConnectionParameters(dtSourceHelper.servername, dtSourceHelper.dbname, dtSourceHelper.username, dtSourceHelper.password);
                                                sqlDataSource.ConnectionParameters = parametersBQ;
                                                break;
                                        }
                                    }
                                }
                                sqlDataSource.RebuildResultSchema();
                            }
                            string[] reportParameters = Convert.ToString(strParameter).Split("&");

                            foreach (string reportParameter in reportParameters)
                            {
                                string[] paramValues = reportParameter.Split("=");



                                foreach (Parameter reportParam in report.Parameters)
                                {

                                    mylog.LogInfo("GenerateEmailAttachmentFromReportDesingerDevExpress param " + reportParam.Name + " " + reportParam.Value);

                                    if (reportParam.Name == paramValues[0])
                                    {
                                        if (DateTime.TryParse(paramValues[1], out DateTime parameterPeriode))
                                        {
                                            //paramValues[1] = parameterPeriode.ToString("dd/MM/yyyy");
                                            report.Parameters[paramValues[0]].Value = parameterPeriode;
                                        }
                                        else
                                        {
                                            report.Parameters[paramValues[0]].Value = paramValues[1];
                                        }
                                    }
                                }
                            }

                            if (StrFormat == "PDF")
                            {
                                //donehendra:ganti method export pdf,xls dll
                                //reportInternal.ExportToPdf ()
                                using (MemoryStream ms = new MemoryStream())
                                {
                                    report.ExportToPdf(ms);

                                    mylog.LogInfo("GenerateEmailAttachmentFromReportDesingerDevExpress after  generate ");
                                    byte[] bytes = ms.ToArray();
                                    string strupdate = "update emailtemplateschedulerdetailattachment set isifile=@isifile,fk_emailgenerateattachmentstatus_id=2 where fk_emailtemplateschedulerdetail_id=@fk_emailtemplateschedulerdetail_id and PK_EmailTemplateSchedulerDetailAttachment_Id=@PK_EmailTemplateSchedulerDetailAttachment_Id";

                                    SqlParameter[] objsqlparam = new SqlParameter[]
                                    {
                                        new SqlParameter("@isifile", bytes),
                                        new SqlParameter("@fk_emailtemplateschedulerdetail_id", FK_EmailTEmplateSchedulerDetail_ID),
                                        new SqlParameter("@PK_EmailTemplateSchedulerDetailAttachment_Id", PK_EmailTemplateSchedulerDetailAttachment_Id)
                                    };
                                    NawaDAO.ExecuteNonQuery(strupdate, objsqlparam);
                                }
                            }
                            else if (StrFormat == "EXCEL")
                            {
                                using (MemoryStream ms = new MemoryStream())
                                {
                                    report.ExportToXlsx(ms);
                                    byte[] bytes = ms.ToArray();
                                    string strupdate = "update emailtemplateschedulerdetailattachment set isifile=@isifile,fk_emailgenerateattachmentstatus_id=2 where fk_emailtemplateschedulerdetail_id=@fk_emailtemplateschedulerdetail_id and PK_EmailTemplateSchedulerDetailAttachment_Id=@PK_EmailTemplateSchedulerDetailAttachment_Id";

                                    SqlParameter[] objsqlparam = new SqlParameter[]
                                    {
                                        new SqlParameter("@isifile", bytes),
                                        new SqlParameter("@fk_emailtemplateschedulerdetail_id", FK_EmailTEmplateSchedulerDetail_ID),
                                        new SqlParameter("@PK_EmailTemplateSchedulerDetailAttachment_Id", PK_EmailTemplateSchedulerDetailAttachment_Id)
                                    };
                                    NawaDAO.ExecuteNonQuery(strupdate, objsqlparam);
                                }
                            }
                            else if (StrFormat == "WORD")
                            {
                                using (MemoryStream ms = new MemoryStream())
                                {
                                    report.ExportToDocx(ms);
                                    byte[] bytes = ms.ToArray();
                                    string strupdate = "update emailtemplateschedulerdetailattachment set isifile=@isifile,fk_emailgenerateattachmentstatus_id=2 where fk_emailtemplateschedulerdetail_id=@fk_emailtemplateschedulerdetail_id and PK_EmailTemplateSchedulerDetailAttachment_Id=@PK_EmailTemplateSchedulerDetailAttachment_Id";

                                    SqlParameter[] objsqlparam = new SqlParameter[]
                                    {
                                        new SqlParameter("@isifile", bytes),
                                        new SqlParameter("@fk_emailtemplateschedulerdetail_id", FK_EmailTEmplateSchedulerDetail_ID),
                                        new SqlParameter("@PK_EmailTemplateSchedulerDetailAttachment_Id", PK_EmailTemplateSchedulerDetailAttachment_Id)
                                    };
                                    NawaDAO.ExecuteNonQuery(strupdate, objsqlparam);
                                }
                            }
                            else if (StrFormat == "CSV")
                            {
                                using (MemoryStream ms = new MemoryStream())
                                {
                                    report.ExportToCsv(ms);
                                    byte[] bytes = ms.ToArray();
                                    string strupdate = "update emailtemplateschedulerdetailattachment set isifile=@isifile,fk_emailgenerateattachmentstatus_id=2 where fk_emailtemplateschedulerdetail_id=@fk_emailtemplateschedulerdetail_id and PK_EmailTemplateSchedulerDetailAttachment_Id=@PK_EmailTemplateSchedulerDetailAttachment_Id";

                                    SqlParameter[] objsqlparam = new SqlParameter[]
                                    {
                                        new SqlParameter("@isifile", bytes),
                                        new SqlParameter("@fk_emailtemplateschedulerdetail_id", FK_EmailTEmplateSchedulerDetail_ID),
                                        new SqlParameter("@PK_EmailTemplateSchedulerDetailAttachment_Id", PK_EmailTemplateSchedulerDetailAttachment_Id)
                                    };
                                    NawaDAO.ExecuteNonQuery(strupdate, objsqlparam);
                                }
                            }
                            else if (StrFormat == "IMAGE")
                            {
                                using (MemoryStream ms = new MemoryStream())
                                {
                                    report.ExportToImage(ms, ImageFormat.Png);
                                    byte[] bytes = ms.ToArray();
                                    string strupdate = "update emailtemplateschedulerdetailattachment set isifile=@isifile,fk_emailgenerateattachmentstatus_id=2 where fk_emailtemplateschedulerdetail_id=@fk_emailtemplateschedulerdetail_id and PK_EmailTemplateSchedulerDetailAttachment_Id=@PK_EmailTemplateSchedulerDetailAttachment_Id";

                                    SqlParameter[] objsqlparam = new SqlParameter[]
                                    {
                                        new SqlParameter("@isifile", bytes),
                                        new SqlParameter("@fk_emailtemplateschedulerdetail_id", FK_EmailTEmplateSchedulerDetail_ID),
                                        new SqlParameter("@PK_EmailTemplateSchedulerDetailAttachment_Id", PK_EmailTemplateSchedulerDetailAttachment_Id)
                                    };
                                    NawaDAO.ExecuteNonQuery(strupdate, objsqlparam);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            SqlParameter[] prms = new SqlParameter[]
                            {
                                new SqlParameter("@ErrorMessage", ex.Message),
                                new SqlParameter("@PK_EmailTemplateSchedulerDetail_ID", FK_EmailTEmplateSchedulerDetail_ID)
                            };
                            string strerror = "UPDATE EmailTemplateSchedulerDetail SET FK_EmailStatus_ID = 5, ErrorMessage = @ErrorMessage  WHERE PK_EmailTemplateSchedulerDetail_ID=@PK_EmailTemplateSchedulerDetail_ID";
                            NawaDAO.ExecuteNonQuery(strerror, prms);
                            mylog.LogError("Error: GenerateEmailAttachmentFromReportDesingerDevExpress", ex);
                            throw;
                        }
                    }
                    //call reportdesingerdevexpress

                    //update emailtemplateschedulerdetailattachment
                }

                return true;
            }
            catch (Exception ex)
            {
                SqlParameter[] prms = new SqlParameter[]
                {
                    new SqlParameter("@ErrorMessage", ex.Message),
                    new SqlParameter("@PK_EmailTemplateSchedulerDetail_ID", FK_EmailTEmplateSchedulerDetail_ID)
                };
                string strerror = "UPDATE EmailTemplateSchedulerDetail SET FK_EmailStatus_ID = 5, ErrorMessage = @ErrorMessage WHERE PK_EmailTemplateSchedulerDetail_ID=@PK_EmailTemplateSchedulerDetail_ID";
                NawaDAO.ExecuteNonQuery(strerror, prms);
                mylog.LogError("Error: GenerateEmailAttachmentFromReportDesingerDevExpress", ex);
                throw;
            }
        }
        public class DataSourceHelper
        {
            public string servername { get; set; }
            public string dbname { get; set; }
            public string username { get; set; }
            public string password { get; set; }
            public string connectionname { get; set; }
            public long fk_connectiontype_id { get; set; }
        }
    }
}