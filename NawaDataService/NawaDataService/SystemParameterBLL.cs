﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Data.SqlClient;
using NawaDataDAL;

namespace NawaDataService
{
    public class SystemParameterBLL
    {
        public static string GetSystemParameter(int PK_SystemParameter_ID)
        {
            string query = "SELECT SettingValue, IsEncript, EncriptionKey FROM SystemParameter WHERE PK_SystemParameter_ID = @PK_SystemParameter_ID";
            SqlParameter[] sqlParams = new SqlParameter[]
            {
                new SqlParameter("@PK_SystemParameter_ID", System.Data.SqlDbType.Int) {Value = PK_SystemParameter_ID}
            };

            DataRow dr = NawaDAO.ExecuteRow(query, sqlParams);

            if(dr != null)
            {
                string settingValue = "";
                if (Convert.ToBoolean(dr["IsEncript"]))
                {
                    settingValue = Convert.ToString(dr["SettingValue"]);
                    string encryptionKey = Convert.ToString(dr["EncriptionKey"]);

                    settingValue = NawaEncryption.Common.DecryptRijndael(settingValue, encryptionKey);
                }
                else
                {
                    settingValue = Convert.ToString(dr["SettingValue"]);
                }

                return settingValue;
            }
            else
            {
                throw new Exception(string.Format("System Parameter {0} not found.", PK_SystemParameter_ID));
            }
        }
    }
}
