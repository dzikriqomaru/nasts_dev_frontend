﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NawaDataService
{
    public class EODSchedulerLog
    {
        public long PK_EODSchedulerLog_ID { get; set; }
        public long FK_EODSchedulerID { get; set; }
        public string ExecuteBy { get; set; }
        public DateTime ProcessDate { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime Enddate { get; set; }
        public string ErrorMessage { get; set; }
        public int FK_MsEODStatus_ID { get; set; }
        public DateTime DataDate { get; set; }
        public string KodeCabang { get; set; }
    }

}
