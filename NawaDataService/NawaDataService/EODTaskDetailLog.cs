﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NawaDataService
{
    public class EODTaskDetailLog
    {
        public long PK_EODTaskDetailLog_ID { get; set; }
        public long EODSchedulerLogID { get; set; }
        public long EODTaskLogID { get; set; }
        public long FK_EODTAskDetail_ID { get; set; }
        public string ExecuteBy { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime Enddate { get; set; }
        public string ErrorMessage { get; set; }
        public int FK_MsEODStatus_ID { get; set; }
        public string executionID { get; set; }
    }

}
