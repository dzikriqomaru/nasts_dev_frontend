﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NawaDataService
{
    public class WorkerOptions
    {
        public string IsTLS { get; set; }
        public string IsAutoRedirect { get; set; }
        public string IsCallBack { get; set; }
        public string IsCertiValidationCallBack { get; set; }
        public string crtfile { get; set; }
        public string pfxfile { get; set; }
        public string passphrase { get; set; }
    }
}
