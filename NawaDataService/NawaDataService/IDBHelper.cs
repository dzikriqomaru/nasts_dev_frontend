﻿using System.Data;
using System.Data.Common;

namespace NawaDataService
{
    public interface IDBHelper
    {
        public DbConnection CreateConnection();
        public int ExecuteNonQuery(string cmdText, IDataParameter[] cmdParams, int commandTimeout, NawaConnection connection, CommandType commandType);
        public DataTable ExecuteTable(string cmdText, IDataParameter[] cmdParams, int commandTimeout, NawaConnection connection, CommandType commandType);
        public DataTable ExecuteTable(string table, string fields, string filters, string sorts, int pageIndex, int pageSize, out int count, IDataParameter[] cmdParams, int commandTimeout, NawaConnection connection);
        public DataRow ExecuteRow(string cmdText, IDataParameter[] cmdParams, int commandTimeout, NawaConnection connection, CommandType commandType);
        public object ExecuteScalar(string cmdText, IDataParameter[] cmdParams, int commandTimeout, NawaConnection connection, CommandType commandType);
        public bool InsertBulkCopy(DataTable data, string destName, NawaConnection connection);
    }
}
