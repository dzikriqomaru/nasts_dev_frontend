﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.Common;

namespace NawaDataService
{
    public class NawaConnection
    {
        public DbConnection conn { get; set; }
        public DbTransaction trans { get; set; }

        public NawaConnection(DbConnection conn)
        {
            this.conn = conn;
            this.trans = null;
        }

        public void BeginTransaction()
        {
            if(this.conn != null && this.conn.State == System.Data.ConnectionState.Open)
            {
                this.trans = this.conn.BeginTransaction();
            }
        }

        public void Commit()
        {
            if (this.trans != null)
            {
                this.trans.Commit();
                this.trans.Dispose();
                this.trans = null;
            }
        }
        public void Rollback()
        {
            if (this.trans != null)
            {
                this.trans.Rollback();
                this.trans.Dispose();
                this.trans = null;
            }
        }

        public void Dispose()
        {
            if(this.trans != null)
            {
                this.trans.Rollback();
                this.trans.Dispose();
                this.trans = null;
            }

            if(this.conn.State == System.Data.ConnectionState.Open)
            {
                this.conn.Close();
            }

            this.conn.Dispose();
            this.conn = null;
        }
    }
}
