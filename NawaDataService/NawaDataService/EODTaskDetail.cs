﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NawaDataService
{
    public class EODTaskDetail
    {
        public long PK_EODTaskDetail_ID { get; set; }
        public long FK_EODTask_ID { get; set; }
        public int FK_EODTaskDetailType_ID { get; set; }
        public int OrderNo { get; set; }
        public string SSISName { get; set; }
        public byte[] SSISFIle { get; set; }
        public string StoreProcedureName { get; set; }
        public string Keterangan { get; set; }
        public bool IsUseParameterProcessDate { get; set; }
        public bool IsUseParameterBranch { get; set; }
        public string Argument { get; set; }
        public string Region { get; set; }
    }

}
