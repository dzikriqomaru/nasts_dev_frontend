using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Data.SqlClient;
using System.Data;
using NawaDataDAL;

namespace NawaDataService
{
    public class WorkerEOD : BackgroundService
    {
        private readonly ILogger<WorkerEOD> _logger;
        private NawaConsoleLog mylog = new NawaConsoleLog();
        private List<Thread> objArrThreadEodScheduler = new List<Thread>();

        public WorkerEOD(ILogger<WorkerEOD> logger)
        {
            _logger = logger;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            mylog.LogInfo("Service EOD Started");

            while (!stoppingToken.IsCancellationRequested)
            {
                _logger.LogInformation("Worker EOD running at: {time}", DateTimeOffset.Now);

                Run();

                await Task.Delay(1000, stoppingToken);
            }

            mylog.LogInfo("Service EOD Stopped");
        }

        protected void Run()
        {
            try
            {
                ExecSPCurrentScheduler();

                int intjmlAntrian = Convert.ToInt32(NawaDAO.ExecuteScalar(" IF EXISTS ( SELECT t.TABLE_NAME FROM INFORMATION_SCHEMA.TABLES AS t WHERE t.TABLE_NAME = 'NDSMapProcessQueue' ) BEGIN SELECT COUNT(1) jml FROM dbo.NDSMapProcessQueue WHERE ACTIVE = 1 END ELSE BEGIN SELECT 0 AS jml END "));

                if(intjmlAntrian == 0)
                {
                    Thread objThreadEODScheduler = new Thread(new ParameterizedThreadStart(RunEODScheduler));
                    objThreadEODScheduler.Name = "0";
                    objThreadEODScheduler.IsBackground = true;

                    if(!objArrThreadEodScheduler.Any(x => x.Name == objThreadEODScheduler.Name))
                    {
                        objArrThreadEodScheduler.Add(objThreadEODScheduler);
                    }
                }
                else
                {
                    DataTable objdt = NawaDAO.ExecuteTable("SELECT DISTINCT y.QueueNo FROM dbo.NDSMapProcessQueue x INNER JOIN dbo.NDSProcessQueue y ON y.PK_NDSProcess_ID=x.FK_NDSQueue_ID WHERE x.ACTIVE = 1 UNION SELECT 0  ORDER BY y.QueueNo");
                    foreach(DataRow item in objdt.Rows)
                    {
                        Thread objThreadEODScheduler = new Thread(new ParameterizedThreadStart(RunEODScheduler));
                        objThreadEODScheduler.Name = Convert.ToString(item["QueueNo"]);
                        objThreadEODScheduler.IsBackground = true;

                        if(!objArrThreadEodScheduler.Any(x => x.Name == objThreadEODScheduler.Name))
                        {
                            objArrThreadEodScheduler.Add(objThreadEODScheduler);
                        }
                    }
                }

                foreach(Thread itemsthread in objArrThreadEodScheduler)
                {
                    if (!itemsthread.IsAlive)
                    {
                        itemsthread.Start(itemsthread.Name);
                    }
                }
            }
            catch (Exception ex)
            {
                mylog.LogError("An Error has been occurred on WorkerEmail: ", ex);
            }
        }

        protected void ExecSPCurrentScheduler()
        {
            int intPeriodprocessdate = Convert.ToInt32(NawaDAO.ExecuteScalar("SELECT SettingValue FROM SystemParameter WHERE PK_SystemParameter_ID = 3000"));

            SqlParameter[] sqlParams = new SqlParameter[]
            {
                new SqlParameter("@datProcessDate", System.Data.SqlDbType.DateTime) {Value = DateTime.Now},
                new SqlParameter("@strUserId", System.Data.SqlDbType.VarChar) {Value = "sysadmin"},
                new SqlParameter("@datadate", System.Data.SqlDbType.DateTime) {Value = DateTime.Now.AddDays(intPeriodprocessdate)}
            };

            NawaDAO.ExecuteNonQuery("exec Usp_Console_InsertEODLog @datProcessDate,@strUserId,@datadate", sqlParams);
        }

        protected void RunEODScheduler(object strparam)
        {
            EODSchedulerSatuanBLL objEOdSchedulerSatuanBLL = new EODSchedulerSatuanBLL();
            objEOdSchedulerSatuanBLL.run(Convert.ToString(strparam));
        }
    }
}
