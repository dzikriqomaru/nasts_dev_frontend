﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using ICSharpCode.SharpZipLib.Zip;
using Microsoft.Data.SqlClient;
using NawaDataDAL;

namespace NawaDataService
{
    public class ReportingServiceAttachment
    {
        private NawaConsoleLog mylog = new NawaConsoleLog();

        public static void GenerateEmailAttachmentFromSSRS(int FK_EmailTEmplateSchedulerDetail_ID, string strFileExtention, string StrReportName, string strParameter, string StrFormat, WorkerOptions _configuration)
        {
            string StrURL;
            string StrCommand = "Render";

            if (string.IsNullOrEmpty(StrFormat))
                StrFormat = "PDF";

            string ObjParamReportingServiceURL = SystemParameterBLL.GetSystemParameter(13);
            string ObjParamReportingServiceUserName = SystemParameterBLL.GetSystemParameter(9);
            string ObjParamReportingServicePassword = SystemParameterBLL.GetSystemParameter(10);
            string ReportingServiceURL = "http://localhost/ReportServer_SQL2016?/";

            if (!string.IsNullOrEmpty(ObjParamReportingServiceURL))
            {
                ReportingServiceURL = ObjParamReportingServiceURL;

                if (!string.IsNullOrEmpty(ReportingServiceURL))
                {
                    if (ReportingServiceURL.EndsWith("/") & !ReportingServiceURL.Contains("?"))
                        ReportingServiceURL = ReportingServiceURL.Insert(ReportingServiceURL.LastIndexOf("/"), "?");
                    else if (ReportingServiceURL.EndsWith("?"))
                        ReportingServiceURL = ReportingServiceURL + "/";
                    else
                        ReportingServiceURL = ReportingServiceURL + "?/";
                }
            }

            string StrReportingServiceUserName = "dev";
            string StrReportingServicePassword = "nawadata";

            if (!string.IsNullOrEmpty(ObjParamReportingServiceURL))
                StrReportingServiceUserName = ObjParamReportingServiceUserName;

            if (!string.IsNullOrEmpty(ObjParamReportingServiceURL))
                StrReportingServicePassword = ObjParamReportingServicePassword;

            if (!strParameter.StartsWith("&") && !string.IsNullOrEmpty(strParameter))
                strParameter = "&" + strParameter;

            // cek current email harus di zip ngak
            string sql;
            sql = " " + Environment.NewLine
                + "SELECT COUNT(1) " + Environment.NewLine
                + "FROM   EmailTemplateSchedulerDetail AS etsd " + Environment.NewLine
                + "       INNER JOIN EmailTemplateScheduler " + Environment.NewLine
                + "            ON  etsd.FK_EmailTEmplateScheduler_ID = EmailTemplateScheduler.PK_EmailTemplateScheduler_ID " + Environment.NewLine
                + "       INNER JOIN ( " + Environment.NewLine
                + "                SELECT s.val " + Environment.NewLine
                + "                FROM   dbo.[Split]( " + Environment.NewLine
                + "                           ( " + Environment.NewLine
                + "                               SELECT sp.SettingValue " + Environment.NewLine
                + "                               FROM   SystemParameter AS sp " + Environment.NewLine
                + "                               WHERE  sp.PK_SystemParameter_Id = 4016 " + Environment.NewLine
                + "                           ), " + Environment.NewLine
                + "                           ',' " + Environment.NewLine
                + "                       ) AS s " + Environment.NewLine
                + "            ) xx " + Environment.NewLine
                + "            ON  CONVERT(VARCHAR(50), xx.val) = CONVERT(VARCHAR(50), EmailTemplateScheduler.PK_EmailTemplate_ID) " + Environment.NewLine
                + "WHERE  (etsd.PK_EmailTemplateSchedulerDetail_ID = " + FK_EmailTEmplateSchedulerDetail_ID + ") " + Environment.NewLine
                + "";
            int intresult = Convert.ToInt32(NawaDAO.ExecuteScalar(sql));
            string strpasswordzip = "";

            try
            {
                strpasswordzip = SystemParameterBLL.GetSystemParameter(4015);
            }
            catch
            {
                strpasswordzip = "";
            }

            // Dim arremail As List(Of String) = EmailTemplateNeedZip.Split(",").ToList

            try
            {
                // URL = Convert.ToString((Convert.ToString((Convert.ToString((Convert.ToString((Convert.ToString(URL & Convert.ToString("&rs:Command=")) & Command()) + "&rs:Format=") & Format()) + "&ReportMonth=") & paramReportMonth) + "&ReportYear=") & paramReportYear) + "&EmpID=") & paramEmpID
                StrURL = ReportingServiceURL + StrReportName + "&rs:Command=" + StrCommand + "&rs:Format=" + StrFormat + strParameter;

                string IsTLS = _configuration.IsTLS;
                string IsAutoRedirect = _configuration.IsAutoRedirect;
                string IsCallBack = _configuration.IsCallBack;
                string IsCertiValidationCallBack = _configuration.IsCertiValidationCallBack;
                string crtfile = _configuration.crtfile;
                string pfxfile = _configuration.pfxfile;
                string passphrase = _configuration.passphrase;

                // Create an HttpClientHandler object and set to use default credentials
                HttpClientHandler handler = new HttpClientHandler();

                if (string.IsNullOrEmpty(StrReportingServicePassword))
                {
                    handler.UseDefaultCredentials = true;
                }
                else
                {
                    handler.Credentials = new NetworkCredential(StrReportingServiceUserName, StrReportingServicePassword);
                }

                if (StrURL.Contains("https"))
                {
                    if (IsCertiValidationCallBack == "1")
                    {
                        if (IsAutoRedirect == "1")
                        {
                            handler.AllowAutoRedirect = false;
                        }
                        else if (IsAutoRedirect == "2")
                        {
                            handler.AllowAutoRedirect = true;
                        }

                        if (IsCallBack == "1")
                        {
                            handler.ServerCertificateCustomValidationCallback = delegate { return true; };
                        }
                        else if (IsCallBack == "2")
                        {
                            handler.ServerCertificateCustomValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
                        }

                        if (IsTLS == "0")
                        {
                            handler.SslProtocols = SslProtocols.Tls;
                        }
                        else if (IsTLS == "1")
                        {
                            handler.SslProtocols = SslProtocols.Tls11;
                        }
                        else if (IsTLS == "2")
                        {
                            handler.SslProtocols = SslProtocols.Tls12;
                        }
                        else if (IsTLS == "3")
                        {
                            handler.SslProtocols = SslProtocols.Tls13;
                        }
                        else if (IsTLS == "4")
                        {
                            handler.SslProtocols = SslProtocols.Ssl2;
                        }
                        else if (IsTLS == "5")
                        {
                            handler.SslProtocols = SslProtocols.Ssl3;
                        }
                        else if (IsTLS == "6")
                        {
                            handler.SslProtocols = SslProtocols.None;
                        }
                        else if (IsTLS == "7")
                        {
                            handler.SslProtocols = SslProtocols.Default;
                        }
                    }
                    else
                    {
                        handler.ClientCertificateOptions = ClientCertificateOption.Manual;
                        handler.SslProtocols = SslProtocols.Tls | SslProtocols.Tls11 | SslProtocols.Tls12;
                        handler.ClientCertificates.Add(new X509Certificate2(System.AppContext.BaseDirectory + "/" + crtfile));
                        handler.ClientCertificates.Add(new X509Certificate2(System.AppContext.BaseDirectory + "/" + pfxfile, passphrase));
                    }
                }

                byte[] dataByte;

                using (HttpClient client = new HttpClient(handler))
                {
                    HttpResponseMessage response = client.GetAsync(StrURL).Result;
                    dataByte = response.Content.ReadAsByteArrayAsync().Result;
                }

                //System.Net.HttpWebRequest Req = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(StrURL);
                //// Req.Credentials = System.Net.CredentialCache.DefaultCredentials
                //Req.Credentials = new System.Net.NetworkCredential(StrReportingServiceUserName, StrReportingServicePassword);
                //Req.Method = "GET";

                //System.Net.WebResponse objResponse = Req.GetResponse();
                //System.IO.MemoryStream MemoryStreamData = new System.IO.MemoryStream();
                //System.IO.Stream stream = objResponse.GetResponseStream();

                if (intresult == 0)
                {
                    // ngak butuh di zip
                    //byte[] buf = new byte[1024];
                    //int len = stream.Read(buf, 0, 1024);
                    //while (len > 0)
                    //{
                    //    MemoryStreamData.Write(buf, 0, len);
                    //    len = stream.Read(buf, 0, 1024);
                    //}
                    //stream.Close();
                    //MemoryStreamData.Close();

                    string StrSQLAttachment = "UPDATE [dbo].[EmailTemplateSchedulerDetailAttachment] SET  [IsiFile]=@IsiFile, FK_EmailGenerateAttachmentStatus_ID=1  WHERE " + Environment.NewLine + "FK_EmailTEmplateSchedulerDetail_ID=@FK_EmailTEmplateSchedulerDetail_ID and NamaReport=@NamaReport";

                    List<SqlParameter> ListSqlParam = new List<SqlParameter>();
                    ListSqlParam.Add(new SqlParameter("@FK_EmailTEmplateSchedulerDetail_ID", FK_EmailTEmplateSchedulerDetail_ID));

                    //byte[] IsiFile = MemoryStreamData.ToArray();
                    SqlParameter SqlParam;
                    SqlParam = new SqlParameter("@IsiFile", System.Data.SqlDbType.VarBinary, dataByte.Length);
                    SqlParam.Value = dataByte;
                    ListSqlParam.Add(SqlParam);

                    // Dim StrFileName As String = StrReportName & Guid.NewGuid().ToString() & "." & StrFormat
                    ListSqlParam.Add(new SqlParameter("@NamaReport", StrReportName));

                    NawaDAO.ExecuteNonQuery(StrSQLAttachment, ListSqlParam.ToArray());
                }
                else
                {
                    // butuh di zip
                    if (!Directory.Exists("Attachment")) Directory.CreateDirectory("Attachment");

                    FileStream file;
                    string strFilePathToZip = @"Attachment\" + StrReportName + strFileExtention;
                    string strFileZip = @"Attachment\" + StrReportName + ".zip";

                    if (System.IO.File.Exists(strFilePathToZip))
                        System.IO.File.Delete(strFilePathToZip);

                    if (System.IO.File.Exists(strFileZip))
                        System.IO.File.Delete(strFileZip);

                    //stream.CopyTo(MemoryStreamData);
                    file = new FileStream(strFilePathToZip, FileMode.Create, System.IO.FileAccess.Write);
                    file.Write(dataByte, 0, dataByte.Length);
                    file.Close();
                    //MemoryStreamData.Close();
                    //stream.Close();

                    file.Dispose();
                    //MemoryStreamData.Dispose();
                    //stream.Dispose();

                    using(FileStream fr = File.OpenRead(strFilePathToZip))
                    {
                        using (FileStream fs = File.OpenWrite(strFileZip))
                        {
                            using (ZipOutputStream zipStream = new ZipOutputStream(fs))
                            {
                                zipStream.SetLevel(9);

                                if (!string.IsNullOrEmpty(strpasswordzip))
                                {
                                    zipStream.Password = strpasswordzip;
                                }

                                var newEntry = new ZipEntry(Path.GetFileName(strFilePathToZip)) { DateTime = DateTime.Now };
                                zipStream.PutNextEntry(newEntry);

                                fr.CopyTo(zipStream);
                                zipStream.CloseEntry();
                            }
                        }
                    }

                    FileStream fileStreamZip = new FileStream(strFileZip, FileMode.Open, System.IO.FileAccess.Read);
                    string StrSQLAttachment = "UPDATE [dbo].[EmailTemplateSchedulerDetailAttachment] SET  [IsiFile]=@IsiFile, Filename=@Filename,FK_EmailGenerateAttachmentStatus_ID=1,FileExtension='.zip' WHERE " + Environment.NewLine + "FK_EmailTEmplateSchedulerDetail_ID=@FK_EmailTEmplateSchedulerDetail_ID and NamaReport=@NamaReport";

                    List<SqlParameter> ListSqlParam = new List<SqlParameter>();
                    ListSqlParam.Add(new SqlParameter("@FK_EmailTEmplateSchedulerDetail_ID", FK_EmailTEmplateSchedulerDetail_ID));

                    byte[] IsiFile = new byte[fileStreamZip.Length + 1];
                    fileStreamZip.Read(IsiFile, 0, (int)fileStreamZip.Length);

                    SqlParameter SqlParam;
                    SqlParam = new SqlParameter("@IsiFile", System.Data.SqlDbType.VarBinary, IsiFile.Length);
                    SqlParam.Value = IsiFile;
                    ListSqlParam.Add(SqlParam);

                    // Dim StrFileName As String = StrReportName & Guid.NewGuid().ToString() & "." & StrFormat
                    ListSqlParam.Add(new SqlParameter("@NamaReport", StrReportName));
                    ListSqlParam.Add(new SqlParameter("@Filename", StrReportName + ".zip"));

                    NawaDAO.ExecuteNonQuery(StrSQLAttachment, ListSqlParam.ToArray());
                    fileStreamZip.Dispose();
                    fileStreamZip = null;

                    if (System.IO.File.Exists(strFilePathToZip))
                        System.IO.File.Delete(strFilePathToZip);

                    if (System.IO.File.Exists(strFileZip))
                        System.IO.File.Delete(strFileZip);
                }

                // Const StrSQLAttachment As String = "UPDATE [dbo].[EmailTemplateSchedulerDetailAttachment] SET  [IsiFile]=@IsiFile, FK_EmailGenerateAttachmentStatus_ID=1  WHERE " & vbCrLf &
                // "FK_EmailTEmplateSchedulerDetail_ID=@FK_EmailTEmplateSchedulerDetail_ID and NamaReport=@NamaReport"


                // Dim ListSqlParam As List(Of SqlParameter) = New List(Of SqlParameter)
                // ListSqlParam.Add(New SqlParameter("@FK_EmailTEmplateSchedulerDetail_ID", FK_EmailTEmplateSchedulerDetail_ID))

                // Dim IsiFile As Byte() = MemoryStreamData.ToArray()
                // Dim SqlParam As SqlParameter
                // SqlParam = New SqlParameter("@IsiFile", SqlDbType.VarBinary, IsiFile.Length)
                // SqlParam.Value = IsiFile
                // ListSqlParam.Add(SqlParam)

                // 'Dim StrFileName As String = StrReportName & Guid.NewGuid().ToString() & "." & StrFormat
                // ListSqlParam.Add(New SqlParameter("@NamaReport", StrReportName))

                // SQLHelper.ExecuteNonQuery(SQLHelper.strConnectionString, CommandType.Text, StrSQLAttachment, ListSqlParam.ToArray()).ToString()

                //return true;
            }
            catch (Exception ex)
            {
                string strerror = "UPDATE EmailTemplateSchedulerDetail SET FK_EmailStatus_ID = 5, ErrorMessage = '" + ex.Message.ToString() + "'  WHERE PK_EmailTemplateSchedulerDetail_ID=" + FK_EmailTEmplateSchedulerDetail_ID;
                NawaDAO.ExecuteNonQuery(strerror);

                throw;
            }
        }
    }
}
