﻿using Microsoft.Data.SqlClient;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;

namespace NawaDataService
{
    public static class NawaDAO
    {
        #region Variable
        public enum DBType
        {
            SQLServer,
            PostgreSQL
        }

        public static Dictionary<string, DBType> dbType { get; set; }
        public static Dictionary<string, string> server { get; set; }
        public static Dictionary<string, string> database { get; set; }
        public static Dictionary<string, string> user { get; set; }
        public static Dictionary<string, string> password { private get; set; }
        public static Dictionary<string, int> commandTimeout { get; set; }
        public static Dictionary<string, string> connectionString { get; private set; }
        private static Dictionary<string, IDBHelper> helper { get; set; }

        #endregion

        #region Method
        public static void Config(DBType dbType, string server, string database, string user, string password, int commandTimeout = 30, string connectionKey = "default")
        {
            CheckKey(connectionKey);

            NawaDAO.dbType[connectionKey] = dbType;
            NawaDAO.server[connectionKey] = server;
            NawaDAO.database[connectionKey] = database;
            NawaDAO.user[connectionKey] = user;
            NawaDAO.password[connectionKey] = password;
            NawaDAO.commandTimeout[connectionKey] = commandTimeout;

            switch (dbType)
            {
                case DBType.SQLServer:
                    if (string.IsNullOrEmpty(user) && string.IsNullOrEmpty(password))
                    {
                        NawaDAO.connectionString[connectionKey] = String.Format("Integrated Security=True;Persist Security Info=True;user id={2};Initial Catalog={1};Data Source={0}", NawaDAO.server[connectionKey], NawaDAO.database[connectionKey], NawaDAO.user[connectionKey]);
                    }
                    else
                    {
                        NawaDAO.connectionString[connectionKey] = String.Format("Password={3};Persist Security Info=True;user id={2};Initial Catalog={1};Data Source={0}", NawaDAO.server[connectionKey], NawaDAO.database[connectionKey], NawaDAO.user[connectionKey], NawaDAO.password[connectionKey]);
                    }

                    NawaDAO.helper[connectionKey] = new SQLHelper(NawaDAO.connectionString[connectionKey]);
                    break;
                //case DBType.PostgreSQL:
                //    NawaDAO.connectionString[connectionKey] = String.Format("Password={3};Persist Security Info=True;user id={2};Initial Catalog={1};Data Source={0}", NawaDAO.server[connectionKey], NawaDAO.database[connectionKey], NawaDAO.user[connectionKey], NawaDAO.password[connectionKey]);

                //    NawaDAO.helper[connectionKey] = new PostgreSQLHelper(NawaDAO.connectionString[connectionKey]);
                //    break;
                default:
                    if (string.IsNullOrEmpty(user) && string.IsNullOrEmpty(password))
                    {
                        NawaDAO.connectionString[connectionKey] = String.Format("Integrated Security=True;Persist Security Info=True;user id={2};Initial Catalog={1};Data Source={0}", NawaDAO.server[connectionKey], NawaDAO.database[connectionKey], NawaDAO.user[connectionKey]);
                    }
                    else
                    {
                        NawaDAO.connectionString[connectionKey] = String.Format("Password={3};Persist Security Info=True;user id={2};Initial Catalog={1};Data Source={0}", NawaDAO.server[connectionKey], NawaDAO.database[connectionKey], NawaDAO.user[connectionKey], NawaDAO.password[connectionKey]);
                    }

                    NawaDAO.helper[connectionKey] = new SQLHelper(NawaDAO.connectionString[connectionKey]);
                    break;
            }
        }

        private static void CheckKey(string connectionKey)
        {
            if (NawaDAO.dbType == null)
            {
                NawaDAO.dbType = new Dictionary<string, DBType>();
            }
            if (NawaDAO.server == null)
            {
                NawaDAO.server = new Dictionary<string, string>();
            }
            if (NawaDAO.database == null)
            {
                NawaDAO.database = new Dictionary<string, string>();
            }
            if (NawaDAO.user == null)
            {
                NawaDAO.user = new Dictionary<string, string>();
            }
            if (NawaDAO.password == null)
            {
                NawaDAO.password = new Dictionary<string, string>();
            }
            if (NawaDAO.commandTimeout == null)
            {
                NawaDAO.commandTimeout = new Dictionary<string, int>();
            }
            if (NawaDAO.connectionString == null)
            {
                NawaDAO.connectionString = new Dictionary<string, string>();
            }
            if (NawaDAO.helper == null)
            {
                NawaDAO.helper = new Dictionary<string, IDBHelper>();
            }

            if (!NawaDAO.dbType.ContainsKey(connectionKey))
            {
                NawaDAO.dbType.Add(connectionKey, DBType.SQLServer);
            }
            if (!NawaDAO.server.ContainsKey(connectionKey))
            {
                NawaDAO.server.Add(connectionKey, null);
            }
            if (!NawaDAO.database.ContainsKey(connectionKey))
            {
                NawaDAO.database.Add(connectionKey, null);
            }
            if (!NawaDAO.user.ContainsKey(connectionKey))
            {
                NawaDAO.user.Add(connectionKey, null);
            }
            if (!NawaDAO.password.ContainsKey(connectionKey))
            {
                NawaDAO.password.Add(connectionKey, null);
            }
            if (!NawaDAO.commandTimeout.ContainsKey(connectionKey))
            {
                NawaDAO.commandTimeout.Add(connectionKey, 30);
            }
            if (!NawaDAO.connectionString.ContainsKey(connectionKey))
            {
                NawaDAO.connectionString.Add(connectionKey, null);
            }
            if (!NawaDAO.helper.ContainsKey(connectionKey))
            {
                NawaDAO.helper.Add(connectionKey, null);
            }
        }

        public static string GetConnectionString(string connectionKey = "default")
        {
            if (!server.ContainsKey(connectionKey)) throw new Exception(string.Format("Connection Key {0} not Found", connectionKey));
            return NawaDAO.connectionString[connectionKey];
        }

        public static NawaConnection CreateConnection(string connectionKey = "default")
        {
            if (!server.ContainsKey(connectionKey)) throw new Exception(string.Format("Connection Key {0} not Found", connectionKey));
            return new NawaConnection(helper[connectionKey].CreateConnection());
        }

        public static int ExecuteNonQuery(string cmdText, IDataParameter[] cmdParams = null, NawaConnection connection = null, CommandType commandType = CommandType.Text, string connectionKey = "default")
        {
            if (!server.ContainsKey(connectionKey)) throw new Exception(string.Format("Connection Key {0} not Found", connectionKey));
            return helper[connectionKey].ExecuteNonQuery(cmdText, cmdParams, NawaDAO.commandTimeout[connectionKey], connection, commandType);
        }

        public static DataTable ExecuteTable(string cmdText, IDataParameter[] cmdParams = null, NawaConnection connection = null, CommandType commandType = CommandType.Text, string connectionKey = "default")
        {
            if (!server.ContainsKey(connectionKey)) throw new Exception(string.Format("Connection Key {0} not Found", connectionKey));
            return helper[connectionKey].ExecuteTable(cmdText, cmdParams, NawaDAO.commandTimeout[connectionKey], connection, commandType);
        }

        public static DataTable ExecuteTable(string table, string fields, string filters, string sorts, int pageIndex, int pageSize, out int count, IDataParameter[] cmdParams = null, NawaConnection connection = null, string connectionKey = "default")
        {
            if (!server.ContainsKey(connectionKey)) throw new Exception(string.Format("Connection Key {0} not Found", connectionKey));
            int countRes;

            DataTable dt = helper[connectionKey].ExecuteTable(table, fields, filters, sorts, pageIndex, pageSize, out countRes, cmdParams, NawaDAO.commandTimeout[connectionKey], connection);

            count = countRes;

            return dt;
        }

        public static DataRow ExecuteRow(string cmdText, IDataParameter[] cmdParams = null, NawaConnection connection = null, CommandType commandType = CommandType.Text, string connectionKey = "default")
        {
            if (!server.ContainsKey(connectionKey)) throw new Exception(string.Format("Connection Key {0} not Found", connectionKey));
            return helper[connectionKey].ExecuteRow(cmdText, cmdParams, NawaDAO.commandTimeout[connectionKey], connection, commandType);
        }

        public static object ExecuteScalar(string cmdText, IDataParameter[] cmdParams = null, NawaConnection connection = null, CommandType commandType = CommandType.Text, string connectionKey = "default")
        {
            if (!server.ContainsKey(connectionKey)) throw new Exception(string.Format("Connection Key {0} not Found", connectionKey));
            return helper[connectionKey].ExecuteScalar(cmdText, cmdParams, NawaDAO.commandTimeout[connectionKey], connection, commandType);
        }

        public static bool InsertBulkCopy(DataTable data, string destTable, NawaConnection connection, string connectionKey = "default")
        {
            if (!server.ContainsKey(connectionKey)) throw new Exception(string.Format("Connection Key {0} not Found", connectionKey));
            return helper[connectionKey].InsertBulkCopy(data, destTable, connection);
        }

        public static List<T> ConvertDataTable<T>(DataTable dt) where T : new()
        {
            List<T> data = new List<T>();

            BindingFlags flg = BindingFlags.Public | BindingFlags.Instance;

            List<CFieldProperty> objFieldNames = typeof(T).GetProperties(flg).Cast<PropertyInfo>().
            Select(item => new CFieldProperty
            {
                Name = item.Name,
                Type = Nullable.GetUnderlyingType(item.PropertyType) ?? item.PropertyType
            }).ToList();

            List<CFieldProperty> dtlFieldNames = dt.Columns.Cast<DataColumn>().
                Select(item => new CFieldProperty
                {
                    Name = item.ColumnName,
                    Type = item.DataType
                }).ToList();

            foreach (DataRow dataRow in dt.AsEnumerable().ToList())
            {
                T obj = new T();

                foreach (var dtField in dtlFieldNames)
                {
                    PropertyInfo propertyInfos = obj.GetType().GetProperty(dtField.Name);

                    var field = objFieldNames.Find(x => x.Name == dtField.Name);

                    if (field != null)
                    {

                        if (propertyInfos.PropertyType == typeof(DateTime))
                        {
                            propertyInfos.SetValue(obj, ConvertToDateTime(dataRow[dtField.Name]), null);
                        }
                        else if (propertyInfos.PropertyType == typeof(Boolean))
                        {
                            propertyInfos.SetValue(obj, ConvertToBoolean(dataRow[dtField.Name]), null);
                        }
                        else if (propertyInfos.PropertyType == typeof(bool))
                        {
                            propertyInfos.SetValue(obj, ConvertToBool(dataRow[dtField.Name]), null);
                        }
                        else if (propertyInfos.PropertyType == typeof(int))
                        {
                            propertyInfos.SetValue(obj, ConvertToInt(dataRow[dtField.Name]), null);
                        }
                        else if (propertyInfos.PropertyType == typeof(long))
                        {
                            propertyInfos.SetValue(obj, ConvertToLong(dataRow[dtField.Name]), null);
                        }
                        else if (propertyInfos.PropertyType == typeof(byte[]))
                        {
                            propertyInfos.SetValue(obj, HelperFunctions.ReturnByteZeroIfNull(dataRow[dtField.Name]), null);
                        }
                        else if (propertyInfos.PropertyType == typeof(decimal))
                        {
                            propertyInfos.SetValue(obj, ConvertToDecimal(dataRow[dtField.Name]), null);
                        }
                        else if (propertyInfos.PropertyType == typeof(String))
                        {
                            if (dataRow[dtField.Name].GetType() == typeof(DateTime))
                            {
                                propertyInfos.SetValue(obj, ConvertToDateString(dataRow[dtField.Name]), null);
                            }
                            else
                            {
                                propertyInfos.SetValue(obj, ConvertToString(dataRow[dtField.Name]), null);
                            }
                        }
                    }
                }
                data.Add(obj);
            }

            return data;
        }

        public static T ConvertDataTable<T>(DataRow dr) where T : new()
        {
            T data;

            BindingFlags flg = BindingFlags.Public | BindingFlags.Instance;

            List<CFieldProperty> objFieldNames = typeof(T).GetProperties(flg).Cast<PropertyInfo>().
            Select(item => new CFieldProperty
            {
                Name = item.Name,
                Type = Nullable.GetUnderlyingType(item.PropertyType) ?? item.PropertyType
            }).ToList();

            List<CFieldProperty> dtlFieldNames = dr.Table.Columns.Cast<DataColumn>().
                Select(item => new CFieldProperty
                {
                    Name = item.ColumnName,
                    Type = item.DataType
                }).ToList();

            T obj = new T();

            foreach (var dtField in dtlFieldNames)
            {
                PropertyInfo propertyInfos = obj.GetType().GetProperty(dtField.Name);

                var field = objFieldNames.Find(x => x.Name == dtField.Name);

                if (field != null)
                {

                    if (propertyInfos.PropertyType == typeof(DateTime))
                    {
                        propertyInfos.SetValue(obj, ConvertToDateTime(dr[dtField.Name]), null);
                    }
                    else if (propertyInfos.PropertyType == typeof(DateTime?) && ConvertToString(dr[dtField.Name]) != "")
                    {
                        propertyInfos.SetValue(obj, ConvertToDateTime(dr[dtField.Name]), null);
                    }
                    else if (propertyInfos.PropertyType == typeof(Boolean))
                    {
                        propertyInfos.SetValue(obj, ConvertToBoolean(dr[dtField.Name]), null);
                    }
                    else if (propertyInfos.PropertyType == typeof(Boolean))
                    {
                        propertyInfos.SetValue(obj, ConvertToBoolean(dr[dtField.Name]), null);
                    }
                    else if (propertyInfos.PropertyType == typeof(bool))
                    {
                        propertyInfos.SetValue(obj, ConvertToBool(dr[dtField.Name]), null);
                    }
                    else if (propertyInfos.PropertyType == typeof(int))
                    {
                        propertyInfos.SetValue(obj, ConvertToInt(dr[dtField.Name]), null);
                    }
                    else if (propertyInfos.PropertyType == typeof(byte[]))
                    {
                        propertyInfos.SetValue(obj, HelperFunctions.ReturnByteZeroIfNull(dr[dtField.Name]), null);
                    }
                    else if (propertyInfos.PropertyType == typeof(long))
                    {
                        propertyInfos.SetValue(obj, ConvertToLong(dr[dtField.Name]), null);
                    }
                    else if (propertyInfos.PropertyType == typeof(decimal))
                    {
                        propertyInfos.SetValue(obj, ConvertToDecimal(dr[dtField.Name]), null);
                    }
                    else if (propertyInfos.PropertyType == typeof(String))
                    {
                        if (dr[dtField.Name].GetType() == typeof(DateTime))
                        {
                            propertyInfos.SetValue(obj, ConvertToDateString(dr[dtField.Name]), null);
                        }
                        else
                        {
                            propertyInfos.SetValue(obj, ConvertToString(dr[dtField.Name]), null);
                        }
                    }
                }
            }
            data = obj;

            return data;
        }

        public static DataTable ConvertListObject<T>(IList<T> data)
        {
            /* --- Convert class object sesuai isinya --- */
            DataTable table = new DataTable();

            //special handling for value types and string
            if (typeof(T).IsValueType || typeof(T).Equals(typeof(string)))
            {
                DataColumn dc = new DataColumn("Value");
                table.Columns.Add(dc);
                foreach (T item in data)
                {
                    DataRow row = table.NewRow();
                    row[0] = item;
                    table.Rows.Add(row);
                }
            }
            else
            {
                PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
                foreach (PropertyDescriptor prop in properties)
                {
                    table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
                }
                foreach (T item in data)
                {
                    DataRow row = table.NewRow();
                    foreach (PropertyDescriptor prop in properties)
                    {
                        try
                        {
                            row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                        }
                        catch (Exception ex)
                        {
                            row[prop.Name] = DBNull.Value;
                        }
                    }
                    table.Rows.Add(row);
                }
            }
            return table;
        }

        public static DataTable ConvertActualObject<T>(IList<T> data, string tableName)
        {
            /* --- Convert class object sesuai table yg didefine --- */
            DataTable table = new DataTable();

            #region Cari column sesuai table 
            List<string> listColumn = new List<string>();
            string queryStr = @" SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS
                                WHERE TABLE_NAME = @TableName
                                ORDER BY ORDINAL_POSITION ";

            SqlParameter[] queryParams = new SqlParameter[]
            {
                    new SqlParameter("@TableName", SqlDbType.VarChar) { Value = tableName }
            };

            DataTable dtColumn = ExecuteTable(queryStr, queryParams);
            if (dtColumn.Rows.Count > 0)
            {
                listColumn = dtColumn.AsEnumerable()
                       .Select(r => r.Field<string>("COLUMN_NAME"))
                       .ToList();
            }
            #endregion

            //special handling for value types and string
            if (typeof(T).IsValueType || typeof(T).Equals(typeof(string)))
            {
                DataColumn dc = new DataColumn("Value");
                table.Columns.Add(dc);
                foreach (T item in data)
                {
                    DataRow row = table.NewRow();
                    row[0] = item;
                    table.Rows.Add(row);
                }
            }
            else
            {
                PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
                foreach (PropertyDescriptor prop in properties)
                {
                    if (listColumn.FirstOrDefault(x => x == prop.Name) != null)
                        table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
                }
                foreach (T item in data)
                {
                    DataRow row = table.NewRow();
                    foreach (PropertyDescriptor prop in properties)
                    {
                        if (listColumn.FirstOrDefault(x => x == prop.Name) != null)
                        {
                            try
                            {
                                row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                            }
                            catch (Exception ex)
                            {
                                row[prop.Name] = DBNull.Value;
                            }
                        }
                    }
                    table.Rows.Add(row);
                }
            }
            return table;
        }

        public static DataTable ConvertListDictionary(List<Dictionary<string, object>> listDictionary)
        {
            DataTable dtResult = new DataTable();
            if (listDictionary.Count == 0)
                return dtResult;

            var ColumnNames = listDictionary.SelectMany(dict => dict.Keys).Distinct();
            dtResult.Columns.AddRange(ColumnNames.Select(c => new DataColumn(c)).ToArray());
            foreach (Dictionary<string, object> item in listDictionary)
            {
                var row = dtResult.NewRow();
                foreach (var key in item.Keys)
                {
                    row[key] = item[key];
                }

                dtResult.Rows.Add(row);
            }

            return dtResult;
        }
        #endregion

        #region Convert Data Type
        private static string ConvertToDateString(object date)
        {
            if (date == null)
                return string.Empty;

            return HelperFunctions.ConvertDate(Convert.ToDateTime(date));
        }


        private static string ConvertToString(object value)
        {
            return Convert.ToString(HelperFunctions.ReturnEmptyIfNull(value));
        }

        private static int ConvertToInt(object value)
        {
            return Convert.ToInt32(HelperFunctions.ReturnZeroIfNull(value));
        }

        private static bool ConvertToBool(object value)
        {
            return Convert.ToBoolean(HelperFunctions.ReturnFalseIfNull(value));
        }

        private static long ConvertToLong(object value)
        {
            return Convert.ToInt64(HelperFunctions.ReturnZeroIfNull(value));
        }

        private static decimal ConvertToDecimal(object value)
        {
            return Convert.ToDecimal(HelperFunctions.ReturnZeroIfNull(value));
        }

        private static DateTime ConvertToDateTime(object date)
        {
            return Convert.ToDateTime(HelperFunctions.ReturnDateTimeMinIfNull(date));
        }

        private static bool ConvertToBoolean(object value)
        {
            return Convert.ToBoolean(HelperFunctions.ReturnFalseIfNull(value));
        }

        public static MemoryStream ConvertToExcel(DataTable dt)
        {
            IWorkbook workbook = new XSSFWorkbook();
            ISheet sheet = workbook.CreateSheet("Data");

            IRow headerRow = sheet.CreateRow(0);
            int counter = 0;
            foreach (DataColumn dc in dt.Columns)
            {
                ICell cell = headerRow.CreateCell(counter++, CellType.String);
                cell.SetCellValue(dc.ColumnName);
            }

            counter = 1;
            foreach (DataRow dr in dt.Rows)
            {
                IRow row = sheet.CreateRow(counter++);

                int columnCounter = 0;
                foreach (DataColumn dc in dt.Columns)
                {
                    ICell cell = row.CreateCell(columnCounter++);
                    if (dc.DataType == typeof(DateTime))
                    {
                        IDataFormat dataFormatCustom = workbook.CreateDataFormat();
                        cell.CellStyle = workbook.CreateCellStyle();
                        cell.CellStyle.DataFormat = dataFormatCustom.GetFormat("dd MMM yyyy");
                    }
                    if (dr[dc.ColumnName] == DBNull.Value) continue;

                    switch (Type.GetTypeCode(dc.DataType))
                    {
                        case TypeCode.Int16:
                        case TypeCode.Int32:
                        case TypeCode.Int64:
                        case TypeCode.Single:
                        case TypeCode.Double:
                            cell.SetCellValue(Convert.ToDouble(dr[dc.ColumnName]));
                            break;
                        case TypeCode.Boolean:
                            cell.SetCellValue(Convert.ToBoolean(dr[dc.ColumnName]));
                            break;
                        case TypeCode.DateTime:
                            cell.SetCellValue(Convert.ToDateTime(dr[dc.ColumnName]));
                            break;
                        case TypeCode.String:
                            cell.SetCellValue(Convert.ToString(dr[dc.ColumnName]));
                            break;
                        default:
                            cell.SetCellValue(Convert.ToString(dr[dc.ColumnName]));
                            break;
                    }

                }
            }

            for (int i = 0; i <= dt.Columns.Count; i++) sheet.AutoSizeColumn(i);

            MemoryStream ms = new MemoryStream();
            using (MemoryStream tempStream = new MemoryStream())
            {
                workbook.Write(tempStream);
                byte[] byteArray = tempStream.ToArray();
                ms.Write(byteArray, 0, byteArray.Length);
            }

            return ms;
        }

        #endregion

        private class CFieldProperty
        {
            public string Name { get; set; }
            public Type Type { get; set; }
        }
    }
}
