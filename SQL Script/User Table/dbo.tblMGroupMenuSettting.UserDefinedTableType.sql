/****** Object:  UserDefinedTableType [dbo].[tblMGroupMenuSettting]    Script Date: 12/12/2023 11:22:54 AM ******/
CREATE TYPE [dbo].[tblMGroupMenuSettting] AS TABLE(
	[PK_MGroupMenuSettting_ID] [int] NULL,
	[FK_MGroupMenu_ID] [int] NULL,
	[mMenuID] [varchar](50) NULL,
	[mMenuLabel] [varchar](250) NULL,
	[mMenuParentID] [varchar](50) NULL,
	[mMenuURL] [varchar](250) NULL,
	[FK_Module_ID] [int] NULL,
	[FK_Action_ID] [int] NULL,
	[urutan] [int] NULL,
	[IconName] [varchar](250) NULL
)
GO
