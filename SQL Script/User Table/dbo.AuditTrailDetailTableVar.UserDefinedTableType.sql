/****** Object:  UserDefinedTableType [dbo].[AuditTrailDetailTableVar]    Script Date: 12/12/2023 11:22:54 AM ******/
CREATE TYPE [dbo].[AuditTrailDetailTableVar] AS TABLE(
	[PK_AuditTrailDetail_id] [bigint] NULL,
	[FK_AuditTrailHeader_ID] [bigint] NULL,
	[FieldNameInTable] [varchar](250) NULL,
	[FieldName] [varchar](100) NULL,
	[OldValue] [varchar](max) NULL,
	[NewValue] [varchar](max) NULL,
	[PrimarykeyData] [varchar](8000) NULL,
	[nawa_action] [varchar](50) NULL
)
GO
