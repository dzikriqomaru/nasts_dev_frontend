/****** Object:  UserDefinedTableType [dbo].[tblMGroupMenuAccessField]    Script Date: 12/12/2023 11:22:54 AM ******/
CREATE TYPE [dbo].[tblMGroupMenuAccessField] AS TABLE(
	[PK_MGroupMenuAccessField_ID] [int] NULL,
	[FK_MGroupMenu_ID] [int] NULL,
	[FK_Module_ID] [int] NULL,
	[ModuleField] [varchar](250) NULL,
	[bAdd] [bit] NULL,
	[bEdit] [bit] NULL,
	[bDelete] [bit] NULL,
	[bActivation] [bit] NULL,
	[bView] [bit] NULL,
	[bApproval] [bit] NULL,
	[bUpload] [bit] NULL,
	[bDetail] [bit] NULL
)
GO
