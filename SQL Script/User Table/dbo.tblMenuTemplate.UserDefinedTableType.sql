/****** Object:  UserDefinedTableType [dbo].[tblMenuTemplate]    Script Date: 12/12/2023 11:22:54 AM ******/
CREATE TYPE [dbo].[tblMenuTemplate] AS TABLE(
	[mMenuID] [varchar](50) NOT NULL,
	[mMenuLabel] [varchar](250) NULL,
	[mMenuParentID] [varchar](50) NULL,
	[mMenuURL] [varchar](250) NULL,
	[FK_Module_ID] [int] NULL,
	[FK_Action_ID] [int] NULL,
	[urutan] [int] NULL,
	[IconName] [varchar](250) NULL
)
GO
