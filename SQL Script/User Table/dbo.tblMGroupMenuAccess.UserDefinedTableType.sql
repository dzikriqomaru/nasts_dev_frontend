/****** Object:  UserDefinedTableType [dbo].[tblMGroupMenuAccess]    Script Date: 12/12/2023 11:22:54 AM ******/
CREATE TYPE [dbo].[tblMGroupMenuAccess] AS TABLE(
	[PK_MGroupMenuAcess_ID] [int] NULL,
	[FK_GroupMenu_ID] [int] NULL,
	[FK_Module_ID] [int] NULL,
	[bAdd] [bit] NULL,
	[bEdit] [bit] NULL,
	[bDelete] [bit] NULL,
	[bActivation] [bit] NULL,
	[bView] [bit] NULL,
	[bApproval] [bit] NULL,
	[bUpload] [bit] NULL,
	[bDetail] [bit] NULL
)
GO
