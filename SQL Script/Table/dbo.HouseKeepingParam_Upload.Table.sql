/****** Object:  Table [dbo].[HouseKeepingParam_Upload]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HouseKeepingParam_Upload](
	[PK_upload_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[nawa_userid] [varchar](50) NULL,
	[nawa_recordnumber] [bigint] NULL,
	[KeteranganError] [varchar](max) NULL,
	[nawa_Action] [varchar](50) NULL,
	[PK_House_Keeping_Param_ID] [varchar](max) NULL,
	[scrTable] [varchar](max) NULL,
	[destTable] [varchar](max) NULL,
	[FieldName] [varchar](max) NULL,
	[PeriodType] [varchar](max) NULL,
	[RetentionPeriod] [varchar](max) NULL,
	[Active] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
