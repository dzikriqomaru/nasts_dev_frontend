/****** Object:  Table [dbo].[MViewBuilder]    Script Date: 12/12/2023 11:13:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MViewBuilder](
	[PK_MViewBuilder_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[FK_Module_ID] [int] NULL,
	[Active] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[LastUpdateBy] [varchar](50) NULL,
	[ApprovedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[ApprovedDate] [datetime] NULL,
	[Alternateby] [varchar](50) NULL,
	[DraftModule_JSON] [varchar](max) NULL,
	[IsDraft] [bit] NULL,
 CONSTRAINT [PK_MViewBuilder] PRIMARY KEY CLUSTERED 
(
	[PK_MViewBuilder_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
