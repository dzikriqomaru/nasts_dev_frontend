/****** Object:  Table [dbo].[EmailAttachmentType]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmailAttachmentType](
	[PK_EmailAttachmentType_ID] [int] IDENTITY(1,1) NOT NULL,
	[EmailAttachmentType] [varchar](50) NULL,
 CONSTRAINT [PK_EmailAttachmentType] PRIMARY KEY CLUSTERED 
(
	[PK_EmailAttachmentType_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
