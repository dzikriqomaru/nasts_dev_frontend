/****** Object:  Table [dbo].[MUserStructure]    Script Date: 12/12/2023 11:13:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MUserStructure](
	[PK_MUserStructure] [int] IDENTITY(1,1) NOT NULL,
	[FK_User_ID] [int] NOT NULL,
	[RM_code] [varchar](50) NULL,
	[FK_Parent_ID] [int] NOT NULL,
	[Active] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[LastUpdateBy] [varchar](50) NULL,
	[ApprovedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[ApprovedDate] [datetime] NULL,
	[Alternateby] [varchar](50) NULL,
 CONSTRAINT [PK_MUserStructure] PRIMARY KEY CLUSTERED 
(
	[PK_MUserStructure] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
