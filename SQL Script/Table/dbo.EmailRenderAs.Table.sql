/****** Object:  Table [dbo].[EmailRenderAs]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmailRenderAs](
	[PK_EmailRenderAs_ID] [int] NOT NULL,
	[EmailRenderAsName] [varchar](50) NULL,
	[FileExtension] [varchar](10) NULL,
	[MimeType] [varchar](250) NULL,
 CONSTRAINT [PK_EmailRenderAs] PRIMARY KEY CLUSTERED 
(
	[PK_EmailRenderAs_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
