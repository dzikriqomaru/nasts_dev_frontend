/****** Object:  Table [dbo].[AuditTrailDetail]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AuditTrailDetail](
	[PK_AuditTrailDetail_id] [bigint] IDENTITY(1,1) NOT NULL,
	[FK_AuditTrailHeader_ID] [bigint] NULL,
	[FieldName] [varchar](100) NULL,
	[OldValue] [varchar](max) NULL,
	[NewValue] [varchar](max) NULL,
 CONSTRAINT [PK_AuditTrail] PRIMARY KEY CLUSTERED 
(
	[PK_AuditTrailDetail_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[AuditTrailDetail]  WITH NOCHECK ADD  CONSTRAINT [FK_AuditTrailDetail_AuditTrailHeader] FOREIGN KEY([FK_AuditTrailHeader_ID])
REFERENCES [dbo].[AuditTrailHeader] ([PK_AuditTrail_ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AuditTrailDetail] CHECK CONSTRAINT [FK_AuditTrailDetail_AuditTrailHeader]
GO
