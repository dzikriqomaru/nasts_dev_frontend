/****** Object:  Table [dbo].[MWorkFlow]    Script Date: 12/12/2023 11:13:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MWorkFlow](
	[PK_WorkFlow] [int] IDENTITY(1,1) NOT NULL,
	[WorkFlowName] [varchar](250) NOT NULL,
	[ReqEmailTemplateID] [int] NULL,
	[RevEmailTemplateID] [int] NULL,
	[ApprovedEmailTemplateID] [int] NULL,
	[RejectEmailTemplateID] [int] NULL,
	[Active] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[LastUpdateBy] [varchar](50) NULL,
	[ApprovedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[ApprovedDate] [datetime] NULL,
	[Alternateby] [varchar](50) NULL,
 CONSTRAINT [PK_MWorkFlow] PRIMARY KEY CLUSTERED 
(
	[PK_WorkFlow] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
