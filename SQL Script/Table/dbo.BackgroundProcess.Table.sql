/****** Object:  Table [dbo].[BackgroundProcess]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BackgroundProcess](
	[PK_BackgroundProcess_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[FK_Module_ID] [int] NULL,
	[FK_ProcessType_ID] [int] NULL,
	[FK_Status_ID] [int] NULL,
	[Message] [varchar](max) NULL,
	[Data] [varbinary](max) NULL,
	[Active] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[LastUpdateBy] [varchar](50) NULL,
	[ApprovedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[ApprovedDate] [datetime] NULL,
	[Alternateby] [varchar](50) NULL,
 CONSTRAINT [PK_BackgroundProcess] PRIMARY KEY CLUSTERED 
(
	[PK_BackgroundProcess_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
