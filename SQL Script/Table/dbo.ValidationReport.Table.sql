/****** Object:  Table [dbo].[ValidationReport]    Script Date: 12/12/2023 11:20:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ValidationReport](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SegmentData] [varchar](100) NOT NULL,
	[NamaField] [varchar](255) NULL,
	[ValidationMessage] [varchar](8000) NULL,
	[Active] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[LastUpdateBy] [varchar](50) NULL,
	[ApprovedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[ApprovedDate] [datetime] NULL,
	[KeyFieldValue] [varchar](1000) NULL,
	[KeyField] [varchar](100) NULL,
	[ModuleURL] [varchar](200) NULL,
	[RecordID] [varchar](50) NULL,
	[ModuleID] [int] NULL,
	[FK_ValidationType] [int] NULL,
	[Edited] [bit] NULL,
	[ErrorType] [varchar](50) NULL,
	[FieldValue] [varchar](max) NULL,
	[TanggalData] [datetime] NOT NULL,
	[KodeCabang] [varchar](6) NULL,
	[ReferenceField] [varchar](100) NULL,
	[ReferenceValue] [varchar](100) NULL,
	[FK_DataSet] [int] NULL,
	[FK_KategoriValidasi] [int] NULL,
 CONSTRAINT [PK_ValidationReport] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
