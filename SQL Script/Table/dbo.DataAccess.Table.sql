/****** Object:  Table [dbo].[DataAccess]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DataAccess](
	[PK_DataAccess_ID] [int] IDENTITY(1,1) NOT NULL,
	[FK_Role_ID] [int] NULL,
	[FK_Module_ID] [int] NOT NULL,
	[FilterData] [varchar](8000) NOT NULL,
	[Active] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[LastUpdateBy] [varchar](50) NULL,
	[ApprovedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[ApprovedDate] [datetime] NULL,
	[AlternateBy] [varchar](50) NULL,
	[FK_ModuleAction] [int] NULL,
 CONSTRAINT [PK_DataAccess] PRIMARY KEY CLUSTERED 
(
	[PK_DataAccess_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
