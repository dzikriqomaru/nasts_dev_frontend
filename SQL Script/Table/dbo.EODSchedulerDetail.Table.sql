/****** Object:  Table [dbo].[EODSchedulerDetail]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EODSchedulerDetail](
	[PK_EODSchedulerDetail_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[FK_EODSCheduler_ID] [bigint] NULL,
	[FK_EODTask_ID] [bigint] NULL,
	[OrderNo] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[LastUpdateBy] [varchar](50) NULL,
	[ApprovedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[ApprovedDate] [datetime] NULL,
 CONSTRAINT [PK_EODSchedulerDetail] PRIMARY KEY CLUSTERED 
(
	[PK_EODSchedulerDetail_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
