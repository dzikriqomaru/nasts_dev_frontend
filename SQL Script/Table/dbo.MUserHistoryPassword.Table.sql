/****** Object:  Table [dbo].[MUserHistoryPassword]    Script Date: 12/12/2023 11:13:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MUserHistoryPassword](
	[PK_MUser_HistoryPassword_ID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [varchar](50) NULL,
	[HistoryPassword] [varchar](250) NULL,
	[HistoryPasswordDate] [datetime] NULL,
 CONSTRAINT [PK_MUserHistoryPassword] PRIMARY KEY CLUSTERED 
(
	[PK_MUser_HistoryPassword_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
