/****** Object:  Table [dbo].[EmailTemplateAction]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmailTemplateAction](
	[PK_EmailTemplate_Action_ID] [int] IDENTITY(1,1) NOT NULL,
	[FK_EmailTemplate_ID] [int] NULL,
	[FK_EmailActionType_ID] [int] NULL,
	[TSQLtoExecute] [varchar](max) NULL,
 CONSTRAINT [PK_EmailTemplateAction] PRIMARY KEY CLUSTERED 
(
	[PK_EmailTemplate_Action_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
