/****** Object:  Table [dbo].[remoteConnection_Upload]    Script Date: 12/12/2023 11:20:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[remoteConnection_Upload](
	[PK_upload_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[nawa_userid] [varchar](50) NULL,
	[nawa_recordnumber] [bigint] NULL,
	[KeteranganError] [varchar](max) NULL,
	[nawa_Action] [varchar](50) NULL,
	[PK_Conn_ID] [varchar](max) NULL,
	[serverName] [varchar](max) NULL,
	[dbName] [varchar](max) NULL,
	[userName] [varchar](max) NULL,
	[password] [varchar](max) NULL,
	[passwordSalt] [varchar](max) NULL,
	[connectionName] [varchar](max) NULL,
	[connectionType] [varchar](max) NULL,
	[Active] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
