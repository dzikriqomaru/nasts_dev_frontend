/****** Object:  Table [dbo].[ComponenSettingAction_Upload]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ComponenSettingAction_Upload](
	[PK_upload_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[nawa_userid] [varchar](50) NULL,
	[nawa_recordnumber] [bigint] NULL,
	[KeteranganError] [varchar](max) NULL,
	[nawa_Action] [varchar](50) NULL,
	[FieldNameTest] [varchar](max) NULL,
	[Checkbox1Unique] [varchar](max) NULL,
	[Checkbox2Req] [varchar](max) NULL,
	[Checkbox3Req] [varchar](max) NULL,
	[CheckboxFilter] [varchar](max) NULL,
	[CheckboxParent] [varchar](max) NULL,
	[CheckboxHidden] [varchar](max) NULL,
	[CheckboxRead] [varchar](max) NULL,
	[Active] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
