/****** Object:  Table [dbo].[ApprovalDesignerApproval]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApprovalDesignerApproval](
	[PK_ApprovalDesignerApproval_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ApprovalDesignerName] [varchar](250) NULL,
	[ApprovalDesignerKey] [varchar](1000) NULL,
	[ApprovalDesignerField] [text] NULL,
	[ApprovalDesignerFieldBefore] [text] NULL,
	[FK_ModuleAction_ID] [int] NULL,
	[FK_ApprovalDesigner_ID] [int] NULL,
	[FK_MRole_ID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
 CONSTRAINT [PK_ApprovalDesignerApproval] PRIMARY KEY CLUSTERED 
(
	[PK_ApprovalDesignerApproval_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
