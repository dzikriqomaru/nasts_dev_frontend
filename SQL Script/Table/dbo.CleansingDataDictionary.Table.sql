/****** Object:  Table [dbo].[CleansingDataDictionary]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CleansingDataDictionary](
	[PK_CleansingDataDictionary_ID] [int] IDENTITY(1,1) NOT NULL,
	[TableName] [int] NOT NULL,
	[FieldName] [varchar](250) NOT NULL,
	[CleansingName] [varchar](100) NOT NULL,
	[TextToReplace] [varchar](255) NOT NULL,
	[ReplaceWith] [varchar](255) NOT NULL,
	[SortOrder] [int] NULL,
	[NeedConfirmation] [bit] NULL,
	[Active] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[LastUpdateBy] [varchar](50) NULL,
	[ApprovedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[ApprovedDate] [datetime] NULL,
	[Alternateby] [varchar](50) NULL,
 CONSTRAINT [PK_CleansingDataDictionary] PRIMARY KEY CLUSTERED 
(
	[PK_CleansingDataDictionary_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
