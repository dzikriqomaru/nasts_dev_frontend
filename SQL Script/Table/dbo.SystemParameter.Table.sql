/****** Object:  Table [dbo].[SystemParameter]    Script Date: 12/12/2023 11:20:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SystemParameter](
	[PK_SystemParameter_ID] [int] NOT NULL,
	[FK_SystemParameterGroup_ID] [int] NULL,
	[SettingName] [varchar](250) NULL,
	[SettingValue] [varchar](max) NULL,
	[Active] [bit] NULL,
	[Hide] [bit] NULL,
	[fk_MFieldType_ID] [int] NULL,
	[IsEncript] [bit] NULL,
	[EncriptionKey] [varchar](550) NULL,
 CONSTRAINT [PK_ApplicationSetting] PRIMARY KEY CLUSTERED 
(
	[PK_SystemParameter_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
