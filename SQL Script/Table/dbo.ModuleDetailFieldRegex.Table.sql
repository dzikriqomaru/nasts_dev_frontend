/****** Object:  Table [dbo].[ModuleDetailFieldRegex]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ModuleDetailFieldRegex](
	[PK_ModuleDetailFieldRegex] [bigint] IDENTITY(1,1) NOT NULL,
	[FK_ModuleDetailField_ID] [bigint] NULL,
	[Regex] [varchar](250) NULL
) ON [PRIMARY]
GO
