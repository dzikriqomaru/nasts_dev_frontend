/****** Object:  Table [dbo].[CleansingReport]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CleansingReport](
	[PK_Record_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[SegmentData] [varchar](100) NULL,
	[CleansingRules] [varchar](max) NULL,
	[KeyField] [varchar](100) NULL,
	[KeyFieldValue] [varchar](1000) NULL,
	[NamaField] [varchar](255) NULL,
	[OriginalValue] [varchar](max) NULL,
	[CleanedValue] [varchar](max) NULL,
	[Status] [varchar](50) NULL,
	[Active] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[LastUpdateBy] [varchar](50) NULL,
	[ApprovedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[ApprovedDate] [datetime] NULL,
	[UniqueField] [varchar](50) NULL,
	[UniqueValue] [varchar](500) NULL,
	[ModuleID] [varchar](50) NULL,
	[ModuleName] [varchar](50) NULL,
	[Clean] [bit] NULL,
	[Keep] [bit] NULL,
	[FieldName] [varchar](100) NULL,
 CONSTRAINT [PK_CleansingReport] PRIMARY KEY CLUSTERED 
(
	[PK_Record_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
