/****** Object:  Table [dbo].[ApprovalDesignerField]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApprovalDesignerField](
	[PK_ApprovalDesignerField_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[FK_ApprovalDesigner_ID] [int] NULL,
	[FieldName] [varchar](250) NULL,
	[FieldLabel] [varchar](250) NULL,
	[Sequence] [int] NULL,
	[Required] [bit] NULL,
	[IsPrimaryKey] [bit] NULL,
	[IsUnik] [bit] NULL,
	[IsShowInView] [bit] NULL,
	[IsShowInForm] [bit] NULL,
	[DefaultValue] [varchar](max) NULL,
	[FK_FieldType_ID] [int] NULL,
	[SizeField] [int] NULL,
	[FK_ExtType_ID] [int] NULL,
	[TabelReferenceName] [varchar](250) NULL,
	[TabelReferenceNameAlias] [varchar](250) NULL,
	[TableReferenceFieldKey] [varchar](250) NULL,
	[TableReferenceFieldDisplayName] [varchar](250) NULL,
	[TableReferenceFilter] [varchar](1000) NULL,
	[IsUseRegexValidation] [bit] NULL,
	[TableReferenceAdditonalJoin] [varchar](1000) NULL,
	[BCasCade] [bit] NULL,
	[FieldNameParent] [varchar](250) NULL,
	[FilterCascade] [varchar](1000) NULL,
 CONSTRAINT [PK_ApprovalDesignerField] PRIMARY KEY CLUSTERED 
(
	[PK_ApprovalDesignerField_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[ApprovalDesignerField]  WITH NOCHECK ADD  CONSTRAINT [FK_ApprovalDesignerField_ApprovalDesigner] FOREIGN KEY([FK_ApprovalDesigner_ID])
REFERENCES [dbo].[ApprovalDesigner] ([PK_ApprovalDesigner_ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ApprovalDesignerField] CHECK CONSTRAINT [FK_ApprovalDesignerField_ApprovalDesigner]
GO
ALTER TABLE [dbo].[ApprovalDesignerField]  WITH NOCHECK ADD  CONSTRAINT [FK_ApprovalDesignerField_MExtType] FOREIGN KEY([FK_ExtType_ID])
REFERENCES [dbo].[MExtType] ([PK_ExtType_ID])
GO
ALTER TABLE [dbo].[ApprovalDesignerField] CHECK CONSTRAINT [FK_ApprovalDesignerField_MExtType]
GO
ALTER TABLE [dbo].[ApprovalDesignerField]  WITH NOCHECK ADD  CONSTRAINT [FK_ApprovalDesignerField_MFieldType] FOREIGN KEY([FK_FieldType_ID])
REFERENCES [dbo].[MFieldType] ([PK_FieldType_ID])
GO
ALTER TABLE [dbo].[ApprovalDesignerField] CHECK CONSTRAINT [FK_ApprovalDesignerField_MFieldType]
GO
