/****** Object:  Table [dbo].[LogConsoleService]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LogConsoleService](
	[Pk_LogConsoleService_Id] [bigint] IDENTITY(1,1) NOT NULL,
	[LogCreatedDate] [datetime] NULL,
	[LogStatus] [varchar](255) NULL,
	[LogInfo] [varchar](max) NULL,
	[LogDescription] [varchar](max) NULL,
 CONSTRAINT [PK_LogConsoleService] PRIMARY KEY CLUSTERED 
(
	[Pk_LogConsoleService_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
