/****** Object:  Table [dbo].[EODSchedulerLog]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EODSchedulerLog](
	[PK_EODSchedulerLog_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[FK_EODSchedulerID] [bigint] NULL,
	[ExecuteBy] [varchar](250) NULL,
	[DataDate] [datetime] NULL,
	[ProcessDate] [datetime] NULL,
	[StartDate] [datetime] NULL,
	[Enddate] [datetime] NULL,
	[ErrorMessage] [varchar](max) NULL,
	[FK_MsEODStatus_ID] [int] NULL,
	[KodeCabang] [varchar](10) NULL,
 CONSTRAINT [PK_EODSchedulerLog] PRIMARY KEY CLUSTERED 
(
	[PK_EODSchedulerLog_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
