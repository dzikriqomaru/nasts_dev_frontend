/****** Object:  Table [dbo].[usedToken]    Script Date: 12/12/2023 11:20:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[usedToken](
	[PK_UsedToken_ID] [int] IDENTITY(1,1) NOT NULL,
	[FK_MUser_ID] [int] NOT NULL,
	[userID] [varchar](max) NOT NULL,
	[token] [varchar](max) NOT NULL,
	[dateExpired] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[PK_UsedToken_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
