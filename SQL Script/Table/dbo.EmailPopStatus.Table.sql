/****** Object:  Table [dbo].[EmailPopStatus]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmailPopStatus](
	[PK_EmailPopStatus_ID] [int] NOT NULL,
	[EmailPopStatusName] [varchar](50) NULL,
 CONSTRAINT [PK_EmailPopStatus] PRIMARY KEY CLUSTERED 
(
	[PK_EmailPopStatus_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
