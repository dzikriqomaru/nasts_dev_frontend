/****** Object:  Table [dbo].[ModuleDetailApproval]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ModuleDetailApproval](
	[PK_ModuleDetailApproval_ID] [int] IDENTITY(1,1) NOT NULL,
	[ModuleDetailName] [varchar](250) NULL,
	[ModuleDetailKey] [varchar](1000) NULL,
	[ModuleDetailField] [varchar](max) NULL,
	[ModuleDetailFieldBefore] [varchar](max) NULL,
	[FK_ModuleApproval_ID] [bigint] NULL,
	[PK_ModuleAction_ID] [int] NULL,
	[Active] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[LastUpdateBy] [varchar](50) NULL,
	[ApprovedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[ApprovedDate] [datetime] NULL,
	[Alternateby] [varchar](50) NULL,
	[FK_ModuleDetail_ID] [int] NULL,
 CONSTRAINT [PK_ModuleDetailApproval] PRIMARY KEY CLUSTERED 
(
	[PK_ModuleDetailApproval_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
