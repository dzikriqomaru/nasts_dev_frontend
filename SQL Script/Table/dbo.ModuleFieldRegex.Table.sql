/****** Object:  Table [dbo].[ModuleFieldRegex]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ModuleFieldRegex](
	[PK_ModuleFieldRegex] [bigint] IDENTITY(1,1) NOT NULL,
	[FK_ModuleField_ID] [bigint] NULL,
	[Regex] [varchar](250) NULL,
 CONSTRAINT [PK_ModuleFieldRegex] PRIMARY KEY CLUSTERED 
(
	[PK_ModuleFieldRegex] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ModuleFieldRegex]  WITH NOCHECK ADD  CONSTRAINT [FK_ModuleFieldRegex_ModuleFieldRegex] FOREIGN KEY([FK_ModuleField_ID])
REFERENCES [dbo].[ModuleField] ([PK_ModuleField_ID])
GO
ALTER TABLE [dbo].[ModuleFieldRegex] CHECK CONSTRAINT [FK_ModuleFieldRegex_ModuleFieldRegex]
GO
