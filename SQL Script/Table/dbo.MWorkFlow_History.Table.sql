/****** Object:  Table [dbo].[MWorkFlow_History]    Script Date: 12/12/2023 11:13:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MWorkFlow_History](
	[PK_MWorkflow_History_ID] [int] IDENTITY(1,1) NOT NULL,
	[FK_Module_ID] [int] NULL,
	[FK_Unik_ID] [varchar](250) NULL,
	[FK_MUserId] [int] NULL,
	[FK_MRoleId] [int] NULL,
	[intLevel] [int] NULL,
	[RoleName] [varchar](250) NULL,
	[UserName] [varchar](250) NULL,
	[UserNameExecute] [varchar](250) NULL,
	[CreatedDate_Basic] [datetime] NULL,
	[ResponseDate] [datetime] NULL,
	[FK_MWorkflow_ApprovalStatus_ID] [int] NULL,
	[Notes] [varchar](max) NULL,
	[Active] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[LastUpdateBy] [varchar](50) NULL,
	[ApprovedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[ApprovedDate] [datetime] NULL,
	[Alternateby] [varchar](50) NULL,
	[FK_ModuleApproval_ID] [bigint] NULL,
 CONSTRAINT [PK_MWorkFlow_History] PRIMARY KEY CLUSTERED 
(
	[PK_MWorkflow_History_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
