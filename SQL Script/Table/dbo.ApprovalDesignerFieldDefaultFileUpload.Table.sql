/****** Object:  Table [dbo].[ApprovalDesignerFieldDefaultFileUpload]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApprovalDesignerFieldDefaultFileUpload](
	[PK_ApprovalDesignerField_ID] [bigint] NOT NULL,
	[FieldName] [varchar](250) NULL,
	[FieldLabel] [varchar](250) NULL,
	[Sequence] [int] NULL,
	[Required] [bit] NULL,
	[IsPrimaryKey] [bit] NULL,
	[IsUnik] [bit] NULL,
	[FK_FieldType_ID] [int] NULL,
	[SizeField] [int] NULL,
	[FK_ExtType_ID] [int] NULL
) ON [PRIMARY]
GO
