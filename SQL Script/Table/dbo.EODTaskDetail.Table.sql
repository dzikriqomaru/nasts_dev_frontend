/****** Object:  Table [dbo].[EODTaskDetail]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EODTaskDetail](
	[PK_EODTaskDetail_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[FK_EODTask_ID] [bigint] NULL,
	[FK_EODTaskDetailType_ID] [int] NULL,
	[OrderNo] [int] NULL,
	[SSISName] [varchar](250) NULL,
	[SSISFIle] [varbinary](max) NULL,
	[StoreProcedureName] [varchar](250) NULL,
	[Keterangan] [varchar](8000) NULL,
	[IsUseParameterProcessDate] [bit] NULL,
	[IsUseParameterBranch] [bit] NULL,
	[Argument] [varchar](255) NULL,
	[Region] [varchar](50) NULL,
 CONSTRAINT [PK_EODSSIS_1] PRIMARY KEY CLUSTERED 
(
	[PK_EODTaskDetail_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
