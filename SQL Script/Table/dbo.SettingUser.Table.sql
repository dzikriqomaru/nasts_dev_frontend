/****** Object:  Table [dbo].[SettingUser]    Script Date: 12/12/2023 11:20:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SettingUser](
	[PK_Setting_ID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [varchar](200) NULL,
	[SettingName] [varchar](200) NULL,
	[SettingValue] [varchar](200) NULL,
PRIMARY KEY CLUSTERED 
(
	[PK_Setting_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
