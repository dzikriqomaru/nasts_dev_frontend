/****** Object:  Table [dbo].[MWorkFlow_History_Upload]    Script Date: 12/12/2023 11:13:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MWorkFlow_History_Upload](
	[PK_upload_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[nawa_userid] [varchar](50) NULL,
	[nawa_recordnumber] [bigint] NULL,
	[KeteranganError] [varchar](max) NULL,
	[nawa_Action] [varchar](50) NULL,
	[PK_MWorkflow_History_ID] [varchar](max) NULL,
	[FK_Module_ID] [varchar](max) NULL,
	[FK_Unik_ID] [varchar](max) NULL,
	[FK_MUserId] [varchar](max) NULL,
	[FK_MRoleId] [varchar](max) NULL,
	[intLevel] [varchar](max) NULL,
	[RoleName] [varchar](max) NULL,
	[UserName] [varchar](max) NULL,
	[UserNameExecute] [varchar](max) NULL,
	[CreatedDate_Basic] [varchar](max) NULL,
	[ResponseDate] [varchar](max) NULL,
	[FK_MWorkflow_ApprovalStatus_ID] [varchar](max) NULL,
	[Notes] [varchar](max) NULL,
	[Active] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
