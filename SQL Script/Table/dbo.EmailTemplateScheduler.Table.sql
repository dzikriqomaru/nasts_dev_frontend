/****** Object:  Table [dbo].[EmailTemplateScheduler]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmailTemplateScheduler](
	[PK_EmailTemplateScheduler_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[PK_EmailTemplate_ID] [bigint] NULL,
	[ProcessDate] [datetime] NULL,
	[FK_EmailStatus_ID] [int] NULL,
 CONSTRAINT [PK_EmailTemplateScheduler] PRIMARY KEY CLUSTERED 
(
	[PK_EmailTemplateScheduler_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
