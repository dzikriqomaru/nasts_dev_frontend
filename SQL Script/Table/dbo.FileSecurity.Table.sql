/****** Object:  Table [dbo].[FileSecurity]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FileSecurity](
	[FileSecurityID] [int] IDENTITY(1,1) NOT NULL,
	[FileSecurityGroupId] [int] NULL,
	[FileSecurityFileName] [varchar](255) NULL,
 CONSTRAINT [PK_FileSecurity] PRIMARY KEY CLUSTERED 
(
	[FileSecurityID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[FileSecurity]  WITH NOCHECK ADD  CONSTRAINT [FK_FileSecurity_MGroupMenu] FOREIGN KEY([FileSecurityGroupId])
REFERENCES [dbo].[MGroupMenu] ([PK_MGroupMenu_ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[FileSecurity] CHECK CONSTRAINT [FK_FileSecurity_MGroupMenu]
GO
