/****** Object:  Table [dbo].[Web_ValidationReport_RowComplate]    Script Date: 12/12/2023 11:20:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Web_ValidationReport_RowComplate](
	[PK_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[TanggalData] [datetime] NULL,
	[SegmentData] [varchar](100) NOT NULL,
	[RecordID] [varchar](50) NULL,
	[ModuleURL] [varchar](200) NULL,
	[ModuleID] [int] NULL,
	[KeyFieldValue] [varchar](1000) NULL,
	[KeyField] [varchar](100) NULL,
	[Messagedetail] [nvarchar](max) NOT NULL,
	[Edited] [bit] NULL,
	[KodeCabang] [varchar](6) NULL,
	[MessageDetailView] [varchar](max) NULL,
	[ReferenceField] [varchar](100) NULL,
	[ReferenceValue] [varchar](100) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
