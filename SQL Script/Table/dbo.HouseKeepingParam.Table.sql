/****** Object:  Table [dbo].[HouseKeepingParam]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HouseKeepingParam](
	[PK_House_Keeping_Param_ID] [int] IDENTITY(1,1) NOT NULL,
	[scrTable] [varchar](255) NOT NULL,
	[destTable] [varchar](255) NOT NULL,
	[FieldName] [varchar](255) NOT NULL,
	[PeriodType] [int] NOT NULL,
	[RetentionPeriod] [smallint] NOT NULL,
	[Active] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[LastUpdateBy] [varchar](50) NULL,
	[ApprovedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[ApprovedDate] [datetime] NULL,
	[Alternateby] [varchar](50) NULL,
 CONSTRAINT [PK_HouseKeepingParam] PRIMARY KEY CLUSTERED 
(
	[PK_House_Keeping_Param_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
