/****** Object:  Table [dbo].[ValidationParameter]    Script Date: 12/12/2023 11:20:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ValidationParameter](
	[PK_ValidationParameter] [int] IDENTITY(1,1) NOT NULL,
	[TableName] [int] NOT NULL,
	[FieldName] [varchar](250) NOT NULL,
	[ExpressionType] [varchar](50) NOT NULL,
	[ValidationExpression] [varchar](8000) NULL,
	[Description] [varchar](200) NULL,
	[ValidationMessage] [varchar](1000) NOT NULL,
	[ValidationType] [int] NOT NULL,
	[ErrorType] [varchar](50) NOT NULL,
	[Active] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[LastUpdateBy] [varchar](50) NULL,
	[ApprovedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[ApprovedDate] [datetime] NULL,
	[Alternateby] [varchar](50) NULL,
 CONSTRAINT [PK_ValidationParameter] PRIMARY KEY CLUSTERED 
(
	[PK_ValidationParameter] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
