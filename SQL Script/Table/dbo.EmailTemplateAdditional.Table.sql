/****** Object:  Table [dbo].[EmailTemplateAdditional]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmailTemplateAdditional](
	[PK_EmailTemplateAdditional_ID] [int] IDENTITY(1,1) NOT NULL,
	[FK_EmailTemplate_ID] [int] NULL,
	[FK_EmailTableType_ID] [int] NULL,
	[NamaTable] [varchar](250) NULL,
	[QueryData] [varchar](max) NULL,
	[QueryDataDesigner] [varchar](max) NULL,
	[FieldUnikPrimaryTable] [varchar](250) NULL,
 CONSTRAINT [PK_EmailTemplateAdditional] PRIMARY KEY CLUSTERED 
(
	[PK_EmailTemplateAdditional_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
