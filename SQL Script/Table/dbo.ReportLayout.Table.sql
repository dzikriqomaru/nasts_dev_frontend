/****** Object:  Table [dbo].[ReportLayout]    Script Date: 12/12/2023 11:20:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReportLayout](
	[PK_Report_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[DisplayName] [varchar](max) NULL,
	[LayoutData] [varbinary](max) NULL,
	[Active] [bit] NULL,
	[CreatedBy] [varchar](max) NULL,
	[CreatedDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
