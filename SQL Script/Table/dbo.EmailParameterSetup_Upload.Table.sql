/****** Object:  Table [dbo].[EmailParameterSetup_Upload]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmailParameterSetup_Upload](
	[PK_upload_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[nawa_userid] [varchar](50) NULL,
	[nawa_recordnumber] [bigint] NULL,
	[KeteranganError] [varchar](max) NULL,
	[nawa_Action] [varchar](50) NULL,
	[Pk_EmailParameterSetup_Id] [varchar](max) NULL,
	[ParameterName] [varchar](max) NULL,
	[EmailAddressList] [varchar](max) NULL,
	[Active] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
