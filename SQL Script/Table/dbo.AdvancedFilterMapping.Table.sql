/****** Object:  Table [dbo].[AdvancedFilterMapping]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AdvancedFilterMapping](
	[PK_AdvancedFilterMapping_ID] [int] IDENTITY(1,1) NOT NULL,
	[FK_AdvancedFilterType_ID] [int] NOT NULL,
	[FK_AdvanceDataType_ID] [int] NOT NULL,
	[FK_FieldType_ID] [int] NULL,
	[Active] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[LastUpdateBy] [varchar](50) NULL,
	[ApprovedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[ApprovedDate] [datetime] NULL,
	[Alternateby] [varchar](50) NULL,
	[FK_SqlDataType] [varchar](200) NULL,
 CONSTRAINT [PK_AdvancedFilterMapping] PRIMARY KEY CLUSTERED 
(
	[PK_AdvancedFilterMapping_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
