/****** Object:  Table [dbo].[AllowedFileModuleDetail]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AllowedFileModuleDetail](
	[PK_Allowed_File_Module_Detail] [int] IDENTITY(1,1) NOT NULL,
	[Module_Name] [varchar](250) NULL,
	[Module_Detail_Name] [varchar](250) NULL,
	[FK_Allowed_FileType] [int] NULL,
	[FieldName] [varchar](250) NULL,
PRIMARY KEY CLUSTERED 
(
	[PK_Allowed_File_Module_Detail] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
