/****** Object:  Table [dbo].[MGroupMenu]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MGroupMenu](
	[PK_MGroupMenu_ID] [int] IDENTITY(1,1) NOT NULL,
	[GroupMenuName] [varchar](250) NOT NULL,
	[GroupMenuDesciption] [varchar](500) NULL,
	[Active] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[LastUpdateBy] [varchar](50) NULL,
	[ApprovedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[ApprovedDate] [datetime] NULL,
	[Alternateby] [varchar](50) NULL,
	[DataMenu] [varchar](max) NULL,
	[DataMenuMobile] [varchar](max) NULL,
 CONSTRAINT [PK_MGroupMenu] PRIMARY KEY CLUSTERED 
(
	[PK_MGroupMenu_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[MGroupMenu] ADD  CONSTRAINT [DF_MGroupMenu_GroupMenuName]  DEFAULT ('') FOR [GroupMenuName]
GO
