/****** Object:  Table [dbo].[MFieldType]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MFieldType](
	[PK_FieldType_ID] [int] IDENTITY(1,1) NOT NULL,
	[FieldTypeCaption] [varchar](50) NOT NULL,
	[FieldTypeDescription] [varchar](255) NULL,
	[FieldTypeSQLName] [varchar](50) NOT NULL,
	[FieldTypeNetName] [varchar](50) NULL,
	[CodeEffecttype] [varchar](50) NULL,
	[CodeEffectNETType] [varchar](50) NULL,
 CONSTRAINT [PK_MsFieldType] PRIMARY KEY CLUSTERED 
(
	[PK_FieldType_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
