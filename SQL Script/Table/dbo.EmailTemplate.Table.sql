/****** Object:  Table [dbo].[EmailTemplate]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmailTemplate](
	[PK_EmailTemplate_ID] [int] IDENTITY(1,1) NOT NULL,
	[EmailTemplateName] [varchar](550) NULL,
	[EmailTo] [varchar](1000) NULL,
	[EmailCC] [varchar](1000) NULL,
	[EmailBCC] [varchar](1000) NULL,
	[EmailSubject] [varchar](1000) NULL,
	[EmailBody] [varchar](max) NULL,
	[FK_Monitoringduration_ID] [int] NULL,
	[StartDate] [datetime] NULL,
	[StartTime] [varchar](50) NULL,
	[ExcludeHoliday] [bit] NULL,
	[IsSupportEmail] [bit] NULL,
	[IsSupportNotification] [bit] NULL,
	[Active] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[LastUpdateBy] [varchar](50) NULL,
	[ApprovedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[ApprovedDate] [datetime] NULL,
	[FK_EmailTemplateImportance_ID] [int] NOT NULL,
	[FK_EmailTemplateSensitivity_ID] [int] NOT NULL,
	[DeliveryReceipt] [bit] NULL,
	[ReadReceipt] [bit] NULL,
 CONSTRAINT [PK_EmailTemplate] PRIMARY KEY CLUSTERED 
(
	[PK_EmailTemplate_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[EmailTemplate] ADD  DEFAULT ((2)) FOR [FK_EmailTemplateImportance_ID]
GO
ALTER TABLE [dbo].[EmailTemplate] ADD  DEFAULT ((1)) FOR [FK_EmailTemplateSensitivity_ID]
GO
ALTER TABLE [dbo].[EmailTemplate]  WITH NOCHECK ADD  CONSTRAINT [FK_EmailTemplate_MonitoringDuration] FOREIGN KEY([FK_Monitoringduration_ID])
REFERENCES [dbo].[MonitoringDuration] ([PK_MonitoringDuration_Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[EmailTemplate] CHECK CONSTRAINT [FK_EmailTemplate_MonitoringDuration]
GO
