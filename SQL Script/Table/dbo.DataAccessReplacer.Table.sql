/****** Object:  Table [dbo].[DataAccessReplacer]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DataAccessReplacer](
	[PK_Replacer_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Replacer_Name] [varchar](250) NOT NULL,
	[Replacer_Label] [varchar](250) NOT NULL,
	[FK_FieldType_ID] [int] NOT NULL,
	[Active] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[LastUpdateBy] [varchar](50) NULL,
	[ApprovedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[ApprovedDate] [datetime] NULL,
	[Alternateby] [varchar](50) NULL,
	[FK_ExtType_ID] [int] NULL,
	[TableReferenceName] [varchar](250) NULL,
	[TableReferenceAlias] [varchar](250) NULL,
	[TableReferenceKey] [varchar](250) NULL,
	[TableReferenceDisplayName] [varchar](250) NULL,
 CONSTRAINT [PK_DataAccessReplacer] PRIMARY KEY CLUSTERED 
(
	[PK_Replacer_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
