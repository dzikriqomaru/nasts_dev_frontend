/****** Object:  Table [dbo].[EmailTemplateImportance]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmailTemplateImportance](
	[PK_EmailTemplateImportance_ID] [int] NOT NULL,
	[ImportanceDescription] [varchar](50) NOT NULL
) ON [PRIMARY]
GO
