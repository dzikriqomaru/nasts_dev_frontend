/****** Object:  Table [dbo].[MWorkFlowDetail]    Script Date: 12/12/2023 11:13:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MWorkFlowDetail](
	[PK_WorkFlowDetail] [int] IDENTITY(1,1) NOT NULL,
	[FK_WorkFlow] [int] NOT NULL,
	[RoleID] [int] NOT NULL,
	[UserType] [int] NOT NULL,
	[Level] [int] NOT NULL,
	[SLAType] [int] NOT NULL,
	[SLAValue] [int] NOT NULL,
	[BreachSLAAction] [int] NOT NULL,
	[BreachSLAActionTo] [int] NOT NULL,
	[RevisedActionTo] [int] NOT NULL,
	[Active] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[LastUpdateBy] [varchar](50) NULL,
	[ApprovedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[ApprovedDate] [datetime] NULL,
	[Alternateby] [varchar](50) NULL,
 CONSTRAINT [PK_MWorkFlowDetail] PRIMARY KEY CLUSTERED 
(
	[PK_WorkFlowDetail] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
