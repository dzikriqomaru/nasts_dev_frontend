/****** Object:  Table [dbo].[ValidationReportPerInformation]    Script Date: 12/12/2023 11:20:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ValidationReportPerInformation](
	[PK_ValidationReportPerInformation] [int] IDENTITY(1,1) NOT NULL,
	[InformationName] [varchar](255) NOT NULL,
	[ValidationMessage] [varchar](8000) NOT NULL,
	[ModuleURL] [varchar](255) NULL,
	[ModuleID] [int] NULL,
	[FK_ValidationType] [int] NULL,
	[ErrorType] [varchar](50) NULL,
	[TanggalData] [datetime] NULL,
	[KodeCabang] [varchar](6) NULL,
	[FK_DataSet] [int] NULL,
	[FK_KategoriValidasi] [int] NULL,
	[Active] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[LastUpdateBy] [varchar](50) NULL,
	[ApprovedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[ApprovedDate] [datetime] NULL,
	[Alternateby] [varchar](50) NULL,
 CONSTRAINT [PK_ValidationReportPerInformation] PRIMARY KEY CLUSTERED 
(
	[PK_ValidationReportPerInformation] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
