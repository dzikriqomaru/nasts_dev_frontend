/****** Object:  Table [dbo].[EODScheduler]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EODScheduler](
	[PK_EODScheduler_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[EODSchedulerName] [varchar](255) NULL,
	[EODSchedulerDescription] [varchar](8000) NULL,
	[HasPeriodikScheduler] [bit] NULL,
	[EODPeriod] [int] NULL,
	[FK_MsEODPeriod] [int] NULL,
	[StartDate] [datetime] NULL,
	[Active] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[LastUpdateBy] [varchar](50) NULL,
	[ApprovedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[ApprovedDate] [datetime] NULL,
	[StartTime] [varchar](50) NULL,
 CONSTRAINT [PK_EODScheduler] PRIMARY KEY CLUSTERED 
(
	[PK_EODScheduler_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
