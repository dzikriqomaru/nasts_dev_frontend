/****** Object:  Table [dbo].[LogConsoleWeb]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LogConsoleWeb](
	[PK_LogConsoleWeb_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Log_Page] [varchar](500) NULL,
	[Log_Function] [varchar](500) NULL,
	[Log_Data] [varchar](max) NULL,
	[Log_Status] [varchar](500) NULL,
	[Log_Message] [varchar](max) NULL,
	[Log_Description] [varchar](max) NULL,
	[Active] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[LastUpdateBy] [varchar](50) NULL,
	[ApprovedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[ApprovedDate] [datetime] NULL,
	[Alternateby] [varchar](50) NULL,
 CONSTRAINT [PK_LogConsoleWeb] PRIMARY KEY CLUSTERED 
(
	[PK_LogConsoleWeb_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
