/****** Object:  Table [dbo].[BackgroundProcess_Upload]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BackgroundProcess_Upload](
	[PK_upload_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[nawa_userid] [varchar](50) NULL,
	[nawa_recordnumber] [bigint] NULL,
	[KeteranganError] [varchar](max) NULL,
	[nawa_Action] [varchar](50) NULL,
	[PK_BackgroundProcess_ID] [varchar](max) NULL,
	[FK_Module_ID] [varchar](max) NULL,
	[FK_ProcessType_ID] [varchar](max) NULL,
	[FK_Status_ID] [varchar](max) NULL,
	[Message] [varchar](max) NULL,
	[Data] [varchar](max) NULL,
	[Active] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
