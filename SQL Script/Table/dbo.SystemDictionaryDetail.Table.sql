/****** Object:  Table [dbo].[SystemDictionaryDetail]    Script Date: 12/12/2023 11:20:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SystemDictionaryDetail](
	[PK_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Keywords] [varchar](255) NULL,
	[ParamName] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[PK_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
