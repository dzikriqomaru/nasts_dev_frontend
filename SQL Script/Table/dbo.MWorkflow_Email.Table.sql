/****** Object:  Table [dbo].[MWorkflow_Email]    Script Date: 12/12/2023 11:13:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MWorkflow_Email](
	[PK_MWorkflow_Email_ID] [int] IDENTITY(1,1) NOT NULL,
	[FK_Module_ID] [int] NULL,
	[FK_Unik_ID] [varchar](250) NULL,
	[UserID] [varchar](50) NULL,
	[EmailAddress] [varchar](250) NULL,
	[IsSendEmail] [bit] NULL,
	[FK_MWorkflow_ApprovalStatus_ID] [int] NULL,
	[UserName] [varchar](250) NULL,
	[FK_ModuleApproval_ID] [bigint] NULL,
	[Active] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[LastUpdateBy] [varchar](50) NULL,
	[ApprovedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[ApprovedDate] [datetime] NULL,
	[Alternateby] [varchar](50) NULL,
 CONSTRAINT [PK_MWorkflow_Email] PRIMARY KEY CLUSTERED 
(
	[PK_MWorkflow_Email_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
