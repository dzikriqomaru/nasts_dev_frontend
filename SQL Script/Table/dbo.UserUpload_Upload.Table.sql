/****** Object:  Table [dbo].[UserUpload_Upload]    Script Date: 12/12/2023 11:20:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserUpload_Upload](
	[PK_upload_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[nawa_userid] [varchar](50) NULL,
	[nawa_recordnumber] [bigint] NULL,
	[KeteranganError] [varchar](max) NULL,
	[nawa_Action] [varchar](50) NULL,
	[UserID] [varchar](max) NULL,
	[Nama] [varchar](max) NULL,
	[FK_Role_ID] [varchar](max) NULL,
	[FK_MGroupMenu_ID] [varchar](max) NULL,
	[EmailAddress] [varchar](max) NULL,
	[Active] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
