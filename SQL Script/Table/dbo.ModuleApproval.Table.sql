/****** Object:  Table [dbo].[ModuleApproval]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ModuleApproval](
	[PK_ModuleApproval_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ModuleName] [varchar](250) NULL,
	[ModuleKey] [varchar](1000) NULL,
	[ModuleField] [varchar](max) NULL,
	[ModuleFieldBefore] [varchar](max) NULL,
	[PK_ModuleAction_ID] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[FK_Module_ID] [int] NULL,
	[FK_MRole_ID] [int] NULL,
 CONSTRAINT [PK_ModuleApproval] PRIMARY KEY CLUSTERED 
(
	[PK_ModuleApproval_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
