/****** Object:  Table [dbo].[MNotification_Upload]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MNotification_Upload](
	[PK_upload_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[nawa_userid] [varchar](50) NULL,
	[nawa_recordnumber] [bigint] NULL,
	[KeteranganError] [varchar](max) NULL,
	[nawa_Action] [varchar](50) NULL,
	[PK_Notification_ID] [varchar](max) NULL,
	[FromUserID] [varchar](max) NULL,
	[ToUserID] [varchar](max) NULL,
	[NotificationHeader] [varchar](max) NULL,
	[NotificationBody] [varchar](max) NULL,
	[IsRead] [varchar](max) NULL,
	[DirectPage] [varchar](max) NULL,
	[FK_Module_ID] [varchar](max) NULL,
	[FK_ModuleAction_ID] [varchar](max) NULL,
	[FK_ModuleApproval_ID] [varchar](max) NULL,
	[FK_NotificationType_ID] [varchar](max) NULL,
	[Active] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
