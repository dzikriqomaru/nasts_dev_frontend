/****** Object:  Table [dbo].[ModuleFieldDefault]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ModuleFieldDefault](
	[PK_ModuleField_ID] [bigint] NOT NULL,
	[FieldName] [varchar](250) NULL,
	[FieldLabel] [varchar](250) NULL,
	[Sequence] [int] NULL,
	[Required] [bit] NULL,
	[IsPrimaryKey] [bit] NULL,
	[IsUnik] [bit] NULL,
	[FK_FieldType_ID] [int] NULL,
	[SizeField] [int] NULL,
	[FK_ExtType_ID] [int] NULL,
 CONSTRAINT [PK_ModuleFieldDefault] PRIMARY KEY CLUSTERED 
(
	[PK_ModuleField_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
