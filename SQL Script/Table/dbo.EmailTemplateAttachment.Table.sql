/****** Object:  Table [dbo].[EmailTemplateAttachment]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmailTemplateAttachment](
	[PK_EmailTemplateAttachment_ID] [int] IDENTITY(1,1) NOT NULL,
	[FK_EmailTemplate_ID] [int] NULL,
	[FK_EmailAttachmentType_ID] [int] NULL,
	[NamaReport] [varchar](1000) NULL,
	[ParameterReport] [varchar](1000) NULL,
	[NamaFile] [varchar](250) NULL,
	[IsiFile] [varbinary](max) NULL,
	[FK_EmailRenderAs_Id] [int] NULL,
	[FileType] [varchar](max) NULL,
	[UseAttachment] [varchar](max) NULL,
 CONSTRAINT [PK_EmailTemplateAttachment] PRIMARY KEY CLUSTERED 
(
	[PK_EmailTemplateAttachment_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
