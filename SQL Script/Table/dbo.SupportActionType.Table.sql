/****** Object:  Table [dbo].[SupportActionType]    Script Date: 12/12/2023 11:20:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SupportActionType](
	[PK_SupportActionType_ID] [int] IDENTITY(1,1) NOT NULL,
	[SupportActionTypeName] [varchar](250) NULL,
	[SupportActionTypeDescription] [varchar](550) NULL,
 CONSTRAINT [PK_SupportActionType] PRIMARY KEY CLUSTERED 
(
	[PK_SupportActionType_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
