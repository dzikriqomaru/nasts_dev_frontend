/****** Object:  Table [dbo].[SMSTemplateAttachment]    Script Date: 12/12/2023 11:20:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SMSTemplateAttachment](
	[PK_SMSTemplateAttachment_ID] [int] IDENTITY(1,1) NOT NULL,
	[FK_SMSTemplate_ID] [int] NULL,
	[FK_SMSAttachmentType_ID] [int] NULL,
	[NamaReport] [varchar](1000) NULL,
	[ParameterReport] [varchar](1000) NULL,
	[NamaFile] [varchar](250) NULL,
	[IsiFile] [varbinary](max) NULL,
	[FK_SMSRenderAs_Id] [int] NULL,
 CONSTRAINT [PK_SMSTemplateAttachment] PRIMARY KEY CLUSTERED 
(
	[PK_SMSTemplateAttachment_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
