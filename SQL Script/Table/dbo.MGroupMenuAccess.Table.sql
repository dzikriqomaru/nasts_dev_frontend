/****** Object:  Table [dbo].[MGroupMenuAccess]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MGroupMenuAccess](
	[PK_MGroupMenuAcess_ID] [int] IDENTITY(1,1) NOT NULL,
	[FK_GroupMenu_ID] [int] NULL,
	[FK_Module_ID] [int] NULL,
	[bAdd] [bit] NULL,
	[bEdit] [bit] NULL,
	[bDelete] [bit] NULL,
	[bActivation] [bit] NULL,
	[bView] [bit] NULL,
	[bApproval] [bit] NULL,
	[bUpload] [bit] NULL,
	[bDetail] [bit] NULL,
 CONSTRAINT [PK_MGroupMenuAccess] PRIMARY KEY CLUSTERED 
(
	[PK_MGroupMenuAcess_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
