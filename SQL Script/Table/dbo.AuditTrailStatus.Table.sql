/****** Object:  Table [dbo].[AuditTrailStatus]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AuditTrailStatus](
	[Pk_AuditTrailStatus_ID] [int] IDENTITY(1,1) NOT NULL,
	[AuditTrailStatusName] [varchar](100) NULL,
 CONSTRAINT [Pk_AuditTrailStatus] PRIMARY KEY CLUSTERED 
(
	[Pk_AuditTrailStatus_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
