/****** Object:  Table [dbo].[EmailTemplateSchedulerDetailAttachment]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmailTemplateSchedulerDetailAttachment](
	[PK_EmailTemplateSchedulerDetailAttachment_Id] [bigint] IDENTITY(1,1) NOT NULL,
	[IsiFile] [varbinary](max) NULL,
	[FileName] [varchar](350) NULL,
	[FK_EmailTEmplateSchedulerDetail_ID] [bigint] NULL,
	[FK_EmailAttachmentType_ID] [int] NULL,
	[NamaReport] [varchar](1000) NULL,
	[ParameterReport] [varchar](1000) NULL,
	[EmailRenderAsName] [varchar](50) NULL,
	[FileExtension] [varchar](10) NULL,
	[MimeType] [varchar](250) NULL,
	[FK_EmailGenerateAttachmentStatus_ID] [int] NULL,
	[FileType] [varchar](250) NULL,
 CONSTRAINT [PK_EmailTemplateSchedulerDetailAttachment] PRIMARY KEY CLUSTERED 
(
	[PK_EmailTemplateSchedulerDetailAttachment_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
