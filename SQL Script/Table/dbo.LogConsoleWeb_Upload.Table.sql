/****** Object:  Table [dbo].[LogConsoleWeb_Upload]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LogConsoleWeb_Upload](
	[PK_upload_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[nawa_userid] [varchar](50) NULL,
	[nawa_recordnumber] [bigint] NULL,
	[KeteranganError] [varchar](max) NULL,
	[nawa_Action] [varchar](50) NULL,
	[PK_LogConsoleWeb_ID] [varchar](max) NULL,
	[Log_Page] [varchar](max) NULL,
	[Log_Function] [varchar](max) NULL,
	[Log_Data] [varchar](max) NULL,
	[Log_Status] [varchar](max) NULL,
	[Log_Message] [varchar](max) NULL,
	[Log_Description] [varchar](max) NULL,
	[Active] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
