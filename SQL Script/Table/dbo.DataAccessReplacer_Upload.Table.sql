/****** Object:  Table [dbo].[DataAccessReplacer_Upload]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DataAccessReplacer_Upload](
	[PK_upload_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[nawa_userid] [varchar](50) NULL,
	[nawa_recordnumber] [bigint] NULL,
	[KeteranganError] [varchar](max) NULL,
	[nawa_Action] [varchar](50) NULL,
	[PK_Replacer_ID] [varchar](max) NULL,
	[Replacer_Name] [varchar](max) NULL,
	[Replacer_Label] [varchar](max) NULL,
	[FK_FieldType_ID] [varchar](max) NULL,
	[FK_ExtType_ID] [varchar](max) NULL,
	[TableReferenceName] [varchar](max) NULL,
	[TableReferenceAlias] [varchar](max) NULL,
	[TableReferenceKey] [varchar](max) NULL,
	[TableReferenceDisplayName] [varchar](max) NULL,
	[Active] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
