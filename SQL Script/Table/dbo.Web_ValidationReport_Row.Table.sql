/****** Object:  Table [dbo].[Web_ValidationReport_Row]    Script Date: 12/12/2023 11:20:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Web_ValidationReport_Row](
	[TanggalData] [datetime] NULL,
	[SegmentData] [varchar](100) NOT NULL,
	[RecordID] [varchar](50) NULL,
	[ModuleURL] [varchar](200) NULL,
	[ModuleID] [int] NULL,
	[KeyFieldValue] [varchar](1000) NULL,
	[KeyField] [varchar](100) NULL,
	[KodeCabang] [varchar](6) NULL,
	[ReferenceField] [varchar](100) NULL,
	[ReferenceValue] [varchar](100) NULL
) ON [PRIMARY]
GO
