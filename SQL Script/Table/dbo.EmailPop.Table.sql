/****** Object:  Table [dbo].[EmailPop]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmailPop](
	[PK_EmailPop_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[EmailFrom] [varchar](1000) NULL,
	[EmailTo] [varchar](1000) NULL,
	[EmailCC] [varchar](1000) NULL,
	[EmailBCC] [varchar](1000) NULL,
	[EmailSubject] [varchar](1000) NULL,
	[EmailBody] [varchar](max) NULL,
	[ProcessDate] [datetime] NULL,
	[EmailDate] [datetime] NULL,
	[ErrorMessage] [varchar](max) NULL,
	[EmailID] [varchar](250) NULL,
	[EmailRaw] [varbinary](max) NULL,
	[FK_EmailPopStatus_ID] [int] NULL,
	[CreatedBy] [varchar](50) NULL,
	[LastUpdateBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
 CONSTRAINT [PK_EmailPop] PRIMARY KEY CLUSTERED 
(
	[PK_EmailPop_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
