/****** Object:  Table [dbo].[EmailLog]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmailLog](
	[PK_EmailLog_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[FK_EmailTemplate_ID] [int] NULL,
	[UserId] [varchar](50) NULL,
	[EmailTo] [varchar](255) NULL,
	[EmailCC] [varchar](255) NULL,
	[EmailBCC] [varchar](255) NULL,
	[EmailSubject] [varchar](500) NULL,
	[EmailBody] [varchar](max) NULL,
	[FK_EmailStatus] [int] NULL,
	[ErrorMessage] [varchar](max) NULL,
	[ProcessDate] [datetime] NULL,
	[SendDate] [datetime] NULL,
 CONSTRAINT [PK_EmailLog] PRIMARY KEY CLUSTERED 
(
	[PK_EmailLog_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
