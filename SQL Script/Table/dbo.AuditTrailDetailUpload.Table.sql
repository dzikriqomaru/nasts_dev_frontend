/****** Object:  Table [dbo].[AuditTrailDetailUpload]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AuditTrailDetailUpload](
	[PK_AuditTrailDetailUpload_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[FK_AuditTrailHeader_ID] [bigint] NULL,
	[FieldName] [varchar](500) NULL,
	[OldValue] [varchar](max) NULL,
	[NewValue] [varchar](max) NULL,
	[PrimarykeyData] [varchar](8000) NULL,
	[nawa_action] [varchar](50) NULL,
	[FK_Module_ID] [bigint] NULL,
	[UserID] [varchar](50) NULL,
 CONSTRAINT [PK_AuditTrailDetailUpload] PRIMARY KEY CLUSTERED 
(
	[PK_AuditTrailDetailUpload_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
