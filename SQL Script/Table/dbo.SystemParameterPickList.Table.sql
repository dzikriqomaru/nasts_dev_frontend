/****** Object:  Table [dbo].[SystemParameterPickList]    Script Date: 12/12/2023 11:20:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SystemParameterPickList](
	[Pk_ApplicationAuthentication_ID] [int] NOT NULL,
	[ApplicationAuthentication] [varchar](50) NULL,
	[SettingName] [varchar](50) NULL
) ON [PRIMARY]
GO
