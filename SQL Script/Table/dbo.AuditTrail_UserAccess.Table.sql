/****** Object:  Table [dbo].[AuditTrail_UserAccess]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AuditTrail_UserAccess](
	[PK_UserAccessID] [bigint] IDENTITY(1,1) NOT NULL,
	[Userid] [varchar](150) NULL,
	[ActionDate] [datetime] NULL,
	[Action] [varchar](150) NULL,
 CONSTRAINT [PK_AuditTrail_UserAccess] PRIMARY KEY CLUSTERED 
(
	[PK_UserAccessID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
