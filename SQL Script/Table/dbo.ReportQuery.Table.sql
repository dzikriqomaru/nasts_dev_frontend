/****** Object:  Table [dbo].[ReportQuery]    Script Date: 12/12/2023 11:20:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReportQuery](
	[PK_ReportQuery_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ReportName] [varchar](550) NULL,
	[ReportDescription] [varchar](max) NULL,
	[QueryData] [varchar](max) NULL,
	[IsUseDesigner] [bit] NULL,
	[QueryObjectDesigner] [varchar](max) NULL,
	[Active] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[LastUpdateBy] [varchar](50) NULL,
	[ApprovedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[ApprovedDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
