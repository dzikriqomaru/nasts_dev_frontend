/****** Object:  Table [dbo].[ModuleDetail]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ModuleDetail](
	[PK_ModuleDetail_ID] [int] IDENTITY(1,1) NOT NULL,
	[FK_Module_ID] [int] NOT NULL,
	[ModuleDetailName] [varchar](250) NULL,
	[ModuleDetailLabel] [varchar](250) NULL,
	[ModuleDetailDescription] [varchar](500) NULL,
	[Active] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[LastUpdateBy] [varchar](50) NULL,
	[ApprovedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[ApprovedDate] [datetime] NULL,
	[IsTypeOnetoOne] [bit] NULL,
	[FK_Parent_ID] [int] NULL,
 CONSTRAINT [PK_ModuleDetail] PRIMARY KEY CLUSTERED 
(
	[PK_ModuleDetail_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
