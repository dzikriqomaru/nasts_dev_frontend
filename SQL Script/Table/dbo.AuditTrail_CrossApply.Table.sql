/****** Object:  Table [dbo].[AuditTrail_CrossApply]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AuditTrail_CrossApply](
	[FK_Module_ID] [bigint] NULL,
	[UserID] [varchar](50) NULL,
	[primarykeydata] [varchar](8000) NULL,
	[FieldName] [varchar](100) NULL,
	[OldValue] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
