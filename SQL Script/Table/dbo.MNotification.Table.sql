/****** Object:  Table [dbo].[MNotification]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MNotification](
	[PK_Notification_ID] [int] IDENTITY(1,1) NOT NULL,
	[FromUserID] [int] NULL,
	[ToUserID] [int] NOT NULL,
	[NotificationHeader] [varchar](50) NULL,
	[NotificationBody] [varchar](max) NULL,
	[IsRead] [bit] NOT NULL,
	[DirectPage] [varchar](100) NULL,
	[FK_Module_ID] [int] NULL,
	[FK_ModuleAction_ID] [int] NULL,
	[FK_ModuleApproval_ID] [bigint] NULL,
	[Active] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[LastUpdateBy] [varchar](50) NULL,
	[ApprovedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[ApprovedDate] [datetime] NULL,
	[Alternateby] [varchar](50) NULL,
	[FK_NotificationType_ID] [varchar](50) NULL,
 CONSTRAINT [PK_MNotification] PRIMARY KEY CLUSTERED 
(
	[PK_Notification_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
