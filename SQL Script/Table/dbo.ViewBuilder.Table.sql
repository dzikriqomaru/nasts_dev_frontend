/****** Object:  Table [dbo].[ViewBuilder]    Script Date: 12/12/2023 11:20:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ViewBuilder](
	[PK_ViewBuilder_ID] [int] IDENTITY(1,1) NOT NULL,
	[ViewBuilder_JSON] [varchar](max) NULL,
	[Active] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[LastUpdateBy] [varchar](50) NULL,
	[ApprovedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[ApprovedDate] [datetime] NULL,
	[Alternateby] [varchar](50) NULL,
	[FK_ModuleAction_ID] [int] NOT NULL,
	[FK_MViewBuilder_ID] [bigint] NULL,
 CONSTRAINT [PK_ViewBuilder] PRIMARY KEY CLUSTERED 
(
	[PK_ViewBuilder_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[ViewBuilder] ADD  CONSTRAINT [DF_ViewBuilder_FK_ModuleAction_ID]  DEFAULT ('') FOR [FK_ModuleAction_ID]
GO
