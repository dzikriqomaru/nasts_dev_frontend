/****** Object:  Table [dbo].[SystemCalendar_Upload]    Script Date: 12/12/2023 11:20:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SystemCalendar_Upload](
	[PK_upload_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[nawa_userid] [varchar](50) NULL,
	[nawa_recordnumber] [bigint] NULL,
	[KeteranganError] [varchar](max) NULL,
	[nawa_Action] [varchar](50) NULL,
	[PK_SystemCalendar_ID] [varchar](max) NULL,
	[CalendarDate] [varchar](max) NULL,
	[IsWeekend] [varchar](max) NULL,
	[IsHoliday] [varchar](max) NULL,
	[IsWorkday] [varchar](max) NULL,
	[CalendarText] [varchar](max) NULL,
	[CalendarDescription] [varchar](max) NULL,
	[Active] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
