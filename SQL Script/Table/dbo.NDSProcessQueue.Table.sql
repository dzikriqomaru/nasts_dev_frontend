/****** Object:  Table [dbo].[NDSProcessQueue]    Script Date: 12/12/2023 11:20:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NDSProcessQueue](
	[Active] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[LastUpdateBy] [varchar](50) NULL,
	[ApprovedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[ApprovedDate] [datetime] NULL,
	[Alternateby] [varchar](50) NULL,
	[PK_NDSProcess_ID] [int] NULL,
	[QueueNo] [int] NULL,
	[QueueName] [varchar](500) NULL,
	[QueueDescription] [varchar](1000) NULL
) ON [PRIMARY]
GO
