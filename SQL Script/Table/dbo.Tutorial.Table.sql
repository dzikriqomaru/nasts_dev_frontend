/****** Object:  Table [dbo].[Tutorial]    Script Date: 12/12/2023 11:20:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tutorial](
	[pk_tutorial_id] [bigint] IDENTITY(1,1) NOT NULL,
	[title] [varchar](1000) NOT NULL,
	[description] [varchar](500) NULL,
	[tutorial] [varchar](max) NULL,
	[ref_all] [bit] NOT NULL,
	[ref_module_id] [varchar](max) NULL,
	[ref_module_action_id] [varchar](1000) NULL,
	[ref_page_url] [varchar](max) NULL,
	[Active] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[LastUpdateBy] [varchar](50) NULL,
	[ApprovedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[ApprovedDate] [datetime] NULL,
	[Alternateby] [varchar](50) NULL,
 CONSTRAINT [PK_Tutorial] PRIMARY KEY CLUSTERED 
(
	[pk_tutorial_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
