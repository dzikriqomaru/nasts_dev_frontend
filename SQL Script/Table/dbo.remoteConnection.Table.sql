/****** Object:  Table [dbo].[remoteConnection]    Script Date: 12/12/2023 11:20:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[remoteConnection](
	[PK_Conn_ID] [int] IDENTITY(1,1) NOT NULL,
	[serverName] [varchar](max) NULL,
	[dbName] [varchar](max) NULL,
	[userName] [varchar](max) NULL,
	[password] [varchar](max) NULL,
	[passwordSalt] [varchar](max) NULL,
	[connectionName] [varchar](max) NULL,
	[connectionType] [bigint] NULL,
	[Active] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[LastUpdateBy] [varchar](50) NULL,
	[ApprovedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[ApprovedDate] [datetime] NULL,
	[Alternateby] [varchar](50) NULL,
 CONSTRAINT [PK_remoteConnection] PRIMARY KEY CLUSTERED 
(
	[PK_Conn_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
