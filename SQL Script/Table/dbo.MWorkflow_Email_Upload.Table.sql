/****** Object:  Table [dbo].[MWorkflow_Email_Upload]    Script Date: 12/12/2023 11:13:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MWorkflow_Email_Upload](
	[PK_upload_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[nawa_userid] [varchar](50) NULL,
	[nawa_recordnumber] [bigint] NULL,
	[KeteranganError] [varchar](max) NULL,
	[nawa_Action] [varchar](50) NULL,
	[PK_MWorkflow_Email_ID] [varchar](max) NULL,
	[FK_Module_ID] [varchar](max) NULL,
	[FK_Unik_ID] [varchar](max) NULL,
	[UserID] [varchar](max) NULL,
	[EmailAddress] [varchar](max) NULL,
	[IsSendEmail] [varchar](max) NULL,
	[FK_MWorkflow_ApprovalStatus_ID] [varchar](max) NULL,
	[UserName] [varchar](max) NULL,
	[FK_ModuleApproval_ID] [varchar](max) NULL,
	[Active] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
