/****** Object:  Table [dbo].[userTryLogin]    Script Date: 12/12/2023 11:20:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[userTryLogin](
	[pkUserTry] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [varchar](255) NULL,
	[countTry] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[pkUserTry] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
