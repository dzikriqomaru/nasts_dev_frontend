/****** Object:  Table [dbo].[ValidationSummary]    Script Date: 12/12/2023 11:20:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ValidationSummary](
	[PK_Record_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[SegmentData] [varchar](1000) NULL,
	[Status] [varchar](50) NULL,
	[TanggalData] [datetime] NULL,
	[ModuleURL] [varchar](max) NULL,
	[KodeCabang] [varchar](6) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
