/****** Object:  Table [dbo].[ApprovalDesignerFieldRegex]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApprovalDesignerFieldRegex](
	[PK_ApprovalDesignerFieldRegex] [bigint] IDENTITY(1,1) NOT NULL,
	[FK_ApprovalDesignerField_ID] [bigint] NULL,
	[Regex] [varchar](250) NULL,
 CONSTRAINT [PK_ApprovalDesignerFieldRegex] PRIMARY KEY CLUSTERED 
(
	[PK_ApprovalDesignerFieldRegex] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ApprovalDesignerFieldRegex]  WITH NOCHECK ADD  CONSTRAINT [FK_ApprovalDesignerFieldRegex_ApprovalDesignerFieldRegex] FOREIGN KEY([FK_ApprovalDesignerField_ID])
REFERENCES [dbo].[ApprovalDesignerField] ([PK_ApprovalDesignerField_ID])
GO
ALTER TABLE [dbo].[ApprovalDesignerFieldRegex] CHECK CONSTRAINT [FK_ApprovalDesignerFieldRegex_ApprovalDesignerFieldRegex]
GO
