/****** Object:  Table [dbo].[MGroupMenuSettting]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MGroupMenuSettting](
	[PK_MGroupMenuSettting_ID] [int] IDENTITY(1,1) NOT NULL,
	[FK_MGroupMenu_ID] [int] NULL,
	[mMenuID] [varchar](50) NULL,
	[mMenuLabel] [varchar](250) NULL,
	[mMenuParentID] [varchar](50) NULL,
	[mMenuURL] [varchar](250) NULL,
	[FK_Module_ID] [int] NULL,
	[FK_Action_ID] [int] NULL,
	[urutan] [int] NULL,
	[IconName] [varchar](250) NULL,
 CONSTRAINT [PK_MGroupMenuSettting] PRIMARY KEY CLUSTERED 
(
	[PK_MGroupMenuSettting_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
