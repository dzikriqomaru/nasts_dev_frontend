/****** Object:  Table [dbo].[EmailActionType]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmailActionType](
	[PK_EmailActionType_Id] [int] IDENTITY(1,1) NOT NULL,
	[EmailActionTypeName] [varchar](50) NULL,
 CONSTRAINT [PK_EmailActionType] PRIMARY KEY CLUSTERED 
(
	[PK_EmailActionType_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
