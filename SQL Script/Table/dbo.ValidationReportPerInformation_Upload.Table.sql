/****** Object:  Table [dbo].[ValidationReportPerInformation_Upload]    Script Date: 12/12/2023 11:20:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ValidationReportPerInformation_Upload](
	[PK_upload_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[nawa_userid] [varchar](50) NULL,
	[nawa_recordnumber] [bigint] NULL,
	[KeteranganError] [varchar](max) NULL,
	[nawa_Action] [varchar](50) NULL,
	[PK_ValidationReportPerInformation] [varchar](max) NULL,
	[InformationName] [varchar](max) NULL,
	[ValidationMessage] [varchar](max) NULL,
	[ModuleURL] [varchar](max) NULL,
	[ModuleID] [varchar](max) NULL,
	[FK_ValidationType] [varchar](max) NULL,
	[ErrorType] [varchar](max) NULL,
	[TanggalData] [varchar](max) NULL,
	[KodeCabang] [varchar](max) NULL,
	[FK_DataSet] [varchar](max) NULL,
	[FK_KategoriValidasi] [varchar](max) NULL,
	[Active] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
