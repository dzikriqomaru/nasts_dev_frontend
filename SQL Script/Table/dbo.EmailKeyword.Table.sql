/****** Object:  Table [dbo].[EmailKeyword]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmailKeyword](
	[PK_Email_Keyword_ID] [int] IDENTITY(1,1) NOT NULL,
	[Email_Keyword] [varchar](255) NOT NULL,
	[Expression] [varchar](8000) NULL,
	[Email_Keyword_Parameter_Subject] [varchar](max) NULL,
	[Email_Keyword_Parameter_Body] [varchar](max) NULL,
	[NeedConfirmation] [bit] NULL,
	[FK_EmailTemplateSuccessResponse_ID] [int] NULL,
	[FK_EmailTemplateCancelResponse_ID] [int] NULL,
	[FK_EmailTemplateFailedResponse_ID] [int] NULL,
	[Active] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[LastUpdateBy] [varchar](50) NULL,
	[ApprovedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[ApprovedDate] [datetime] NULL,
	[Alternateby] [varchar](50) NULL,
 CONSTRAINT [PK_MS_Email_Keyword] PRIMARY KEY CLUSTERED 
(
	[PK_Email_Keyword_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
