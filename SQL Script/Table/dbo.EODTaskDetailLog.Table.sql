/****** Object:  Table [dbo].[EODTaskDetailLog]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EODTaskDetailLog](
	[PK_EODTaskDetailLog_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[EODSchedulerLogID] [bigint] NULL,
	[EODTaskLogID] [bigint] NULL,
	[FK_EODTAskDetail_ID] [bigint] NULL,
	[ExecuteBy] [varchar](250) NULL,
	[StartDate] [datetime] NULL,
	[Enddate] [datetime] NULL,
	[ErrorMessage] [varchar](max) NULL,
	[FK_MsEODStatus_ID] [int] NULL,
	[executionID] [varchar](150) NULL,
 CONSTRAINT [PK_EODSSISID] PRIMARY KEY CLUSTERED 
(
	[PK_EODTaskDetailLog_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
