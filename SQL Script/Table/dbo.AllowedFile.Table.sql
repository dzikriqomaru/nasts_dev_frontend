/****** Object:  Table [dbo].[AllowedFile]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AllowedFile](
	[Pk_Allowed_File] [bigint] IDENTITY(1,1) NOT NULL,
	[Module_Name] [varchar](250) NULL,
	[Fk_Allowed_FileType] [int] NULL,
	[Fieldname] [varchar](250) NULL
) ON [PRIMARY]
GO
