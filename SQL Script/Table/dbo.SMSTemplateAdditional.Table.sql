/****** Object:  Table [dbo].[SMSTemplateAdditional]    Script Date: 12/12/2023 11:20:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SMSTemplateAdditional](
	[PK_SMSTemplateAdditional_ID] [int] IDENTITY(1,1) NOT NULL,
	[FK_SMSTemplate_ID] [int] NULL,
	[FK_SMSTableType_ID] [int] NULL,
	[NamaTable] [varchar](250) NULL,
	[QueryData] [varchar](max) NULL,
	[QueryDataDesigner] [varchar](max) NULL,
	[FieldUnikPrimaryTable] [varchar](250) NULL,
 CONSTRAINT [PK_SMSTemplateAdditional] PRIMARY KEY CLUSTERED 
(
	[PK_SMSTemplateAdditional_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
