/****** Object:  Table [dbo].[GettingStarted]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GettingStarted](
	[PK_GettingStarted_ID] [int] IDENTITY(1,1) NOT NULL,
	[StartedName] [varchar](200) NOT NULL,
	[Sequence] [int] NULL,
	[Active] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[LastUpdateBy] [varchar](50) NULL,
	[ApprovedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[ApprovedDate] [datetime] NULL,
	[Icon] [varbinary](max) NULL,
	[FK_Module_ID] [int] NULL,
	[FK_ModuleAction_ID] [int] NULL,
	[IconName] [varchar](300) NULL,
	[Alternateby] [varchar](50) NULL,
 CONSTRAINT [PK_GettingStarted] PRIMARY KEY CLUSTERED 
(
	[PK_GettingStarted_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
