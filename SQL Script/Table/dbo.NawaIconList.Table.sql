/****** Object:  Table [dbo].[NawaIconList]    Script Date: 12/12/2023 11:20:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NawaIconList](
	[IconUrl] [varchar](255) NULL,
	[Category] [varchar](255) NULL,
	[IconName] [varchar](255) NULL
) ON [PRIMARY]
GO
