/****** Object:  Table [dbo].[MUser]    Script Date: 12/12/2023 11:13:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MUser](
	[PK_MUser_ID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [varchar](50) NOT NULL,
	[UserName] [varchar](250) NULL,
	[FK_MRole_ID] [int] NULL,
	[FK_MGroupMenu_ID] [int] NULL,
	[UserPasword] [varchar](250) NULL,
	[UserPasswordSalt] [varchar](250) NULL,
	[UserEmailAddress] [varchar](250) NULL,
	[CreatedBy] [varchar](50) NULL,
	[LastUpdateBy] [varchar](50) NULL,
	[ApprovedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[ApprovedDate] [datetime] NULL,
	[IPAddress] [varchar](250) NULL,
	[Active] [bit] NULL,
	[InUsed] [bit] NULL,
	[IsDisabled] [bit] NULL,
	[LastLogin] [datetime] NULL,
	[LastChangePassword] [datetime] NULL,
	[Alternateby] [varchar](50) NULL,
 CONSTRAINT [PK_MUser] PRIMARY KEY CLUSTERED 
(
	[PK_MUser_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
