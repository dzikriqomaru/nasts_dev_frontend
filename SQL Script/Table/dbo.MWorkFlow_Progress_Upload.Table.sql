/****** Object:  Table [dbo].[MWorkFlow_Progress_Upload]    Script Date: 12/12/2023 11:13:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MWorkFlow_Progress_Upload](
	[PK_upload_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[nawa_userid] [varchar](50) NULL,
	[nawa_recordnumber] [bigint] NULL,
	[KeteranganError] [varchar](max) NULL,
	[nawa_Action] [varchar](50) NULL,
	[PK_MWorkflow_Progress_ID] [varchar](max) NULL,
	[FK_Module_ID] [varchar](max) NULL,
	[FK_Unik_ID] [varchar](max) NULL,
	[FK_MWorkflow_ID] [varchar](max) NULL,
	[WorkflowLevel] [varchar](max) NULL,
	[FK_MWorkFlowUserType_ID] [varchar](max) NULL,
	[FK_MWorkflowRole_ID] [varchar](max) NULL,
	[FK_MWorkflowUser_ID] [varchar](max) NULL,
	[FK_MWorkflow_Status_ID] [varchar](max) NULL,
	[SLAType] [varchar](max) NULL,
	[SLAValue] [varchar](max) NULL,
	[BreachSLAAction] [varchar](max) NULL,
	[BreachSLAActionLevelTo] [varchar](max) NULL,
	[RevisedActionLevelTo] [varchar](max) NULL,
	[Active] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
