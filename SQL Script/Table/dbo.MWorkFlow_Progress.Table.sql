/****** Object:  Table [dbo].[MWorkFlow_Progress]    Script Date: 12/12/2023 11:13:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MWorkFlow_Progress](
	[PK_MWorkflow_Progress_ID] [int] IDENTITY(1,1) NOT NULL,
	[FK_Module_ID] [int] NULL,
	[FK_Unik_ID] [varchar](250) NULL,
	[FK_MWorkflow_ID] [int] NULL,
	[WorkflowLevel] [int] NULL,
	[FK_MWorkFlowUserType_ID] [int] NULL,
	[FK_MWorkflowRole_ID] [int] NULL,
	[FK_MWorkflowUser_ID] [int] NULL,
	[FK_MWorkflow_Status_ID] [int] NULL,
	[SLAType] [varchar](50) NULL,
	[SLAValue] [int] NULL,
	[BreachSLAAction] [varchar](250) NULL,
	[BreachSLAActionLevelTo] [int] NULL,
	[RevisedActionLevelTo] [int] NULL,
	[Active] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[LastUpdateBy] [varchar](50) NULL,
	[ApprovedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[ApprovedDate] [datetime] NULL,
	[Alternateby] [varchar](50) NULL,
 CONSTRAINT [PK_MWorkFlow_Progress] PRIMARY KEY CLUSTERED 
(
	[PK_MWorkflow_Progress_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
