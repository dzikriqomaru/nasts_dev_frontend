/****** Object:  Table [dbo].[AuditTrail_UserLogin]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AuditTrail_UserLogin](
	[UserLoginID] [bigint] IDENTITY(1,1) NOT NULL,
	[UserLoginUserid] [varchar](150) NULL,
	[UserLoginActionDate] [datetime] NULL,
	[UserLoginAction] [varchar](50) NULL,
	[UserLoginIPAddress] [varchar](50) NULL,
	[UserLoginDescription] [varchar](max) NULL,
 CONSTRAINT [PK_UserLogin] PRIMARY KEY CLUSTERED 
(
	[UserLoginID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
