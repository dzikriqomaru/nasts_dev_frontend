/****** Object:  Table [dbo].[MenuTemplate]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MenuTemplate](
	[mMenuID] [varchar](50) NOT NULL,
	[mMenuLabel] [varchar](250) NULL,
	[mMenuParentID] [varchar](50) NULL,
	[mMenuURL] [varchar](250) NULL,
	[FK_Module_ID] [int] NULL,
	[FK_Action_ID] [int] NULL,
	[urutan] [int] NULL,
	[IconName] [varchar](250) NULL,
 CONSTRAINT [PK_mMenu] PRIMARY KEY CLUSTERED 
(
	[mMenuID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
