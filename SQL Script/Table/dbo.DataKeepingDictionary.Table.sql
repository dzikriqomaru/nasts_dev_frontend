/****** Object:  Table [dbo].[DataKeepingDictionary]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DataKeepingDictionary](
	[PK_DataKeepingDictionary] [int] IDENTITY(1,1) NOT NULL,
	[TableName] [int] NOT NULL,
	[FieldName] [varchar](250) NOT NULL,
	[ValueToKeep] [varchar](1000) NOT NULL,
	[KeyField] [varchar](50) NOT NULL,
	[KeyFieldValue] [varchar](1000) NOT NULL,
	[Keep] [bit] NOT NULL,
	[Active] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[LastUpdateBy] [varchar](50) NULL,
	[ApprovedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[ApprovedDate] [datetime] NULL,
	[Alternateby] [varchar](50) NULL,
 CONSTRAINT [PK_DataKeepingDictionary] PRIMARY KEY CLUSTERED 
(
	[PK_DataKeepingDictionary] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
