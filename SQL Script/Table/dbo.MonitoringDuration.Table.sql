/****** Object:  Table [dbo].[MonitoringDuration]    Script Date: 12/12/2023 11:13:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MonitoringDuration](
	[PK_MonitoringDuration_Id] [int] IDENTITY(1,1) NOT NULL,
	[MonitoringDurationName] [varchar](50) NULL,
 CONSTRAINT [PK_MonitoringDuration] PRIMARY KEY CLUSTERED 
(
	[PK_MonitoringDuration_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[MonitoringDuration]  WITH NOCHECK ADD  CONSTRAINT [FK_MonitoringDuration_MonitoringDuration] FOREIGN KEY([PK_MonitoringDuration_Id])
REFERENCES [dbo].[MonitoringDuration] ([PK_MonitoringDuration_Id])
GO
ALTER TABLE [dbo].[MonitoringDuration] CHECK CONSTRAINT [FK_MonitoringDuration_MonitoringDuration]
GO
