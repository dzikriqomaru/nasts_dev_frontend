/****** Object:  Table [dbo].[ModuleValidation]    Script Date: 12/12/2023 11:13:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ModuleValidation](
	[PK_ModuleValidation_ID] [int] IDENTITY(1,1) NOT NULL,
	[FK_Module_ID] [int] NULL,
	[FK_ModuleAction_ID] [int] NULL,
	[FK_ModuleTime_ID] [int] NULL,
	[StoreProcedureName] [varchar](550) NULL,
	[StoreProcedureParameter] [varchar](2000) NULL,
	[StoreProcedureParameterValueFieldSequence] [varchar](500) NULL,
 CONSTRAINT [PK_ModuleValidation] PRIMARY KEY CLUSTERED 
(
	[PK_ModuleValidation_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ModuleValidation]  WITH NOCHECK ADD  CONSTRAINT [FK_ModuleValidation_Module] FOREIGN KEY([FK_Module_ID])
REFERENCES [dbo].[Module] ([PK_Module_ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ModuleValidation] CHECK CONSTRAINT [FK_ModuleValidation_Module]
GO
ALTER TABLE [dbo].[ModuleValidation]  WITH NOCHECK ADD  CONSTRAINT [FK_ModuleValidation_ModuleAction] FOREIGN KEY([FK_ModuleAction_ID])
REFERENCES [dbo].[ModuleAction] ([PK_ModuleAction_ID])
GO
ALTER TABLE [dbo].[ModuleValidation] CHECK CONSTRAINT [FK_ModuleValidation_ModuleAction]
GO
