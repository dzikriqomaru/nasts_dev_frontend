/****** Object:  Table [dbo].[ServiceManagement]    Script Date: 12/12/2023 11:20:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ServiceManagement](
	[PK_ServiceManagment_ID] [int] IDENTITY(1,1) NOT NULL,
	[ServerName] [varchar](200) NULL,
	[ServiceName] [varchar](200) NULL,
	[FK_ServiceStatus_ID] [int] NULL,
	[ErrorFile] [varbinary](4000) NULL,
	[ErrorFileName] [varchar](300) NULL,
	[Active] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[LastUpdateBy] [varchar](50) NULL,
	[ApprovedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[ApprovedDate] [datetime] NULL,
	[Alternateby] [varchar](50) NULL,
	[ErrFileName] [varchar](200) NULL
) ON [PRIMARY]
GO
