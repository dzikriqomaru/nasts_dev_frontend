/****** Object:  Table [dbo].[ModuleDetailApproval_Upload]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ModuleDetailApproval_Upload](
	[PK_upload_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[nawa_userid] [varchar](50) NULL,
	[nawa_recordnumber] [bigint] NULL,
	[KeteranganError] [varchar](max) NULL,
	[nawa_Action] [varchar](50) NULL,
	[PK_ModuleDetailApproval_ID] [varchar](max) NULL,
	[FK_ModuleDetail_ID] [varchar](max) NULL,
	[ModuleDetailName] [varchar](max) NULL,
	[ModuleDetailKey] [varchar](max) NULL,
	[ModuleDetailField] [varchar](max) NULL,
	[ModuleDetailFieldBefore] [varchar](max) NULL,
	[FK_ModuleApproval_ID] [varchar](max) NULL,
	[PK_ModuleAction_ID] [varchar](max) NULL,
	[Active] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
