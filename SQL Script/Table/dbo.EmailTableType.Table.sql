/****** Object:  Table [dbo].[EmailTableType]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmailTableType](
	[PK_EmailTableType_ID] [int] NOT NULL,
	[EmailTableTypeName] [varchar](50) NULL,
 CONSTRAINT [PK_EmailTableType] PRIMARY KEY CLUSTERED 
(
	[PK_EmailTableType_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
