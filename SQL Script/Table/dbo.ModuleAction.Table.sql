/****** Object:  Table [dbo].[ModuleAction]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ModuleAction](
	[PK_ModuleAction_ID] [int] NOT NULL,
	[ModuleActionName] [varchar](250) NULL,
	[ModuleActionDescription] [varchar](550) NULL,
 CONSTRAINT [PK_ModuleAction] PRIMARY KEY CLUSTERED 
(
	[PK_ModuleAction_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
