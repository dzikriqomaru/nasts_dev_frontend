/****** Object:  Table [dbo].[SMSTemplateDetail]    Script Date: 12/12/2023 11:20:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SMSTemplateDetail](
	[PK_SMSTemplateDetail_ID] [int] IDENTITY(1,1) NOT NULL,
	[FK_SMSTemplate_ID] [int] NULL,
	[Replacer] [varchar](550) NULL,
	[FieldReplacer] [varchar](250) NULL,
 CONSTRAINT [PK_SMSTemplateDetail] PRIMARY KEY CLUSTERED 
(
	[PK_SMSTemplateDetail_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
