/****** Object:  Table [dbo].[EmailTemplateSchedulerDetail]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmailTemplateSchedulerDetail](
	[PK_EmailTemplateSchedulerDetail_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[FK_EmailTEmplateScheduler_ID] [bigint] NULL,
	[UnikFieldTablePrimary] [varchar](500) NULL,
	[EmailTo] [varchar](1000) NULL,
	[EmailCC] [varchar](1000) NULL,
	[EmailBCC] [varchar](1000) NULL,
	[EmailSubject] [varchar](1000) NULL,
	[EmailBody] [varchar](max) NULL,
	[ProcessDate] [datetime] NULL,
	[SendEmailDate] [datetime] NULL,
	[FK_EmailStatus_ID] [int] NULL,
	[ErrorMessage] [varchar](max) NULL,
	[retrycount] [int] NULL,
	[EmailID] [varchar](250) NOT NULL,
	[resendByUserCount] [int] NOT NULL,
	[EmailIDReference] [varchar](250) NULL,
	[AttachmentPassword] [varchar](max) NULL,
	[AttachmentPasswordSalt] [varchar](max) NULL,
 CONSTRAINT [PK_EmailTemplateSchedulerDetail] PRIMARY KEY CLUSTERED 
(
	[PK_EmailTemplateSchedulerDetail_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[EmailTemplateSchedulerDetail] ADD  CONSTRAINT [DF_EmailTemplateSchedulerDetail_EmailID]  DEFAULT (newid()) FOR [EmailID]
GO
ALTER TABLE [dbo].[EmailTemplateSchedulerDetail] ADD  CONSTRAINT [DF_EmailTemplateSchedulerDetail_resendByUserCount]  DEFAULT ((0)) FOR [resendByUserCount]
GO
