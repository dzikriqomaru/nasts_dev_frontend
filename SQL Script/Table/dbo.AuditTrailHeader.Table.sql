/****** Object:  Table [dbo].[AuditTrailHeader]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AuditTrailHeader](
	[PK_AuditTrail_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [varchar](50) NULL,
	[ApproveBy] [varchar](50) NULL,
	[ModuleLabel] [varchar](100) NULL,
	[FK_ModuleAction_ID] [int] NULL,
	[FK_AuditTrailStatus_ID] [int] NULL,
	[PK_ModuleApproval_ID] [bigint] NULL,
 CONSTRAINT [PK_AuditTrailHeader] PRIMARY KEY CLUSTERED 
(
	[PK_AuditTrail_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
