/****** Object:  Table [dbo].[AdvancedFilterWhereClause]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AdvancedFilterWhereClause](
	[PK_AdvancedFilterWhereClause_ID] [int] IDENTITY(1,1) NOT NULL,
	[FK_AdvanceFilter_ID] [int] NOT NULL,
	[Active] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[LastUpdateBy] [varchar](50) NULL,
	[ApprovedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[ApprovedDate] [datetime] NULL,
	[Alternateby] [varchar](50) NULL,
	[FilterWhereClause] [varchar](500) NULL,
	[FilterWhereFormat] [varchar](500) NULL,
	[JmlFilter] [int] NULL,
 CONSTRAINT [PK_AdvancedFilterWhereClause] PRIMARY KEY CLUSTERED 
(
	[PK_AdvancedFilterWhereClause_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
