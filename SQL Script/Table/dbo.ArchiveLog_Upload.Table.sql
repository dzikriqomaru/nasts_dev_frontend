/****** Object:  Table [dbo].[ArchiveLog_Upload]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ArchiveLog_Upload](
	[PK_upload_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[nawa_userid] [varchar](50) NULL,
	[nawa_recordnumber] [bigint] NULL,
	[KeteranganError] [varchar](max) NULL,
	[nawa_Action] [varchar](50) NULL,
	[PK_ArchiveLog_ID] [varchar](max) NULL,
	[ProcessDate] [varchar](max) NULL,
	[DataDate] [varchar](max) NULL,
	[SourceTable] [varchar](max) NULL,
	[RowDelete] [varchar](max) NULL,
	[DestinationTable] [varchar](max) NULL,
	[RowInserted] [varchar](max) NULL,
	[Active] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
