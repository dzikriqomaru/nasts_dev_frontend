/****** Object:  Table [dbo].[ArchiveLog]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ArchiveLog](
	[PK_ArchiveLog_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ProcessDate] [datetime] NULL,
	[DataDate] [datetime] NULL,
	[SourceTable] [varchar](500) NULL,
	[RowDelete] [bigint] NULL,
	[DestinationTable] [varchar](500) NULL,
	[RowInserted] [bigint] NULL,
	[Active] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[LastUpdateBy] [varchar](50) NULL,
	[ApprovedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[ApprovedDate] [datetime] NULL,
	[Alternateby] [varchar](50) NULL,
 CONSTRAINT [PK_ArchiveLog] PRIMARY KEY CLUSTERED 
(
	[PK_ArchiveLog_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
