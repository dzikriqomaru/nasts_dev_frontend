/****** Object:  Table [dbo].[SMSTemplate]    Script Date: 12/12/2023 11:20:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SMSTemplate](
	[PK_SMSTemplate_ID] [int] IDENTITY(1,1) NOT NULL,
	[SMSTemplateName] [varchar](550) NULL,
	[SMSTo] [varchar](1000) NULL,
	[SMSSubject] [varchar](1000) NULL,
	[SMSBody] [varchar](max) NULL,
	[FK_Monitoringduration_ID] [int] NULL,
	[StartDate] [datetime] NULL,
	[StartTime] [varchar](50) NULL,
	[ExcludeHoliday] [bit] NULL,
	[Active] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[LastUpdateBy] [varchar](50) NULL,
	[ApprovedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[ApprovedDate] [datetime] NULL,
 CONSTRAINT [PK_SMSTemplate] PRIMARY KEY CLUSTERED 
(
	[PK_SMSTemplate_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_SMSTemplate] UNIQUE NONCLUSTERED 
(
	[SMSTemplateName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[SMSTemplate]  WITH CHECK ADD  CONSTRAINT [FK_SMSTemplate_MonitoringDuration] FOREIGN KEY([FK_Monitoringduration_ID])
REFERENCES [dbo].[MonitoringDuration] ([PK_MonitoringDuration_Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SMSTemplate] CHECK CONSTRAINT [FK_SMSTemplate_MonitoringDuration]
GO
