/****** Object:  Table [dbo].[UserAccessHistory_Upload]    Script Date: 12/12/2023 11:20:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserAccessHistory_Upload](
	[PK_upload_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[nawa_userid] [varchar](50) NULL,
	[nawa_recordnumber] [bigint] NULL,
	[KeteranganError] [varchar](max) NULL,
	[nawa_Action] [varchar](50) NULL,
	[PK_UserAccessHistory_ID] [varchar](max) NULL,
	[FK_MUser_ID] [varchar](max) NULL,
	[MenuName] [varchar](max) NULL,
	[MenuURL] [varchar](max) NULL,
	[FK_Module_ID] [varchar](max) NULL,
	[ActionType] [varchar](max) NULL,
	[Active] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
