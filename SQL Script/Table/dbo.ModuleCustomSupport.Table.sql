/****** Object:  Table [dbo].[ModuleCustomSupport]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ModuleCustomSupport](
	[PK_ModuleCustomSupport_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[FK_Module_ID] [int] NULL,
	[SupportName] [varchar](100) NULL,
	[ActionType] [int] NULL,
	[Target] [varchar](100) NULL,
	[Parameter] [varchar](500) NULL,
	[ParameterValue] [varchar](50) NULL,
	[EnableConditionQuery] [varchar](max) NULL,
 CONSTRAINT [PK_ModuleCustomSupport] PRIMARY KEY CLUSTERED 
(
	[PK_ModuleCustomSupport_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
