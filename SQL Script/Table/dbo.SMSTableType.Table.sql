/****** Object:  Table [dbo].[SMSTableType]    Script Date: 12/12/2023 11:20:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SMSTableType](
	[PK_SMSTableType_ID] [int] NOT NULL,
	[SMSTableTypeName] [varchar](50) NULL,
 CONSTRAINT [PK_SMSTableType] PRIMARY KEY CLUSTERED 
(
	[PK_SMSTableType_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
