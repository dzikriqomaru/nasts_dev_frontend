/****** Object:  Table [dbo].[ComponenSettingAction]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ComponenSettingAction](
	[FieldNameTest] [int] IDENTITY(1,1) NOT NULL,
	[Checkbox1Unique] [varchar](max) NULL,
	[Checkbox2Req] [varchar](max) NULL,
	[Checkbox3Req] [varchar](max) NULL,
	[CheckboxFilter] [varchar](max) NULL,
	[CheckboxParent] [varchar](max) NULL,
	[CheckboxHidden] [varchar](max) NULL,
	[CheckboxRead] [varchar](max) NULL,
	[Active] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[LastUpdateBy] [varchar](50) NULL,
	[ApprovedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[ApprovedDate] [datetime] NULL,
	[Alternateby] [varchar](50) NULL,
	[Draft] [bit] NULL,
 CONSTRAINT [PK_ComponenSettingAction] PRIMARY KEY CLUSTERED 
(
	[FieldNameTest] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
