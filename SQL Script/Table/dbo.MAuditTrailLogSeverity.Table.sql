/****** Object:  Table [dbo].[MAuditTrailLogSeverity]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MAuditTrailLogSeverity](
	[PK_LogSeverity_ID] [int] IDENTITY(1,1) NOT NULL,
	[LogSeverity] [varchar](250) NULL,
	[Active] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[LastUpdateBy] [varchar](50) NULL,
	[ApprovedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[ApprovedDate] [datetime] NULL,
	[Alternateby] [varchar](50) NULL,
	[LogSeverityCode] [varchar](5) NULL,
 CONSTRAINT [PK_MAuditTrailLogSeverity] PRIMARY KEY CLUSTERED 
(
	[PK_LogSeverity_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
