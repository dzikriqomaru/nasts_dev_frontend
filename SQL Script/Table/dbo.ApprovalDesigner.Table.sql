/****** Object:  Table [dbo].[ApprovalDesigner]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApprovalDesigner](
	[PK_ApprovalDesigner_ID] [int] IDENTITY(1,1) NOT NULL,
	[ApprovalDesignerName] [varchar](250) NULL,
	[ApprovalDesignerLabel] [varchar](250) NULL,
	[ApprovalDesignerDescription] [varchar](500) NULL,
	[IsUseDesigner] [bit] NULL,
	[IsUseApproval] [bit] NULL,
	[IsSupportAdd] [bit] NULL,
	[IsSupportEdit] [bit] NULL,
	[IsSupportDelete] [bit] NULL,
	[IsSupportActivation] [bit] NULL,
	[IsSupportView] [bit] NULL,
	[IsSupportUpload] [bit] NULL,
	[IsSupportDetail] [bit] NULL,
	[UrlAdd] [varchar](550) NULL,
	[UrlEdit] [varchar](550) NULL,
	[UrlDelete] [varchar](550) NULL,
	[UrlActivation] [varchar](550) NULL,
	[UrlView] [varchar](550) NULL,
	[UrlUpload] [varchar](550) NULL,
	[UrlApproval] [varchar](550) NULL,
	[UrlApprovalDetail] [varchar](550) NULL,
	[UrlDetail] [varchar](550) NULL,
	[IsUseStoreProcedureValidation] [bit] NULL,
	[Active] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[LastUpdateBy] [varchar](50) NULL,
	[ApprovedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[ApprovedDate] [datetime] NULL,
 CONSTRAINT [PK_ApprovalDesigner] PRIMARY KEY CLUSTERED 
(
	[PK_ApprovalDesigner_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
