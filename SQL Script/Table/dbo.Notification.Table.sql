/****** Object:  Table [dbo].[Notification]    Script Date: 12/12/2023 11:20:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Notification](
	[PK_Notification_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[SenderNotification] [varchar](50) NULL,
	[ReceiverNotification] [varchar](50) NULL,
	[SubjectNotification] [varchar](500) NULL,
	[CreatedDate] [datetime] NULL,
	[Notification] [varchar](max) NULL,
	[FK_Module_ID] [int] NULL,
	[FK_ModuleAction_ID] [int] NULL,
	[PKUnik] [varchar](50) NULL,
	[IsRead] [bit] NULL,
 CONSTRAINT [PK_Notification] PRIMARY KEY CLUSTERED 
(
	[PK_Notification_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
