/****** Object:  Table [dbo].[NawadataConnectionString]    Script Date: 12/12/2023 11:20:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NawadataConnectionString](
	[PK_NawaDataConnectionString_ID] [int] IDENTITY(1,1) NOT NULL,
	[ConnecitonName] [varchar](8000) NOT NULL,
	[ConnectionValue] [varchar](8000) NOT NULL,
	[PasswordValue] [varchar](8000) NOT NULL,
	[Active] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[LastUpdateBy] [varchar](50) NULL,
	[ApprovedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[ApprovedDate] [datetime] NULL,
	[Alternateby] [varchar](50) NULL,
	[PasswordValue1] [varchar](8000) NULL,
	[PasswordEncript] [varchar](8000) NULL,
	[PasswordBinary] [varbinary](max) NULL,
	[PasswordBinaryName] [varchar](300) NULL,
 CONSTRAINT [PK_NawadataConnectionString] PRIMARY KEY CLUSTERED 
(
	[PK_NawaDataConnectionString_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
