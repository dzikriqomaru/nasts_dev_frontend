/****** Object:  Table [dbo].[EmailTemplateSensitivity]    Script Date: 12/12/2023 11:13:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmailTemplateSensitivity](
	[PK_EmailTemplateSensitivity_ID] [int] NOT NULL,
	[SensitivityDescription] [varchar](50) NOT NULL
) ON [PRIMARY]
GO
