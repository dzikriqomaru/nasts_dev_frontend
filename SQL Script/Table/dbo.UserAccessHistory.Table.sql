/****** Object:  Table [dbo].[UserAccessHistory]    Script Date: 12/12/2023 11:20:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserAccessHistory](
	[PK_UserAccessHistory_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[FK_MUser_ID] [int] NOT NULL,
	[MenuName] [varchar](250) NOT NULL,
	[MenuURL] [varchar](250) NOT NULL,
	[FK_Module_ID] [int] NULL,
	[Active] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[LastUpdateBy] [varchar](50) NULL,
	[ApprovedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[ApprovedDate] [datetime] NULL,
	[Alternateby] [varchar](50) NULL,
	[ActionType] [int] NULL,
 CONSTRAINT [PK_UserAccessHistory] PRIMARY KEY CLUSTERED 
(
	[PK_UserAccessHistory_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
