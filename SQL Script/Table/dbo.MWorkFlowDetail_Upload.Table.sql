/****** Object:  Table [dbo].[MWorkFlowDetail_Upload]    Script Date: 12/12/2023 11:13:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MWorkFlowDetail_Upload](
	[PK_upload_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[nawa_userid] [varchar](50) NULL,
	[nawa_recordnumber] [bigint] NULL,
	[KeteranganError] [varchar](max) NULL,
	[nawa_Action] [varchar](50) NULL,
	[PK_WorkFlowDetail] [varchar](max) NULL,
	[FK_WorkFlow] [varchar](max) NULL,
	[RoleID] [varchar](max) NULL,
	[UserType] [varchar](max) NULL,
	[Level] [varchar](max) NULL,
	[SLAType] [varchar](max) NULL,
	[SLAValue] [varchar](max) NULL,
	[BreachSLAAction] [varchar](max) NULL,
	[BreachSLAActionTo] [varchar](max) NULL,
	[RevisedActionTo] [varchar](max) NULL,
	[Active] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
