/****** Object:  Table [dbo].[NDSMapProcessQueue]    Script Date: 12/12/2023 11:20:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NDSMapProcessQueue](
	[Active] [bit] NULL,
	[CreatedBy] [varchar](50) NULL,
	[LastUpdateBy] [varchar](50) NULL,
	[ApprovedBy] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[LastUpdateDate] [datetime] NULL,
	[ApprovedDate] [datetime] NULL,
	[Alternateby] [varchar](50) NULL,
	[PK_NDSMapProcessQueue_ID] [int] NULL,
	[FK_NDSQueue_ID] [int] NULL,
	[FK_Process_ID] [bigint] NULL
) ON [PRIMARY]
GO
