/****** Object:  UserDefinedFunction [dbo].[Split]    Script Date: 12/12/2023 11:23:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION  [dbo].[Split]  
(  
 @delimited     NVARCHAR(MAX),  
 @delimiter     NVARCHAR(2000)  
)  
RETURNS @t TABLE (id INT IDENTITY(1, 1), val NVARCHAR(MAX))  
AS  
BEGIN  
 DECLARE @xml XML   
 SET @xml = N'<t>' + REPLACE(@delimited, @delimiter, '</t><t>') + '</t>'   
 INSERT INTO @t  
   (  
     val  
   )  
 SELECT LTRIM(RTRIM(r.value('.', 'varchar(MAX)')))  AS item  
 FROM   @xml.nodes('/t')              AS records(r)  
   
 RETURN  
END
GO
