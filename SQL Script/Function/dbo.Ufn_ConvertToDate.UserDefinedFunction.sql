/****** Object:  UserDefinedFunction [dbo].[Ufn_ConvertToDate]    Script Date: 12/12/2023 11:23:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[Ufn_ConvertToDate]
(
	@StrDate AS VARCHAR(10)
) RETURNS DATETIME
AS
BEGIN
	DECLARE @DatReturn AS DATETIME

	SET @DatReturn = SUBSTRING(@StrDate, 5, 4) + SUBSTRING(@StrDate, 3, 2) + SUBSTRING(@StrDate, 1, 2)

	RETURN @DatReturn
END

GO
