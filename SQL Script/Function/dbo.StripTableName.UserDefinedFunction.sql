/****** Object:  UserDefinedFunction [dbo].[StripTableName]    Script Date: 12/12/2023 11:23:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[StripTableName](@TableName VARCHAR(MAX)) RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE @NewTableName VARCHAR(MAX)
	SET @NewTableName = REPLACE(@TableName, '.', '_')
	RETURN @NewTableName
END
GO
