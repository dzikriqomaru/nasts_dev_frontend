/****** Object:  UserDefinedFunction [dbo].[ufn_GetFilterAdditionalPerUser]    Script Date: 12/12/2023 11:23:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_GetFilterAdditionalPerUser]
/***********************************************************
* Function description:
* Date:   24/07/2016  
* Author: Hendra
*
* Changes
* Date		Modified By			Comments
************************************************************
*
************************************************************/
(
	@Userid VARCHAR(100),@ModuleID int
) 
RETURNS VARCHAR(MAX)
BEGIN
	
	----cek kalau mengandung field :bulan,tahun,FK_KodeKantorCabang baru di tambah filter peruser
	
	--DECLARE @jmlFieldMatch int 
 --   DECLARE @additionalFilterPeruser VARCHAR(MAX)		
	
	--SELECT @jmlFieldMatch =COUNT(1) FROM ModuleField mf WHERE mf.FK_Module_ID=@ModuleID
	--AND mf.FieldName IN ('Bulan','Tahun','FK_KodeKantorCabang')
	
	
	--IF @jmlFieldMatch=3 
	--BEGIN
	--SELECT @additionalFilterPeruser= 'Bulan='''+ ss.Bulan +''' and Tahun='''+ ss.Tahun +''' and FK_KodeKantorCabang like ''' +ss.KodeCabang +'%'''
	--  FROM SettingSLIKPersonal ss WHERE ss.UserID=@Userid	
	--END
	--ELSE
	--	BEGIN
	
	--SET @additionalFilterPeruser='' 		
	--	END
		
	--	RETURN @additionalFilterPeruser
	
	
	--cek kalau mengandung field: Report_Date, Kode_Cabang_BI baru di tambah filter peruser
	DECLARE @tabelname VARCHAR(8000)=''
	SELECT @tabelname=m.ModuleName FROM Module AS m WHERE m.PK_Module_ID=@ModuleID
	DECLARE @jmlFieldMatch int 
    DECLARE @additionalFilterPeruser VARCHAR(MAX)		
	
	SELECT @jmlFieldMatch =COUNT(1) FROM ModuleField mf WHERE mf.FK_Module_ID=@ModuleID
	AND mf.FieldName IN ('Report_Date', 'Kode_Cabang_BI')
	
	
	IF @jmlFieldMatch=2
	BEGIN
		DECLARE @KodeCabang VARCHAR(MAX), @ReportDate VARCHAR(MAX);
		SELECT @KodeCabang = ss.KodeCabang , @ReportDate = CONVERT(VARCHAR, ss.ReportDate, 112)
		FROM dbo.SettingPersonal ss WHERE ss.UserID = @UserID

		SET @additionalFilterPeruser = ' Report_Date=''' + @ReportDate + '''';

		IF @KodeCabang <> 'All' 
        BEGIN
			SET @additionalFilterPeruser += ' AND Kode_Cabang_BI LIKE ''%' + @KodeCabang + '%''';
		END
	END
	ELSE
	BEGIN
		SET @additionalFilterPeruser='' 		
	END
		
	RETURN @additionalFilterPeruser
END


GO
