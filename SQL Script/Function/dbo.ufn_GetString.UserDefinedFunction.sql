/****** Object:  UserDefinedFunction [dbo].[ufn_GetString]    Script Date: 12/12/2023 11:23:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_GetString]
/***********************************************************
* Function description:
* Date:   11/22/2017  
* Author: Hendra
*
* Changes
* Date		Modified By			Comments
************************************************************
*
************************************************************/
(
	@string VARCHAR(8000),@keysValueToSearch VARCHAR(100),@untilThisCharAppears VARCHAR(100)
) 
RETURNS VARCHAR(8000)
BEGIN
	
	DECLARE @keysValueToSearchPattern VARCHAR(4000) = '%' + @keysValueToSearch + '%'
	DECLARE @retvalue VARCHAR(8000)=''
	

DECLARE @indexend INT =CHARINDEX(
               @untilThisCharAppears,
               @string,
               PATINDEX(@keysValueToSearchPattern, @string) + LEN(@keysValueToSearch)
)
	
DECLARE @lenresult INT=
           CHARINDEX(
               @untilThisCharAppears,
               @string,
               PATINDEX(@keysValueToSearchPattern, @string) + LEN(@keysValueToSearch)
           ) -(PATINDEX(@keysValueToSearchPattern, @string) + LEN(@keysValueToSearch))

IF @indexend=0 SET @lenresult=LEN(@string)
 
           
	
 --Nothing to touch here     
 
 IF PATINDEX(@keysValueToSearchPattern, @string)>0
 BEGIN
	set @retvalue =SUBSTRING(
           @string,	PATINDEX(@keysValueToSearchPattern, @string) + LEN(@keysValueToSearch),@lenresult
		)
 	
 END
 ELSE
 	BEGIN
 		set @retvalue=''
 	END
	
	RETURN @retvalue
END
GO
