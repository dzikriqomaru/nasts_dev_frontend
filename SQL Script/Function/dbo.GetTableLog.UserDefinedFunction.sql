/****** Object:  UserDefinedFunction [dbo].[GetTableLog]    Script Date: 12/12/2023 11:23:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GetTableLog]
/***********************************************************
* Function description:
* Date:   1/19/2017  
* Author: Hendra
*
* Changes
* Date		Modified By			Comments
************************************************************
*
************************************************************/
(
	@executionid VARCHAR(300)
) 
RETURNS VARCHAR(MAX)
BEGIN
	
DECLARE @retvalue VARCHAR(MAX)=''
	
SELECT @retvalue +=data FROM (	
SELECT '<table border=''1'' bordercolor=''#000000'' style=''width: 100%''><tr><th valign=''TOP'' style=''width:20px''>No</th><th valign=''TOP'' style=''width:150px''>Event</th><th valign=''TOP'' style=''width:150ox''>Source</th><th valign=''TOP'' style=''wi
dth:150px''>Executionid</th><th valign=''TOP'' style=''width:150px''>Start Date</th><th valign=''TOP'' style=''width:150px''>End Date</th><th valign=''TOP'' style=''width:20%''>Message</th>' data
UNION all 
SELECT '<tr><td>'+CONVERT(VARCHAR(20), ROW_NUMBER() OVER (ORDER BY s.id))+'</td><td>'+s.[event]+'</td><td>' +s.source+'</td><td>'+convert(varchar(100),s.executionid)+'</td><td>'+format(s.starttime,'dd-MMM-yyyy HH:mm')+'</td><td>'+format(s.endtime,'dd-MMM-
yyyy HH:mm')+'</td><td>'+s.[message]+'</td></tr>'
  FROM sysssislog s WHERE s.executionid=@executionid
  UNION all
  SELECT '</table>'
	)Xx
	
	RETURN @retvalue
END

 
GO
