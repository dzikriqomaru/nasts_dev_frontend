/****** Object:  UserDefinedFunction [dbo].[ufn_isDecimal]    Script Date: 12/12/2023 11:23:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_isDecimal]
(
	@string VARCHAR(8000),@prec int,@scale int
) 
RETURNS int

BEgin 
	
	if(CHARINDEX('-',@string) = 0)
	BEGIN
		declare @leng int = len(@string) 
		DECLARE @puluhanLength int = CHARINDEX('.',@string)

		set  @puluhanLength = case when @puluhanLength = 0 then @leng else @puluhanLength-1 END

		Declare @decimal int = case when (@leng-@puluhanLength) = 0 then 0 else @leng-@puluhanLength-1 end

	
		IF  (@prec-@scale) >= @puluhanLength 
			return 1
		ELSE 
			return 0
	END
	ELSE
		return 0
	return 0
END
GO
