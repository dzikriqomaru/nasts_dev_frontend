/****** Object:  UserDefinedFunction [dbo].[Ufn_GetDelimitedValue]    Script Date: 12/12/2023 11:23:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[Ufn_GetDelimitedValue](@StrData AS VARCHAR(MAX), @StrDelimiter AS VARCHAR(MAX), @IntIndex AS INT) 
RETURNS VARCHAR(MAX)
AS
BEGIN

	DECLARE @StrReturn AS VARCHAR(MAX)
	SET @StrReturn = ''

	DECLARE @IntCurrentIndex AS INT
	SET @IntCurrentIndex = 1

	DECLARE @IntEndIndex INT
	
	WHILE @IntCurrentIndex <= @IntIndex
	BEGIN
		SET @IntEndIndex = CHARINDEX(@StrDelimiter, RTRIM(LTRIM(@StrData)))
		
		IF @IntCurrentIndex = @IntIndex
		BEGIN
			IF @IntEndIndex <> 0
			BEGIN
				SET @StrReturn = LEFT(@StrData, @IntEndIndex - 1)
			END
			ELSE
			BEGIN
				SET @StrReturn = @StrData
			END
		END
		ELSE
		BEGIN
			SET @StrData = RIGHT(@StrData, LEN(@StrData) - @IntEndIndex)
		END

		SET @IntCurrentIndex = @IntCurrentIndex + 1
	END



	RETURN @StrReturn

END

GO
