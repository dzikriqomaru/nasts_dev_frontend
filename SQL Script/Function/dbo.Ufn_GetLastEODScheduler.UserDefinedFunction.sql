/****** Object:  UserDefinedFunction [dbo].[Ufn_GetLastEODScheduler]    Script Date: 12/12/2023 11:23:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[Ufn_GetLastEODScheduler] (@lngPk_EODScheduler_Id BIGINT, @datProcessDate DATETIME)  
RETURNS DATETIME  
  
AS  
BEGIN  
  
 DECLARE @datReturnDate DATETIME  
  
 DECLARE @intPeriod INT  
 DECLARE @lngFk_MsPeriodType_Id BIGINT  
 DECLARE @datStartDate DATETIME  
 DECLARE @intStartHour INT  
 DECLARE @intStartMinute INT  
  
 --Ambil detail dari EODScheduler  
 SELECT   
 @intPeriod = e.EODPeriod,   
 @lngFk_MsPeriodType_Id = e.FK_MsEODPeriod,   
 @datStartDate = e.StartDate,  
 @intStartHour = DATEPART(hour, e.StartDate),   
 @intStartMinute = DATEPART(minute, e.StartDate)  
 FROM EODScheduler e  
 WHERE e.PK_EODScheduler_ID = @lngPk_EODScheduler_Id  
  
 DECLARE @datCurrentDate AS DATETIME  
 SET @datCurrentDate = @datStartDate  
  
   
 --Looping terus sampai ketemu posisi run time terakhir  
 WHILE DATEDIFF(ss, @datCurrentDate, @datProcessDate) > 0  
 BEGIN  
  SET @datReturnDate = @datCurrentDate  
    
  IF @lngFk_MsPeriodType_Id = 1  --Day(s)  
  BEGIN  
   SET @datCurrentDate = DATEADD(DAY, @intPeriod, @datCurrentDate)  
  END  
  ELSE IF @lngFk_MsPeriodType_Id = 2 --Week(s)  
  BEGIN  
   SET @datCurrentDate = DATEADD(WEEK, @intPeriod, @datCurrentDate)  
  END  
  ELSE IF @lngFk_MsPeriodType_Id = 3 --Month(s)  
  BEGIN  
   SET @datCurrentDate = DATEADD(MONTH, @intPeriod, @datCurrentDate)  
  END  
  ELSE IF @lngFk_MsPeriodType_Id = 4 --Year(s)  
  BEGIN  
   SET @datCurrentDate = DATEADD(YEAR, @intPeriod, @datCurrentDate)  
  END  
  ELSE IF @lngFk_MsPeriodType_Id = 5 --Hour(s)      
  BEGIN    
   SET @datCurrentDate = DATEADD(HOUR, @intPeriod, @datCurrentDate)    
  END
  ELSE IF @lngFk_MsPeriodType_Id = 6 --Menit(s)  
  BEGIN  
   SET @datCurrentDate = DATEADD(MINUTE, @intPeriod, @datCurrentDate)  
  END  
  
  --SET @datCurrentDate = CONVERT(DATETIME,   
  -- CONVERT(VARCHAR, @datCurrentDate, 112) + ' ' +   
  -- RIGHT('00' + CONVERT(VARCHAR(2), @intStartHour), 2) + ':' +   
  -- RIGHT('00' + CONVERT(VARCHAR(2), @intStartMinute), 2) + ':00.000')  
    
 END  
  
 --SELECT @datReturnDate, * FROM EODScheduler e WHERE e.Pk_EODScheduler_Id = @lngPk_EODScheduler_Id  
 RETURN @datReturnDate  
    
END  
GO
