/****** Object:  UserDefinedFunction [dbo].[ufn_GetWorkingDate]    Script Date: 12/12/2023 11:23:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ufn_GetWorkingDate](@StartDate DATETIME, @Interval INT)
	RETURNS DATETIME
AS
BEGIN
	DECLARE @EndDate DATETIME

	SET @StartDate = CAST(@StartDate AS DATE)

	SET @EndDate = @StartDate

	IF @Interval < 0
	BEGIN
		SELECT @EndDate = CalendarDate
		FROM (
			SELECT CalendarDate, ROW_NUMBER() OVER(ORDER BY CalendarDate DESC) RowNumber
			FROM dbo.SystemCalendar
			WHERE SystemCalendar.IsWorkday = 1
				  AND SystemCalendar.CalendarDate < @StartDate
				  AND SystemCalendar.CalendarDate > DATEADD(MONTH, -1, @StartDate)
		) X
		WHERE X.RowNumber = -@Interval
	END
	ELSE IF @Interval > 0
	BEGIN
		SELECT @EndDate = CalendarDate
		FROM (
			SELECT CalendarDate, ROW_NUMBER() OVER(ORDER BY CalendarDate) RowNumber
			FROM dbo.SystemCalendar
			WHERE SystemCalendar.IsWorkday = 1
				  AND SystemCalendar.CalendarDate > @StartDate
				  AND SystemCalendar.CalendarDate < DATEADD(MONTH, 1, @StartDate)
		) X
		WHERE X.RowNumber = @Interval
	END

	RETURN @EndDate
END
GO
