/****** Object:  UserDefinedFunction [dbo].[ufn_GetDateTimeFormat]    Script Date: 12/12/2023 11:23:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[ufn_GetDateTimeFormat]
/***********************************************************
* Function description:
* Date		: 24/08/2022
* Author	: Fauzan
*
* Changes
* Date		Modified By			Comments
************************************************************
*
************************************************************/
(@ID int)
RETURNS VARCHAR(MAX)
BEGIN
	DECLARE @retValue VARCHAR(MAX)
	DECLARE @sysParamValue VARCHAR(MAX)
	DECLARE @mDateID BIGINT
	DECLARE @mTimeID BIGINT
	DECLARE @dateFormat VARCHAR(255)
	DECLARE @timeFormat VARCHAR(255)

	SELECT @sysParamValue = SettingValue FROM SystemParameter WHERE PK_SystemParameter_ID = @ID
	SET @mDateID = CAST(LEFT(@sysParamValue, LEN(@sysParamValue) - CHARINDEX(',',@sysParamValue)) AS BIGINT)
	SET @mTimeID = CAST(RIGHT(@sysParamValue, LEN(@sysParamValue) - CHARINDEX(',',@sysParamValue)) AS BIGINT)
	SELECT @dateFormat = SQLFormat FROM MDateFormat WHERE PK_DateFormat_ID = @mDateID
	SELECT @timeFormat = SQLFormat FROM MTimeFormat WHERE PK_TimeFormat_ID = @mTimeID
	SET @retValue = ISNULL(@dateFormat, 'mm/dd/yy') + ' ' + ISNULL(@timeFormat, 'HH:mm')

	RETURN @retValue
END

GO
