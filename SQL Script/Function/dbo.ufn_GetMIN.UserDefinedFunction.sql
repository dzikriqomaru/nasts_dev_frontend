/****** Object:  UserDefinedFunction [dbo].[ufn_GetMIN]    Script Date: 12/12/2023 11:23:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[ufn_GetMIN]
(@Param1 FLOAT, @Param2 FLOAT)
RETURNS FLOAT 
AS
BEGIN
	RETURN CASE WHEN @Param1 < @Param2 THEN @Param1 ELSE @Param2 END
END
GO
