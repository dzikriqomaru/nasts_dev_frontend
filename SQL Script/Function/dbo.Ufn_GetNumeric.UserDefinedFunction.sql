/****** Object:  UserDefinedFunction [dbo].[Ufn_GetNumeric]    Script Date: 12/12/2023 11:23:55 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[Ufn_GetNumeric](@StrValue VARCHAR(30))    
RETURNS NUMERIC(18, 2)
AS    
BEGIN    
	Declare @IntReturn AS NUMERIC(18, 2)    
	SET @IntReturn = NULL
 
	IF ISNUMERIC(@StrValue) = 1 
	BEGIN    
		SET @StrValue = LTRIM(RTRIM(STR(@StrValue, 25)))
		SET @IntReturn = CONVERT(NUMERIC(18, 2), @strvalue)    
	END    
    
	RETURN @IntReturn    
END    

GO
