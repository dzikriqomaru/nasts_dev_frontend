
BEGIN TRY
    BEGIN TRANSACTION;
	-- Clean up existing
	truncate table NASTS_ClientProjectSLA
	truncate table NASTS_ClientProjectUserInternal
	truncate table NASTS_ClientProjectUserExternal

	INSERT INTO [dbo].[NASTS_ClientProject]
               ([FK_Client_ID]
               ,[Product]
               ,[SubProduct]
               ,[ProjectName]
               ,[FK_MaintenanceType_ID]
               ,[Active]
               ,[CreatedBy]
               ,[LastUpdateBy]
               ,[CreatedDate]
               ,[LastUpdateDate]
               ,[Alternateby]
               ,[Draft])
	    SELECT  c.PK_Client_ID
               ,p.PK_Ms_Product_ID
               ,sp.PK_Ms_SubProduct_ID
               ,CONCAT(c.ClientCode,' ',p.Product_Code,' ',
                 CASE
                     WHEN LEN(sp.Sub_Product_Name) > 17 THEN sp.SubProduct_Code
                     ELSE isnull(sp.Sub_Product_Name, sp.SubProduct_Code)
                 END
                ) AS ProjectName
         	   ,1 as maintenance_type
		       ,1
		       ,'MIGRATION'
		       ,'MIGRATION'
		       ,GETDATE()
		       ,GETDATE()
		       ,''
		       ,0
           FROM ProjectMigration mdb
           join NASTS_Client c on c.ClientCode = mdb.customer and mdb.customer NOT IN ('SCB', 'SB','HANA','BSI') 
           join NASTS_Ms_Product p on p.Product_Code = mdb.Department
           join NASTS_Ms_SubProduct sp on sp.FK_Ms_Product_ID = p.PK_Ms_Product_ID and sp.SubProduct_Code = mdb.SubProduct
           where not exists (select 1 from NASTS_ClientProject where FK_Client_ID = c.PK_Client_ID and Product = p.PK_Ms_Product_ID and SubProduct = sp.PK_Ms_SubProduct_ID and FK_MaintenanceType_ID = 1)
           GROUP BY c.ClientCode, p.Product_Code,sp.Sub_Product_Name, c.PK_Client_ID, p.PK_Ms_Product_ID, sp.PK_Ms_SubProduct_ID, sp.SubProduct_Code
		   --Iregular 19
		INSERT INTO [dbo].[NASTS_ClientProject]
		           ([FK_Client_ID]
		           ,[Product]
		           ,[SubProduct]
		           ,[ProjectName]
		           ,[FK_MaintenanceType_ID]
		           ,[Active]
		           ,[CreatedBy]
		           ,[LastUpdateBy]
		           ,[CreatedDate]
		           ,[LastUpdateDate]
		           ,[Alternateby]
		           ,[Draft])
		    SELECT  c.PK_Client_ID
		           ,p.PK_Ms_Product_ID
		           ,sp.PK_Ms_SubProduct_ID
		           ,CONCAT(c.ClientCode,' ',p.Product_Code,' ',
		             CASE
		                 WHEN LEN(sp.Sub_Product_Name) > 17 THEN sp.SubProduct_Code
		                 ELSE isnull(sp.Sub_Product_Name, sp.SubProduct_Code)
		             END
		            ) AS ProjectName
		     	   ,2 as maintenance_type
			       ,1
			       ,'MIGRATION'
			       ,'MIGRATION'
			       ,GETDATE()
			       ,GETDATE()
			       ,''
			       ,0
			FROM ProjectMigration mdb
			join NASTS_Client c on c.ClientCode = mdb.customer and (mdb.customer = 'SCB' AND mdb.department = 'ONELOAN')
			join NASTS_Ms_Product p on p.Product_Code = mdb.Department
			join NASTS_Ms_SubProduct sp on sp.FK_Ms_Product_ID = p.PK_Ms_Product_ID and sp.SubProduct_Code = mdb.SubProduct
			where not exists (select 1 from NASTS_ClientProject where FK_Client_ID = c.PK_Client_ID and Product = p.PK_Ms_Product_ID and SubProduct = sp.PK_Ms_SubProduct_ID and		FK_MaintenanceType_ID = 2)
			GROUP BY c.ClientCode, p.Product_Code,sp.Sub_Product_Name, c.PK_Client_ID, p.PK_Ms_Product_ID, sp.PK_Ms_SubProduct_ID, sp.SubProduct_Code
		
		--Iregular 24
		INSERT INTO [dbo].[NASTS_ClientProject]
		           ([FK_Client_ID]
		           ,[Product]
		           ,[SubProduct]
		           ,[ProjectName]
		           ,[FK_MaintenanceType_ID]
		           ,[Active]
		           ,[CreatedBy]
		           ,[LastUpdateBy]
		           ,[CreatedDate]
		           ,[LastUpdateDate]
		           ,[Alternateby]
		           ,[Draft])
			SELECT  c.PK_Client_ID
		           ,p.PK_Ms_Product_ID
		           ,sp.PK_Ms_SubProduct_ID
		           ,CONCAT(c.ClientCode,' ',p.Product_Code,' ',
		             CASE
		                 WHEN LEN(sp.Sub_Product_Name) > 17 THEN sp.SubProduct_Code
		                 ELSE isnull(sp.Sub_Product_Name, sp.SubProduct_Code)
		             END
		            ) AS ProjectName
		     	   ,3 as maintenance_type
			       ,1
			       ,'MIGRATION'
			       ,'MIGRATION'
			       ,GETDATE()
			       ,GETDATE()
			       ,''
			       ,0
			FROM ProjectMigration mdb
			join NASTS_Client c on c.ClientCode = mdb.customer and (mdb.customer IN ('SB','HANA','BSI') OR (mdb.customer = 'SCB' AND mdb.department <> 'ONELOAN'))
			join NASTS_Ms_Product p on p.Product_Code = mdb.Department
			join NASTS_Ms_SubProduct sp on sp.FK_Ms_Product_ID = p.PK_Ms_Product_ID and sp.SubProduct_Code = mdb.SubProduct
			where not exists (select 1 from NASTS_ClientProject cp where cp.FK_Client_ID = c.PK_Client_ID and cp.Product = p.PK_Ms_Product_ID and cp.SubProduct = sp.PK_Ms_SubProduct_ID and	FK_MaintenanceType_ID = 3)
			GROUP BY c.ClientCode, p.Product_Code,sp.Sub_Product_Name, c.PK_Client_ID, p.PK_Ms_Product_ID, sp.PK_Ms_SubProduct_ID, sp.SubProduct_Code

	DECLARE @ClientProjectName VARCHAR(100), @MaintenanceTypeID bigint;
	
	-- Declare a cursor to loop through the result set
	DECLARE curProjects CURSOR FOR
	SELECT PK_Client_Project_ID,FK_MaintenanceType_ID
	FROM NASTS_ClientProject;
	
	-- Open the cursor
	OPEN curProjects;
	
	-- Fetch the first row
	FETCH NEXT FROM curProjects INTO @ClientProjectName,@MaintenanceTypeID
	
	-- Loop through the result set
	WHILE @@FETCH_STATUS = 0
	BEGIN
	    -- Execute the stored procedure with the current parameter values
	    IF EXISTS(SELECT 1 FROM NASTS_ClientProject AS CLIPro WHERE CLIPro.ProjectName = @ClientProjectName)
		BEGIN
	
			INSERT INTO NASTS_ClientProjectSLA (
				 [FK_Client_Project_ID]
				,[Level]
				,[FK_Ms_Severity_ID]
				,[Internal]
				,[TemporaryMinutes]
				,[PermanentMinutes]
				,[Active]
				,[CreatedBy]
				,[LastUpdateBy]
				,[CreatedDate]
				,[LastUpdateDate]
				,[Alternateby]
				,[Draft])
			SELECT 
				 @ClientProjectID
				,SLALevel
				,FK_Ms_Severity_ID
				,1
				,Temporary_Minutes
				,Permanent_Minutes
				,1
				,'MIGRATION'
				,'MIGRATION'
				,GETDATE()
				,GETDATE()
				,''
				,0
			FROM	[dbo].NASTS_Ms_MaintenanceSLA where FK_MaintenanceType_ID = @MaintenanceTypeID
			RETURN;
		 END
	
	    -- Fetch the next row
	    FETCH NEXT FROM curProjects INTO @ClientProjectID, @MaintenanceTypeID;
	END;
	
	-- Close and deallocate the cursor
	CLOSE curProjects;
	DEALLOCATE curProjects;

	select * from NASTS_ClientProject, NASTS_ClientProjectSLA
    IF @@TRANCOUNT > 0
	BEGIN
         COMMIT TRANSACTION;
	END

END TRY
BEGIN CATCH
    IF @@TRANCOUNT > 0
        ROLLBACK TRANSACTION; -- Roll back if an error occurs
    -- Optionally, log or raise the error
	PRINT('An error occurred during the transaction. Details: ' + ERROR_MESSAGE());
	PRINT 'Error occurred on line: ' + CAST(ERROR_LINE() AS NVARCHAR(10));
END CATCH;