USE [NASTS]
GO
/* 
	FOR PROJECT SLA:
	DROP TABLE IF EXISTS #tempmastersla
	SELECT
	    PARSENAME(REPLACE(projectname, ' ', '.'), 3) AS ClientCode,
	    PARSENAME(REPLACE(projectname, ' ', '.'), 2) AS Product,
	    PARSENAME(REPLACE(projectname, ' ', '.'), 1) AS Subproduct,
		projectname
		into #tempmastersla
	from masterclientSLAmigration

	DROP TABLE IF EXISTS #tempsla
	SELECT
	    projectname,
	    CASE
	        WHEN severity = 'permanentsolutionCritical' THEN 1
	        WHEN severity = 'permanentsolutionHigh'		THEN 2
	        WHEN severity = 'permanentsolutionMedium'	THEN 3
	        WHEN severity = 'permanentsolutionLow'		THEN 4
	        ELSE severity
	    END AS FK_Ms_Severity_ID,
	    CASE
	        WHEN severity = 'permanentsolutionCritical' THEN TemporarysolutionCritical
	        WHEN severity = 'permanentsolutionHigh'		THEN TemporarysolutionHigh
	        WHEN severity = 'permanentsolutionMedium'	THEN TemporarysolutionMedium
	        WHEN severity = 'permanentsolutionLow'		THEN TemporarysolutionLow
	        ELSE NULL
	    END AS TemporarySolution,
	    CAST(CAST(SUBSTRING(VALUE, 1, CHARINDEX('*', VALUE) - 1) AS FLOAT)*CAST(SUBSTRING(VALUE, CHARINDEX('*', VALUE) + 1, LEN(VALUE)) AS FLOAT) AS INT) AS PermanenSolution
	INTO #tempsla
	FROM masterclientSLAmigration
	UNPIVOT (
	    value FOR severity IN (
	        permanentsolutionCritical,
	        permanentsolutionHigh,
	        permanentsolutionMedium,
	        permanentsolutionLow
	    )
	) AS unpivoted_temp;

	IF YOU FOUND ERROR :
		Invalid object name '#TESTING'.
	RUN THIS SELECTED QUERY FIRST:

	DROP TABLE IF EXISTS #TESTING
	DECLARE @separator NVARCHAR(4) = N'||'; -- Unicode separator
	SELECT
	  ticketno,
	  TRIM(value) AS noteprogress
	 into #TESTING
	FROM migration
	CROSS APPLY STRING_SPLIT(REPLACE(noteprogress, @separator, NCHAR(9999)), NCHAR(9999))
	WHERE LEN(TRIM(value)) > 0;

	(IF AN ERROR OCCURED, IGNORED IT AND RUN IT ONCE MORE)

	IF YOU FOUND ERROR :
		Msg 208, Level 16, State 1, Line 13
		Invalid object name 'STRING_SPLIT'.
	then its a problem of database compability. to fix this run the folloing query:
	-- 1. check the current level
		SELECT compatibility_level
		FROM sys.databases
		WHERE name = 'NASTS';
	-- 2. update the curent compability level to database that can run STRING_SPLIT (sql server 2016)
		USE master;
		GO
		ALTER DATABASE NASTS SET COMPATIBILITY_LEVEL = 130;
	-- 3. recheck current level (do step 1)
	-- 4. now run the query above to solve Invalid object name '#TESTING'.

*/
BEGIN TRY
    BEGIN TRANSACTION;
	-- Clean up existing
	truncate table NASTS_ClientProjectSLA
	truncate table NASTS_ClientProjectUserInternal
	truncate table NASTS_ClientProjectUserExternal
	TRUNCATE TABLE NASTS_TicketIssue;
	TRUNCATE TABLE NASTS_TicketIssueSLA;
	TRUNCATE TABLE NASTS_TicketIssueAssignment;
	TRUNCATE TABLE NASTS_TicketIssueDocument;
	TRUNCATE TABLE NASTS_TicketIssueThread;
	TRUNCATE TABLE NASTS_TicketIssueLog;
	TRUNCATE TABLE NASTS_TicketIssueBounceLog;
	TRUNCATE TABLE NASTS_TicketIssueNotes;
	
	
	-- Inserting Migration data to Client Project
	--insert from Project Migration
	INSERT INTO [dbo].[NASTS_ClientProject]
               ([FK_Client_ID]
               ,[Product]
               ,[SubProduct]
               ,[ProjectName]
               ,[FK_MaintenanceType_ID]
               ,[Active]
               ,[CreatedBy]
               ,[LastUpdateBy]
               ,[CreatedDate]
               ,[LastUpdateDate]
               ,[Alternateby]
               ,[Draft])
	    SELECT  c.PK_Client_ID
               ,p.PK_Ms_Product_ID
               ,sp.PK_Ms_SubProduct_ID
               ,CONCAT(c.ClientCode,' ',p.Product_Code,' ',
                 CASE
                     WHEN LEN(sp.Sub_Product_Name) > 17 THEN sp.SubProduct_Code
                     ELSE isnull(sp.Sub_Product_Name, sp.SubProduct_Code)
                 END
                ) AS ProjectName
         	   ,1 as maintenance_type
		       ,1
		       ,'MIGRATION'
		       ,'MIGRATION'
		       ,GETDATE()
		       ,GETDATE()
		       ,''
		       ,0
           FROM ProjectMigration mdb
           join NASTS_Client c on c.ClientCode = mdb.customer and mdb.customer NOT IN ('SCB', 'SB','HANA','BSI') 
           join NASTS_Ms_Product p on p.Product_Code = mdb.Department
           join NASTS_Ms_SubProduct sp on sp.FK_Ms_Product_ID = p.PK_Ms_Product_ID and sp.SubProduct_Code = mdb.SubProduct
           where not exists (select 1 from NASTS_ClientProject where FK_Client_ID = c.PK_Client_ID and Product = p.PK_Ms_Product_ID and SubProduct = sp.PK_Ms_SubProduct_ID and FK_MaintenanceType_ID = 1)
           GROUP BY c.ClientCode, p.Product_Code,sp.Sub_Product_Name, c.PK_Client_ID, p.PK_Ms_Product_ID, sp.PK_Ms_SubProduct_ID, sp.SubProduct_Code
		   --Iregular 19
	INSERT INTO [dbo].[NASTS_ClientProject]
	           ([FK_Client_ID]
	           ,[Product]
	           ,[SubProduct]
	           ,[ProjectName]
	           ,[FK_MaintenanceType_ID]
	           ,[Active]
	           ,[CreatedBy]
	           ,[LastUpdateBy]
	           ,[CreatedDate]
	           ,[LastUpdateDate]
	           ,[Alternateby]
	           ,[Draft])
	    SELECT  c.PK_Client_ID
	           ,p.PK_Ms_Product_ID
	           ,sp.PK_Ms_SubProduct_ID
	           ,CONCAT(c.ClientCode,' ',p.Product_Code,' ',
	             CASE
	                 WHEN LEN(sp.Sub_Product_Name) > 17 THEN sp.SubProduct_Code
	                 ELSE isnull(sp.Sub_Product_Name, sp.SubProduct_Code)
	             END
	            ) AS ProjectName
	     	   ,2 as maintenance_type
		       ,1
		       ,'MIGRATION'
		       ,'MIGRATION'
		       ,GETDATE()
		       ,GETDATE()
		       ,''
		       ,0
		FROM ProjectMigration mdb
		join NASTS_Client c on c.ClientCode = mdb.customer and (mdb.customer = 'SCB' AND mdb.department = 'ONELOAN')
		join NASTS_Ms_Product p on p.Product_Code = mdb.Department
		join NASTS_Ms_SubProduct sp on sp.FK_Ms_Product_ID = p.PK_Ms_Product_ID and sp.SubProduct_Code = mdb.SubProduct
		where not exists (select 1 from NASTS_ClientProject where FK_Client_ID = c.PK_Client_ID and Product = p.PK_Ms_Product_ID and SubProduct = sp.PK_Ms_SubProduct_ID and		FK_MaintenanceType_ID = 2)
		GROUP BY c.ClientCode, p.Product_Code,sp.Sub_Product_Name, c.PK_Client_ID, p.PK_Ms_Product_ID, sp.PK_Ms_SubProduct_ID, sp.SubProduct_Code
	
	--Iregular 24
	INSERT INTO [dbo].[NASTS_ClientProject]
	           ([FK_Client_ID]
	           ,[Product]
	           ,[SubProduct]
	           ,[ProjectName]
	           ,[FK_MaintenanceType_ID]
	           ,[Active]
	           ,[CreatedBy]
	           ,[LastUpdateBy]
	           ,[CreatedDate]
	           ,[LastUpdateDate]
	           ,[Alternateby]
	           ,[Draft])
		SELECT  c.PK_Client_ID
	           ,p.PK_Ms_Product_ID
	           ,sp.PK_Ms_SubProduct_ID
	           ,CONCAT(c.ClientCode,' ',p.Product_Code,' ',
	             CASE
	                 WHEN LEN(sp.Sub_Product_Name) > 17 THEN sp.SubProduct_Code
	                 ELSE isnull(sp.Sub_Product_Name, sp.SubProduct_Code)
	             END
	            ) AS ProjectName
	     	   ,3 as maintenance_type
		       ,1
		       ,'MIGRATION'
		       ,'MIGRATION'
		       ,GETDATE()
		       ,GETDATE()
		       ,''
		       ,0
		FROM ProjectMigration mdb
		join NASTS_Client c on c.ClientCode = mdb.customer and (mdb.customer IN ('SB','HANA','BSI') OR (mdb.customer = 'SCB' AND mdb.department <> 'ONELOAN'))
		join NASTS_Ms_Product p on p.Product_Code = mdb.Department
		join NASTS_Ms_SubProduct sp on sp.FK_Ms_Product_ID = p.PK_Ms_Product_ID and sp.SubProduct_Code = mdb.SubProduct
		where not exists (select 1 from NASTS_ClientProject cp where cp.FK_Client_ID = c.PK_Client_ID and cp.Product = p.PK_Ms_Product_ID and cp.SubProduct =sp.PK_Ms_SubProduct_ID and	FK_MaintenanceType_ID = 3)
		GROUP BY c.ClientCode, p.Product_Code,sp.Sub_Product_Name, c.PK_Client_ID, p.PK_Ms_Product_ID, sp.PK_Ms_SubProduct_ID, sp.SubProduct_Code

	-- insert from master SLA
	INSERT INTO [dbo].[NASTS_ClientProject]
               ([FK_Client_ID]
               ,[Product]
               ,[SubProduct]
               ,[ProjectName]
               ,[FK_MaintenanceType_ID]
               ,[Active]
               ,[CreatedBy]
               ,[LastUpdateBy]
               ,[CreatedDate]
               ,[LastUpdateDate]
               ,[Alternateby]
               ,[Draft])
	    SELECT  c.PK_Client_ID
               ,p.PK_Ms_Product_ID
               ,sp.PK_Ms_SubProduct_ID
               ,CONCAT(c.ClientCode,' ',p.Product_Code,' ',
                 CASE
                     WHEN LEN(sp.Sub_Product_Name) > 17 THEN sp.SubProduct_Code
                     ELSE isnull(sp.Sub_Product_Name, sp.SubProduct_Code)
                 END
                ) AS ProjectName
         	   ,1 as maintenance_type
		       ,1
		       ,'MIGRATION'
		       ,'MIGRATION'
		       ,GETDATE()
		       ,GETDATE()
		       ,''
		       ,0
           FROM #tempmastersla mdb
           join NASTS_Client c on c.ClientCode = mdb.clientcode and mdb.clientcode NOT IN ('SCB', 'SB','HANA','BSI') 
           join NASTS_Ms_Product p on p.Product_Code = mdb.product
           join NASTS_Ms_SubProduct sp on sp.FK_Ms_Product_ID = p.PK_Ms_Product_ID and sp.SubProduct_Code = mdb.SubProduct
           where not exists (select 1 from NASTS_ClientProject where FK_Client_ID = c.PK_Client_ID and Product = p.PK_Ms_Product_ID and SubProduct = sp.PK_Ms_SubProduct_ID and FK_MaintenanceType_ID = 1)
           GROUP BY c.ClientCode, p.Product_Code,sp.Sub_Product_Name, c.PK_Client_ID, p.PK_Ms_Product_ID, sp.PK_Ms_SubProduct_ID, sp.SubProduct_Code
		   --Iregular 19
	INSERT INTO [dbo].[NASTS_ClientProject]
	           ([FK_Client_ID]
	           ,[Product]
	           ,[SubProduct]
	           ,[ProjectName]
	           ,[FK_MaintenanceType_ID]
	           ,[Active]
	           ,[CreatedBy]
	           ,[LastUpdateBy]
	           ,[CreatedDate]
	           ,[LastUpdateDate]
	           ,[Alternateby]
	           ,[Draft])
	    SELECT  c.PK_Client_ID
	           ,p.PK_Ms_Product_ID
	           ,sp.PK_Ms_SubProduct_ID
	           ,CONCAT(c.ClientCode,' ',p.Product_Code,' ',
	             CASE
	                 WHEN LEN(sp.Sub_Product_Name) > 17 THEN sp.SubProduct_Code
	                 ELSE isnull(sp.Sub_Product_Name, sp.SubProduct_Code)
	             END
	            ) AS ProjectName
	     	   ,2 as maintenance_type
		       ,1
		       ,'MIGRATION'
		       ,'MIGRATION'
		       ,GETDATE()
		       ,GETDATE()
		       ,''
		       ,0
        FROM #tempmastersla mdb
        join NASTS_Client c on c.ClientCode = mdb.clientcode and (mdb.clientcode = 'SCB' AND mdb.product = 'ONELOAN')
		join NASTS_Ms_Product p on p.Product_Code = mdb.product
		join NASTS_Ms_SubProduct sp on sp.FK_Ms_Product_ID = p.PK_Ms_Product_ID and sp.SubProduct_Code = mdb.SubProduct
		where not exists (select 1 from NASTS_ClientProject where FK_Client_ID = c.PK_Client_ID and Product = p.PK_Ms_Product_ID and SubProduct = sp.PK_Ms_SubProduct_ID and		FK_MaintenanceType_ID = 2)
		GROUP BY c.ClientCode, p.Product_Code,sp.Sub_Product_Name, c.PK_Client_ID, p.PK_Ms_Product_ID, sp.PK_Ms_SubProduct_ID, sp.SubProduct_Code
	
	--Iregular 24
	INSERT INTO [dbo].[NASTS_ClientProject]
	           ([FK_Client_ID]
	           ,[Product]
	           ,[SubProduct]
	           ,[ProjectName]
	           ,[FK_MaintenanceType_ID]
	           ,[Active]
	           ,[CreatedBy]
	           ,[LastUpdateBy]
	           ,[CreatedDate]
	           ,[LastUpdateDate]
	           ,[Alternateby]
	           ,[Draft])
		SELECT  c.PK_Client_ID
	           ,p.PK_Ms_Product_ID
	           ,sp.PK_Ms_SubProduct_ID
	           ,CONCAT(c.ClientCode,' ',p.Product_Code,' ',
	             CASE
	                 WHEN LEN(sp.Sub_Product_Name) > 17 THEN sp.SubProduct_Code
	                 ELSE isnull(sp.Sub_Product_Name, sp.SubProduct_Code)
	             END
	            ) AS ProjectName
	     	   ,3 as maintenance_type
		       ,1
		       ,'MIGRATION'
		       ,'MIGRATION'
		       ,GETDATE()
		       ,GETDATE()
		       ,''
		       ,0
		FROM #tempmastersla mdb
		join NASTS_Client c on c.ClientCode = mdb.ClientCode and (mdb.ClientCode IN ('SB','HANA','BSI') OR (mdb.ClientCode = 'SCB' AND mdb.product <> 'ONELOAN'))
		join NASTS_Ms_Product p on p.Product_Code = mdb.product
		join NASTS_Ms_SubProduct sp on sp.FK_Ms_Product_ID = p.PK_Ms_Product_ID and sp.SubProduct_Code = mdb.SubProduct
		where not exists (select 1 from NASTS_ClientProject cp where cp.FK_Client_ID = c.PK_Client_ID and cp.Product = p.PK_Ms_Product_ID and cp.SubProduct =sp.PK_Ms_SubProduct_ID and	FK_MaintenanceType_ID = 3)
		GROUP BY c.ClientCode, p.Product_Code,sp.Sub_Product_Name, c.PK_Client_ID, p.PK_Ms_Product_ID, sp.PK_Ms_SubProduct_ID, sp.SubProduct_Code
	-- insert from tiket migration
    INSERT INTO [dbo].[NASTS_ClientProject]
               ([FK_Client_ID]
               ,[Product]
               ,[SubProduct]
               ,[ProjectName]
               ,[FK_MaintenanceType_ID]
               ,[Active]
               ,[CreatedBy]
               ,[LastUpdateBy]
               ,[CreatedDate]
               ,[LastUpdateDate]
               ,[Alternateby]
               ,[Draft])
	    SELECT  c.PK_Client_ID
               ,p.PK_Ms_Product_ID
               ,sp.PK_Ms_SubProduct_ID
               ,CONCAT(c.ClientCode,' ',p.Product_Code,' ',
                 CASE
                     WHEN LEN(sp.Sub_Product_Name) > 17 THEN sp.SubProduct_Code
                     ELSE isnull(sp.Sub_Product_Name, sp.SubProduct_Code)
                 END
                ) AS ProjectName
         	   ,1 as maintenance_type
		       ,1
		       ,'MIGRATION'
		       ,'MIGRATION'
		       ,GETDATE()
		       ,GETDATE()
		       ,''
		       ,0
           FROM [Migration] mdb
           join NASTS_Client c on c.ClientCode = mdb.BANK and mdb.BANK NOT IN ('SCB', 'SB','HANA','BSI') 
           join NASTS_Ms_Product p on p.Product_Code = mdb.Product
           join NASTS_Ms_SubProduct sp on sp.FK_Ms_Product_ID = p.PK_Ms_Product_ID and sp.SubProduct_Code = mdb.SubProduct
           where not exists (select 1 from NASTS_ClientProject where FK_Client_ID = c.PK_Client_ID and Product = p.PK_Ms_Product_ID and SubProduct = sp.PK_Ms_SubProduct_ID and FK_MaintenanceType_ID = 1)
           GROUP BY c.ClientCode, p.Product_Code,sp.Sub_Product_Name, c.PK_Client_ID, p.PK_Ms_Product_ID, sp.PK_Ms_SubProduct_ID, sp.SubProduct_Code
	
	--Iregular 19
	INSERT INTO [dbo].[NASTS_ClientProject]
               ([FK_Client_ID]
               ,[Product]
               ,[SubProduct]
               ,[ProjectName]
               ,[FK_MaintenanceType_ID]
               ,[Active]
               ,[CreatedBy]
               ,[LastUpdateBy]
               ,[CreatedDate]
               ,[LastUpdateDate]
               ,[Alternateby]
               ,[Draft])
	    SELECT  c.PK_Client_ID
               ,p.PK_Ms_Product_ID
               ,sp.PK_Ms_SubProduct_ID
               ,CONCAT(c.ClientCode,' ',p.Product_Code,' ',
                 CASE
                     WHEN LEN(sp.Sub_Product_Name) > 17 THEN sp.SubProduct_Code
                     ELSE isnull(sp.Sub_Product_Name, sp.SubProduct_Code)
                 END
                ) AS ProjectName
         	   ,2 as maintenance_type
		       ,1
		       ,'MIGRATION'
		       ,'MIGRATION'
		       ,GETDATE()
		       ,GETDATE()
		       ,''
		       ,0
           FROM [dbo].[Migration] mdb
           join NASTS_Client c on c.ClientCode = mdb.BANK  and (BANK = 'SCB' AND Product = 'ONELOAN')
           join NASTS_Ms_Product p on p.Product_Code = mdb.Product
           join NASTS_Ms_SubProduct sp on sp.FK_Ms_Product_ID = p.PK_Ms_Product_ID and sp.SubProduct_Code = mdb.SubProduct
           where not exists (select 1 from NASTS_ClientProject where FK_Client_ID = c.PK_Client_ID and Product = p.PK_Ms_Product_ID and SubProduct = sp.PK_Ms_SubProduct_ID and FK_MaintenanceType_ID = 2)
           GROUP BY c.ClientCode, p.Product_Code,sp.Sub_Product_Name, c.PK_Client_ID, p.PK_Ms_Product_ID, sp.PK_Ms_SubProduct_ID, sp.SubProduct_Code
    
	--Iregular 24
	INSERT INTO [dbo].[NASTS_ClientProject]
               ([FK_Client_ID]
               ,[Product]
               ,[SubProduct]
               ,[ProjectName]
               ,[FK_MaintenanceType_ID]
               ,[Active]
               ,[CreatedBy]
               ,[LastUpdateBy]
               ,[CreatedDate]
               ,[LastUpdateDate]
               ,[Alternateby]
               ,[Draft])
		SELECT  c.PK_Client_ID
               ,p.PK_Ms_Product_ID
               ,sp.PK_Ms_SubProduct_ID
               ,CONCAT(c.ClientCode,' ',p.Product_Code,' ',
                 CASE
                     WHEN LEN(sp.Sub_Product_Name) > 17 THEN sp.SubProduct_Code
                     ELSE isnull(sp.Sub_Product_Name, sp.SubProduct_Code)
                 END
                ) AS ProjectName
         	   ,3 as maintenance_type
		       ,1
		       ,'MIGRATION'
		       ,'MIGRATION'
		       ,GETDATE()
		       ,GETDATE()
		       ,''
		       ,0
           FROM [dbo].[Migration] mdb
           join NASTS_Client c on c.ClientCode = mdb.BANK and (BANK IN ('SB','HANA','BSI') OR (BANK = 'SCB' AND Product <> 'ONELOAN'))
           join NASTS_Ms_Product p on p.Product_Code = mdb.Product
           join NASTS_Ms_SubProduct sp on sp.FK_Ms_Product_ID = p.PK_Ms_Product_ID and sp.SubProduct_Code = mdb.SubProduct
           where not exists (select 1 from NASTS_ClientProject where FK_Client_ID = c.PK_Client_ID and Product = p.PK_Ms_Product_ID and SubProduct = sp.PK_Ms_SubProduct_ID and FK_MaintenanceType_ID = 3)
           GROUP BY c.ClientCode, p.Product_Code,sp.Sub_Product_Name, c.PK_Client_ID, p.PK_Ms_Product_ID, sp.PK_Ms_SubProduct_ID, sp.SubProduct_Code

	--inserting internal SLA from master maintenance type sla
	DECLARE @ClientProjectID bigint, @MaintenanceTypeID bigint;
	
	DECLARE curProjects CURSOR FOR
	SELECT PK_Client_Project_ID,FK_MaintenanceType_ID
	FROM NASTS_ClientProject;
	
	OPEN curProjects;
	
	FETCH NEXT FROM curProjects INTO @ClientProjectID,@MaintenanceTypeID
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
	    IF EXISTS(SELECT 1 FROM NASTS_ClientProject AS CLIPro WHERE CLIPro.PK_Client_Project_ID = @ClientProjectID)
		BEGIN
	
			INSERT INTO NASTS_ClientProjectSLA (
				 [FK_Client_Project_ID]
				,[Level]
				,[FK_Ms_Severity_ID]
				,[Internal]
				,[TemporaryMinutes]
				,[PermanentMinutes]
				,[Active]
				,[CreatedBy]
				,[LastUpdateBy]
				,[CreatedDate]
				,[LastUpdateDate]
				,[Alternateby]
				,[Draft])
			SELECT 
				 @ClientProjectID
				,SLALevel
				,FK_Ms_Severity_ID
				,1
				,Temporary_Minutes
				,Permanent_Minutes
				,1
				,'MIGRATION'
				,'MIGRATION'
				,GETDATE()
				,GETDATE()
				,''
				,0
			FROM	[dbo].NASTS_Ms_MaintenanceSLA where FK_MaintenanceType_ID = @MaintenanceTypeID
		 END

	    FETCH NEXT FROM curProjects INTO @ClientProjectID, @MaintenanceTypeID;
	END;
	
	CLOSE curProjects;
	DEALLOCATE curProjects;
	
	-- inserting External SLA from MasterProjectSLA
	INSERT INTO NASTS_ClientProjectSLA (
				 [FK_Client_Project_ID]
				,[Level]
				,[FK_Ms_Severity_ID]
				,[Internal]
				,[TemporaryMinutes]
				,[PermanentMinutes]
				,[Active]
				,[CreatedBy]
				,[LastUpdateBy]
				,[CreatedDate]
				,[LastUpdateDate]
				,[Alternateby]
				,[Draft])
		select	PK_Client_Project_ID as FK_Client_Project_ID
				,0 AS Level
				,FK_Ms_Severity_ID
				,0 AS Internal
				,isnull(CAST(CAST(SUBSTRING(TemporarySolution, 1, CHARINDEX('*', TemporarySolution) - 1) AS FLOAT)*CAST(SUBSTRING(TemporarySolution, CHARINDEX('*', TemporarySolution) + 1, LEN(TemporarySolution)) AS FLOAT) AS INT),(select cpsla.TemporaryMinutes from NASTS_ClientProjectSLA cpsla where FK_Client_Project_ID = PK_Client_Project_ID and level = 3 and cpsla.FK_Ms_Severity_ID =  #tempsla.FK_Ms_Severity_ID)) AS TemporaryMinutes
				,PermanenSolution AS PermanentMinutes
				,1
				,'MIGRATION'
				,'MIGRATION'
				,GETDATE()
				,GETDATE()
				,''
				,0 
			from #tempsla JOIN NASTS_ClientProject CP ON CP.ProjectName = #tempsla.PROJECTNAME -- where TemporarySolution is null
    
	-- SELECT * FROM NASTS_ClientProject
	-- delete from NASTS_ClientProject where CreatedBy = 'MIGRATION'
	/*

	-- for reseting Primary key auto increment
	declare @maxpkid int;
	select @maxpkid = MAX(PK_Client_Project_ID) FROM NASTS_ClientProject
	DBCC CHECKIDENT ('NASTS_ClientProject', RESEED, @maxpkid)

	*/

    -- inserting Migration data to Ticket issue after Project Creation
	-- ticket issue header
    INSERT INTO [dbo].[NASTS_TicketIssue]
               ([TicketNo]
               ,[RaiseDate]
               ,[FK_Client_ID]
               ,[FK_Client_Project_ID]
               ,[FK_Ms_Severity_ID]
               ,[FK_Ms_Category_ID]
               ,[IssueTitle]
               ,[IssueDescription]
               ,[IncidentChronology]
               ,[IssueStatus]
               ,[RootCause]
               ,[Solution]
               ,[Rating]
               ,[BreachInternal]
               ,[BreachExternal]
               ,[Active]
               ,[CreatedBy]
               ,[LastUpdateBy]
               ,[CreatedDate]
               ,[LastUpdateDate]
               ,[Alternateby]
               ,[Draft])
        SELECT mdb.[TicketNo]
               ,CAST(CONVERT(VARCHAR(10), mdb.DateRaise, 120) + ' ' + CONVERT(VARCHAR(5), ISNULL(mdb.TimeRaise,'08:30'), 108) AS DATETIME) AS RaiseDate
        	   ,c.PK_Client_ID
               ,cp.PK_Client_Project_ID as FK_Client_Project_ID
               ,s.PK_Ms_Severity_ID
               ,cat.PK_Ms_Category_ID
    		   ,CONCAT('Issue ',c.ClientCode, ' on ',sp.SubProduct_Code, ', [DataMigration]') as IssueTitle
               ,mdb.IssueDescription
    		   ,mdb.IssueDescription
               ,mdb.CurrentStatus
               ,mdb.Rootcause
               ,mdb.Solution
               ,mdb.RatingTicket
               ,mdb.InternalL3StatusBreach
               ,mdb.BreachSLA
               ,1
               ,'MIGRATION'
               ,'MIGRATION'
               ,GETDATE()
               ,GETDATE()
               ,''
               ,0
          FROM [dbo].[Migration] mdb
          JOIN NASTS_Client c ON c.ClientCode = mdb.BANK
          JOIN NASTS_Ms_Product p ON p.Product_Code = mdb.Product
          JOIN NASTS_Ms_SubProduct sp ON p.PK_Ms_Product_ID = sp.FK_Ms_Product_ID AND sp.SubProduct_Code = mdb.SubProduct
          JOIN NASTS_ClientProject cp ON p.PK_Ms_Product_ID = cp.Product AND sp.PK_Ms_SubProduct_ID = cp.SubProduct AND c.PK_Client_ID = cp.FK_Client_ID
          JOIN NASTS_Ms_Severity s ON s.Severity_Name = SUBSTRING(ISNULL(mdb.FinalSeverityCategory,mdb.Severity), 3,LEN(ISNULL(mdb.FinalSeverityCategory,mdb.Severity)))
          JOIN NASTS_Ms_Category cat ON cat.CategoryName = mdb.Category
          LEFT JOIN NASTS_Ms_Rating r ON r.PK_Ms_Rating_ID = mdb.RatingTicket

	-- SELECT * FROM [Migration]

	-- Ticket issue SLA
	INSERT INTO [dbo].[NASTS_TicketIssueSLA]
           ([FK_Ticket_Issue_ID]
           ,[TicketNo]
           ,[Level]
           ,[StartDateTime]
           ,[InternalPermanentSolution]
           ,[ExternalTemporarySolution]
           ,[ExternalPermanentSolution]
		   ,[Active]
		   ,[CreatedBy]
		   ,[LastUpdateBy]
		   ,[CreatedDate]
		   ,[LastUpdateDate]
		   ,[Alternateby]
		   ,[Draft])
	SELECT  ti.PK_Ticket_Issue_ID
		   ,UnpivotedData.TicketNo
		   ,CASE
				WHEN Level = 'InternalL1DateTime' THEN 1
				WHEN Level = 'InternalL2DateTime' THEN 2
				WHEN Level = 'InternalL3DateTime' THEN 3
			END AS Level
		   ,StartDateTime
		   ,InternalPermanentSolution
		   ,ExternalTemporarySolution
		   ,ExternalPermanentSolution
           ,1
           ,'MIGRATION'
           ,'MIGRATION'
           ,GETDATE()
           ,GETDATE()
           ,''
           ,0
	FROM (
	    SELECT  TicketNo
			   ,CAST(CONVERT(VARCHAR(10), DateRaise, 120) + ' ' + CONVERT(VARCHAR(5), ISNULL(TimeRaise,'08:30'), 108) AS DATETIME) as StartDateTime
			   ,CAST(CONVERT(VARCHAR(10), InternalL1Date, 120) + ' ' + CONVERT(VARCHAR(5), InternalL1Time, 108) AS DATETIME) as InternalL1DateTime
			   ,CAST(CONVERT(VARCHAR(10), InternalL2Date, 120) + ' ' + CONVERT(VARCHAR(5), InternalL2Time, 108) AS DATETIME) as InternalL2DateTime
			   ,CAST(CONVERT(VARCHAR(10), InternalL3Date, 120) + ' ' + CONVERT(VARCHAR(5), InternalL3Time, 108) AS DATETIME) as InternalL3DateTime
			   ,CAST(CONVERT(VARCHAR(10), ExternalTemporarySolutionDate, 120) + ' ' + CONVERT(VARCHAR(5), ExternalTemporarySolutionTime, 108) AS DATETIME) as ExternalTemporarySolution
			   ,CAST(CONVERT(VARCHAR(10), ExternalPermanentSolutionDate, 120) + ' ' + CONVERT(VARCHAR(5), ExternalPermanentSolutionTime, 108) AS DATETIME) as ExternalPermanentSolution
	    FROM Migration
	) AS SourceTable
	UNPIVOT (
	    InternalPermanentSolution FOR Level IN (InternalL1DateTime, InternalL2DateTime,InternalL3DateTime)
	) AS UnpivotedData
	JOIN NASTS_TicketIssue ti ON ti.TicketNo = UnpivotedData.TicketNo
	-- ticket issue Log (for report)
	
	INSERT INTO NASTS_TicketIssueLog (
			 FK_Ticket_Issue_ID
			,LogName
			,LogDescription
			,FK_MUser_ID
			,Date
		    ,[Active]
		    ,[CreatedBy]
		    ,[LastUpdateBy]
		    ,[CreatedDate]
		    ,[LastUpdateDate]
		    ,[Alternateby]
		    ,[Draft])
	SELECT	 ti.PK_Ticket_Issue_ID
			,'MOVEMENT'
			,'Ticket Status Updated - INTERNAL TEST to DONE'
			,1
			,t.SolvedDate
            ,1
            ,'MIGRATION'
            ,'MIGRATION'
            ,GETDATE()
            ,GETDATE()
            ,''
            ,0
	   FROM Migration t join NASTS_TicketIssue ti on t.TicketNo = ti.TicketNo
	   where t.SolvedDate is not null
	   
	-- ticket issue notes
	IF OBJECT_ID('tempdb..#TESTING') IS NULL
	BEGIN
		RAISERROR('Please create the #TESTING temp table at the top of this file', 16, 1);
	END

	INSERT INTO [dbo].[NASTS_TicketIssueNotes]
		       ([NotesDescription]
		       ,[NotesDate]
		       ,[FK_TicketIssue_ID]
		       ,[Active]
		       ,[CreatedBy]
		       ,[LastUpdateBy]
		       ,[CreatedDate]
		       ,[LastUpdateDate]
		       ,[Alternateby]
		       ,[Draft])
	SELECT t.noteprogress
		  ,GETDATE()
		  ,ti.PK_Ticket_Issue_ID
          ,1
          ,'MIGRATION'
          ,'MIGRATION'
          ,GETDATE()
          ,GETDATE()
          ,''
          ,0
	FROM #TESTING t join NASTS_TicketIssue ti on t.TicketNo = ti.TicketNo

	DROP TABLE IF EXISTS [dbo].[MasterClientSLAMigration]
	DROP TABLE IF EXISTS [dbo].[Migration]
	DROP TABLE IF EXISTS [dbo].[ProjectMigration]

    IF @@TRANCOUNT > 0
	BEGIN
         COMMIT TRANSACTION;
	END

END TRY
BEGIN CATCH
    IF @@TRANCOUNT > 0
        ROLLBACK TRANSACTION; -- Roll back if an error occurs
    -- Optionally, log or raise the error
	PRINT('An error occurred during the transaction. Details: ' + ERROR_MESSAGE());
	PRINT 'Error occurred on line: ' + CAST(ERROR_LINE() AS NVARCHAR(10));
END CATCH;