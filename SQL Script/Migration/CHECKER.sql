SELECT mdb.SubProduct
  FROM [NASTS_DEV].[dbo].[Migration] mdb
  left join NASTS_Ms_SubProduct sp on sp.SubProduct_Code = mdb.SubProduct
  where sp.SubProduct_Code is null
group by mdb.SubProduct

/*
update Migration
set SubProduct = 'CFD'
where SubProduct = 'CRS'
select * from Migration where SubProduct = 'RWA ATMR'
update NASTS_Ms_SubProduct
set SubProduct_Code = 'RRPM'
where SubProduct_Code = 'RPPM'
select * from NASTS_Ms_SubProduct where SubProduct_Code = 'RRPM'
select * from NASTS_Ms_Product where PK_Ms_Product_ID = 4

select * from NASTS_Client where ClientCode = 'anz'

SELECT mdb.TicketNo, COUNT(*) as Datacount
FROM [NASTS_DEV].[dbo].[Migration] mdb
GROUP BY mdb.TicketNo
HAVING COUNT(*) = 1
order by mdb.TicketNo;
select * from migration where ticketno = 203523

SELECT mdb.TicketNo, COUNT(*) AS DuplicateCount, mdb.DateRaise, mdb.IssueDescription, mdb.CurrentStatus
FROM [NASTS_DEV].[dbo].[Migration] mdb
GROUP BY mdb.TicketNo, mdb.DateRaise, mdb.IssueDescription, mdb.CurrentStatus
HAVING COUNT(*) > 1;

WITH RankedData AS (
    SELECT
        mdb.[TicketNo],
        CAST(CONVERT(VARCHAR(10), mdb.DateRaise, 120) + ' ' + CONVERT(VARCHAR(5), ISNULL(mdb.TimeRaise, '08:30'), 108) AS DATETIME) AS RaiseDate,
        mdb.BANK,
        mdb.Product,
        mdb.SubProduct,
        SUBSTRING(ISNULL(mdb.FinalSeverityCategory, mdb.Severity), 3, LEN(ISNULL(mdb.FinalSeverityCategory, mdb.Severity))) AS Severity,
        mdb.IssueDescription,
        mdb.PIC,
        mdb.Category,
        mdb.CurrentStatus,
        mdb.Rootcause,
        mdb.Solution,
        mdb.RatingTicket,
        mdb.InternalL3StatusBreach,
        mdb.BreachSLA,
        ROW_NUMBER() OVER (PARTITION BY mdb.[TicketNo] ORDER BY (SELECT NULL)) AS RowNum
    FROM [NASTS_DEV].[dbo].[Migration] mdb
)
SELECT *
FROM RankedData
WHERE RowNum = 1;

*/
