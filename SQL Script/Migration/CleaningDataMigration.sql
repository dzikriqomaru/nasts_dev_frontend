-- update temporary newline replacer
UPDATE NASTS_TicketIssue
SET IssueDescription = REPLACE(REPLACE(IssueDescription, '||', CHAR(13) + CHAR(10)), '?', '')
WHERE IssueDescription LIKE '%||%' OR IssueDescription LIKE '%?%';

UPDATE NASTS_TicketIssue
SET rootcause = REPLACE(REPLACE(rootcause, '||', CHAR(13) + CHAR(10)), '?', '')
WHERE rootcause LIKE '%||%' OR rootcause LIKE '%?%';

UPDATE NASTS_TicketIssue
SET solution = REPLACE(REPLACE(solution, '||', CHAR(13) + CHAR(10)), '?', '')
WHERE solution LIKE '%||%' OR solution LIKE '%?%';



/*
-- check the string after convert
declare @string varchar(2000)
select top(1) @string = noteprogress from Migration
print @string
*/

select * from Migration