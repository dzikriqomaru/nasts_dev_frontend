USE [NASTS_DEV]
GO

-- for ticket issue table
SELECT m.[TicketNo]
      ,CAST(CONVERT(VARCHAR(10), m.DateRaise, 120) + ' ' + CONVERT(VARCHAR(5), ISNULL(m.TimeRaise,'08:30'), 108) AS DATETIME) as RaiseDate
	  ,m.BANK
      ,m.Product
      ,m.SubProduct
      ,substring(ISNULL(m.FinalSeverityCategory,m.Severity), 3,LEN(ISNULL(m.FinalSeverityCategory,m.Severity))) AS Severity
      ,m.IssueDescription
      ,m.PIC
      ,m.Category
      ,m.CurrentStatus
      ,m.Rootcause
      ,m.Solution
      ,m.RatingTicket
      ,m.InternalL3StatusBreach
      ,m.BreachSLA
  FROM [NASTS_DEV].[dbo].[Migration] m

--  LEFT JOIN (
SELECT mdb.[TicketNo]
      ,CAST(CONVERT(VARCHAR(10), mdb.DateRaise, 120) + ' ' + CONVERT(VARCHAR(5), ISNULL(mdb.TimeRaise,'08:30'), 108) AS DATETIME) as RaiseDate
	  ,c.PK_Client_ID
	  ,p.PK_Ms_Product_ID
	  ,sp.PK_Ms_SubProduct_ID
--      ,cp.PK_Client_Project_ID as FK_Client_Project_ID
--	  ,cp.FK_MaintenanceType_ID
      ,s.PK_Ms_Severity_ID
      ,mdb.IssueDescription
      ,cat.PK_Ms_Category_ID
      ,mdb.CurrentStatus
      ,mdb.Rootcause
      ,mdb.Solution
      ,mdb.RatingTicket
      ,mdb.InternalL3StatusBreach
      ,mdb.BreachSLA
  FROM [NASTS_DEV].[dbo].[Migration] mdb
  join NASTS_Client c on c.ClientCode = mdb.BANK
  join NASTS_Ms_Product p on p.Product_Code = mdb.Product
  join NASTS_Ms_SubProduct sp on p.PK_Ms_Product_ID = sp.FK_Ms_Product_ID and sp.SubProduct_Code = mdb.SubProduct
--  join NASTS_ClientProject cp on p.PK_Ms_Product_ID = cp.Product and sp.PK_Ms_SubProduct_ID = cp.SubProduct and c.PK_Client_ID = cp.FK_Client_ID
  join NASTS_Ms_Severity s on s.Severity_Name = substring(ISNULL(mdb.FinalSeverityCategory,mdb.Severity), 3,LEN(ISNULL(mdb.FinalSeverityCategory,mdb.Severity)))
  join NASTS_Ms_Category cat on cat.CategoryName = mdb.Category
  left join NASTS_Ms_Rating r on r.PK_Ms_Rating_ID = mdb.RatingTicket
  /*
  ) testingt on m.ticketno = testingt.ticketno
  where testingt.ticketno is null
  */ 
  
  
  where exists (SELECT fil.TicketNo, COUNT(*) AS DuplicateCount
FROM [NASTS_DEV].[dbo].[Migration] fil
WHERE fil.TicketNo = mdb.TicketNo
GROUP BY fil.TicketNo
HAVING COUNT(*) > 1)

select * from NASTS_ClientProject where PK_Client_Project_ID in (3,1015)
SELECT * FROM NASTS_TicketIssue WHERE FK_Client_Project_ID IN (3,1015)


-- for project table REGULAR
SELECT [BANK]
      ,[Product]
      ,[SubProduct]
      ,CONCAT(BANK,' ',Product,' ',SubProduct) AS ProjectName
	  ,1 as maintenance_type
  FROM [NASTS_DEV].[dbo].[Migration] 
  WHERE BANK NOT IN ('SCB', 'SB','HANA','BSI') 
  GROUP BY BANK, PRODUCT,SubProduct

SELECT c.PK_Client_ID
      ,p.PK_Ms_Product_ID
      ,sp.PK_Ms_SubProduct_ID
      ,CONCAT(c.ClientCode,' ',p.Product_Code,' ',
        CASE
            WHEN LEN(sp.Sub_Product_Name) > 17 THEN sp.SubProduct_Code
            ELSE isnull(sp.Sub_Product_Name, sp.SubProduct_Code)
        END
       ) AS ProjectName
	  ,1 as maintenance_type
	  ,1
	  ,'sysadmin'
	  ,'sysadmin'
	  ,GETDATE()
	  ,GETDATE()
	  ,''
	  ,0
  FROM [NASTS_DEV].[dbo].[Migration] mdb
  join NASTS_Client c on c.ClientCode = mdb.BANK and mdb.BANK NOT IN ('SCB', 'SB','HANA','BSI') 
  join NASTS_Ms_Product p on p.Product_Code = mdb.Product
  join NASTS_Ms_SubProduct sp on sp.FK_Ms_Product_ID = p.PK_Ms_Product_ID and sp.SubProduct_Code = mdb.SubProduct
  where not exists (select 1 from NASTS_ClientProject where FK_Client_ID = c.PK_Client_ID and Product = p.PK_Ms_Product_ID and SubProduct = sp.PK_Ms_SubProduct_ID and FK_MaintenanceType_ID = 1)
  GROUP BY c.ClientCode, p.Product_Code,sp.Sub_Product_Name, c.PK_Client_ID, p.PK_Ms_Product_ID, sp.PK_Ms_SubProduct_ID, sp.SubProduct_Code

-- for project table IRREGULAR 19
SELECT [BANK]
      ,[Product]
      ,[SubProduct]
      ,CONCAT(BANK,' ',Product,' ',SubProduct) AS ProjectName
	  ,2 as maintenance_type
	  ,1
	  ,'sysadmin'
	  ,'sysadmin'
	  ,GETDATE()
	  ,GETDATE()
	  ,''
	  ,0
  FROM [NASTS_DEV].[dbo].[Migration] 
  WHERE BANK = 'SCB' AND Product = 'ONELOAN'
  GROUP BY BANK, PRODUCT,SubProduct

SELECT c.PK_Client_ID
      ,p.PK_Ms_Product_ID
      ,sp.PK_Ms_SubProduct_ID
      ,CONCAT(c.ClientCode,' ',p.Product_Code,' ',
        CASE
            WHEN LEN(sp.Sub_Product_Name) > 17 THEN sp.SubProduct_Code
            ELSE isnull(sp.Sub_Product_Name, sp.SubProduct_Code)
        END
       ) AS ProjectName
	  ,2 as maintenance_type
	  ,1
	  ,'sysadmin'
	  ,'sysadmin'
	  ,GETDATE()
	  ,GETDATE()
	  ,''
	  ,0
  FROM [NASTS_DEV].[dbo].[Migration] mdb
  join NASTS_Client c on c.ClientCode = mdb.BANK and (BANK = 'SCB' AND Product = 'ONELOAN')
  join NASTS_Ms_Product p on p.Product_Code = mdb.Product
  join NASTS_Ms_SubProduct sp on sp.FK_Ms_Product_ID = p.PK_Ms_Product_ID and sp.SubProduct_Code = mdb.SubProduct
  where not exists (select 1 from NASTS_ClientProject where FK_Client_ID = c.PK_Client_ID and Product = p.PK_Ms_Product_ID and SubProduct = sp.PK_Ms_SubProduct_ID and FK_MaintenanceType_ID = 2)
  GROUP BY c.ClientCode, p.Product_Code,sp.Sub_Product_Name, c.PK_Client_ID, p.PK_Ms_Product_ID, sp.PK_Ms_SubProduct_ID, sp.SubProduct_Code

-- for project table IRREGULAR 24
SELECT [BANK]
      ,[Product]
      ,[SubProduct]
      ,CONCAT(BANK,' ',Product,' ',SubProduct) AS ProjectName
	  ,3 as maintenance_type
	  ,1
	  ,'sysadmin'
	  ,'sysadmin'
	  ,GETDATE()
	  ,GETDATE()
	  ,''
	  ,0
  FROM [NASTS_DEV].[dbo].[Migration] 
  WHERE BANK IN ('SB','HANA','BSI') OR (BANK = 'SCB' AND Product <> 'ONELOAN')
  GROUP BY BANK, PRODUCT,SubProduct

SELECT c.PK_Client_ID
      ,p.PK_Ms_Product_ID
      ,sp.PK_Ms_SubProduct_ID
      ,CONCAT(c.ClientCode,' ',p.Product_Code,' ',
        CASE
            WHEN LEN(sp.Sub_Product_Name) > 17 THEN sp.SubProduct_Code
            ELSE isnull(sp.Sub_Product_Name, sp.SubProduct_Code)
        END
       ) AS ProjectName
	  ,3 as maintenance_type
	  ,1
	  ,'sysadmin'
	  ,'sysadmin'
	  ,GETDATE()
	  ,GETDATE()
	  ,''
	  ,0
  FROM [NASTS_DEV].[dbo].[Migration] mdb
  join NASTS_Client c on c.ClientCode = mdb.BANK and (BANK IN ('SB','HANA','BSI') OR (BANK = 'SCB' AND Product <> 'ONELOAN'))
  join NASTS_Ms_Product p on p.Product_Code = mdb.Product
  join NASTS_Ms_SubProduct sp on sp.FK_Ms_Product_ID = p.PK_Ms_Product_ID and sp.SubProduct_Code = mdb.SubProduct
  where not exists (select 1 from NASTS_ClientProject where FK_Client_ID = c.PK_Client_ID and Product = p.PK_Ms_Product_ID and SubProduct = sp.PK_Ms_SubProduct_ID and FK_MaintenanceType_ID = 3)
  GROUP BY c.ClientCode, p.Product_Code,sp.Sub_Product_Name, c.PK_Client_ID, p.PK_Ms_Product_ID, sp.PK_Ms_SubProduct_ID, sp.SubProduct_Code

  -- for update client Customer experience, but it is unnecessary for now as CE for each client is only 1, cant add multiple internal pic into one client.
  SELECT [BANK]
      ,[PIC]
  FROM [NASTS_DEV].[dbo].[Migration] group by PIC, BANK ORDER BY BANK

  SELECT compatibility_level
	FROM sys.databases
	WHERE name = 'NASTS_DEV';

  -- make temp table for notes RUN THIS
	DROP TABLE IF EXISTS #TESTING
	DECLARE @separator NVARCHAR(4) = N'||'; -- Unicode separator
	SELECT
	  ticketno,
	  TRIM(value) AS noteprogress
	 into #TESTING
	FROM migration
	CROSS APPLY STRING_SPLIT(REPLACE(noteprogress, @separator, NCHAR(9999)), NCHAR(9999))
	WHERE LEN(TRIM(value)) > 0;

	SELECT * FROM NASTS_TicketIssueSLA

	SELECT  TicketNo
		   ,StartDateTime
		   ,CASE
				WHEN Level = 'InternalL1DateTime' THEN 1
				WHEN Level = 'InternalL2DateTime' THEN 2
				WHEN Level = 'InternalL3DateTime' THEN 3
			END AS Level
		   ,InternalPermanentSolution
		   ,ExternalTemporarySolution
		   ,ExternalPermanentSolution
           ,1
           ,'MIGRATION'
           ,'MIGRATION'
           ,GETDATE()
           ,GETDATE()
           ,''
           ,0
	FROM (
	    SELECT  TicketNo
			   ,CAST(CONVERT(VARCHAR(10), DateRaise, 120) + ' ' + CONVERT(VARCHAR(5), ISNULL(TimeRaise,'08:30'), 108) AS DATETIME) as StartDateTime
			   ,CAST(CONVERT(VARCHAR(10), InternalL1Date, 120) + ' ' + CONVERT(VARCHAR(5), InternalL1Time, 108) AS DATETIME) as InternalL1DateTime
			   ,CAST(CONVERT(VARCHAR(10), InternalL2Date, 120) + ' ' + CONVERT(VARCHAR(5), InternalL2Time, 108) AS DATETIME) as InternalL2DateTime
			   ,CAST(CONVERT(VARCHAR(10), InternalL3Date, 120) + ' ' + CONVERT(VARCHAR(5), InternalL3Time, 108) AS DATETIME) as InternalL3DateTime
			   ,CAST(CONVERT(VARCHAR(10), ExternalTemporarySolutionDate, 120) + ' ' + CONVERT(VARCHAR(5), ExternalTemporarySolutionTime, 108) AS DATETIME) as ExternalTemporarySolution
			   ,CAST(CONVERT(VARCHAR(10), ExternalPermanentSolutionDate, 120) + ' ' + CONVERT(VARCHAR(5), ExternalPermanentSolutionTime, 108) AS DATETIME) as ExternalPermanentSolution
	    FROM Migration
	) AS SourceTable
	UNPIVOT (
	    InternalPermanentSolution FOR Level IN (InternalL1DateTime, InternalL2DateTime,InternalL3DateTime)
	) AS UnpivotedData

GO


