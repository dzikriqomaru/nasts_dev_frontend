/****** Object:  StoredProcedure [dbo].[usp_GetGroupAccessByGroupMenu_New]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_GetGroupAccessByGroupMenu_New]    
(@pkgroupmenuid INT)    
AS    
BEGIN    
	SELECT
		ISNULL(c.PK_MGroupMenuAcess_ID, 0) AS PK_MGroupMenuAcess_ID,
		xxx.PK_Module_ID AS FK_Module_ID,    
		xxx.PK_MGroupMenu_ID AS FK_GroupMenu_ID,    
		xxx.GroupMenuName,    
		xxx.ModuleLabel,    
		CASE WHEN isnull(m.IsSupportAdd, 0) = 1 THEN isnull(c.bAdd, 0) ELSE 0 END bAdd,    
		CASE WHEN isnull(m.IsSupportEdit, 0) = 1 THEN isnull(c.bEdit,0) ELSE 0 END bEdit,    
		CASE WHEN isnull(m.IsSupportDelete, 0) = 1 THEN isnull(c.bDelete,0) ELSE 0 END bDelete,    
		CASE WHEN isnull(m.IsSupportActivation, 0) = 1 THEN isnull(c.bActivation,0) ELSE 0 END bActivation,    
		CASE WHEN isnull(m.IsSupportView, 0) = 1 THEN isnull(c.bView,0) ELSE 0 END bView,    
		CASE WHEN isnull(m.IsUseApproval, 0) = 1 THEN isnull(c.bApproval,0) ELSE 0 END bApproval,    
		CASE WHEN isnull(m.IsSupportUpload, 0) = 1 THEN isnull(c.bUpload,0) ELSE 0 END bUpload,    
		CASE WHEN isnull(m.IsSupportDetail, 0) = 1 THEN isnull(c.bDetail,0) ELSE 0 END bDetail,   
		m.IsSupportAdd, 
		m.IsSupportEdit, 
		m.IsSupportDelete, 
		m.IsSupportActivation,
		m.IsSupportView, 
		m.IsSupportUpload, 
		m.IsSupportDetail,
		m.IsUseApproval
	FROM (    
		SELECT 
			m.PK_Module_ID,
			b.PK_MGroupMenu_ID,
			b.GroupMenuName,
			m.ModuleLabel
		FROM Module m
		CROSS JOIN MGroupMenu b
	) xxx    
	LEFT JOIN MGroupMenuAccess c 
		ON xxx.PK_MGroupMenu_ID = c.FK_GroupMenu_ID
		AND xxx.PK_Module_ID = c.FK_Module_ID
	LEFT JOIN Module m
		ON  m.PK_Module_ID = xxx.PK_Module_ID    
	WHERE xxx.PK_MGroupMenu_ID = @pkgroupmenuid   
	ORDER BY    
		xxx.PK_MGroupMenu_ID,
		xxx.ModuleLabel
END
GO
