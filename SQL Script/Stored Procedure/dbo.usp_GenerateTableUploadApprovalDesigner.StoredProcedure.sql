/****** Object:  StoredProcedure [dbo].[usp_GenerateTableUploadApprovalDesigner]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_GenerateTableUploadApprovalDesigner]
/***********************************************************
* Procedure description:
* Date:   23/08/2021 
* Author: Fauzan
*
* Changes
* Date		Modified By			Comments
************************************************************
*
************************************************************/
(@ApprovalDesignerID INT)
AS
BEGIN
	DECLARE @ApprovalDesignerName           VARCHAR(250),
	        @ApprovalDesignerLabel          VARCHAR(250),
	        @ApprovalDesignerDescription	VARCHAR(500),
	        @IsUseDesigner					BIT,
	        @IsUseApproval					BIT,
	        @IsSupportAdd					BIT,
	        @IsSupportEdit					BIT,
	        @IsSupportDelete				BIT,
	        @IsSupportActivation			BIT,
	        @IsSupportView					BIT,
	        @UrlAdd							VARCHAR(550),
	        @UrlEdit						VARCHAR(550),
	        @UrlDelete						VARCHAR(550),
	        @UrlActivation					VARCHAR(550),
	        @UrlView						VARCHAR(550),
	        @UrlApproval					VARCHAR(250),
	        @IsUseStoreProcedureValidation	BIT

	SELECT 
		@ApprovalDesignerName			= ApprovalDesignerName,
	    @ApprovalDesignerLabel          = ApprovalDesignerLabel,
	    @ApprovalDesignerDescription	= ApprovalDesignerDescription,
	    @IsUseDesigner					= IsUseDesigner,
	    @IsUseApproval					= IsUseApproval,
	    @IsSupportAdd					= IsSupportAdd,
	    @IsSupportEdit					= IsSupportEdit,
	    @IsSupportDelete				= IsSupportDelete,
	    @IsSupportActivation			= IsSupportActivation,
	    @IsSupportView					= IsSupportView,
	    @UrlAdd							= UrlAdd,
	    @UrlEdit						= UrlEdit,
	    @UrlDelete						= UrlDelete,
	    @UrlActivation					= UrlActivation,
	    @UrlView						= UrlView,
	    @UrlApproval					= UrlApproval,
	    @IsUseStoreProcedureValidation	= IsUseStoreProcedureValidation
	FROM dbo.ApprovalDesigner m
	WHERE m.PK_ApprovalDesigner_ID = @ApprovalDesignerID
	
	SET @ApprovalDesignerName = @ApprovalDesignerName + '_Upload'
	
	IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES t WHERE t.TABLE_NAME = @ApprovalDesignerName)
	BEGIN
		EXEC ('drop table ' + @ApprovalDesignerName)
	END

	DECLARE @sql VARCHAR(8000)
    SET @sql = '	CREATE TABLE dbo. ' + @ApprovalDesignerName + CHAR(10)+CHAR(13)
				+ '	( ' + CHAR(10)+CHAR(13)
	SET @sql += ' PK_upload_ID bigint identitY(1,1), '+CHAR(10)+CHAR(13)    
	SET @sql += ' nawa_userid varchar(50), '+CHAR(10)+CHAR(13)
	SET @sql += ' nawa_recordnumber bigint, '+CHAR(10)+CHAR(13)
	SET @sql += ' KeteranganError varchar(max), '+CHAR(10)+CHAR(13)
	SET @sql += ' nawa_Action varchar(50), '+CHAR(10)+CHAR(13)
	    
	DECLARE @PK_ApprovalDesignerField_ID    BIGINT,
	        @FK_ApprovalDesigner_ID         INT,
	        @FieldName						VARCHAR(250),
	        @FieldLabel						VARCHAR(250),
	        @Sequence						INT,
	        @Required						BIT,
	        @IsPrimaryKey					BIT,
	        @IsUnik							BIT,
	        @FK_FieldType_ID				INT,
	        @SizeField						INT,
	        @FK_ExtType_ID					INT,
	        @TabelReferenceName				VARCHAR(250),
	        @TableReferenceFieldKey			VARCHAR(250),
	        @TableReferenceFieldDisplayName	VARCHAR(250),
	        @TableReferenceFilter			VARCHAR(550),
	        @IsUseRegexValidation			BIT
	    
	DECLARE my_cursor CURSOR FAST_FORWARD READ_ONLY FOR
		SELECT
			PK_ApprovalDesignerField_ID,
	        FK_ApprovalDesigner_ID,
	        FieldName,
	        FieldLabel,
	        Sequence,
	        [Required],
	        IsPrimaryKey,
	        IsUnik,
	        FK_FieldType_ID,
	        SizeField,
	        FK_ExtType_ID,
	        TabelReferenceName,
	        TableReferenceFieldKey,
	        TableReferenceFieldDisplayName,
	        TableReferenceFilter,
	        IsUseRegexValidation
		FROM dbo.ApprovalDesignerField mf 
		WHERE mf.FK_ApprovalDesigner_ID = @ApprovalDesignerID
		ORDER BY mf.Sequence
	    
	OPEN my_cursor
	    
	FETCH FROM my_cursor INTO @PK_ApprovalDesignerField_ID, @FK_ApprovalDesigner_ID, @FieldName,
	@FieldLabel, @Sequence, @Required, @IsPrimaryKey,
	@IsUnik, @FK_FieldType_ID, @SizeField, @FK_ExtType_ID,
	@TabelReferenceName, @TableReferenceFieldKey,
	@TableReferenceFieldDisplayName,
	@TableReferenceFilter, @IsUseRegexValidation
	    
	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @sql = @sql + ' [' + @FieldName + ']  varchar(8000),' +CHAR(10)+CHAR(13)	        
	        
	    FETCH FROM my_cursor INTO @PK_ApprovalDesignerField_ID, @FK_ApprovalDesigner_ID, @FieldName,
	    @FieldLabel, @Sequence, @Required, @IsPrimaryKey,
	    @IsUnik, @FK_FieldType_ID, @SizeField,
	    @FK_ExtType_ID, @TabelReferenceName,
	    @TableReferenceFieldKey,
	    @TableReferenceFieldDisplayName,
	    @TableReferenceFilter, @IsUseRegexValidation
	END
	    
	CLOSE my_cursor
	DEALLOCATE my_cursor

	DECLARE @PK_ApprovalDesignerField_IDdefault bigint, @FieldNameDefault varchar(250),
    @FieldLabelDefault varchar(250), @SequenceDefault int, @Requireddefault bit,
    @IsPrimaryKeyDefault bit, @IsUnikDefault bit, @FK_FieldType_IDDefault int, @SizeFieldDefault int,
    @FK_ExtType_IDDefault int
	    
	DECLARE my_cursor CURSOR FAST_FORWARD READ_ONLY FOR
	    SELECT
			PK_ApprovalDesignerField_ID,
			FieldName,
			FieldLabel,
			Sequence,
			[Required],
	        IsPrimaryKey,
			IsUnik,
			FK_FieldType_ID,
			SizeField,
			FK_ExtType_ID
	    FROM dbo.ApprovalDesignerFieldDefault
		WHERE PK_ApprovalDesignerField_ID = 1
	    
	OPEN my_cursor	    
	FETCH FROM my_cursor INTO @PK_ApprovalDesignerField_IDdefault, @FieldNamedefault, @FieldLabeldefault,
	                              @Sequencedefault, @Requireddefault, @IsPrimaryKeydefault, @IsUnikdefault,
	                              @FK_FieldType_IDdefault, @SizeFielddefault, @FK_ExtType_IDdefault
	    
	WHILE @@FETCH_STATUS = 0
	BEGIN
	    SET @sql = @sql + ' [' + @FieldNamedefault + ']  bit' + CHAR(10) + CHAR(13)
	    
	    FETCH FROM my_cursor INTO @PK_ApprovalDesignerField_IDdefault, @FieldNamedefault,
	    	                    @FieldLabeldefault, @Sequencedefault, @Requireddefault,
	    	                    @IsPrimaryKeydefault, @IsUnikdefault, @FK_FieldType_IDdefault,
	    	                    @SizeFielddefault, @FK_ExtType_IDdefault
	END
	    
	CLOSE my_cursor
	DEALLOCATE my_cursor

	IF LEN(@sql) > 0
		SET @sql = SUBSTRING(@sql, 0, LEN(@sql) -1)    
	    
	SET @sql = @sql + ')  ON [PRIMARY]'    
	
	EXEC (@sql)
END
GO
