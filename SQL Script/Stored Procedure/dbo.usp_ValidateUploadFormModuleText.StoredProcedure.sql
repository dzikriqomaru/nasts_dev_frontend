/****** Object:  StoredProcedure [dbo].[usp_ValidateUploadFormModuleText]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[usp_ValidateUploadFormModuleText]
/***********************************************************
* Procedure description:
* Date:   12/1/2015 
* Author: Hendra
*
* Changes
* Date        Modified By                Comments
************************************************************
*
************************************************************/
(@Moduleid INT, @userid VARCHAR(50),@intMode int)
AS
BEGIN
       
       
       DECLARE @sql VARCHAR(MAX)
       --declare @Moduleid INT, @userid VARCHAR(50),@intMode INT
       --SET @Moduleid =3110
       --SET @userid='sysadmin'
       --SET @intMode=0
       
       
DECLARE @strmodulename VARCHAR(250)      
SELECT @strmodulename =m.ModuleName
  FROM Module m WHERE m.PK_Module_ID=@Moduleid 
  
SET @sql = 'UPDATE '+@strmodulename+'_Upload SET nawa_Action = ''Insert'' WHERE nawa_Action IS NULL AND nawa_userid='''+@userid+''''
EXEC(@sql)




SET @sql = 'UPDATE '+@strmodulename+'_Upload SET KeteranganError = '''' WHERE nawa_userid='''+@userid+''''
EXEC(@sql)



IF @strmodulename='Karyawan'
BEGIN
	
SET @sql = '	UPDATE '+@strmodulename+'_Upload SET Nama = b.NamaBadanUsaha ' + char(10)
         + 'FROM   '+@strmodulename+'_Upload A ' + char(10)
         + 'INNER JOIN D02_DebiturBadanUsaha b ON a.CIF=b.CIF'
EXEC(@sql)



END

       

       
       DECLARE @PK_ModuleField_ID          BIGINT,
               @FK_Module_ID               INT,
               @FieldName                  VARCHAR(250),
               @FieldLabel                 VARCHAR(250),
               @Sequence                   INT,
               @Required                   BIT,
               @IsPrimaryKey               BIT,
               @IsUnik                     BIT,
               @IsShowInView               BIT,
               @FK_FieldType_ID            INT,
               @SizeField                  INT,
               @FK_ExtType_ID              INT,
               @TabelReferenceName         VARCHAR(250),
               @TableReferenceFieldKey     VARCHAR(250),
               @TableReferenceFieldDisplayName VARCHAR(250),
               @TableReferenceFilter       VARCHAR(550),
               @IsUseRegexValidation       BIT,
               @Modulename                 VARCHAR(250),
               @FieldTypeDescription            VARCHAR(255)
       
       DECLARE my_cursor CURSOR FAST_FORWARD READ_ONLY 
       FOR
           SELECT PK_ModuleField_ID,
                  FK_Module_ID,
                  FieldName,
                  FieldLabel,
                  Sequence,
                  [Required],
                  IsPrimaryKey,
                  IsUnik,
                  IsShowInView,
                  FK_FieldType_ID,
                  SizeField,
                  FK_ExtType_ID,
                  TabelReferenceName,
                  TableReferenceFieldKey,
                  TableReferenceFieldDisplayName,
                  TableReferenceFilter,
                  IsUseRegexValidation,
                  m.ModuleName,mt.FieldTypeDescription
           FROM   dbo.ModuleField mf
                  INNER JOIN Module m
                       ON  mf.FK_Module_ID = m.PK_Module_ID
                       INNER JOIN MFieldType mt ON mt.PK_FieldType_ID=mf.FK_FieldType_ID
           WHERE  mf.FK_Module_ID = @Moduleid
       
       OPEN my_cursor
       
       FETCH FROM my_cursor INTO @PK_ModuleField_ID, @FK_Module_ID, @FieldName,
       @FieldLabel, @Sequence, @Required, @IsPrimaryKey,
       @IsUnik, @IsShowInView, @FK_FieldType_ID, @SizeField,
       @FK_ExtType_ID, @TabelReferenceName,
       @TableReferenceFieldKey,
       @TableReferenceFieldDisplayName,
       @TableReferenceFilter, @IsUseRegexValidation,@Modulename,@FieldTypeDescription
       
       WHILE @@FETCH_STATUS = 0
       BEGIN
           --mfieldtype
           --cek required dan kosong maka invalid
           IF @Required = 1
              AND @FK_FieldType_ID <> 12 -- identity
           BEGIN
               SET @sql = ' UPDATE ' + @Modulename + 
                   '_Upload SET KeteranganError =KeteranganError + ''Line ''+ convert(Varchar(20),nawa_recordnumber) +'' : '
                   + @FieldLabel + ' is required </br>'' WHERE (' + @FieldName + 
                   '='''' or ' + @FieldName + ' is null )  and nawa_userid=''' + @userid
                   + ''''
               
               EXEC (@sql)
           END 
           
           
           IF @FK_FieldType_ID = 11 --reference tabel
           BEGIN
               SET @sql = 'UPDATE ' + @Modulename + 
                   '_upload SEt KeteranganError = KeteranganError + ''Line '' + CONVERT(VARCHAR(20), nawa_recordnumber) + '' : '
                   + @FieldLabel + 
                   ' not exist in database </br>'' + CHAR(13) + CHAR(10) FROM ' + @Modulename
                   + '_upload INNER JOIN ( SELECT tulv.PK_upload_ID FROM ' + @Modulename
                   + 
                   '_upload tulv LEFT JOIN ( SELECT CONVERT(VARCHAR(8000), ltrim(rtrim( ls.'
                   + @TableReferenceFieldKey + ')))'
                   + ' AS DisplayField FROM ' + @TabelReferenceName +' ls '
                           IF @TableReferenceFilter<>'' 
                           BEGIN
                   SET @sql +=    ' WHERE ls.' + @TableReferenceFilter 
                           END
                   SET @sql +=    ' )xx ON tulv.' + @FieldName	
                   + ' = xx.DisplayField WHERE tulv.nawa_userid = ''' + @userid + 
                   ''' AND xx.DisplayField IS NULL AND tulv.' + @FieldName + 
                   ' <> '''' )notvalid ON notvalid.PK_upload_ID = '+@Modulename+'_upload.PK_upload_ID'
               
               EXEC (@sql)
           END
           
           
           IF @IsUnik = 1
              AND @FK_FieldType_ID <> 12 --cek unik untuk mode insert
           BEGIN
              IF @intMode=0
              BEGIN
                        SET @sql = 'UPDATE ' + @Modulename + 
                   '_upload SET    KeteranganError = KeteranganError + ''Line '' + CONVERT(VARCHAR(20), nawa_recordnumber)         + '' : '
                   + @FieldLabel + 
                   ' Already Exist in database </br>'' + CHAR(13) + CHAR(10) FROM   ' + 
                   @Modulename + 
                   '_upload        INNER JOIN (                 SELECT A.PK_upload_ID                 FROM   '
                   + @Modulename + '_Upload A                        INNER JOIN ' + 
                   @Modulename + ' B                             ON  A.' + @FieldName 
                   + ' = b.' + @FieldName + 
                   '                 WHERE  a.nawa_Action = ''Insert''    AND a.nawa_userid='''
                   + @userid + 
                   '''         )notvalid             ON  notvalid.PK_upload_ID = ' + 
                   @Modulename + '_upload.PK_upload_ID'
               
               EXEC (@sql)        
                     END
               
           END
           
           
           IF @FK_FieldType_ID = 10
               --datetime 
               
               
               begin
               
               IF @Required =1
               BEGIN
                     
                     SET @sql ='UPDATE '+@Modulename+'_upload SET KeteranganError = KeteranganError + ''Line '' + CONVERT(VARCHAR(20), nawa_recordnumber) + '' : '+@FieldLabel+' is not a valid date </br>'' + CHAR(13) + CHAR(10) FROM '+@Modulename+'_upload INNER JOIN ( SELECT A.PK_upload_ID FROM '+@Modulename+'_Upload A WHERE (a.nawa_Action = ''Insert'' OR a.nawa_Action=''Update'' ) AND a.nawa_userid='''+@userid+''' AND isdate(a.'+@FieldName+')=0  )notvalid ON notvalid.PK_upload_ID = '+@Modulename+'_upload.PK_upload_ID'
               END
               ELSE
                     BEGIN
               
               --ada update kalau null dianggap valid kalau ngak required
                                  SET @sql ='UPDATE '+@Modulename+'_upload SET KeteranganError = KeteranganError + ''Line '' + CONVERT(VARCHAR(20), nawa_recordnumber) + '' : '+@FieldLabel+' is not a valid date </br>'' + CHAR(13) + CHAR(10) FROM '+@Modulename+'_upload INNER JOIN ( SELECT A.PK_upload_ID FROM '+@Modulename+'_Upload A WHERE (a.nawa_Action = ''Insert'' OR a.nawa_Action=''Update'' ) AND a.nawa_userid='''+@userid+''' AND isdate(a.'+@FieldName+')=0  AND ( a.'+@FieldName+'<>''null'' and a.'+@FieldName+'<>'''' ))notvalid ON notvalid.PK_upload_ID = '+@Modulename+'_upload.PK_upload_ID'
                     
                     END
               
                     EXEC (@sql)
           END 
           
           
           
          IF @FK_FieldType_ID= 9 -- varchar
           BEGIN
           --cek maxlength jika terisi
              
             
                     SET @sql = 'UPDATE '+@Modulename+'_upload SET KeteranganError = KeteranganError + ''Line '' + CONVERT(VARCHAR(20), nawa_recordnumber) + '' : '+@FieldLabel+' Max Length is '+ CONVERT(VARCHAR(20), @SizeField) +' char </br> '' + CHAR(13) + CHAR(10) FROM '+@Modulename+'_upload INNER JOIN ( SELECT A.PK_upload_ID FROM '+@Modulename+'_Upload A WHERE (a.nawa_Action = ''Insert'' OR a.nawa_Action=''Update'' ) AND a.nawa_userid='''+@userid+''' AND len(LTRIM(rtrim(a.'+@FieldName+')))>'+convert(varchar(20),@SizeField) +' )notvalid ON notvalid.PK_upload_ID = '+@Modulename+'_upload.PK_upload_ID'
                     
                     EXEC(@sql)


           END 
           
           
           
           IF @FK_FieldType_ID= 1 OR @FK_FieldType_ID= 2 OR @FK_FieldType_ID= 3 OR @FK_FieldType_ID= 4 OR @FK_FieldType_ID= 5 OR @FK_FieldType_ID= 6 OR @FK_FieldType_ID= 7 OR @FK_FieldType_ID= 8
           BEGIN
--            UPDATE Sales_upload
--SET    KeteranganError = KeteranganError + 'Line ' + CONVERT(VARCHAR(20), nawa_recordnumber) 
--       + ' : Sales Name range must  Huge Fixed Number data with range from -2^63 (-9.223.372.036.854.775.808) through 2^63-1 (9.223.372.036.854.775.807).' + CHAR(13) + CHAR(10)
--FROM   Sales_upload
--       INNER JOIN (
--                SELECT A.PK_upload_ID
--                FROM   Sales_Upload A
                       
--                WHERE  (a.nawa_Action = 'Insert' OR a.nawa_Action='Update' ) AND a.nawa_userid='adm01'
--                AND  try_cast(a.SalesName AS DECIMAL(28,2)) IS NULL 
--                AND a.SalesName IS NOT null   
--            )notvalid
--            ON  notvalid.PK_upload_ID = Sales_upload.PK_upload_ID



SET @sql = 'UPDATE '+@Modulename+'_upload ' + char(10)
         + 'SET    KeteranganError = KeteranganError + ''Line '' + CONVERT(VARCHAR(20), nawa_recordnumber)  ' + char(10)
         + '       + '' : '+@FieldLabel+' range must  '+ @FieldTypeDescription +' </br> '' + CHAR(13) + CHAR(10) ' + char(10)
         + 'FROM   '+@Modulename+'_upload ' + char(10)
         + '       INNER JOIN ( ' + char(10)
         + '                SELECT A.PK_upload_ID ' + char(10)
         + '                FROM   '+@Modulename+'_Upload A ' + char(10)
         + '                        ' + char(10)
         + '                WHERE  (a.nawa_Action = ''Insert'' OR a.nawa_Action=''Update'' ) AND a.nawa_userid='''+@Modulename+''' ' + char(10)
         + '                AND  try_cast(a.'+@FieldName+' AS DECIMAL(28,2)) IS NULL  ' + char(10)
         + '                AND a.'+@FieldName+' IS NOT null    ' + char(10)
         + '            )notvalid ' + char(10)
         + '            ON  notvalid.PK_upload_ID = '+@Modulename+'_upload.PK_upload_ID'
         
              EXEC(@sql)

           END
             
           
           
           FETCH FROM my_cursor INTO @PK_ModuleField_ID, @FK_Module_ID, @FieldName,
           @FieldLabel, @Sequence, @Required, @IsPrimaryKey,
           @IsUnik, @IsShowInView, @FK_FieldType_ID,
           @SizeField, @FK_ExtType_ID, @TabelReferenceName,
           @TableReferenceFieldKey,
           @TableReferenceFieldDisplayName,
           @TableReferenceFilter, @IsUseRegexValidation,@Modulename,@FieldTypeDescription
       END
       
       CLOSE my_cursor
       DEALLOCATE my_cursor
END
GO
