/****** Object:  StoredProcedure [dbo].[usp_GetMgroupMenuSettingByGroupMenuID]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[usp_GetMgroupMenuSettingByGroupMenuID] (@GroupMenuid AS int)

AS 



	

WITH directable (PK_MGroupMenuSettting_ID,FK_MGroupMenu_ID,mMenuID,mMenuLabel,mMenuParentID,mMenuURL,FK_Module_ID,FK_Action_ID,urutan)

AS	

(

	

	

SELECT DISTINCT  b.PK_MGroupMenuSettting_ID, b.FK_MGroupMenu_ID, b.mMenuID, b.mMenuLabel,

       b.mMenuParentID, b.mMenuURL, b.FK_Module_ID, b.FK_Action_ID,b.urutan

  FROM MGroupMenuAccess a 

INNER JOIN MGroupMenuSettting b ON a.FK_GroupMenu_ID=b.FK_MGroupMenu_ID

WHERE a.FK_GroupMenu_ID=@GroupMenuid AND b.FK_Module_ID=0



UNION ALL 

SELECT b.PK_MGroupMenuSettting_ID, b.FK_MGroupMenu_ID, b.mMenuID, b.mMenuLabel,

       b.mMenuParentID, b.mMenuURL, b.FK_Module_ID, b.FK_Action_ID,b.urutan

  FROM (SELECT mma.FK_GroupMenu_ID, mma.FK_Module_ID,CASE WHEN bview=1 THEN 5 ELSE 0 END AS viewaction ,CASE WHEN bapproval=1 THEN 6 ELSE 0 END AS viewapproval,CASE WHEN mma.bUpload=1 THEN 7 ELSE 0 END AS viewUpload  
          FROM MGroupMenuAccess mma)a 

INNER JOIN MGroupMenuSettting b ON a.FK_Module_ID=b.FK_Module_ID AND a.FK_GroupMenu_ID=b.FK_MGroupMenu_ID AND a.viewaction=b.FK_Action_ID

WHERE a.FK_GroupMenu_ID=@GroupMenuid



UNION ALL



SELECT b.PK_MGroupMenuSettting_ID, b.FK_MGroupMenu_ID, b.mMenuID, b.mMenuLabel,

       b.mMenuParentID, b.mMenuURL, b.FK_Module_ID, b.FK_Action_ID,b.urutan

  FROM (SELECT mma.FK_GroupMenu_ID, mma.FK_Module_ID,CASE WHEN bview=1 THEN 5 ELSE 0 END AS viewaction ,CASE WHEN bapproval=1 THEN 6 ELSE 0 END AS viewapproval,CASE WHEN mma.bUpload=1 THEN 7 ELSE 0 END AS viewUpload   FROM MGroupMenuAccess mma)a 

INNER JOIN MGroupMenuSettting b ON a.FK_Module_ID=b.FK_Module_ID AND a.FK_GroupMenu_ID=b.FK_MGroupMenu_ID AND a.viewapproval=b.FK_Action_ID

WHERE a.FK_GroupMenu_ID=@GroupMenuid


UNION ALL


SELECT b.PK_MGroupMenuSettting_ID, b.FK_MGroupMenu_ID, b.mMenuID, b.mMenuLabel,

       b.mMenuParentID, b.mMenuURL, b.FK_Module_ID, b.FK_Action_ID,b.urutan

  FROM (SELECT mma.FK_GroupMenu_ID, mma.FK_Module_ID,CASE WHEN bview=1 THEN 5 ELSE 0 END AS viewaction ,CASE WHEN bapproval=1 THEN 6 ELSE 0 END AS viewapproval,CASE WHEN mma.bUpload=1 THEN 7 ELSE 0 END AS viewUpload   FROM MGroupMenuAccess mma)a 

INNER JOIN MGroupMenuSettting b ON a.FK_Module_ID=b.FK_Module_ID AND a.FK_GroupMenu_ID=b.FK_MGroupMenu_ID AND a.viewUpload=b.FK_Action_ID

WHERE a.FK_GroupMenu_ID=@GroupMenuid


UNION all



SELECT xxx.PK_MGroupMenuSettting_ID, xxx.FK_MGroupMenu_ID, xxx.mMenuID,
       xxx.mMenuLabel, xxx.mMenuParentID, xxx.mMenuURL, xxx.FK_Module_ID,
       xxx.FK_Action_ID, xxx.urutan FROM (
       	
SELECT PK_MGroupMenuSettting_ID, FK_MGroupMenu_ID, mMenuID, mMenuLabel,
       mMenuParentID, mMenuURL, FK_Module_ID, FK_Action_ID, urutan         
 	from vw_Menuall
WHERE vw_Menuall.FK_MGroupMenu_ID=@GroupMenuid
)xxx INNER JOIN 
directable ON directable.mMenuParentID=xxx.mmenuid
)

SELECT distinct * FROM directable ORDER BY mMenuID
GO
