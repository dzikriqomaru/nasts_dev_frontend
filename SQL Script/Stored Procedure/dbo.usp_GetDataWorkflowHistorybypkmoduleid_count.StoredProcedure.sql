/****** Object:  StoredProcedure [dbo].[usp_GetDataWorkflowHistorybypkmoduleid_count]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_GetDataWorkflowHistorybypkmoduleid_count] 
  /***********************************************************
  * Procedure description:
  * Date:   5/6/2019 
  * Author: nawadata
  *
  * Changes
  * Date		Modified By			Comments
  ************************************************************
  *
  ************************************************************/
  (
  	@pkmoduleapprovalid BIGINT
  )
  AS
  BEGIN
 
 

SELECT COUNT(1) CountData
  FROM MWorkFlow_History AS mfh 
left JOIN MWorkflow_ApprovalStatus AS mas ON mfh.FK_MWorkflow_ApprovalStatus_ID=mas.PK_MWorkflow_ApprovalStatus_ID
WHERE mfh.FK_ModuleApproval_ID =@pkmoduleapprovalid 

 
 
  END

GO
