/****** Object:  StoredProcedure [dbo].[usp_StoreProcedureData]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_StoreProcedureData]
/***********************************************************
* Procedure description:
* Date:   8/7/2017 
* Author: Hendra
*
* Changes
* Date		Modified By			Comments
************************************************************
*
************************************************************/
(
    @StoreProcedureName VARCHAR(500),@StoreProcedureContent VARCHAR(MAX)	
)
AS
BEGIN
	
	
	DECLARE @sql varchar(max)
	SET @sql = 'IF object_id('''+ @StoreProcedureName +''') IS NOT NULL ' + char(10) +CHAR(13)
         + 'BEGIN  ' + char(10) +CHAR(13)
         + '	DROP PROCEDURE '+@StoreProcedureName +'  	 ' + char(10)+CHAR(13)
         + 'END'
EXEC(@sql)         
set @sql =@StoreProcedureContent         
         
EXEC(@sql)

	
	
	
	
END
GO
