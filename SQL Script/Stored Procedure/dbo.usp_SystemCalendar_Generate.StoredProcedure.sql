/****** Object:  StoredProcedure [dbo].[usp_SystemCalendar_Generate]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_SystemCalendar_Generate]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	TRUNCATE TABLE SystemCalendar

	Declare @dt_start date = CAST(Year(GETDATE())-10 AS VARCHAR) + '-01-01', @dt_end date = CAST(Year(GETDATE())+20 AS VARCHAR) + '-12-31', @total_days int, @i int = 0
	SELECT @total_days = DATEDIFF(d, @dt_start, @dt_end) 

	WHILE @i <= @total_days
	begin
		INSERT INTO SystemCalendar (CalendarDate, IsWeekend, IsHoliday, IsWorkday, Active, CreatedBy, CreatedDate) 
		SELECT CAST(DATEADD(d, @i, @dt_start) as DATE), 0, 0, 0, 1, 'system', GETDATE()

		SET @i = @i + 1
	end

	-- Set Weekend
	UPDATE SystemCalendar
	SET IsWeekend = CASE DATEPART(weekday, CalendarDate) WHEN 1 THEN 1 WHEN 7 THEN 1 ELSE 0 END 

	-- Set Fix Holiday 
	UPDATE SystemCalendar
	SET IsHoliday = 1,
	CalendarText='Tahun Baru ' + CAST(YEAR(CalendarDate) AS VARCHAR),
	CalendarDescription='Tahun Baru '  + CAST(YEAR(CalendarDate) AS VARCHAR)
	WHERE MONTH(CalendarDate)=1 AND DAY(CalendarDate)=1

	UPDATE SystemCalendar
	SET IsHoliday = 1,
	CalendarText='Natal ' + CAST(YEAR(CalendarDate) AS VARCHAR),
	CalendarDescription='Natal '  + CAST(YEAR(CalendarDate) AS VARCHAR)
	WHERE MONTH(CalendarDate)=12 AND DAY(CalendarDate)=25

	UPDATE SystemCalendar
	SET IsHoliday = 1,
	CalendarText='Kemerdekaan RI ' + CAST(YEAR(CalendarDate) AS VARCHAR),
	CalendarDescription='Kemerdekaan RI '  + CAST(YEAR(CalendarDate) AS VARCHAR)
	WHERE MONTH(CalendarDate)=8 AND DAY(CalendarDate)=17

	UPDATE SystemCalendar
	SET IsHoliday = 1,
	CalendarText='Hari Lahir Pancasila ' + CAST(YEAR(CalendarDate) AS VARCHAR),
	CalendarDescription='Hari Lahir Pancasila '  + CAST(YEAR(CalendarDate) AS VARCHAR)
	WHERE MONTH(CalendarDate)=6 AND DAY(CalendarDate)=1
	
		UPDATE SystemCalendar
	SET IsHoliday = 1,
	CalendarText='Hari Buruh ' + CAST(YEAR(CalendarDate) AS VARCHAR),
	CalendarDescription='Hari Buruh '  + CAST(YEAR(CalendarDate) AS VARCHAR)
	WHERE MONTH(CalendarDate)=5 AND DAY(CalendarDate)=1

	-- Set the non-holiday days
	UPDATE SystemCalendar 
	SET IsWorkday = CASE WHEN IsWeekend = 0 AND IsHoliday = 0 THEN 1 ELSE 0 END

END
GO
