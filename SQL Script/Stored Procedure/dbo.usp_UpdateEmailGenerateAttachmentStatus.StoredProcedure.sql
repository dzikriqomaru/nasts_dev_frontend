/****** Object:  StoredProcedure [dbo].[usp_UpdateEmailGenerateAttachmentStatus]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  Johan Peterson  
-- Create date: 2017-10-06  
-- Description:   
-- =============================================  
CREATE PROCEDURE [dbo].[usp_UpdateEmailGenerateAttachmentStatus]   
 -- Add the parameters for the stored procedure here  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
    -- Insert statements for procedure here  
  
 UPDATE EmailTemplateSchedulerDetailAttachment  
 SET FK_EmailGenerateAttachmentStatus_ID=2  
 FROM EmailTemplateSchedulerDetailAttachment  
 WHERE (FK_EmailAttachmentType_ID=2 OR FK_EmailAttachmentType_ID=4) AND FK_EmailGenerateAttachmentStatus_ID IS null  
  
  
 UPDATE EmailTemplateSchedulerDetailAttachment  
 SET FK_EmailGenerateAttachmentStatus_ID=1  
 FROM EmailTemplateSchedulerDetailAttachment  
 WHERE FK_EmailAttachmentType_ID=1 AND FK_EmailGenerateAttachmentStatus_ID IS null
   
  
  
END
GO
