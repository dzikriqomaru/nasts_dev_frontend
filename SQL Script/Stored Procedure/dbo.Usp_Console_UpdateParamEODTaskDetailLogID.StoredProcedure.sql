/****** Object:  StoredProcedure [dbo].[Usp_Console_UpdateParamEODTaskDetailLogID]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[Usp_Console_UpdateParamEODTaskDetailLogID]
/***********************************************************
* Procedure description:
* Date:   4/13/2016 
* Author: Indra
*
* Changes
* Date		Modified By			Comments
************************************************************
*
************************************************************/
(
	@PK_EODTaskDetailLog_ID BigInt
)
AS
BEGIN
	Update NawaDataETLConfigurations Set ConfiguredValue = @PK_EODTaskDetailLog_ID where ConfigurationFilter= 'PK_EODTaskDetailLog_ID'
END
GO
