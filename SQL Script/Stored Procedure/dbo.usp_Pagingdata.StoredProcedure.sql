/****** Object:  StoredProcedure [dbo].[usp_Pagingdata]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[usp_Pagingdata]  
(  
 @Tables VARCHAR(MAX),  
 @fields VARCHAR(MAX),  
  
 @WhereClause varchar (8000)  ,  
  
 @OrderBy varchar (2000)  ,  
  
 @PageIndex int   ,  
  
 @PageSize int     
)  
AS  
  
  
      
    BEGIN  
    DECLARE @PageLowerBound int  
    DECLARE @PageUpperBound int  
      
      
    DECLARE @defaultSort VARCHAR(1000)  
    DECLARE @indexkoma INT  
      
      
    SET @indexkoma=CHARINDEX(',',@fields,0)  
   IF @indexkoma=0  
   BEGIN  
    SET @defaultSort=@fields   
   END   
   ELSE  
    BEGIN  
    SET @defaultSort=SUBSTRING(@fields,0,@indexkoma)   
    END  
       
     
     
      
    -- Set the page bounds  
    SET @PageLowerBound = @PageSize * @PageIndex  
    SET @PageUpperBound = @PageLowerBound + @PageSize  
  
    IF (@OrderBy is null or LEN(@OrderBy) < 1)  
    BEGIN  
     -- default order by to first column  
     SET @OrderBy = @defaultSort  
    END  
  
    -- SQL Server 2005 Paging  
    declare @SQL as varchar(max)  
    SET @SQL = 'WITH PageIndex AS ('  
    SET @SQL = @SQL + ' SELECT'  
    IF @PageSize > 0  
    BEGIN  
     SET @SQL = @SQL + ' TOP ' + convert(nvarchar, @PageUpperBound)  
    END  
    SET @SQL = @SQL + ' ROW_NUMBER() OVER (ORDER BY ' + @OrderBy + ') as RowIndex'  
    SET @SQL = @SQL + ',' +@fields  
      
    SET @SQL = @SQL + ' FROM '+@Tables  
    IF LEN(@WhereClause) > 0  
    BEGIN  
     SET @SQL = @SQL + ' WHERE ' + @WhereClause  
    END  
    SET @SQL = @SQL + ' ) SELECT '  
    SET @SQL = @SQL + @fields  
      
    SET @SQL = @SQL + ' FROM PageIndex'  
    SET @SQL = @SQL + ' WHERE RowIndex > ' + convert(nvarchar, @PageLowerBound)  
    IF @PageSize > 0  
    BEGIN  
     SET @SQL = @SQL + ' AND RowIndex <= ' + convert(nvarchar, @PageUpperBound)  
    END  
    SET @SQL = @SQL + ' ORDER BY ' + @OrderBy  
    exec(@SQL)  
      
    -- get row count  
    SET @SQL = 'SELECT COUNT(*) as TotalRowCount'  
    SET @SQL = @SQL + ' from '+ @Tables  
    IF LEN(@WhereClause) > 0  
    BEGIN  
     SET @SQL = @SQL + ' WHERE ' + @WhereClause  
    END  
    
    exec(@SQL)  
     
    END
GO
