/****** Object:  StoredProcedure [dbo].[usp_GetListOptionsCount]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[usp_GetListOptionsCount]
	@PK_ModuleField_ID BIGINT,
	@ParentValue VARCHAR(MAX),
	@UserID VARCHAR(50),
	@SearchKeyword VARCHAR(MAX) = ''
AS
BEGIN
	-- COPIED FROM [usp_GetListOptions]
	DECLARE @TableReferenceName VARCHAR(250),
			@TableReferenceNameAlias VARCHAR(250),
			@TableReferenceFieldKey VARCHAR(250),
			@TableReferenceFieldDisplayName VARCHAR(250),
			@TableReferenceFilter VARCHAR(1000),
			@TableReferenceAdditonalJoin VARCHAR(1000),
			@FilterCascade VARCHAR(1000),
			@SQLQuery VARCHAR(MAX),
			@SearchWord	VARCHAR(MAX) = '''%' + REPLACE(@SearchKeyword, '''', '''''') + '%'''

	SELECT @TableReferenceName = TabelReferenceName,
			@TableReferenceNameAlias = TabelReferenceNameAlias,
			@TableReferenceFieldKey = TableReferenceFieldKey,
			@TableReferenceFieldDisplayName = TableReferenceFieldDisplayName,
			@TableReferenceFilter = TableReferenceFilter,
			@TableReferenceAdditonalJoin = TableReferenceAdditonalJoin,
			@FilterCascade = CASE WHEN BCasCade = 1 THEN FilterCascade ELSE '' END
	FROM dbo.ModuleField
	WHERE PK_ModuleField_ID = @PK_ModuleField_ID

	SET @TableReferenceNameAlias = CASE WHEN ISNULL(@TableReferenceNameAlias, '') = '' THEN @TableReferenceName ELSE @TableReferenceNameAlias END

	-- the SELECT statement has been altered to COUNT for [usp_GetListOptionsCount]
	SET @SQLQuery = 'SELECT COUNT(*) AS [RowCount]' + CHAR(13) +
					'FROM ' + @TableReferenceName + ' AS ' + @TableReferenceNameAlias + CHAR(10) + CHAR(13) +

					-- search
					'WHERE (' + @TableReferenceNameAlias + '.' + @TableReferenceFieldKey + ' LIKE ' + @SearchWord + 
					' OR '+ @TableReferenceNameAlias + '.' + @TableReferenceFieldDisplayName + ' LIKE ' + @SearchWord + ') ' +				

					-- filter
					CASE WHEN ISNULL(@TableReferenceFilter, '') <> ''
						 THEN 'AND ' + REPLACE(@TableReferenceFilter, '@UserID', @UserID) +
								CASE WHEN ISNULL(@FilterCascade, '') <> ''
									 THEN ' AND ' + REPLACE(@FilterCascade, '@Parent', ISNULL(@ParentValue, 'NULL'))
									 ELSE ''
								END
						 ELSE CASE WHEN ISNULL(@FilterCascade, '') <> ''
								   THEN 'AND ' + REPLACE(@FilterCascade, '@Parent', ISNULL(@ParentValue, 'NULL'))
								   ELSE ''
							  END
					END
	-- END LINE OF [usp_GetListOptions]

	-- EXEC QUERY
	EXEC (@SQLQuery)
END
GO
