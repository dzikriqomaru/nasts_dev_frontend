/****** Object:  StoredProcedure [dbo].[usp_GetModuleCorrection]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_GetModuleCorrection]
	@ModuleKey VARCHAR(1000),
	@UserID VARCHAR(50),
	@Search VARCHAR(MAX),
	@filterKey VARCHAR(50),
	@filterValue VARCHAR(MAX),
	@RoleID INT
AS
DECLARE @PK_MUser_ID INT
SELECT @PK_MUser_ID = PK_MUser_ID
FROM MUser
WHERE UserID = @UserID

DECLARE @DateFormat VARCHAR(MAX)
DECLARE @DateFormatSysParam VARCHAR(MAX)
SELECT @DateFormatSysParam = SettingValue FROM SystemParameter WHERE PK_SystemParameter_ID = 6
SELECT @DateFormat = SQLFormat FROM MDateFormat WHERE PK_DateFormat_ID = CAST(@DateFormatSysParam AS bigint)

DECLARE @TimeFormat VARCHAR(MAX)
DECLARE @TimeFormatSysParam VARCHAR(MAX)
SELECT @TimeFormatSysParam = SettingValue FROM SystemParameter WHERE PK_SystemParameter_ID = 33
SELECT @TimeFormat = SQLFormat FROM MTimeFormat WHERE PK_TimeFormat_ID = CAST(@TimeFormatSysParam AS bigint)

DECLARE @DateTimeFormat VARCHAR(MAX) = @DateFormat + ' ' + @TimeFormat

IF (len(@Search) > 0 OR len(@filterKey) <= 0)
BEGIN
	IF EXISTS(SELECT 13 FROM Module_TR_WorkFlow WHERE FK_ModuleID = @ModuleKey AND Active = 1)
	BEGIN
		SELECT 
			ModuleApproval.PK_ModuleApproval_ID,
			ModuleApproval.ModuleName,
			ModuleApproval.ModuleField,
			ModuleApproval.ModuleKey,
			ModuleApproval.PK_ModuleAction_ID,
			ModuleApproval.CreatedDate,
			ModuleApproval.CreatedBy,
			ModuleApproval.FK_Module_ID,
			ModuleApproval.FK_MRole_ID,
			ModuleActionName
		FROM ModuleApproval
		JOIN Module
			ON ModuleApproval.ModuleName = Module.ModuleName
		JOIN ModuleAction
			ON ModuleApproval.PK_ModuleAction_ID = ModuleAction.PK_ModuleAction_ID
		INNER JOIN MWorkFlow_Progress Workflow
			ON Workflow.FK_Module_ID = Module.PK_Module_ID
			AND (
				Workflow.FK_Unik_ID = ModuleApproval.ModuleKey
				OR Workflow.FK_Unik_ID = CONVERT(VARCHAR(50), ModuleApproval.PK_ModuleApproval_ID) + 'Import'
			)
		WHERE 
			PK_Module_ID = @ModuleKey
			AND (
				(
					(
						FK_MWorkFlowUserType_ID = 1
						OR FK_MWorkFlowUserType_ID = 2
					)
					AND FK_MWorkflowUser_ID = @PK_MUser_ID
					AND FK_MWorkflow_Status_ID = 6
				)
				OR (
					FK_MWorkFlowUserType_ID = 4
					AND FK_MWorkflowRole_ID = @RoleID
					AND FK_MWorkflow_Status_ID = 6
				)
			)
			AND (
				PK_ModuleApproval_ID LIKE '%' + @Search + '%'
				OR ModuleApproval.ModuleName LIKE '%' + @Search + '%'
				OR ModuleApproval.ModuleField LIKE '%' + @Search + '%'
				OR ModuleKey LIKE '%' + @Search + '%'
				OR ModuleActionName LIKE '%' + @Search + '%'
				OR FORMAT(ModuleApproval.CreatedDate, @DateTimeFormat) LIKE '%' + @Search + '%'
				OR ModuleApproval.CreatedBy LIKE '%' + @Search + '%'
			)
	END
	ELSE
	BEGIN
		SELECT
			ModuleApproval.PK_ModuleApproval_ID,
			ModuleApproval.ModuleName,
			ModuleApproval.ModuleField,
			ModuleApproval.ModuleKey,
			ModuleApproval.PK_ModuleAction_ID,
			ModuleApproval.CreatedDate,
			ModuleApproval.CreatedBy,
			ModuleApproval.FK_Module_ID,
			ModuleApproval.FK_MRole_ID,
			ModuleActionName
		FROM ModuleApproval
		JOIN Module
			ON ModuleApproval.ModuleName = Module.ModuleName
		JOIN ModuleAction
			ON ModuleApproval.PK_ModuleAction_ID = ModuleAction.PK_ModuleAction_ID
		JOIN MUser UserCreator
			ON ModuleApproval.CreatedBy = UserCreator.UserID
		WHERE 
			PK_Module_ID = @ModuleKey
			AND ModuleApproval.CreatedBy <> @UserID
			AND (
				PK_ModuleApproval_ID LIKE '%' + @Search + '%'
				OR ModuleApproval.ModuleName LIKE '%' + @Search + '%'
				OR ModuleApproval.ModuleField LIKE '%' + @Search + '%'
				OR ModuleKey LIKE '%' + @Search + '%'
				OR ModuleActionName LIKE '%' + @Search + '%'
				OR FORMAT(ModuleApproval.CreatedDate, @DateTimeFormat) LIKE '%' + @Search + '%'
				OR ModuleApproval.CreatedBy LIKE '%' + @Search + '%'
			)
			AND ISNULL(ModuleApproval.FK_MRole_ID, UserCreator.FK_MRole_ID) = @RoleID
	END
END
ELSE
BEGIN
	IF EXISTS(SELECT 13 FROM Module_TR_WorkFlow WHERE FK_ModuleID = @ModuleKey AND Active = 1)
	BEGIN
		SELECT
			ModuleApproval.PK_ModuleApproval_ID,
			ModuleApproval.ModuleName,
			ModuleApproval.ModuleField,
			ModuleApproval.ModuleKey,
			ModuleApproval.ModuleField,
			ModuleApproval.PK_ModuleAction_ID,
			ModuleApproval.CreatedDate,
			ModuleApproval.CreatedBy,
			ModuleApproval.FK_Module_ID,
			ModuleApproval.FK_MRole_ID,
			ModuleActionName
		FROM ModuleApproval
		JOIN Module
			ON ModuleApproval.ModuleName = Module.ModuleName
		JOIN ModuleAction
			ON ModuleApproval.PK_ModuleAction_ID = ModuleAction.PK_ModuleAction_ID
		INNER JOIN MWorkFlow_Progress Workflow
			ON Workflow.FK_Module_ID = Module.PK_Module_ID
			AND (
				Workflow.FK_Unik_ID = ModuleApproval.ModuleKey
				OR Workflow.FK_Unik_ID = CONVERT(VARCHAR(50), ModuleApproval.PK_ModuleApproval_ID) + 'Import'
			)
		WHERE 
			PK_Module_ID = @ModuleKey
			AND (
				(
					(
						FK_MWorkFlowUserType_ID = 1
						OR FK_MWorkFlowUserType_ID = 2
					)
					AND FK_MWorkflowUser_ID = @PK_MUser_ID
					AND FK_MWorkflow_Status_ID = 6
				)
				OR (
					FK_MWorkFlowUserType_ID = 4
					AND FK_MWorkflowRole_ID = @RoleID
					AND FK_MWorkflow_Status_ID = 6
				)
			)
			AND (
				(@filterKey = 'PK_ModuleApproval_ID' AND PK_ModuleApproval_ID LIKE '%' + @filterValue + '%')
				OR (@filterKey = 'ModuleApproval.ModuleName' AND ModuleApproval.ModuleName LIKE '%' + @filterValue + '%')
				OR (@filterKey = 'ModuleApproval.ModuleField' AND ModuleApproval.ModuleField LIKE '%' + @filterValue + '%')
				OR (@filterKey = 'ModuleKey' AND ModuleKey LIKE '%' + @filterValue + '%')
				OR (@filterKey = 'ModuleActionName' AND ModuleActionName LIKE '%' + @filterValue + '%' )
				OR (@filterKey = 'ModuleApproval.CreatedDate' AND FORMAT(ModuleApproval.CreatedDate, @DateTimeFormat) LIKE '%' + @filterValue + '%' )
				OR (@filterKey = 'ModuleApproval.CreatedBy' AND ModuleApproval.CreatedBy LIKE '%' + @filterValue + '%' )
			)
	END
	ELSE
	BEGIN
		SELECT
			ModuleApproval.PK_ModuleApproval_ID,
			ModuleApproval.ModuleName,
			ModuleApproval.ModuleKey,
			ModuleApproval.ModuleField,
			ModuleApproval.PK_ModuleAction_ID,
			ModuleApproval.CreatedDate,
			ModuleApproval.CreatedBy,
			ModuleApproval.FK_Module_ID,
			ModuleApproval.FK_MRole_ID,
			ModuleActionName
		FROM ModuleApproval
		JOIN Module
			ON ModuleApproval.ModuleName = Module.ModuleName
		JOIN ModuleAction
			ON ModuleApproval.PK_ModuleAction_ID = ModuleAction.PK_ModuleAction_ID
		JOIN MUser UserCreator
			ON ModuleApproval.CreatedBy = UserCreator.UserID
		WHERE 
			PK_Module_ID = @ModuleKey
			AND ModuleApproval.CreatedBy <> @UserID
			AND (
				(@filterKey = 'PK_ModuleApproval_ID' AND PK_ModuleApproval_ID LIKE '%' + @filterValue + '%')
				OR (@filterKey = 'ModuleApproval.ModuleName' AND ModuleApproval.ModuleName LIKE '%' + @filterValue + '%')
				OR (@filterKey = 'ModuleApproval.ModuleField' AND ModuleApproval.ModuleField LIKE '%' + @filterValue + '%')
				OR (@filterKey = 'ModuleKey' AND ModuleKey LIKE '%' + @filterValue + '%')
				OR (@filterKey = 'ModuleActionName' AND ModuleActionName LIKE '%' + @filterValue + '%' )
				OR (@filterKey = 'ModuleApproval.CreatedDate' AND FORMAT(ModuleApproval.CreatedDate, @DateTimeFormat) LIKE '%' + @filterValue + '%' )
				OR (@filterKey = 'ModuleApproval.CreatedBy' AND ModuleApproval.CreatedBy LIKE '%' + @filterValue + '%' )
			)
			AND ISNULL(ModuleApproval.FK_MRole_ID, UserCreator.FK_MRole_ID) = @RoleID
	END
END



GO
