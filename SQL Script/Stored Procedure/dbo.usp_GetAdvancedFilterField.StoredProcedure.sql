/****** Object:  StoredProcedure [dbo].[usp_GetAdvancedFilterField]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_GetAdvancedFilterField]
    
/***********************************************************    
* Procedure description:    
* Date:   6/11/2018     
* Author: hendra1    
*    
* Changes    
* Date  Modified By   Comments    
************************************************************    
*    
************************************************************/    
(    
 @tablename VARCHAR(500)    
)    
AS    
BEGIN    
     
 SELECT c.COLUMN_NAME AS FieldLabel,c.DATA_TYPE AS FieldName,c.DATA_TYPE  FROM INFORMATION_SCHEMA.[COLUMNS] AS c WHERE c.TABLE_NAME=@tablename    
     
END
GO
