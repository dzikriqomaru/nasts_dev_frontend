/****** Object:  StoredProcedure [dbo].[usp_ExecuteDataReplacingManual]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ExecuteDataReplacingManual]     
(@PK_EODTaskDetailLog_ID AS BIGINT)    
-- Add the parameters for the stored procedure here    
--@bulan VARCHAR(2),    
--@tahun VARCHAR(4)    
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;     
   
 --PRINT('A')  
  
 DECLARE @SQL             VARCHAR(8000),    
         @ModuleID        VARCHAR(255),    
         @ModuleName      VARCHAR(255),    
         @modulelabel VARCHAR(500),    
         @FieldName       VARCHAR(255),    
         @ValueToKeep     VARCHAR(5000),    
         @KeyValue        VARCHAR(5000),    
         @KeyField        VARCHAR(1000), @pk bigint    
     
 DECLARE my_cursor CURSOR FAST_FORWARD READ_ONLY     
 FOR    
     SELECT DISTINCT dr.PK_DataReplacer, dr.TableName,    
            Module.ModuleName,Module.ModuleLabel,    
            dr.FieldName,    
            dr.ValueToKeep,    
            dr.KeyFieldValue,    
            dr.KeyField    
     FROM   DataReplacer AS dr    
            LEFT JOIN Module    
                 ON  PK_Module_ID = dr.TableName    
     WHERE dr.StatusReplace=0                     
                
 --PRINT('B')   
                 
     
 OPEN my_cursor    
    --PRINT('B2')   
  
 FETCH FROM my_cursor INTO @pk,@ModuleID,@ModuleName,@modulelabel,@FieldName,@ValueToKeep,@KeyValue,@KeyField    
       --PRINT('B3')   
  
 WHILE @@FETCH_STATUS = 0    
 BEGIN    
        --PRINT('B4')   
  
     SET @SQL = 'DECLARE @OldValue VARCHAR(5000)'  
   + CHAR(10) + 'SELECT  @OldValue = '+ @FieldName + ' FROM ' + @ModuleName + ' WHERE ' + @KeyField +' = ' + @KeyValue  
   + CHAR(10) +  
   + CHAR(10) + 'UPDATE ' + @ModuleName    
         + CHAR(10) + ' SET ' + @FieldName + ' = ''' + @ValueToKeep + ''''    
         + CHAR(10) + ' WHERE ' + @KeyField + ' = ''' + @KeyValue + ''''   
   + CHAR(10) +  
   + CHAR(10) + 'INSERT INTO AuditTrailHeader(CreatedDate, CreatedBy, ApproveBy, ModuleLabel, FK_ModuleAction_ID, FK_AuditTrailStatus_ID)'  
   + CHAR(10) + 'VALUES (GETDATE(), ''Manual Data Replacer Procedure'',''System'', '''+ @modulelabel +''', 2, 1)'  
   + CHAR(10) +  
   + CHAR(10) + 'INSERT INTO AuditTrailDetail(FK_AuditTrailHeader_ID, FieldName, OldValue, NewValue)'  
   + CHAR(10) + 'VALUES (IDENT_CURRENT(''AuditTrailHeader''), '''+ @FieldName + ''', @OldValue, ''' + @ValueToKeep + ''')'   
     --PRINT('C')     
       
  --PRINT (@SQL)  
     EXEC (@SQL)    
       
  
     UPDATE DataReplacer    
     SET    
         StatusReplace = 1 WHERE PK_DataReplacer=@pk    
             
        UPDATE Web_ValidationReport_RowComplate    
        SET    
            Edited = 1 WHERE SegmentData=@modulelabel AND KeyFieldValue=@KeyValue     
         
     FETCH FROM my_cursor INTO @pk,@ModuleID,@ModuleName,@modulelabel,@FieldName,@ValueToKeep,@KeyValue,@KeyField    
 END    
     
 CLOSE my_cursor    
 DEALLOCATE my_cursor    
  
END    
GO
