/****** Object:  StoredProcedure [dbo].[usp_SaveUploadDataModuleDetail]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_SaveUploadDataModuleDetail]
(@IntMode INT, @ModuleDetailID INT, @UserID VARCHAR(50))
AS
BEGIN
	--intmode = 0 no action, intmode = 1 delete all

	--declare @IntMode INT, @ModuleDetailID INT, @UserID VARCHAR(50)
	--set @IntMode = 2
	--SET @ModuleDetailID = 1
	--SET @UserID = 'sysadmin'

	DECLARE @DateFormat VARCHAR(50)
	SELECT @DateFormat = SettingValue FROM SystemParameter WHERE PK_SystemParameter_ID = 14

	DECLARE
		@PK_ModuleDetail_ID      INT,
		@FK_Module_ID			 INT,
		@ModuleDetailName		 VARCHAR(250),
		@ModuleDetailLabel		 VARCHAR(250),
		@ModuleDetailDescription VARCHAR(500),
		@Active                  BIT,
		@CreatedBy               VARCHAR(50),
		@LastUpdateBy            VARCHAR(50),
		@ApprovedBy              VARCHAR(50),
		@CreatedDate             DATETIME,
		@LastUpdateDate          DATETIME,
		@ApprovedDate            DATETIME

	SELECT
		@PK_ModuleDetail_ID		 = PK_ModuleDetail_ID,
		@FK_Module_ID			 = FK_Module_ID,
		@ModuleDetailName		 = ModuleDetailName,
		@ModuleDetailLabel		 = ModuleDetailLabel,
		@ModuleDetailDescription = ModuleDetailDescription,
		@Active                  = [Active],
		@CreatedBy               = CreatedBy,
		@LastUpdateBy            = LastUpdateBy,
		@ApprovedBy              = ApprovedBy,
		@CreatedDate             = CreatedDate,
		@LastUpdateDate          = LastUpdateDate,
		@ApprovedDate            = ApprovedDate
	FROM dbo.ModuleDetail
	WHERE PK_ModuleDetail_ID = @ModuleDetailID

	DECLARE @RoleID INT
	DECLARE @FilterPersonal VARCHAR(8000) = ''
	DECLARE @FilterAccess VARCHAR(8000) = ''
	DECLARE @FilterCombine VARCHAR(8000) = ''

	SELECT @FilterPersonal = dbo.ufn_GetFilterAdditionalPerUser(@UserID, @PK_ModuleDetail_ID)
	SELECT @RoleID = FK_MRole_ID FROM MUser WHERE UserID = @UserID
	
	-- Start 20221005 davin
	DECLARE @ModuleFieldUniq VARCHAR(250) = '' 
	DECLARE @ModuleName VARCHAR(250) = ''
	DECLARE @ModuleDetailFieldFKName VARCHAR(250) = ''
	DECLARE @ModuleFieldPKName VARCHAR(250) = ''
	DECLARE @ModuleFieldUniqDataType INT
	DECLARE @ModuleFieldUniqFieldType INT

	SELECT TOP 1 
		@ModuleFieldUniq = FieldName, 
		@ModuleFieldUniqDataType = FK_FieldType_ID,
		@ModuleFieldUniqFieldType = FK_ExtType_ID 
	FROM dbo.ModuleField 
	WHERE IsUnik = 1 and IsPrimaryKey <> 1 and FK_Module_ID = @FK_Module_ID 

	SELECT @ModuleName = ModuleName FROM Module 
	WHERE PK_Module_ID = @FK_Module_ID 

	SELECT @ModuleDetailFieldFKName = FieldName,
			@ModuleFieldPKName = FieldNameParent 
	FROM ModuleDetailField 
	WHERE FK_FieldType_ID = 16 and FK_ModuleDetail_ID = @ModuleDetailID
	--End 20221005 Davin 


	IF EXISTS (
		SELECT 1
		FROM INFORMATION_SCHEMA.TABLES
		WHERE
			TABLE_TYPE     = 'BASE TABLE'
			AND TABLE_NAME = 'DataAccess'
	)
		SELECT TOP 1 @FilterAccess = xx.FilterData
		FROM (
			SELECT
				REPLACE(da.FilterData, '@UserID', @UserID) AS FilterData,
				da.FK_Role_ID
			FROM DataAccess da
			WHERE
				da.FK_Role_ID = @RoleID
				AND da.FK_Module_ID = @ModuleDetailID
			UNION
			SELECT
				REPLACE(da.FilterData, '@UserID', @UserID) AS FilterData,
				da.FK_Role_ID
			FROM DataAccess da
			WHERE
				da.FK_Role_ID = 0
				AND da.FK_Module_ID = @ModuleDetailID
		) xx
		ORDER BY FK_Role_ID DESC
	ELSE
		SELECT @FilterAccess = '' ;

	IF LEN(@FilterAccess) > 0 OR LEN(@FilterPersonal) > 0
	SET @FilterCombine = ' where '

	IF LEN(@FilterAccess) > 0
	SET @FilterCombine = @FilterCombine + ' ' + @FilterAccess

	IF LEN(@FilterPersonal) > 0  AND LEN(@FilterAccess) = 0
	SET @FilterCombine = @FilterCombine + ' ' + @FilterPersonal

	IF LEN(@FilterPersonal) > 0  AND LEN(@FilterAccess) > 0
	SET @FilterCombine = @FilterCombine + ' and ' + @FilterPersonal

	DECLARE @sql VARCHAR(MAX)
	IF @IntMode = 1
	BEGIN
		SET @sql = 'DELETE FROM ' + @ModuleDetailName + @FilterCombine
		EXEC (@sql)
	END

    DECLARE
		@FK_ModuleDetail_ID				INT,
        @FieldName						VARCHAR(250),
        @FieldLabel						VARCHAR(250),
        @Sequence						INT,
        @Required						BIT,
        @IsPrimaryKey					BIT,
        @IsUnik							BIT,
        @IsShowInView					BIT,
        @FK_FieldType_ID				INT,
        @SizeField						INT,
        @FK_ExtType_ID					INT,
        @TabelReferenceName				VARCHAR(250),
        @TableReferenceFieldKey			VARCHAR(250),
        @TableReferenceFieldDisplayName VARCHAR(250),
        @TableReferenceFilter			VARCHAR(550),
        @IsUseRegexValidation			BIT,
        @fields							VARCHAR(MAX),
        @fieldDefault					VARCHAR(MAX),
        @fieldactive					VARCHAR(100),
        @fieldvaluedefault				VARCHAR(MAX),
		@FieldUpdate					VARCHAR(MAX),
        @fieldupdatedefault				VARCHAR(MAX),
        @filedupdateactive				VARCHAR(100),
		@fieldprimarykey				VARCHAR(500),
		@fieldInsertValue				VARCHAR(MAX)

	SET @fieldInsertValue = ''
	SET @filedupdateactive = ''
	SET @fields = ''
	SET @fieldDefault = ''
	SET @fieldactive = ''
	SET @fieldvaluedefault = ''
	SET @FieldUpdate = ''
	SET @fieldupdatedefault = ''
	SET @fieldprimarykey = ''

	DECLARE my_cursor CURSOR FAST_FORWARD READ_ONLY
	FOR
	SELECT 
		FK_ModuleDetail_ID,
		FieldName,
		FieldLabel,
		Sequence,
		[Required],
		IsPrimaryKey,
		IsUnik,
		IsShowInView,
		FK_FieldType_ID,
		SizeField,
		FK_ExtType_ID,
		TabelReferenceName,
		TableReferenceFieldKey,
		TableReferenceFieldDisplayName,
		TableReferenceFilter,
		IsUseRegexValidation
	FROM dbo.ModuleDetailField
	WHERE FK_ModuleDetail_ID = @ModuleDetailID AND FK_FieldType_ID <> 14
	ORDER BY Sequence

	OPEN my_cursor

	FETCH FROM my_cursor INTO @FK_ModuleDetail_ID, @FieldName,
	@FieldLabel, @Sequence, @Required, @IsPrimaryKey,
	@IsUnik, @IsShowInView, @FK_FieldType_ID, @SizeField,
	@FK_ExtType_ID, @TabelReferenceName,
	@TableReferenceFieldKey,
	@TableReferenceFieldDisplayName,
	@TableReferenceFilter, @IsUseRegexValidation

	WHILE @@FETCH_STATUS = 0 
	BEGIN
		/*{ ... Cursor logic here ... }*/
		 IF @IsPrimaryKey=1 SET @fieldprimarykey=@FieldName    
		   /*{ ... Cursor logic here ... }*/    
           IF  @FK_ExtType_ID=11 OR ( @FK_ExtType_ID=7 AND @FK_FieldType_ID = 11)     
             BEGIN
			  FETCH FROM my_cursor INTO @FK_ModuleDetail_ID, @FieldName,
				@FieldLabel, @Sequence, @Required, @IsPrimaryKey,
				@IsUnik, @IsShowInView, @FK_FieldType_ID, @SizeField,
				@FK_ExtType_ID, @TabelReferenceName,
				@TableReferenceFieldKey,
				@TableReferenceFieldDisplayName,
				@TableReferenceFilter, @IsUseRegexValidation
			   CONTINUE
			 END
          
	

		IF @FK_FieldType_ID <> 12 AND @FK_FieldType_ID <> 15 
		begin
			SET @fields = @fields + @FieldName + ',' + CHAR(10) + CHAR(13)
		END

		IF @FK_FieldType_ID <> 12 AND @FK_FieldType_ID <> 11 AND @FK_FieldType_ID <> 10 AND @FK_FieldType_ID <> 15 AND @FK_FieldType_ID <> 16	--20221005 Davin 
		begin
			SET @fieldInsertValue= @fieldInsertValue + @ModuleDetailName + '_upload.' +@FieldName + ',' + CHAR(10) + CHAR(13)
		END

		--20221005 Davin 
		IF @FK_FieldType_ID = 16 
        BEGIN
		  SET @fieldInsertValue = @fieldInsertValue  + @ModuleName + '.' + @ModuleFieldPKName +  ',' + CHAR(10) + CHAR(13)
        END
		-----------

        IF @FK_FieldType_ID between 1 AND 9
			IF @FK_ExtType_ID <> 16
            BEGIN
                SET @FieldUpdate = @FieldUpdate + @FieldName + '=' + @ModuleDetailName + '_upload.' + @FieldName + ',' + CHAR(10)  + CHAR(13)
            END

        IF @FK_FieldType_ID = 10
        BEGIN
            SET @FieldUpdate = @FieldUpdate + @FieldName + '=dbo.ufn_GetDateValue(' + @ModuleDetailName + '_upload.' + @FieldName + ',''' + @DateFormat + '''),' + CHAR(10) + CHAR(13)

            IF @Required = 1
            BEGIN
                SET @fieldInsertValue = @fieldInsertValue + @ModuleDetailName + '_upload.' + @FieldName + ',' + CHAR(10) + CHAR(13)
            END
            ELSE
            BEGIN
                SET @fieldInsertValue = @fieldInsertValue + 'CASE WHEN  ' + @ModuleDetailName + '_upload.' + @FieldName + '='''' or ' + @ModuleDetailName + '_upload.' +  @FieldName  + ' =''NULL'' then null else ' + @ModuleDetailName + '_upload.' + @FieldName + ' end as ' + @FieldName + ' ,' + CHAR(10) + CHAR(13)
            END
		END

        IF @FK_FieldType_ID = 11
        BEGIN
			IF @FK_ExtType_ID = 7
			BEGIN
				SET @fieldInsertValue=  @fieldInsertValue
				--SET @FieldUpdate = @FieldUpdate + @FieldName + '= 2112 ,' + CHAR(10)
			END
			ELSE IF @FK_ExtType_ID = 15
			BEGIN
				SET @FieldUpdate = @FieldUpdate + @FieldName + '= CASE WHEN  ' + @ModuleDetailName + '_upload.' + @FieldName + '='''' or ' + @ModuleDetailName + '_upload.' +  @FieldName  + ' =''NULL'' then null else ' + @ModuleDetailName + '_upload.' + @FieldName + ' end ,' + CHAR(10) + CHAR(13)
			      SET @fieldInsertValue = @fieldInsertValue + 'CASE WHEN  ' + @ModuleDetailName + '_upload.' + @FieldName + '='''' or ' + @ModuleDetailName + '_upload.' +  @FieldName  + ' =''NULL'' then null else ' + @ModuleDetailName + '_upload.' + @FieldName + ' end as ' + @FieldName + ' ,' + CHAR(10) + CHAR(13)
         END
			ELSE
			BEGIN
				SET @FieldUpdate = @FieldUpdate + @FieldName + '=SUBSTRING( ' + @ModuleDetailName + '_upload.' + @FieldName + ', CHARINDEX(''('', ' + @ModuleDetailName + '_upload.' + @FieldName + ',1)+1,  case when CHARINDEX('')'', ' + @ModuleDetailName + '_upload.' + @FieldName + ',1)-2 >0 then CHARINDEX('')'', ' + @ModuleDetailName + '_upload.' + @FieldName + ',1)-2 ELSE 0 END ),' + char(10) + CHAR(13)
				SET @fieldInsertValue = @fieldInsertValue + 'SUBSTRING( ' + @ModuleDetailName + '_upload.' + @FieldName + ', CHARINDEX(''('', ' + @ModuleDetailName + '_upload.' + @FieldName + ',1)+1,  case when CHARINDEX('')'', ' + @ModuleDetailName + '_upload.' + @FieldName + ',1)-2 >0 then CHARINDEX('')'', ' + @ModuleDetailName + '_upload.' + @FieldName + ',1)-2 ELSE 0 END ),' + char(10) + CHAR(13)
			END
		END

        IF @FK_FieldType_ID = 13 --varchar
        BEGIN
			SET @FieldUpdate = @FieldUpdate + @FieldName + '=' + @ModuleDetailName + '_upload.' + @FieldName + ',' + CHAR(10) + CHAR(13)
        END

		

        FETCH FROM my_cursor INTO @FK_ModuleDetail_ID, @FieldName,
        @FieldLabel, @Sequence, @Required, @IsPrimaryKey,
        @IsUnik, @IsShowInView, @FK_FieldType_ID,
        @SizeField, @FK_ExtType_ID, @TabelReferenceName,
        @TableReferenceFieldKey,
        @TableReferenceFieldDisplayName,
        @TableReferenceFilter, @IsUseRegexValidation
	END

	CLOSE my_cursor
	DEALLOCATE my_cursor

    IF LEN(@fields) > 0 SET @fields = SUBSTRING(@fields, 1, LEN(@fields) -3)
    IF LEN(@fieldInsertValue) > 0 SET @fieldInsertValue= SUBSTRING(@fieldInsertValue, 1, LEN(@fieldInsertValue) -3)

    DECLARE 
		@PK_ModuleField_ID_Default	BIGINT,
        @FieldName_Default			VARCHAR(250),
        @FieldLabel_Default			VARCHAR(250),
        @Sequence_Default			INT,
        @Required_Default			BIT,
        @IsPrimaryKey_Default		BIT,
        @IsUnik_Default				BIT,
        @FK_FieldType_ID_Default	INT,
        @SizeField_Default			INT,
        @FK_ExtType_ID_Default		INT

    DECLARE my_cursor CURSOR FAST_FORWARD READ_ONLY
    FOR
    SELECT 
		PK_ModuleField_ID,
        FieldName,
        FieldLabel,
        Sequence,
        [Required],
        IsPrimaryKey,
        IsUnik,
        FK_FieldType_ID,
        SizeField,
        FK_ExtType_ID
    FROM dbo.ModuleFieldDefault

    OPEN my_cursor

    FETCH FROM my_cursor INTO @PK_ModuleField_ID_Default, @FieldName_Default, @FieldLabel_Default,
    @Sequence_Default, @Required_Default, @IsPrimaryKey_Default, @IsUnik_Default,
    @FK_FieldType_ID_Default, @SizeField_Default, @FK_ExtType_ID_Default

    WHILE @@FETCH_STATUS = 0
    BEGIN
		/*{ ... Cursor logic here ... }*/

		SET @fieldDefault = @fieldDefault + @ModuleDetailName +'_upload.' + @FieldName_Default + ',' + CHAR(10) +CHAR(13)

		IF @FK_FieldType_ID_Default = 13 And @PK_ModuleField_ID_Default <> 9
		BEGIN
			SET @fieldactive ='isnull('+ @ModuleDetailName +'_upload.' + @FieldName_Default + ',1),' + CHAR(10) + CHAR(13)
			set @filedupdateactive=@filedupdateactive +@FieldName_Default +'=isnull('+ @ModuleDetailName +'_upload.' +@FieldName_Default +',1),'+ CHAR(10) + CHAR(13)
		END

        --cek tipenya varchar(percaya kalau cuma isinya createdby,lastupdateby,approveby)
        IF @FK_FieldType_ID_Default = 9
        BEGIN
            SET @fieldvaluedefault = @fieldvaluedefault + '''' + @UserID + ''','+ CHAR(10) + CHAR(13)
        END
        IF @PK_ModuleField_ID_Default= 3 or @PK_ModuleField_ID_Default=4
			SET @fieldupdatedefault = @fieldupdatedefault +@FieldName_Default+ '=''' + @UserID + ''','+ CHAR(10) + CHAR(13)

           --cek tipenya date(percaya kalau isinya createdate,lastupdatedate,approveate)
		IF @FK_FieldType_ID_Default = 10
			SET @fieldvaluedefault = @fieldvaluedefault + '''' + CONVERT(VARCHAR(20), GETDATE(), 120)+ ''',' + CHAR(10) + CHAR(13)

		IF @PK_ModuleField_ID_Default= 6 or @PK_ModuleField_ID_Default=7
			SET @fieldupdatedefault = @fieldupdatedefault +@FieldName_Default+ '=''' +  CONVERT(VARCHAR(20), GETDATE(), 120) + ''','+ CHAR(10) + CHAR(13)
		
		--Tambah Field Draft
         IF @PK_ModuleField_ID_Default= 9   
			BEGIN
                     SET @fieldupdatedefault = @fieldupdatedefault +@FieldName_Default+ '=1,'+ CHAR(10) + CHAR(13)  
					 SET @fieldvaluedefault = @fieldvaluedefault + '1,'+ CHAR(10) + CHAR(13)    
			END

        FETCH FROM my_cursor INTO @PK_ModuleField_ID_Default, @FieldName_Default,
        @FieldLabel_Default,
        @Sequence_Default, @Required_Default, @IsPrimaryKey_Default, @IsUnik_Default,
        @FK_FieldType_ID_Default, @SizeField_Default, @FK_ExtType_ID_Default
    END

    CLOSE my_cursor
    DEALLOCATE my_cursor

    IF LEN(@fieldDefault) > 0
        SET @fieldDefault = SUBSTRING(@fieldDefault, 1, LEN(@fieldDefault) -3)

    IF LEN(@fieldvaluedefault) > 0
        SET @fieldvaluedefault = SUBSTRING(@fieldvaluedefault, 1, LEN(@fieldvaluedefault) -3)

    --insert mode

    DECLARE @strinsert VARCHAR(MAX) = ''

    SET @strinsert = 'INSERT INTO ' + @ModuleDetailName + '(' + CHAR(10) + CHAR(13)
    SET @strinsert += @fields
    IF LEN(@fieldDefault) > 0
    BEGIN
        SET @strinsert += ',' + CHAR(10) + CHAR(13)
        SET @strinsert += @fieldDefault + CHAR(10) + CHAR(13)
    END

    SET @strinsert += ')' + CHAR(10) + CHAR(13)
    SET @strinsert += ' SELECT ' + CHAR(10) + CHAR(13)
    SET @strinsert += @fieldInsertValue
    IF LEN(@fieldactive) > 0
    BEGIN
        SET @strinsert += ',' + CHAR(10) + CHAR(13)
        SET @strinsert += @fieldactive + CHAR(10) + CHAR(13)
    END

    SET @strinsert += @fieldvaluedefault + CHAR(10) + CHAR(13)

	IF @ModuleFieldUniqDataType = 11
	BEGIN
		SET @strinsert += ' FROM ' + @ModuleDetailName +
        '_Upload ' 
		+ ' JOIN ' + @ModuleName + '_Upload on ' + @ModuleDetailName +
        '_Upload.' + @ModuleDetailFieldFKName + ' = ' + @ModuleName + '_Upload.' + @ModuleFieldPKName 
		+ ' JOIN ' + @ModuleName + ' on '+ @ModuleName + '.'+ @ModuleFieldUniq + ' = '
		+ 'SUBSTRING( ' + @ModuleName + '_upload.' + @ModuleFieldUniq + ', CHARINDEX(''('', ' + @ModuleName + '_upload.' + @ModuleFieldUniq + ',1)+1,  case when CHARINDEX('')'', ' + @ModuleName + '_upload.' + @ModuleFieldUniq + ',1)-2 >0 then CHARINDEX('')'', ' + @ModuleName + '_upload.' + @ModuleFieldUniq + ',1)-2 ELSE 0 END )' 
		+ ' WHERE '+ @ModuleDetailName +
        '_Upload.' +'nawa_userid=''' + @UserID + ''''
	END
	ELSE
	BEGIN
		SET @strinsert += ' FROM ' + @ModuleDetailName +
        '_Upload ' 
		+ ' JOIN ' + @ModuleName + '_Upload on ' + @ModuleDetailName +
        '_Upload.' + @ModuleDetailFieldFKName + ' = ' + @ModuleName + '_Upload.' + @ModuleFieldPKName 
		+ ' JOIN ' + @ModuleName + ' on '+ @ModuleName + '.'+ @ModuleFieldUniq + ' = '+ @ModuleName + '_Upload.' + @ModuleFieldUniq 
		+ ' WHERE '+ @ModuleDetailName +
        '_Upload.' +'nawa_userid=''' + @UserID + ''''
	END

     

    SET @strinsert +=
        ' AND '+ @ModuleDetailName +
        '_Upload.' +'nawa_Action=''INSERT'' AND ISNULL('+ @ModuleDetailName +
        '_Upload.' +'KeteranganError, '''') = '''' AND ( ' +  @ModuleName + 
		'_Upload.' +'nawa_Action=''INSERT'' OR ' +  @ModuleName +'_Upload.' +'nawa_Action=''UPDATE'')  AND ISNULL('+ @ModuleName +
        '_Upload.' +'KeteranganError, '''') = '''' AND ' +  @ModuleName + 
        '_Upload.' +'nawa_userid=''' + @UserID + ''''

    -- SELECT  @strinsert
	PRINT (@strinsert)
    EXEC (@strinsert)

    --upate  mode
    DECLARE @strupdate VARCHAR(MAX) = ''
    IF LEN(@fieldupdatedefault) > 0
		SET @fieldupdatedefault = SUBSTRING(@fieldupdatedefault, 1, LEN(@fieldupdatedefault) -3)

	SET @strupdate = 'UPDATE '+ @ModuleDetailName +' SET ' + char(10)
              + @FieldUpdate
              + @filedupdateactive
       +@fieldupdatedefault
              +' from '+@ModuleDetailName
         + '  INNER JOIN '+@ModuleDetailName+'_Upload ON '+@ModuleDetailName+'.'+@fieldprimarykey +'='+@ModuleDetailName+'_upload.'+@fieldprimarykey +'' + char(10)
         + '  WHERE '+@ModuleDetailName+'_Upload.nawa_userid='''+@UserID+''' AND ISNULL('+@ModuleDetailName+'_Upload.KeteranganError, '''') = '''' AND '+@ModuleDetailName+'_Upload.nawa_Action=''Update'' ' + char(10)
         + '  '

	--SELECT @strupdate
	PRINT(@strupdate)
	EXEC(@strupdate)

    --delete mode
	DECLARE @sqlDelete varchar(max)
	SET @sqlDelete = '   DELETE '+@ModuleDetailName+'            ' + char(10)
         + '         FROM '+@ModuleDetailName+' ' + char(10)
         + '         INNER JOIN '+@ModuleDetailName+'_Upload ON '+@ModuleDetailName+'.'+@fieldprimarykey+'='+@ModuleDetailName+'_upload.'+@fieldprimarykey +'          ' + char(10)
         + '         WHERE '+@ModuleDetailName+'_Upload.nawa_Action=''Delete'' AND ISNULL('+@ModuleDetailName+'_Upload.KeteranganError, '''') = '''' ' + char(10)
         + '         AND '+@ModuleDetailName+'_Upload.nawa_userid='''+@UserID+''''
	--SELECT  @sqlDelete
	PRINT(@sqlDelete)
	EXEC(@sqlDelete )
END

GO
