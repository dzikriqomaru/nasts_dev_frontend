/****** Object:  StoredProcedure [dbo].[usp_DecryptReportConnectionData]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[usp_DecryptReportConnectionData]  
/*********************  
* Procedure description:  
* Date:   9/2/2017   
* Author: Hendra  
*  
* Changes  
* Date  Modified By   Comments  
********************  
*  
********************/ 
 
(  
 @encryptedPass VARCHAR(MAX)
)  
AS  
BEGIN  

DECLARE @decryptedPass VARBINARY(MAX)

OPEN SYMMETRIC KEY ConfigSymKey DECRYPTION BY CERTIFICATE ConfigCert

	set @decryptedPass = CONVERT(VARBINARY(MAX), @encryptedPass, 1);

	SELECT  CONVERT(VARCHAR
	(MAX), DecryptByKey(@decryptedPass)) AS decryptedPass
END
GO
