/****** Object:  StoredProcedure [dbo].[usp_EmailReadReceipt]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_EmailReadReceipt]
/***********************************************************
* Procedure description:
* Date:   26/05/2023 
* Author: Hendra
*
* Changes
* Date		Modified By			Comments
************************************************************
*
************************************************************/
(
@ID AS varchar(max)
)
AS
BEGIN

DECLARE @ONOFF AS bit=0

--Cek dulu apakah id nya ada di tabel atau tidak
IF EXISTS(
	select PK_EmailTemplateSchedulerDetail_ID 
	FROM EmailTemplateSchedulerdetail
	--WHERE PK_EmailTemplateSchedulerDetail_ID=@ID
	WHERE emailid=@ID)
begin 
		--7 email status read
		IF EXISTS(
			select PK_EmailTemplateSchedulerDetail_ID 
			FROM EmailTemplateSchedulerdetail
			--WHERE PK_EmailTemplateSchedulerDetail_ID=@ID and FK_EmailStatus_ID<>7
			WHERE emailid=@ID and FK_EmailStatus_ID<>7)
			BEGIN

				--buat log atau debug masukkan ketabel LogConsoleWeb ON/OFF Performance Console Log
				IF EXISTS(
				SELECT SETTINGVALUE FROM SystemParameter WHERE PK_SystemParameter_ID=330
				)
				BEGIN
					SET @ONOFF=(SELECT SETTINGVALUE FROM SystemParameter WHERE PK_SystemParameter_ID=330)
					IF @ONOFF=1
					begin
						insert into LogConsoleWeb
						(
						Log_Page
						,Log_Function
						,Log_Data
						,Log_Status
						,Log_Message
						,Log_Description
						,Active
						,CreatedBy
						,CreatedDate
						)
						values('Email Read Receipt','GetEmailRead',' ID : '+try_cast(@ID as varchar(max)),'INFO','Start','ID from table EmailTemplateSchedulerdetail kolom emailid',1,'sysadmin',getdate())
					END
				END

				--cek apabila status email 7 read tidak ada
				IF NOT EXISTS(SELECT PK_EmailStatus_ID FROM EmailStatus WHERE PK_EmailStatus_ID=7	)
				BEGIN
					INSERT EmailStatus
					(
					PK_EmailStatus_ID
					,EmailStatusName
					)VALUES(7,'Read')
				END

				UPDATE EmailTemplateSchedulerdetail
				SET FK_EmailStatus_ID=7
				--WHERE PK_EmailTemplateSchedulerDetail_ID=@ID
				WHERE emailid=@ID

				--buat log atau debug masukkan ketabel LogConsoleWeb ON/OFF Performance Console Log
				IF @ONOFF=1
				begin
					insert into LogConsoleWeb
					(
					Log_Page
					,Log_Function
					,Log_Data
					,Log_Status
					,Log_Message
					,Log_Description
					,Active
					,CreatedBy
					,CreatedDate
					)
					values('Email Read Receipt','GetEmailRead',' ID : '+try_cast(@ID as varchar(max)),'INFO','Finish','ID from table EmailTemplateSchedulerdetail kolom emailid',1,'sysadmin',getdate())
				END
			END		
	END
	ELSE
	BEGIN
		RAISERROR('Not Valid Data',16,1);
	END
END

GO
