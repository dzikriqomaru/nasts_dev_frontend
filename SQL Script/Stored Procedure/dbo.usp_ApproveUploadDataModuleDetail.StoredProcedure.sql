/****** Object:  StoredProcedure [dbo].[usp_ApproveUploadDataModuleDetail]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ApproveUploadDataModuleDetail]
/***********************************************************
* Procedure description:
* Date:   02/02/2022 
* Author: Fauzan
*
* Changes
* Date		Modified By			Comments
************************************************************
* 20221005	Davin J				Menambahkan Relasi insert ketika upload header detail
************************************************************/
(@moduleDetailApprovalID BIGINT, @userID VARCHAR(50))
AS
BEGIN
	DECLARE @sqlmajorversion INT  
    SELECT @sqlmajorversion= CONVERT(INT, SERVERPROPERTY('ProductMajorVersion'))

	--Get ModuleDetailApproval Data
	DECLARE @ModuleDetailName VARCHAR(250), @ModuleDetailKey VARCHAR(1000), @ModuleDetailField VARCHAR(MAX), 
		@ModuleDetailFieldBefore VARCHAR(MAX), @FK_ModuleApproval_ID BIGINT, @PK_ModuleAction_ID INT, 
		@CreatedDate DATETIME, @CreatedBy VARCHAR(50)
	
	SELECT
		@ModuleDetailName = ModuleDetailName,
		@ModuleDetailKey = ModuleDetailKey, 
		@ModuleDetailField = ModuleDetailField,
		@ModuleDetailFieldBefore = ModuleDetailFieldBefore,
		@PK_ModuleAction_ID = PK_ModuleAction_ID, 
		@CreatedDate = CreatedDate,
		@CreatedBy = CreatedBy
	FROM dbo.ModuleDetailApproval
	WHERE PK_ModuleDetailApproval_ID = @moduleDetailApprovalID
	
	--Get Upload Table Name
	DECLARE @bIsCreateTableUploadForEachUser AS BIT = 0
	DECLARE @strTableUploadName AS VARCHAR(500) = ''
	SELECT @bIsCreateTableUploadForEachUser = sp.SettingValue
	FROM   SystemParameter AS sp
	WHERE  sp.PK_SystemParameter_ID = 53

	--Get Date Format
	DECLARE @dateformat VARCHAR(50)
	SELECT @dateformat = sp.SettingValue
	FROM dbo.SystemParameter sp 
	WHERE sp.PK_SystemParameter_ID =14
	
	IF @bIsCreateTableUploadForEachUser = 1
	BEGIN
	    SET @strTableUploadName = @ModuleDetailName + '_Upload_data_' + @CreatedBy + '_approval'
	END
	ELSE
	BEGIN
	    SET @strTableUploadName = @ModuleDetailName + '_Upload_Approval'
	END

	--Get ModuleDetail Data
	DECLARE @moduleDetailID INT, 
			@moduleDetailLabelApproval VARCHAR(500), 
			@fkModuleId INT 
	SELECT @moduleDetailID = PK_ModuleDetail_ID, 
			@moduleDetailLabelApproval = ModuleDetailLabel,
			@fkModuleId = FK_Module_ID 
	FROM dbo.ModuleDetail 
	WHERE ModuleDetailName = @ModuleDetailName

	--Get Primary Field
	DECLARE @primaryField VARCHAR(250)
	SELECT @primaryField = FieldName FROM ModuleDetailField WHERE FK_ModuleDetail_ID = @moduleDetailID AND IsPrimaryKey = 1
	
	-- Get Module Field Uniq, ModuleName, And PK FK -- 20221006 Davin
	DECLARE @ModuleFieldUniq VARCHAR(250) = '' 
	DECLARE @ModuleName VARCHAR(250) = ''
	DECLARE @ModuleDetailFieldFKName VARCHAR(250) = ''
	DECLARE @ModuleFieldPKName VARCHAR(250) = ''

	SELECT TOP 1 @ModuleFieldUniq = FieldName FROM dbo.ModuleField 
	WHERE IsUnik = 1 and IsPrimaryKey <> 1 and FK_Module_ID = @fkModuleId 

	SELECT @ModuleName = ModuleName FROM Module 
	WHERE PK_Module_ID = @fkModuleId 

	SELECT @ModuleDetailFieldFKName = FieldName,
			@ModuleFieldPKName = FieldNameParent 
	FROM ModuleDetailField 
	WHERE FK_FieldType_ID = 16 and FK_ModuleDetail_ID = @ModuleDetailID
	-- End 20221006 Davin

	--Clear Data
	DECLARE @sql VARCHAR(MAX)=''
	IF @ModuleDetailKey = 1
	BEGIN
	    SET @sql = 'DELETE FROM ' + @ModuleDetailName
	    exec  (@sql)
	END

	--Declare Fields
	DECLARE 
		@fields				VARCHAR(MAX),
		@fieldDefault       VARCHAR(MAX),
		@fieldactive        VARCHAR(100),
		@fieldvaluedefault  VARCHAR(MAX),
		@FieldUpdate		VARCHAR(MAX),
		@fieldupdatedefault	VARCHAR(MAX),
		@filedupdateactive	VARCHAR(100),
		@fieldprimarykey	VARCHAR(500),
		@fieldInsertValue	VARCHAR(MAX)	        
	SET @fieldInsertValue=''	        
	SET @filedupdateactive='' 
	SET @fields = ''
	SET @fieldDefault = ''
	SET @fieldactive = ''
	SET @fieldvaluedefault = ''  
	SET @FieldUpdate=''
	SET @fieldupdatedefault =''
	SET @fieldprimarykey=''

	--ModuleDetailField
	DECLARE @PK_ModuleDetailField_ID bigint, @FK_ModuleDetail_ID int, @FieldName varchar(250),
        @FieldLabel varchar(250), @Sequence int, @Required bit,
        @IsPrimaryKey bit, @IsUnik bit, @IsShowInView bit, @FK_FieldType_ID int,
        @SizeField int, @FK_ExtType_ID int, @TabelReferenceName varchar(250),
        @TableReferenceFieldKey varchar(250),
        @TableReferenceFieldDisplayName varchar(250),
        @TableReferenceFilter varchar(550), @IsUseRegexValidation bit

	DECLARE my_cursor CURSOR FAST_FORWARD READ_ONLY FOR
		SELECT PK_ModuleDetailField_ID, FK_ModuleDetail_ID, FieldName, FieldLabel, [Sequence],
			   [Required], IsPrimaryKey, IsUnik, IsShowInView, FK_FieldType_ID,
			   SizeField, FK_ExtType_ID, TabelReferenceName, TableReferenceFieldKey,
			   TableReferenceFieldDisplayName, TableReferenceFilter, IsUseRegexValidation
		FROM dbo.ModuleDetailField
		WHERE FK_ModuleDetail_ID = @moduleDetailID
			AND FK_FieldType_ID <> 14 
			--AND (IsShowInForm = 1 OR IsPrimaryKey = 1)
		ORDER BY [Sequence]
	
	OPEN my_cursor
		
	FETCH FROM my_cursor INTO @PK_ModuleDetailField_ID, @FK_ModuleDetail_ID, @FieldName,
	                          @FieldLabel, @Sequence, @Required, @IsPrimaryKey,
	                          @IsUnik, @IsShowInView, @FK_FieldType_ID,
	                          @SizeField, @FK_ExtType_ID, @TabelReferenceName,
	                          @TableReferenceFieldKey, @TableReferenceFieldDisplayName,
	                          @TableReferenceFilter, @IsUseRegexValidation
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF @IsPrimaryKey = 1 SET @fieldprimarykey = @FieldName
	    
		/*{ ... Cursor logic here ... }*/    
           IF  @FK_ExtType_ID=11 OR (@FK_ExtType_ID=7 AND @FK_FieldType_ID = 11)     
             BEGIN
			  FETCH FROM my_cursor INTO @PK_ModuleDetailField_ID, @FK_ModuleDetail_ID, @FieldName,
				@FieldLabel, @Sequence, @Required, @IsPrimaryKey,
				@IsUnik, @IsShowInView, @FK_FieldType_ID,
				@SizeField, @FK_ExtType_ID, @TabelReferenceName,
				@TableReferenceFieldKey, @TableReferenceFieldDisplayName,
				@TableReferenceFilter, @IsUseRegexValidation
				CONTINUE
			 END


	    IF @FK_FieldType_ID <> 12 AND @FK_FieldType_ID <> 15
			SET @fields = @fields + @FieldName + ',' + CHAR(10) + CHAR(13)    
	    
	    IF @FK_FieldType_ID = 5
			SET @fieldInsertValue= @fieldInsertValue + 'try_convert(decimal(28,2),'+@strTableUploadName+'.'+ @FieldName + '),' + CHAR(10) + CHAR(13)
	    
	    IF @FK_FieldType_ID = 6
			SET @fieldInsertValue= @fieldInsertValue + 'try_convert(float,CASE WHEN '+@strTableUploadName+'.'+ @FieldName + '='''' then null else '+@strTableUploadName+'.'+ @FieldName +' end ),' + CHAR(10) + CHAR(13) 
	    
	    IF @FK_FieldType_ID NOT IN (5, 6, 10, 11, 12, 15, 16)
			SET @fieldInsertValue= @fieldInsertValue + @strTableUploadName+'.'+ @FieldName + ',' + CHAR(10) + CHAR(13)

		IF @FK_FieldType_ID BETWEEN 1 AND 8
			SET @FieldUpdate = @FieldUpdate + @FieldName +'= case when '+ @strTableUploadName+'.'+ @FieldName +'='''' then null else ' + @strTableUploadName+'.'+ @FieldName +' end ,'+CHAR(10)+CHAR(13)
		
		IF @FK_FieldType_ID = 9  --varchar
			-- jika tipenya RichText maka data tidak berubah
			IF @FK_ExtType_ID <> 16
            BEGIN
				SET @FieldUpdate=@FieldUpdate + @FieldName +'='+ @strTableUploadName+'.'+ @FieldName +','+CHAR(10)+CHAR(13)
			END
			
		IF @FK_FieldType_ID = 10	
		BEGIN
			SET @FieldUpdate=@FieldUpdate + @FieldName +'=dbo.ufn_GetDateValue('+ @strTableUploadName+'.'+@FieldName +','''+@dateformat+'''),'+CHAR(10)+CHAR(13)
			 IF @Required = 1
            BEGIN
                SET @fieldInsertValue = @fieldInsertValue + @strTableUploadName + '.' + @FieldName + ',' + CHAR(10) + CHAR(13)
            END
            ELSE
            BEGIN
                SET @fieldInsertValue = @fieldInsertValue + 'CASE WHEN  ' + @strTableUploadName + '.' + @FieldName + '='''' or ' + @strTableUploadName + '.' +  @FieldName  + ' =''NULL'' then null else ' + @strTableUploadName + '.' + @FieldName + ' end as ' + @FieldName + ' ,' + CHAR(10) + CHAR(13)
            END
			--SET @fieldInsertValue= @fieldInsertValue +'dbo.ufn_GetDateValue('+@strTableUploadName+'.'+ @FieldName +','''+@dateformat+'''),'+CHAR(10)+CHAR(13)
		END
		
		IF @FK_FieldType_ID = 11
		BEGIN
		IF @FK_ExtType_ID = 7
			BEGIN
				SET @fieldInsertValue=  @fieldInsertValue
				--SET @FieldUpdate = @FieldUpdate + @FieldName + '= 2112 ,' + CHAR(10)
			END
		ELSE IF @FK_ExtType_ID = 15
		BEGIN
			SET @FieldUpdate=@FieldUpdate + @FieldName +'='+ @strTableUploadName+'.'+ @FieldName +','+CHAR(10)+CHAR(13)
			SET @fieldInsertValue= @fieldInsertValue + @strTableUploadName + '.' +@FieldName + ',' + CHAR(10) + CHAR(13)
		END
		ELSE
			BEGIN
				SET @FieldUpdate=@FieldUpdate + @FieldName +'=nullif(SUBSTRING( '+@strTableUploadName+'.'+@FieldName+', CHARINDEX(''('', '+@strTableUploadName+'.'+@FieldName+',1)+1,  case when CHARINDEX('')'', '+@strTableUploadName+'.'+@FieldName+',1)-2 >0 then CHARINDEX('')'', '+@strTableUploadName+'.'+@FieldName+',1)-2 ELSE 0 END ),''''),' + char(10)+CHAR(13)

				SET @fieldInsertValue= @fieldInsertValue + 'nullif(SUBSTRING( '+@strTableUploadName+'.'+@FieldName+', CHARINDEX(''('', '+@strTableUploadName+'.'+@FieldName+',1)+1,  case when CHARINDEX('')'', '+@strTableUploadName+'.'+@FieldName+',1)-2 >0 then CHARINDEX('')'', ' +@strTableUploadName+'.'+@FieldName+',1)-2 ELSE 0 END ),''''),' + char(10)+CHAR(13)
			END
		END

			--20221005 Davin 
		IF @FK_FieldType_ID = 16 
        BEGIN
			--SET @FieldUpdate=@FieldUpdate + @FieldName +'=nullif(SUBSTRING( '+@strTableUploadName+'.'+@FieldName+', CHARINDEX(''('', '+@strTableUploadName+'.'+@FieldName+',1)+1,  case when CHARINDEX('')'', '+@strTableUploadName+'.'+@FieldName+',1)-2 >0 then CHARINDEX('')'', '+@strTableUploadName+'.'+@FieldName+',1)-2 ELSE 0 END ),''''),' + char(10)+CHAR(13)
			SET @FieldUpdate=@FieldUpdate +@FieldName + '=' + @strTableUploadName+'.'+@FieldName +',' + char(10)+CHAR(13)

			SET @fieldInsertValue = @fieldInsertValue  + @ModuleName + '.' + @ModuleFieldPKName +  ',' + CHAR(10) + CHAR(13)
			
        END
		-----------

		IF @FK_FieldType_ID = 13
		BEGIN
			SET @FieldUpdate=@FieldUpdate + @FieldName +'='+ @strTableUploadName+'.'+ @FieldName +','+CHAR(10)+CHAR(13)
		END

	    FETCH FROM my_cursor INTO @PK_ModuleDetailField_ID, @FK_ModuleDetail_ID, @FieldName,
	                          @FieldLabel, @Sequence, @Required, @IsPrimaryKey,
	                          @IsUnik, @IsShowInView, @FK_FieldType_ID,
	                          @SizeField, @FK_ExtType_ID, @TabelReferenceName,
	                          @TableReferenceFieldKey, @TableReferenceFieldDisplayName,
	                          @TableReferenceFilter, @IsUseRegexValidation
	END
	
	CLOSE my_cursor
	DEALLOCATE my_cursor

	IF LEN(@fields) > 0 SET @fields = SUBSTRING(@fields, 1, LEN(@fields) -3)
	IF LEN(@fieldInsertValue) > 0 SET @fieldInsertValue= SUBSTRING(@fieldInsertValue, 1, LEN(@fieldInsertValue) -3)

	DECLARE 
		@PK_ModuleDetailField_ID_Default	BIGINT,
		@FieldName_Default					VARCHAR(250),
	    @FieldLabel_Default					VARCHAR(250),
	    @Sequence_Default					INT,
	    @Required_Default					BIT,
	    @IsPrimaryKey_Default				BIT,
	    @IsUnik_Default					BIT,
	    @FK_FieldType_ID_Default			INT,
	    @SizeField_Default					INT,
	    @FK_ExtType_ID_Default				INT
	
	DECLARE my_cursor CURSOR FAST_FORWARD READ_ONLY 
	FOR
	    SELECT PK_ModuleField_ID,
	           FieldName,
	           FieldLabel,
	           Sequence,
	           [Required],
	           IsPrimaryKey,
	           IsUnik,
	           FK_FieldType_ID,
	           SizeField,
	           FK_ExtType_ID
	    FROM   dbo.ModuleFieldDefault 
	
	OPEN my_cursor
	
	FETCH FROM my_cursor INTO @PK_ModuleDetailField_ID_Default, @FieldName_Default, @FieldLabel_Default,
	@Sequence_Default, @Required_Default, @IsPrimaryKey_Default, @IsUnik_Default,
	@FK_FieldType_ID_Default, @SizeField_Default, @FK_ExtType_ID_Default
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @fieldDefault = @fieldDefault + @FieldName_Default + ',' + CHAR(10) +CHAR(13)
	    
	    IF @FK_FieldType_ID_Default = 13 and @FieldName_Default = 'Active'
	    BEGIN
			SET @fieldactive = @strTableUploadName+'.'+@FieldName_Default + ',' + CHAR(10) + CHAR(13)	    
			set @filedupdateactive=@filedupdateactive +@FieldName_Default +'='+ @strTableUploadName+'.' +@FieldName_Default +','+ CHAR(10) + CHAR(13)
	    END
	    
	    IF @PK_ModuleDetailField_ID_Default= 2
	    BEGIN
			SET @fieldvaluedefault = @fieldvaluedefault + '''' + @CreatedBy + ''','+ CHAR(10) + CHAR(13)
		END 
	    
	    IF @PK_ModuleDetailField_ID_Default= 3 
	    begin
			SET @fieldupdatedefault = @fieldupdatedefault +@FieldName_Default+ '=''' + @CreatedBy + ''','+ CHAR(10) + CHAR(13)
			SET @fieldvaluedefault = @fieldvaluedefault + '''' + @CreatedBy + ''','+ CHAR(10) + CHAR(13)
		END

	    IF  @PK_ModuleDetailField_ID_Default=4
	    BEGIN
	    	SET @fieldupdatedefault = @fieldupdatedefault +@FieldName_Default+ '=''' + @userID + ''','+ CHAR(10) + CHAR(13)
	    	SET @fieldvaluedefault = @fieldvaluedefault + '''' + @userID + ''','+ CHAR(10) + CHAR(13)
	    END
			
		IF  @PK_ModuleDetailField_ID_Default=8
	    BEGIN
	    	SET @fieldupdatedefault = @fieldupdatedefault +@FieldName_Default+ '='''','+ CHAR(10) + CHAR(13)
	    	SET @fieldvaluedefault = @fieldvaluedefault + ''''','+ CHAR(10) + CHAR(13)
	    END
	    
	    IF @FK_FieldType_ID_Default = 10
	        SET @fieldvaluedefault = @fieldvaluedefault + '''' + CONVERT(VARCHAR(20), GETDATE(), 120)+ ''',' + CHAR(10) + CHAR(13)
	    
	     IF @PK_ModuleDetailField_ID_Default= 6 or @PK_ModuleDetailField_ID_Default=7
			SET @fieldupdatedefault = @fieldupdatedefault +@FieldName_Default+ '=''' +  CONVERT(VARCHAR(20), GETDATE(), 120) + ''','+ CHAR(10) + CHAR(13)

		IF @FK_FieldType_ID_Default = 13 and @FieldName_Default = 'Draft'
	    BEGIN
	    	SET @fieldupdatedefault = @fieldupdatedefault +@FieldName_Default+ '=1 ,'+ CHAR(10) + CHAR(13)
	    	SET @fieldvaluedefault = @fieldvaluedefault + '1' + ','+ CHAR(10) + CHAR(13)
	    END
	    
	    FETCH FROM my_cursor INTO @PK_ModuleDetailField_ID_Default, @FieldName_Default, @FieldLabel_Default,
			@Sequence_Default, @Required_Default, @IsPrimaryKey_Default, @IsUnik_Default,
			@FK_FieldType_ID_Default, @SizeField_Default, @FK_ExtType_ID_Default
	END
	
	CLOSE my_cursor
	DEALLOCATE my_cursor

	IF LEN(@fieldDefault) > 0
	    SET @fieldDefault = SUBSTRING(@fieldDefault, 1, LEN(@fieldDefault) -3)
	
	IF LEN(@fieldvaluedefault) > 0
	    SET @fieldvaluedefault = SUBSTRING(@fieldvaluedefault, 1, LEN(@fieldvaluedefault) -3)

	--Insert Mode	
	DECLARE @strinsert VARCHAR(MAX) = ''
	DECLARE @moduleNameUploadApproval  VARCHAR(250) = @modulename+ '_Upload_approval'
	
	SET @strinsert = 'INSERT INTO ' + @ModuleDetailName + '(' + CHAR(10) + CHAR(13)	
	SET @strinsert += @fields
	 
	IF LEN(@fieldDefault) > 0
	BEGIN
	    SET @strinsert += ',' + CHAR(10) + CHAR(13)
	    SET @strinsert += @fieldDefault + CHAR(10) + CHAR(13)
	END
	
	SET @strinsert += ')' + CHAR(10) + CHAR(13)
	SET @strinsert += ' SELECT ' + CHAR(10) + CHAR(13)
	SET @strinsert += @fieldInsertValue
	 
	IF LEN(@fieldactive) > 0
	BEGIN
	    SET @strinsert += ',' + CHAR(10) + CHAR(13)
	    SET @strinsert += @fieldactive + CHAR(10) + CHAR(13)
	END
	
	SET @strinsert += @fieldvaluedefault + CHAR(10) + CHAR(13)
	SET @strinsert += ' FROM ' + @strTableUploadName
	SET @strinsert += ' JOIN ' + @moduleNameUploadApproval + ' On ' + @strTableUploadName + '.' + @ModuleDetailFieldFKName + ' = ' + @moduleNameUploadApproval+ '.' + @ModuleFieldPKName 
	SET @strinsert += ' JOIN ' + @ModuleName + ' On ' + @ModuleName + '.' + @ModuleFieldUniq + 
		'= CASE WHEN  CHARINDEX('')'','+@moduleNameUploadApproval+ '.' + @ModuleFieldUniq+') <> 0 
		THEN SUBSTRING('+ @moduleNameUploadApproval+ '.' + @ModuleFieldUniq+',2, CHARINDEX('')'','+ @moduleNameUploadApproval+ '.' + @ModuleFieldUniq+')-2)  
		ELSE '+ @moduleNameUploadApproval+ '.' + @ModuleFieldUniq+' END'
	SET @strinsert += ' WHERE '+@strTableUploadName+'.FK_ModuleDetailApproval_ID = ' + CAST(@moduleDetailApprovalID AS VARCHAR)
	SET @strinsert += ' AND '+@strTableUploadName+'.nawa_Action=''INSERT'' AND '+@strTableUploadName+'.KeteranganError='''' order by '+@strTableUploadName+'.nawa_recordnumber'

	exec (@strinsert)

	--Update Mode
	DECLARE @strupdate VARCHAR(MAX) = ''
	
	IF LEN(@fieldupdatedefault) > 0
	    SET @fieldupdatedefault = SUBSTRING(@fieldupdatedefault, 1, LEN(@fieldupdatedefault) -3)

	SET @strupdate = 'UPDATE '+ @ModuleDetailName +' SET ' + char(10)
		+ @FieldUpdate + ' '
		+ @filedupdateactive+ ' '
		+ @fieldupdatedefault+ ' '
		+ ' from '+@ModuleDetailName
        + '	INNER JOIN '+@strTableUploadName+' ON '+@ModuleDetailName+'.'+@fieldprimarykey +'='+@strTableUploadName+'.'+@fieldprimarykey +'' + char(10)
        + '	WHERE FK_ModuleDetailApproval_ID = ' + CAST(@moduleDetailApprovalID AS VARCHAR) + ' AND ' + @strTableUploadName+'.nawa_Action=''Update'' ' + char(10)
        + '	 '
	EXEC(@strupdate)


	--delete mode
	DECLARE @sqlDelete varchar(max)
	SET @sqlDelete = '	DELETE '+@ModuleDetailName+'		 ' + char(10)
		+ '		FROM '+@ModuleDetailName+' ' + char(10)
		+ '		INNER JOIN '+@strTableUploadName+' ON '+@ModuleDetailName+'.'+@fieldprimarykey+'='+@strTableUploadName+'.'+@fieldprimarykey +'		  ' + char(10)
		+ '		WHERE '+@strTableUploadName+'.nawa_Action=''Delete'' ' + char(10)
		+ '		AND FK_ModuleDetailApproval_ID = ' + CAST(@moduleDetailApprovalID AS VARCHAR)
	EXEC(@sqlDelete)				
END


GO
