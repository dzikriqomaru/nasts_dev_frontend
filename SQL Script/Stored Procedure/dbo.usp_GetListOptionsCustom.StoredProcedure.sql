/****** Object:  StoredProcedure [dbo].[usp_GetListOptionsCustom]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[usp_GetListOptionsCustom]
	@TableReferenceName VARCHAR(250),
	@TableReferenceNameAlias VARCHAR(250),
	@TableReferenceFieldKey VARCHAR(250),
	@TableReferenceFieldDisplayName VARCHAR(250),
	@TableReferenceFilter VARCHAR(1000),
	@TableReferenceAdditonalJoin VARCHAR(1000),
	@FilterCascade VARCHAR(1000),
	@ParentValue VARCHAR(MAX),
	@UserID VARCHAR(50),
	@SearchKeyword VARCHAR(MAX) = ''
AS
DECLARE @SearchWord	VARCHAR(MAX) = '''%' + REPLACE(@SearchKeyword, '''', '''''') + '%''',
		@SQLQuery NVARCHAR(MAX),
		@Params NVARCHAR(MAX) = '@Parent VARCHAR(MAX)'

SET @TableReferenceNameAlias = CASE WHEN ISNULL(@TableReferenceNameAlias, '') = '' THEN @TableReferenceName ELSE @TableReferenceNameAlias END

SET @SQLQuery = 'SELECT ' + @TableReferenceNameAlias + '.' + @TableReferenceFieldKey + ' AS [value],' + CHAR(10) + CHAR(13) +
				'		' + @TableReferenceNameAlias + '.' + @TableReferenceFieldDisplayName + ' AS [text]' + CHAR(10) + CHAR(13) +
				'FROM ' + @TableReferenceName + ' AS ' + @TableReferenceNameAlias + CHAR(10) + CHAR(13) +

				-- search
				'WHERE (' + @TableReferenceNameAlias + '.' + @TableReferenceFieldKey + ' LIKE ' + @SearchWord + 
				' OR '+ @TableReferenceNameAlias + '.' + @TableReferenceFieldDisplayName + ' LIKE ' + @SearchWord + ') ' +

				-- filter
				CASE WHEN ISNULL(@TableReferenceFilter, '') <> ''
					 THEN 'AND ' + REPLACE(@TableReferenceFilter, '@UserID', @UserID) +
							CASE WHEN ISNULL(@FilterCascade, '') <> ''
								 THEN ' AND ' + @FilterCascade
								 ELSE ''
							END
					 ELSE CASE WHEN ISNULL(@FilterCascade, '') <> ''
							   THEN 'AND ' + @FilterCascade
							   ELSE ''
						  END
				END
				
EXEC sp_executesql @SQLQuery, @Params, @ParentValue
GO
