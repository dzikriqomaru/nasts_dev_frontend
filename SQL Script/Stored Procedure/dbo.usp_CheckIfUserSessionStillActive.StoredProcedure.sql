/****** Object:  StoredProcedure [dbo].[usp_CheckIfUserSessionStillActive]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[usp_CheckIfUserSessionStillActive]
	@Userid varchar(50)
as 
if	exists(
			select * 
			from (
				select top 1 *, DATEDIFF(MINUTE,ActionDate,getdate())timeout
				from AuditTrail_UserAccess 
				where Userid = @Userid 
				order by PK_UserAccessID  desc
			)a where  timeout > (select SettingValue  from SystemParameter where PK_SystemParameter_ID = 17)
)
	select casT(0 as bit) islogin
else 
	select casT(1 as bit) islogin
GO
