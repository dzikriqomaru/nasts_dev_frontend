/****** Object:  StoredProcedure [dbo].[usp_GetstructureByModule]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_GetstructureByModule]
/***********************************************************
* Procedure description:
* Date:   4/14/2018 
* Author: Hendra
*
* Changes
* Date		Modified By			Comments
************************************************************
*
************************************************************/
(
	@PKModuleID int
)
AS
BEGIN
	
	
	
	
	
  SELECT  
	CASE WHEN mt.FieldTypeCaption<>'Varbinary' THEN   mf.FieldName ELSE mf.FieldName+'Name' END AS FieldName, mf.FieldLabel , mt.CodeEffecttype ,  mt.CodeEffectNETType ,mf.Sequence				
   FROM ModuleField mf 
  INNER JOIN MFieldType mt ON mf.FK_FieldType_ID=mt.PK_FieldType_ID
  WHERE mf.FK_Module_ID=@PKModuleID AND mf.FK_FieldType_ID <>11
  union
   SELECT  mf.FieldName, 
	mf.FieldLabel  ,  (
	
  SELECT  mt.CodeEffecttype
    FROM INFORMATION_SCHEMA.[COLUMNS] c 
  INNER JOIN MFieldType mt ON mt.FieldTypeCaption=c.DATA_TYPE
  WHERE c.TABLE_NAME=m.ModuleName AND c.COLUMN_NAME=mf.FieldName
	        		
	) CodeEffecttype,
	 (
	
  SELECT  mt.CodeEffectNETType
    FROM INFORMATION_SCHEMA.[COLUMNS] c 
  INNER JOIN MFieldType mt ON mt.FieldTypeCaption=c.DATA_TYPE
  WHERE c.TABLE_NAME=m.ModuleName AND c.COLUMN_NAME=mf.FieldName
	        		
	) CodeEffectNETType
			,mf.Sequence	
   FROM ModuleField mf 
   INNER JOIN module m ON m.PK_Module_ID=mf.FK_Module_ID
  INNER JOIN MFieldType mt ON mf.FK_FieldType_ID=mt.PK_FieldType_ID
  WHERE mf.FK_Module_ID=@PKModuleID AND mf.FK_FieldType_ID =11
  
 UNION 
 SELECT  'Active' AS FieldName,'Active' AS FieldLabel,'bool','System.Boolean',100 AS sequence
 
   FROM module WHERE PK_Module_ID=@PKModuleID
   AND IsSupportActivation=1
   ORDER BY mf.Sequence
   
   
END
GO
