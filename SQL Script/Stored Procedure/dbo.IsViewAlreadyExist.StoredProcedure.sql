/****** Object:  StoredProcedure [dbo].[IsViewAlreadyExist]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[IsViewAlreadyExist]
/***********************************************************
* Procedure description:
* Date:   12/02/2016 
* Author: Hendra
*
* Changes
* Date		Modified By			Comments
************************************************************
*
************************************************************/
(
	@ViewName VARCHAR(550)
)
AS
BEGIN
	
	 
	SELECT  COUNT(1) AS Jml  FROM INFORMATION_SCHEMA.[VIEWS] v WHERE v.TABLE_NAME=@ViewName

END
GO
