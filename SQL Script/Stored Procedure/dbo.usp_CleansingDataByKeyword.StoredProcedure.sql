/****** Object:  StoredProcedure [dbo].[usp_CleansingDataByKeyword]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Davin Julian
-- Create date: 10 October 2023
-- Description:	Sp Cleansing Data by Keyword
-- =============================================
CREATE   PROCEDURE [dbo].[usp_CleansingDataByKeyword] 
	@Keywords Varchar(max)
AS
BEGIN
	DROP Table IF EXISTS #Temp
	EXECUTE ('SELECT * INTO #Temp from sys.objects
	where  type_desc IN (''SQL_STORED_PROCEDURE'', ''USER_TABLE'',''VIEW'') And name like ''%' + @Keywords + '%''');


	DECLARE @ObjectName VARCHAR(100)
	DECLARE @TableType VARCHAR(50)
	DECLARE @i INT = 0;
	DECLARE @count INT = (SELECT COUNT(1) FROM #Temp)
	DECLARE @TableWhere VARCHAR(MAX)=''''''

	WHILE @i <= @count
	BEGIN
		PRINT CAST(@i AS VARCHAR(50)) + '. Object Name: '+ @ObjectName + '; Type: '+ @TableType + ' is deleting'
	
		SELECT TOP 1 @ObjectName = name, @TableType = type_desc FROM  #Temp
	
		-----------DROP OBJECT SCHEMA----------------- 
		IF @TableType = 'USER_TABLE'
		BEGIN
			EXECUTE ('DROP TABLE IF EXISTS ['+@ObjectName +']')
			IF @i > 0
			BEGIN
				SET @TableWhere = @TableWhere + ', ''' + @ObjectName+''''
			END
			ELSE
			BEGIN
				SET @TableWhere = ''''+ @ObjectName +''''
			END
		END
		ELSE IF @TableType = 'VIEW'
		BEGIN
			EXECUTE ('DROP View IF EXISTS ['+@ObjectName +']')
		END
		ELSE IF @TableType = 'SQL_STORED_PROCEDURE'
		BEGIN
			EXECUTE ('DROP Procedure IF EXISTS ['+@ObjectName +']')
		END

		Delete FROM #Temp WHERE name = @ObjectName
		SET @i = @i +1;
	END


	-----------DELETE DATA IN FRAMEWORK TABLE----------------- 
		EXECUTE('
		DELETE ModuleDetailFieldRegex
		WHERE FK_ModuleDetailField_ID IN (
				SELECT PK_ModuleDetailField_ID
				FROM ModuleDetailField
				WHERE FK_ModuleDetail_ID IN (
						SELECT PK_ModuleDetail_ID
						FROM ModuleDetail
						WHERE FK_Module_ID IN (
							SELECT PK_Module_ID
							FROM Module
							WHERE ModuleName IN (
							'+ @TableWhere +'
							)
						)
				)
			)');

		EXECUTE('
		DELETE ModuleDetailField
		WHERE FK_ModuleDetail_ID IN (
				SELECT PK_ModuleDetail_ID
				FROM ModuleDetail
				WHERE FK_Module_ID IN (
						SELECT PK_Module_ID
						FROM Module
						WHERE ModuleName IN (
						'+ @TableWhere +'
						)
				)
		)');

		EXECUTE('
		DELETE
		FROM ModuleDetail
		WHERE FK_Module_ID IN (
			SELECT PK_Module_ID
			FROM Module
			WHERE ModuleName IN (
			'+ @TableWhere +'
			)
		)');

		EXECUTE('
		DELETE ModuleFieldRegex
		WHERE FK_ModuleField_ID IN (
				SELECT PK_ModuleField_ID
				FROM ModuleField
				WHERE FK_Module_ID IN (
						SELECT PK_Module_ID
						FROM Module
						WHERE ModuleName IN (
						'+ @TableWhere +'
						)
				)
			)');

		EXECUTE('
		DELETE ModuleField
		WHERE FK_Module_ID IN (
				SELECT PK_Module_ID
				FROM Module
				WHERE ModuleName IN (
						'+ @TableWhere +'
						)
				)');

		EXECUTE('
		DELETE ModuleValidation
		WHERE FK_Module_ID IN (
			SELECT PK_Module_ID
			FROM Module
			WHERE ModuleName IN (
					'+ @TableWhere +'
					)
			)');

		EXECUTE('
		DELETE
		FROM Module
		WHERE ModuleName IN (
			'+ @TableWhere +'
			)');

		DELETE
		FROM MenuTemplate
		WHERE FK_Module_ID NOT IN (
				SELECT PK_Module_ID
				FROM Module
				) OR FK_Module_ID IS NULL

		DELETE
		FROM MGroupMenuAccess
		WHERE FK_Module_ID NOT IN (
				SELECT PK_Module_ID
				FROM Module
				)

		DELETE ModuleApproval Where 1=1
END
GO
