/****** Object:  StoredProcedure [dbo].[usp_SaveMenuTemplateApprovalAuditTrail]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_SaveMenuTemplateApprovalAuditTrail]
/***********************************************************
* Procedure description:
* Date:   11/18/2019 
* Author: nawadata
*
* Changes
* Date			Modified By			Comments
************************************************************
* 31 Mar 2023	Humam				Add IconName
************************************************************/
(
	@tblMenuTemplate AS tblMenuTemplate READONLY,
	@createdby                  VARCHAR(50),
    @Approveby                  VARCHAR(50),
    @FK_AuditTrailStatus_ID     INT,
    @FK_ModuleAction_ID         INT,
    @ModuleLabel                VARCHAR(50),
    @PK_ModuleApproval_ID       BIGINT	
)
AS
BEGIN
		DECLARE @pkAudittrailheader BIGINT
	INSERT INTO AuditTrailHeader
	  (
	    -- PK_AuditTrail_ID -- this column value is auto-generated
	    CreatedDate,
	    CreatedBy,
	    ApproveBy,
	    ModuleLabel,
	    FK_ModuleAction_ID,
	    FK_AuditTrailStatus_ID,
	    PK_ModuleApproval_ID
	  )
	VALUES
	  (
	    GETDATE(),
	    @createdby,
	    @Approveby,
	    @ModuleLabel,
	    @FK_ModuleAction_ID,
	    @FK_AuditTrailStatus_ID,
	    @PK_ModuleApproval_ID
	  )
	
	SET @pkAudittrailheader = SCOPE_IDENTITY()
	
		
	INSERT INTO AuditTrailDetail
	  (
	    -- PK_AuditTrailDetail_id -- this column value is auto-generated
	    FK_AuditTrailHeader_ID,
	    FieldName,
	    OldValue,
	    NewValue
	  )
	  	SELECT @pkAudittrailheader     FK_AuditTrailHeader_ID,
	       ca.fieldname,
	       ''                      OldValue,
	       ca.NewValue
	FROM   (
	        SELECT * FROM @tblMenuTemplate
	       )xx
	       CROSS APPLY (
	    VALUES 
	     ('mMenuID', try_convert(varchar(8000),mMenuID)),
('mMenuLabel', try_convert(varchar(8000),mMenuLabel)),
('mMenuParentID', try_convert(varchar(8000),mMenuParentID)),
('mMenuURL', try_convert(varchar(8000),mMenuURL)),
('FK_Module_ID', try_convert(varchar(8000),FK_Module_ID)),
('FK_Action_ID', try_convert(varchar(8000),FK_Action_ID)),
('urutan', try_convert(varchar(8000),urutan)),
('IconName', try_convert(varchar(8000),IconName))
	)ca(Fieldname, NewValue)
	
	
	
	
END
GO
