/****** Object:  StoredProcedure [dbo].[usp_GenerateTableApprovalDesigner]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_GenerateTableApprovalDesigner]
/***********************************************************
* Procedure description:
* Date:   23/08/2021 
* Author: Fauzan
*
* Changes
* 
* Date		Modified By			Comments
************************************************************
*
************************************************************/
(@ApprovalDesignerID INT)
AS
BEGIN
	--DECLARE @ApprovalDesignerID INT
	--SET @ApprovalDesignerID =9846

	DECLARE 
	@ApprovalDesignerName			VARCHAR(250),
	@ApprovalDesignerLabel			VARCHAR(250),
	@ApprovalDesignerDescription	VARCHAR(500),
	@IsUseDesigner					BIT,
	@IsUseApproval					BIT,
	@IsSupportAdd					BIT,
	@IsSupportEdit					BIT,
	@IsSupportDelete				BIT,
	@IsSupportActivation			BIT,
	@IsSupportView					BIT,
	@UrlAdd							VARCHAR(550),
	@UrlEdit						VARCHAR(550),
	@UrlDelete						VARCHAR(550),
	@UrlActivation					VARCHAR(550),
	@UrlView						VARCHAR(550),
	@UrlApproval					VARCHAR(250),
	@IsUseStoreProcedureValidation	BIT
	
	
	SELECT 
		@ApprovalDesignerName = ApprovalDesignerName,
		@ApprovalDesignerLabel = ApprovalDesignerLabel,
		@ApprovalDesignerDescription = ApprovalDesignerDescription,
		@IsUseDesigner = IsUseDesigner,
		@IsUseApproval = IsUseApproval,
		@IsSupportAdd = IsSupportAdd,
		@IsSupportEdit = IsSupportEdit,
		@IsSupportDelete = IsSupportDelete,
		@IsSupportActivation = IsSupportActivation,
		@IsSupportView = IsSupportView,
		@UrlAdd = UrlAdd,
		@UrlEdit = UrlEdit,
		@UrlDelete = UrlDelete,
		@UrlActivation = UrlActivation,
		@UrlView = UrlView,
		@UrlApproval = UrlApproval,
		@IsUseStoreProcedureValidation = IsUseStoreProcedureValidation
	FROM ApprovalDesigner
	WHERE PK_ApprovalDesigner_ID = @ApprovalDesignerID

	IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES t WHERE t.TABLE_NAME = @ApprovalDesignerName)
	BEGIN
		DECLARE @sql VARCHAR(8000)
		SET @sql = 'CREATE TABLE dbo.' + @ApprovalDesignerName + CHAR(10)
					+ '	( ' + CHAR(10)
	    
		DECLARE 
		@PK_ApprovalDesignerField_ID    BIGINT,
		@FK_ApprovalDesigner_ID         INT,
		@FieldName						VARCHAR(250),
		@FieldLabel						VARCHAR(250),
		@Sequence						INT,
		@Required						BIT,
		@IsPrimaryKey					BIT,
		@IsUnik							BIT,
		@FK_FieldType_ID				INT,
		@SizeField						INT,
		@FK_ExtType_ID					INT,
		@TabelReferenceName				VARCHAR(250),
		@TableReferenceFieldKey			VARCHAR(250),
		@TableReferenceFieldDisplayName VARCHAR(250),
		@TableReferenceFilter			VARCHAR(550),
		@IsUseRegexValidation			BIT
	    
		DECLARE my_cursor CURSOR FAST_FORWARD READ_ONLY 
		FOR
			SELECT 
				PK_ApprovalDesignerField_ID,
				FK_ApprovalDesigner_ID,
				FieldName,
				FieldLabel,
				Sequence,
				[Required],
				IsPrimaryKey,
				IsUnik,
				FK_FieldType_ID,
				SizeField,
				FK_ExtType_ID,
				TabelReferenceName,
				TableReferenceFieldKey,
				TableReferenceFieldDisplayName,
				TableReferenceFilter,
				IsUseRegexValidation
			FROM dbo.ApprovalDesignerField 
			WHERE FK_ApprovalDesigner_ID = @ApprovalDesignerID 	        
			UNION ALL
			SELECT 
				a.PK_ApprovalDesignerField_ID,
				@ApprovalDesignerID, 
				REPLACE(a.FieldName, '[FieldName]', b.FieldName) AS FieldNAme, 
				REPLACE(a.FieldLabel,'[FieldName]',b.FieldLabel) AS FieldLabel, 
				a.Sequence, 
				a.[Required],
				a.IsPrimaryKey, 
				a.IsUnik, 
				a.FK_FieldType_ID, 
				a.SizeField,
				a.FK_ExtType_ID,
				NULL,
				NULL,
				NULL,
				NULL,
				NULL
			FROM ApprovalDesignerFieldDefaultFileUpload a
			CROSS JOIN dbo.ApprovalDesignerField b 
			WHERE 
				b.FK_ApprovalDesigner_ID = @ApprovalDesignerID 
				AND b.FK_ExtType_ID = 8 
				AND b.FK_FieldType_ID=14   
			ORDER BY Sequence
	    
		OPEN my_cursor
		FETCH FROM my_cursor INTO @PK_ApprovalDesignerField_ID, @FK_ApprovalDesigner_ID, @FieldName,
		@FieldLabel, @Sequence, @Required, @IsPrimaryKey, @IsUnik, @FK_FieldType_ID, @SizeField, 
		@FK_ExtType_ID, @TabelReferenceName, @TableReferenceFieldKey, @TableReferenceFieldDisplayName,
		@TableReferenceFilter, @IsUseRegexValidation
	    
		WHILE @@FETCH_STATUS = 0
		BEGIN
			DECLARE @datatype VARCHAR(30)=''
	        
			IF @FK_FieldType_ID<>11 
			BEGIN
				SELECT @datatype = FieldTypeSQLName
				FROM   MFieldType
				WHERE  PK_FieldType_ID = @FK_FieldType_ID
			END 
	    
			IF @FK_FieldType_ID=11
			BEGIN
				IF @FK_ExtType_ID=15
				BEGIN
					--Varchar(max) because it's a checkbox
					set @datatype = 'varchar(max)'
				END
				ELSE
				BEGIN
					SELECT	@datatype = c.DATA_TYPE + case when c.CHARACTER_MAXIMUM_LENGTH IS NULL THEN '' WHEN c.CHARACTER_MAXIMUM_LENGTH=-1 THEN '(max)' ELSE '('+ CONVERT(VARCHAR(20), c.CHARACTER_MAXIMUM_LENGTH) +')' END   
	        		FROM	INFORMATION_SCHEMA.[COLUMNS] c 
					WHERE	c.TABLE_NAME = @TabelReferenceName AND c.COLUMN_NAME = @TableReferenceFieldKey
				END
			END

			DECLARE @strsizefield VARCHAR(30) = ''	    
			IF @FK_FieldType_ID = 9 OR @FK_FieldType_ID = 14
				IF @SizeField <> -1 
					SET @strsizefield = ' (' + CONVERT(VARCHAR(50), @SizeField) +')'	
				ELSE
					SET @strsizefield = ' (max)'

			DECLARE @strreq AS VARCHAR(30) = ''
			IF @Required = 1
				SET @strreq = ' Not Null '
			ELSE
				SET @strreq = ' NULL '
	        
			DECLARE @identity VARCHAR(30)
			SET @identity = ''

			IF @FK_FieldType_ID = 12 OR @FK_FieldType_ID = 15
				SET @identity = ' IDENTITY (1, 1) '	
	        
			SET @sql = @sql + ' [' + @FieldName + ']  ' + @datatype + @strsizefield + ' ' + @strreq + @identity + ',' + CHAR(10) + CHAR(13)
	        
			FETCH FROM my_cursor INTO @PK_ApprovalDesignerField_ID, @FK_ApprovalDesigner_ID, @FieldName,
			@FieldLabel, @Sequence, @Required, @IsPrimaryKey,
			@IsUnik, @FK_FieldType_ID, @SizeField,
			@FK_ExtType_ID, @TabelReferenceName,
			@TableReferenceFieldKey,
			@TableReferenceFieldDisplayName,
			@TableReferenceFilter, @IsUseRegexValidation
		END
	    
		CLOSE my_cursor
		DEALLOCATE my_cursor

		DECLARE @PK_ApprovalDesignerField_IDdefault bigint, @FieldNameDefault varchar(250),@FieldLabelDefault varchar(250), 
		@SequenceDefault int, @Requireddefault bit, @IsPrimaryKeyDefault bit, @IsUnikDefault bit,
		@FK_FieldType_IDDefault int, @SizeFieldDefault int, @FK_ExtType_IDDefault int
	    
		DECLARE my_cursor CURSOR FAST_FORWARD READ_ONLY FOR
		SELECT PK_ApprovalDesignerField_ID, FieldName, FieldLabel, Sequence, [Required],
				IsPrimaryKey, IsUnik, FK_FieldType_ID, SizeField, FK_ExtType_ID
		FROM dbo.ApprovalDesignerFieldDefault 
	    
		OPEN my_cursor
		FETCH FROM my_cursor INTO @PK_ApprovalDesignerField_IDdefault, @FieldNamedefault, @FieldLabeldefault,
									@Sequencedefault, @Requireddefault, @IsPrimaryKeydefault, @IsUnikdefault,
									@FK_FieldType_IDdefault, @SizeFielddefault, @FK_ExtType_IDdefault
	    
		WHILE @@FETCH_STATUS = 0
		BEGIN
			DECLARE @datatypedefault VARCHAR(30) = ''
			SELECT @datatypedefault = mt.FieldTypeSQLName
			FROM   MFieldType mt
			WHERE  mt.PK_FieldType_ID = @FK_FieldType_IDDefault
	        
			DECLARE @strsizefieldDefault VARCHAR(30)=''
			SET @strsizefielddefault = '' 
			IF @FK_FieldType_IDDefault = 9
				SET @strsizefieldDefault = ' (' + CONVERT(VARCHAR(50), @SizeFieldDefault) + ')'
	        
			DECLARE @strreqDefault AS VARCHAR(30)=''
			IF @Requireddefault = 1
				SET @strreqDefault = ' Not Null '
			ELSE
				SET @strreqDefault = ' NULL ' 

			DECLARE @identityDefault VARCHAR(30)
			SET @identityDefault = ''
			IF @FK_FieldType_IDDefault = 12 OR @FK_FieldType_IDDefault = 15
				SET @identityDefault = ' IDENTITY (1, 1) '
			
			SET @sql = @sql + ' [' + @FieldNamedefault + ']  ' + @datatypeDefault + @strsizefielddefault	 + ' ' + @strreqDefault + @identitydefault + ','+CHAR(10)+CHAR(13)
	    
			FETCH FROM my_cursor INTO @PK_ApprovalDesignerField_IDdefault, @FieldNamedefault,
			@FieldLabeldefault, @Sequencedefault, @Requireddefault,
			@IsPrimaryKeydefault, @IsUnikdefault, @FK_FieldType_IDdefault,
			@SizeFielddefault, @FK_ExtType_IDdefault
		END
	    
		CLOSE my_cursor
		DEALLOCATE my_cursor

		IF LEN(@sql) > 0
			SET @sql = SUBSTRING(@sql, 0, LEN(@sql) -3)    
	    
		SET @sql = @sql + ')  ON [PRIMARY]'

		--PRINT @sql
		EXEC (@sql)
	    
		DECLARE @keyprimary VARCHAR(50)
		SELECT @keyprimary = mf.FieldName
		FROM   ApprovalDesignerField mf
		WHERE  mf.FK_ApprovalDesigner_ID = @ApprovalDesignerID AND mf.IsPrimaryKey = 1
	    
		SET @sql = 'ALTER TABLE dbo.' + @ApprovalDesignerName + ' ADD CONSTRAINT ' + CHAR(10)
				+ '	PK_' + @ApprovalDesignerName + ' PRIMARY KEY CLUSTERED  ' + CHAR(10)
				+ '	( ' + CHAR(10)
				+ '	' + @keyprimary + ' ' + CHAR(10)
				+
				') WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY] ' 
				+ CHAR(10)
	    
			--PRINT (@sql)
			EXEC (@sql)
	END
	ELSE
	BEGIN
		DECLARE  @FieldNameCek varchar(250), @FieldTypeSQLNameCek varchar(50),
		@SizeFieldCek INT, @datatypecek VARCHAR(50)
        
		-- Get new fields
		DECLARE my_cursor CURSOR FAST_FORWARD READ_ONLY FOR
			SELECT 
				mf.FieldName,
				CASE WHEN mf.FK_ExtType_ID <> 15 THEN mt.FieldTypeSQLName ELSE 'varchar' END AS FieldTypeSQLName,
				CASE WHEN mf.FK_ExtType_ID <> 15 THEN mf.SizeField ELSE -1 END AS SizeField,
				CASE WHEN mf.FK_ExtType_ID <> 15 THEN mt.FieldTypeSQLName ELSE 'varchar' END AS FieldTypeSQLName
			FROM (
				SELECT 
					PK_ApprovalDesignerField_ID,
					FK_ApprovalDesigner_ID,
					FieldName,
					FK_FieldType_ID,
					SizeField,
					FK_ExtType_ID
				FROM  ApprovalDesignerField
				WHERE FK_ApprovalDesigner_ID = @ApprovalDesignerID 
				UNION ALL
				SELECT 
					a.PK_ApprovalDesignerField_ID,
					@ApprovalDesignerID,
					REPLACE(a.FieldName,'[FieldName]',b.FieldName),
					a.FK_FieldType_ID,
					a.SizeField,
					a.FK_ExtType_ID
				FROM ApprovalDesignerFieldDefaultFileUpload a
				CROSS JOIN dbo.ApprovalDesignerField b 
				WHERE
					b.FK_ApprovalDesigner_ID = @ApprovalDesignerID 
					AND b.FK_ExtType_ID=8
					AND b.FK_FieldType_ID=14     
			  ) mf
			INNER JOIN MFieldType mt ON mt.PK_FieldType_ID = mf.FK_FieldType_ID 
			INNER JOIN ApprovalDesigner x ON x.PK_ApprovalDesigner_ID = mf.FK_ApprovalDesigner_ID
			LEFT JOIN INFORMATION_SCHEMA.[COLUMNS] c ON mf.FieldName = c.COLUMN_NAME AND c.TABLE_NAME = x.ApprovalDesignerName
			WHERE 
				mf.FK_ApprovalDesigner_ID = @ApprovalDesignerID
				AND c.COLUMN_NAME IS NULL

		OPEN my_cursor
		FETCH FROM my_cursor INTO @FieldNameCek, @FieldTypeSQLNameCek, @SizeFieldCek, @datatypecek

		-- Add new fields
		WHILE @@FETCH_STATUS = 0
		BEGIN
			IF @SizeFieldCek = -1 
			BEGIN
				IF @datatypecek = 'varchar' OR @datatypecek = 'varbinary' SET @datatypecek = @datatypecek + '(max)'
			END
			ELSE
			BEGIN
				IF @datatypecek='varchar' OR @datatypecek = 'varbinary' SET @datatypecek = @datatypecek + '('+ CONVERT(VARCHAR(20), @SizeFieldCek) +')'
			END

			SET @sql = ' Alter table ' + @ApprovalDesignerName + ' add ' + @FieldNameCek + ' ' + @datatypecek
	
			--PRINT @sql
			EXEC(@sql)
		
			FETCH FROM my_cursor INTO  @FieldNamecek, @FieldTypeSQLNamecek, @SizeFieldcek, @datatypecek
		END

		CLOSE my_cursor
		DEALLOCATE my_cursor

		DECLARE  @FieldNamebuang varchar(250), @FieldTypeSQLNamebuang varchar(50),  @SizeFieldbuang int
	
		--Get existing columns in the table which aren't in the settings anymore
		DECLARE my_cursor CURSOR FAST_FORWARD READ_ONLY FOR
			SELECT 
				c.COLUMN_NAME,
				c.DATA_TYPE,
				isnull(c.CHARACTER_MAXIMUM_LENGTH,0) AS maxpanjang
			FROM (
				SELECT 
					PK_ApprovalDesignerField_ID,
					FK_ApprovalDesigner_ID,
					FieldName,
					FK_FieldType_ID,
					SizeField
				FROM  ApprovalDesignerField
				WHERE FK_ApprovalDesigner_ID=@ApprovalDesignerID 
				UNION ALL
				SELECT
					a.PK_ApprovalDesignerField_ID,
					@ApprovalDesignerID,
					REPLACE(a.FieldName,'[FieldName]',b.FieldName),
					a.FK_FieldType_ID,
					a.SizeField
				FROM ApprovalDesignerFieldDefaultFileUpload a
				CROSS JOIN dbo.ApprovalDesignerField b 
				WHERE 
					b.FK_ApprovalDesigner_ID = @ApprovalDesignerID
					AND b.FK_ExtType_ID = 8
					AND b.FK_FieldType_ID = 14
			) mf
			INNER JOIN MFieldType mt ON mt.PK_FieldType_ID = mf.FK_FieldType_ID
			INNER JOIN ApprovalDesigner x ON x.PK_ApprovalDesigner_ID = mf.FK_ApprovalDesigner_ID
			RIGHT JOIN INFORMATION_SCHEMA.[COLUMNS] c ON mf.FieldName = c.COLUMN_NAME AND c.TABLE_NAME = x.ApprovalDesignerName
			WHERE 
				c.TABLE_NAME = @ApprovalDesignerName
				AND mf.PK_ApprovalDesignerField_ID IS NULL
				AND c.COLUMN_NAME NOT IN (
					SELECT mfd.FieldName FROM ApprovalDesignerFieldDefault mfd	
				)

		--Drop excess field
		OPEN my_cursor
		FETCH FROM my_cursor INTO @FieldNamebuang,@FieldTypeSQLNamebuang,@SizeFieldbuang
	
		WHILE @@FETCH_STATUS = 0
		BEGIN
			SET @sql = ' Alter table ' + @ApprovalDesignerName + ' drop column ' + @FieldNamebuang
	 
			--PRINT @sql 
			EXEC(@sql)
			FETCH FROM my_cursor INTO @FieldNamebuang, @FieldTypeSQLNamebuang, @SizeFieldbuang
		END

		CLOSE my_cursor
		DEALLOCATE my_cursor
	
		DECLARE @FieldNameubah varchar(250), @FieldTypeSQLNameubah varchar(50),
				@SizeFieldubah INT, @tipedataubah VARCHAR(50)
	
		--Find existing columns that has its data type / size changed
		DECLARE my_cursor CURSOR FAST_FORWARD READ_ONLY FOR
			SELECT
				xx.FieldName,
				xx.FieldTypeSQLName,
				xx.SizeFieldcurrent,
				xx.DATA_TYPE   
			FROM (
				SELECT
					mf.FieldName,
					CASE 
						WHEN mf.FK_FieldType_ID <> 11 THEN mt.FieldTypeSQLName
						WHEN mf.FK_FieldType_ID = 11 AND mf.FK_ExtType_ID <> 15 THEN (
							 SELECT c.DATA_TYPE 
							 FROM   INFORMATION_SCHEMA.[COLUMNS] c
							 WHERE  c.TABLE_NAME = mf.TabelReferenceName
									AND c.COLUMN_NAME = mf.TableReferenceFieldKey
						)   
						WHEN mf.FK_FieldType_ID = 11 AND mf.FK_ExtType_ID = 15 THEN 'varchar'
					END AS FieldTypeSQLName,
					CASE 
						WHEN mf.FK_FieldType_ID = 11 AND mf.FK_ExtType_ID = 15 THEN -1
						WHEN mf.FK_FieldType_ID <> 11 THEN mf.SizeField
						WHEN mf.FK_FieldType_ID = 11 AND mf.FK_ExtType_ID <> 15 THEN (
							 SELECT                     
								CASE
									WHEN c.CHARACTER_MAXIMUM_LENGTH IS NULL THEN ''
									WHEN c.CHARACTER_MAXIMUM_LENGTH = -1 THEN 'max'
									ELSE CONVERT(VARCHAR(20), c.CHARACTER_MAXIMUM_LENGTH)
								END
							FROM INFORMATION_SCHEMA.[COLUMNS] c
							WHERE 
								c.TABLE_NAME = mf.TabelReferenceName
								AND c.COLUMN_NAME = mf.TableReferenceFieldKey
						) 
					END AS SizeFieldcurrent,
					CASE
						WHEN mf.FK_ExtType_ID <> 15 THEN c.DATA_TYPE 
						ELSE 'varchar' 
					END AS Data_type, 
					CASE 
						WHEN mf.FK_ExtType_ID <> 15 THEN ISNULL(c.CHARACTER_MAXIMUM_LENGTH,'')
						ELSE '-1' 
					END AS sizeFieldexisting
				FROM (
					SELECT
						PK_ApprovalDesignerField_ID,
						FK_ApprovalDesigner_ID,
						FieldName,
						FK_FieldType_ID,
						SizeField,
						TabelReferenceName,
						TableReferenceFieldKey,
						FK_ExtType_ID
					FROM ApprovalDesignerField
					WHERE FK_ApprovalDesigner_ID = @ApprovalDesignerID 
					UNION ALL
					SELECT
						a.PK_ApprovalDesignerField_ID,
						@ApprovalDesignerID,
						REPLACE(a.FieldName,'[FieldName]',b.FieldName),
						a.FK_FieldType_ID,
						a.SizeField,
						NULL,
						NULL,
						a.FK_ExtType_ID
					FROM ApprovalDesignerFieldDefaultFileUpload a
					CROSS JOIN dbo.ApprovalDesignerField b 
					WHERE 
						b.FK_ApprovalDesigner_ID = @ApprovalDesignerID
						AND b.FK_ExtType_ID = 8
						AND b.FK_FieldType_ID=14     
				) mf
				INNER JOIN MFieldType mt ON  mt.PK_FieldType_ID = mf.FK_FieldType_ID
				INNER JOIN INFORMATION_SCHEMA.[COLUMNS] c ON  (
					mf.FieldName = c.COLUMN_NAME
					AND mt.FieldTypeSQLName = c.DATA_TYPE
				) 
				WHERE
					mf.FK_ApprovalDesigner_ID = @ApprovalDesignerID
					AND c.TABLE_NAME = @ApprovalDesignerName
			) xx 
			WHERE 
				xx.FieldTypeSQLName <> xx.DATA_TYPE 
				OR xx.SizeFieldcurrent <> xx.sizeFieldexisting    

		--Checkpoint Ozan
		OPEN my_cursor
		FETCH FROM my_cursor INTO @FieldNameubah, @FieldTypeSQLNameubah, @SizeFieldubah, @tipedataubah

		WHILE @@FETCH_STATUS = 0
		BEGIN
			IF @SizeFieldubah=-1
			BEGIN
				IF @FieldTypeSQLNameubah = 'varchar' OR @FieldTypeSQLNameubah = 'varbinary' SET @FieldTypeSQLNameubah = @FieldTypeSQLNameubah + '(max)'
			END
			ELSE
			BEGIN
				IF @FieldTypeSQLNameubah = 'varchar' OR @FieldTypeSQLNameubah = 'varbinary' SET @FieldTypeSQLNameubah = @FieldTypeSQLNameubah + '(' + convert(VARCHAR(20), @SizeFieldubah) + ')'
			END

			SET @sql = ' Alter table ' + @ApprovalDesignerName + ' alter column ' + @FieldNameubah + ' ' + @FieldTypeSQLNameubah
	
			--PRINT @sql
			EXEC(@sql)

			FETCH FROM my_cursor INTO  @FieldNameubah,@FieldTypeSQLNameubah,@SizeFieldubah,@tipedataubah
		END

		CLOSE my_cursor
		DEALLOCATE my_cursor   
	END
END
GO
