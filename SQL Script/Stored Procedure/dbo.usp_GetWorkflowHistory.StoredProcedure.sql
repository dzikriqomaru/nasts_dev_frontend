/****** Object:  StoredProcedure [dbo].[usp_GetWorkflowHistory]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE    PROCEDURE [dbo].[usp_GetWorkflowHistory]
	@ModuleApprovalID BIGINT,
	@Search VARCHAR(MAX),
	@OrderBy VARCHAR(MAX),
	@PageIndex INTEGER,
	@PageSize INTEGER
AS

DECLARE @SQL VARCHAR(MAX)

IF @OrderBy='' SET @OrderBy= '1'

SET @SQL =		  'SELECT MWorkFlow_History.*, ApprovalStatusName' + CHAR(10) + CHAR(13)
SET @SQL = @SQL + 'FROM MWorkFlow_History' + CHAR(10) + CHAR(13)
SET @SQL = @SQL + '	 LEFT JOIN MWorkflow_ApprovalStatus' + CHAR(10) + CHAR(13)
SET @SQL = @SQL + '	 ON FK_MWorkflow_ApprovalStatus_ID = PK_MWorkflow_ApprovalStatus_ID' + CHAR(10) + CHAR(13)
SET @SQL = @SQL + 'WHERE FK_ModuleApproval_ID = ' + CAST(@ModuleApprovalID AS VARCHAR) + CHAR(10) + CHAR(13)
SET @SQL = @SQL + '		 AND (
							RoleName LIKE ''%' + @Search + '%''' + CHAR(10) + CHAR(13)
SET @SQL = @SQL + '			OR UserName LIKE ''%' + @Search + '%''' + CHAR(10) + CHAR(13)
SET @SQL = @SQL + '			OR UserNameExecute LIKE ''%' + @Search + '%''' + CHAR(10) + CHAR(13)
SET @SQL = @SQL + '			OR ApprovalStatusName LIKE ''%' + @Search + '%''' + CHAR(10) + CHAR(13)
SET @SQL = @SQL + '			OR Notes LIKE ''%' + @Search + '%''' + CHAR(10) + CHAR(13)
SET @SQL = @SQL + '		 )' + CHAR(10) + CHAR(13)
SET @SQL = @SQL + 'ORDER BY ' + @OrderBy + CHAR(10) + CHAR(13)
SET @SQL = @SQL + 'OFFSET ' + CAST((@PageIndex - 1) * @PageSize AS VARCHAR) + ' ROWS' + CHAR(10) + CHAR(13)
SET @SQL = @SQL + 'FETCH NEXT ' + CAST(@PageSize AS VARCHAR) + ' ROWS ONLY' + CHAR(10) + CHAR(13)

EXEC(@SQL)

GO
