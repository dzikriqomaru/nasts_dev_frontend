/****** Object:  StoredProcedure [dbo].[usp_NDS_CreateTableUploadPerUser]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_NDS_CreateTableUploadPerUser]
/***********************************************************
* Procedure description:
* Date:   10/3/2020 
* Author: nawadata
*
* Changes
* Date		Modified By			Comments
************************************************************
*
************************************************************/
(
	@userid VARCHAR(50),@modulename VARCHAR(500)
)
AS
BEGIN


 DECLARE @sql varchar(max)
SET @sql = 'DROP TABLE IF EXISTS '+@modulename+'_upload_data_'+dbo.StripTableName(@userid)+' ' + char(10)
         + 'SELECT * INTO  '+@modulename+'_upload_data_'+dbo.StripTableName(@userid)+' FROM '+@modulename+'_upload where 1=2 ' + char(10)
         + 'ALTER TABLE '+@modulename+'_upload_data_'+dbo.StripTableName(@userid)+' ADD PRIMARY KEY ( PK_upload_ID) ' + char(10)         
         + 'CREATE INDEX idx_'+@modulename+'_upload_data_'+dbo.StripTableName(@userid)+'_userid ON '+@modulename+'_upload_data_'+dbo.StripTableName(@userid)+'(nawa_userid) ' + char(10)
         + 'CREATE INDEX idx_'+@modulename+'_upload_data_'+dbo.StripTableName(@userid)+'_userid_action ON '+@modulename+'_upload_data_'+dbo.StripTableName(@userid)+'(nawa_userid,nawa_Action) ' + char(10)
         + ''
EXEC(@sql)
END
GO
