/****** Object:  StoredProcedure [dbo].[usp_CreateTableEmailPrimary]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[usp_CreateTableEmailPrimary]
/***********************************************************
* Procedure description:
* Date:   23/02/2016 
* Author: Hendra
*
* Changes
* Date		Modified By			Comments
************************************************************
*
************************************************************/
(
@tablename VARCHAR(500),@querydata VARCHAR(MAX)	
)
AS
BEGIN
	

DECLARE @sql varchar(max)
SET @sql = 'IF EXISTS( ' + char(10)
         + '	       SELECT * ' + char(10)
         + '	       FROM   INFORMATION_SCHEMA.TABLES t ' + char(10)
         + '	       WHERE  t.TABLE_NAME = ''__'+ @tablename +''' ' + char(10)
         + '	   ) ' + char(10)
         + '	    DROP TABLE __'+ @tablename +' ' + char(10)
         + '	 ' + char(10)
         + '	SELECT * INTO     __'+ @tablename +' ' + char(10)
         + '	FROM   ( ' + char(10)
         + '	'+ @querydata +'' + char(10)        
         + '	       )          xx'
EXEC(@sql)


	
	       
	       
END
GO
