/****** Object:  StoredProcedure [dbo].[usp_GetDataWorkflowHistorybypkmoduleid_new]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_GetDataWorkflowHistorybypkmoduleid_new] 
  /***********************************************************
  * Procedure description:
  * Date:   5/6/2019 
  * Author: nawadata
  *
  * Changes
  * Date		Modified By			Comments
  ************************************************************
  *
  ************************************************************/
  (
  	@pkmoduleapprovalid BIGINT,
	@OrderBy VARCHAR(MAX),
	@Page INT,
	@PageSize INT
  )
  AS
  BEGIN
 
 
DECLARE @sql VARCHAR(MAX)
SET @sql = '
SELECT *
FROM (
SELECT mfh.PK_MWorkflow_History_ID, mfh.FK_ModuleApproval_ID, mfh.FK_Module_ID,
       mfh.FK_Unik_ID, mfh.FK_MUserId, mfh.FK_MRoleId, mfh.intLevel, mfh.RoleName,
       mfh.UserName, mfh.UserNameExecute, mfh.ResponseDate,
       mfh.FK_MWorkflow_ApprovalStatus_ID, mfh.Notes, mas.ApprovalStatusName ,mfh.CreatedDate
  FROM MWorkFlow_History AS mfh 
left JOIN MWorkflow_ApprovalStatus AS mas ON mfh.FK_MWorkflow_ApprovalStatus_ID=mas.PK_MWorkflow_ApprovalStatus_ID
WHERE mfh.FK_ModuleApproval_ID =' + CAST(@pkmoduleapprovalid AS VARCHAR(MAX)) + '
) X
ORDER BY ' + @OrderBy + '
OFFSET ' + CAST((@Page - 1) * @PageSize AS VARCHAR(MAX)) + ' ROWS FETCH NEXT ' + CAST(@PageSize AS VARCHAR(MAX)) + ' ROWS ONLY
'
EXEC (@sql)
 
END
GO
