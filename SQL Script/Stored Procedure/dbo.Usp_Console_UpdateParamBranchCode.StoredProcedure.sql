/****** Object:  StoredProcedure [dbo].[Usp_Console_UpdateParamBranchCode]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Usp_Console_UpdateParamBranchCode]
/***********************************************************
* Procedure description:
* Date:   10/22/2019 
* Author: nawadata
*
* Changes
* Date		Modified By			Comments
************************************************************
*
************************************************************/
(
	@KodeCabang VARCHAR(50)
)
AS
BEGIN
	
UPDATE NawaDataETLConfigurations SET ConfiguredValue = @KodeCabang WHERE ConfigurationFilter= 'Kode_Cabang'
	
END
GO
