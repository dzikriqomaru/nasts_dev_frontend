/****** Object:  StoredProcedure [dbo].[IsHaveWorkflow]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[IsHaveWorkflow]
/***********************************************************
* Procedure description:
* Date:   5/11/2019 
* Author: nawadata
*
* Changes
* Date		Modified By			Comments
************************************************************
*
************************************************************/
(
	@pkmoduleid int
)
AS
BEGIN
	
SELECT   COUNT(1) AS jmlworkflow
FROM      Module_TR_WorkFlow INNER JOIN
                MWorkFlow ON Module_TR_WorkFlow.FK_WorkFlow_ID = MWorkFlow.PK_WorkFlow
WHERE FK_ModuleID=@pkmoduleid AND Module_TR_WorkFlow.[Active]=1
END
GO
