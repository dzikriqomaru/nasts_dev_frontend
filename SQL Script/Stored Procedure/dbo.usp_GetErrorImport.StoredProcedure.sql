/****** Object:  StoredProcedure [dbo].[usp_GetErrorImport]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[usp_GetErrorImport]  
/***********************************************************  
* Procedure description:  
* Date:   6/4/2012   
* Author: Hendra  
*  
* Changes  
* Date  Modified By   Comments  
************************************************************  
*  
************************************************************/  
(  
 @userid VARCHAR(50)  ,@Moduleid int
)  
AS  
BEGIN  
   
DECLARE @modulename VARCHAR(250)
   SELECT @modulename = m.ModuleName FROM Module m WHERE m.PK_Module_ID=@Moduleid
   
  
-- DECLARE @List varchar(max)  
--SELECT @List = COALESCE(@List + '', '') + u.KeteranganError   
--FROM sales_upload u  
--WHERE  u.KeteranganError <> '' AND u.nawa_userid=@userid  
--SELECT isnull(@List,'')

DECLARE @sql varchar(max)
SET @sql = ' DECLARE @List varchar(max)   ' + char(10)
         + 'SELECT @List = COALESCE(@List + '''', '''') + u.KeteranganError    ' + char(10)
         + 'FROM '+ @modulename	+'_upload u   ' + char(10)
         + 'WHERE  u.KeteranganError <> '''' AND u.nawa_userid='''+@userid +'''   ' + char(10)
         + 'SELECT isnull(@List,'''') as ErrorDesc ' + char(10)
         + ''
         PRINT @sql
EXEC(@sql)

  
END
GO
