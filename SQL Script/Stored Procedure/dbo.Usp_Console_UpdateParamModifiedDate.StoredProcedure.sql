/****** Object:  StoredProcedure [dbo].[Usp_Console_UpdateParamModifiedDate]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Usp_Console_UpdateParamModifiedDate]
/***********************************************************
* Procedure description:
* Date:   1/13/2016 
* Author: Hendra
*
* Changes
* Date        Modified By                Comments
************************************************************
*
************************************************************/
(
       @ProcessDate VARCHAR(50)
)
AS
BEGIN

UPDATE NawaDataETLConfigurations SET ConfiguredValue = @ProcessDate WHERE ConfigurationFilter= 'DataDate'

UPDATE NawaDataETLConfigurations SET ConfiguredValue = @ProcessDate WHERE ConfigurationFilter= 'Report_Date'

END


GO
