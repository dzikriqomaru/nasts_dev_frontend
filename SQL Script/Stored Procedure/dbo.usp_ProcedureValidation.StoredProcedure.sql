/****** Object:  StoredProcedure [dbo].[usp_ProcedureValidation]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ProcedureValidation]
	@Param1 VARCHAR(MAX)
AS
IF @Param1 LIKE '%Test%'
BEGIN
	RAISERROR ('Unable to save Test Data', -- Message text.  
               16, -- Severity.  
               1 -- State.  
               );  
END
GO
