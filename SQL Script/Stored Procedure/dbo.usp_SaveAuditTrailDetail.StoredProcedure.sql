/****** Object:  StoredProcedure [dbo].[usp_SaveAuditTrailDetail]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_SaveAuditTrailDetail]  
/***********************************************************  
* Procedure description:  
* Date:   7/6/2019   
* Author: nawadata  
*  
* Changes  
* Date  Modified By   Comments  
************************************************************  
*  
************************************************************/  
(  
 @AuditTrailDetailTableVar  AS dbo.AuditTrailDetailTableVar READONLY ,@pkmoduleid INT,@userid VARCHAR(50)  
)  
AS  
BEGIN  
   
 --declare @AuditTrailDetailTableVar  AS dbo.AuditTrailDetailTableVar READONLY  
   
 --declare @pkmoduleid INT,@userid VARCHAR(50)  
 --SET @pkmoduleid=9021  
 -- SET @userid='sysadmin'  
   
   
 DECLARE @modulename VARCHAR(500)  
 DECLARE @primarykeyfield VARCHAR(500)  
 SELECT @modulename=ModuleName FROM module WHERE PK_Module_ID=@pkmoduleid  
   
 SELECT @primarykeyfield=mf.FieldName FROM ModuleField AS mf WHERE mf.FK_Module_ID=@pkmoduleid AND mf.IsPrimaryKey=1  
   
   
   
   
   
 DECLARE @sqlbefore VARCHAR(MAX)=''  
 set @sqlbefore = 'DROP  TABLE IF EXISTS '+@modulename +'_AuditTrail_'+@userid +CHAR(13)  
 set @sqlbefore =@sqlbefore+' select '+@modulename +'.* into '+@modulename +'_AuditTrail_'+@userid+'  from '+@modulename +CHAR(13)  
 set @sqlbefore =@sqlbefore+' inner join '+@modulename+'_upload ON '+@modulename+'_Upload.'+@primarykeyfield+' = CONVERT(VARCHAR(500), '+@modulename+'.'+@primarykeyfield+')' +CHAR(13)   
 set @sqlbefore =@sqlbefore+ ' WHERE nawa_Action IN (''update'',''delete'')'  
 set @sqlbefore =@sqlbefore+ ' and  nawa_userid ='''+ @userid  +''''  
   
 EXEC (@sqlbefore)  
   
   
 --done   
 --DROP  TABLE IF EXISTS Ref_cabang_AuditTrail_sysadmin  
 --SELECT ref_Cabang.* INTO Ref_cabang_AuditTrail_sysadmin FROM Ref_Cabang   
 --INNER JOIN Ref_Cabang_Upload  ON Ref_Cabang_Upload.Sandi_Referensi = CONVERT(VARCHAR(500), Ref_Cabang.Sandi_Referensi)  
 --WHERE nawa_Action IN ('update','delete')  
   
   
 DECLARE @sqlcrossApply VARCHAR(MAX)=''  

 --set @sqlcrossApply = 'drop table if exists AuditTrail_CrossApply_'+@userid+' '
   set @sqlcrossApply =' delete from AuditTrail_CrossApply where FK_Module_ID=' + CONVERT(VARCHAR(50), @pkmoduleid) +' and Userid='''+@userid+'''' 
 exec(@sqlcrossApply)  
  DECLARE @sql varchar(max)
  SET @sql ='insert into AuditTrail_CrossApply(primarykeydata,FieldName,OldValue,FK_Module_ID,UserID)'  
	     + ' SELECT xx.'+@primarykeyfield +' AS Primarykeydata, ' + char(10)  
         + '        ca.* ,'+ CONVERT(VARCHAR(50), @pkmoduleid ) +','''+@userid+'''  ' + char(10)  
        
         + ' FROM   ( ' + char(10)  
         + '            SELECT rc.* ' + char(10)  
         + '            FROM   '+@modulename+' AS rc ' + char(10)  
         + '                   INNER JOIN '+@modulename+'_AuditTrail_'+@userid+' ' + char(10)  
         + '                        ON  rc.'+@primarykeyfield+' = '+@modulename+'_AuditTrail_'+@userid+'.'+@primarykeyfield+' ' + char(10)  
         + '        )xx ' + char(10)  
         + '        CROSS APPLY( ' + char(10)  
         + '     VALUES'  
  
   DECLARE @queryfield VARCHAR(MAX)=''  
   DECLARE @PK_ModuleField_ID bigint, @FK_Module_ID int, @FieldName varchar(250),  
        @FieldLabel varchar(250), @Sequence int, @Required bit,  
        @IsPrimaryKey bit, @IsUnik bit, @IsShowInView bit, @IsShowInForm bit,  
        @DefaultValue varchar(max), @FK_FieldType_ID int, @SizeField int,  
        @FK_ExtType_ID int, @TabelReferenceName varchar(250),  
        @TabelReferenceNameAlias varchar(250),  
        @TableReferenceFieldKey varchar(250),  
        @TableReferenceFieldDisplayName varchar(250),  
        @TableReferenceFilter varchar(550), @IsUseRegexValidation bit,  
        @TableReferenceAdditonalJoin varchar(1000), @BCasCade bit,  
        @FieldNameParent varchar(250), @FilterCascade varchar(1000)  
     
   DECLARE my_cursor CURSOR FAST_FORWARD READ_ONLY FOR  
   SELECT PK_ModuleField_ID, FK_Module_ID, FieldName, FieldLabel,  
          Sequence, [Required], IsPrimaryKey, IsUnik, IsShowInView,  
          IsShowInForm, DefaultValue, FK_FieldType_ID, SizeField,  
          FK_ExtType_ID, TabelReferenceName, TabelReferenceNameAlias,  
          TableReferenceFieldKey, TableReferenceFieldDisplayName,  
          TableReferenceFilter, IsUseRegexValidation,  
          TableReferenceAdditonalJoin, BCasCade, FieldNameParent,  
          FilterCascade  
   FROM ModuleField WHERE FK_Module_ID=@pkmoduleid  
     
   OPEN my_cursor  
     
   FETCH FROM my_cursor INTO @PK_ModuleField_ID, @FK_Module_ID,  
                             @FieldName, @FieldLabel, @Sequence,  
                             @Required, @IsPrimaryKey, @IsUnik,  
                             @IsShowInView, @IsShowInForm, @DefaultValue,  
                             @FK_FieldType_ID, @SizeField, @FK_ExtType_ID,  
                             @TabelReferenceName,  
                             @TabelReferenceNameAlias,  
                             @TableReferenceFieldKey,  
                             @TableReferenceFieldDisplayName,  
                             @TableReferenceFilter, @IsUseRegexValidation,  
                             @TableReferenceAdditonalJoin, @BCasCade,  
                             @FieldNameParent, @FilterCascade  
     
   WHILE @@FETCH_STATUS = 0  
   BEGIN  
    /*{ ... Cursor logic here ... }*/  
   IF @FK_FieldType_ID=9  
   BEGIN  
     set @queryfield=@queryfield + ' ('''+@FieldLabel+''', '+@FieldName+'),'  
   END  
     
      
   IF  (@FK_FieldType_ID  IN  ( 1,2,3,4,5,6,7,8,12,13))  
    SET @queryfield=@queryfield + ' ('''+@FieldLabel+''', TRY_CONVERT(VARCHAR(8000), '+@FieldName+')),'  
  if (@FK_FieldType_ID =10)  
   SET @queryfield=@queryfield + ' ('''+@FieldLabel+''', TRY_CONVERT(VARCHAR(8000), '+@FieldName+')),'  
    
  IF (@FK_FieldType_ID=11)  
     SET @queryfield=@queryfield + ' ('''+@FieldLabel+''', TRY_CONVERT(VARCHAR(8000), '+@FieldName+')),'  
    
     
    FETCH FROM my_cursor INTO @PK_ModuleField_ID, @FK_Module_ID,  
                              @FieldName, @FieldLabel, @Sequence,  
                              @Required, @IsPrimaryKey, @IsUnik,  
                              @IsShowInView, @IsShowInForm,  
                              @DefaultValue, @FK_FieldType_ID,  
                              @SizeField, @FK_ExtType_ID,  
                              @TabelReferenceName,  
                              @TabelReferenceNameAlias,  
                              @TableReferenceFieldKey,  
                              @TableReferenceFieldDisplayName,  
                              @TableReferenceFilter,  
                              @IsUseRegexValidation,  
                              @TableReferenceAdditonalJoin, @BCasCade,  
                              @FieldNameParent, @FilterCascade  
   END  
     
   CLOSE my_cursor  
   DEALLOCATE my_cursor  
     
  IF LEN(@queryfield)>0 SET @queryfield=SUBSTRING(@queryfield,1,LEN(@queryfield)-1)   
  
  SET @queryfield=@queryfield+' ) AS CA(FieldName, OldValue)'   
 set @sql=@sql + @queryfield   
 exec (@sql)
   
 
 
 
   
   
   
 INSERT INTO AuditTrailDetail  
 (  
  -- PK_AuditTrailDetail_id -- this column value is auto-generated  
  FK_AuditTrailHeader_ID,  
  FieldName,  
  OldValue,  
  NewValue  
 )SELECT   
 newtable.FK_AuditTrailHeader_ID,  
  newtable.FieldName,  
  oldtable.OldValue,  
  newtable.NewValue  
  FROM @AuditTrailDetailTableVar newtable  
  left JOIN AuditTrail_CrossApply oldtable  
  ON newtable.primarykeydata=  
  oldtable.primarykeydata  AND newtable.Fieldname =oldtable.FieldName  AND  oldtable.FK_Module_ID=@pkmoduleid AND oldtable.userid=@userid
  WHERE (nawa_Action='update' OR nawa_Action='Insert')  
  
    
    
  INSERT INTO AuditTrailDetail  
 (  
  -- PK_AuditTrailDetail_id -- this column value is auto-generated  
  FK_AuditTrailHeader_ID,  
  FieldName,  
  OldValue,  
  NewValue  
 )  
  select  
   newtable.FK_AuditTrailHeader_ID,  
  newtable.FieldName,  
  newtable.OldValue,  
  newtable.NewValue  
  FROM @AuditTrailDetailTableVar newtable  
  left JOIN AuditTrail_CrossApply oldtable  
  ON newtable.primarykeydata=  
  oldtable.primarykeydata  AND newtable.Fieldname =oldtable.FieldName  
  WHERE nawa_Action='delete'   
   AND oldtable.FK_Module_ID=@pkmoduleid AND oldtable.userid=@userid
   
  
   
DELETE FROM  AuditTrail_CrossApply  WHERE fk_module_id=@pkmoduleid AND userid=@userid
EXEC('DROP  TABLE IF EXISTS '+@modulename+'_AuditTrail_'+@userid)  
   
END
GO
