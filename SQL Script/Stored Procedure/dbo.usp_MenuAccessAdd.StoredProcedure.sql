/****** Object:  StoredProcedure [dbo].[usp_MenuAccessAdd]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_MenuAccessAdd]
/***********************************************************
* Procedure description:
* Date:   11/9/2019 
* Author: nawadata
*
* Changes
* Date			Modified By			Comments
************************************************************
* 31 Mar 2023	Humam				Add IconName
************************************************************/
(
 @groupmenuid INT,	@tblMGroupMenuAccess AS tblMGroupMenuAccess READONLY,@tblMGroupMenuSettting as tblMGroupMenuSettting READONLY,@createdby VARCHAR(50),@Approveby VARCHAR(50),@FK_AuditTrailStatus_ID INT,@FK_ModuleAction_ID INT,@ModuleLabel VARCHAR(50),@PK_ModuleApproval_ID bigint
)
AS
BEGIN
	
	INSERT INTO MGroupMenuAccess
	(
		-- PK_MGroupMenuAcess_ID -- this column value is auto-generated
		FK_GroupMenu_ID,
		FK_Module_ID,
		bAdd,
		bEdit,
		bDelete,
		bActivation,
		bView,
		bApproval,
		bUpload,
		bDetail
	)
	SELECT 
	   FK_GroupMenu_ID,
		FK_Module_ID,
		bAdd,
		bEdit,
		bDelete,
		bActivation,
		bView,
		bApproval,
		bUpload,
		bDetail
	FROM 
	@tblMGroupMenuAccess
	
	INSERT INTO MGroupMenuSettting
	(
		-- PK_MGroupMenuSettting_ID -- this column value is auto-generated
		FK_MGroupMenu_ID,
		mMenuID,
		mMenuLabel,
		mMenuParentID,
		mMenuURL,
		FK_Module_ID,
		FK_Action_ID,
		IconName,
		urutan
	)
	SELECT 
		FK_MGroupMenu_ID,
		mMenuID,
		mMenuLabel,
		mMenuParentID,
		mMenuURL,
		FK_Module_ID,
		FK_Action_ID,
		IconName,
		urutan
	FROM  @tblMGroupMenuSettting
	
	DECLARE @pkAudittrailheader bigint
	INSERT INTO AuditTrailHeader
	(
		-- PK_AuditTrail_ID -- this column value is auto-generated
		CreatedDate,
		CreatedBy,
		ApproveBy,
		ModuleLabel,
		FK_ModuleAction_ID,
		FK_AuditTrailStatus_ID,
		PK_ModuleApproval_ID
	)
	VALUES
	(
		GETDATE(),
		@createdby,
		@Approveby,
		@ModuleLabel,
		@FK_ModuleAction_ID,
		@FK_AuditTrailStatus_ID,
		@PK_ModuleApproval_ID
	)
	
	SET @pkAudittrailheader=SCOPE_IDENTITY()
	
	INSERT INTO AuditTrailDetail
	(
		-- PK_AuditTrailDetail_id -- this column value is auto-generated
		FK_AuditTrailHeader_ID,
		FieldName,
		OldValue,
		NewValue
	)
	SELECT @pkAudittrailheader FK_AuditTrailHeader_ID,ca.fieldname,'' OldValue ,ca.NewValue 
	FROM (
		SELECT * FROM MGroupMenuAccess AS mma WHERE mma.FK_GroupMenu_ID = @groupmenuid
	)xx 
	CROSS APPLY (
		VALUES 
		('PK_MGroupMenuAcess_ID', try_convert(varchar(8000),PK_MGroupMenuAcess_ID)),
		('FK_GroupMenu_ID', try_convert(varchar(8000),FK_GroupMenu_ID)),
		('FK_Module_ID', try_convert(varchar(8000),FK_Module_ID)),
		('bAdd', try_convert(varchar(8000),bAdd)),
		('bEdit', try_convert(varchar(8000),bEdit)),
		('bDelete', try_convert(varchar(8000),bDelete)),
		('bActivation', try_convert(varchar(8000),bActivation)),
		('bView', try_convert(varchar(8000),bView)),
		('bApproval', try_convert(varchar(8000),bApproval)),
		('bUpload', try_convert(varchar(8000),bUpload)),
		('bDetail', try_convert(varchar(8000),bDetail)) 	
	) ca (Fieldname,NewValue)
	UNION all
	SELECT @pkAudittrailheader FK_AuditTrailHeader_ID,ca.fieldname,'' OldValue ,ca.NewValue 
	FROM (
		SELECT * FROM MGroupMenuSettting  AS mma WHERE mma.FK_MGroupMenu_ID=@groupmenuid
	)xx 
	CROSS APPLY (
		VALUES 
		('PK_MGroupMenuSettting_ID', try_convert(varchar(8000),PK_MGroupMenuSettting_ID)),
		('FK_MGroupMenu_ID', try_convert(varchar(8000),FK_MGroupMenu_ID)),
		('mMenuID', try_convert(varchar(8000),mMenuID)),
		('mMenuLabel', try_convert(varchar(8000),mMenuLabel)),
		('mMenuParentID', try_convert(varchar(8000),mMenuParentID)),
		('mMenuURL', try_convert(varchar(8000),mMenuURL)),
		('FK_Module_ID', try_convert(varchar(8000),FK_Module_ID)),
		('FK_Action_ID', try_convert(varchar(8000),FK_Action_ID)),
		('IconName', try_convert(varchar(8000),IconName)),
		('urutan', try_convert(varchar(8000),urutan))
	) ca (Fieldname,NewValue)

	SELECT @pkAudittrailheader AS PK_AuditTrail_ID
END
GO
