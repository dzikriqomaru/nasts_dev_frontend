/****** Object:  StoredProcedure [dbo].[Usp_Batch_AdvanceDataFixing]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Usp_Batch_AdvanceDataFixing]
(
	@PK_EODTaskDetailLog_ID BIGINT,
	@DataDate DATETIME,
	@TransformCategoryId INT
)
AS
BEGIN

/*
Available Keyword:
@KodeCabang
@ReportDate
@UserID
*/

DECLARE @PK_ORS_AdvancedDataFixing BIGINT,
	@FK_CategoryTransform BIGINT,
	@SqlStatement VARCHAR(MAX),
	@ExecutionOrder INT,
	@Description VARCHAR(MAX)
	
	DECLARE @UpdateCursor AS CURSOR;

	SET @UpdateCursor = CURSOR FOR
	SELECT        PK_ORS_AdvancedDataFixing, FK_CategoryTransform, SqlStatement, ExecutionOrder, Description
	FROM            ORS_AdvancedDataFixing AS ad
	WHERE ad.FK_CategoryTransform = @TransformCategoryId
	ORDER BY ad.ExecutionOrder

	OPEN @UpdateCursor;
	FETCH NEXT FROM @UpdateCursor INTO @PK_ORS_AdvancedDataFixing, @FK_CategoryTransform, @SqlStatement,
		@ExecutionOrder, @Description
 
	WHILE @@FETCH_STATUS = 0
	BEGIN
		--Insert Code Here
		SELECT @SqlStatement

		FETCH NEXT FROM @UpdateCursor INTO @PK_ORS_AdvancedDataFixing, @FK_CategoryTransform, @SqlStatement,
		@ExecutionOrder, @Description
	END


	CLOSE @UpdateCursor;
	DEALLOCATE @UpdateCursor;

END
GO
