/****** Object:  StoredProcedure [dbo].[Usp_RejectModuleUpload]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Usp_RejectModuleUpload]
	@pkapprovalid BIGINT,
	@userid VARCHAR(50)
	 -- WITH ENCRYPTION, RECOMPILE, EXECUTE AS CALLER|SELF|OWNER| 'user_name'
AS
BEGIN

--	DECLARE @pkapprovalid BIGINT=36391,
--	@userid VARCHAR(50)='supervisor1'
	DECLARE @PK_ModuleApproval_ID     BIGINT,
	        @ModuleNameApproval               VARCHAR(250),
	        @ModuleKey                VARCHAR(1000),
	        @ModuleField              VARCHAR(MAX),
	        @ModuleFieldBefore        VARCHAR(MAX),
	        @PK_ModuleAction_ID       INT, 
	        @CreatedDate              DATETIME,
	        @CreatedBy                VARCHAR(50)
	
	SELECT @PK_ModuleApproval_ID = PK_ModuleApproval_ID,
	       @ModuleNameApproval              = ModuleName,
	       @ModuleKey               = ModuleKey,
	       @ModuleField             = ModuleField,
	       @ModuleFieldBefore       = ModuleFieldBefore,
	       @PK_ModuleAction_ID      = PK_ModuleAction_ID,
	       @CreatedDate             = CreatedDate,
	       @CreatedBy               = CreatedBy
	FROM   ModuleApproval
	WHERE  PK_ModuleApproval_ID     = @pkapprovalid
	
	
	
	
	DECLARE @pkmoduleid              INT,
	        @modulelabelapproval     VARCHAR(500)
	
	SELECT @pkmoduleid = PK_Module_ID,
	       @modulelabelapproval     = ModuleLabel
	FROM   dbo.Module
	WHERE  ModuleName               = @ModuleNameApproval
	

	
	DECLARE @pkaudittrailheader BIGINT = 0
	INSERT dbo.AuditTrailHeader
	  (
	    CreatedDate,
	    CreatedBy,
	    ApproveBy,
	    ModuleLabel,
	    FK_ModuleAction_ID,
	    FK_AuditTrailStatus_ID,
	    PK_ModuleApproval_ID
	  )
	VALUES
	  (
	    GETDATE(),	-- CreatedDate - datetime
	    @CreatedBy,	-- CreatedBy - varchar(50)
	    @userid,	-- ApproveBy - varchar(50)
	    @modulelabelapproval,	-- ModuleLabel - varchar(100)
	    7,	-- FK_ModuleAction_ID - int
	    2,	-- FK_AuditTrailStatus_ID - int
	    @pkapprovalid -- PK_ModuleApproval_ID - bigint
	  )
	
	SET @pkaudittrailheader = SCOPE_IDENTITY()
	
	
	--================================================================================================
	-- Comment Coding audittrail beforenya
	--================================================================================================
	
	
	DECLARE @modulename VARCHAR(500)  
	DECLARE @primarykeyfield VARCHAR(500)  
	SELECT @modulename = ModuleName
	FROM   module
	WHERE  PK_Module_ID = @pkmoduleid  
	
	SELECT @primarykeyfield = mf.FieldName
	FROM   ModuleField AS mf
	WHERE  mf.FK_Module_ID = @pkmoduleid
	       AND mf.IsPrimaryKey = 1 
	
	
	--================================================================================================
	-- Comment Coding table cobabigdata_upload akan terisi ketika sp usp_Getdataupload. jadi tinggal pake
	--================================================================================================
	
	DECLARE @sqlfirst VARCHAR(MAX)
	SET @sqlfirst = ' DROP TABLE IF EXISTS  '+ @ModuleName + '_upload_' + @userid +' ' + CHAR(10)
	    + ' SELECT nawa_Action,'+@primarykeyfield+'  ' + CHAR(10)
	    + ' INTO ' + @ModuleName + '_upload_' + @userid + ' ' + CHAR(10)
	    + ' FROM dbo.' + @ModuleName + '_Upload  WHERE nawa_userid=''' + @Createdby + ''' AND KeteranganError='''''
	
	PRINT @sqlfirst
	EXEC (@sqlfirst)
	
	
	
	
	DECLARE @sqlbefore VARCHAR(MAX) = ''  
	SET @sqlbefore = 'DROP  TABLE IF EXISTS ' + @modulename + '_AuditTrail_' + @userid + CHAR(13)  
	SET @sqlbefore = @sqlbefore + ' select ' + @modulename + '.* into ' + @modulename + '_AuditTrail_' + @userid +
	    '  from ' + @modulename + CHAR(13)
	
	SET @sqlbefore = @sqlbefore + ' inner join ' + @modulename + '_upload_' + @userid + ' ON ' + @modulename +
	    '_Upload_' + @userid + '.' + @primarykeyfield + ' = CONVERT(VARCHAR(8000), ' + @modulename + '.' +
	    @primarykeyfield + ')' + CHAR(13)
	
	SET @sqlbefore = @sqlbefore + ' WHERE nawa_Action IN (''update'',''delete'')' 
	
	PRINT (@sqlbefore)
	EXEC (@sqlbefore) 
	
	
	--================================================================================================
	-- Comment Coding :simpan data uudittrailbefore
	--================================================================================================
	
	DECLARE @sqlcrossApply VARCHAR(MAX) = '' 
	
	--set @sqlcrossApply = 'drop table if exists AuditTrail_CrossApply_'+@userid+' '  
	SET @sqlcrossApply = ' delete from AuditTrail_CrossApply where FK_Module_ID=' + CONVERT(VARCHAR(50), @pkmoduleid) + 
	    ' and Userid=''' + @userid + ''''
	
	PRINT @sqlcrossApply
	EXEC (@sqlcrossApply)
	    
	DECLARE @sqlcreatecrossapply VARCHAR(MAX)  
	SET @sqlcreatecrossapply = 
	    'insert into AuditTrail_CrossApply(primarykeydata,FieldName,OldValue,FK_Module_ID,UserID)' 
	    + ' SELECT xx.' + @primarykeyfield + ' AS Primarykeydata, ' + CHAR(10) 
	    + '        ca.* ,' + CONVERT(VARCHAR(50), @pkmoduleid) + ',''' + @userid + '''  ' + CHAR(10) 
	    
	    + ' FROM   ( ' + CHAR(10) 
	    + '            SELECT rc.* ' + CHAR(10) 
	    + '            FROM   ' + @modulename + ' AS rc ' + CHAR(10) 
	    + '                   INNER JOIN ' + @modulename + '_AuditTrail_' + @userid + ' ' + CHAR(10) 
	    + ' ON  rc.' + @primarykeyfield + ' = ' + @modulename + '_AuditTrail_' + @userid + '.' + @primarykeyfield + ' ' 
	    + CHAR(10) 
	    + '        )xx ' + CHAR(10) 
	    + '        CROSS APPLY( ' + CHAR(10) 
	    + '     VALUES' 
	
	
	--================================================================================================
	-- Comment Coding:generate statement insert ,insertdetail
	--================================================================================================	
	
	
	DECLARE @fields VARCHAR(MAX) = ''
	DECLARE @fieldinsert VARCHAR(MAX) = ''
	DECLARE @fieldinsertdetail VARCHAR(MAX) = ''
	
	
	
	
	SET @fields = '  myTempTable.XmlCol.query(''./nawa_userid'').value(''.'', ''varchar(8000)'') AS nawa_userid , ' + 
	    CHAR(10)
	    + '  myTempTable.XmlCol.query(''./nawa_recordnumber'').value(''.'', ''varchar(8000)'') AS nawa_recordnumber , ' 
	    + CHAR(10)
	    + '  myTempTable.XmlCol.query(''./KeteranganError'').value(''.'', ''varchar(8000)'') AS KeteranganError , ' + 
	    CHAR(10)
	    + '  myTempTable.XmlCol.query(''./nawa_Action'').value(''.'', ''varchar(8000)'') AS nawa_Action ,' + CHAR(10)
	    + '  myTempTable.XmlCol.query(''./Active'').value(''.'', ''varchar(8000)'') AS Active ,' + CHAR(10)
	
	SET @fieldinsert = 'nawa_userid,' + CHAR(10) 
	    + 'nawa_recordnumber,' + CHAR(10)
	    + 'KeteranganError,' + CHAR(10)
	    + 'nawa_Action,' + CHAR(10)
	    + 'Active,' + CHAR(10) 
	
	--set @fieldinsertdetail='nawa_userid,'+CHAR(10)
	--               +'nawa_recordnumber,'+CHAR(10)
	--               +'KeteranganError,'+CHAR(10)
	--               +'nawa_Action,'+CHAR(10)
	--               +'Active,'+CHAR(10)  	
	
	
	DECLARE @dateformat VARCHAR(50)
	SELECT @dateformat = sp.SettingValue
	FROM   dbo.SystemParameter sp
	WHERE  sp.PK_SystemParameter_ID = 6
	
	
	DECLARE @queryfieldinsertupdate VARCHAR(MAX) = ''
	DECLARE @queryfield VARCHAR(MAX) = ''
	DECLARE @PK_ModuleField_ID          BIGINT,
	        @FK_Module_ID               INT,
	        @FieldName                  VARCHAR(250),
	        @FieldLabel                 VARCHAR(250),
	        @Sequence                   INT,
	        @Required                   BIT,
	        @IsPrimaryKey               BIT,
	        @IsUnik                     BIT,
	        @IsShowInView               BIT,
	        @FK_FieldType_ID            INT,
	        @SizeField                  INT,
	        @FK_ExtType_ID              INT,
	        @TabelReferenceName         VARCHAR(250),
	        @TableReferenceFieldKey     VARCHAR(250),
	        @TableReferenceFieldDisplayName VARCHAR(250),
	        @TableReferenceFilter       VARCHAR(550),
	        @IsUseRegexValidation       BIT
	
	DECLARE my_cursor CURSOR FAST_FORWARD READ_ONLY 
	FOR
	    SELECT mf.PK_ModuleField_ID,
	           mf.FK_Module_ID,
	           mf.FieldName,
	           mf.FieldLabel,
	           mf.Sequence,
	           mf.Required,
	           mf.IsPrimaryKey,
	           mf.IsUnik,
	           mf.IsShowInView,
	           mf.FK_FieldType_ID,
	           mf.SizeField,
	           mf.FK_ExtType_ID,
	           mf.TabelReferenceName,
	           mf.TableReferenceFieldKey,
	           mf.TableReferenceFieldDisplayName,
	           mf.TableReferenceFilter,
	           mf.IsUseRegexValidation
	    FROM   dbo.ModuleField mf
	    WHERE  mf.FK_Module_ID = @pkmoduleid
	           AND mf.FK_FieldType_ID <> 14
	    ORDER BY
	           mf.Sequence
	
	OPEN my_cursor
	
	FETCH FROM my_cursor INTO @PK_ModuleField_ID, @FK_Module_ID, @FieldName,
	@FieldLabel, @Sequence, @Required, @IsPrimaryKey,
	@IsUnik, @IsShowInView, @FK_FieldType_ID,
	@SizeField, @FK_ExtType_ID, @TabelReferenceName,
	@TableReferenceFieldKey,
	@TableReferenceFieldDisplayName,
	@TableReferenceFilter, @IsUseRegexValidation
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
	    /*{ ... Cursor logic here ... }*/
	    
	    SET @fields = @fields + 'myTempTable.XmlCol.query(''./' + @FieldName + ''').value(''.'', ''varchar(8000)'') AS '
	        + @FieldName + ' ,' + CHAR(10)
	    
	    SET @fieldinsert = @fieldinsert + @FieldName + ',' + CHAR(10)
	    
	    
	    /*{ ... Cursor logic here ... }*/    
	    IF @FK_FieldType_ID = 9
	    BEGIN
	        SET @queryfieldinsertupdate = @queryfieldinsertupdate + ' (''' + @FieldLabel + ''', ' + @FieldName + '),'
	    END    
	    
	    
	    IF (@FK_FieldType_ID IN (1, 2, 3, 4, 5, 6, 7, 8, 12, 13))
	        SET @queryfieldinsertupdate = @queryfieldinsertupdate + ' (''' + @FieldLabel + 
	            ''', TRY_CONVERT(VARCHAR(8000), ' + @FieldName + ')),'
	    
	    IF (@FK_FieldType_ID = 10)
	        SET @queryfieldinsertupdate = @queryfieldinsertupdate + ' (''' + @FieldLabel + 
	            ''', TRY_CONVERT(VARCHAR(8000), ' + @FieldName + ')),'    
	    
	    IF (@FK_FieldType_ID = 11)
	    BEGIN
	    	 SET @queryfieldinsertupdate = @queryfieldinsertupdate + ' (''' + @FieldLabel + 
	            ''', TRY_CONVERT(VARCHAR(8000), ' + @FieldName + ')),'    
	            
	        --SET @queryfieldinsertupdate = @queryfieldinsertupdate + ' (''' + @FieldLabel + 
	        --    ''', try_convert(varchar(8000), SUBSTRING(' + @FieldName + ', PATINDEX(''%(%'', ' + @FieldName + 
	        --    ')+1, CASE WHEN PATINDEX(''%)%'',' + @FieldName + ')=0 THEN LEN(' + @FieldName + 
	        --    ') else PATINDEX(''%)%'',' + @FieldName + ')-2 END ))),' + CHAR(10)
	    END
	    
	    
	    --IF @FK_FieldType_ID<>10
	    --BEGIN
	    --	SET @fieldinsertdetail=@fieldinsertdetail+ @FieldName +','+CHAR(10)
	    --END
	    --ELSE
	    --	BEGIN
	    --SET @fieldinsertdetail=@fieldinsertdetail+ 'FORMAT( CONVERT(DATETIME,' + @FieldName +') ,'''+ @dateformat +''') '+','+CHAR(10)
	    --	END
	    
	    
	    
	    FETCH FROM my_cursor INTO @PK_ModuleField_ID, @FK_Module_ID, @FieldName,
	    @FieldLabel, @Sequence, @Required,
	    @IsPrimaryKey, @IsUnik, @IsShowInView,
	    @FK_FieldType_ID, @SizeField, @FK_ExtType_ID,
	    @TabelReferenceName, @TableReferenceFieldKey,
	    @TableReferenceFieldDisplayName,
	    @TableReferenceFilter, @IsUseRegexValidation
	END
	
	CLOSE my_cursor
	DEALLOCATE my_cursor
	
	
	IF LEN(@fields) > 0
	    SET @fields = SUBSTRING(@fields, 1, LEN(@fields) -2)
	
	IF LEN(@fieldinsert) > 0
	    SET @fieldinsert = SUBSTRING(@fieldinsert, 1, LEN(@fieldinsert) -2)
	
	IF LEN(@fieldinsertdetail) > 0
	    SET @fieldinsertdetail = SUBSTRING(@fieldinsertdetail, 1, LEN(@fieldinsertdetail) -2)
	
	IF LEN(@queryfieldinsertupdate) > 0
	    SET @queryfieldinsertupdate = SUBSTRING(
	            @queryfieldinsertupdate,
	            1,
	            LEN(@queryfieldinsertupdate) -1
	        )     
	
	SET @queryfield = @queryfieldinsertupdate + ' ) AS CA(FieldName, OldValue)'     
	SET @sqlcreatecrossapply = @sqlcreatecrossapply + @queryfield 
	PRINT @sqlcreatecrossapply
	EXEC (@sqlcreatecrossapply)  
	
	
	
	DECLARE @deleteaudittrailupload VARCHAR(MAX) = 'delete from AuditTrailDetailUpload where FK_Module_ID=' + CONVERT(VARCHAR(50), @pkmoduleid)
	        + ' and UserID=''' + @userid + ''''
	PRINT @deleteaudittrailupload
	EXEC (@deleteaudittrailupload) 
	
	--================================================================================================
	-- Comment Coding : getdata want to save and save audittraildetailupload
	--================================================================================================
	
	DECLARE @sqlupdatedataexisting VARCHAR(MAX) = ''
	SET @sqlupdatedataexisting = 'INSERT into dbo.AuditTrailDetailUpload ' + CHAR(10)
	    + '( ' + CHAR(10)
	    + '    FK_AuditTrailHeader_ID, ' + CHAR(10)
	    + '    FieldName, ' + CHAR(10)
	    + '    OldValue, ' + CHAR(10)
	    + '    NewValue, ' + CHAR(10)
	    + '    PrimarykeyData, ' + CHAR(10)
	    + '    nawa_action, ' + CHAR(10)
	    + '    FK_Module_ID, ' + CHAR(10)
	    + '    UserID ' + CHAR(10)
	    + ') ' + CHAR(10)
	    + ' ' + CHAR(10)
	    + 'SELECT ' + CONVERT(VARCHAR(50), @pkaudittrailheader) + 
	    ', Fieldname,'''' AS OldValue, NewValue ,xx.'+@primarykeyfield+' AS primarykeydata,nawa_Action,' + CONVERT(VARCHAR(50), @pkmoduleid)
	    + ',''' + @userid + ''' FROM ( ' + CHAR(10)
	    + 'SELECT * FROM ' + @modulename + '_Upload AS cbdu WHERE cbdu.nawa_userid=''' + @CreatedBy + ''' ' + CHAR(10)
	    + ')xx ' + CHAR(10)
	    + 'CROSS APPLY( ' + CHAR(10)
	    + 'values'
	
	
	
	DECLARE @queryfieldaudittraildelete AS VARCHAR(MAX) = @queryfieldinsertupdate
	
	SET @queryfieldinsertupdate = @queryfieldinsertupdate + 
	    ' ) AS CA(FieldName, NewValue) WHERE xx.nawa_Action=''update'' OR xx.nawa_Action=''insert'' OR xx.nawa_Action='''''
	
	SET @queryfieldaudittraildelete = @queryfieldaudittraildelete + 
	    ' ) AS CA(FieldName, OldValue) WHERE xx.nawa_Action=''delete'' '
	
	SET @sqlupdatedataexisting = @sqlupdatedataexisting + @queryfieldinsertupdate 
	
	--PRINT @sqlupdatedataexisting
	EXEC (@sqlupdatedataexisting)
	
	
	
	--================================================================================================
	-- Comment Coding:insert audittrail delete
	--================================================================================================
	
	
	DECLARE @sqlaudittraildetailuploaddelete VARCHAR(MAX) = ''
	SET @sqlaudittraildetailuploaddelete = ' ' + CHAR(10)
	    + 'INSERT into dbo.AuditTrailDetailUpload ' + CHAR(10)
	    + '( ' + CHAR(10)
	    + '    FK_AuditTrailHeader_ID, ' + CHAR(10)
	    + '    FieldName, ' + CHAR(10)
	    + '    OldValue, ' + CHAR(10)
	    + '    NewValue, ' + CHAR(10)
	    + '    PrimarykeyData, ' + CHAR(10)
	    + '    nawa_action, ' + CHAR(10)
	    + '    FK_Module_ID, ' + CHAR(10)
	    + '    UserID ' + CHAR(10)
	    + ') ' + CHAR(10)
	    + ' ' + CHAR(10)
	    
	    + 'SELECT ' + CONVERT(VARCHAR(50), @pkaudittrailheader) + 
	    ', Fieldname,OldValue, '''' NewValue ,xx.'+@primarykeyfield+' AS primarykeydata,nawa_Action,' + CONVERT(VARCHAR(50), @pkmoduleid)
	    + ',''' + @userid + ''' FROM ( ' + CHAR(10) 
	    + 'SELECT * FROM ' + @modulename + '_Upload AS cbdu WHERE cbdu.nawa_userid=''' + @CreatedBy + ''' ' + CHAR(10)
	    + ')xx ' + CHAR(10)
	    + 'CROSS APPLY( ' + CHAR(10)
	    + 'values	'
	
	
	SET @sqlaudittraildetailuploaddelete = @sqlaudittraildetailuploaddelete + @queryfieldaudittraildelete
	
	PRINT @sqlaudittraildetailuploaddelete
	EXEC (@sqlaudittraildetailuploaddelete)
	
	
	
	--================================================================================================
	-- Comment Coding:insert audittraildetail untuk mode update dan delete
	--================================================================================================
	
	
	INSERT INTO AuditTrailDetail
	  (
	    -- PK_AuditTrailDetail_id -- this column value is auto-generated    
	    FK_AuditTrailHeader_ID,
	    FieldName,
	    OldValue,
	    NewValue
	  )
	SELECT newtable.FK_AuditTrailHeader_ID,
	       newtable.FieldName,
	       oldtable.OldValue,
	       newtable.NewValue
	FROM   AuditTrailDetailUpload newtable
	       INNER JOIN AuditTrail_CrossApply oldtable
	            ON  newtable.primarykeydata = oldtable.primarykeydata
	            AND newtable.Fieldname = oldtable.FieldName
	            AND oldtable.FK_Module_ID = newtable.FK_Module_ID
	            AND oldtable.userid = newtable.UserID
	WHERE  (nawa_Action = 'update' OR nawa_Action = 'Insert')
	       AND newtable.FK_Module_ID = @pkmoduleid
	       AND newtable.userid = @userid 
	
	
	
	--================================================================================================
	-- Comment Coding insert audittraildetail untuk mode delete
	--================================================================================================     
	INSERT INTO AuditTrailDetail
	  (
	    -- PK_AuditTrailDetail_id -- this column value is auto-generated    
	    FK_AuditTrailHeader_ID,
	    FieldName,
	    OldValue,
	    NewValue
	  )
	SELECT newtable.FK_AuditTrailHeader_ID,
	       newtable.FieldName,
	       newtable.OldValue,
	       newtable.NewValue
	FROM   AuditTrailDetailUpload newtable
	       INNER JOIN AuditTrail_CrossApply oldtable
	            ON  newtable.primarykeydata = oldtable.primarykeydata
	            AND newtable.Fieldname = oldtable.FieldName
	            AND newtable.FK_Module_ID = oldtable.FK_Module_ID
	            AND oldtable.UserID = newtable.UserID
	WHERE  nawa_Action = 'delete'
	       AND oldtable.FK_Module_ID = @pkmoduleid
	       AND oldtable.userid = @userid  
	
	
	
	DELETE 
	FROM   AuditTrail_CrossApply
	WHERE  fk_module_id = @pkmoduleid
	       AND userid = @userid
	
	EXEC (
	         'DROP  TABLE IF EXISTS ' + @modulename + '_AuditTrail_' + @userid
	     )    
	
	
	EXEC (
	         'DROP TABLE IF EXISTS ' + @modulename + '_upload_' + @userid + ''
	     )
	
	
	DECLARE @sqldataUploaddrop VARCHAR(8000) = ''     
	SET @sqldataUploaddrop = 'DELETE FROM audittraildetailupload WHERE FK_Module_ID=' + CONVERT(VARCHAR(50), @pkmoduleid) 
	    + '  AND UserID=''' + @userid + ''''
	
	EXEC (@sqldataUploaddrop)

	DECLARE @sqldeleteapproval VARCHAR(MAX)='DELETE FROM dbo.ModuleApproval WHERE PK_ModuleApproval_ID='+ CONVERT(VARCHAR(50), @pkapprovalid)
	EXEC (@sqldeleteapproval )


	DECLARE @sqlclear1 VARCHAR(MAX)

SET @sqlclear1='DROP TABLE IF EXISTS '+ @modulename +'_ApprovalDetail_'+ @userid +''
EXEC (@sqlclear1)		






END






GO
