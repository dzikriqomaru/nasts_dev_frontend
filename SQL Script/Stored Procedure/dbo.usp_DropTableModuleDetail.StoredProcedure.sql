/****** Object:  StoredProcedure [dbo].[usp_DropTableModuleDetail]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_DropTableModuleDetail]
/***********************************************************
* Procedure description:
* Date:   15/09/2022
* Author: Fauzan
*
* Changes
* Date		Modified By			Comments
************************************************************
*
************************************************************/
(@PK_ModuleDetail_ID INT)
AS
BEGIN
	DECLARE @ModuleDetailName varchar(250)

	SELECT @ModuleDetailName = ModuleDetailName
	FROM dbo.ModuleDetail
	WHERE PK_ModuleDetail_ID = @PK_ModuleDetail_ID

	IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES t WHERE t.TABLE_NAME = @ModuleDetailName)
	EXEC ('DROP TABLE ' + @ModuleDetailName)

	DECLARE @ModuleDetailNameUpload VARCHAR(300)
	SET @ModuleDetailNameUpload = @ModuleDetailName + '_Upload'
	IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES t WHERE t.TABLE_NAME = @ModuleDetailNameUpload)
	EXEC ('DROP TABLE ' + @ModuleDetailNameUpload)

	DECLARE @ModuleDetailNameUploadApproval VARCHAR(350)
	SET @ModuleDetailNameUpload = @ModuleDetailName + '_Upload_Approval'
	IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES t WHERE t.TABLE_NAME = @ModuleDetailNameUploadApproval)
	EXEC ('DROP TABLE ' + @ModuleDetailNameUploadApproval)
END
GO
