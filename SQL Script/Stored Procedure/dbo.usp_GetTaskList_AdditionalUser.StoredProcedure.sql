/****** Object:  StoredProcedure [dbo].[usp_GetTaskList_AdditionalUser]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_GetTaskList_AdditionalUser]        
/***********************************************************            
* Procedure description            
* Date   10/7/2015            
* Author Hendra            
*            
* Changes            
* Date  Modified By   Comments            
************************************************************            
*            
************************************************************/            
(            
    @userid          VARCHAR(50),            
    @groupmenuid     INT,            
 @roleid    INT,       
    @actionid        INT,            
    @orderby         VARCHAR(4000),            
    @whereclause     VARCHAR(4000),            
    @PageIndex       INT,            
    @PageSize        INT            
)            
AS            
BEGIN               
 DECLARE @pkuserid INT            
             
 SELECT @pkuserid =m.PK_MUser_ID                
 FROM   MUser m                
 WHERE  m.UserID = @userid                
             
             
             
     DECLARE @sql varchar(max)      
  DECLARE @sql2 VARCHAR(MAX)      
          
SET @sql = ' ' + char(10)        
  + 'DECLARE @UrlView VARCHAR(MAX), @PK_Module_ID BIGINT        
SELECT @UrlView = m.UrlView, @PK_Module_ID = m.PK_Module_ID        
FROM Module AS m WHERE LOWER(m.ModuleName) = ''validationreport'';' + char(10)        
         + 'WITH PageIndex AS ( SELECT xx.jumlah as jumlah,ab.ModuleLabel AS ModuleName,  yy.mMenuURL,yy.FK_Module_ID as ModuleID, xx.FK_MWorkflow_Status_ID FROM (   ' + char(10)        
         + '    ' + char(10)        
         + '  SELECT COUNT(1)                       AS jumlah,  ' + char(10)        
         + '         ma.ModuleName                     NamaModule,  ' + char(10)  
		 + '		 mfp.FK_MWorkflow_Status_ID		'	+ CHAR(10)      
         + '  FROM   ModuleApproval                 AS ma  ' + char(10)        
         + '         INNER JOIN MUser m  ' + char(10)        
         + '              ON  m.UserID = ma.CreatedBy  ' + char(10)        
         + '         INNER JOIN Module              AS m2  ' + char(10)        
         + '              ON  m2.ModuleName = ma.ModuleName  ' + char(10)        
         + '         INNER JOIN MWorkFlow_Progress  AS mfp  ' + char(10)        
         + '              ON  mfp.FK_Module_ID = m2.PK_Module_ID  ' + char(10)        
         + '              AND (mfp.FK_Unik_ID = ma.ModuleKey  OR mfp.FK_Unik_ID=convert(varchar(50),ma.PK_ModuleApproval_ID)+''import'' )   ' + char(10)        
         + '  WHERE  mfp.FK_MWorkflow_Status_ID IN (3)  ' + char(10)        
         + '         AND (  ' + char(10)        
         + '                 (  ' + char(10)        
         + '                     mfp.FK_MWorkFlowUserType_ID = 4  ' + char(10)        
         + '                     AND mfp.FK_MWorkflowRole_ID =   '+ CONVERT(VARCHAR(50), @roleid )+ char(10)        
         + '                 )  ' + char(10)        
         + '                 OR (  ' + char(10)        
         + '                        (  ' + char(10)        
         + '                            mfp.FK_MWorkFlowUserType_ID = 1  ' + char(10)        
         + '                            OR mfp.FK_MWorkFlowUserType_ID = 2  ' + char(10)        
         + '                        )  ' + char(10)        
         + '                        AND mfp.FK_MWorkflowUser_ID =   '+ CONVERT(VARCHAR(50), @pkuserid) + char(10)        
         + '                    )  ' + char(10)        
         + '             )  ' + char(10)        
         + '         AND ma.CreatedBy <> '''+@userid+'''  ' + char(10)        
         + '  GROUP BY  ' + char(10)        
         + '         ma.ModuleName, mfp.FK_MWorkflow_Status_ID  ' + char(10)        
         + '   ' + char(10)        
         + '          ' + char(10)        
         + '  UNION  ' + char(10)        
         + '   ALL        ' + char(10)        
         + '    ' + char(10)        
         + '    ' + char(10)        
         + '  SELECT dataapprovalbyrole.jumlah,  ' + char(10)        
         + '         dataapprovalbyrole.NamaModule,  ' + char(10)      
		 + '         NULL AS FK_MWorkflow_Status_ID  ' + char(10)      
         + '  FROM   (  ' + char(10)        
         + '             SELECT COUNT(1)           AS jumlah,  ' + char(10)        
         + '           ma.ModuleName         NamaModule,  ' + char(10)        
         + '                    (  ' + char(10)        
         + '                        SELECT COUNT(1)  ' + char(10)        
         + '                        FROM   Module_TR_WorkFlow  ' + char(10)        
         + '                        WHERE  FK_ModuleID = m2.PK_Module_ID  ' + char(10)        
         + '                               AND [Active] = 1  ' + char(10)        
         + '                    )                  AS jmlworkflow  ' + char(10)        
         + '             FROM   ModuleApproval ma  ' + char(10)        
         + '       INNER JOIN vw_AdditionalUser m  ' + char(10)        
         + '                         ON  m.UserID = ma.CreatedBy  ' + char(10)        
   + '       AND m.PK_MRole_ID = ma.FK_MRole_ID ' + CHAR(10)      
         + '                    INNER JOIN Module  AS m2  ' + char(10)        
         + '                         ON  m2.ModuleName = ma.ModuleName  ' + char(10)        
         + '             WHERE  ma.CreatedBy <> '''+@userid+''' AND m.PK_MRole_ID =  '+CONVERT(VARCHAR(50),@roleid) + char(10)        
         + '    AND m.PK_MGroupMenu_ID = ' + CONVERT(VARCHAR(50),@groupmenuid) + CHAR(10)        
   + '             GROUP BY  ' + char(10)        
         + '                    ma.ModuleName,  ' + char(10)        
         + '                    m2.PK_Module_ID  ' + char(10)        
         + '         )dataapprovalbyrole  ' + char(10)        
         + '  WHERE  dataapprovalbyrole.jmlworkflow = 0  ' + char(10)        
         + '   ' + char(10)        
         + '           ' + char(10)        
         + '    ' + char(10)        
         + ')xx   ' + char(10)        
         + 'INNER JOIN (   ' + char(10)        
         + '   ' + char(10)        
         + 'SELECT distinct m.Modulename as namamodule,mms.mMenuURL,mms.FK_Module_ID   ' + char(10)        
         + '  FROM MGroupMenuSettting mms   ' + char(10)        
         + 'INNER JOIN Module m ON mms.FK_Module_ID=m.PK_Module_ID   ' + char(10)        
         + 'WHERE mms.FK_MGroupMenu_ID='+ CONVERT(VARCHAR(50),@groupmenuid) +' AND mms.FK_Action_ID= 6 ' + char(10)        
         + ')yy ON xx.NamaModule=yy.namamodule   ' + char(10)        
         + ' INNER JOIN Module ab ON ab.ModuleName=xx.NamaModule   ' + char(10)        
         + '   ' + char(10)        
         + 'UNION ALL ' + char(10)        
         + ' ' + char(10)        
         + ' ' + char(10)        
         + '    ' + char(10)        
         + '  SELECT COUNT(1)                       AS jumlah,  ' + char(10)        
         + '         m2.ModuleLabel                     NamaModule ,''/Parameter/ParameterCorrection.aspx''AS mmenuurl,m2.PK_Module_ID, mfp.FK_MWorkflow_Status_ID ' + char(10)        
         + '  FROM   ModuleApproval                 AS ma  ' + char(10)        
         + '         INNER JOIN MUser m  ' + char(10)        
         + '              ON  m.UserID = ma.CreatedBy  ' + char(10)        
         + '         INNER JOIN Module              AS m2  ' + char(10)        
         + '              ON  m2.ModuleName = ma.ModuleName  ' + char(10)        
         + '         INNER JOIN MWorkFlow_Progress  AS mfp  ' + char(10)        
         + '              ON  mfp.FK_Module_ID = m2.PK_Module_ID  ' + char(10)        
         + '              AND mfp.FK_Unik_ID = ma.ModuleKey  ' + char(10)        
         + '  WHERE  mfp.FK_MWorkflow_Status_ID IN ( 6)  ' + char(10)        
         + '         AND (  ' + char(10)        
         + '                 (  ' + char(10)        
         + '                     mfp.FK_MWorkFlowUserType_ID = 4  ' + char(10)        
         + '                     AND mfp.FK_MWorkflowRole_ID =   '+ CONVERT(VARCHAR(50),@roleid) + char(10)        
         + '                 )  ' + char(10)        
         + '                 OR (  ' + char(10)                 + '                        (  ' + char(10)        
         + '                            mfp.FK_MWorkFlowUserType_ID = 1  ' + char(10)        
         + '                            OR mfp.FK_MWorkFlowUserType_ID = 2  ' + char(10)        
         + '                        )  ' + char(10)        
         + '                        AND mfp.FK_MWorkflowUser_ID =   '+CONVERT(VARCHAR(50),@pkuserid) + char(10)        
         + '                    )  ' + char(10)        
         + '             )  ' + char(10)        
         + '          ' + char(10)        
         + '  GROUP BY  ' + char(10)        
         + '        m2.ModuleLabel ,m2.PK_Module_ID, mfp.FK_MWorkflow_Status_ID       ' + char(10)        
         + '            ' + char(10)        
         + ' UNION ALL  ' + char(10)        
         + '   ' + char(10)        
         + 'SELECT COUNT(*)           AS jumlah,  ' + char(10)        
         + 'wvrrc.SegmentData + '' Validation'' SegmentData,  ' + char(10)        
         + '@UrlView +''?Segmen='' + REPLACE(wvrrc.SegmentData,'' '',''_'') mMenuURL,  ' + char(10)        
         + '@PK_Module_ID  AS ModuleID,  ' + char(10)
		 + 'NULL AS FK_MWorkflow_Status_ID  ' + char(10)        
         + 'FROM Web_ValidationReport_RowComplate AS wvrrc  WITH(NOLOCK)' + char(10)        
         + 'INNER JOIN SettingPersonal sp  ' + char(10)        
         + 'ON wvrrc.TanggalData = sp.ReportDate  ' + char(10)        
         + 'AND (CASE WHEN sp.KodeCabang <> ''All'' THEN wvrrc.KodeCabang ELSE sp.KodeCabang END) = sp.KodeCabang ' + char(10)          
         + 'AND sp.UserID = '''+@userid+'''  ' + char(10)        
         + 'GROUP BY wvrrc.SegmentData ' + char(10)        
         + ' '        
 + 'UNION ALL'        
     + CHAR(10) + 'SELECT COUNT(1) AS jumlah,'        
     + CHAR(10) + ''' Neraca Validation'' SegmentData,'        
     + CHAR(10) + 'm.UrlView mMenuURL,'        
     + CHAR(10) + 'm.PK_Module_ID  AS ModuleID,'
	 + CHAR(10) + 'NULL FK_MWorkflow_Status_ID'      
     + CHAR(10) + 'FROM InterFormValidationReport vr'        
     + CHAR(10) + 'INNER JOIN SettingPersonal sp'        
     + CHAR(10) + 'ON vr.Report_Date = sp.ReportDate'        
     + CHAR(10) + 'AND (CASE WHEN vr.Kode_Cabang_BI <> ''All'' THEN vr.Kode_Cabang_BI ELSE sp.KodeCabang END) = sp.KodeCabang '          
     + CHAR(10) + 'AND sp.UserID = ''' + @userid + ''''        
     + CHAR(10) + 'LEFT JOIN Module AS m ON LOWER(m.ModuleName) = ''InterFormValidationReport'''         
     + CHAR(10) + 'GROUP BY m.UrlView, PK_Module_ID '        
        
 IF EXISTS (      
  SELECT 1 FROM sys.tables WHERE name = 'NDSSettingApproval'      
 )      
  BEGIN      
   SET @sql = + @sql + ') '       
      + 'SELECT * '      
      + 'FROM (SELECT a.jumlah, a.ModuleName, a.mMenuURL, a.ModuleID' + CHAR(10)
	  + '		FROM pageindex a LEFT JOIN NDSSettingApproval b ON a.ModuleID = b.FK_Module_ID AND b.Active = 1 ' + CHAR(10)
	  +	'		WHERE b.PK_NDSSettingApproval_ID IS NULL OR a.FK_MWorkflow_Status_ID = 6) pageindex'      
   IF LEN(@whereclause) > 0            
    SET @sql = @sql + ' where ' + @whereclause      
   SELECT @sql2 =       
    STUFF (      
       (      
        SELECT      
        ' UNION ALL '  + CHAR(10) +      
        'SELECT COUNT(1) AS jumlah, y.namamodule ModuleName, y.mMenuURL, y.FK_Module_ID AS ModuleID ' + CHAR(10) +       
        'FROM ( '   + CHAR(10) +      
        '  SELECT ModuleLabel ' + CHAR(10) +      
        '  FROM '  +      
        LOWER(sa.QueryApproval) + CHAR(10) +      
        '  WHERE '  +      
        REPLACE( 
			REPLACE(
				REPLACE(      
					REPLACE(      
						REPLACE(      
							LOWER(sa.FilterApproval), '@userid', '''' + @userid + ''''      
						), '@modulename', '''' + m.ModuleName + ''''      
					), '@roleid', '''' + CONVERT(VARCHAR(50),@roleid)  + ''''   
				), '@groupid', '''' + CONVERT(VARCHAR(50),@groupmenuid) + ''''   
			), '<>', '!='      
        ) + CHAR(10) +      
        ')x'    + CHAR(10) +      
        'INNER JOIN ('  + CHAR(10) +      
        ' SELECT distinct m.Modulename as namamodule, m.ModuleLabel, mms.mMenuURL, mms.FK_Module_ID ' + CHAR(10) +      
        ' FROM MGroupMenuSettting mms ' + CHAR(10) +      
        ' INNER JOIN Module m ' + CHAR(10) +      
        '  ON mms.FK_Module_ID=m.PK_Module_ID ' + CHAR(10) +      
        ' WHERE mms.FK_MGroupMenu_ID = ' + CONVERT(VARCHAR(50),@groupmenuid) + CHAR(10) +       
        '  AND mms.FK_Action_ID = 6 ' + CHAR(10) +      
        ')y'    + CHAR(10) +      
        ' ON x.ModuleLabel = y.ModuleLabel ' + CHAR(10) +      
        'GROUP BY y.namamodule, y.mMenuURL, y.FK_Module_ID' + CHAR(10)      
        FROM module m      
        INNER JOIN NDSSettingApproval sa      
         ON m.PK_Module_ID = sa.FK_Module_ID    
   AND sa.Active = 1    
        FOR XML PATH('')      
       ), 1, 12, ''      
      )    
       
   IF @sql2 != ''  
   BEGIN  
  SET @sql2 = 'SELECT * FROM (' + @sql2 + ')pageindex'    
  SET @sql = @sql + ' UNION ALL ' + @sql2      
   END  
      
  END      
 ELSE      
  SET @sql = @sql + ') select * From pageindex'      
        
             
 IF LEN(@whereclause) > 0            
  SET @sql = @sql + ' where ' + @whereclause            
 SET @sql = @sql + ' ORDER BY  ' + @orderby + CHAR(10)            
        
            
 EXEC (@sql)          
 PRINT @sql   
           
END 
GO
