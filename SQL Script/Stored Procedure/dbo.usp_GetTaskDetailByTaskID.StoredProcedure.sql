/****** Object:  StoredProcedure [dbo].[usp_GetTaskDetailByTaskID]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_GetTaskDetailByTaskID]
/***********************************************************
* Procedure description:
* Date:   12/29/2015 
* Author: Hendra
*
* Changes
* Date		Modified By			Comments
************************************************************
*
************************************************************/
(
	@EODTASKID bigint
)
AS
BEGIN
	
SELECT ed.PK_EODTaskDetail_ID ,edt.EODTaskDetailType,ed.OrderNo, ed.SSISName,
       ed.StoreProcedureName, ed.Keterangan, ed.IsUseParameterProcessDate, ed.IsUseParameterBranch
  FROM EODTaskDetail ed 
INNER JOIN EODTaskDetailType edt ON ed.FK_EODTaskDetailType_ID=edt.PK_EODTaskDetailType_ID
WHERE ed.FK_EODTask_ID=@EODTASKID

END

GO
