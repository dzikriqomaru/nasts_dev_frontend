/****** Object:  StoredProcedure [dbo].[usp_GetSchedulerDetailBySchedulerID]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[usp_GetSchedulerDetailBySchedulerID]
/***********************************************************
* Procedure description:
* Date:   12/29/2015 
* Author: Hendra
*
* Changes
* Date		Modified By			Comments
************************************************************
*
************************************************************/
(
	@EODSchedulerID bigint
)
AS
BEGIN


SELECT e.PK_EODScheduler_ID, e.EODSchedulerName, e.EODSchedulerDescription,
       e.EODPeriod, e.StartDate, e.[Active],e2.EODTaskName,ed.OrderNo
  FROM EODScheduler e	
  INNER JOIN EODSchedulerDetail ed ON e.PK_EODScheduler_ID=ed.FK_EODSCheduler_ID
  INNER JOIN EODTask e2 ON ed.FK_EODTask_ID=e2.PK_EODTask_ID
WHERE ed.FK_EODSCheduler_ID=@EODSchedulerID  
ORDER BY ed.OrderNo


	
END
GO
