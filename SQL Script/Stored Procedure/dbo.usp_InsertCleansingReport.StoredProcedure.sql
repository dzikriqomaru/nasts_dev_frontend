/****** Object:  StoredProcedure [dbo].[usp_InsertCleansingReport]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[usp_InsertCleansingReport] 
(
	@DataDate DATETIME, @KodeCabang VARCHAR(10)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;	
	
	DECLARE @SQL             VARCHAR(8000),
	        @TableID         VARCHAR(255),
	        @TableName       VARCHAR(255),
	        @FieldName       VARCHAR(255),
	        @TableLabel      VARCHAR(255),
	        @FieldLabel      VARCHAR(255),
	        @Unique          VARCHAR(255),
	        @UniqueField     VARCHAR(255),
			@DictionaryID	BIGINT,
			@TextToReplace VARCHAR(255),
			@ReplaceWith VARCHAR(255),
			@SortOrder INT,
			@NeedConfirmation INT

	
	SET @SQL = 'DELETE CleansingReport'
		--+ CHAR(10) + 'WHERE Report_Date = ''' + CONVERT(VARCHAR, @DataDate, 112) + ''' AND Kode_Cabang_BI = ''' + @KodeCabang + ''''
	EXEC (@SQL)
	
	DECLARE my_cursor CURSOR FAST_FORWARD READ_ONLY 
	FOR
	    SELECT DISTINCT Module.ModuleName,
	           cdd.FieldName,
	           Module.ModuleLabel,
	           cdd.CleansingName,
	           Module.PK_Module_ID,
	           mfk.FieldName KeyValue,
	           mfk.FieldName KeyField,
			   cdd.PK_CleansingDataDictionary_ID, 
			   cdd.TextToReplace, 
			   cdd.ReplaceWith, 
			   cdd.SortOrder,
			   cdd.NeedConfirmation
	    FROM   CleansingDataDictionary cdd
	           LEFT JOIN Module
	                ON  PK_Module_ID = cdd.TableName
	           LEFT JOIN ModuleField AS mfk
	                ON  mfk.FK_Module_ID = cdd.TableName
						AND mfk.IsPrimaryKey = 1
	
	OPEN my_cursor
	
	PRINT 'START'
	
	FETCH FROM my_cursor INTO @TableName, @FieldName, @TableLabel, @FieldLabel, @TableID, @Unique, @UniqueField,
		@DictionaryID, @TextToReplace, @ReplaceWith, @SortOrder, @NeedConfirmation
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
	    PRINT @TableName + @FieldName + @TableLabel + @FieldLabel + @TableID + @Unique
	    SET @SQL = 'INSERT INTO CleansingReport'
	        + CHAR(10) + '('
	        --+ CHAR(10) + 'Report_Date, Kode_Cabang_BI,'
	        + CHAR(10) + '	SegmentData, CleansingRules, KeyField, KeyFieldValue, NamaField, OriginalValue, CleanedValue, [Status],'
	        + CHAR(10) + '	[Active], CreatedBy, LastUpdateBy, ApprovedBy, CreatedDate, LastUpdateDate, ApprovedDate,'
	        + CHAR(10) + '	UniqueField, UniqueValue, ModuleID, ModuleName,'
	        + CHAR(10) + '	Clean, [Keep], FieldName'
	        + CHAR(10) + ')'
	        + CHAR(10) + 'SELECT ''' + @TableLabel + ''', ''' +  @FieldLabel + ''', ''' +  @Unique + 
			''', ' + 'f.' + @Unique + ', ''' + @FieldName 
	        + ''', ' + 'f.' + @FieldName + ', ''' + REPLACE(@ReplaceWith, '''', '''''') 
			+ CASE WHEN @NeedConfirmation = 1 THEN ''', NULL,' ELSE ''', ''CLEANED'',' END 
	        + CHAR(10) + '''1'', ''System'', '
			+ CHAR(10) + CASE WHEN @NeedConfirmation = 1 THEN 'NULL' ELSE '''System''' END + ', NULL, GETDATE(), '
			+ CHAR(10) + CASE WHEN @NeedConfirmation = 1 THEN 'NULL' ELSE 'GETDATE()' END + ', NULL, '
	        + CHAR(10) + '''' + @UniqueField + ''', ' + REPLACE(@Unique,'cln.',@TableName+'.') + ', ' + @TableID + ', ''' + @TableName 
	        + ''','
	        + CHAR(10) + CASE WHEN @NeedConfirmation = 1 THEN '0' ELSE '1' END + ', 0, '''+@FieldName+''''
	        + CHAR(10) + 'FROM ' + @TableName + ' AS f'
	        + CHAR(10) + 'WHERE f.' + @FieldName + ' = ''' + REPLACE(@TextToReplace, '''', '''''') + ''''
	        --+ CHAR(10) + 'AND f.Report_Date = ''' + CONVERT(VARCHAR, @DataDate, 112) + ''' AND f.Kode_Cabang_BI = ''' + @KodeCabang + ''''
	    
	    PRINT(@SQL)
	    EXEC(@SQL)
	    
		IF @NeedConfirmation = 0
		BEGIN
			DECLARE @SQLQuery AS VARCHAR(MAX)
			SET @SQLQuery = 'UPDATE ' + @TableName 
				+ CHAR(10) + 'SET ' + @FieldName + ' = ''' + REPLACE(@ReplaceWith, '''', '''''') + ''''
				+ CHAR(10) + 'WHERE ' + @FieldName + ' = ''' + REPLACE(@TextToReplace, '''', '''''') + ''''
		        --+ CHAR(10) + 'AND Report_Date = ''' + CONVERT(VARCHAR, @DataDate, 112) + ''' AND Kode_Cabang_BI = ''' + @KodeCabang + ''''

			PRINT(@SQLQuery)
			EXEC(@SQLQuery)
		END

	    FETCH FROM my_cursor INTO @TableName, @FieldName, @TableLabel, @FieldLabel, @TableID, @Unique, @UniqueField,
			@DictionaryID, @TextToReplace, @ReplaceWith, @SortOrder, @NeedConfirmation
	END
	
	CLOSE my_cursor
	DEALLOCATE my_cursor
END
GO
