/****** Object:  StoredProcedure [dbo].[usp_GetGroupAccessByGroupMenu]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_GetGroupAccessByGroupMenu]
/***********************************************************
* Procedure description:
* Date:   11/12/2015 
* Author: Hendra
*
* Changes
* Date		Modified By			Comments
************************************************************
*
************************************************************/
(@pkgroupmenuid INT)
AS
BEGIN
	
SELECT xxx.PK_Module_ID,
       xxx.PK_MGroupMenu_ID,
       xxx.GroupMenuName,
       xxx.ModuleLabel,
       CASE WHEN isnull(m.IsSupportAdd,0)=1 THEN  CASE WHEN  isnull(c.bAdd,0) ='true' THEN 'Y' ELSE 'N' END ELSE '-' END  BAdd,
       CASE WHEN isnull(m.IsSupportEdit,0)=1 THEN  CASE WHEN  isnull(c.bEdit,0) ='true' THEN 'Y' ELSE 'N' END ELSE '-' END  BEdit,
       CASE WHEN isnull(m.IsSupportDelete,0)=1 THEN  CASE WHEN  isnull(c.bDelete,0) ='true' THEN 'Y' ELSE 'N' END ELSE '-' END  BDelete,
       CASE WHEN isnull(m.IsSupportActivation,0)=1 THEN  CASE WHEN  isnull(c.bActivation,0) ='true' THEN 'Y' ELSE 'N' END ELSE '-' END  BActivation,
       CASE WHEN isnull(m.IsSupportView,0)=1 THEN  CASE WHEN  isnull(c.bView,0) ='true' THEN 'Y' ELSE 'N' END ELSE '-' END  BView,
       CASE WHEN isnull(m.IsUseApproval,0)=1 THEN  CASE WHEN  isnull(c.bApproval,0) ='true' THEN 'Y' ELSE 'N' END ELSE '-' END  BApproval,
       CASE WHEN isnull(m.IsSupportUpload,0)=1 THEN  CASE WHEN  isnull(c.bUpload,0) ='true' THEN 'Y' ELSE 'N' END ELSE '-' END  BUpload,
       CASE WHEN isnull(m.IsSupportDetail,0)=1 THEN  CASE WHEN  isnull(c.bDetail,0) ='true' THEN 'Y' ELSE 'N' END ELSE '-' END  BDetail,

       m.IsSupportAdd, m.IsSupportEdit, m.IsSupportDelete, m.IsSupportActivation,
       m.IsSupportView, m.IsSupportUpload, m.IsSupportDetail,m.IsUseApproval
       
       
       
       
FROM   (
           SELECT m.PK_Module_ID,
                  b.PK_MGroupMenu_ID,
                  b.GroupMenuName,
                  m.ModuleLabel
           FROM   Module m
                  CROSS JOIN MGroupMenu b
       )xxx
       LEFT JOIN MGroupMenuAccess c
            ON  xxx.PK_MGroupMenu_ID = c.FK_GroupMenu_ID
            AND xxx.PK_Module_ID = c.FK_Module_ID
       LEFT JOIN Module m
            ON  m.PK_Module_ID = xxx.PK_Module_ID
WHERE  xxx.PK_MGroupMenu_ID = @pkgroupmenuid
ORDER BY
       xxx.PK_MGroupMenu_ID,
       xxx.ModuleLabel
       
    
	       
END
GO
