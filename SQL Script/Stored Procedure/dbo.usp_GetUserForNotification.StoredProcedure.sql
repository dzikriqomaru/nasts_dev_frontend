/****** Object:  StoredProcedure [dbo].[usp_GetUserForNotification]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_GetUserForNotification]

@ModuleID BIGINT,
@UserID BIGINT,
@RoleID BIGINT,
@ActionType VARCHAR(MAX)

AS

DECLARE @CurrentLevel AS INT = 0
DECLARE @HasWorkflow AS INT = 0
DECLARE @HasUserStructure AS INT = 0
DECLARE @PreviousUserUpline AS INT = 0
DECLARE @ActionToLevel AS INT = 0


SELECT @HasWorkflow = COUNT(1)
	FROM Module WITH(NOLOCK)
	JOIN Module_TR_WorkFlow WITH(NOLOCK)
	ON Module.PK_Module_ID = Module_TR_WorkFlow.FK_ModuleID
	WHERE Module_TR_WorkFlow.FK_ModuleID = @ModuleID

SELECT @CurrentLevel = MWorkFlowDetail.Level
	FROM Muser WITH(NOLOCK)
	JOIN MWorkFlowDetail WITH(NOLOCK)
	ON MUser.FK_MRole_ID = MWorkFlowDetail.RoleID
	WHERE MUser.PK_MUser_ID = @UserID

IF (@HasWorkflow = 0)
	BEGIN
		SELECT MUser.*  
		FROM MUser WITH(NOLOCK)
		LEFT JOIN MGroupMenuAccess WITH(NOLOCK)
		ON MGroupMenuAccess.FK_GroupMenu_ID = MUser.FK_MGroupMenu_ID
		WHERE MUser.FK_MRole_ID = @RoleID
		AND MUser.PK_MUser_ID != @UserID
		AND MUser.PK_MUser_ID != 1
		AND MGroupMenuAccess.bApproval = 1
		AND MGroupMenuAccess.FK_Module_ID = @ModuleID
		UNION
		SELECT MUser.* 
		FROM MUser WITH(NOLOCK)
		LEFT JOIN NDSAdditionalSettingUser WITH(NOLOCK)
		ON MUser.UserID = NDSAdditionalSettingUser.UserID
		LEFT JOIN MGroupMenuAccess WITH(NOLOCK)
		ON MGroupMenuAccess.FK_GroupMenu_ID = NDSAdditionalSettingUser.FK_MGroupMenu_ID
		WHERE NDSAdditionalSettingUser.FK_MRole_ID = @RoleID
		AND MUser.PK_MUser_ID != @UserID
		AND MUser.PK_MUser_ID != 1
		AND MGroupMenuAccess.bApproval = 1
		AND MGroupMenuAccess.FK_Module_ID = @ModuleID
	END
ELSE
	BEGIN
		IF (@ActionType = 'revise')
			BEGIN
                SET @ActionToLevel = (
                    SELECT MWorkFlowDetail.RevisedActionTo
                    FROM Muser WITH(NOLOCK)
                    JOIN MWorkFlowDetail WITH(NOLOCK)
                    ON MUser.FK_MRole_ID = MWorkFlowDetail.RoleID
                    JOIN Module_TR_WorkFlow WITH(NOLOCK)
                    ON Module_TR_WorkFlow.FK_WorkFlow_ID = MWorkFlowDetail.FK_WorkFlow
                    WHERE MUser.PK_MUser_ID = @UserID
                    AND Module_TR_WorkFlow.FK_ModuleID = @ModuleID
				)

				SELECT @PreviousUserUpline = COUNT(1)
				FROM MWorkFlowDetail WITH(NOLOCK)
				JOIN Module_TR_WorkFlow WITH(NOLOCK)
				ON MWorkFlowDetail.FK_WorkFlow = Module_TR_WorkFlow.FK_WorkFlow_ID
				WHERE MWorkFlowDetail.Level = @ActionToLevel
				AND Module_TR_WorkFlow.FK_ModuleID = @ModuleID
				AND MWorkFlowDetail.UserType = 2

				IF (@PreviousUserUpline = 0)
					BEGIN
						SELECT MUser.*
						FROM Module_TR_WorkFlow WITH(NOLOCK)
						JOIN MWorkFlowDetail WITH(NOLOCK)
						ON Module_TR_WorkFlow.FK_WorkFlow_ID = MWorkFlowDetail.FK_WorkFlow
						JOIN MRole WITH(NOLOCK)
						ON MRole.PK_MRole_ID = MWorkFlowDetail.RoleID
						JOIN MUser WITH(NOLOCK)
						ON MUser.FK_MRole_ID = MRole.PK_MRole_ID
						WHERE Module_TR_WorkFlow.FK_ModuleID = @ModuleID
						AND MWorkFlowDetail.Level = @ActionToLevel
					END
				ELSE
				BEGIN
					SELECT MUser.* FROM MWorkFlowDetail WITH(NOLOCK)
					JOIN Module_TR_WorkFlow WITH(NOLOCK)
					ON MWorkFlowDetail.FK_WorkFlow = Module_TR_WorkFlow.FK_WorkFlow_ID
					JOIN MUser WITH(NOLOCK)
					ON MUser.FK_MRole_ID = MWorkFlowDetail.RoleID
					JOIN MUserStructure WITH(NOLOCK)
					ON MUserStructure.FK_Parent_ID = MUser.PK_MUser_ID
					WHERE Module_TR_WorkFlow.FK_ModuleID = @ModuleID
					AND MWorkFlowDetail.Level = @ActionToLevel
				END
			END
		ELSE
			BEGIN
				SELECT @PreviousUserUpline = COUNT(1)
				FROM MWorkFlowDetail WITH(NOLOCK)
				JOIN Module_TR_WorkFlow WITH(NOLOCK)
				ON MWorkFlowDetail.FK_WorkFlow = Module_TR_WorkFlow.FK_WorkFlow_ID
				WHERE MWorkFlowDetail.Level = @CurrentLevel + 1
				AND Module_TR_WorkFlow.FK_ModuleID = @ModuleID
				AND MWorkFlowDetail.UserType = 2

				IF (@PreviousUserUpline = 0)
					BEGIN
						SELECT Muser.*
						FROM Muser WITH(NOLOCK)
						JOIN MWorkFlowDetail WITH(NOLOCK)
						ON MUser.FK_MRole_ID = MWorkFlowDetail.RoleID
						JOIN Module_TR_WorkFlow WITH(NOLOCK)
						ON Module_TR_WorkFlow.FK_WorkFlow_ID = MWorkFlowDetail.FK_WorkFlow
						WHERE Module_TR_WorkFlow.FK_ModuleID = @ModuleID AND MWorkFlowDetail.Level = @CurrentLevel + 1
					END
				ELSE
					BEGIN
						SELECT Muser.*
						FROM MUserStructure WITH(NOLOCK)
						JOIN Muser WITH(NOLOCK)
						ON Muser.PK_MUser_ID = MUserStructure.FK_Parent_ID
						WHERE MUserStructure.FK_User_ID = @UserID
					END
			END
	END
GO
