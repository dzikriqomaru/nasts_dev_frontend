/****** Object:  StoredProcedure [dbo].[usp_generateTable]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[usp_generateTable]
/***********************************************************
* Procedure description:
* Date:   10/15/2015 
* Author: Hendra
*
* Changes
* 
* Date		Modified By			Comments
************************************************************
* 20 Sept 2023 Davin			Nullable update in Information Schema for save draft feature
************************************************************/
(@PkModuleID INT)
AS
BEGIN
	--DECLARE @PkModuleID INT
	--SET @PkModuleID = 9846
	DECLARE @ModuleName              VARCHAR(250),
	        @ModuleLabel             VARCHAR(250),
	        @ModuleDescription       VARCHAR(500),
	        @IsUseDesigner           BIT,
	        @IsUseApproval           BIT,
	        @IsSupportAdd            BIT,
	        @IsSupportEdit           BIT,
	        @IsSupportDelete         BIT,
	        @IsSupportActivation     BIT,
	        @IsSupportView           BIT,
	        @UrlAdd                  VARCHAR(550),
	        @UrlEdit                 VARCHAR(550),
	        @UrlDelete               VARCHAR(550),
	        @UrlActivation           VARCHAR(550),
	        @UrlView                 VARCHAR(550),
	        @UrlApproval             VARCHAR(250),
	        @IsUseStoreProcedureValidation BIT
	
	SELECT @ModuleName = ModuleName,
	       @ModuleLabel             = ModuleLabel,
	       @ModuleDescription       = ModuleDescription,
	       @IsUseDesigner           = IsUseDesigner,
	       @IsUseApproval           = IsUseApproval,
	       @IsSupportAdd            = IsSupportAdd,
	       @IsSupportEdit           = IsSupportEdit,
	       @IsSupportDelete         = IsSupportDelete,
	       @IsSupportActivation     = IsSupportActivation,
	       @IsSupportView           = IsSupportView,
	       @UrlAdd                  = UrlAdd,
	       @UrlEdit                 = UrlEdit,
	       @UrlDelete               = UrlDelete,
	       @UrlActivation           = UrlActivation,
	       @UrlView                 = UrlView,
	       @UrlApproval             = UrlApproval,
	       @IsUseStoreProcedureValidation = IsUseStoreProcedureValidation
	FROM   dbo.Module m
	WHERE  m.PK_Module_ID = @PkModuleID

	IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES t WHERE t.TABLE_NAME = @ModuleName)
	BEGIN
		-- add mode karean belum ada tablenya
	    DECLARE @sql VARCHAR(8000)
	    SET @sql = '	CREATE TABLE dbo. ' + @ModuleName + CHAR(10)
	        + '	( ' + CHAR(10)
	    
	    DECLARE @PK_ModuleField_ID          BIGINT,
	            @FK_Module_ID               INT,
	            @FieldName                  VARCHAR(250),
	            @FieldLabel                 VARCHAR(250),
	            @Sequence                   INT,
	            @Required                   BIT,
	            @IsPrimaryKey               BIT,
	            @IsUnik                     BIT,
	            @FK_FieldType_ID            INT,
	            @SizeField                  INT,
	            @FK_ExtType_ID              INT,
	            @TabelReferenceName         VARCHAR(250),
	            @TableReferenceFieldKey     VARCHAR(250),
	            @TableReferenceFieldDisplayName VARCHAR(250),
	            @TableReferenceFilter       VARCHAR(550),
	            @IsUseRegexValidation       BIT
	    
	    DECLARE my_cursor CURSOR FAST_FORWARD READ_ONLY 
	    FOR
			SELECT PK_ModuleField_ID,
	               FK_Module_ID,
	               FieldName,
	               FieldLabel,
	               Sequence,
	               [Required],
	               IsPrimaryKey,
	               IsUnik,
	               FK_FieldType_ID,
	               SizeField,
	               FK_ExtType_ID,
	               TabelReferenceName,
	               TableReferenceFieldKey,
	               TableReferenceFieldDisplayName,
	               TableReferenceFilter,
	               IsUseRegexValidation
	        FROM   dbo.ModuleField mf WHERE mf.FK_Module_ID=@PkModuleID 	        
	        UNION all
			SELECT a.PK_ModuleField_ID,@PkModuleID, REPLACE(a.FieldName,'[FieldName]',b.FieldName) AS FieldNAme, REPLACE(a.FieldLabel,'[FieldName]',b.FieldLabel) AS FieldLabel,
				a.Sequence, a.[Required],
				a.IsPrimaryKey, a.IsUnik, a.FK_FieldType_ID, a.SizeField, a.FK_ExtType_ID, null, null, null, null, NULL
			FROM ModuleFieldDefaultFileUpload a
			CROSS JOIN dbo.ModuleField b 
			WHERE b.FK_Module_ID = @PkModuleID AND b.FK_ExtType_ID IN (7, 8) AND b.FK_FieldType_ID = 14
	        ORDER BY Sequence
	    
	    OPEN my_cursor
	    
	    FETCH FROM my_cursor INTO @PK_ModuleField_ID, @FK_Module_ID, @FieldName,
	    @FieldLabel, @Sequence, @Required, @IsPrimaryKey,
	    @IsUnik, @FK_FieldType_ID, @SizeField, @FK_ExtType_ID,
	    @TabelReferenceName, @TableReferenceFieldKey,
	    @TableReferenceFieldDisplayName,
	    @TableReferenceFilter, @IsUseRegexValidation
	    
	    WHILE @@FETCH_STATUS = 0
	    BEGIN
	        DECLARE @datatype VARCHAR(30) = ''
	        
	        IF @FK_FieldType_ID <> 11 
	        begin
				SELECT @datatype = mt.FieldTypeSQLName
				FROM   MFieldType mt
				WHERE  mt.PK_FieldType_ID = @FK_FieldType_ID
	        END 
	        IF @FK_FieldType_ID=11
	        BEGIN	        	
	        	IF @FK_ExtType_ID=15  OR @FK_ExtType_ID = 21
	        	BEGIN
	        		--karena checkbox, maka tipedatanya varchar(max)
	        		set @datatype='varchar(max)'
	        	END
	        	ELSE
	        	BEGIN
	        		SELECT @datatype=c.DATA_TYPE+ case when c.CHARACTER_MAXIMUM_LENGTH IS NULL THEN '' WHEN c.CHARACTER_MAXIMUM_LENGTH=-1 THEN '(max)' ELSE '('+ CONVERT(VARCHAR(20), c.CHARACTER_MAXIMUM_LENGTH) +')' END   
	        		FROM INFORMATION_SCHEMA.[COLUMNS] c WHERE c.TABLE_NAME=@TabelReferenceName AND c.COLUMN_NAME=@TableReferenceFieldKey
	        	END
	        END
			
	        DECLARE @strsizefield VARCHAR(30) = ''
	        SET @strsizefield = '' 
	        IF @FK_FieldType_ID = 9
				IF @SizeField<>-1 
				BEGIN
					SET @strsizefield = ' (' + CONVERT(VARCHAR(50), @SizeField) +')'	
				END 	
				ELSE
					SET @strsizefield = ' (max)'	
	        ELSE IF @FK_FieldType_ID = 14
				SET @strsizefield = ' (max)'	
	        
	        DECLARE @strreq AS VARCHAR(30)=''
	        IF @Required = 1 And @IsPrimaryKey = 1
				SET @strreq = ' Not Null '
	        ELSE
	            SET @strreq = ' NULL ' 
	        
	        
	        DECLARE @identity VARCHAR(30)
	        SET @identity = ''
	        IF @FK_FieldType_ID = 12 OR @FK_FieldType_ID = 15
	            SET @identity = ' IDENTITY (1, 1) '	
	        
	        SET @sql = @sql + ' [' + @FieldName + ']  ' + @datatype + @strsizefield + ' ' + @strreq + @identity + ',' +CHAR(10)+CHAR(13)
	        
	        FETCH FROM my_cursor INTO @PK_ModuleField_ID, @FK_Module_ID, @FieldName,
	        @FieldLabel, @Sequence, @Required, @IsPrimaryKey,
	        @IsUnik, @FK_FieldType_ID, @SizeField,
	        @FK_ExtType_ID, @TabelReferenceName,
	        @TableReferenceFieldKey,
	        @TableReferenceFieldDisplayName,
	        @TableReferenceFilter, @IsUseRegexValidation
	    END
	    
	    CLOSE my_cursor
	    DEALLOCATE my_cursor

	    DECLARE @PK_ModuleField_IDdefault bigint, @FieldNameDefault varchar(250),
        @FieldLabelDefault varchar(250), @SequenceDefault int, @Requireddefault bit,
        @IsPrimaryKeyDefault bit, @IsUnikDefault bit, @FK_FieldType_IDDefault int, @SizeFieldDefault int,
        @FK_ExtType_IDDefault int
	    
	    DECLARE my_cursor CURSOR FAST_FORWARD READ_ONLY FOR
	    SELECT PK_ModuleField_ID, FieldName, FieldLabel, Sequence, [Required],
	           IsPrimaryKey, IsUnik, FK_FieldType_ID, SizeField, FK_ExtType_ID
	    FROM dbo.ModuleFieldDefault 
	    
	    OPEN my_cursor
	    
	    FETCH FROM my_cursor INTO @PK_ModuleField_IDdefault, @FieldNamedefault, @FieldLabeldefault,
	                              @Sequencedefault, @Requireddefault, @IsPrimaryKeydefault, @IsUnikdefault,
	                              @FK_FieldType_IDdefault, @SizeFielddefault, @FK_ExtType_IDdefault
	    
	    WHILE @@FETCH_STATUS = 0
	    BEGIN
	    
	     DECLARE @datatypedefault VARCHAR(30)=''
	        SELECT @datatypedefault= mt.FieldTypeSQLName
	        FROM   MFieldType mt
	        WHERE  mt.PK_FieldType_ID = @FK_FieldType_IDDefault
	        
	        DECLARE @strsizefieldDefault VARCHAR(30)=''
	        SET @strsizefielddefault = '' 
	        IF @FK_FieldType_IDDefault = 9
	            SET @strsizefieldDefault = ' (' + CONVERT(VARCHAR(50), @SizeFieldDefault) + ')'
	        
	        DECLARE @strreqDefault AS VARCHAR(30)=''
	        IF @Requireddefault = 1
	            SET @strreqDefault = ' Not Null '
	        ELSE
	            SET @strreqDefault = ' NULL ' 
	        
	        
	        DECLARE @identityDefault VARCHAR(30)
	        SET @identityDefault = ''
	        IF @FK_FieldType_IDDefault = 12 OR @FK_FieldType_IDDefault = 15
	            SET @identityDefault = ' IDENTITY (1, 1) '	
	        
	        
	    SET @sql = @sql + ' [' + @FieldNamedefault + ']  ' + @datatypeDefault + @strsizefielddefault	 + ' ' + @strreqDefault + @identitydefault + ','+CHAR(10)+CHAR(13)
	    
	    	FETCH FROM my_cursor INTO @PK_ModuleField_IDdefault, @FieldNamedefault,
	    	                          @FieldLabeldefault, @Sequencedefault, @Requireddefault,
	    	                          @IsPrimaryKeydefault, @IsUnikdefault, @FK_FieldType_IDdefault,
	    	                          @SizeFielddefault, @FK_ExtType_IDdefault
	    END
	    
	    CLOSE my_cursor
	    DEALLOCATE my_cursor
	    
	    IF LEN(@sql) > 0
	        SET @sql = SUBSTRING(@sql, 0, LEN(@sql) -3)    
	    
	    SET @sql = @sql + ')  ON [PRIMARY]'
	    
	    EXEC (@sql)
	    
	    DECLARE @keyprimary VARCHAR(50)
	    SELECT @keyprimary = mf.FieldName
	    FROM   ModuleField mf
	    WHERE  mf.FK_Module_ID = @PkModuleID
	           AND mf.IsPrimaryKey = 1
	    
	    SET @sql = 'ALTER TABLE dbo.' + @ModuleName + ' ADD CONSTRAINT ' + CHAR(10)
	        + '	PK_' + @ModuleName + ' PRIMARY KEY CLUSTERED  ' + CHAR(10)
	        + '	( ' + CHAR(10)
	        + '	' + @keyprimary + ' ' + CHAR(10)
	        +
	        ') WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY] ' 
	        + CHAR(10)

	    EXEC (@sql)
	END
	ELSE
	BEGIN
	-----------------
	-- ADD COLUMN
	-----------------
		DECLARE  @FieldNameCek varchar(250), @FieldTypeSQLNameCek varchar(50), @SizeFieldCek INT, @datatypecek VARCHAR(50)
		DECLARE my_cursor CURSOR FAST_FORWARD READ_ONLY FOR
		-- cari yg ada di setting dan tidak ada di table
		SELECT 
			mf.FieldName, 
			CASE WHEN mf.FK_ExtType_ID <> 15 AND mf.FK_ExtType_ID <> 21 THEN mt.FieldTypeSQLName ELSE 'varchar' end AS FieldTypeSQLName, 
			CASE WHEN mf.FK_ExtType_ID <> 15 AND mf.FK_ExtType_ID <> 21 THEN mf.SizeField ELSE -1 END AS SizeField, 
			CASE WHEN mf.FK_ExtType_ID <> 15 AND mf.FK_ExtType_ID <> 21 THEN mt.FieldTypeSQLName ELSE 'varchar' end AS FieldTypeSQLName
		FROM 
		(
			SELECT PK_ModuleField_ID, FK_Module_ID, FieldName, FK_FieldType_ID, SizeField,FK_ExtType_ID
			FROM  ModuleField
			WHERE FK_Module_ID=@PkModuleID 
			UNION ALL
			SELECT a.PK_ModuleField_ID, @PkModuleID, REPLACE(a.FieldName,'[FieldName]',b.FieldName) , a.FK_FieldType_ID, a.SizeField,a.FK_ExtType_ID
			FROM  ModuleFieldDefaultFileUpload a
			CROSS JOIN dbo.ModuleField b 
			WHERE b.FK_Module_ID=@PkModuleID AND b.FK_ExtType_ID IN (7, 8) AND b.FK_FieldType_ID=14     
		) mf
		INNER JOIN MFieldType mt 
			ON mt.PK_FieldType_ID=mf.FK_FieldType_ID 
		INNER JOIN module x 
			ON x.PK_Module_ID=mf.FK_Module_ID
		left JOIN INFORMATION_SCHEMA.[COLUMNS] c 
			ON mf.FieldName = c.COLUMN_NAME AND c.TABLE_NAME = x.ModuleName
		WHERE 
			mf.FK_Module_ID = @PkModuleID
			AND c.COLUMN_NAME IS NULL
		-- add field yg kurang
		OPEN my_cursor
		FETCH FROM my_cursor INTO @FieldNameCek, @FieldTypeSQLNameCek, @SizeFieldCek, @datatypecek
		WHILE @@FETCH_STATUS = 0
		BEGIN
			IF @SizeFieldCek=-1 
			BEGIN
				IF @datatypecek='varchar' OR @datatypecek='varbinary' SET @datatypecek=@datatypecek + '(max)'
			END
			ELSE
			BEGIN
				IF @datatypecek='varchar' SET @datatypecek=@datatypecek + '('+ CONVERT(VARCHAR(20), @SizeFieldCek)	 +')'
				ELSE IF @datatypecek='varbinary' SET @datatypecek=@datatypecek + '(max)'
			END

			set @sql = ' Alter table ' + @ModuleName + ' add [' + @FieldNameCek + '] ' + @datatypecek
	
			EXEC(@sql)
		
			FETCH FROM my_cursor INTO @FieldNamecek, @FieldTypeSQLNamecek, @SizeFieldcek, @datatypecek
		END	
		CLOSE my_cursor
		DEALLOCATE my_cursor
	-----------------
	-- DELETE COLUMN
	-----------------
		DECLARE @FieldNamebuang varchar(250), @FieldTypeSQLNamebuang varchar(50), @SizeFieldbuang int
		DECLARE my_cursor CURSOR FAST_FORWARD READ_ONLY FOR
		--cari yg ada di table dan tidak ada di setting
		SELECT 
			c.COLUMN_NAME, 
			c.DATA_TYPE, 
			isnull(c.CHARACTER_MAXIMUM_LENGTH, 0) AS maxpanjang
		FROM 
		(
			SELECT PK_ModuleField_ID, FK_Module_ID, FieldName, FK_FieldType_ID, SizeField
			FROM  ModuleField
			WHERE FK_Module_ID=@PkModuleID 
			UNION ALL
			SELECT a.PK_ModuleField_ID, @PkModuleID, REPLACE(a.FieldName,'[FieldName]',b.FieldName) , a.FK_FieldType_ID, a.SizeField
			FROM  ModuleFieldDefaultFileUpload a
			CROSS JOIN dbo.ModuleField  b 
			WHERE b.FK_Module_ID=@PkModuleID AND b.FK_ExtType_ID IN (7, 8) AND b.FK_FieldType_ID=14   
		) mf
		INNER JOIN MFieldType mt 
			ON mt.PK_FieldType_ID=mf.FK_FieldType_ID 
		INNER JOIN module x 
			ON x.PK_Module_ID=mf.FK_Module_ID
		right JOIN INFORMATION_SCHEMA.[COLUMNS] c 
			ON mf.FieldName=c.COLUMN_NAME AND c.TABLE_NAME=x.ModuleName
		WHERE 
			c.TABLE_NAME=@ModuleName
			AND mf.PK_ModuleField_ID IS NULL
			AND c.COLUMN_NAME NOT IN 
			(
				SELECT mfd.FieldName FROM ModuleFieldDefault mfd	
			)
		--buang field yg lebih
		OPEN my_cursor
		FETCH FROM my_cursor INTO @FieldNamebuang,@FieldTypeSQLNamebuang,@SizeFieldbuang
		WHILE @@FETCH_STATUS = 0
		BEGIN
			set @sql=' Alter table ' + @ModuleName + ' drop column [' + @FieldNamebuang + ']'	 
			EXEC(@sql)
			FETCH FROM my_cursor INTO  @FieldNamebuang,@FieldTypeSQLNamebuang,@SizeFieldbuang
		END
		CLOSE my_cursor
		DEALLOCATE my_cursor
	
	-----------------
	-- UPDATE COLUMN
	-----------------
		DECLARE  @FieldNameubah varchar(250), @FieldTypeSQLNameubah varchar(500), @SizeFieldubah INT, @tipedataubah VARCHAR(50), @RequiredUbah BIT
		
		DECLARE @ToVarbinary BIT = 0
		DECLARE my_cursor CURSOR FAST_FORWARD READ_ONLY FOR
		SELECT 
			xx.FieldName, 
			xx.FieldTypeSQLName, 
			xx.SizeFieldcurrent, 
			xx.DATA_TYPE,
			xx.[Required]
		FROM 
		(
			SELECT 
				mf.FieldName,
				CASE 
					WHEN mf.FK_FieldType_ID <> 11 AND mf.FK_ExtType_ID <> 21 THEN mt.FieldTypeSQLName
					WHEN mf.FK_FieldType_ID = 11 AND mf.FK_ExtType_ID<>15 AND mf.FK_ExtType_ID <> 21 THEN 
					(
						SELECT c.DATA_TYPE 
						FROM   INFORMATION_SCHEMA.[COLUMNS] c
						WHERE  c.TABLE_NAME = mf.TabelReferenceName
                        AND c.COLUMN_NAME = mf.TableReferenceFieldKey
					)   
					WHEN mf.FK_FieldType_ID=11 AND (mf.FK_ExtType_ID = 15 OR mf.FK_ExtType_ID = 21) THEN 'varchar'
				END AS FieldTypeSQLName,  
	            CASE 
					WHEN mf.FK_FieldType_ID=11  AND (mf.FK_ExtType_ID = 15 OR mf.FK_ExtType_ID = 21) THEN -1
					WHEN mf.FK_FieldType_ID <> 11 AND mf.FK_ExtType_ID <> 21 THEN mf.SizeField
					WHEN mf.FK_FieldType_ID = 11 AND mf.FK_ExtType_ID<>15 AND mf.FK_ExtType_ID <> 21 THEN 
					(
						 	SELECT 
								CASE 
								WHEN c.CHARACTER_MAXIMUM_LENGTH IS NULL THEN '' 
								WHEN c.CHARACTER_MAXIMUM_LENGTH = -1 THEN -1
								ELSE c.CHARACTER_MAXIMUM_LENGTH
							END
						 FROM   INFORMATION_SCHEMA.[COLUMNS] c
						 WHERE  
							c.TABLE_NAME = mf.TabelReferenceName
                            AND c.COLUMN_NAME = mf.TableReferenceFieldKey
					)
				END  AS SizeFieldcurrent, 
				CASE 
					WHEN mf.FK_ExtType_ID<>15 THEN  c.DATA_TYPE 
					ELSE 'varchar'
				end AS Data_type, 
				CASE 
					WHEN isnull(mf.FK_ExtType_ID, 0) <> 15 THEN isnull(c.CHARACTER_MAXIMUM_LENGTH,'')
					ELSE '-1' 
				END  AS sizeFieldexisting,
				mf.[Required],
				c.IS_NULLABLE AS RequiredExisting
			FROM
			(
				SELECT PK_ModuleField_ID, FK_Module_ID, FieldName, FK_FieldType_ID, SizeField,TabelReferenceName,TableReferenceFieldKey,FK_ExtType_ID, [Required]
				FROM  ModuleField
				WHERE FK_Module_ID=@PkModuleID 
				UNION ALL
				SELECT a.PK_ModuleField_ID, @PkModuleID, REPLACE(a.FieldName,'[FieldName]',b.FieldName) , a.FK_FieldType_ID, a.SizeField, NULL,NULL,a.FK_ExtType_ID, b.[Required]
				FROM  ModuleFieldDefaultFileUpload a
				CROSS JOIN dbo.ModuleField b 
				WHERE b.FK_Module_ID=@PkModuleID AND b.FK_ExtType_ID IN (7, 8) AND b.FK_FieldType_ID=14     
			) mf
			INNER JOIN MFieldType mt
				ON  mt.PK_FieldType_ID = mf.FK_FieldType_ID
			INNER JOIN INFORMATION_SCHEMA.[COLUMNS] c
				ON  
				(
					mf.FieldName = c.COLUMN_NAME
					--AND mt.FieldTypeSQLName = c.DATA_TYPE
				) 
			WHERE  mf.FK_Module_ID = @PkModuleID
			AND c.TABLE_NAME = @ModuleName
		)xx 
		WHERE xx.FieldTypeSQLName<>xx.DATA_TYPE OR xx.SizeFieldcurrent<>xx.sizeFieldexisting OR (CASE WHEN xx.[Required] = 1 THEN 'NO' ELSE 'YES' END) <> xx.RequiredExisting
		OPEN my_cursor
		FETCH FROM my_cursor INTO @FieldNameubah,@FieldTypeSQLNameubah,@SizeFieldubah,@tipedataubah, @RequiredUbah
		WHILE @@FETCH_STATUS = 0
		BEGIN
			IF @FieldTypeSQLNameubah='varbinary'
				SET @ToVarbinary = 1
			
			IF @SizeFieldubah = -1
			BEGIN
				IF @FieldTypeSQLNameubah='varchar' OR @FieldTypeSQLNameubah='varbinary' 
					SET @FieldTypeSQLNameubah=@FieldTypeSQLNameubah + '(max)'
			END
			ELSE
			BEGIN
				IF @FieldTypeSQLNameubah='varchar'
					SET @FieldTypeSQLNameubah=@FieldTypeSQLNameubah + '('+ convert(VARCHAR(20), @SizeFieldubah)	 +')'
				ELSE IF @FieldTypeSQLNameubah='varbinary'
					SET @FieldTypeSQLNameubah=@FieldTypeSQLNameubah + '(max)'
			END

			IF @ToVarbinary = 1
			BEGIN
				SET @sql = ' Alter table ' + @ModuleName + ' drop column [' + @FieldNameubah + ']  ' + CHAR(10)
					+ ' Alter table ' + @ModuleName + ' add [' + @FieldNameubah + ']  ' + @FieldTypeSQLNameubah
			END
			ELSE
			BEGIN
				SET @sql = ' Alter table ' + @ModuleName + ' alter column [' + @FieldNameubah + ']  ' + @FieldTypeSQLNameubah
			END
	
			EXEC(@sql)
			FETCH FROM my_cursor INTO  @FieldNameubah,@FieldTypeSQLNameubah,@SizeFieldubah,@tipedataubah, @RequiredUbah
		END
		CLOSE my_cursor
		DEALLOCATE my_cursor
	END
END
GO
