/****** Object:  StoredProcedure [dbo].[usp_GetListOptionsByID]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[usp_GetListOptionsByID]
	@PK_ModuleField_ID BIGINT,
	@ParentValue VARCHAR(MAX),
	@UserID VARCHAR(50),
	@FieldKey VARCHAR(MAX) = ''
AS
DECLARE @TableReferenceName VARCHAR(250),
		@TableReferenceNameAlias VARCHAR(250),
		@TableReferenceFieldKey VARCHAR(250),
		@TableReferenceFieldDisplayName VARCHAR(250),
		@TableReferenceFilter VARCHAR(1000),
		@TableReferenceAdditonalJoin VARCHAR(1000),
		@FilterCascade VARCHAR(1000),
		@SQLQuery VARCHAR(MAX)

SELECT @TableReferenceName = TabelReferenceName,
		@TableReferenceNameAlias = TabelReferenceNameAlias,
		@TableReferenceFieldKey = TableReferenceFieldKey,
		@TableReferenceFieldDisplayName = TableReferenceFieldDisplayName,
		@TableReferenceFilter = TableReferenceFilter,
		@TableReferenceAdditonalJoin = TableReferenceAdditonalJoin,
		@FilterCascade = CASE WHEN BCasCade = 1 THEN FilterCascade ELSE '' END
FROM dbo.ModuleField
WHERE PK_ModuleField_ID = @PK_ModuleField_ID

SET @TableReferenceNameAlias = CASE WHEN ISNULL(@TableReferenceNameAlias, '') = '' THEN @TableReferenceName ELSE @TableReferenceNameAlias END

SET @SQLQuery = 'SELECT ' + @TableReferenceNameAlias + '.' + @TableReferenceFieldKey + ' AS [value],' + CHAR(10) + CHAR(13) +
				'		' + @TableReferenceNameAlias + '.' + @TableReferenceFieldDisplayName + ' AS [text]' + CHAR(10) + CHAR(13) +
				'FROM ' + @TableReferenceName + ' AS ' + @TableReferenceNameAlias + CHAR(10) + CHAR(13) +

				-- search
				'WHERE ' + @TableReferenceNameAlias + '.' + @TableReferenceFieldKey + ' = ' + REPLACE(@FieldKey, '''', '''''') + CHAR(10) + CHAR(13) +

				-- filter
				CASE WHEN ISNULL(@TableReferenceFilter, '') <> ''
					 THEN 'AND ' + REPLACE(@TableReferenceFilter, '@UserID', @UserID) +
							CASE WHEN ISNULL(@FilterCascade, '') <> ''
								 THEN ' AND ' + REPLACE(@FilterCascade, '@Parent', ISNULL(@ParentValue, 'NULL'))
								 ELSE ''
							END
					 ELSE CASE WHEN ISNULL(@FilterCascade, '') <> ''
							   THEN 'AND ' + REPLACE(@FilterCascade, '@Parent', ISNULL(@ParentValue, 'NULL'))
							   ELSE ''
						  END
				END
				
EXEC(@SQLQuery)
GO
