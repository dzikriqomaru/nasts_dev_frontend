/****** Object:  StoredProcedure [dbo].[usp_GetListOptionsModuleDetailWithPage]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[usp_GetListOptionsModuleDetailWithPage]
	@PK_ModuleDetailField_ID BIGINT,
	@ParentValue VARCHAR(MAX),
	@SearchKeyword VARCHAR(MAX) = '',
	@PageIndex INT = 1,
	@PageSize INT = 10
AS
BEGIN
	-- COPIED FROM [usp_GetListOptionsModuleDetail]
	DECLARE @TableReferenceName VARCHAR(250),
			@TableReferenceNameAlias VARCHAR(250),
			@TableReferenceFieldKey VARCHAR(250),
			@TableReferenceFieldDisplayName VARCHAR(250),
			@TableReferenceFilter VARCHAR(1000),
			@TableReferenceAdditonalJoin VARCHAR(1000),
			@FilterCascade VARCHAR(1000),
			@SQLQuery VARCHAR(MAX),
			@SearchWord	VARCHAR(MAX) = '''%' + REPLACE(@SearchKeyword, '''', '''''') + '%'''

	SELECT @TableReferenceName = TabelReferenceName,
			@TableReferenceNameAlias = TabelReferenceNameAlias,
			@TableReferenceFieldKey = TableReferenceFieldKey,
			@TableReferenceFieldDisplayName = TableReferenceFieldDisplayName,
			@TableReferenceFilter = TableReferenceFilter,
			@TableReferenceAdditonalJoin = TableReferenceAdditonalJoin,
			@FilterCascade = CASE WHEN BCasCade = 1 THEN FilterCascade ELSE '' END
	FROM dbo.ModuleDetailField
	WHERE PK_ModuleDetailField_ID = @PK_ModuleDetailField_ID

	SET @TableReferenceNameAlias = CASE WHEN ISNULL(@TableReferenceNameAlias, '') = '' THEN @TableReferenceName ELSE @TableReferenceNameAlias END

	SET @SQLQuery = 'SELECT ' + @TableReferenceNameAlias + '.' + @TableReferenceFieldKey + ' AS [value],' + CHAR(10) + CHAR(13) +
					'		' + @TableReferenceNameAlias + '.' + @TableReferenceFieldDisplayName + ' AS [text]' + CHAR(10) + CHAR(13) +
					'FROM ' + @TableReferenceName + ' AS ' + @TableReferenceNameAlias + CHAR(10) + CHAR(13) +

					-- search
					'WHERE (' + @TableReferenceNameAlias + '.' + @TableReferenceFieldKey + ' LIKE ' + @SearchWord + 
					' OR '+ @TableReferenceNameAlias + '.' + @TableReferenceFieldDisplayName + ' LIKE ' + @SearchWord + ') ' +				

					-- filter
					CASE WHEN ISNULL(@TableReferenceFilter, '') <> ''
						 THEN 'AND ' + @TableReferenceFilter + 
								CASE WHEN ISNULL(@FilterCascade, '') <> ''
									THEN ' AND ' + REPLACE(@FilterCascade, '@Parent', ISNULL(@ParentValue, 'NULL'))
									ELSE ''
								END
						 ELSE CASE WHEN ISNULL(@FilterCascade, '') <> ''
								   THEN 'AND ' + REPLACE(@FilterCascade, '@Parent', ISNULL(@ParentValue, 'NULL'))
								   ELSE ''
							  END
					END
	-- END LINE OF [usp_GetListOptionsModuleDetail]

	-- RUN [usp_PagingNew]
	PRINT @SQLQuery
	EXEC [dbo].[usp_PagingNew] @SQLQuery, '', @PageIndex, @PageSize
END
GO
