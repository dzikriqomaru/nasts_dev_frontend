/****** Object:  StoredProcedure [dbo].[usp_GetAdvancedWhereClauseByPK]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_GetAdvancedWhereClauseByPK]
/***********************************************************
* Procedure description:
* Date:   6/12/2018 
* Author: hendra1
*
* Changes
* Date		Modified By			Comments
************************************************************
*
************************************************************/
(
	@PK_AdvancedFilterMapping_ID INT 
)
AS
BEGIN
	
	SELECT * FROM AdvancedFilterWhereClause WHERE PK_AdvancedFilterWhereClause_ID=@PK_AdvancedFilterMapping_ID 
	
END

GO
