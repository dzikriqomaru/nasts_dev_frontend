/****** Object:  StoredProcedure [dbo].[usp_GetAlternateList]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_GetAlternateList]
/***********************************************************
* Procedure description:
* Date:   7/24/2017 
* Author: Hendra
*
* Changes
* Date		Modified By			Comments
************************************************************
*
************************************************************/
(
	@UserID VARCHAR(100)
)
AS
BEGIN
	
SELECT a.UserID , a.UserID+' - '+m.UserName AS UserName, a.StartDate, a.EndDate FROM Alternate a
INNER JOIN MUser m ON a.UserID=m.UserID
WHERE a.UserAlternate=@UserID
AND GETDATE() BETWEEN startdate AND dateadd(day,1,a.EndDate)
UNION 
SELECT x.UserID,x.UserID+' - '+x.UserName,NULL,null FROM muser x WHERE UserID=@UserID
END
GO
