/****** Object:  StoredProcedure [dbo].[usp_ArchiveValidation]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ArchiveValidation] @Report_Date DATETIME, @ProcessType AS INT
AS
BEGIN
	--1: Archive, 2: Restore
	--SELECT @Report_Date = '2018-09-01', @ProcessType = 1

	/*
	Indra Buana, 13 Desember 2018:
	Tambah pengecekan Archive:
	- Jika di DB Transaksi tidak ada data, maka keluarkan error bahwa tidak ada data yg di backup (Result = 1)
	- Jika di DB Archive sudah ada data, maka keluarkan konfirmasi ke user apakah yakin mau melakukan backup ulang (Result = 2)

	Pengecekan Restore:
	- Jika di DB Archive tidak ada data, maka keluarkan error bahwa tidak ada data yg di restore (Result = 1)
	- Jika di DB Transaksi sudah ada data, maka keluarkan konfirmasi ke user apakah yakin mau melakukan restore ulang (Result = 2)
	*/
	DECLARE 
		@SQLQuery AS NVARCHAR(MAX) = '',
		@SQLQueryArchive AS NVARCHAR(MAX) = '',
		@DBArchiveName AS VARCHAR(100) = '',
		@StrResult AS VARCHAR(10) = '0'

	--DECLARE @IntCount AS INT = 0
	----SELECT @DBArchiveName = Value FROM ApplicationParameter WHERE NAME ='Archieve Database'
	--SELECT @DBArchiveName = SettingValue FROM SystemParameter WHERE PK_SystemParameter_ID = 1011

	--SELECT @SQLQuery = @SQLQuery + 
	--'SELECT Jumlah = COUNT(1), NamaForm = ''' + m.ModuleName + ''' FROM ' + m.ModuleName + ' fl ' + 
	--'WHERE MONTH(fl.Report_Date) = ' + CONVERT(VARCHAR, MONTH(@Report_Date)) + 
	--' AND YEAR(fl.Report_Date) = ' + CONVERT(VARCHAR, YEAR(@Report_Date)) + ' UNION ALL ' + CHAR(10)
	--FROM ORS_FormInfo lh
	--INNER JOIN Module m
	--ON lh.FK_Module_ID = m.PK_Module_ID
	--WHERE lh.KeyField = 'ID'

	--SELECT @SQLQueryArchive = @SQLQueryArchive + 
	--'SELECT Jumlah = COUNT(1), NamaForm = ''' + m.ModuleName + '_Archieve'' FROM ' + @DBArchiveName + '..' + m.ModuleName + '_Archieve fl ' + 
	--'WHERE MONTH(fl.Report_Date) = ' + CONVERT(VARCHAR, MONTH(@Report_Date)) + 
	--' AND YEAR(fl.Report_Date) = ' + CONVERT(VARCHAR, YEAR(@Report_Date)) + ' UNION ALL ' + CHAR(10)
	--FROM ORS_FormInfo lh
	--INNER JOIN Module m
	--ON lh.FK_Module_ID = m.PK_Module_ID
	--WHERE lh.KeyField = 'ID'

	--IF LEN(@SQLQuery) > 0
	--BEGIN
	--	SET @SQLQuery = LEFT(@SQLQuery, LEN(@SQLQuery) - 11)
	--	SET @SQLQuery = 'SELECT @Jumlah = SUM(a.Jumlah) FROM (' + @SQLQuery + ')a'

	--	SET @SQLQueryArchive = LEFT(@SQLQueryArchive, LEN(@SQLQueryArchive) - 11)
	--	SET @SQLQueryArchive = 'SELECT @Jumlah = SUM(a.Jumlah) FROM (' + @SQLQueryArchive + ')a'

	--	PRINT(@SQLQuery)
	--	PRINT(@SQLQueryArchive)

	--	IF @ProcessType = 1			--Archive Data
	--	BEGIN
	--		EXECUTE sp_executesql @SQLQuery, N'@Jumlah int OUTPUT', @Jumlah = @IntCount OUTPUT

	--		IF @IntCount = 0
	--		BEGIN
	--			SET @StrResult = '1'
	--		END
	--		ELSE
	--		BEGIN
	--			EXECUTE sp_executesql @SQLQueryArchive, N'@Jumlah int OUTPUT', @Jumlah = @IntCount OUTPUT
	--			IF @IntCount <> 0
	--			BEGIN
	--				SET @StrResult = '2'
	--			END
	--		END
	--	END
	--	ELSE IF @ProcessType = 2	--Restore Data
	--	BEGIN
	--		EXECUTE sp_executesql @SQLQueryArchive, N'@Jumlah int OUTPUT', @Jumlah = @IntCount OUTPUT

	--		IF @IntCount = 0
	--		BEGIN
	--			SET @StrResult = '1'
	--		END
	--		ELSE
	--		BEGIN
	--			EXECUTE sp_executesql @SQLQuery, N'@Jumlah int OUTPUT', @Jumlah = @IntCount OUTPUT
	--			IF @IntCount <> 0
	--			BEGIN
	--				SET @StrResult = '2'
	--			END
	--		END
	--	END
	--END

	SELECT @StrResult
END


GO
