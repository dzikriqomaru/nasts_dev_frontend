/****** Object:  StoredProcedure [dbo].[usp_GroupmenuAccessNew]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_GroupmenuAccessNew]
/***********************************************************
* Procedure description:
* Date:   11/11/2015 
* Author: Hendra
*
* Changes
* Date		Modified By			Comments
************************************************************
*
************************************************************/

AS
BEGIN
	

SELECT mm.PK_MGroupMenu_ID, mm.GroupMenuName
  FROM MGroupMenu mm WHERE mm.[Active] =1 AND mm.PK_MGroupMenu_ID NOT IN (
SELECT mms.FK_MGroupMenu_ID FROM MGroupMenuSettting mms	
  ) ORDER BY mm.GroupMenuName
  
END
GO
