/****** Object:  StoredProcedure [dbo].[usp_GetTaskList]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_GetTaskList]      
/***********************************************************      
* Procedure description      
* Date   10/7/2015      
* Author Hendra      
*      
* Changes      
* Date			Modified By		Comments 
* 04-04-2022	Fauzan			Add Correction / Approval Info     
************************************************************      
*      
************************************************************/      
(      
    @userid VARCHAR(50),
    @groupmenuid INT = NULL,
	@roleid INT = NULL
)      
AS      
BEGIN      
	DECLARE @roleTempid INT, @groupmenuTempid INT, @pkuserid INT 
 
	SELECT 
	@roleTempid = Case When @roleid IS NULL THEN m.FK_MRole_ID ELSE @roleid END , 
	@groupmenuTempid =  Case When @groupmenuid IS NULL THEN m.FK_MGroupMenu_ID ELSE @groupmenuid END , 
	@pkuserid = m.PK_MUser_ID 
	FROM MUser m 
	WHERE m.UserID = @userid; 

	WITH PageIndex AS 
	( 
		SELECT 
			xx.jumlah as jumlah,
			ab.ModuleLabel AS ModuleName,
			yy.mMenuURL,
			yy.FK_Module_ID as ModuleID,
			'Approval' AS ActionType
		FROM 
		(
			SELECT 
				COUNT(1) AS jumlah,
				ma.ModuleName AS NamaModule 
			FROM ModuleApproval AS ma 
			INNER JOIN MUser m 
				ON m.UserID = ma.CreatedBy 
			INNER JOIN Module AS m2 
				ON m2.ModuleName = ma.ModuleName 
			INNER JOIN MWorkFlow_Progress AS mfp 
				ON mfp.FK_Module_ID = m2.PK_Module_ID 
				AND (mfp.FK_Unik_ID = ma.ModuleKey OR mfp.FK_Unik_ID = convert(varchar(50),ma.PK_ModuleApproval_ID)+'import' ) 
			WHERE mfp.FK_MWorkflow_Status_ID IN (3) 
				AND 
				( 
					( 
						mfp.FK_MWorkFlowUserType_ID = 4 
						AND mfp.FK_MWorkflowRole_ID = @roleTempid
					) 
					OR 
					( 
						( 
							mfp.FK_MWorkFlowUserType_ID = 1 
							OR mfp.FK_MWorkFlowUserType_ID = 2 
						) 
						AND mfp.FK_MWorkflowUser_ID = @pkuserid
					) 
				) 
				AND ma.CreatedBy <> @userid
			GROUP BY ma.ModuleName
			
			UNION ALL 
 
			SELECT 
				dataapprovalbyrole.jumlah, 
				dataapprovalbyrole.NamaModule 
			FROM 
			( 
				SELECT 
					COUNT(1) AS jumlah, 
					ma.ModuleName AS NamaModule, 
					( 
						SELECT COUNT(1) 
						FROM Module_TR_WorkFlow 
						WHERE FK_ModuleID = m2.PK_Module_ID 
							AND [Active] = 1 
					) AS jmlworkflow
				FROM ModuleApproval ma 
				INNER JOIN MUser m 
					ON m.UserID = ma.CreatedBy 
				INNER JOIN Module AS m2 
					ON m2.ModuleName = ma.ModuleName 
				WHERE ma.CreatedBy <> @userid AND m.FK_MRole_ID = @roleTempid
				GROUP BY ma.ModuleName, m2.PK_Module_ID 
			) dataapprovalbyrole 
			WHERE dataapprovalbyrole.jmlworkflow = 0 
		) xx 
		INNER JOIN 
		(
			SELECT DISTINCT 
				m.Modulename as namamodule,
				mms.mMenuURL,
				mms.FK_Module_ID 
			FROM MGroupMenuSettting mms 
			INNER JOIN Module m 
				ON mms.FK_Module_ID = m.PK_Module_ID 
			WHERE mms.FK_MGroupMenu_ID = @groupmenuTempid AND mms.FK_Action_ID = 6 
		) yy 
			ON xx.NamaModule = yy.namamodule 
		INNER JOIN Module ab ON ab.ModuleName=xx.NamaModule 
 
		UNION ALL 

		SELECT 
			COUNT(1) AS jumlah, 
			m2.ModuleLabel AS NamaModule,
			'/ParameterCorrection' AS mmenuurl,
			m2.PK_Module_ID,
			'Correction' AS ActionType
		FROM ModuleApproval AS ma 
		INNER JOIN MUser m 
			ON m.UserID = ma.CreatedBy 
		INNER JOIN Module AS m2 
			ON m2.ModuleName = ma.ModuleName 
		INNER JOIN MWorkFlow_Progress AS mfp 
			ON mfp.FK_Module_ID = m2.PK_Module_ID 
			AND mfp.FK_Unik_ID = ma.ModuleKey 
		WHERE 
			mfp.FK_MWorkflow_Status_ID IN (6) 
			AND 
			( 
				( 
					mfp.FK_MWorkFlowUserType_ID = 4 
					AND mfp.FK_MWorkflowRole_ID = @roleTempid
				) 
				OR 
				( 
					( 
						mfp.FK_MWorkFlowUserType_ID = 1 
						OR mfp.FK_MWorkFlowUserType_ID = 2 
					) 
					AND mfp.FK_MWorkflowUser_ID = @pkuserid
				) 
			) 
		GROUP BY m2.ModuleLabel, m2.PK_Module_ID
	) SELECT jumlah, ModuleName, mMenuURL + '?ModuleID=' + CAST(ModuleID AS VARCHAR) mMenuURL,  ModuleID,ActionType From pageindex
END
GO
