/****** Object:  StoredProcedure [dbo].[usp_InsertSummary]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_InsertSummary] 
-- Add the parameters for the stored procedure here
--@bulan varchar(2),
--@tahun varchar(4)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;	
	
	DELETE 
	FROM   ValidationSummary
	
	INSERT INTO [dbo].[ValidationSummary]
	  (
	    [SegmentData],
	    [Status],
	    [TanggalData],
	    [ModuleURL],
		[KodeCabang]
	  )
	SELECT chk.SegmenData,
	       CASE 
	            WHEN cnt > 0 THEN 'Invalid (' + CAST(cnt AS VARCHAR) + ' records)'
	            ELSE     'Valid'
	       END,
	       chk.TanggalData,
	       (
	           SELECT m.UrlView
	           FROM   Module AS m
	           WHERE  m.ModuleName = 'ValidationReport'
	       ) + '?Segmen=' + REPLACE(chk.SegmenData, ' ', '_'),
		   chk.KodeCabang
	FROM   (
	           SELECT COUNT(*)     cnt,
	                  (
	                      SELECT m.ModuleLabel
	                      FROM   Module AS m
	                      WHERE  m.PK_Module_ID = vr.ModuleID
	                  )            SegmenData,
	                  vr.TanggalData, vr.KodeCabang
	           FROM   Web_ValidationReport_RowComplate AS vr
	           GROUP BY
	                  vr.ModuleID,
	                  vr.TanggalData,
					  vr.KodeCabang
	       )             chk
END

GO
