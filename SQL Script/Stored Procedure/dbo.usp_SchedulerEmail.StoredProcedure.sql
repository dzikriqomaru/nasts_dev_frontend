/****** Object:  StoredProcedure [dbo].[usp_SchedulerEmail]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_SchedulerEmail]
/***********************************************************
* Procedure description:
* Date:   23/02/2016 
* Author: Hendra
*
* Changes
* Date		Modified By			Comments
************************************************************
*
************************************************************/

AS
BEGIN
	
	
	declare @ProcessDate DATETIME 
SET @ProcessDate = GETDATE();


WITH dates AS (   
  
SELECT et.PK_EmailTemplate_ID, CONVERT(DATETIME, left(CONVERT(VARCHAR(20), et.StartDate,120),10) + ' '+ et.StartTime,120)Startdate
  FROM EmailTemplate et WHERE et.[Active]=1 AND et.FK_Monitoringduration_ID=2    
  AND CONVERT(DATETIME, left(CONVERT(VARCHAR(20), et.StartDate,120),10) + ' '+ et.StartTime,120) <= @ProcessDate
     UNION ALL
SELECT PK_EmailTemplate_ID,DATEADD(DAY,1,d.startdate) FROM dates d
WHERE DATEADD(DAY,1,d.startdate)<=@ProcessDate ) 

INSERT INTO EmailTemplateScheduler
(
	-- PK_EmailTemplateScheduler_ID -- this column value is auto-generated
	PK_EmailTemplate_ID,
	ProcessDate,FK_EmailStatus_ID
)

    
SELECT  a.PK_EmailTemplate_ID,a.startdate,1 FROM dates A
JOIN EmailTemplate et ON a.PK_EmailTemplate_ID = et.PK_EmailTemplate_ID
JOIN SystemCalendar sc ON CAST(a.Startdate AS DATE) = CAST( sc.CalendarDate AS DATE)
LEFT JOIN EmailTemplateScheduler B ON A.PK_EmailTemplate_ID=B.PK_EmailTemplate_ID 
AND A.startdate=b.ProcessDate WHERE B.PK_EmailTemplateScheduler_ID IS null
AND (et.ExcludeHoliday = 0 OR (et.ExcludeHoliday = 1 AND sc.IsWorkday = 1)) 

ORDER BY a.PK_EmailTemplate_ID,startdate
OPTION(MAXRECURSION 365)  ;  




WITH dates AS (   
  
SELECT et.PK_EmailTemplate_ID, CONVERT(DATETIME, left(CONVERT(VARCHAR(20), et.StartDate,120),10) + ' '+ et.StartTime,120)Startdate
  FROM EmailTemplate et WHERE et.[Active]=1 AND et.FK_Monitoringduration_ID=3
  AND CONVERT(DATETIME, left(CONVERT(VARCHAR(20), et.StartDate,120),10) + ' '+ et.StartTime,120) <= @ProcessDate
     UNION ALL
SELECT PK_EmailTemplate_ID,DATEADD(week,1,d.startdate) FROM dates d
WHERE DATEADD(week,1,d.startdate)<=@ProcessDate ) 




INSERT INTO EmailTemplateScheduler
(
	-- PK_EmailTemplateScheduler_ID -- this column value is auto-generated
	PK_EmailTemplate_ID,
	ProcessDate,FK_EmailStatus_ID
)

    
SELECT  a.PK_EmailTemplate_ID,a.startdate,1 FROM dates A
JOIN EmailTemplate et ON a.PK_EmailTemplate_ID = et.PK_EmailTemplate_ID
JOIN SystemCalendar sc ON CAST(a.Startdate AS DATE) = CAST( sc.CalendarDate AS DATE)
LEFT JOIN EmailTemplateScheduler B ON A.PK_EmailTemplate_ID=B.PK_EmailTemplate_ID 
AND A.startdate=b.ProcessDate WHERE B.PK_EmailTemplateScheduler_ID IS null  
AND (et.ExcludeHoliday = 0 OR (et.ExcludeHoliday = 1 AND sc.IsWorkday = 1)) 

ORDER BY a.PK_EmailTemplate_ID,startdate
OPTION(MAXRECURSION 365)    ;



WITH dates AS (   
  
SELECT et.PK_EmailTemplate_ID, CONVERT(DATETIME, left(CONVERT(VARCHAR(20), et.StartDate,120),10) + ' '+ et.StartTime,120)Startdate
  FROM EmailTemplate et WHERE et.[Active]=1 AND et.FK_Monitoringduration_ID=4
  AND CONVERT(DATETIME, left(CONVERT(VARCHAR(20), et.StartDate,120),10) + ' '+ et.StartTime,120) <= @ProcessDate
     UNION ALL
SELECT PK_EmailTemplate_ID,DATEADD(month,1,d.startdate) FROM dates d
WHERE DATEADD(month,1,d.startdate)<=@ProcessDate ) 




INSERT INTO EmailTemplateScheduler
(
	-- PK_EmailTemplateScheduler_ID -- this column value is auto-generated
	PK_EmailTemplate_ID,
	ProcessDate,FK_EmailStatus_ID
)

    
SELECT  a.PK_EmailTemplate_ID,a.startdate,1 FROM dates A
JOIN EmailTemplate et ON a.PK_EmailTemplate_ID = et.PK_EmailTemplate_ID
JOIN SystemCalendar sc ON CAST(a.Startdate AS DATE) = CAST( sc.CalendarDate AS DATE)
LEFT JOIN EmailTemplateScheduler B ON A.PK_EmailTemplate_ID=B.PK_EmailTemplate_ID 
AND A.startdate=b.ProcessDate WHERE B.PK_EmailTemplateScheduler_ID IS null  
AND (et.ExcludeHoliday = 0 OR (et.ExcludeHoliday = 1 AND sc.IsWorkday = 1)) 

ORDER BY a.PK_EmailTemplate_ID,startdate
OPTION(MAXRECURSION 365)    ;




WITH dates AS (   
  
SELECT et.PK_EmailTemplate_ID, CONVERT(DATETIME, left(CONVERT(VARCHAR(20), et.StartDate,120),10) + ' '+ et.StartTime,120)Startdate
  FROM EmailTemplate et WHERE et.[Active]=1 AND et.FK_Monitoringduration_ID=5
  AND CONVERT(DATETIME, left(CONVERT(VARCHAR(20), et.StartDate,120),10) + ' '+ et.StartTime,120) <= @ProcessDate
     UNION ALL
SELECT PK_EmailTemplate_ID,DATEADD(hour,1,d.startdate) FROM dates d
WHERE DATEADD(hour,1,d.startdate)<=@ProcessDate ) 




INSERT INTO EmailTemplateScheduler
(
	-- PK_EmailTemplateScheduler_ID -- this column value is auto-generated
	PK_EmailTemplate_ID,
	ProcessDate,FK_EmailStatus_ID
)

    
SELECT  a.PK_EmailTemplate_ID,a.startdate,1 FROM dates A
JOIN EmailTemplate et ON a.PK_EmailTemplate_ID = et.PK_EmailTemplate_ID
JOIN SystemCalendar sc ON CAST(a.Startdate AS DATE) = CAST( sc.CalendarDate AS DATE)
LEFT JOIN EmailTemplateScheduler B ON A.PK_EmailTemplate_ID=B.PK_EmailTemplate_ID 
AND A.startdate=b.ProcessDate WHERE B.PK_EmailTemplateScheduler_ID IS null  
AND (et.ExcludeHoliday = 0 OR (et.ExcludeHoliday = 1 AND sc.IsWorkday = 1)) 

ORDER BY a.PK_EmailTemplate_ID,startdate
OPTION(MAXRECURSION 365)    ;


      
      
      
END
GO
