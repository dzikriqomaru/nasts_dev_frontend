/****** Object:  StoredProcedure [dbo].[usp_updateReportQuery]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[usp_updateReportQuery]
/***********************************************************
* Procedure description:
* Date:   01/02/2016 
* Author: Hendra
*
* Changes
* Date		Modified By			Comments
************************************************************
*
************************************************************/
(
	@PK_ReportQuery_ID bigint
)
AS
BEGIN
	
DECLARE @ReportName varchar(550),
        @ReportDescription varchar(max), @QueryData varchar(max), @Active bit,
        @CreatedBy varchar(50), @LastUpdateBy varchar(50),
        @ApprovedBy varchar(50), @CreatedDate datetime,
        @LastUpdateDate datetime, @ApprovedDate datetime

SELECT
	 @ReportName = ReportName,
	@ReportDescription = ReportDescription, @QueryData = QueryData,
	@Active = [Active], @CreatedBy = CreatedBy, @LastUpdateBy = LastUpdateBy,
	@ApprovedBy = ApprovedBy, @CreatedDate = CreatedDate,
	@LastUpdateDate = LastUpdateDate, @ApprovedDate = ApprovedDate
FROM dbo.ReportQuery rq
WHERE rq.PK_ReportQuery_ID=@PK_ReportQuery_ID
	
	
IF EXISTS(SELECT t.TABLE_NAME FROM INFORMATION_SCHEMA.TABLES t WHERE t.TABLE_TYPE='VIEW' AND t.TABLE_NAME=@ReportName)
BEGIN
	exec('DROP VIEW ' + @ReportName)
END
	
DECLARE @sql varchar(max)
SET @sql = 'CREATE VIEW ['+@ReportName +'] ' + char(10)+CHAR(13)
         + 'AS	 ' + char(10)+CHAR(13)
		 +@QueryData
EXEC(@sql)

	
	
	
	
	
END
GO
