/****** Object:  StoredProcedure [dbo].[usp_GetAdvancedWhereClause]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_GetAdvancedWhereClause]  
/***********************************************************  
* Procedure description:  
* Date:   6/11/2018   
* Author: hendra1  
*  
* Changes  
* Date  Modified By   Comments  
************************************************************  
*  
************************************************************/  
(  
 @pkadvanceddatatypeid INT,  
 @value VARCHAR(200)  
)  
AS  
BEGIN  
   
 IF @pkadvanceddatatypeid=2   
 BEGIN  
  SELECT e.FilterWhereClause,e.FilterWhereFormat,e.PK_AdvancedFilterWhereClause_ID 
    From AdvancedFilterMapping a  
  INNER JOIN AdvanceFilterType AS b ON a.FK_AdvancedFilterType_ID=b.PK_AdvancedFilterType_ID  
  INNER JOIN AdvanceDataType AS c ON c.PK_AdvanceDataType_ID=a.FK_AdvanceDataType_ID  
  INNER JOIN AdvancedFilterWhereClause AS e ON e.FK_AdvanceFilter_ID=a.FK_AdvancedFilterType_ID  
  LEFT JOIN MFieldType AS d ON d.PK_FieldType_ID=a.FK_FieldType_ID  
  WHERE   a.FK_AdvanceDataType_ID=@pkadvanceddatatypeid  
  AND convert(VARCHAR(50),d.PK_FieldType_ID)=@value  
    
 END  
 ELSE  
  BEGIN  
     
  
   SELECT  e.FilterWhereClause,e.FilterWhereFormat,e.PK_AdvancedFilterWhereClause_ID   
     From AdvancedFilterMapping a  
   INNER JOIN AdvanceFilterType AS b ON a.FK_AdvancedFilterType_ID=b.PK_AdvancedFilterType_ID  
   INNER JOIN AdvanceDataType AS c ON c.PK_AdvanceDataType_ID=a.FK_AdvanceDataType_ID  
   INNER JOIN AdvancedFilterWhereClause AS e ON e.FK_AdvanceFilter_ID=a.FK_AdvancedFilterType_ID  
  
   WHERE a.FK_AdvanceDataType_ID=@pkadvanceddatatypeid  
   AND a.FK_SqlDataType=@value  
  
  END  
  
    
  
  
END  

GO
