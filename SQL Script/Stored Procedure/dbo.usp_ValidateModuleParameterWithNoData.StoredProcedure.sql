/****** Object:  StoredProcedure [dbo].[usp_ValidateModuleParameterWithNoData]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ValidateModuleParameterWithNoData]  
/***********************************************************  
* Procedure description:  
* Date:   12/15/2016   
* Author: Hendra  
*  
* Changes  
* Date  Modified By   Comments  
************************************************************  
*  
************************************************************/  
(  
 @moduleID INT ,@userid VARCHAR(50)   
)  
AS  
BEGIN  
  
DECLARE @jml INT  
DECLARE @jmlvalidationpermodule INT   
SELECT @jml =COUNT(1) FROM INFORMATION_SCHEMA.TABLES WHERE table_name='validationparameter'  
  
IF @jml>0   
BEGIN  
 --cek modulenya ada di validationparameter ngak, kalau ada dipanggil  
 SELECT @jmlvalidationpermodule=COUNT(1) FROM ValidationParameter vp WHERE vp.TableName=@moduleID  
  
   
END  
  
  SELECT @jmlvalidationpermodule
END
GO
