/****** Object:  StoredProcedure [dbo].[usp_UpdatelinkModule]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[usp_UpdatelinkModule]
/***********************************************************
* Procedure description:
* Date:   12/8/2015 
* Author: Hendra
*
* Changes
* Date		Modified By			Comments
************************************************************
*
************************************************************/
(
	@ModuleID int
)
AS
BEGIN

DECLARE @PK_Module_ID int, @ModuleName varchar(250), @ModuleLabel varchar(250),
        @ModuleDescription varchar(500), @IsUseDesigner bit, @IsUseApproval bit,
        @IsSupportAdd bit, @IsSupportEdit bit, @IsSupportDelete bit,
        @IsSupportActivation bit, @IsSupportView bit, @IsSupportUpload bit,
        @UrlAdd varchar(550), @UrlEdit varchar(550), @UrlDelete varchar(550),
        @UrlActivation varchar(550), @UrlView varchar(550),
        @UrlUpload varchar(550), @UrlApproval varchar(550),
        @UrlApprovalDetail varchar(550), @IsUseStoreProcedureValidation bit,
        @Active bit, @CreatedBy varchar(50), @LastUpdateBy varchar(50),
        @ApprovedBy varchar(50), @CreatedDate datetime,
        @LastUpdateDate datetime, @ApprovedDate datetime

SELECT
	@PK_Module_ID = PK_Module_ID, @ModuleName = ModuleName,
	@ModuleLabel = ModuleLabel, @ModuleDescription = ModuleDescription,
	@IsUseDesigner = IsUseDesigner, @IsUseApproval = IsUseApproval,
	@IsSupportAdd = IsSupportAdd, @IsSupportEdit = IsSupportEdit,
	@IsSupportDelete = IsSupportDelete,
	@IsSupportActivation = IsSupportActivation, @IsSupportView = IsSupportView,
	@IsSupportUpload = IsSupportUpload, @UrlAdd = UrlAdd, @UrlEdit = UrlEdit,
	@UrlDelete = UrlDelete, @UrlActivation = UrlActivation, @UrlView = UrlView,
	@UrlUpload = UrlUpload, @UrlApproval = UrlApproval,
	@UrlApprovalDetail = UrlApprovalDetail,
	@IsUseStoreProcedureValidation = IsUseStoreProcedureValidation,
	@Active = [Active], @CreatedBy = CreatedBy, @LastUpdateBy = LastUpdateBy,
	@ApprovedBy = ApprovedBy, @CreatedDate = CreatedDate,
	@LastUpdateDate = LastUpdateDate, @ApprovedDate = ApprovedDate
FROM dbo.Module m WHERE m.PK_Module_ID=@ModuleID



IF @IsSupportView=1
BEGIN
	UPDATE MenuTemplate
	SET
		mMenuURL = @UrlView
	FROM MenuTemplate A
	INNER JOIN Module B ON a.FK_Module_ID=b.PK_Module_ID
	WHERE b.PK_Module_ID=@ModuleID AND a.FK_Action_ID=5
	
	UPDATE MGroupMenuSettting	
	SET
	    mMenuURL = @UrlView
	FROM   MGroupMenuSettting A
	INNER JOIN Module B ON a.FK_Module_ID=b.PK_Module_ID
	WHERE b.PK_Module_ID=@ModuleID AND a.FK_Action_ID=5
	
END

IF @IsUseApproval=1
BEGIN
	UPDATE MenuTemplate
	SET
		mMenuURL = @UrlApproval
	FROM MenuTemplate A
	INNER JOIN Module B ON a.FK_Module_ID=b.PK_Module_ID
	WHERE b.PK_Module_ID=@ModuleID AND a.FK_Action_ID=6
	
	
	UPDATE MGroupMenuSettting	
	SET
	    mMenuURL = @UrlApproval
	FROM   MGroupMenuSettting A
	INNER JOIN Module B ON a.FK_Module_ID=b.PK_Module_ID
	WHERE b.PK_Module_ID=@ModuleID AND a.FK_Action_ID=6
	
END

IF @IsSupportUpload=1
BEGIN
	UPDATE MenuTemplate
	SET
		mMenuURL = @UrlUpload
	FROM MenuTemplate A
	INNER JOIN Module B ON a.FK_Module_ID=b.PK_Module_ID
	WHERE b.PK_Module_ID=@ModuleID AND a.FK_Action_ID=7
	
	UPDATE MGroupMenuSettting	
	SET
	    mMenuURL = @UrlUpload
	FROM   MGroupMenuSettting A
	INNER JOIN Module B ON a.FK_Module_ID=b.PK_Module_ID
	WHERE b.PK_Module_ID=@ModuleID AND a.FK_Action_ID=7
	
END



   

END
GO
