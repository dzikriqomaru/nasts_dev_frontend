/****** Object:  StoredProcedure [dbo].[usp_RebuildDataModel]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_RebuildDataModel]
(
	@DestDB AS VARCHAR(MAX)
)
AS 
BEGIN
	DECLARE @CurrentDB AS VARCHAR(MAX) = DB_NAME(),
			@MaxID AS VARCHAR(MAX) = '100000';

	IF (LOWER(@CurrentDB) = LOWER(@DestDB))
	BEGIN
		RAISERROR ('Destination DB must not be same with current DB. ', 16, 1);
	END

	BEGIN TRANSACTION [TransData]
	BEGIN TRY
		DECLARE @SQL AS VARCHAR(MAX) = '';
	
		-- DELETE Module, ModuleField, ModuleFieldRegex, ValidationParameter
		SET @SQL = 'DELETE FROM [' + @DestDB + '].dbo.ValidationParameter WHERE TableName <= ' + @MaxID + ' AND Fk_Ref_KategoriValidasi_Id <> 1;' + CHAR(10);
		SET @SQL += 'DELETE FROM [' + @DestDB + '].dbo.ModuleFieldRegex WHERE EXISTS(SELECT 1 FROM ' + @CurrentDB + '.dbo.ModuleField mf
				WHERE mf.PK_ModuleField_ID = ModuleFieldRegex.FK_ModuleField_ID AND mf.FK_Module_ID <= ' + @MaxID + ');' + CHAR(10);

		SET @SQL += 'DELETE FROM [' + @DestDB + '].dbo.ModuleField WHERE FK_Module_ID <= ' + @MaxID + ';' + CHAR(10)
		SET @SQL += 'DELETE FROM [' + @DestDB + '].dbo.Module WHERE PK_Module_ID <= ' + @MaxID + ';' + CHAR(10) + CHAR(13);
		EXEC(@SQL)

		-- INSERT Module, ModuleField, ModuleFieldRegex, ValidationParameter
		DECLARE  @ModuleID AS INTEGER, @IsUseDesigner AS BIT;
		DECLARE CursorModule CURSOR LOCAL FAST_FORWARD READ_ONLY 
		FOR
			SELECT PK_Module_ID, IsUseDesigner FROM Module WHERE PK_Module_ID <= @MaxID
			
		OPEN CursorModule
		FETCH FROM CursorModule INTO @ModuleID, @IsUseDesigner;
		WHILE @@FETCH_STATUS = 0
		BEGIN
			-- Module
			SET @SQL = 'USE ' + @DestDB + ';' + CHAR(10) + 
				'SET IDENTITY_INSERT [' + @DestDB + '].dbo.Module ON;' + CHAR(10) + CHAR(13);

			SET @SQL += 'INSERT INTO [' + @DestDB + '].dbo.Module ' + CHAR(10) +
			'(PK_Module_ID, ModuleName, ModuleLabel, ModuleDescription, IsUseDesigner, IsUseApproval,
			IsSupportAdd, IsSupportEdit, IsSupportDelete, IsSupportActivation, IsSupportView, IsSupportUpload, IsSupportDetail,
			UrlAdd, UrlEdit, UrlDelete, UrlActivation, UrlView, UrlUpload, UrlApproval, UrlApprovalDetail, UrlDetail,
			IsUseStoreProcedureValidation, Active, CreatedBy, LastUpdateBy, ApprovedBy, CreatedDate, LastUpdateDate, ApprovedDate)' + CHAR(10);

			SET @SQL += 'SELECT PK_Module_ID, ModuleName, ModuleLabel, ModuleDescription, IsUseDesigner, IsUseApproval,
			IsSupportAdd, IsSupportEdit, IsSupportDelete, IsSupportActivation, IsSupportView, IsSupportUpload, IsSupportDetail,
			UrlAdd, UrlEdit, UrlDelete, UrlActivation, UrlView, UrlUpload, UrlApproval, UrlApprovalDetail, UrlDetail,
			IsUseStoreProcedureValidation, Active, CreatedBy, ''sysadmin'', ApprovedBy, CreatedDate, GETDATE(), ApprovedDate
			FROM ' + @CurrentDB + '.dbo.Module WHERE PK_Module_ID = ' + CAST(@ModuleID AS VARCHAR(MAX)) + ' ORDER BY PK_Module_ID;' + CHAR(10) + CHAR(13);

			-- ModuleField
			SET @SQL += 'INSERT INTO [' + @DestDB + '].dbo.ModuleField ' + CHAR(10) +
			'(FK_Module_ID, FieldName, FieldLabel, Sequence, Required, IsPrimaryKey, IsUnik, IsShowInView, IsShowInForm, 
			DefaultValue, FK_FieldType_ID, SizeField, FK_ExtType_ID, TabelReferenceName, TabelReferenceNameAlias, 
			TableReferenceFieldKey, TableReferenceFieldDisplayName, TableReferenceFilter, IsUseRegexValidation, 
			TableReferenceAdditonalJoin, BCasCade, FieldNameParent, FilterCascade)' + CHAR(10);

			SET @SQL += 'SELECT FK_Module_ID, FieldName, FieldLabel, Sequence, Required, IsPrimaryKey, IsUnik, IsShowInView, IsShowInForm, 
			DefaultValue, FK_FieldType_ID, SizeField, FK_ExtType_ID, TabelReferenceName, TabelReferenceNameAlias, 
			TableReferenceFieldKey, TableReferenceFieldDisplayName, TableReferenceFilter, IsUseRegexValidation, 
			TableReferenceAdditonalJoin, BCasCade, FieldNameParent, FilterCascade
			FROM ' + @CurrentDB + '.dbo.ModuleField WHERE FK_Module_ID = ' + CAST(@ModuleID AS VARCHAR(MAX)) + ' ORDER BY Sequence;' + CHAR(10) + CHAR(13);

			IF @IsUseDesigner = 1
			BEGIN
			   SET @SQL += 'EXEC ' + @DestDB + '.dbo.usp_generateTable ' + CAST(@ModuleID AS VARCHAR(MAX)) + ';' + CHAR(10);
			   SET @SQL += 'EXEC ' + @DestDB + '.dbo.usp_GenerateTableUpload ' + CAST(@ModuleID AS VARCHAR(MAX)) + ';' + CHAR(10) + CHAR(13);
            END

			SET @SQL += 'SET IDENTITY_INSERT [' + @DestDB + '].dbo.Module OFF;' + CHAR(10);
			SET @SQL += 'PRINT(' + CAST(@ModuleID AS VARCHAR(MAX)) + ')'
			EXEC(@SQL)

			FETCH FROM CursorModule INTO @ModuleID, @IsUseDesigner;
		END

		CLOSE CursorModule;
		DEALLOCATE CursorModule;
		
			-- ModuleFieldRegex
		SET @SQL = 'INSERT INTO [' + @DestDB + '].dbo.ModuleFieldRegex (FK_ModuleField_ID, Regex)
					SELECT mfnew.PK_ModuleField_ID, reg.Regex
					FROM ' + @CurrentDB + '.dbo.ModuleFieldRegex reg
					INNER JOIN ' + @CurrentDB + '.dbo.ModuleField mfold ON mfold.PK_ModuleField_ID = reg.FK_ModuleField_ID
					INNER JOIN ' + @DestDB + '.dbo.ModuleField mfnew ON mfnew.FieldName = mfold.FieldName AND mfnew.FK_Module_ID = mfold.FK_Module_ID
					WHERE mfold.FK_Module_ID <= ' + @MaxID + CHAR(10) + CHAR(13);

		-- ValidationParameter
		SET @SQL += 'INSERT INTO [' + @DestDB + '].dbo.ValidationParameter
					(TableName, FieldName, ExpressionType, ValidationExpression, Description, ValidationMessage, ValidationType,
					ErrorType, Active, CreatedBy, LastUpdateBy, ApprovedBy, CreatedDate, LastUpdateDate, ApprovedDate, Alternateby)
					SELECT TableName, FieldName, ExpressionType, ValidationExpression, Description, ValidationMessage, ValidationType,
						ErrorType, Active, CreatedBy, ''sysadmin'', ApprovedBy, CreatedDate, GETDATE(), ApprovedDate, Alternateby
					FROM ' + @CurrentDB + '.dbo.ValidationParameter
					WHERE TableName <= ' + @MaxID + ' AND Fk_Ref_KategoriValidasi_Id <> 1' + CHAR(10) + CHAR(13);
		EXEC(@SQL)

		COMMIT TRANSACTION [TransData]
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION [TransData]
		EXEC('SET IDENTITY_INSERT [' + @DestDB + '].dbo.Module OFF');
		SELECT ERROR_MESSAGE() AS ERROR
	END CATCH
END
GO
