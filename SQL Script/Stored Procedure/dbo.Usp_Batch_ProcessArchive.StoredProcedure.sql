/****** Object:  StoredProcedure [dbo].[Usp_Batch_ProcessArchive]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Usp_Batch_ProcessArchive] 
(@PK_EODTaskDetailLog_ID AS BIGINT, @DataDate DATETIME)
AS
BEGIN
	DECLARE @srcTable            VARCHAR(255),
	        @destTable           VARCHAR(255),
	        @FieldName           VARCHAR(255),
	        @PeriodType          INT,
	        @RetentionPeriod     SMALLINT
	
	DECLARE my_cursor2 CURSOR FAST_FORWARD READ_ONLY 
	FOR
	    SELECT scrTable,
	           destTable,
	           FieldName,
	           PeriodType,
	           RetentionPeriod
	    FROM HouseKeepingParam AS hkp
	    
	    --WHERE  ACTIVE = 1
	
	OPEN my_cursor2
	
	FETCH FROM my_cursor2 
	INTO @srcTable,
	@destTable,
	@FieldName,
	@PeriodType,
	@RetentionPeriod 
	
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
	    DECLARE @StrQuery AS VARCHAR(MAX)
	    DECLARE @listField AS VARCHAR(8000)
	    DECLARE @operator VARCHAR(50)
	    


	    
	    IF @PeriodType = 1 
	    BEGIN
	    	set @operator ='DAY'
	    END 
	     IF @PeriodType = 2 
	    BEGIN
	    	set @operator ='week'
	    END 
	      IF @PeriodType = 3
	    BEGIN
	    	set @operator ='month'
	    END 
	       IF @PeriodType = 4
	    BEGIN
	    	set @operator ='year'
	    END 
	    	
	   
	        SELECT @listField = STUFF(
	                   (
	                       SELECT ',[' + COLUMN_NAME + ']'
	                       FROM   INFORMATION_SCHEMA.COLUMNS AS inside
	                       WHERE  inside.TABLE_NAME = outside.TABLE_NAME
	                       ORDER BY
	                              ORDINAL_POSITION
	                              FOR XML PATH('')
	                   ),
	                   1,
	                   1,
	                   ''
	               )
	        FROM   INFORMATION_SCHEMA.COLUMNS AS outside
	        WHERE  TABLE_NAME IN (@srcTable)
	        GROUP BY
	               TABLE_NAME
	    
	    
	    
	    DECLARE @sql VARCHAR(MAX) = 'declare @ProcessDate DATETIME =''' + FORMAT(@DataDate, 'yyyy-MM-dd') + ''' '
	    
	   SET @sql = @sql +'  DECLARE @rowInserted BIGINT '+CHAR(13)
	 SET @sql = @sql  +' DECLARE @rowdeleted BIGINT '+CHAR(13)
	 
	    SET @sql = @sql + ' ' + CHAR(10)
	        + ' ' + CHAR(10)
	        + 'BEGIN TRANSACTION;   ' + CHAR(10)
	        + '   ' + CHAR(10)
	        + 'BEGIN TRY   ' + CHAR(10)
	        + ' ' + CHAR(10)
	        IF LOWER( @destTable)<>'remove' 
	        BEGIN
	        	
			   SET @sql = @sql  	  + ' IF EXISTS( SELECT t.TABLE_NAME  ' + char(10)
				+ '   FROM INFORMATION_SCHEMA.TABLES AS t WHERE t.TABLE_NAME='''+@destTable+''')  ' + char(10)
				+ '   BEGIN ' + char(10)
         
				+ '   SET IDENTITY_INSERT ' + @destTable + ' on ' + CHAR(10)
				+ '   INSERT INTO ' + @destTable + ' ' + CHAR(10)
				+ '   (' + @listField + ') ' + CHAR(10)
				+ '   ' + CHAR(10)
				+ '   SELECT  ' + CHAR(10)
				+ '   ' + @listField + ' ' + CHAR(10)
				+ '    FROM ' + @srcTable + ' ' + CHAR(10)
				+ '    WHERE DATEDIFF(DAY,' + @FieldName + ',@ProcessDate) > '+CONVERT(VARCHAR(50),@RetentionPeriod) + CHAR(10)
				+ '	   SELECT @rowInserted= @@ROWCOUNT ' + CHAR(10)
				+ '      ' + CHAR(10)
				+ '    SET IDENTITY_INSERT ' + @destTable + ' OFF ' + CHAR(10)
				+ '     ' + CHAR(10)
				+ ' END  ' + char(10)
				+ ' ELSE ' + char(10)
				+ ' BEGIN ' + char(10)
				+ ' 	SELECT * INTO '+@destTable+' FROM '+ @srcTable+'   WHERE DATEDIFF(DAY,'+@FieldName+',@ProcessDate) > '+CONVERT(VARCHAR(50),@RetentionPeriod) + char(10)
				+ ' END ' + char(10)
         
	        END
	      
			SET @sql = @sql       + '    DELETE FROM ' + @srcTable + ' WHERE DATEDIFF(DAY,[' + @FieldName + '],@ProcessDate) > '+CONVERT(VARCHAR(50),@RetentionPeriod) + CHAR(10)
	        
	          + ' SELECT @rowdeleted= @@ROWCOUNT ' + CHAR(10)
	    
		 + 'INSERT INTO ArchiveLog ' + char(10)
         + '  ( ' + char(10)
         + '  	-- PK_ArchiveLog_ID -- this column value is auto-generated ' + char(10)
         + '  	ProcessDate, ' + char(10)
         + '  	DataDate, ' + char(10)
         + '  	SourceTable, ' + char(10)
         + '  	RowDelete, ' + char(10)
         + '  	DestinationTable, ' + char(10)
         + '  	RowInserted ' + char(10)
         + '  ) ' + char(10)
         + '  VALUES ' + char(10)
         + '  ( ' + char(10)
         + '  	GETDATE(), ' + char(10)
         + '  	@ProcessDate, ' + char(10)
         + '  	'''+@srcTable+''', ' + char(10)
         + '  	@rowdeleted, ' + char(10)
         + '  	'''+@destTable+''', ' + char(10)
         + '  isnull(	@rowInserted,0) ' + char(10)
         + '  )'

	        + '    ' + CHAR(10)
	        + '    ' + CHAR(10)
	        + 'END TRY   ' + CHAR(10)
	        + 'BEGIN CATCH   ' + CHAR(10)
	        + '   ' + CHAR(10)
	        + '   ' + CHAR(10)
	        + '    IF @@TRANCOUNT > 0   ' + CHAR(10)
	        + '        ROLLBACK TRANSACTION;   ' + CHAR(10)
	        +'  DECLARE @rrmessage VARCHAR(MAX) ' + char(10)
           + '	  select @rrmessage =ERROR_MESSAGE()   ' + char(10)
           + '	RAISERROR (@rrmessage,16,1);'
	        + 'END CATCH;   ' + CHAR(10)
	        + '   ' + CHAR(10)
	        + 'IF @@TRANCOUNT > 0   ' + CHAR(10)
	        + '    COMMIT TRANSACTION;   ' + CHAR(10)
	        + '     ' + CHAR(10)
	        + '    ' + CHAR(10)
	      
	        + '     ' + CHAR(10)
	        + '    '
	    
	  

	  
		IF @PeriodType=1
		BEGIN
			--PRINT  'exec daily'
			print ( @sql)
		END
		
		IF @PeriodType=2 AND DATEPART(dw,@DataDate)=2
		BEGIN
		--	PRINT  'exec weekly on monday'						
		print( @sql)
		END
		 	IF @PeriodType=3 AND DATEPART(day,@DataDate)=1 
		BEGIN
			--PRINT 'exec monthly on first month'						
		print( @sql)
		END
		
		 	IF @PeriodType=4 AND DATEPART(day,@DataDate)=1 AND DATEPART(month,@DataDate)=1 
		BEGIN			
			--PRINT 'exec yearly on 1 jan'			
			print( @sql)
		END
	  
	  
	    FETCH FROM my_cursor2 INTO @srcTable,
	    @destTable,
	    @FieldName,
	    @PeriodType,
	    @RetentionPeriod
END

CLOSE my_cursor2
DEALLOCATE my_cursor2 
--Update Field-field tambahan

END
GO
