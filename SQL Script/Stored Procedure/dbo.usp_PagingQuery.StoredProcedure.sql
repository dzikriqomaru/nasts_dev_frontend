/****** Object:  StoredProcedure [dbo].[usp_PagingQuery]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_PagingQuery]
/***********************************************************
* Procedure description:
* Date:   7/5/2019 
* Author: nawadata
*
* Changes
* Date		Modified By			Comments
************************************************************
*
************************************************************/
(
	@querydata VARCHAR(MAX),@orderby VARCHAR(8000),@PageNum INT ,@PageSize INT 
)
AS
BEGIN
	
	DECLARE @strquery VARCHAR(MAX)
	
	set @strquery =' declare @PageNum INT = ' +convert(varchar(50),@PageNum)
	set @strquery =@strquery+' declare @PageSize INT = ' +convert(varchar(50),@PageSize)
	set @strquery=@strquery+' '+@querydata


	
	IF @orderby='' SET @orderby=' 1 '
	
	
	DECLARE @pagingquery VARCHAR(8000)='  OFFSET (@PageNum - 1) * @PageSize ROWS FETCH NEXT @PageSize ROWS ONLY;'
	SET @strquery=@strquery  +' order by '+ @orderby
	SET @strquery=@strquery+ @pagingquery
	exec(@strquery)
END
GO
