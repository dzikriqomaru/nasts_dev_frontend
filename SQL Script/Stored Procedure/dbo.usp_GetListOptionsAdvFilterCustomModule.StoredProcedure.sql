/****** Object:  StoredProcedure [dbo].[usp_GetListOptionsAdvFilterCustomModule]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   PROC [dbo].[usp_GetListOptionsAdvFilterCustomModule]
	@TableReferenceName VARCHAR(250),
	@TableReferenceFieldKey VARCHAR(250),
	@TableReferenceFieldDisplayName VARCHAR(250)
AS
DECLARE @SQLQuery NVARCHAR(MAX)

SET @SQLQuery = 'SELECT ' + @TableReferenceName + '.' + @TableReferenceFieldKey + ' AS [text],' + CHAR(10) + CHAR(13) +
				'		' + @TableReferenceName + '.' + @TableReferenceFieldDisplayName + ' AS [value]' + CHAR(10) + CHAR(13) +
				'FROM ' + @TableReferenceName + ' AS ' + @TableReferenceName + CHAR(10) + CHAR(13) 

EXEC sp_executesql  @SQLQuery 
GO
