/****** Object:  StoredProcedure [dbo].[usp_ReplicateModuleQuery]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ReplicateModuleQuery]
	@PK_Module_ID INT,
	@Mode INT = 1	--1 Keep ID, 2 New ID
AS
BEGIN    
     
DECLARE @strScript VARCHAR(8000) = 'BEGIN TRAN' + CHAR(10)    

PRINT @strScript    

SET @strScript = 'BEGIN TRY' + CHAR(10)

PRINT @strScript    

IF @Mode = 1    
	SET @StrScript = 'SET IDENTITY_INSERT Module ON' + CHAR(10)    
ELSE IF @Mode = 2
	SET @StrScript = 'DECLARE @ModuleID INT' + CHAR(10)    
    
PRINT @strScript    
      
IF @Mode = 1
	SET @StrScript = 'INSERT INTO Module (PK_Module_ID,ModuleName,ModuleLabel,ModuleDescription,IsUseDesigner,IsUseApproval,IsSupportAdd,IsSupportEdit,IsSupportDelete,IsSupportActivation,IsSupportView,IsSupportUpload,IsSupportDetail,UrlAdd,UrlEdit,UrlDelete,UrlActivation,UrlView,UrlUpload,UrlApproval,UrlApprovalDetail,UrlDetail,IsUseStoreProcedureValidation,Active,CreatedBy,LastUpdateBy,ApprovedBy,CreatedDate,LastUpdateDate,ApprovedDate) ' + CHAR(10) + 'VALUES ('    
ELSE IF @Mode = 2
	SET @StrScript = 'INSERT INTO Module (ModuleName,ModuleLabel,ModuleDescription,IsUseDesigner,IsUseApproval,IsSupportAdd,IsSupportEdit,IsSupportDelete,IsSupportActivation,IsSupportView,IsSupportUpload,IsSupportDetail,UrlAdd,UrlEdit,UrlDelete,UrlActivation,UrlView,UrlUpload,UrlApproval,UrlApprovalDetail,UrlDetail,IsUseStoreProcedureValidation,Active,CreatedBy,LastUpdateBy,ApprovedBy,CreatedDate,LastUpdateDate,ApprovedDate) ' + CHAR(10) + 'VALUES ('    

SET @StrScript = @StrScript+(SELECT CASE WHEN @Mode = 1 THEN CAST(ISNULL(PK_Module_ID,'') AS VARCHAR(255))    +',' WHEN @Mode = 2 THEN '' END + ''''
    +CAST(ISNULL(ModuleName,'')              AS VARCHAR(255))    +''','''    
    +CAST(ISNULL(ModuleLabel,'')             AS VARCHAR(255))    +''','''    
    +CAST(ISNULL(ModuleDescription,'')            AS VARCHAR(255))    +''','    
    +CAST(ISNULL(IsUseDesigner,'')             AS VARCHAR(255))    +','    
    +CAST(ISNULL(IsUseApproval,'')             AS VARCHAR(255))    +','    
    +CAST(ISNULL(IsSupportAdd,'')             AS VARCHAR(255))    +','    

    +CAST(ISNULL(IsSupportEdit,'')             AS VARCHAR(255))    +','    
    +CAST(ISNULL(IsSupportDelete,'')            AS VARCHAR(255))    +','    
    +CAST(ISNULL(IsSupportActivation,'')           AS VARCHAR(255))    +','    
    +CAST(ISNULL(IsSupportView,'')             AS VARCHAR(255))    +','    
    +CAST(ISNULL(IsSupportUpload,'')            AS VARCHAR(255))    +','    
    +CAST(ISNULL(IsSupportDetail,'')           AS VARCHAR(255))    +','''    
    +CAST(ISNULL(UrlAdd,'')               AS VARCHAR(255))    +''','''    
    +CAST(ISNULL(UrlEdit,'')              AS VARCHAR(255))    +''','''    
    +CAST(ISNULL(UrlDelete,'')              AS VARCHAR(255))    +''','''    
	+CAST(ISNULL(UrlActivation,'')              AS VARCHAR(255))    +''',''' 
   
	+CAST(ISNULL(UrlView,'')              AS VARCHAR(255))    +''','''    
    +CAST(ISNULL(UrlUpload,'')              AS VARCHAR(255))    +''','''    
    +CAST(ISNULL(UrlApproval,'')             AS VARCHAR(255))    +''','''    
    +CAST(ISNULL(UrlApprovalDetail,'')            AS VARCHAR(255))    +''','''    
    +CAST(ISNULL(UrlDetail,'')              AS VARCHAR(255))    +''','    
    +CAST(ISNULL(IsUseStoreProcedureValidation,'') AS VARCHAR(255))    +','    
    +CAST(ISNULL([ACTIVE],'')              AS VARCHAR(255))    +','''    
    +CAST(ISNULL(CreatedBy,'')              AS VARCHAR(255))    +''','''    
    +CAST(ISNULL(LastUpdateBy,'')             AS VARCHAR(255))    +''','''    
    +CAST(ISNULL(ApprovedBy,'')             AS VARCHAR(255))    +''',GETDATE(),GETDATE(),GETDATE())'

	FROM Module where PK_Module_ID=@PK_Module_ID) + CHAR(10)     

	PRINT @strScript    

	IF @Mode = 1
		SET @StrScript = 'SET IDENTITY_INSERT Module OFF' + CHAR(10)     
	ELSE IF @Mode = 2
		SET @StrScript = 'SET @ModuleID = SCOPE_IDENTITY()' + CHAR(10)     

	PRINT @StrScript    
    
--SELECT PK_ModuleField_ID FROM ModuleField  WHERE FK_Module_ID=120173 ORDER BY [Sequence]    
    
DECLARE @PK_ModuleField_ID INT    
    
DECLARE CURSOR_Module_Field_ID CURSOR LOCAL     

 FOR      
     
  SELECT PK_ModuleField_ID FROM ModuleField  WHERE FK_Module_ID=@PK_Module_ID ORDER BY [Sequence]    
     
     
  OPEN CURSOR_Module_Field_ID;          
  FETCH NEXT FROM CURSOR_Module_Field_ID INTO          
  @PK_ModuleField_ID;     
        
          
  WHILE @@FETCH_STATUS = 0           
  BEGIN        
       
   SET @strScript = ''    
   SET @strScript =  @strScript + 'INSERT INTO ModuleField(FK_Module_ID,FieldName,FieldLabel,Sequence,Required,IsPrimaryKey,IsUnik,IsShowInView,IsShowInForm,DefaultValue,FK_FieldType_ID,SizeField,FK_ExtType_ID,TabelReferenceName,TabelReferenceNameAlias,TableReferenceFieldKey,TableReferenceFieldDisplayName,TableReferenceFilter,IsUseRegexValidation,TableReferenceAdditonalJoin,BCasCade,FieldNameParent,FilterCascade) Values(' + CASE WHEN @Mode = 1 THEN CAST(@PK_Module_ID AS VARCHAR(255)) WHEN @Mode = 2 THEN '@ModuleID' END + ','''    
   SET @strScript =  @strScript + (SELECT CAST(ISNULL(FieldName,'')      AS VARCHAR(255))    +''','''    
            +CAST(ISNULL(FieldLabel,'')      AS VARCHAR(255))    +''','    
            +CAST(ISNULL(Sequence,'')      AS VARCHAR(255))    +','    
            +CAST(ISNULL(Required,'')      AS VARCHAR(255))    +','    
            +CAST(ISNULL(IsPrimaryKey,'')     AS VARCHAR(255))    +','    
            +CAST(ISNULL(IsUnik,'')       AS VARCHAR(255))    +','    
            +CAST(ISNULL(IsShowInView,'')     AS VARCHAR(255))    +','    
            +CAST(ISNULL(IsShowInForm,'')     AS VARCHAR(255))    +','''    
            +CAST(ISNULL(DefaultValue,'')     AS VARCHAR(255))    +''','    
            +CAST(ISNULL(FK_FieldType_ID,'')     AS VARCHAR(255))    +','    
            +CAST(ISNULL(SizeField,'')      AS VARCHAR(255))    +','    
			+CAST(ISNULL(FK_ExtType_ID,'')     AS VARCHAR(255))    +','''    
            +CAST(ISNULL(TabelReferenceName,'')    AS VARCHAR(255))    +''','''    
            +CAST(ISNULL(TabelReferenceNameAlias,'')   AS VARCHAR(255))    +''','''    
            +CAST(ISNULL(TableReferenceFieldKey,'')   AS VARCHAR(255))    +''','''    
            +CAST(ISNULL(TableReferenceFieldDisplayName,'') AS VARCHAR(255))    +''','''    
            +CAST(ISNULL(TableReferenceFilter,'')   AS VARCHAR(255))    +''','    
            +CAST(ISNULL(IsUseRegexValidation,'')   AS VARCHAR(255))    +','''    
            +CAST(ISNULL(TableReferenceAdditonalJoin,'')  AS VARCHAR(255))    +''','    
            +CAST(ISNULL(BCasCade,'')      AS VARCHAR(255))    +','''    
            +CAST(ISNULL(FieldNameParent,'')     AS VARCHAR(255))    +''','''    
            +CAST(ISNULL(FilterCascade,'')     AS VARCHAR(255))    +''')'     
                
            FROM ModuleField WHERE PK_ModuleField_ID=@PK_ModuleField_ID)    
  
    PRINT @strScript          
      
	FETCH NEXT FROM CURSOR_Module_Field_ID INTO           @PK_ModuleField_ID;             
      
	END          
	CLOSE CURSOR_Module_Field_ID;     
	DEALLOCATE CURSOR_Module_Field_ID;         
	
DECLARE @PK_ModuleValidation_ID INT    
DECLARE CURSOR_Module_Validation_ID CURSOR LOCAL     

 FOR      
     
  SELECT PK_ModuleValidation_ID FROM ModuleValidation  WHERE FK_Module_ID=@PK_Module_ID
     
     
  OPEN CURSOR_Module_Validation_ID;          
  FETCH NEXT FROM CURSOR_Module_Validation_ID INTO @PK_ModuleValidation_ID;     
        
          
  WHILE @@FETCH_STATUS = 0           
  BEGIN        
       
   SET @strScript = ''    
   SET @strScript =  @strScript + 'INSERT INTO ModuleValidation(FK_Module_ID, FK_ModuleAction_ID, FK_ModuleTime_ID, StoreProcedureName, StoreProcedureParameter, StoreProcedureParameterValueFieldSequence) Values(' + CASE WHEN @Mode = 1 THEN CAST(@PK_Module_ID AS VARCHAR(255)) WHEN @Mode = 2 THEN '@ModuleID' END + ','    
   SET @strScript =  @strScript + (SELECT CAST(ISNULL(FK_ModuleAction_ID,'')      AS VARCHAR(255))    +','    
            +CAST(ISNULL(FK_ModuleTime_ID,'')      AS VARCHAR(255))    +','''    
            +CAST(ISNULL(StoreProcedureName,'')      AS VARCHAR(255))    +''','''    
            +CAST(ISNULL(StoreProcedureParameter,'')      AS VARCHAR(255))    +''','''    
            +CAST(ISNULL(StoreProcedureParameterValueFieldSequence,'')      AS VARCHAR(255))    +''')'     
                
            FROM ModuleValidation WHERE PK_ModuleValidation_ID=@PK_ModuleValidation_ID)    
  
    PRINT @strScript          
      
	FETCH NEXT FROM CURSOR_Module_Validation_ID INTO @PK_ModuleValidation_ID;     
      
	END          
	CLOSE CURSOR_Module_Validation_ID;     
	DEALLOCATE CURSOR_Module_Validation_ID;         
      
  SET @strScript = 'EXEC usp_generateTable ' + CASE WHEN @Mode = 1 THEN CAST(@PK_Module_ID AS VARCHAR(255)) WHEN @Mode = 2 THEN '@ModuleID' END
      
  PRINT @strScript    
      
  SET @strSCript = 'EXEC usp_generateTableUpload ' + CASE WHEN @Mode = 1 THEN CAST(@PK_Module_ID AS VARCHAR(255)) WHEN @Mode = 2 THEN '@ModuleID' END
      
  PRINT @strScript    
    
  SET @strScript = 'COMMIT' + CHAR(10)

  PRINT @strScript    
    
  SET @strScript = 'PRINT ''SUCCESS''' + CHAR(10)

  PRINT @strScript    
    
  SET @strScript = 'END TRY' + CHAR(10)

  PRINT @strScript 
   
    
  SET @strScript = 'BEGIN CATCH' + CHAR(10)

  PRINT @strScript    
    
  SET @strScript = 'ROLLBACK' + CHAR(10)

  PRINT @strScript    
    
  SET @strScript = 'PRINT ''FAILED''' + CHAR(10)

  PRINT @strScript    
    
  SET @strScript = 'PRINT ERROR_MESSAGE()' + CHAR(10)

  PRINT @strScript    
    
  SET @strScript = 'END CATCH' + CHAR(10)

  PRINT @strScript    

END 
GO
