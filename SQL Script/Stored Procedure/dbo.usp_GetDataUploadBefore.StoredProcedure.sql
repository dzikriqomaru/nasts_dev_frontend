/****** Object:  StoredProcedure [dbo].[usp_GetDataUploadBefore]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_GetDataUploadBefore]

(
	@pkapprovalid BIGINT
)
AS
BEGIN
	
	DECLARE @PK_ModuleApproval_ID bigint, @ModuleName varchar(max),
        @ModuleKey varchar(1000), @ModuleField varchar, @ModuleFieldBefore VARCHAR(MAX),
        @PK_ModuleAction_ID int, @CreatedDate datetime, @CreatedBy varchar(50)
	
	SELECT
		@PK_ModuleApproval_ID = PK_ModuleApproval_ID, @ModuleName = ModuleName,
		@ModuleKey = ModuleKey, @ModuleField = ModuleField,
		@ModuleFieldBefore = ModuleFieldBefore,
		@PK_ModuleAction_ID = PK_ModuleAction_ID, @CreatedDate = CreatedDate,
		@CreatedBy = CreatedBy
	FROM dbo.ModuleApproval ma
	WHERE ma.PK_ModuleApproval_ID=@pkapprovalid
	
	
	DECLARE @pkmoduleid int
	SELECT @pkmoduleid =PK_Module_ID FROM module WHERE ModuleName= @ModuleName
	
	DECLARE @fields VARCHAR(MAX)=''

	
	
	
	SET @fields=  '  myTempTable.XmlCol.query(''./PK_upload_ID'').value(''.'', ''bigint'') AS ID , ' + char(10)
         + '  myTempTable.XmlCol.query(''./nawa_userid'').value(''.'', ''varchar(8000)'') AS nawa_userid , ' + char(10)
         + '  myTempTable.XmlCol.query(''./nawa_recordnumber'').value(''.'', ''varchar(8000)'') AS nawa_recordnumber , ' + char(10)
         + '  myTempTable.XmlCol.query(''./KeteranganError'').value(''.'', ''varchar(8000)'') AS KeteranganError , ' + char(10)
         + '  myTempTable.XmlCol.query(''./nawa_Action'').value(''.'', ''varchar(8000)'') AS nawa_Action ,'+char(10)
		 + '  myTempTable.XmlCol.query(''./Active'').value(''.'', ''varchar(8000)'') AS Active ,'+char(10)



	DECLARE @pkprimary VARCHAR(50)
	DECLARE @PK_ModuleField_ID bigint, @FK_Module_ID int, @FieldName varchar(250),
        @FieldLabel varchar(250), @Sequence int, @Required bit,
        @IsPrimaryKey bit, @IsUnik bit, @IsShowInView bit, @FK_FieldType_ID int,
        @SizeField int, @FK_ExtType_ID int, @TabelReferenceName varchar(250),
        @TableReferenceFieldKey varchar(250),
        @TableReferenceFieldDisplayName varchar(250),
        @TableReferenceFilter varchar(550), @IsUseRegexValidation bit
	
	DECLARE my_cursor CURSOR FAST_FORWARD READ_ONLY FOR
	SELECT PK_ModuleField_ID, FK_Module_ID, FieldName, FieldLabel, Sequence,
	       [Required], IsPrimaryKey, IsUnik, IsShowInView, FK_FieldType_ID,
	       SizeField, FK_ExtType_ID, TabelReferenceName, TableReferenceFieldKey,
	       TableReferenceFieldDisplayName, TableReferenceFilter,
	       IsUseRegexValidation
	FROM dbo.ModuleField mf WHERE mf.FK_Module_ID=@pkmoduleid AND mf.FK_FieldType_ID<>14 
	
	OPEN my_cursor
	
	FETCH FROM my_cursor INTO @PK_ModuleField_ID, @FK_Module_ID, @FieldName,
	                          @FieldLabel, @Sequence, @Required, @IsPrimaryKey,
	                          @IsUnik, @IsShowInView, @FK_FieldType_ID,
	                          @SizeField, @FK_ExtType_ID, @TabelReferenceName,
	                          @TableReferenceFieldKey,
	                          @TableReferenceFieldDisplayName,
	                          @TableReferenceFilter, @IsUseRegexValidation
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
		
		IF @IsPrimaryKey =1
		BEGIN
		                 	
		set @pkprimary=@FieldName                 	
		END
	
	SET @fields=@fields +'myTempTable.XmlCol.query(''./'+@FieldName+''').value(''.'', ''varchar(8000)'') AS '+ @FieldName +' ,' +char(10)
	
	  
		FETCH FROM my_cursor INTO @PK_ModuleField_ID, @FK_Module_ID, @FieldName,
		                          @FieldLabel, @Sequence, @Required,
		                          @IsPrimaryKey, @IsUnik, @IsShowInView,
		                          @FK_FieldType_ID, @SizeField, @FK_ExtType_ID,
		                          @TabelReferenceName, @TableReferenceFieldKey,
		                          @TableReferenceFieldDisplayName,
		                          @TableReferenceFilter, @IsUseRegexValidation
	END
	
	CLOSE my_cursor
	DEALLOCATE my_cursor
	

IF LEN(@fields)>0 SET @fields=SUBSTRING(@fields,1,LEN(@fields)-2) 	
	
	
	DECLARE @sql varchar(max)
	
SET @sql =	' SELECT '+CHAR(10)
    +'c.* ' +CHAR(10) 
 +' FROM ('+CHAR(10)
 	
+ ' ' + char(10)
         + 'SELECT   ' + char(10)
         + @fields + CHAR(10)
         + ' FROM ( ' + char(10)
         + 'SELECT CAST( ma.ModuleField AS xml) AS Rawxml FROM ModuleApproval ma ' + char(10)
         + 'WHERE ma.PK_ModuleApproval_ID='+ CONVERT(VARCHAR(50), @pkapprovalid )+ char(10)
         + ' ) as b ' + char(10)
         + ' CROSS APPLY b.Rawxml.nodes(''//DataUpload/'+@ModuleName+'_Upload'') myTempTable(XmlCol)'+CHAR(10)
         +')xx INNER JOIN '+@ModuleName+' AS c ON xx.'+ @pkprimary +'=c.'+@pkprimary +'' +CHAR(10)
	     +' WHERE xx.nawa_Action=''update'''
PRINT @sql         
exec (@sql)

	
	
	
END

GO
