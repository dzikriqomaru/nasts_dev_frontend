/****** Object:  StoredProcedure [dbo].[usp_GenerateTableDetailUpload]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_GenerateTableDetailUpload]
/***********************************************************
* Procedure description:
* Date:   07/12/2020 
* Author: Fauzan
*
* Changes
* Date		Modified By			Comments
************************************************************
*
************************************************************/

(@PKModuleDetailID INT)

AS
BEGIN

	
	
	DECLARE @ModuleDetailName              VARCHAR(250),
	        @ModuleDetailLabel             VARCHAR(250),
	        @ModuleDetailDescription       VARCHAR(500)

	SELECT 
		@ModuleDetailName		 = ModuleDetailName,
	    @ModuleDetailLabel		 = ModuleDetailLabel,
	    @ModuleDetailDescription = ModuleDetailDescription
	FROM dbo.ModuleDetail
	WHERE PK_ModuleDetail_ID = @PKModuleDetailID
	
set @ModuleDetailName = @ModuleDetailName + '_Upload'

	
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES t WHERE t.TABLE_NAME = @ModuleDetailName)
BEGIN
	EXEC ('drop table '+ @ModuleDetailName)
END	
  -- add mode buat lagi
	    DECLARE @sql VARCHAR(8000)
	    SET @sql = '	CREATE TABLE dbo. ' + @ModuleDetailName + CHAR(10)+CHAR(13)
	        + '	( ' + CHAR(10)+CHAR(13)
	    SET @sql += ' PK_upload_ID bigint identitY(1,1), '+CHAR(10)+CHAR(13)    
	    SET @sql += ' nawa_userid varchar(50), '+CHAR(10)+CHAR(13)
	    SET @sql += ' nawa_recordnumber bigint, '+CHAR(10)+CHAR(13)
	    SET @sql += ' KeteranganError varchar(max), '+CHAR(10)+CHAR(13)
	    SET @sql += ' nawa_Action varchar(50), '+CHAR(10)+CHAR(13)
	    
	    DECLARE @PK_ModuleDetailField_ID          BIGINT,
	            @FK_ModuleDetail_ID               INT,
	            @FieldName                  VARCHAR(250),
	            @FieldLabel                 VARCHAR(250),
	            @Sequence                   INT,
	            @Required                   BIT,
	            @IsPrimaryKey               BIT,
	            @IsUnik		                BIT,
	            @FK_FieldType_ID            INT,
	            @SizeField                  INT,
	            @FK_ExtType_ID              INT,
	            @TabelReferenceName         VARCHAR(250),
	            @TableReferenceFieldKey     VARCHAR(250),
	            @TableReferenceFieldDisplayName VARCHAR(250),
	            @TableReferenceFilter       VARCHAR(550),
	            @IsUseRegexValidation       BIT
	    
	    DECLARE my_cursor CURSOR FAST_FORWARD READ_ONLY 
	    FOR
	        SELECT PK_ModuleDetailField_ID,
	               FK_ModuleDetail_ID,
	               FieldName,
	               FieldLabel,
	               Sequence,
	               [Required],
	               IsPrimaryKey,
	               IsUnik,
	               FK_FieldType_ID,
	               SizeField,
	               FK_ExtType_ID,
	               TabelReferenceName,
	               TableReferenceFieldKey,
	               TableReferenceFieldDisplayName,
	               TableReferenceFilter,
	               IsUseRegexValidation
	        FROM   dbo.ModuleDetailField
			WHERE  FK_ModuleDetail_ID = @PKModuleDetailID 
			ORDER BY Sequence
	    
	    OPEN my_cursor
	    
	    FETCH FROM my_cursor INTO @PK_ModuleDetailField_ID, @FK_ModuleDetail_ID, @FieldName,
	    @FieldLabel, @Sequence, @Required, @IsPrimaryKey,
	    @IsUnik, @FK_FieldType_ID, @SizeField, @FK_ExtType_ID,
	    @TabelReferenceName, @TableReferenceFieldKey,
	    @TableReferenceFieldDisplayName,
	    @TableReferenceFilter, @IsUseRegexValidation
	    
	    WHILE @@FETCH_STATUS = 0
	    BEGIN
	       
	        SET @sql = @sql + ' [' + @FieldName + ']  varchar(8000),' +CHAR(10)+CHAR(13)
	        
	        
	        FETCH FROM my_cursor INTO @PK_ModuleDetailField_ID, @FK_ModuleDetail_ID, @FieldName,
	        @FieldLabel, @Sequence, @Required, @IsPrimaryKey,
	        @IsUnik, @FK_FieldType_ID, @SizeField,
	        @FK_ExtType_ID, @TabelReferenceName,
	        @TableReferenceFieldKey,
	        @TableReferenceFieldDisplayName,
	        @TableReferenceFilter, @IsUseRegexValidation
	    END
	    
	    CLOSE my_cursor
	    DEALLOCATE my_cursor
	    
	   
	    
	    DECLARE @PK_ModuleField_IDdefault bigint, @FieldNameDefault varchar(250),
        @FieldLabelDefault varchar(250), @SequenceDefault int, @Requireddefault bit,
        @IsPrimaryKeyDefault bit, @IsUnikDefault bit, @FK_FieldType_IDDefault int, @SizeFieldDefault int,
        @FK_ExtType_IDDefault int
	    
	    DECLARE my_cursor CURSOR FAST_FORWARD READ_ONLY FOR
	    SELECT PK_ModuleField_ID, FieldName, FieldLabel, Sequence, [Required],
	           IsPrimaryKey, IsUnik, FK_FieldType_ID, SizeField, FK_ExtType_ID
	    FROM dbo.ModuleFieldDefault WHERE PK_ModuleField_ID=1 --hanya field active yg ditambahkan
	    
	    OPEN my_cursor
	    
	    FETCH FROM my_cursor INTO @PK_ModuleField_IDdefault, @FieldNamedefault, @FieldLabeldefault,
	                              @Sequencedefault, @Requireddefault, @IsPrimaryKeydefault, @IsUnikdefault,
	                              @FK_FieldType_IDdefault, @SizeFielddefault, @FK_ExtType_IDdefault
	    
	    WHILE @@FETCH_STATUS = 0
	    BEGIN
	    
	    
	        
	        
	    SET @sql = @sql + ' [' + @FieldNamedefault + ']  bit' +CHAR(10)+CHAR(13)
	    
	    	FETCH FROM my_cursor INTO @PK_ModuleField_IDdefault, @FieldNamedefault,
	    	                          @FieldLabeldefault, @Sequencedefault, @Requireddefault,
	    	                          @IsPrimaryKeydefault, @IsUnikdefault, @FK_FieldType_IDdefault,
	    	                          @SizeFielddefault, @FK_ExtType_IDdefault
	    END
	    
	    CLOSE my_cursor
	    DEALLOCATE my_cursor
	    
	    
	    
	    IF LEN(@sql) > 0
	        SET @sql = SUBSTRING(@sql, 0, LEN(@sql) -1)    
	    
	    SET @sql = @sql + ')  ON [PRIMARY]'    
	
	EXEC (@sql)     	
	
END
GO
