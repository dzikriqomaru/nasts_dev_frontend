/****** Object:  StoredProcedure [dbo].[usp_GetConnectionStringSAML]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[usp_GetConnectionStringSAML]  
(  
 @ConnecitonName varchar(8000)   
)  
AS  
BEGIN  
   
   
   
 DECLARE @PK_NawaDataConnectionString_ID int, @ConnectionValue varchar(8000), 
 @PasswordValue varchar(8000), @Passwordbinary varbinary(max)  
   
 SELECT  
  @PK_NawaDataConnectionString_ID = PK_NawaDataConnectionString_ID,  @ConnectionValue = ConnectionValue,  
  @PasswordValue = PasswordValue, @Passwordbinary = Passwordbinary  
 FROM NawadataConnectionString  
 WHERE LTRIM(RTRIM(ConnecitonName))= LTRIM(RTRIM(@ConnecitonName))  
   
 DECLARE @result VARCHAR(8000)  
    OPEN SYMMETRIC KEY ConfigSymKey DECRYPTION BY CERTIFICATE ConfigCert   
 SET @result = decryptByKey(@Passwordbinary)  
 
 SELECT @result
   
END
GO
