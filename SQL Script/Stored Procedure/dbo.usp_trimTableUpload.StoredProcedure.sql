/****** Object:  StoredProcedure [dbo].[usp_trimTableUpload]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_trimTableUpload]
/***********************************************************
* Procedure description:
* Date:   16/05/2016
* Author: Hendra
*
* Changes
* Date		Modified By			Comments
************************************************************
*
************************************************************/
(
	@Moduleid INT, @userid VARCHAR(50),@intMode INT
)
AS
BEGIN
	DECLARE @dateFormat VARCHAR(MAX)
	SELECT @dateFormat = [dbo].[ufn_GetDateTimeFormat] (14)

	DECLARE @PK_ModuleField_ID bigint, @FK_Module_ID int, @FieldName varchar(250),
        @FieldLabel varchar(250), @Sequence int, @Required bit,
        @IsPrimaryKey bit, @IsUnik bit, @IsShowInView bit, @FK_FieldType_ID int,
        @SizeField int, @FK_ExtType_ID int, @TabelReferenceName varchar(250),
        @TabelReferenceNameAlias varchar(250),
        @TableReferenceFieldKey varchar(250),
        @TableReferenceFieldDisplayName varchar(250),
        @TableReferenceFilter varchar(550), @IsUseRegexValidation BIT,@strsql VARCHAR(8000),@fieldtanggal VARCHAR(8000)

	SET @strsql=''
	SET @fieldtanggal=''
	DECLARE my_cursor CURSOR FAST_FORWARD READ_ONLY FOR
	SELECT PK_ModuleField_ID, FK_Module_ID, FieldName, FieldLabel, Sequence,
	       [Required], IsPrimaryKey, IsUnik, IsShowInView, FK_FieldType_ID,
	       SizeField, FK_ExtType_ID, TabelReferenceName,
	       TabelReferenceNameAlias, TableReferenceFieldKey,
	       TableReferenceFieldDisplayName, TableReferenceFilter,
	       IsUseRegexValidation
	FROM ModuleField WHERE FK_Module_ID=@Moduleid

	OPEN my_cursor

	FETCH FROM my_cursor INTO @PK_ModuleField_ID, @FK_Module_ID, @FieldName,
	                          @FieldLabel, @Sequence, @Required, @IsPrimaryKey,
	                          @IsUnik, @IsShowInView, @FK_FieldType_ID,
	                          @SizeField, @FK_ExtType_ID, @TabelReferenceName,
	                          @TabelReferenceNameAlias, @TableReferenceFieldKey,
	                          @TableReferenceFieldDisplayName,
	                          @TableReferenceFilter, @IsUseRegexValidation

	WHILE @@FETCH_STATUS = 0
	BEGIN
		/*{ ... Cursor logic here ... }*/
		--set @strsql=@strsql+ ' '+ @FieldName +' = LTRIM(RTRIM('+@FieldName+')) ,'
		set @strsql=@strsql+ ' '+ @FieldName +' = dbo.udf_StrStripControl('+@FieldName+') ,'

		IF @FK_FieldType_ID=10
		BEGIN
			SET @fieldtanggal = @fieldtanggal + ' ' + @FieldName + ' = ISNULL( CAST( FORMAT( TRY_CONVERT(DATETIME, '+ @FieldName +'),''' + @dateformat + ''') AS VARCHAR), ' + @FieldName + '), '
			--SET @fieldtanggal=@fieldtanggal+' '+@FieldName+'= CONVERT(VARCHAR(8000), TRY_CONVERT(DATETIME, '+ @FieldName +'),'+ CAST(@dateformat AS VARCHAR) +'),'
		END

		FETCH FROM my_cursor INTO @PK_ModuleField_ID, @FK_Module_ID, @FieldName,
		                          @FieldLabel, @Sequence, @Required,
		                          @IsPrimaryKey, @IsUnik, @IsShowInView,
		                          @FK_FieldType_ID, @SizeField, @FK_ExtType_ID,
		                          @TabelReferenceName, @TabelReferenceNameAlias,
		                          @TableReferenceFieldKey,
		                          @TableReferenceFieldDisplayName,
		                          @TableReferenceFilter, @IsUseRegexValidation
	END

	CLOSE my_cursor
	DEALLOCATE my_cursor

	IF LEN(@strsql) > 0
	    SET @strsql = SUBSTRING(@strsql, 1, LEN(@strsql) -1)

	IF LEN(@fieldtanggal) > 0
		SET @fieldtanggal = SUBSTRING(@fieldtanggal, 1, LEN(@fieldtanggal) -1)

	DECLARE @strmodulename VARCHAR(250)
	SELECT @strmodulename =m.ModuleName
	FROM Module m WHERE m.PK_Module_ID=@Moduleid

	DECLARE @bcreatetableforeachuser AS BIT=0
	SELECT @bcreatetableforeachuser = settingvalue FROM SystemParameter AS sp WHERE sp.PK_SystemParameter_ID=53
	DECLARE @strtableuploadname VARCHAR(500)=''
	IF @bcreatetableforeachuser=1
	BEGIN
		SET @strtableuploadname=@strmodulename +'_Upload_data_' + dbo.StripTableName(@userid)
	END
	ELSE
	BEGIN
		SET @strtableuploadname=@strmodulename +'_Upload'
	END

	IF LEN(@fieldtanggal)>0
	BEGIN
		DECLARE @querytanggal VARCHAR(MAX)
		SET @querytanggal='UPDATE '+ @strtableuploadname +' SET '
		SET @querytanggal= @querytanggal + @fieldtanggal

		 SET @querytanggal =@querytanggal +' where nawa_userid='''+ @userid +''''
	     -- PRINT @querytanggal
		 EXEC( @querytanggal)
	 END
END
GO
