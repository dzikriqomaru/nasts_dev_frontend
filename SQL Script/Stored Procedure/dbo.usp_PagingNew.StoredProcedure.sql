/****** Object:  StoredProcedure [dbo].[usp_PagingNew]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_PagingNew]  
/***********************************************************  
* Procedure description:  
* Date:   7/19/2019   
* Author: nawadata  
*  
* Changes  
* Date  Modified By   Comments  
************************************************************  
*  
************************************************************/  
(  
 @Query  VARCHAR(MAX),      
    @OrderBy    VARCHAR(max),  
    @PageIndex INT,  
    @PageSize   INT,  
 @Search  VARCHAR(MAX) = ''  
)  
AS  
BEGIN  
 IF ISNULL(@OrderBy, '') = '' SET @OrderBy = ' 1 '  
 IF (@PageSize = 0) SET @PageSize = 1  
  
 DECLARE @sql varchar(max)  
 SET @Query = REPLACE(@Query, '!Search', REPLACE(@Search, '''', ''''''))  
 SET @sql = ' declare @PageIndex int,@PageSize int' + char(10)  
   +'  set @PageIndex= '+CONVERT(VARCHAR(50),@PageIndex) +'' +CHAR(10)  
   +'  set @PageSize= '+CONVERT(VARCHAR(50),@PageSize) +'' +CHAR(10)  
         + '   '+ @Query +' ' + char(10)  
         + 'ORDER BY   '+@OrderBy  + char(10)  
         + 'OFFSET (@PageIndex - 1) * @PageSize  ROWS ' + char(10)  
         + 'FETCH NEXT @PageSize ROWS ONLY ' + char(10)  
         + ''  
 EXEC(@sql)  
END  
GO
