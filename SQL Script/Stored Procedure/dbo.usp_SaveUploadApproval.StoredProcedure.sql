/****** Object:  StoredProcedure [dbo].[usp_SaveUploadApproval]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_SaveUploadApproval]    
/***********************************************************    
* Procedure description:    
* Date:   12/7/2015     
* Author: Hendra    
*    
* Changes    
* Date  Modified By   Comments    
* 20221006	Changes INNER JOIN AuditTrail_CrossApply_ to LEFT JOIN AuditTrail_CrossApply_
* 20221006	Changes Parameter 54 to 80 because Parameter InUsed Show profile Setting
************************************************************    
*    
************************************************************/    
(    
 @Modeid INT ,@userid VARCHAR(50),@ModuleID INT   ,@roleid INT  
)    
AS    
BEGIN    
     
     
  DECLARE @sqlmajorversion INT  
    SELECT @sqlmajorversion= CONVERT(INT, SERVERPROPERTY('ProductMajorVersion'))
--declare @Modeid INT ,@userid VARCHAR(50),@ModuleID INT     
--SET @Modeid =0    
--SET @userid='operator1'    
--SET @ModuleID=  8801  
	DECLARE @bIsCreateTableUploadForEachUser AS BIT = 0
	DECLARE @strTableUploadName AS VARCHAR(500) = ''
	SELECT @bIsCreateTableUploadForEachUser = sp.SettingValue
	FROM   SystemParameter AS sp
	WHERE  sp.PK_SystemParameter_ID = 53
    
     
 DECLARE @PK_Module_ID int, @ModuleName varchar(250), @ModuleLabel varchar(250),    
        @ModuleDescription varchar(500), @IsUseDesigner bit, @IsUseApproval bit,    
        @IsSupportAdd bit, @IsSupportEdit bit, @IsSupportDelete bit,    
        @IsSupportActivation bit, @IsSupportView bit, @IsSupportUpload bit,    
        @UrlAdd varchar(550), @UrlEdit varchar(550), @UrlDelete varchar(550),    
        @UrlActivation varchar(550), @UrlView varchar(550),    
        @UrlUpload varchar(550), @UrlApproval varchar(550),    
        @UrlApprovalDetail varchar(550), @IsUseStoreProcedureValidation bit,    
        @Active bit, @CreatedBy varchar(50), @LastUpdateBy varchar(50),    
        @ApprovedBy varchar(50), @CreatedDate datetime,    
        @LastUpdateDate datetime, @ApprovedDate datetime    
     
 SELECT    
  @PK_Module_ID = PK_Module_ID, @ModuleName = ModuleName,    
  @ModuleLabel = ModuleLabel, @ModuleDescription = ModuleDescription,    
  @IsUseDesigner = IsUseDesigner, @IsUseApproval = IsUseApproval,    
  @IsSupportAdd = IsSupportAdd, @IsSupportEdit = IsSupportEdit,    
  @IsSupportDelete = IsSupportDelete,    
  @IsSupportActivation = IsSupportActivation,    
  @IsSupportView = IsSupportView, @IsSupportUpload = IsSupportUpload,    
  @UrlAdd = UrlAdd, @UrlEdit = UrlEdit, @UrlDelete = UrlDelete,    
  @UrlActivation = UrlActivation, @UrlView = UrlView,    
  @UrlUpload = UrlUpload, @UrlApproval = UrlApproval,    
  @UrlApprovalDetail = UrlApprovalDetail,    
  @IsUseStoreProcedureValidation = IsUseStoreProcedureValidation,    
  @Active = [Active], @CreatedBy = CreatedBy,    
  @LastUpdateBy = LastUpdateBy, @ApprovedBy = ApprovedBy,    
  @CreatedDate = CreatedDate, @LastUpdateDate = LastUpdateDate,    
  @ApprovedDate = ApprovedDate    
 FROM dbo.Module m    
 WHERE m.PK_Module_ID=@ModuleID    
     
     
 IF @bIsCreateTableUploadForEachUser = 1
	BEGIN
	    SET @strTableUploadName = @ModuleName + '_Upload_data_' + dbo.StripTableName(@userid)
	END
	ELSE
	BEGIN
	    SET @strTableUploadName = @ModuleName + '_Upload'
	END
DECLARE @PkAuditTrailHeaderID BIGINT     
DECLARE @querydatauplaod VARCHAR(MAX)=''  
DECLARE @primarykeyfield VARCHAR(500)    
     
   INSERT INTO AuditTrailHeader    
   (    
    -- PK_AuditTrail_ID -- this column value is auto-generated    
    CreatedDate,    
    CreatedBy,    
    ApproveBy,    
    ModuleLabel,    
    FK_ModuleAction_ID,    
    FK_AuditTrailStatus_ID,    
    PK_ModuleApproval_ID    
   )    
       
   VALUES    
   (    
   GETDATE(),    
    @userid,    
    '',    
    @ModuleLabel,    
    7,      
    3,    
    null    
   )    
       
  SET  @PkAuditTrailHeaderID=SCOPE_IDENTITY()    
      

--20221006 Insert Into Audit Trail log
 INSERT INTO AuditTrailLog    
   (    
    CreatedDate,    
    CreatedBy,  
	LastUpdateDate,
	LastUpdateBy,
    LogName,    
    LogTime,    
    LogDescription,    
    LogCode,
	LogSeverityCode
   )    
       
   VALUES    
   (    
   GETDATE(),    
    @userid,  
   GETDATE(),    
    @userid,    
    @ModuleLabel + ' - Import',    
    GETDATE(),    
    'WaitingApproval - CreatedBy: '+@userid+ '- ApprovalBy: ',      
    NEWID() ,
	0
   )    
-------------------
      
  SELECT @primarykeyfield=mf.FieldName FROM ModuleField AS mf WHERE mf.FK_Module_ID=@ModuleID AND mf.IsPrimaryKey=1    
      
DECLARE @LimitRecordNotInsertAuditTrail BIGINT=0
  --SELECT @LimitRecordNotInsertAuditTrail=sp.SettingValue
  --  FROM SystemParameter AS sp WHERE sp.PK_SystemParameter_ID=54
  SELECT @LimitRecordNotInsertAuditTrail=sp.SettingValue
    FROM SystemParameter AS sp WHERE sp.PK_SystemParameter_ID=80

	DECLARE @jmlUploadRecord BIGINT
	DECLARE @tblcountupload TABLE(jml BIGINT)
	
	INSERT INTO @tblcountupload (jml)
	EXEC('SELECT  COUNT(1) FROM '+ @strTableUploadName +' ')
	
	 SELECT @jmlUploadRecord=jml FROM @tblcountupload 
       
       
 DECLARE @sqlbefore VARCHAR(MAX)=''      
 set @sqlbefore = 'DROP  TABLE IF EXISTS '+@modulename +'_AuditTrail_'+@userid +CHAR(13)      
 set @sqlbefore =@sqlbefore+' select '+@modulename +'.* into '+@modulename +'_AuditTrail_'+@userid+'  from '+@modulename +CHAR(13)      
 set @sqlbefore =@sqlbefore+' inner join '+@strTableUploadName +' ON '+@strTableUploadName +'.'+@primarykeyfield+' = CONVERT(VARCHAR(500), '+@modulename+'.'+@primarykeyfield+')' +CHAR(13)         
 set @sqlbefore =@sqlbefore+ ' WHERE nawa_Action IN (''update'',''delete'')'      
 set @sqlbefore =@sqlbefore+ ' and  nawa_userid ='''+ @userid  +''''      
 -- PRINT @sqlbefore     
 EXEC (@sqlbefore)      
     
 DECLARE @sqlcrossApply VARCHAR(MAX)=''      
 IF @sqlmajorversion>=13
 BEGIN
 	SET @sqlcrossApply = '	DROP TABLE IF EXISTS  AuditTrail_CrossApply_'+@userid + char(10)
         + '	SELECT * INTO AuditTrail_CrossApply_'+@userid+' FROM AuditTrail_CrossApply WHERE 1=2 ' + char(10)         
         + '	CREATE INDEX IdxAuditTrail_CrossApply_'+@userid+'  ON AuditTrail_CrossApply_'+@userid+'  ' + char(10)
         + '	(FK_Module_ID,UserID) INCLUDE (primarykeydata)'
         
            
 END
 ELSE
 BEGIN
 	
 		SET @sqlcrossApply = ' IF OBJECT_ID(AuditTrail_CrossApply_'+@userid+', ''U'') IS NOT NULL  DROP TABLE AuditTrail_CrossApply_'+@userid+' ' + char(10)
         + '	SELECT * INTO AuditTrail_CrossApply_'+@userid+' FROM AuditTrail_CrossApply WHERE 1=2 ' + char(10)         
         + '	CREATE INDEX IdxAuditTrail_CrossApply_'+@userid+'  ON AuditTrail_CrossApply_'+@userid+'  ' + char(10)
         + '	(FK_Module_ID,UserID) INCLUDE (primarykeydata)'
 	END
 exec(@sqlcrossApply)      
    
    
 DECLARE @sql varchar(max)    
  SET @sql ='insert into AuditTrail_CrossApply_'+@userid+'(primarykeydata,FieldName,OldValue,FK_Module_ID,UserID)'        
      + ' SELECT xx.'+@primarykeyfield +' AS Primarykeydata, ' + char(10)      
         + '        ca.* ,'+ CONVERT(VARCHAR(50), @ModuleID ) +','''+@userid+'''  ' + char(10)      
            
         + ' FROM   ( ' + char(10)      
         + '            SELECT rc.* ' + char(10)      
         + '            FROM   '+@modulename+' AS rc ' + char(10)      
         + '                   INNER JOIN '+@modulename+'_AuditTrail_'+@userid+' ' + char(10)      
         + '                        ON  rc.'+@primarykeyfield+' = '+@modulename+'_AuditTrail_'+@userid+'.'+@primarykeyfield+' ' + char(10)      
         + '        )xx ' + char(10)      
         + '        CROSS APPLY( ' + char(10)      
         + '     VALUES'      
      
   DECLARE @queryfield VARCHAR(MAX)='', @querycleanfield VARCHAR(MAX) = ''
   DECLARE @PK_ModuleField_ID bigint, @FK_Module_ID int, @FieldName varchar(250),      
        @FieldLabel varchar(250), @Sequence int, @Required bit,      
        @IsPrimaryKey bit, @IsUnik bit, @IsShowInView bit, @IsShowInForm bit,      
        @DefaultValue varchar(max), @FK_FieldType_ID int, @SizeField int,      
        @FK_ExtType_ID int, @TabelReferenceName varchar(250),      
        @TabelReferenceNameAlias varchar(250),      
        @TableReferenceFieldKey varchar(250),      
        @TableReferenceFieldDisplayName varchar(250),      
        @TableReferenceFilter varchar(550), @IsUseRegexValidation bit,      
        @TableReferenceAdditonalJoin varchar(1000), @BCasCade bit,      
        @FieldNameParent varchar(250), @FilterCascade varchar(1000)      
         
   DECLARE my_cursor CURSOR FAST_FORWARD READ_ONLY FOR      
   SELECT PK_ModuleField_ID, FK_Module_ID, FieldName, FieldLabel,      
          Sequence, [Required], IsPrimaryKey, IsUnik, IsShowInView,      
          IsShowInForm, DefaultValue, FK_FieldType_ID, SizeField,      
          FK_ExtType_ID, TabelReferenceName, TabelReferenceNameAlias,      
          TableReferenceFieldKey, TableReferenceFieldDisplayName,      
          TableReferenceFilter, IsUseRegexValidation,      
          TableReferenceAdditonalJoin, BCasCade, FieldNameParent,      
          FilterCascade      
   FROM ModuleField WHERE FK_Module_ID=@ModuleID      
         
   OPEN my_cursor      
         
   FETCH FROM my_cursor INTO @PK_ModuleField_ID, @FK_Module_ID,      
                             @FieldName, @FieldLabel, @Sequence,      
                             @Required, @IsPrimaryKey, @IsUnik,      
                             @IsShowInView, @IsShowInForm, @DefaultValue,      
                             @FK_FieldType_ID, @SizeField, @FK_ExtType_ID,      
                             @TabelReferenceName,      
                             @TabelReferenceNameAlias,      
                             @TableReferenceFieldKey,      
                             @TableReferenceFieldDisplayName,      
                             @TableReferenceFilter, @IsUseRegexValidation,      
                             @TableReferenceAdditonalJoin, @BCasCade,      
                             @FieldNameParent, @FilterCascade      
         
   WHILE @@FETCH_STATUS = 0      
   BEGIN      
   SET @querycleanfield = @querycleanfield + @FieldName + ', '
    /*{ ... Cursor logic here ... }*/      
   IF @FK_FieldType_ID=9 
   BEGIN      
     set @queryfield=@queryfield + ' ('''+@FieldLabel+''', '+@FieldName+'),'      
   END      
         
          
   IF  (@FK_FieldType_ID  IN  ( 1,2,3,4,5,6,7,8,12,13))      
    SET @queryfield=@queryfield + ' ('''+@FieldLabel+''', TRY_CONVERT(VARCHAR(8000), '+@FieldName+')),'      
  if (@FK_FieldType_ID =10)      
   SET @queryfield=@queryfield + ' ('''+@FieldLabel+''', TRY_CONVERT(VARCHAR(8000), '+@FieldName+')),'      
        
  IF (@FK_FieldType_ID=11)      
     --SET @queryfield=@queryfield + ' ('''+@FieldLabel+''', TRY_CONVERT(VARCHAR(8000), '+@FieldName+')),'        

        SET @queryfield=@queryfield +' ('''+@FieldLabel +''', try_convert(varchar(8000), SUBSTRING( convert(varchar(8000),'+ @FieldName +'), PATINDEX(''%(%'', convert(varchar(8000),'+@FieldName+'))+1, CASE WHEN PATINDEX(''%)%'',convert(varchar(8000),'+@FieldName+'))=0 THEN LEN('+@FieldName+') else PATINDEX(''%)%'',convert(varchar(8000),'+@FieldName+'))-2 END ))),'   

       
         
    FETCH FROM my_cursor INTO @PK_ModuleField_ID, @FK_Module_ID,      
                              @FieldName, @FieldLabel, @Sequence,      
                              @Required, @IsPrimaryKey, @IsUnik,      
                              @IsShowInView, @IsShowInForm,      
                              @DefaultValue, @FK_FieldType_ID,      
                              @SizeField, @FK_ExtType_ID,                                  @TabelReferenceName,      
                              @TabelReferenceNameAlias,      
                              @TableReferenceFieldKey,      
                              @TableReferenceFieldDisplayName,      
                              @TableReferenceFilter,      
                              @IsUseRegexValidation,      
                              @TableReferenceAdditonalJoin, @BCasCade,      
                              @FieldNameParent, @FilterCascade      
   END      
         
   CLOSE my_cursor      
   DEALLOCATE my_cursor      
         
  IF LEN(@queryfield)>0 SET @queryfield=SUBSTRING(@queryfield,1,LEN(@queryfield)-1)       
IF LEN(@querycleanfield)>0 SET @querycleanfield=SUBSTRING(@querycleanfield,1,LEN(@querycleanfield)-1)         
      
 SET @querydatauplaod=@queryfield  
 SET @querydatauplaod=@querydatauplaod + ' ) AS CA(FieldName, NewValue)'       
  SET @queryfield=@queryfield+' ) AS CA(FieldName, OldValue)'       
 set @sql=@sql + @queryfield  
IF @LimitRecordNotInsertAuditTrail<=0
BEGIN

exec (@sql)

END
ELSE
	BEGIN
		IF   @jmlUploadRecord <= @LimitRecordNotInsertAuditTrail
		BEGIN
				exec (@sql)
		END 
		
	END
       
    
    
 DECLARE @sqlaudituploadperuser varchar(max)
 IF @sqlmajorversion>=13
 BEGIN
 	SET @sqlaudituploadperuser = ' DROP TABLE IF EXISTS AuditTrailDetailUpload_'+@userid+' ' + char(10)
         + ' SELECT * INTO AuditTrailDetailUpload_'+@userid+' FROM AuditTrailDetailUpload WHERE 1=2 ' + char(10)
         + ' CREATE INDEX IdxAuditTrailDetailUpload_'+@userid+' ON AuditTrailDetailUpload_'+@userid+' ' + char(10)
         + ' (nawa_action,FK_Module_ID,UserID) INCLUDE (PrimarykeyData)'
 END
 ELSE
  begin
 SET @sqlaudituploadperuser = ' IF OBJECT_ID(audittraildetailupload_'+@userid+', ''U'') IS NOT NULL  DROP TABLE audittraildetailupload_'+@userid+'  ' + char(10)
         + ' SELECT * INTO AuditTrailDetailUpload_'+@userid+' FROM AuditTrailDetailUpload WHERE 1=2 ' + char(10)
         + ' CREATE INDEX IdxAuditTrailDetailUpload_'+@userid+' ON AuditTrailDetailUpload_'+@userid+' ' + char(10)
         + ' (nawa_action,FK_Module_ID,UserID) INCLUDE (PrimarykeyData)'
 
 	END 
 
--PRINT (@sqlaudituploadperuser)
EXEC(@sqlaudituploadperuser)
    
    
DECLARE @PK_ModuleField_ID_Default     BIGINT,
	    @FieldName_Default             VARCHAR(250),
	    @FieldLabel_Default            VARCHAR(250),
	    @Sequence_Default              INT,
	    @Required_Default              BIT,
	    @IsPrimaryKey_Default          BIT,
	    @IsUnik_Default                BIT,
	    @FK_FieldType_ID_Default       INT,
	    @SizeField_Default             INT,
	    @FK_ExtType_ID_Default         INT,
		@fieldDefault               VARCHAR(MAX) = '',
		@fieldactive                VARCHAR(100) = ''
	
DECLARE my_cursor CURSOR FAST_FORWARD READ_ONLY 
FOR
	SELECT PK_ModuleField_ID,
	        FieldName,
	        FieldLabel,
	        Sequence,
	        [Required],
	        IsPrimaryKey,
	        IsUnik,
	        FK_FieldType_ID,
	        SizeField,
	        FK_ExtType_ID
	FROM   dbo.ModuleFieldDefault 
	
OPEN my_cursor 
	
FETCH FROM my_cursor INTO @PK_ModuleField_ID_Default, @FieldName_Default, @FieldLabel_Default, 
@Sequence_Default, @Required_Default, @IsPrimaryKey_Default, @IsUnik_Default, 
@FK_FieldType_ID_Default, @SizeField_Default, @FK_ExtType_ID_Default    
	
WHILE @@FETCH_STATUS = 0
BEGIN
	/*{ ... Cursor logic here ... }*/    
	    
	IF @FK_FieldType_ID_Default = 13 and @FieldName_Default = 'Active'
	BEGIN
		SET @fieldDefault = @fieldDefault + @FieldName_Default + ',' + CHAR(10) + CHAR(13)    
	    SET @fieldactive = 'isnull(' + @FieldName_Default + ',1),' + CHAR(10) + CHAR(13)
	END 	    
	    
	FETCH FROM my_cursor INTO @PK_ModuleField_ID_Default, @FieldName_Default, 
	@FieldLabel_Default, 
	@Sequence_Default, @Required_Default, @IsPrimaryKey_Default, @IsUnik_Default, 
	@FK_FieldType_ID_Default, @SizeField_Default, @FK_ExtType_ID_Default
END 
	
CLOSE my_cursor 
DEALLOCATE my_cursor    
	
	
IF LEN(@fieldDefault) > 0
	SET @fieldDefault = SUBSTRING(@fieldDefault, 1, LEN(@fieldDefault) -3)   
IF LEN(@fieldactive) > 0
	SET @fieldactive = SUBSTRING(@fieldactive, 1, LEN(@fieldactive) -3)    
DECLARE @sqldataUploadSaved varchar(max)=''    
SET @sqldataUploadSaved = 'INSERT INTO AuditTrailDetailUpload_'+@userid+' ' + char(10)      
         + '(  ' + char(10)      
         + ' FK_AuditTrailHeader_ID, ' + char(10)      
         + ' FieldName, ' + char(10)      
         + ' OldValue, ' + char(10)      
         + ' NewValue, ' + char(10)      
         + ' PrimarykeyData, ' + char(10)      
         + ' nawa_action, ' + char(10)      
         + ' FK_Module_ID, ' + char(10)      
         + ' UserID ' + char(10)      
         + ') ' + char(10)      
         + ' ' + char(10)      
         + 'SELECT '+ CONVERT(VARCHAR(50), @PkAuditTrailHeaderID )+', Fieldname,'''' AS OldValue,NewValue,xx.'+@primarykeyfield +' AS primarykeydata, nawa_action,'+ CONVERT(VARCHAR(50), @ModuleID ) +','''+ @userid +'''  FROM ( ' + char(10)      
         + 'SELECT * FROM '+ @strTableUploadName +' AS cbdu WHERE cbdu.nawa_userid='''+ @userid +''' ' + char(10)      
         + ')xx ' + char(10)      
         + 'CROSS APPLY( ' + char(10)      
         + 'values '      
    
    
    
    
    
set @sqldataUploadSaved= @sqldataUploadSaved+@querydatauplaod    
IF @LimitRecordNotInsertAuditTrail<=0
BEGIN


exec(@sqldataUploadSaved)  

END
ELSE
	BEGIN
		IF   @jmlUploadRecord <= @LimitRecordNotInsertAuditTrail
		BEGIN
				
exec(@sqldataUploadSaved)  
		END 
		
	END
    
    
    
    
DECLARE @sqlauditdetailupdatedelete varchar(max)
SET @sqlauditdetailupdatedelete = 'INSERT INTO AuditTrailDetail     ' + char(10)
         + ' (       ' + char(10)
         + '  FK_AuditTrailHeader_ID,     ' + char(10)
         + '  FieldName,     ' + char(10)
         + '  OldValue,     ' + char(10)
         + '  NewValue     ' + char(10)
         + ' )SELECT      ' + char(10)
         + ' newtable.FK_AuditTrailHeader_ID,     ' + char(10)
         + '  newtable.FieldName,     ' + char(10)
         + '  oldtable.OldValue,     ' + char(10)
         + '  newtable.NewValue     ' + char(10)
         + '  FROM AuditTrailDetailUpload_'+@userid+' newtable     ' + char(10)
         + '  LEFT JOIN AuditTrail_CrossApply_'+@userid+' oldtable     ' + char(10)
         + '  ON newtable.primarykeydata=     ' + char(10)
         + '  oldtable.primarykeydata  AND newtable.Fieldname =oldtable.FieldName  AND  oldtable.FK_Module_ID=newtable.FK_Module_ID AND oldtable.userid=newtable.UserID   ' + char(10)
         + '  WHERE (nawa_Action=''update'' OR nawa_Action=''Insert'')     ' + char(10)
         + '     AND newtable.FK_Module_ID='+ CONVERT(VARCHAR(50), @ModuleID)+' AND newtable.userid='''+@userid+'''   '
         
         
 --PRINT 'STEP1'
 --PRINT @LimitRecordNotInsertAuditTrail 
 --PRINT @jmlUploadRecord 
IF @LimitRecordNotInsertAuditTrail<=0
BEGIN
--PRINT 'STEP2'
--PRINT(@sqlauditdetailupdatedelete)
EXEC(@sqlauditdetailupdatedelete)

END
ELSE
	BEGIN
		IF   @jmlUploadRecord <= @LimitRecordNotInsertAuditTrail
		BEGIN
--		PRINT 'STEP3'
--PRINT(@sqlauditdetailupdatedelete)				
EXEC(@sqlauditdetailupdatedelete)
		END 
		
	END
             
         

     


	 
 DECLARE @sqlaudittraildelete varchar(max)
SET @sqlaudittraildelete = ' INSERT INTO AuditTrailDetail     ' + char(10)
         + ' (       ' + char(10)
         + '  FK_AuditTrailHeader_ID,     ' + char(10)
         + '  FieldName,     ' + char(10)
         + '  OldValue,     ' + char(10)
         + '  NewValue     ' + char(10)
         + ' )     ' + char(10)
         + '  select     ' + char(10)
         + '   newtable.FK_AuditTrailHeader_ID,     ' + char(10)
         + '  newtable.FieldName,     ' + char(10)
         + '  newtable.OldValue,     ' + char(10)
         + '  newtable.NewValue     ' + char(10)
         + '  FROM AuditTrailDetailUpload_'+@userid+' newtable     ' + char(10)
         + '  LEFT JOIN AuditTrail_CrossApply_'+@userid+' oldtable     ' + char(10)
         + '  ON newtable.primarykeydata=     ' + char(10)
         + '  oldtable.primarykeydata  AND newtable.Fieldname =oldtable.FieldName  AND newtable.FK_Module_ID=oldtable.FK_Module_ID  ' + char(10)
         + '  AND oldtable.UserID = newtable.UserID ' + char(10)
         + '  WHERE nawa_Action=''delete''      ' + char(10)
         + '   AND oldtable.FK_Module_ID='+CONVERT(VARCHAR(50),@ModuleID)+' AND oldtable.userid='''+@userid+'''  '
         
         
         
IF @LimitRecordNotInsertAuditTrail<=0
BEGIN


EXEC(@sqlaudittraildelete)

END
ELSE
	BEGIN
		IF   @jmlUploadRecord <= @LimitRecordNotInsertAuditTrail
		BEGIN
				
EXEC(@sqlaudittraildelete)
		END 
		
	END
             
             




IF @sqlmajorversion>=13
BEGIN
	EXEC('DROP TABLE IF EXISTS AuditTrail_CrossApply_'+@userid+'')
EXEC('DROP TABLE IF exists audittraildetailupload_'+@userid+'')
END
ELSE
	BEGIN
		EXEC( ' IF OBJECT_ID(AuditTrail_CrossApply_'+@userid+', ''U'') IS NOT NULL  DROP TABLE AuditTrail_CrossApply_'+@userid+' ')
			EXEC( ' IF OBJECT_ID(audittraildetailupload_'+@userid+', ''U'') IS NOT NULL  DROP TABLE audittraildetailupload_'+@userid+' ')
	END



IF OBJECT_ID(@strTableUploadName + '_approval', 'U') IS NULL
BEGIN
	DECLARE @sqlCreateTable VARCHAR(MAX)
	SET @sqlCreateTable = 'SELECT CAST(0 AS BIGINT) FK_ModuleApproval_ID, * INTO ' + @strTableUploadName + '_approval FROM ' + @strTableUploadName + ' WHERE 1 = 2;' + CHAR(10)
		+ '	ALTER TABLE ' + @strTableUploadName + '_approval ADD CONSTRAINT PK_' + @strTableUploadName + '_approval_ID PRIMARY KEY (PK_upload_ID);' + CHAR(10)
		+ '	CREATE INDEX Idx' + @strTableUploadName + '_approval_id  ON ' + @strTableUploadName + '_approval (FK_ModuleApproval_ID);' + CHAR(10)
		+ '	CREATE INDEX Idx' + @strTableUploadName + '_approval_userid  ON ' + @strTableUploadName + '_approval (nawa_userid);' + CHAR(10)
		+ '	CREATE INDEX Idx' + @strTableUploadName + '_approval_userid_action  ON ' + @strTableUploadName + '_approval (nawa_userid, nawa_Action);' + CHAR(10)
	EXEC(@sqlCreateTable)
END
ELSE	
BEGIN	
	DECLARE @sqlAlterTable VARCHAR(MAX) = ''	
	DECLARE @ApprovalColumnName	nvarchar(128),	
			@ApprovalDataType	nvarchar(128),	
			@ApprovalCharacterMaximumLength	int,	
			@ApprovalNumericPrecision	tinyint,	
			@ApprovalNumericScale	int,	
			@UploadColumnName	nvarchar(128),	
			@UploadDataType	nvarchar(128),	
			@UploadCharacterMaximumLength	int,	
			@UploadNumericPrecision	tinyint,	
			@UploadNumericScale	int	
	DECLARE c CURSOR FOR	
		SELECT ApprovalTable.COLUMN_NAME ApprovalColumnName, ApprovalTable.DATA_TYPE ApprovalDataType,	
			   ApprovalTable.CHARACTER_MAXIMUM_LENGTH ApprovalCharacterMaximumLength,	
			   ApprovalTable.NUMERIC_PRECISION ApprovalNumericPrecision,	
			   ApprovalTable.NUMERIC_SCALE ApprovalNumericScale,	
			   UploadTable.COLUMN_NAME UploadColumnName, UploadTable.DATA_TYPE UploadDataType,	
			   UploadTable.CHARACTER_MAXIMUM_LENGTH UploadCharacterMaximumLength,	
			   UploadTable.NUMERIC_PRECISION UploadNumericPrecision,	
			   UploadTable.NUMERIC_SCALE UploadNumericScale	
		FROM INFORMATION_SCHEMA.[COLUMNS] ApprovalTable	
			 JOIN INFORMATION_SCHEMA.[COLUMNS] UploadTable	
			 ON UploadTable.TABLE_NAME = @strTableUploadName	
				AND ApprovalTable.COLUMN_NAME = UploadTable.COLUMN_NAME	
		WHERE ApprovalTable.TABLE_NAME = @strTableUploadName + '_approval'	
			  AND ApprovalTable.COLUMN_NAME <> 'FK_ModuleApproval_ID'	
			  AND (	
				ApprovalTable.DATA_TYPE <> UploadTable.DATA_TYPE	
				OR ApprovalTable.CHARACTER_MAXIMUM_LENGTH <> UploadTable.CHARACTER_MAXIMUM_LENGTH	
			  )	
		UNION	
		SELECT ApprovalTable.COLUMN_NAME ApprovalColumnName, ApprovalTable.DATA_TYPE ApprovalDataType,	
			   ApprovalTable.CHARACTER_MAXIMUM_LENGTH ApprovalCharacterMaximumLength,	
			   ApprovalTable.NUMERIC_PRECISION ApprovalNumericPrecision,	
			   ApprovalTable.NUMERIC_SCALE ApprovalNumericScale,	
			   UploadTable.COLUMN_NAME UploadColumnName, UploadTable.DATA_TYPE UploadDataType,	
			   UploadTable.CHARACTER_MAXIMUM_LENGTH UploadCharacterMaximumLength,	
			   UploadTable.NUMERIC_PRECISION UploadNumericPrecision,	
			   UploadTable.NUMERIC_SCALE UploadNumericScale	
		FROM INFORMATION_SCHEMA.[COLUMNS] ApprovalTable	
			 LEFT JOIN INFORMATION_SCHEMA.[COLUMNS] UploadTable	
			 ON UploadTable.TABLE_NAME = @strTableUploadName	
				AND ApprovalTable.COLUMN_NAME = UploadTable.COLUMN_NAME	
		WHERE ApprovalTable.TABLE_NAME = @strTableUploadName + '_approval'	
			  AND ApprovalTable.COLUMN_NAME <> 'FK_ModuleApproval_ID'	
			  AND UploadTable.COLUMN_NAME IS NULL	
		UNION	
		SELECT ApprovalTable.COLUMN_NAME ApprovalColumnName, ApprovalTable.DATA_TYPE ApprovalDataType,	
			   ApprovalTable.CHARACTER_MAXIMUM_LENGTH ApprovalCharacterMaximumLength,	
			   ApprovalTable.NUMERIC_PRECISION ApprovalNumericPrecision,	
			   ApprovalTable.NUMERIC_SCALE ApprovalNumericScale,	
			   UploadTable.COLUMN_NAME UploadColumnName, UploadTable.DATA_TYPE UploadDataType,	
			   UploadTable.CHARACTER_MAXIMUM_LENGTH UploadCharacterMaximumLength,	
			   UploadTable.NUMERIC_PRECISION UploadNumericPrecision,	
			   UploadTable.NUMERIC_SCALE UploadNumericScale	
		FROM INFORMATION_SCHEMA.[COLUMNS] UploadTable	
			 LEFT JOIN INFORMATION_SCHEMA.[COLUMNS] ApprovalTable	
			 ON ApprovalTable.TABLE_NAME = @strTableUploadName + '_approval'	
				AND ApprovalTable.COLUMN_NAME = UploadTable.COLUMN_NAME	
				AND ApprovalTable.COLUMN_NAME <> 'FK_ModuleApproval_ID'	
		WHERE UploadTable.TABLE_NAME = @strTableUploadName	
			  AND ApprovalTable.COLUMN_NAME IS NULL	
	OPEN c	
	FETCH NEXT FROM c INTO @ApprovalColumnName, @ApprovalDataType, @ApprovalCharacterMaximumLength, @ApprovalNumericPrecision, @ApprovalNumericScale,	
						   @UploadColumnName, @UploadDataType, @UploadCharacterMaximumLength, @UploadNumericPrecision, @UploadNumericScale	
	WHILE @@FETCH_STATUS = 0	
	BEGIN	
		IF @UploadColumnName IS NULL	
		BEGIN	
			SET @sqlAlterTable = 'ALTER TABLE ' + @strTableUploadName + '_approval' + CHAR(10) +	
								 'DROP COLUMN ' + @ApprovalColumnName	
		END	
		ELSE IF @ApprovalColumnName IS NULL	
		BEGIN	
			SET @sqlAlterTable = 'ALTER TABLE ' + @strTableUploadName + '_approval' + CHAR(10) +	
								 'ADD ' + @UploadColumnName + ' ' + @UploadDataType +	
								 CASE WHEN @UploadDataType IN ('varchar', 'varbinary') THEN '(' + CASE WHEN @UploadCharacterMaximumLength < 0 THEN 'MAX' ELSE CAST(@UploadCharacterMaximumLength AS VARCHAR) END + ')' ELSE '' END +	
								 ' NULL'	
		END	
		ELSE	
		BEGIN	
			SET @sqlAlterTable = 'ALTER TABLE ' + @strTableUploadName + '_approval' + CHAR(10) +	
								 'ALTER COLUMN ' + @UploadColumnName + ' ' + @UploadDataType +	
								 CASE WHEN @UploadDataType IN ('varchar', 'varbinary') THEN '(' + CASE WHEN @UploadCharacterMaximumLength < 0 THEN 'MAX' ELSE CAST(@UploadCharacterMaximumLength AS VARCHAR) END + ')' ELSE '' END +	
								 ' NULL'	
		END	
		EXEC(@sqlAlterTable)	
		FETCH NEXT FROM c INTO @ApprovalColumnName, @ApprovalDataType, @ApprovalCharacterMaximumLength, @ApprovalNumericPrecision, @ApprovalNumericScale,	
						   @UploadColumnName, @UploadDataType, @UploadCharacterMaximumLength,	
						   @UploadNumericPrecision, @UploadNumericScale	
	END	
	CLOSE c	
	DEALLOCATE c	
END

 DECLARE @sqlfinal varchar(max)    
SET @sqlfinal = ' ' + char(10)    
         --+ 'DECLARE @xmldata VARCHAR(MAX) ' + char(10)      
         --+ ' set @xmldata=  ' + char(10)      
         --+ ' (  ' + char(10)      
         --+ 'SELECT   ' + char(10)      
         --+ ' * ' + char(10)      
         --+ 'FROM ' + char(10)      
         --+ ' '+@strTableUploadName +' ' + char(10)      
         --+ ' WHERE  '+@strTableUploadName +'.nawa_userid='''+@userid+''' AND '+@strTableUploadName +'.KeteranganError=''''  ' + char(10)      
         --+ 'FOR XML AUTO ,ELEMENTS ,ROOT(''DataUpload'')) ' + char(10)      
         --+ '  ' + char(10)      
         --+ '  ' + char(10)      
         --+ '  ' + char(10)      
         --+ '  ' + char(10)      
         --+ '  ' + char(10)      
         + ' --insert table module_approval ' + char(10)      
         + ' INSERT INTO ModuleApproval ' + char(10)      
         + ' ( ' + char(10)      
         + '  -- PK_ModuleApproval_ID -- this column value is auto-generated ' + char(10)      
         + '  ModuleName, ' + char(10)      
         + '  ModuleKey, ' + char(10)      
         + '  ModuleField, ' + char(10)      
         + '  ModuleFieldBefore, ' + char(10)      
         + '  PK_ModuleAction_ID, ' + char(10)      
         + '  CreatedDate, ' + char(10)      
         + '  CreatedBy, ' + char(10)      
         + '  FK_Module_ID, ' + char(10)      
         + '  FK_MRole_ID ' + char(10)      
         + ' ) ' + char(10)      
         + ' VALUES ' + char(10)      
         + ' ( ' + char(10)      
         + '  '''+@ModuleName+''', ' + char(10)      
         + '  '+ CONVERT(VARCHAR(10),@Modeid)  +', ' + char(10)      
         --+ '  @xmldata, ' + char(10)      
		 + '  '''', ' + char(10)      
         + '  '''', ' + char(10)      
         + '  7, ' + char(10)      
         + '  getdate(), ' + char(10)      
         + '  '''+@userid+''', ' + char(10)      
         + '  '+CAST(@ModuleID AS VARCHAR)+', ' + char(10)      
         + '  '+CAST(@roleid AS VARCHAR)+' ' + char(10)      
         + ' ) ' + char(10)      
         + ' DECLARE @PK_ModuleApproval_ID BIGINT = SCOPE_IDENTITY() ' + char(10)     
         + ' INSERT INTO ' + @strTableUploadName + '_approval(FK_ModuleApproval_ID, nawa_userid, nawa_recordnumber, KeteranganError, nawa_Action, ' + @querycleanfield + CASE WHEN @fieldDefault <> '' THEN ', ' + @fieldDefault ELSE '' END + ') ' + char(10)
 
    
         + ' SELECT @PK_ModuleApproval_ID, nawa_userid, nawa_recordnumber, KeteranganError, nawa_Action, ' + @querycleanfield + CASE WHEN @fieldactive <> '' THEN ', ' + @fieldactive ELSE '' END + ' ' + char(10)     
         + ' FROM '+@strTableUploadName +' A ' + char(10)      
         + ' WHERE A.nawa_userid = '''+@userid+''' ' + char(10)      
         + '	   AND A.KeteranganError = '''' ' + char(10)      
         + ' ORDER BY A.PK_upload_ID ' + char(10)      
         + ' DELETE FROM ' + @strTableUploadName + ' where nawa_userid='''+@userid+''' '+ char(10)
		 + ' SELECT @PK_ModuleApproval_ID AS PK_ModuleApproval_ID, ' + CAST(@PkAuditTrailHeaderID AS varchar) + ' AS AuditTrailHeaderID'      
EXEC(@sqlfinal)    
    
END

GO
