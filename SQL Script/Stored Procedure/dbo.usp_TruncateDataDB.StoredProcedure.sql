/****** Object:  StoredProcedure [dbo].[usp_TruncateDataDB]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[usp_TruncateDataDB]
/***********************************************************
* Procedure description:
* Date:   08/08/2016 
* Author: Hendra
*
* Changes
* Date		Modified By			Comments
************************************************************
*
************************************************************/
(@PK_EODTaskDetailLog_ID bigint)
AS
BEGIN


-- update EODTaskDetailLog se execution kalau process yg jalanin tasknya semuanya SSIS
UPDATE EODTaskDetailLog
SET
    executionID = @PK_EODTaskDetailLog_ID WHERE PK_EODTaskDetailLog_ID=@PK_EODTaskDetailLog_ID 
    
    
    	
INSERT INTO EODLogSP
(
	-- PK_EODLogSP_ID -- this column value is auto-generated
	executionID,
	ProcessDate,
	Process,
	Keterangan
)	
SELECT @PK_EODTaskDetailLog_ID ,GETDATE(),'Process House Keeping Data DB ','Process Truncate Data Db Started '

	
DBCC SHRINKFILE (N'NawaData' , 0, TRUNCATEONLY)




INSERT INTO EODLogSP
(
	-- PK_EODLogSP_ID -- this column value is auto-generated
	executionID,
	ProcessDate,
	Process,
	Keterangan
)	
SELECT @PK_EODTaskDetailLog_ID ,GETDATE(),'Process House Keeping Data DB ','Process Truncate Data Db Ended '


END
GO
