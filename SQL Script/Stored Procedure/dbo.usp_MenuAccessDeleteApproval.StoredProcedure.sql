/****** Object:  StoredProcedure [dbo].[usp_MenuAccessDeleteApproval]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_MenuAccessDeleteApproval]
/***********************************************************
* Procedure description:
* Date:   11/9/2019 
* Author: nawadata
*
* Changes
* Date		Modified By			Comments
************************************************************
*
************************************************************/
(
	@groupmenuid INT,@modulename VARCHAR(500),@modulekey VARCHAR(500),@Modulefield VARCHAR(MAX),@ModuleFieldbefore VARCHAR(MAX),@pkmoduleaction INT,@createdby VARCHAR(50)
	
)

                        
AS
BEGIN
	
	--insert ke moduleapproval
	DECLARE @PKModuleapprovalid bigint
	INSERT INTO ModuleApproval
	(
		-- PK_ModuleApproval_ID -- this column value is auto-generated
		ModuleName,
		ModuleKey,
		ModuleField,
		ModuleFieldBefore,
		PK_ModuleAction_ID,
		CreatedDate,
		CreatedBy
	)
	VALUES
	(
		@modulename,
		@modulekey,
		@Modulefield,
		@ModuleFieldbefore,
		@pkmoduleaction,
		GETDATE(),
		@createdby
		
		
	)
	
	SET @PKModuleapprovalid=SCOPE_IDENTITY()
	
	--insert ke audittrailheader
	
	DECLARE @pkaudittrailheader BIGINT
	DECLARE @modulelabel VARCHAR(500)= (SELECT ModuleLabel
	                                     FROM module WHERE ModuleName=@modulename)
	INSERT INTO AuditTrailHeader
	(
		-- PK_AuditTrail_ID -- this column value is auto-generated
		CreatedDate,
		CreatedBy,
		ApproveBy,
		ModuleLabel,
		FK_ModuleAction_ID,
		FK_AuditTrailStatus_ID,
		PK_ModuleApproval_ID
	)
	VALUES
	(
		GETDATE(),
		@createdby,
		@createdby,
		@modulelabel,
		@pkmoduleaction,
		3,
		@PKModuleapprovalid
	)
	
	set @pkaudittrailheader=SCOPE_IDENTITY()
	--insert ke audittraildetail
	
	
	INSERT INTO AuditTrailDetail
	(
		-- PK_AuditTrailDetail_id -- this column value is auto-generated
		FK_AuditTrailHeader_ID,
		FieldName,
		OldValue,
		NewValue
	)
	
SELECT @pkAudittrailheader FK_AuditTrailHeader_ID,ca.fieldname,ca.OldValue,'' NewValue  FROM (  
SELECT * FROM MGroupMenuAccess AS mma WHERE mma.FK_GroupMenu_ID=@groupmenuid  
)xx   
CROSS APPLY (  
VALUES   
 ('PK_MGroupMenuAcess_ID', try_convert(varchar(8000),PK_MGroupMenuAcess_ID)),  
('FK_GroupMenu_ID', try_convert(varchar(8000),FK_GroupMenu_ID)),  
('FK_Module_ID', try_convert(varchar(8000),FK_Module_ID)),  
('bAdd', try_convert(varchar(8000),bAdd)),  
('bEdit', try_convert(varchar(8000),bEdit)),  
('bDelete', try_convert(varchar(8000),bDelete)),  
('bActivation', try_convert(varchar(8000),bActivation)),  
('bView', try_convert(varchar(8000),bView)),  
('bApproval', try_convert(varchar(8000),bApproval)),  
('bUpload', try_convert(varchar(8000),bUpload)),  
('bDetail', try_convert(varchar(8000),bDetail))    
)ca (Fieldname,OldValue)  
  
  
UNION all  
  
SELECT @pkAudittrailheader FK_AuditTrailHeader_ID,ca.fieldname,ca.OldValue,'' NewValue  FROM (  
SELECT * FROM MGroupMenuSettting  AS mma WHERE mma.FK_MGroupMenu_ID=@groupmenuid  
)xx   
CROSS APPLY (  
VALUES   
 ('PK_MGroupMenuSettting_ID', try_convert(varchar(8000),PK_MGroupMenuSettting_ID)),  
('FK_MGroupMenu_ID', try_convert(varchar(8000),FK_MGroupMenu_ID)),  
('mMenuID', try_convert(varchar(8000),mMenuID)),  
('mMenuLabel', try_convert(varchar(8000),mMenuLabel)),  
('mMenuParentID', try_convert(varchar(8000),mMenuParentID)),  
('mMenuURL', try_convert(varchar(8000),mMenuURL)),  
('FK_Module_ID', try_convert(varchar(8000),FK_Module_ID)),  
('FK_Action_ID', try_convert(varchar(8000),FK_Action_ID)),  
('IconName', try_convert(varchar(8000),IconName)),  
('urutan', try_convert(varchar(8000),urutan))  
)ca (Fieldname,OldValue)  
  
	
	
	
END
GO
