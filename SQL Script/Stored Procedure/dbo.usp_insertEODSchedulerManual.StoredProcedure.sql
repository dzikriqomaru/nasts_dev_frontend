/****** Object:  StoredProcedure [dbo].[usp_insertEODSchedulerManual]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_insertEODSchedulerManual]
/***********************************************************
* Procedure description:
* Date:   28/01/2016 
* Author: Hendra
*
* Changes
* Date			Modified By		Comments
* 16/08/2021	Fauzan			Adding AuditTrail
* 05/10/2022	Davin			Adding AuditTrailLog
************************************************************
*
************************************************************/
(
    @dataDate	DATETIME,
    @processID	AS BIGINT,
	@taskID		VARCHAR(MAX),
    @userID		VARCHAR(50)
)
AS
BEGIN
	DECLARE @tabletasklist TABLE(taskid VARCHAR(MAX))
	INSERT into @tabletasklist(taskid) VALUES (@taskID)

	INSERT INTO EODSchedulerLog
	(
		FK_EODSchedulerID,
		ExecuteBy,
		DataDate,
		ProcessDate,
		StartDate,
		Enddate,
		ErrorMessage,
		FK_MsEODStatus_ID
	)
	VALUES
	(
		@processID,
		@userID,
		@dataDate,
		GETDATE(),
		NULL,
		NULL,
		'',
		1
	)

	DECLARE @PK_EODSchedulerLog_ID BIGINT
	SELECT @PK_EODSchedulerLog_ID = SCOPE_IDENTITY()
	
	--Audit Trail
	DECLARE @PKAuditTrailHeader BIGINT
	INSERT INTO AuditTrailHeader
	  (
	    CreatedDate,
	    CreatedBy,
	    ApproveBy,
	    ModuleLabel,
	    FK_ModuleAction_ID,
	    FK_AuditTrailStatus_ID,
	    PK_ModuleApproval_ID
	  )
	VALUES
	  (
	    GETDATE(),
	    @userID,
	    NULL,
	    'EODSchedulerLog',
	    1,
	    1,
	    0
	  )
	
	SET @PKAuditTrailHeader = SCOPE_IDENTITY()

	INSERT INTO AuditTrailLog    
	   (    
		CreatedDate,    
		CreatedBy,  
		LastUpdateDate,
		LastUpdateBy,
		LogName,    
		LogTime,    
		LogDescription,    
		LogCode   
	   )    
    VALUES    
	   (    
		GETDATE(),    
		@userid,  
		GETDATE(),    
		@userid,    
		'EODSchedulerLog - Insert' ,    
		GETDATE(),    
		'Affected To Database - CreatedBy: '+@userid+ ' - ApprovalBy: '+@userid,      
		NEWID()   
	   )    

	INSERT INTO AuditTrailDetail
	(
		FK_AuditTrailHeader_ID,
		FieldName,
		OldValue,
		NewValue
	)
	SELECT 
		@pkAudittrailheader AS FK_AuditTrailHeader_ID,
	    ca.fieldname,
	    '' AS OldValue,
	    ca.NewValue
	FROM
	(
		SELECT *
		FROM   EODSchedulerLog AS EOD
		WHERE  EOD.PK_EODSchedulerLog_ID = @PK_EODSchedulerLog_ID
	) xx
	CROSS APPLY 
	(
		VALUES 
	    ('PK_EODSchedulerLog_ID', TRY_CONVERT(VARCHAR(8000), PK_EODSchedulerLog_ID)),
	    ('FK_EODSchedulerID', TRY_CONVERT(VARCHAR(8000), FK_EODSchedulerID)),
	    ('ExecuteBy', TRY_CONVERT(VARCHAR(8000), ExecuteBy)),
		('DataDate', TRY_CONVERT(VARCHAR(8000), DataDate)),
	    ('ProcessDate', TRY_CONVERT(VARCHAR(8000), ProcessDate)),
	    ('FK_MsEODStatus_ID', TRY_CONVERT(VARCHAR(8000), FK_MsEODStatus_ID))
	) ca (Fieldname, NewValue)

	DECLARE @PK_EODSchedulerDetail_ID     BIGINT,
	        @FK_EODSCheduler_ID           BIGINT,
	        @FK_EODTask_ID                BIGINT,
	        @OrderNo                      INT,
	        @CreatedBy                    VARCHAR(50),
	        @LastUpdateBy                 VARCHAR(50),
	        @ApprovedBy                   VARCHAR(50),
	        @CreatedDate                  DATETIME,
	        @LastUpdateDate               DATETIME,
	        @ApprovedDate                 DATETIME
	
	DECLARE my_cursor1 CURSOR FAST_FORWARD READ_ONLY 
	FOR
    SELECT 
		PK_EODSchedulerDetail_ID,
		FK_EODSCheduler_ID,
		FK_EODTask_ID,
		OrderNo,
		CreatedBy,
		LastUpdateBy,
		ApprovedBy,
		CreatedDate,
		LastUpdateDate,
		ApprovedDate
	FROM   dbo.EODSchedulerDetail ed
	WHERE  ed.FK_EODSCheduler_ID = @processID AND ed.FK_EODTask_ID IN (	    
		SELECT LTRIM(RTRIM(m.n.value('.[1]','varchar(8000)'))) AS tasklist
		FROM
		(
			SELECT CAST('<XMLRoot><RowData>' + REPLACE(taskid,',','</RowData><RowData>') + '</RowData></XMLRoot>' AS XML) AS x
			FROM @tabletasklist
		) t
		CROSS APPLY x.nodes('/XMLRoot/RowData')m(n)	
	)
	ORDER BY ed.OrderNo
	
	OPEN my_cursor1
	
	FETCH FROM my_cursor1 INTO @PK_EODSchedulerDetail_ID, @FK_EODSCheduler_ID,
	@FK_EODTask_ID, @OrderNo, @CreatedBy,
	@LastUpdateBy, @ApprovedBy, @CreatedDate,
	@LastUpdateDate, @ApprovedDate
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
		INSERT INTO EODTaskLog
		(
			EODSchedulerLogID,
			FK_EODTaskID,
			ExecuteBy,
			StartDate,
			Enddate,
			ErrorMessage,
			FK_MsEODStatus_ID
		)
	    VALUES
		(
			@PK_EODSchedulerLog_ID,
			@FK_EODTask_ID,
			@userID,
			NULL,
			NULL,
			'',
			1
		)
		
	    DECLARE @PK_EODTaskLog_ID BIGINT
	    SELECT @PK_EODTaskLog_ID = SCOPE_IDENTITY()	    
	    
		--Audit Trail
		INSERT INTO AuditTrailHeader
		(
			CreatedDate,
			CreatedBy,
			ApproveBy,
			ModuleLabel,
			FK_ModuleAction_ID,
			FK_AuditTrailStatus_ID,
			PK_ModuleApproval_ID
		)
		VALUES
		(
			GETDATE(),
			@userID,
			NULL,
			'EODTaskLog',
			1,
			1,
			0
		)
	
		SET @PKAuditTrailHeader = SCOPE_IDENTITY()

		INSERT INTO AuditTrailDetail
		(
			FK_AuditTrailHeader_ID,
			FieldName,
			OldValue,
			NewValue
		)
		SELECT 
			@pkAudittrailheader AS FK_AuditTrailHeader_ID,
			ca.fieldname,
			'' AS OldValue,
			ca.NewValue
		FROM
		(
			SELECT *
			FROM   EODTaskLog AS EOD
			WHERE  EOD.PK_EODTaskLog_ID = @PK_EODTaskLog_ID
		) xx
		CROSS APPLY 
		(
			VALUES 
			('PK_EODTaskLog_ID', TRY_CONVERT(VARCHAR(8000), PK_EODTaskLog_ID)),
			('EODSchedulerLogID', TRY_CONVERT(VARCHAR(8000), EODSchedulerLogID)),
			('FK_EODTaskID', TRY_CONVERT(VARCHAR(8000), FK_EODTaskID)),
			('ExecuteBy', TRY_CONVERT(VARCHAR(8000), ExecuteBy)),
			('FK_MsEODStatus_ID', TRY_CONVERT(VARCHAR(8000), FK_MsEODStatus_ID))
		) ca (Fieldname, NewValue)

	    DECLARE @PK_EODTaskDetail_ID		BIGINT,
	            @FK_EODTaskDetailType_ID	INT,
	            @SSISName					VARCHAR(250),
	            @SSISFIle					VARBINARY(MAX),
	            @StoreProcedureName			VARCHAR(250),
	            @Keterangan					VARCHAR(8000),
	            @IsUseParameterProcessDate	BIT
	    
	    DECLARE my_cursor2 CURSOR FAST_FORWARD READ_ONLY 
	    FOR
	        SELECT PK_EODTaskDetail_ID,
	               FK_EODTaskDetailType_ID,
	               SSISName,
	               SSISFIle,
	               StoreProcedureName,
	               Keterangan,
	               IsUseParameterProcessDate
	        FROM   dbo.EODTaskDetail ed
	        WHERE  ed.FK_EODTask_ID = @FK_EODTask_ID
	        ORDER BY ed.OrderNo
	    
	    OPEN my_cursor2
	    
	    FETCH FROM my_cursor2 INTO @PK_EODTaskDetail_ID, 
	    @FK_EODTaskDetailType_ID, @SSISName,
	    @SSISFIle, @StoreProcedureName, @Keterangan,
	    @IsUseParameterProcessDate
	    
	    WHILE @@FETCH_STATUS = 0
	    BEGIN
			INSERT INTO EODTaskDetailLog
			(
				EODSchedulerLogID,
				EODTaskLogID,
				FK_EODTAskDetail_ID,
				ExecuteBy,
				StartDate,
				Enddate,
				ErrorMessage,
				FK_MsEODStatus_ID
			)
	        VALUES
			(
				@PK_EODSchedulerLog_ID,
				@PK_EODTaskLog_ID,
				@PK_EODTaskDetail_ID,
				@userID,
				NULL,
				NULL,
				'',
				1
			)

			DECLARE @PK_EODTaskDetailLog_ID BIGINT
			SELECT @PK_EODTaskDetailLog_ID = SCOPE_IDENTITY()	 

			--Audit Trail
			INSERT INTO AuditTrailHeader
			(
				CreatedDate,
				CreatedBy,
				ApproveBy,
				ModuleLabel,
				FK_ModuleAction_ID,
				FK_AuditTrailStatus_ID,
				PK_ModuleApproval_ID
			)
			VALUES
			(
				GETDATE(),
				@userID,
				NULL,
				'EODTaskDetailLog',
				1,
				1,
				0
			)
	
			SET @PKAuditTrailHeader = SCOPE_IDENTITY()

			INSERT INTO AuditTrailDetail
			(
				FK_AuditTrailHeader_ID,
				FieldName,
				OldValue,
				NewValue
			)
			SELECT 
				@pkAudittrailheader AS FK_AuditTrailHeader_ID,
				ca.fieldname,
				'' AS OldValue,
				ca.NewValue
			FROM
			(
				SELECT *
				FROM   EODTaskDetailLog AS EOD
				WHERE  EOD.PK_EODTaskDetailLog_ID = @PK_EODTaskDetailLog_ID
			) xx
			CROSS APPLY 
			(
				VALUES 
				('PK_EODTaskDetailLog_ID', TRY_CONVERT(VARCHAR(8000), PK_EODTaskDetailLog_ID)),
				('EODSchedulerLogID', TRY_CONVERT(VARCHAR(8000), EODSchedulerLogID)),
				('EODTaskLogID', TRY_CONVERT(VARCHAR(8000), EODTaskLogID)),
				('FK_EODTAskDetail_ID', TRY_CONVERT(VARCHAR(8000), FK_EODTAskDetail_ID)),
				('ExecuteBy', TRY_CONVERT(VARCHAR(8000), ExecuteBy)),
				('FK_MsEODStatus_ID', TRY_CONVERT(VARCHAR(8000), FK_MsEODStatus_ID))
			) ca (Fieldname, NewValue)

	        FETCH FROM my_cursor2 INTO @PK_EODTaskDetail_ID, 
	        @FK_EODTaskDetailType_ID, @SSISName,
	        @SSISFIle, @StoreProcedureName, @Keterangan,
	        @IsUseParameterProcessDate
	    END
	    
	    CLOSE my_cursor2
	    DEALLOCATE my_cursor2

	    FETCH FROM my_cursor1 INTO @PK_EODSchedulerDetail_ID,
	    @FK_EODSCheduler_ID, @FK_EODTask_ID, @OrderNo,
	    @CreatedBy, @LastUpdateBy, @ApprovedBy,
	    @CreatedDate, @LastUpdateDate, @ApprovedDate
	END
	
	CLOSE my_cursor1
	DEALLOCATE my_cursor1
END
GO
