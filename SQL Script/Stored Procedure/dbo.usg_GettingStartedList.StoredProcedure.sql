/****** Object:  StoredProcedure [dbo].[usg_GettingStartedList]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usg_GettingStartedList]
/***********************************************************
* Procedure description:
* Date:   7/21/2017 
* Author: Hendra
*
* Changes
* Date		Modified By			Comments
************************************************************
*
************************************************************/
(
	@groupid int
)
AS
BEGIN
	
	

SELECT gs.PK_GettingStarted_ID,
       gs.Sequence,
       gs.StartedName,
       CAST('' AS XML).value('xs:base64Binary(sql:column("gs.Icon"))', 'varchar(max)') AS 
       Iconfile,
       gs.IconName,
       gs.FK_Module_ID AS ModuleID,
       gs.FK_ModuleAction_ID AS ModuleAction,
       xx.Urldata
       
FROM   GettingStarted gs
INNER JOIN (
	
SELECT mma.FK_Module_ID, CASE WHEN  mma.bAdd=1 THEN 1 ELSE -1 END AS Access,m.UrlAdd  AS Urldata
  FROM MGroupMenuAccess mma
  INNER JOIN Module m ON mma.FK_Module_ID=m.PK_Module_ID
WHERE mma.FK_GroupMenu_ID=@groupid
UNION
SELECT mma.FK_Module_ID, CASE WHEN  mma.bview=1 THEN 5 ELSE -1 END AS Access ,m.urlview
  FROM MGroupMenuAccess mma
  INNER JOIN Module m ON mma.FK_Module_ID=m.PK_Module_ID
WHERE mma.FK_GroupMenu_ID=@groupid
UNION
SELECT mma.FK_Module_ID, CASE WHEN  mma.bapproval=1 THEN 6 ELSE -1 END AS Access ,m.UrlApproval
  FROM MGroupMenuAccess mma
  INNER JOIN Module m ON mma.FK_Module_ID=m.PK_Module_ID
WHERE mma.FK_GroupMenu_ID=@groupid
 	 UNION
SELECT mma.FK_Module_ID, CASE WHEN  mma.bupload=1 THEN 7 ELSE -1 END AS Access ,m.UrlUpload
  FROM MGroupMenuAccess mma
  INNER JOIN Module m ON mma.FK_Module_ID=m.PK_Module_ID
WHERE mma.FK_GroupMenu_ID=@groupid
)xx ON gs.FK_Module_ID=xx.FK_Module_ID
and gs.FK_ModuleAction_ID= xx.Access
WHERE  gs.[Active] = 1 ORDER BY gs.Sequence

	
	
END
GO
