/****** Object:  StoredProcedure [dbo].[usp_Deactivate_Alternate]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Johan Peterson
-- Create date: 2017-09-24
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[usp_Deactivate_Alternate] 
	-- Add the parameters for the stored procedure here
	@UserEmailAdress VARCHAR(255) , 
	@UserAlternateEmailAdress VARCHAR(255) ,
	@ActionResponse VARCHAR(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF lower(@ActionResponse)='yes' 
	BEGIN
		DECLARE @UserID AS VARCHAR(50)
		DECLARE @UserAlternateID AS VARCHAR(50)

		SELECT TOP 1 @UserID=UserId FROM MUser
		WHERE UserEmailAddress=@UserEmailAdress

		SELECT TOP 1 @UserAlternateID=UserId FROM MUser
		WHERE UserEmailAddress=@UserAlternateEmailAdress

		UPDATE Alternate
		SET Active=0
		WHERE UserID=@UserID AND UserAlternate=@UserAlternateID

		SELECT 1
	END
	ELSE
	BEGIN
		SELECT 0
	END
END
GO
