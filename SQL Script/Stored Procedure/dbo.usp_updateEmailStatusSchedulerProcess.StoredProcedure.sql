/****** Object:  StoredProcedure [dbo].[usp_updateEmailStatusSchedulerProcess]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_updateEmailStatusSchedulerProcess]
/***********************************************************
* Procedure description:
* Date:   24/02/2016 
* Author: Hendra
*
* Changes
* Date		Modified By			Comments
************************************************************
*
************************************************************/
(
	@PK_EmailTemplateSchedulerDetail_ID BIGINT,@FkStatusemailid INT,@errorMessage VARCHAR(MAX)
)
AS
BEGIN
	
	
	IF @FkStatusemailid=3
	BEGIN
		UPDATE EmailTemplateSchedulerDetail SET retrycount =retrycount+1, SendEmailDate = GETDATE(),  FK_EmailStatus_ID = @FkStatusemailid,ErrorMessage = @errorMessage  WHERE PK_EmailTemplateSchedulerDetail_ID= @PK_EmailTemplateSchedulerDetail_ID
	
		
		
	END
	ELSE
		BEGIN
			UPDATE EmailTemplateSchedulerDetail SET SendEmailDate = GETDATE(),  FK_EmailStatus_ID = @FkStatusemailid,ErrorMessage = @errorMessage  WHERE PK_EmailTemplateSchedulerDetail_ID= @PK_EmailTemplateSchedulerDetail_ID
		END
	
	
	DECLARE @jmlinprocess INT=0
	DECLARE @jmlSucess INT =0
	DECLARE @jmlfail INT =0
	DECLARE @jmlBounce INT=0
	
	
		SELECT
		
			@jmlinprocess=COUNT(1) 
		FROM
			EmailTemplateScheduler ets WHERE ets.PK_EmailTemplateScheduler_ID IN (
			SELECT etsd.FK_EmailTEmplateScheduler_ID
			  FROM EmailTemplateSchedulerDetail etsd WHERE etsd.PK_EmailTemplateSchedulerDetail_ID=@PK_EmailTemplateSchedulerDetail_ID
			  AND etsd.FK_EmailStatus_ID=3	
			) 
		GROUP BY ets.FK_EmailStatus_ID
		
		
		
		
			SELECT
		
			@jmlSucess=COUNT(1) 
		FROM
			EmailTemplateScheduler ets WHERE ets.PK_EmailTemplateScheduler_ID IN (
			SELECT etsd.FK_EmailTEmplateScheduler_ID
			  FROM EmailTemplateSchedulerDetail etsd WHERE etsd.PK_EmailTemplateSchedulerDetail_ID=@PK_EmailTemplateSchedulerDetail_ID
			  AND etsd.FK_EmailStatus_ID=4	
			) 
		GROUP BY ets.FK_EmailStatus_ID
		
		
			SELECT
		
			@jmlfail=COUNT(1) 
		FROM
			EmailTemplateScheduler ets WHERE ets.PK_EmailTemplateScheduler_ID IN (
			SELECT etsd.FK_EmailTEmplateScheduler_ID
			  FROM EmailTemplateSchedulerDetail etsd WHERE etsd.PK_EmailTemplateSchedulerDetail_ID=@PK_EmailTemplateSchedulerDetail_ID
			  AND etsd.FK_EmailStatus_ID=5	
			) 
		GROUP BY ets.FK_EmailStatus_ID
		
		
		
		SELECT
		
			@jmlBounce=COUNT(1) 
		FROM
			EmailTemplateScheduler ets WHERE ets.PK_EmailTemplateScheduler_ID IN (
			SELECT etsd.FK_EmailTEmplateScheduler_ID
			  FROM EmailTemplateSchedulerDetail etsd WHERE etsd.PK_EmailTemplateSchedulerDetail_ID=@PK_EmailTemplateSchedulerDetail_ID
			  AND etsd.FK_EmailStatus_ID=6
			) 
		GROUP BY ets.FK_EmailStatus_ID
		
	IF @jmlfail=0 AND @jmlinprocess=0 AND @jmlBounce=0
	BEGIN
		--sucess
		UPDATE EmailTemplateScheduler SET FK_EmailStatus_ID = 4 WHERE PK_EmailTemplateScheduler_ID
		IN (
		SELECT etsd.FK_EmailTEmplateScheduler_ID
		  FROM EmailTemplateSchedulerDetail etsd WHERE etsd.PK_EmailTemplateSchedulerDetail_ID=@PK_EmailTemplateSchedulerDetail_ID	
		)
	END
	IF @jmlfail>0 AND @jmlinprocess=0 
	BEGIN
		UPDATE EmailTemplateScheduler SET FK_EmailStatus_ID = 5 WHERE PK_EmailTemplateScheduler_ID
		IN (
		SELECT etsd.FK_EmailTEmplateScheduler_ID
		  FROM EmailTemplateSchedulerDetail etsd WHERE etsd.PK_EmailTemplateSchedulerDetail_ID=@PK_EmailTemplateSchedulerDetail_ID	
		)
	END
		
	IF @jmlinprocess>0
	BEGIN
		UPDATE EmailTemplateScheduler SET FK_EmailStatus_ID = 3 WHERE PK_EmailTemplateScheduler_ID
		IN (
		SELECT etsd.FK_EmailTEmplateScheduler_ID
		  FROM EmailTemplateSchedulerDetail etsd WHERE etsd.PK_EmailTemplateSchedulerDetail_ID=@PK_EmailTemplateSchedulerDetail_ID	
		)
	END
	
	IF @jmlBounce>0
	BEGIN
		UPDATE EmailTemplateScheduler SET FK_EmailStatus_ID = 3 WHERE PK_EmailTemplateScheduler_ID
		IN (
		SELECT etsd.FK_EmailTEmplateScheduler_ID
		  FROM EmailTemplateSchedulerDetail etsd WHERE etsd.PK_EmailTemplateSchedulerDetail_ID=@PK_EmailTemplateSchedulerDetail_ID	
		)
	END
	
	
	
END
GO
