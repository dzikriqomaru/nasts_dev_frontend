/****** Object:  StoredProcedure [dbo].[usp_UpdateValidationResult]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create Procedure [dbo].[usp_UpdateValidationResult]
	@KeyField varchar(50),
	@KeyFieldValue bigint
as

SELECT  * FROM ValidationReport where KeyField = @KeyField and KeyFieldValue = @KeyFieldValue
SELECT  * FROM Web_ValidationReport_Row where KeyField = @KeyField and KeyFieldValue = @KeyFieldValue
select  * from Web_ValidationReport_RowComplate where KeyField = @KeyField and KeyFieldValue = @KeyFieldValue

exec [usp_InsertSummary]
GO
