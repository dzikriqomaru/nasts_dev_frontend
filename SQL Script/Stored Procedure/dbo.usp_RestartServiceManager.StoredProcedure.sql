/****** Object:  StoredProcedure [dbo].[usp_RestartServiceManager]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_RestartServiceManager]
(
	@TargetID INT
)
AS 
BEGIN

DECLARE @ServiceStatus_ID INT

SELECT @ServiceStatus_ID = MServiceStatus.PK_ServiceStatus_ID FROM MServiceStatus WHERE MServiceStatus.PK_ServiceStatus_ID = 5

UPDATE ServiceManagment SET FK_ServiceStatus_ID = @ServiceStatus_ID WHERE PK_ServiceManagment_ID = @TargetID

END

GO
