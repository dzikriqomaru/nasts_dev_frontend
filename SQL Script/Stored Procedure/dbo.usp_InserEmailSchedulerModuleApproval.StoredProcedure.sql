/****** Object:  StoredProcedure [dbo].[usp_InserEmailSchedulerModuleApproval]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_InserEmailSchedulerModuleApproval]
/***********************************************************
* Procedure description:
* Date:   24/02/2016 
* Author: Hendra
*
* Changes
* Date		Modified By			Comments
************************************************************
*
************************************************************/
(
	@PkmoduleApprovalid BIGINT,@inttemplateid int
)
AS
BEGIN
	
	INSERT INTO EmailTemplateScheduler
	(
		-- PK_EmailTemplateScheduler_ID -- this column value is auto-generated
		PK_EmailTemplate_ID,
		ProcessDate,
		FK_EmailStatus_ID
	)
	VALUES
	(
		@inttemplateid,
		GETDATE(),
		1
	)
	
	DECLARE @pk BIGINT
	SET @pk=SCOPE_IDENTITY()
	
	
	
	SELECT * FROM EmailTemplateScheduler ets INNER JOIN EmailTemplate et ON ets.PK_EmailTemplate_ID=et.PK_EmailTemplate_ID
	WHERE ets.PK_EmailTemplateScheduler_ID=@pk
END
	
	
GO
