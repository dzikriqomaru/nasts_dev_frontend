/****** Object:  StoredProcedure [dbo].[usp_SelectNew]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_SelectNew]
/***********************************************************
* Procedure description:
* Date:   7/19/2019 
* Author: nawadata
*
* Changes
* Date		Modified By			Comments
************************************************************
*
************************************************************/
(
	@Query          VARCHAR(MAX),    
    @OrderBy         VARCHAR(max)
)
AS
BEGIN
	
	
	
	if @OrderBy='' SET @OrderBy= ' 1 '
 DECLARE @sql varchar(max)

SET @sql = '   '+ @Query +' ' + char(10)
		 + 'ORDER BY   '+@OrderBy  + char(10)
         + ''
PRINT @sql         
EXEC(@sql)

  
	
	
END
GO
