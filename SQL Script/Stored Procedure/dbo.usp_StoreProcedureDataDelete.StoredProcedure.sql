/****** Object:  StoredProcedure [dbo].[usp_StoreProcedureDataDelete]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_StoreProcedureDataDelete]
(
@StoreProcedureName VARCHAR(500)
)
AS 
BEGIN
	
	DECLARE @sql varchar(max)
	SET @sql = 'IF object_id('''+ @StoreProcedureName +''') IS NOT NULL ' + char(10) +CHAR(13)
         + 'BEGIN  ' + char(10) +CHAR(13)
         + '	DROP PROCEDURE '+@StoreProcedureName +'  	 ' + char(10)+CHAR(13)
         + 'END'
	EXEC (@sql)         
	
END 
GO
