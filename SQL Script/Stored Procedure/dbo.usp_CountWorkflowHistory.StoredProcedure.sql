/****** Object:  StoredProcedure [dbo].[usp_CountWorkflowHistory]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE    PROCEDURE [dbo].[usp_CountWorkflowHistory]
	@ModuleApprovalID BIGINT,
	@Search VARCHAR(MAX)
AS
SELECT COUNT(13) NeedApproval
FROM MWorkFlow_History
	 LEFT JOIN MWorkflow_ApprovalStatus
	 ON FK_MWorkflow_ApprovalStatus_ID = PK_MWorkflow_ApprovalStatus_ID
WHERE FK_ModuleApproval_ID = @ModuleApprovalID
		AND (
			RoleName LIKE '%' + @Search + '%'
			OR UserName LIKE '%' + @Search + '%'
			OR UserNameExecute LIKE '%' + @Search + '%'
			OR ApprovalStatusName LIKE '%' + @Search + '%'
			OR Notes LIKE '%' + @Search + '%'
		)

GO
