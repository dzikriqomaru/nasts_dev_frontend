/****** Object:  StoredProcedure [dbo].[usp_InsertModuleApproval]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE    PROCEDURE [dbo].[usp_InsertModuleApproval]
	@ModuleName VARCHAR(250),
	@ModuleKey VARCHAR(1000),
	@ModuleField TEXT,
	@ModuleFieldBefore TEXT,
	@PK_ModuleAction_ID INTEGER,
	@CreatedDate DATETIME,
	@CreatedBy VARCHAR(50),
	@FK_Module_ID INTEGER,
	@FK_MRole_ID INTEGER = NULL
AS
INSERT INTO ModuleApproval(ModuleName, ModuleKey, ModuleField, ModuleFieldBefore, PK_ModuleAction_ID, CreatedDate, CreatedBy, FK_Module_ID, FK_MRole_ID)
VALUES(@ModuleName, @ModuleKey, @ModuleField, @ModuleFieldBefore, @PK_ModuleAction_ID, @CreatedDate, @CreatedBy, @FK_Module_ID, @FK_MRole_ID)

SELECT SCOPE_IDENTITY() AS LastID

GO
