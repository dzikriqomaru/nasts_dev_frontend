/****** Object:  StoredProcedure [dbo].[usp_insertEODSchedulerManualBranch]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_insertEODSchedulerManualBranch]
/***********************************************************
* Procedure description:
* Date:   28/01/2016 
* Author: Hendra
*
* Changes
* Date		Modified By			Comments
************************************************************
*
************************************************************/
(
    @Datadate      DATETIME,
    @processid     AS BIGINT,
	@taskid		VARCHAR(MAX),
    @userID        VARCHAR(50),
	@kodecabang		VARCHAR(10)
)
AS
BEGIN
	
 
 	DECLARE @tabletasklist TABLE(taskid VARCHAR(MAX))
INSERT into @tabletasklist(taskid) VALUEs (@taskid) 



	INSERT INTO EODSchedulerLog
	  (
	    -- PK_EODSchedulerLog_ID -- this column value is auto-generated
	    FK_EODSchedulerID,
	    ExecuteBy,
	    DataDate,
	    ProcessDate,
	    StartDate,
	    Enddate,
	    ErrorMessage,
	    FK_MsEODStatus_ID,
		KodeCabang
	  )
	VALUES
	  (
	    @processid,
	    @userID,
	    @Datadate,
	    GETDATE(),
	    NULL,
	    NULL,
	    '',
	    1,
		@kodecabang
	  )
	DECLARE @PK_EODSchedulerLog_ID BIGINT
	SELECT @PK_EODSchedulerLog_ID = SCOPE_IDENTITY()
	
	
	DECLARE @PK_EODSchedulerDetail_ID     BIGINT,
	        @FK_EODSCheduler_ID           BIGINT,
	        @FK_EODTask_ID                BIGINT,
	        @OrderNo                      INT,
	        @CreatedBy                    VARCHAR(50),
	        @LastUpdateBy                 VARCHAR(50),
	        @ApprovedBy                   VARCHAR(50),
	        @CreatedDate                  DATETIME,
	        @LastUpdateDate               DATETIME,
	        @ApprovedDate                 DATETIME
	
	DECLARE my_cursor1 CURSOR FAST_FORWARD READ_ONLY 
	FOR



    SELECT PK_EODSchedulerDetail_ID,
	           FK_EODSCheduler_ID,
	           FK_EODTask_ID,
	           OrderNo,
	           CreatedBy,
	           LastUpdateBy,
	           ApprovedBy,
	           CreatedDate,
	           LastUpdateDate,
	           ApprovedDate
	    FROM   dbo.EODSchedulerDetail ed
	    WHERE  ed.FK_EODSCheduler_ID = @processid AND ed.FK_EODTask_ID IN (
	    
SELECT 
LTRIM(RTRIM(m.n.value('.[1]','varchar(8000)'))) AS tasklist
FROM
(
SELECT CAST('<XMLRoot><RowData>' + REPLACE(taskid,',','</RowData><RowData>') + '</RowData></XMLRoot>' AS XML) AS x
FROM   @tabletasklist
)t
CROSS APPLY x.nodes('/XMLRoot/RowData')m(n)	
	    )
	    ORDER BY
	           ed.OrderNo
	
	OPEN my_cursor1
	
	FETCH FROM my_cursor1 INTO @PK_EODSchedulerDetail_ID, @FK_EODSCheduler_ID,
	@FK_EODTask_ID, @OrderNo, @CreatedBy,
	@LastUpdateBy, @ApprovedBy, @CreatedDate,
	@LastUpdateDate, @ApprovedDate
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
	    INSERT INTO EODTaskLog
	      (
	        -- PK_EODTaskLog_ID -- this column value is auto-generated
	        EODSchedulerLogID,
	        FK_EODTaskID,
	        ExecuteBy,
	        StartDate,
	        Enddate,
	        ErrorMessage,
	        FK_MsEODStatus_ID
	      )
	    VALUES
	      (
	        @PK_EODSchedulerLog_ID,
	        @FK_EODTask_ID,
	        @userID,
	        NULL,
	        NULL,
	        '',
	        1
	      )
	    
	    
	    DECLARE @PK_EODTaskLog_ID BIGINT
	    SELECT @PK_EODTaskLog_ID = SCOPE_IDENTITY()
	    
	    
	    DECLARE @PK_EODTaskDetail_ID         BIGINT,
	            @FK_EODTaskDetailType_ID     INT,
	            @SSISName                    VARCHAR(250),
	            @SSISFIle                    VARBINARY(MAX),
	            @StoreProcedureName          VARCHAR(250),
	            @Keterangan                  VARCHAR(8000),
	            @IsUseParameterProcessDate BIT
	    
	    DECLARE my_cursor2 CURSOR FAST_FORWARD READ_ONLY 
	    FOR
	        SELECT PK_EODTaskDetail_ID,
	               
	               FK_EODTaskDetailType_ID,
	               
	               SSISName,
	 SSISFIle,
	               StoreProcedureName,
	               Keterangan,
	               IsUseParameterProcessDate
	        FROM   dbo.EODTaskDetail ed
	        WHERE  ed.FK_EODTask_ID = @FK_EODTask_ID
	        ORDER BY
	               ed.OrderNo
	    
	    OPEN my_cursor2
	    
	    FETCH FROM my_cursor2 INTO @PK_EODTaskDetail_ID, 
	    @FK_EODTaskDetailType_ID, @SSISName,
	    @SSISFIle, @StoreProcedureName, @Keterangan,
	    @IsUseParameterProcessDate
	    
	    WHILE @@FETCH_STATUS = 0
	    BEGIN
	        /*{ ... Cursor logic here ... }*/
	        
	        INSERT INTO EODTaskDetailLog
	          (
	            -- PK_EODTaskDetailLog_ID -- this column value is auto-generated
	            EODSchedulerLogID,
	            EODTaskLogID,
	            FK_EODTAskDetail_ID,
	            ExecuteBy,
	            StartDate,
	            Enddate,
	            ErrorMessage,
	            FK_MsEODStatus_ID
	          )
	        VALUES
	          (
	            @PK_EODSchedulerLog_ID,
	            @PK_EODTaskLog_ID,
	            @PK_EODTaskDetail_ID,
	            @userID,
	            NULL,
	            NULL,
	            '',
	            1
	          )
	        
	        FETCH FROM my_cursor2 INTO @PK_EODTaskDetail_ID, 
	        @FK_EODTaskDetailType_ID, @SSISName,
	        @SSISFIle, @StoreProcedureName, @Keterangan,
	        @IsUseParameterProcessDate
	    END
	    
	    CLOSE my_cursor2
	    DEALLOCATE my_cursor2
	    
	    
	    FETCH FROM my_cursor1 INTO @PK_EODSchedulerDetail_ID,
	    @FK_EODSCheduler_ID, @FK_EODTask_ID, @OrderNo,
	    @CreatedBy, @LastUpdateBy, @ApprovedBy,
	    @CreatedDate, @LastUpdateDate, @ApprovedDate
	END
	
	CLOSE my_cursor1
	DEALLOCATE my_cursor1
END

GO
