/****** Object:  StoredProcedure [dbo].[usp_ValidateMWorkflow]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ValidateMWorkflow]
/***********************************************************
* Procedure description:
* Date:   7/17/2018 
* Author: hendra1
*
* Changes
* Date		Modified By			Comments
* 23/06/2021	M Fauzan Girindra	Add brackets to table names in query in case UserID has dot in it
* 13/07/2022 Firman Nugraha, Fix Workflow
************************************************************
*
************************************************************/
(
	@pkmoduleid INT,@pkunikid VARCHAR(8000),@userId VARCHAR(50),@action INT
)
AS
BEGIN
	--DECLARE
	--@pkmoduleid INT = 13415,
	--@pkunikid VARCHAR(8000) = '',
	--@userId VARCHAR(50) = 'testapprovalmaker',
	--@action INT = 5

	IF EXISTS(SELECT 13 FROM Module WHERE PK_Module_ID = @pkmoduleid AND IsUseApproval = 1) AND EXISTS(SELECT 13 FROM MUser WHERE UserID = @userId AND FK_MRole_ID <> 1)
	BEGIN	
		declare @sql VARCHAR(8000)
		--BEGIN TRY	

		SET @sql = ' IF OBJECT_ID(''ResultWorkflowCondition_' + @UserID +
			''', ''U'') IS NOT NULL  ' + CHAR(10) 
			+ '  DROP TABLE [ResultWorkflowCondition_' + @UserID + '];  ' + CHAR(10)
				
		EXEC (@sql)

 EXEC (  
          'select 0 as workflowid,0 as sequencedata into ResultWorkflowCondition_' + @UserID + ' where 1=2'  
      )      
   
   
 DECLARE @PK_ModuleWorkFlow     INT,  
         @FK_ModuleID           INT,  
         @WorkFlowType          VARCHAR(250),  
         @FK_WorkFlow_ID        INT,  
         @ExpresionType         INT,  
         @ExpressionValue       VARCHAR(8000),  
         @Sequence              TINYINT,  
         @Active                BIT,  
         @CreatedBy             VARCHAR(50),  
         @LastUpdateBy          VARCHAR(50),  
         @ApprovedBy            VARCHAR(50),  
         @CreatedDate           DATETIME,  
         @LastUpdateDate        DATETIME,  
         @ApprovedDate          DATETIME,  
         @Alternateby           VARCHAR(50),  
         @FieldUnik             VARCHAR(255),  
         @modulename            VARCHAR(500),  
         @strsql                VARCHAR(MAX),  
         @strinsert             VARCHAR(MAX)      
   
 SET @strinsert = 'insert into ResultWorkflowCondition_' + @UserID      
   
   
 DECLARE my_cursor CURSOR FAST_FORWARD READ_ONLY   
 FOR  
     SELECT PK_ModuleWorkFlow,  
            FK_ModuleID,  
            WorkFlowType,  
            FK_WorkFlow_ID,  
            ExpresionType,  
            ExpressionValue,  
            Sequence,  
            a.[Active],  
            a.CreatedBy,  
            a.LastUpdateBy,  
            a.ApprovedBy,  
            a.CreatedDate,  
            a.LastUpdateDate,  
            a.ApprovedDate,  
            a.Alternateby,  
            FieldUnik,  
            x.ModuleName  
     FROM   Module_TR_WorkFlow a  
            INNER JOIN module x  
                 ON  a.FK_ModuleID = x.PK_Module_ID  
     WHERE  a.FK_ModuleID = @pkmoduleid  
     ORDER BY  
            a.Sequence  
	
		OPEN my_cursor
	
		FETCH FROM my_cursor INTO @PK_ModuleWorkFlow, @FK_ModuleID, @WorkFlowType,
								  @FK_WorkFlow_ID, @ExpresionType, @ExpressionValue,
								  @Sequence, @Active, @CreatedBy, @LastUpdateBy,
								  @ApprovedBy, @CreatedDate, @LastUpdateDate,
								  @ApprovedDate, @Alternateby, @FieldUnik,@modulename
	
		WHILE @@FETCH_STATUS = 0
		BEGIN
			/*{ ... Cursor logic here ... }*/
		
			IF @ExpresionType=1 --expression
			BEGIN
			
				set @strsql='select '+ CONVERT(VARCHAR(50), @FK_WorkFlow_ID) +' as workflowid ,'+ CONVERT(VARCHAR(50), @Sequence) +' as sequence from ['+ @modulename + IIF(@action = 1, '_WorkflowAdd_' + @userId, '') +'] where '+@FieldUnik +'='''+ @pkunikid +''' and  '+@ExpressionValue
			END
		
		
			IF @ExpresionType=2 --full query
			BEGIN
				--set @strsql= @ExpressionValue +' and '+@FieldUnik +'='''+ @pkunikid +''''
			
         SET @strsql = 'select ' + CONVERT(VARCHAR(50), @FK_WorkFlow_ID) + ' as workflowid ,' + CONVERT(VARCHAR(50), @Sequence)   
             + ' as sequence from [' + @modulename + '] with (nolock) ' + @ExpressionValue + ' and [' + @modulename + '].' +   
             @FieldUnik + '=''' + @pkunikid + ''''   
             PRINT(@strsql)  
			END
		
			IF @ExpresionType=3 --without criteria
			BEGIN
				set @strsql='select '+ CONVERT(VARCHAR(50), @FK_WorkFlow_ID) +' as workflowid ,'+ CONVERT(VARCHAR(50), @Sequence) +' as sequence'
			
			END
		
			PRINT @strinsert+ ' '+@strsql
			exec(@strinsert+ ' '+@strsql)
		
		
	
			FETCH FROM my_cursor INTO @PK_ModuleWorkFlow, @FK_ModuleID,
									  @WorkFlowType, @FK_WorkFlow_ID,
									  @ExpresionType, @ExpressionValue, @Sequence,
									  @Active, @CreatedBy, @LastUpdateBy,
									  @ApprovedBy, @CreatedDate, @LastUpdateDate,
									  @ApprovedDate, @Alternateby, @FieldUnik,@modulename
		END
	
		CLOSE my_cursor
		DEALLOCATE my_cursor
	
 EXEC (  
          ' select top 1 workflowid from ResultWorkflowCondition_' + @UserID + ' with (nolock) order by sequencedata'  
      )      
		SET @sql = '	SELECT fk_workflow FROM MWorkFlowDetail AS mfd ' + char(10)
				 + '  WHERE mfd.FK_WorkFlow IN ( ' + char(10)
				 + '  select top 1 workflowid from ResultWorkflowCondition_'+@userId + '' + char(10)
				 + '  with (nolock) order by sequencedata	   ' + char(10)
				 + '  ) ' + char(10)
				 + '  AND mfd.[Level]=1 ' + char(10)
				 + '  AND mfd.RoleID IN ( ' + char(10)
				 + '  SELECT muser.FK_MRole_ID FROM muser WHERE UserID='''+ @userId +'''	 ' + char(10)
				 + '  ) ' + char(10)
				 + ''
		EXEC(@sql)
	END
	ELSE
	BEGIN
		SELECT 0 AS fk_workflow
	END
END

GO
