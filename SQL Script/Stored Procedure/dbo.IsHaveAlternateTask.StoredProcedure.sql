/****** Object:  StoredProcedure [dbo].[IsHaveAlternateTask]    Script Date: 12/12/2023 11:28:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[IsHaveAlternateTask]
/***********************************************************
* Procedure description:
* Date:   7/24/2017 
* Author: Hendra
*
* Changes
* Date		Modified By			Comments
************************************************************
*
************************************************************/
(
	@UserID VARCHAR(100)
)
AS
BEGIN
	
if EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.TABLES t
WHERE t.TABLE_NAME='Alternate'
)
BEGIN
	
	SELECT COUNT(1) AS jml FROM Alternate a
	WHERE a.UserAlternate=@UserID
	AND GETDATE() BETWEEN startdate AND dateadd(day,1,a.EndDate)
	AND a.[Active]=1
	
END
ELSE
	BEGIN
		SELECT 0 AS jml
	END
END
GO
