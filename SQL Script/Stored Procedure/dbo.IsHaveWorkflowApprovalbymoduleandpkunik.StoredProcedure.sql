/****** Object:  StoredProcedure [dbo].[IsHaveWorkflowApprovalbymoduleandpkunik]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 CREATE PROCEDURE [dbo].[IsHaveWorkflowApprovalbymoduleandpkunik]
   /***********************************************************
   * Procedure description:
   * Date:   5/6/2019 
   * Author: nawadata
   *
   * Changes
   * Date		Modified By			Comments
   ************************************************************
   *
   ************************************************************/
   (
   	@PkMOduleid INT, @pkunikid VARCHAR(1000)
   	
   )
   AS
   BEGIN
   	
   	
   	SELECT COUNT(1) AS jml FROM MWorkFlow_Progress AS mfp WHERE mfp.FK_Module_ID=@PkMOduleid AND mfp.FK_Unik_ID=@pkunikid
   
   	
   	
   END
GO
