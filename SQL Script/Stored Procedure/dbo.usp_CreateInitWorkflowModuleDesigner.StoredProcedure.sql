/****** Object:  StoredProcedure [dbo].[usp_CreateInitWorkflowModuleDesigner]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_CreateInitWorkflowModuleDesigner]  
/***********************************************************  
* Procedure description:  
* Date:   7/17/2018   
* Author: hendra1  
*  
* Changes  
* Date  Modified By   Comments  
* 13/07/2022 Firman Nugraha, Fix Workflow comment 20220713
************************************************************  
*  
************************************************************/  
(  
	@pkworkflowid INT, @pkunik VARCHAR(MAX), @pkuserid INT, @pkmoduleid INT, @pkuseridexecute INT, @notes VARCHAR(MAX), @intworkflowstatus int, @pkmoduleapprovalid AS bigint, @intworkflowApprovalStatusid int
)  
AS  
BEGIN
	if @intworkflowstatus<> 1   
	BEGIN
		IF @pkworkflowid IS NOT NULL   
		BEGIN  
			DECLARE @PK_WorkFlowDetail int, @FK_WorkFlow int, @RoleID int, @UserType int,  
			@Level tinyint, @SLAType varchar(50), @SLAValue tinyint,  
			@BreachSLAAction varchar(250), @BreachSLAActionTo tinyint,  
			@RevisedActionTo tinyint, @Active bit, @CreatedBy varchar(50),  
			@LastUpdateBy varchar(50), @ApprovedBy varchar(50),  
			@CreatedDate datetime, @LastUpdateDate datetime, @ApprovedDate datetime,  
			@Alternateby varchar(50)  
   
			SELECT TOP 1  
				@PK_WorkFlowDetail = PK_WorkFlowDetail, @FK_WorkFlow = FK_WorkFlow,  
				@RoleID = RoleID, @UserType = UserType, @Level = [Level],  
				@SLAType = SLAType, @SLAValue = SLAValue,  
				@BreachSLAAction = BreachSLAAction,  
				@BreachSLAActionTo = BreachSLAActionTo,  
				@RevisedActionTo = RevisedActionTo, @Active = [Active],  
				@CreatedBy = CreatedBy, @LastUpdateBy = LastUpdateBy,  
				@ApprovedBy = ApprovedBy, @CreatedDate = CreatedDate,  
				@LastUpdateDate = LastUpdateDate, @ApprovedDate = ApprovedDate,  
				@Alternateby = Alternateby  
			FROM MWorkFlowDetail  
			WHERE FK_WorkFlow = @pkworkflowid  
			AND [Level] = 1

			DECLARE @rolename VARCHAR(500)  
			SELECT @rolename = m.RoleName FROM MRole AS m WHERE m.PK_MRole_ID = @RoleID
   
			DECLARE @username VARCHAR(500)  
			SELECT  @username = m.UserName FROM MUser AS m WHERE m.PK_MUser_ID = @pkuserid  
   
			DECLARE @usernametoExecute VARCHAR(500)  
			SELECT  @usernametoExecute= m.UserName FROM MUser AS m WHERE m.PK_MUser_ID = @pkuseridexecute
   
   
			DECLARE @jmlhis int  
   
			SELECT @jmlhis = COUNT(1) FROM MWorkFlow_History WHERE FK_Module_ID = @pkmoduleid AND FK_Unik_ID = @pkunik AND intLevel = 1 AND FK_ModuleApproval_ID = @pkmoduleapprovalid AND FK_MWorkflow_ApprovalStatus_ID IS NULL   
   
			IF @jmlhis > 0   
			BEGIN  
				UPDATE MWorkFlow_History  
				SET  
					FK_MUserId=@pkuserid,UserName=@username, -- 20220713
					UserNameExecute =  @usernametoExecute ,  
					ResponseDate = GETDATE(),  
					FK_MWorkflow_ApprovalStatus_ID = @intworkflowApprovalStatusid,  --submit
					Notes = @notes  
				FROM MWorkFlow_History x  
				WHERE x.FK_Module_ID = @pkmoduleid AND x.FK_Unik_ID = @pkunik  
					AND x.FK_ModuleApproval_ID = @pkmoduleapprovalid
					AND x.FK_MWorkflow_ApprovalStatus_ID IS NULL  
   
			END  
			ELSE  
			BEGIN
				INSERT INTO MWorkFlow_History  
				(
					FK_ModuleApproval_ID,
					FK_Module_ID,  
					FK_Unik_ID,  
					FK_MUserId,  
					FK_MRoleId,  
					intLevel,  
					RoleName,  
					UserName,  
					UserNameExecute,  
					CreatedDate,  
					ResponseDate,  
					FK_MWorkflow_ApprovalStatus_ID,  
					Notes  
				)  
				VALUES  
				(
					@pkmoduleapprovalid,
					@pkmoduleid,  
					@pkunik,  
					@pkuserid,  
					@RoleID,  
					@Level,  
					@rolename ,  
					@username,  
					@usernametoExecute,  
					GETDATE(),  
					GETDATE(),  
					@intworkflowApprovalStatusid,  
					@notes   
				)  
			END

			-- find next workflow and insert next workflow
			DECLARE @PK_WorkFlowDetailnext int, @FK_WorkFlownext int, @RoleIDnext int, @UserTypenext int,  
			@Levelnext tinyint, @SLATypenext varchar(50), @SLAValuenext tinyint,  
			@BreachSLAActionnext varchar(250), @BreachSLAActionTonext tinyint,  
			@RevisedActionTonext tinyint, @Activenext bit, @CreatedBynext varchar(50),  
			@LastUpdateBynext varchar(50), @ApprovedBynext varchar(50),  
			@CreatedDatenext datetime, @LastUpdateDatenext datetime, @ApprovedDatenext datetime,  
			@Alternatebynext varchar(50)  
   
			SELECT  
				@PK_WorkFlowDetailnext = PK_WorkFlowDetail, @FK_WorkFlownext = FK_WorkFlow,  
				@RoleIDnext = RoleID, @UserTypenext = UserType, @Levelnext = [Level],  
				@SLATypenext = SLAType, @SLAValuenext = SLAValue,  
				@BreachSLAActionnext = BreachSLAAction,  
				@BreachSLAActionTonext = BreachSLAActionTo,  
				@RevisedActionTonext = RevisedActionTo, @Activenext = [Active],  
				@CreatedBynext = CreatedBy, @LastUpdateBynext = LastUpdateBy,  
				@ApprovedBynext = ApprovedBy, @CreatedDatenext = CreatedDate,  
				@LastUpdateDatenext = LastUpdateDate, @ApprovedDatenext = ApprovedDate,  
				@Alternatebynext = Alternateby  
			FROM MWorkFlowDetail  
			WHERE FK_WorkFlow = @pkworkflowid AND [Level] = 2  
			
			DELETE FROM MWorkFlow_Progress WHERE FK_Module_ID = @pkmoduleid AND FK_Unik_ID = @pkunik

			DECLARE @pkuserupline int  
			IF @UserTypenext = 2  
			BEGIN  
				SELECT @pkuserupline = ms.FK_Parent_ID FROM MUserStructure AS ms WHERE ms.FK_User_ID = @pkuserid   
			END  
			 --tambahan  20220713        
			    declare @NextUserid as int , @NextUserName as varchar(max),@GroupMenuID as int         
			    select @GroupMenuID=FK_MGroupMenu_ID from muser where PK_MUser_ID=@pkuserid                  
			     IF @UserTypenext=4                  
			    BEGIN                  
			     SELECT @NextUserid=PK_MUser_ID,@NextUserName=UserName FROM MUser AS ms WHERE PK_MUser_ID<>@pkuserid  and FK_MRole_ID=@RoleID and FK_MGroupMenu_ID<>@GroupMenuID                   
			    END               
			 --tambahan    

			DECLARE @usernameupline VARCHAR(500)  
			SELECT  @usernameupline= m.UserName FROM MUser AS m WHERE m.PK_MUser_ID = @pkuserupline  

			--progress untuk next
			INSERT INTO MWorkFlow_Progress  
			(  
				FK_Module_ID,  
				FK_Unik_ID,  
				FK_MWorkflow_ID,  
				WorkflowLevel,  
				FK_MWorkFlowUserType_ID,  
				FK_MWorkflowRole_ID,  
				FK_MWorkflowUser_ID,  
				FK_MWorkflow_Status_ID,  
				SLAType,  
				SLAValue,  
				BreachSLAAction,  
				BreachSLAActionLevelTo,  
				RevisedActionLevelTo  
			)  
			VALUES  
			(
				@pkmoduleid,    
				@pkunik,  
				@pkworkflowid,  
				@Levelnext,  
				@UserTypenext,  
				@RoleIDnext,  
			 	CASE WHEN @UserTypenext=1 THEN @pkuserid WHEN  @UserTypenext=2 THEN @pkuserupline ELSE NULL END,             
				@intworkflowstatus,  
				@SLATypenext ,  
				@SLAValuenext,  
				@BreachSLAActionnext,
				@BreachSLAActionTonext,
				@RevisedActionTonext  
			)
   
			DECLARE @rolenamenext VARCHAR(500)  
			SELECT @rolenamenext = m.RoleName FROM MRole AS m WHERE m.PK_MRole_ID = @RoleIDnext  
   
			DECLARE @usernamenext VARCHAR(500)  
			SELECT  @usernamenext= m.UserName FROM MUser AS m WHERE m.PK_MUser_ID=@pkuserid  

			INSERT INTO MWorkFlow_History  
			(
				FK_ModuleApproval_ID,
				FK_Module_ID,  
				FK_Unik_ID,  
				FK_MUserId,  
				FK_MRoleId,  
				intLevel,  
				RoleName,  
				UserName,  
				UserNameExecute,  
				CreatedDate,  
				ResponseDate,  
				FK_MWorkflow_ApprovalStatus_ID,  
				Notes  
			)  
			VALUES  
			(
				@pkmoduleapprovalid,
				@pkmoduleid,  
				@pkunik,  
				 --CASE WHEN @UserTypenext=1 THEN @pkuserid WHEN  @UserTypenext=2 THEN @pkuserupline ELSE @NextUserid END,  --user level 2    
				CASE WHEN @UserTypenext=1 THEN @pkuserid WHEN  @UserTypenext=2 THEN @pkuserupline ELSE NULL END,  --user level 2     Update Firman jika type role di buat null 20220713
    				@RoleIDnext   ,                  
    				@Levelnext,                  
    				@rolenamenext,                  
     				--CASE WHEN @UserTypenext=1 THEN @usernamenext WHEN  @UserTypenext=2 THEN @usernameupline ELSE @NextUserName END,  
	  			CASE WHEN @UserTypenext=1 THEN @usernamenext WHEN  @UserTypenext=2 THEN @usernameupline ELSE NULL END, -- Update Firman jika type role di buat null 20220713
				NULL,  
				GETDATE(),  
				null,  
				null,  
				''
			)
		-- 20220713 Update Firman Email Template Request ---                  
		DECLARE @ReqEmail INT,                  
			@QueryEmailTemplate VARCHAR(MAX),                  
			@EmailAddress VARCHAR(MAX),                  
			@UserNameEmail VARCHAR(MAX),                  
			@UserIDEmail VARCHAR(MAX)                  
                   
		SELECT @ReqEmail = ISNULL(ReqEmailTemplateID,0)                 
		FROM MWorkFlow WHERE PK_WorkFlow = @pkworkflowid                  
		IF @ReqEmail > 0                
		BEGIN                
			IF @UserTypenext=2                   
			BEGIN                  
				SELECT @EmailAddress = UserEmailAddress, @UserNameEmail = UserName, @UserIDEmail = UserID                  
				FROM Muser WHERE PK_MUser_ID = @pkuserupline                  
                    
				INSERT INTO MWorkflow_Email (FK_Module_ID,FK_Unik_ID,UserID,EmailAddress,IsSendEmail,FK_MWorkflow_ApprovalStatus_ID,UserName,Active,CreatedBy,CreatedDate,FK_ModuleApproval_ID)                  
				SELECT @pkmoduleid, @pkunik, @UserIDEmail, @EmailAddress, 0, @intworkflowstatus, @UserNameEmail,1,@UserNameEmail,GETDATE(),@pkmoduleapprovalid                 
			END  
			ELSE IF @UserTypenext=4                  
			BEGIN                
                     
				INSERT INTO MWorkflow_Email (FK_Module_ID,FK_Unik_ID,UserID,EmailAddress,IsSendEmail,FK_MWorkflow_ApprovalStatus_ID,UserName,Active,CreatedBy,CreatedDate,FK_ModuleApproval_ID)                  
				SELECT @pkmoduleid, @pkunik, UserID, UserEmailAddress, 0, @intworkflowstatus, UserName,1,UserName,GETDATE(),@pkmoduleapprovalid                  
				FROM MUser WHERE FK_MRole_ID = @RoleIDnext      
			
				--EXEC usp_EMailScheduler_Create @ReqEmail, @pkunik, 0                  
			END                  
                    
			EXEC usp_EMailScheduler_Create @ReqEmail, @pkmoduleapprovalid, 0                  
                   
			UPDATE MWorkflow_Email SET IsSendEmail = 1 WHERE FK_Module_ID = @pkmoduleid AND FK_Unik_ID = @pkunik                  
		END                
                   
		-- End Update Firman Email Template  ---                     
                   
   END                  
		ELSE
		BEGIN
			DELETE FROM MWorkFlow_Progress WHERE FK_Module_ID = @pkmoduleid AND FK_Unik_ID = @pkunik  
     
			INSERT INTO MWorkFlow_Progress  
			(
				FK_Module_ID,  
				FK_Unik_ID,  
				FK_MWorkflow_ID,  
				WorkflowLevel,  
				FK_MWorkFlowUserType_ID,  
				FK_MWorkflowRole_ID,  
				FK_MWorkflowUser_ID,  
				FK_MWorkflow_Status_ID,  
				SLAType,  
				SLAValue,  
				BreachSLAAction,  
				BreachSLAActionLevelTo,  
				RevisedActionLevelTo  
			)  
			VALUES  
			(  
				@pkmoduleid,    
				@pkunik,  
				null,  
				NULL,  
				NULL,  
				NULL,  
				CASE WHEN @UserType=1 OR @UserType=2 THEN @pkuserid ELSE NULL END,  
				1,  
				@SLAType,  
				@SLAValue,  
				@BreachSLAAction,  
				@BreachSLAActionTo,  
				@RevisedActionTo  
			)
		END
	END
ELSE              
	BEGIN              
		DELETE FROM MWorkFlow_Progress WHERE FK_Module_ID=@pkmoduleid AND FK_Unik_ID=@pkunik                  
                     
		INSERT INTO MWorkFlow_Progress                  
		(                  
		-- PK_MWorkflow_Progress_ID -- this column value is auto-generated                  
		FK_Module_ID,                  
		FK_Unik_ID,                  
		FK_MWorkflow_ID,                  
		WorkflowLevel,                  
		FK_MWorkFlowUserType_ID,                  
		FK_MWorkflowRole_ID,                  
		FK_MWorkflowUser_ID,                  
		FK_MWorkflow_Status_ID,                  
		SLAType,                  
		SLAValue,                  
		BreachSLAAction,                  
		BreachSLAActionLevelTo,                  
		RevisedActionLevelTo ,Alternateby                 
		)                  
		VALUES                  
		(                  
		@pkmoduleid,                    
		@pkunik,                  
		null,                  
		NULL,                  
		NULL,                  
		NULL,                  
		NULL,                  
		1,                  
		NULL,                  
		NULL,                  
		NULL,                  
		NULL,                  
		NULL    ,'11'              
		)                  
	END              
                   
                   
                   
                   
                   
                   
                      
                   
END 
GO
