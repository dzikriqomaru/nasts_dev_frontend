/****** Object:  StoredProcedure [dbo].[usp_GetTaskList_Waiting_AdditionalUser]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_GetTaskList_Waiting_AdditionalUser]    
/***********************************************************    
* Procedure description    
* Date   10/7/2015    
* Author Hendra    
*    
* Changes    
* Date  Modified By   Comments    
************************************************************    
*    
************************************************************/    
(    
    @userid          VARCHAR(50),    
    @groupmenuid     INT,   
	@mroleid		 INT, 
    @actionid        INT,    
    @orderby         VARCHAR(4000),    
    @whereclause     VARCHAR(4000),    
    @PageIndex       INT,    
    @PageSize        INT    
)    
AS    
BEGIN    
     
 DECLARE @sql VARCHAR(8000)    
 SET @sql = ' ' + CHAR(10)    
     + ' WITH PageIndex AS ( '    
     +    
     'SELECT xx.jumlah as jumlah,ab.ModuleLabel AS ModuleName,  ''/Parameter/WaitingApproval.aspx''mMenuURL,yy.FK_Module_ID as ModuleID FROM ( '    
     + CHAR(10)    
     + 'SELECT COUNT(1) AS jumlah,ma.ModuleName NamaModule FROM ModuleApproval ma ' +    CHAR(10)    
     + ' INNER JOIN vw_AdditionalUser m ON m.UserID = ma.CreatedBy AND m.PK_MRole_ID = ma.FK_MRole_ID ' + CHAR(10)   
	 + ' AND m.PK_MGroupMenu_ID = ' + CONVERT(VARCHAR(50), @groupmenuid) +  CHAR(10)
	 + 'INNER JOIN Module AS m2 ' + CHAR(10)
	 + '	ON m2.ModuleName = ma.ModuleName ' + CHAR(10)
	 + 'INNER JOIN MWorkFlow_Progress AS mfp ' + CHAR(10) 
	 + '	ON mfp.FK_Module_ID = m2.PK_Module_ID ' + CHAR(10)
	 + '	AND mfp.FK_Unik_ID = ma.ModuleKey ' + CHAR(10)
	 + 'WHERE mfp.FK_MWorkflow_Status_ID NOT IN ( 6 ) ' + CHAR(10)
	 + '	AND ma.CreatedBy =''' + @userid + '''' + CHAR(10)  
     + ' AND m.PK_MRole_ID = ' + CONVERT(VARCHAR(50), @mroleid) +  
     + 'GROUP BY ma.ModuleName ' + CHAR(10)    
	 + '	UNION ALL ' + CHAR(10)
	 + '  SELECT dataapprovalbyrole.jumlah,  ' + char(10)        
	 + '         dataapprovalbyrole.NamaModule  ' + char(10)     
	 + '  FROM   (  ' + char(10)        
	 + '             SELECT COUNT(1)           AS jumlah,  ' + char(10)        
	 + '           ma.ModuleName         NamaModule,  ' + char(10)        
	 + '                    (  ' + char(10)        
	 + '                        SELECT COUNT(1)  ' + char(10)        
	 + '                        FROM   Module_TR_WorkFlow  ' + char(10)        
	 + '                        WHERE  FK_ModuleID = m2.PK_Module_ID  ' + char(10)        
	 + '                               AND [Active] = 1  ' + char(10)        
	 + '                    )                  AS jmlworkflow  ' + char(10)        
	 + '             FROM   ModuleApproval ma  ' + char(10)        
	 + '       INNER JOIN vw_AdditionalUser m  ' + char(10)        
	 + '                         ON  m.UserID = ma.CreatedBy  ' + char(10)        
     + '       AND m.PK_MRole_ID = ma.FK_MRole_ID ' + CHAR(10)      
	 + '                    INNER JOIN Module  AS m2  ' + char(10)        
	 + '                         ON  m2.ModuleName = ma.ModuleName  ' + char(10)        
	 + '             WHERE  ma.CreatedBy = '''+@userid+''' AND m.PK_MRole_ID =  '+CONVERT(VARCHAR(50),@mroleid) + char(10)        
	 + '    AND m.PK_MGroupMenu_ID = ' + CONVERT(VARCHAR(50),@groupmenuid) + CHAR(10)        
	 + '             GROUP BY  ' + char(10)        
	 + '                    ma.ModuleName,  ' + char(10)        
	 + '                    m2.PK_Module_ID  ' + char(10)        
	 + '         )dataapprovalbyrole  ' + char(10)        
	 + '  WHERE  dataapprovalbyrole.jmlworkflow = 0  ' + char(10)  
     + ')xx ' + CHAR(10)    
     + 'INNER JOIN ( ' + CHAR(10)    
     + ' ' + CHAR(10)    
     +    
     'SELECT distinct m.Modulename as namamodule,''/Parameter/WaitingApproval.aspx'' mMenuURL,mms.FK_Module_ID '    
     + CHAR(10)    
     + '  FROM MGroupMenuSettting mms ' + CHAR(10)    
     + 'INNER JOIN Module m ON mms.FK_Module_ID=m.PK_Module_ID ' + CHAR(10)    
     + 'WHERE mms.FK_MGroupMenu_ID=' + CONVERT(VARCHAR(50), @groupmenuid) +    
     ' AND mms.FK_Action_ID=' + CONVERT(VARCHAR(50), @actionid)    
     + CHAR(10)    
       
  
     + ')yy ON xx.NamaModule=yy.namamodule ' + CHAR(10)    
   +' INNER JOIN Module ab ON ab.ModuleName=xx.NamaModule '  
  
  
 SET @sql = @sql + ') select * From pageindex'    
 IF LEN(@whereclause) > 0    
     SET @sql = @sql + ' where ' + @whereclause    
 SET @sql = @sql + ' ORDER BY  ' + @orderby + CHAR(10)    
 PRINT @sql    
 EXEC (@sql)    
END  
GO
