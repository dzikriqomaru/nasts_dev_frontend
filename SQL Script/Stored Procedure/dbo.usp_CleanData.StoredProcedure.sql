/****** Object:  StoredProcedure [dbo].[usp_CleanData]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_CleanData]
as

delete TextFileTemporaryTable
delete GeneratedFileList
delete AuditTrailHeader
truncate table AuditTrail_UserAccess
truncate table AuditTrail_UserLogin
truncate table ELMAH_Error
truncate table Web_ValidationReport_Row 
truncate table Web_ValidationReport_RowComplate
truncate table ValidationSummary
truncate table ValidationReport

truncate table EODTaskLog
truncate table EODTaskDetailLog
truncate table EODSchedulerLog
truncate table SystemCalendar

--delete F101 where PK_F101_ID not in  (select top 10 PK_F101_ID from F101)
--delete F102K where PK_F102K_ID not in  (select top 10 PK_F102K_ID from F102K)
--delete F102S where PK_F102S_ID not in  (select top 10 PK_F102S_ID from F102S)
--delete F201K where PK_F201K_ID not in  (select top 10 PK_F201K_ID from F201K)
--delete F201S where PK_F201S_ID not in  (select top 10 PK_F201S_ID from F201S)
--delete F202K where PK_F202K_ID not in  (select top 10 PK_F202K_ID from F202K)
--delete F202S where PK_F202S_ID not in  (select top 10 PK_F202S_ID from F202S)
--delete F203 where PK_F203_ID not in  (select top 10 PK_F203_ID from F203)
--delete F204 where PK_F204_ID not in  (select top 10 PK_F204_ID from F204)
--delete F205 where PK_F205_ID not in  (select top 10 PK_F205_ID from F205)
--delete F206 where PK_F206_ID not in  (select top 10 PK_F206_ID from F206)
--delete F207K where PK_F207K_ID not in  (select top 10 PK_F207K_ID from F207K)
--delete F207S where PK_F207S_ID not in  (select top 10 PK_F207S_ID from F207S)
--delete F301K where PK_F301K_ID not in  (select top 10 PK_F301K_ID from F301K)
--delete F301S where PK_F301S_ID not in  (select top 10 PK_F301S_ID from F301S)
--delete F501 where PK_F501_ID not in  (select top 10 PK_F501_ID from F501)
--delete F604 where PK_F604_ID not in  (select top 10 PK_F604_ID from F604)

--truncate table F101
--truncate table F102K
--truncate table F102S
--truncate table F201K
--truncate table F201S
--truncate table F202K
--truncate table F202S
--truncate table F203
--truncate table F204
--truncate table F205
--truncate table F206
--truncate table F207K
--truncate table F207S
--truncate table F301K
--truncate table F301S
--truncate table F501
--truncate table F604
GO
