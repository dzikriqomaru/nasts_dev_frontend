/****** Object:  StoredProcedure [dbo].[usp_InjectModule]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[usp_InjectModule]
/***********************************************************
* Procedure description:
* Date:   5/5/2017 
* Author: Hendra
*
* Changes
* Date		Modified By			Comments
************************************************************
*
************************************************************/
(
	@TableName VARCHAR(200),@ModuleName VARCHAR(200),@IsUseDesigner BIT,@IsUseApproval BIT,@IsSupportAdd BIT,@IsSupportEdit BIT,@IsSupportDelete BIT,@IsSupportActivation BIT,@IsSupportView BIT, @IsSupportpload BIT,@IsSupportDetail BIT	
)
AS
BEGIN
	
	
	--declare @TableName VARCHAR(200),@ModuleName VARCHAR(200),@IsUseDesigner BIT,@IsUseApproval BIT,@IsSupportAdd BIT,@IsSupportEdit BIT,@IsSupportDelete BIT,@IsSupportActivation BIT,@IsSupportView BIT, @IsSupportpload BIT,@IsSupportDetail BIT
	--SET @TableName='TV_TransvisionID'
	--SET @ModuleName='TV_TransvisionID'
	--SET @IsUseDesigner=1
	--SET @IsUseApproval=1
	--SET @IsSupportAdd=1
	--SET @IsSupportEdit=1
	--SET @IsSupportDelete=1
	--SET @IsSupportActivation=1
	--SET @IsSupportView=1
	--SET @IsSupportpload=1
	--SET @IsSupportDetail=1
	
	DECLARE @pk VARCHAR(200)=''
	INSERT INTO Module
	(
		-- PK_Module_ID -- this column value is auto-generated
		ModuleName,
		ModuleLabel,
		ModuleDescription,
		IsUseDesigner,
		IsUseApproval,
		IsSupportAdd,
		IsSupportEdit,
		IsSupportDelete,
		IsSupportActivation,
		IsSupportView,
		IsSupportUpload,
		IsSupportDetail,
		UrlAdd,
		UrlEdit,
		UrlDelete,
		UrlActivation,
		UrlView,
		UrlUpload,
		UrlApproval,
		UrlApprovalDetail,
		UrlDetail,
		IsUseStoreProcedureValidation,
		[Active],
		CreatedBy,
		LastUpdateBy,
		ApprovedBy,
		CreatedDate,
		LastUpdateDate,
		ApprovedDate
	)
	VALUES
	(
		@TableName,
		@ModuleName,
		'',
		1,
		@IsUseApproval,
		@IsSupportAdd,
		@IsSupportEdit,
		@IsSupportDelete,
		@IsSupportActivation,
		@IsSupportView,
		@IsSupportpload,
		@IsSupportDetail,
		'/Parameter/ParameterAdd.aspx',
		'/Parameter/ParameterEdit.aspx',
		'/Parameter/ParameterDelete.aspx',
		'/Parameter/ParameterActivation.aspx',
		'/Parameter/ParameterView.aspx',
		'/Parameter/ParameterUpload.aspx',
		'/Parameter/ParameterApproval.aspx',
		'/Parameter/ParameterApprovalDetail.aspx',
		'/Parameter/ParameterDetail.aspx',
		0,
		1,
		'sysadmin',
		'sysadmin',
		'sysadmin',
		GETDATE(),
		GETDATE(),
		GETDATE()
	)


	DECLARE @pkmodule INT 
	set @pkmodule =SCOPE_IDENTITY()

SELECT @pk = kcu.COLUMN_NAME
    
FROM    INFORMATION_SCHEMA.TABLE_CONSTRAINTS p
inner join INFORMATION_SCHEMA.KEY_COLUMN_USAGE kcu
        on p.CONSTRAINT_TYPE = 'PRIMARY KEY'
           and kcu.TABLE_CATALOG = p.TABLE_CATALOG
           AND kcu.TABLE_SCHEMA = p.TABLE_SCHEMA
           AND kcu.TABLE_NAME = p.TABLE_NAME
           AND kcu.CONSTRAINT_NAME = p.CONSTRAINT_NAME
inner join information_schema.columns c
        on c.TABLE_CATALOG = p.TABLE_CATALOG
           AND c.TABLE_SCHEMA = p.TABLE_SCHEMA
           AND c.TABLE_NAME = p.TABLE_NAME
           AND c.column_name = kcu.column_name
AND kcu.TABLE_NAME = @TableName	
	

DECLARE @TABLE_CATALOG nvarchar(128), @TABLE_SCHEMA nvarchar(128),
        @TABLE_NAME sysname, @COLUMN_NAME sysname, @ORDINAL_POSITION int,
        @COLUMN_DEFAULT nvarchar(4000), @IS_NULLABLE varchar(3),
        @DATA_TYPE nvarchar(128), @CHARACTER_MAXIMUM_LENGTH int,
        @CHARACTER_OCTET_LENGTH int, @NUMERIC_PRECISION tinyint,
        @NUMERIC_PRECISION_RADIX smallint, @NUMERIC_SCALE int,
        @DATETIME_PRECISION smallint, @CHARACTER_SET_CATALOG sysname,
        @CHARACTER_SET_SCHEMA sysname, @CHARACTER_SET_NAME sysname,
        @COLLATION_CATALOG sysname, @COLLATION_SCHEMA sysname,
        @COLLATION_NAME sysname, @DOMAIN_CATALOG sysname,
        @DOMAIN_SCHEMA sysname, @DOMAIN_NAME sysname

DECLARE my_cursor CURSOR FAST_FORWARD READ_ONLY FOR
SELECT TABLE_CATALOG, TABLE_SCHEMA, TABLE_NAME, COLUMN_NAME, ORDINAL_POSITION,
       COLUMN_DEFAULT, IS_NULLABLE, DATA_TYPE, CHARACTER_MAXIMUM_LENGTH,
       CHARACTER_OCTET_LENGTH, NUMERIC_PRECISION, NUMERIC_PRECISION_RADIX,
       NUMERIC_SCALE, DATETIME_PRECISION, CHARACTER_SET_CATALOG,
       CHARACTER_SET_SCHEMA, CHARACTER_SET_NAME, COLLATION_CATALOG,
       COLLATION_SCHEMA, COLLATION_NAME, DOMAIN_CATALOG, DOMAIN_SCHEMA,
       DOMAIN_NAME
FROM INFORMATION_SCHEMA.[COLUMNS] WHERE TABLE_NAME=@TableName
AND COLUMN_NAME NOT IN (
'Active',
'CreatedBy',
'LastUpdateBy',
'ApprovedBy',
'CreatedDate',
'LastUpdateDate',
'ApprovedDate'	
)

OPEN my_cursor

FETCH FROM my_cursor INTO @TABLE_CATALOG, @TABLE_SCHEMA, @TABLE_NAME,
                          @COLUMN_NAME, @ORDINAL_POSITION, @COLUMN_DEFAULT,
                          @IS_NULLABLE, @DATA_TYPE, @CHARACTER_MAXIMUM_LENGTH,
                          @CHARACTER_OCTET_LENGTH, @NUMERIC_PRECISION,
                          @NUMERIC_PRECISION_RADIX, @NUMERIC_SCALE,
                          @DATETIME_PRECISION, @CHARACTER_SET_CATALOG,
                          @CHARACTER_SET_SCHEMA, @CHARACTER_SET_NAME,
                          @COLLATION_CATALOG, @COLLATION_SCHEMA,
                          @COLLATION_NAME, @DOMAIN_CATALOG, @DOMAIN_SCHEMA,
                          @DOMAIN_NAME

WHILE @@FETCH_STATUS = 0
BEGIN
	
	DECLARE @isprimary BIT
	DECLARE @isunik BIT=0
	DECLARE @fieldunik VARCHAR(200)=''
	DECLARE @isidentity BIT=0
	DECLARE @mfieldtype int
	DECLARE @sizefiled INT
	
	DECLARE @extid INT 
	
	SET @sizefiled=@CHARACTER_MAXIMUM_LENGTH
	IF @CHARACTER_MAXIMUM_LENGTH=-1 SET @sizefiled=8000
	
	
SELECT @fieldunik = kcu.COLUMN_NAME
    
FROM    INFORMATION_SCHEMA.TABLE_CONSTRAINTS p
inner join INFORMATION_SCHEMA.KEY_COLUMN_USAGE kcu
        on p.CONSTRAINT_TYPE = 'UNIQUE'
           and kcu.TABLE_CATALOG = p.TABLE_CATALOG
           AND kcu.TABLE_SCHEMA = p.TABLE_SCHEMA
           AND kcu.TABLE_NAME = p.TABLE_NAME
           AND kcu.CONSTRAINT_NAME = p.CONSTRAINT_NAME
inner join information_schema.columns c
        on c.TABLE_CATALOG = p.TABLE_CATALOG
           AND c.TABLE_SCHEMA = p.TABLE_SCHEMA
           AND c.TABLE_NAME = p.TABLE_NAME
           AND c.column_name = kcu.column_name
AND kcu.TABLE_NAME = @TableName

IF @fieldunik=@COLUMN_NAME SET @isunik=1

	set @isprimary=0 
	IF @pk=@COLUMN_NAME SET @isprimary =1
	IF @isprimary=1 SET @isunik=1
	 



SELECT @mfieldtype=mt.PK_FieldType_ID
  FROM MFieldType mt WHERE mt.FieldTypeSQLName LIKE '%'+@DATA_TYPE +'%'
AND mt.PK_FieldType_ID NOT IN (11,12)



 IF @DATA_TYPe='datetime' SET @extid=1
 IF @DATA_TYPE='varchar' SET @extid=5
 IF (@DATA_TYPE='int' OR @DATA_TYPE='bigint' ) AND @isidentity=1 SET @extid=5
   IF (@DATA_TYPE='int' OR @DATA_TYPE='bigint' ) AND @isidentity=0 SET @extid=4



select @isidentity =columnproperty(object_id(@TableName),@COLUMN_NAME,'IsIdentity')
IF @isidentity=1 SET @mfieldtype=12

	DECLARE @required BIT=0
	IF @IS_NULLABLE='NO' SET @required=1
	
	
	INSERT INTO ModuleField
	(
		-- PK_ModuleField_ID -- this column value is auto-generated
		FK_Module_ID,
		FieldName,
		FieldLabel,
		Sequence,
		[Required],
		IsPrimaryKey,
		IsUnik,
		IsShowInView,
		IsShowInForm,
		DefaultValue,
		FK_FieldType_ID,
		SizeField,
		FK_ExtType_ID,
		TabelReferenceName,
		TabelReferenceNameAlias,
		TableReferenceFieldKey,
		TableReferenceFieldDisplayName,
		TableReferenceFilter,
		IsUseRegexValidation,
		TableReferenceAdditonalJoin,
		BCasCade,
		FieldNameParent,
		FilterCascade
	)
	VALUES
	(
	@pkmodule ,
		@COLUMN_NAME,
		@COLUMN_NAME,
		@ORDINAL_POSITION,
		@required,
		@isprimary,
		@isunik,
		1,
		1,
		'',
		@mfieldtype,
		@sizefiled,
		@extid,
		'',
		'',
		'',
		'',
		'',
		0,
		'',
		null,
		'',
		''
	)
	
	
	

	FETCH FROM my_cursor INTO @TABLE_CATALOG, @TABLE_SCHEMA, @TABLE_NAME,
	                          @COLUMN_NAME, @ORDINAL_POSITION, @COLUMN_DEFAULT,
	                          @IS_NULLABLE, @DATA_TYPE,
	                          @CHARACTER_MAXIMUM_LENGTH,
	                          @CHARACTER_OCTET_LENGTH, @NUMERIC_PRECISION,
	                          @NUMERIC_PRECISION_RADIX, @NUMERIC_SCALE,
	                          @DATETIME_PRECISION, @CHARACTER_SET_CATALOG,
	                          @CHARACTER_SET_SCHEMA, @CHARACTER_SET_NAME,
	                          @COLLATION_CATALOG, @COLLATION_SCHEMA,
	                          @COLLATION_NAME, @DOMAIN_CATALOG, @DOMAIN_SCHEMA,
	                          @DOMAIN_NAME
END

CLOSE my_cursor
DEALLOCATE my_cursor

	
exec usp_generateTable @pkmodule
exec usp_GenerateTableUpload	@pkmodule
	
	
END
GO
