/****** Object:  StoredProcedure [dbo].[usp_batch_CleansingData]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_batch_CleansingData]
(
	@PK_EODTaskDetailLog_ID bigint,
	@DataDate DATETIME
)
AS
BEGIN

	--EXEC usp_InsertCleansingReport @DataDate
	--EXEC usp_CleanRecordsImmediate @DataDate
	EXEC usp_InsertCleansingReport @DataDate, '001'
	EXEC usp_UpdateCleansingRules @DataDate

END
GO
