/****** Object:  StoredProcedure [dbo].[usp_EMailScheduler_Create]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_EMailScheduler_Create]
(@PK_EmailTemplate_ID BIGINT, @recordID VARCHAR(MAX), @EmailIDReference VARCHAR(250))
AS
BEGIN
	--declare @PK_EmailTemplate_ID BIGINT, @recordID VARCHAR(MAX), @EmailIDReference VARCHAR(250)
	--SET @PK_EmailTemplate_ID=11
	--SET @recordID=format(GETDATE(),'ddMMyyyyHHmmss')
	
	DECLARE @Tab TABLE
	        (
	            PK_EmailTemplateScheduler_ID VARCHAR(20),
	            FK_EmailTemplate_ID VARCHAR(20),
	            ProcessDate DATETIME,
	            FK_EmailStatus_ID VARCHAR(20),
	            PK_EmailTemplate_ID VARCHAR(20),
	            EmailTemplateName VARCHAR(550),
	            EmailTo VARCHAR(1000),
	            EmailCC VARCHAR(1000),
	            EmailBCC VARCHAR(1000),
	            EmailSubject VARCHAR(1000),
	            EmailBody VARCHAR(MAX),
	            FK_Monitoringduration_ID VARCHAR(20),
	            StartDate DATETIME,
	            StartTime VARCHAR(50),
	            ExcludeHoliday BIT,
	            issupportemail BIT,
	            issupportnotification BIT,
	            [ACTIVE] BIT,
	            CreatedBy VARCHAR(50),
	            LastUpdateBy VARCHAR(50),
	            ApprovedBy VARCHAR(50),
	            CreatedDate DATETIME,
	            LastUpdateDate DATETIME,
	            ApprovedDate DATETIME,
				FK_EmailTemplateImportance_ID INT,
				FK_EmailTemplateSensitivity_ID INT,
				DeliveryReceipt BIT,
				ReadReceipt BIT
	        )
	
	DECLARE @NamaTable 
	        VARCHAR(250),
	        @QueryData                 VARCHAR(MAX),
	        @FieldUnikPrimaryTable     VARCHAR(250),
	        @SQL                       VARCHAR(MAX) 

	--Insert + Select Email Scheduler Header
	INSERT @Tab
	EXEC [usp_InserEmailSchedulerModuleApproval] 1,@PK_EmailTemplate_ID
	
	--Get email template additional
	SELECT @NamaTable = eta.NamaTable,
	       @QueryData                 = eta.QueryData,
	       @FieldUnikPrimaryTable     = eta.FieldUnikPrimaryTable
	FROM   EmailTemplateAdditional eta
	WHERE  eta.FK_EmailTableType_ID = 1
	       AND eta.FK_EmailTemplate_ID = @PK_EmailTemplate_ID
	
	
	--Replace Parameter ID dengan ID
	SET @QueryData = REPLACE(@QueryData, '@ID', '''' + @recordID + '''')
	SET @QueryData = REPLACE(@QueryData, '&#39;', '''')
	
	EXEC usp_CreateTableEmailPrimary @NamaTable,
	     @QueryData
	
	--Insert Scheduler Detail
	SET @SQL = 'INSERT INTO EmailTemplateSchedulerDetail '
	    + CHAR(10) + ' ( '
	    + CHAR(10) + ' 	 '
	    + CHAR(10) + ' 	FK_EmailTEmplateScheduler_ID, '
	    + CHAR(10) + ' 	UnikFieldTablePrimary, '
	    + CHAR(10) + ' 	EmailTo, '
	    + CHAR(10) + ' 	EmailCC, '
	    + CHAR(10) + ' 	EmailBCC, '
	    + CHAR(10) + ' 	EmailSubject, '
	    + CHAR(10) + ' 	EmailBody, '
	    + CHAR(10) + ' 	ProcessDate, '
	    + CHAR(10) + ' 	SendEmailDate, '
	    + CHAR(10) + ' 	FK_EmailStatus_ID, '
	    + CHAR(10) + ' 	ErrorMessage, '
	    + CHAR(10) + ' 	retrycount, '
		+ CHAR(10) + ' 	EmailIDReference '
	    + CHAR(10) + ' ) '
	    + CHAR(10) + ' select  '
	    + CHAR(10) + '''' + (
	        SELECT PK_EmailTemplateScheduler_ID
	        FROM   @Tab
	    ) + ''' ,'
	    + CHAR(10) + '' + @FieldUnikPrimaryTable + ' ,'
	    + CHAR(10) + '''' + (
	        SELECT EmailTo
	        FROM   @Tab
	    ) + ''','
	    + CHAR(10) + '''' + (
	        SELECT EmailCC
	        FROM   @Tab
	    ) + ''','
	    + CHAR(10) + '''' + (
	        SELECT EmailBCC
	        FROM   @Tab
	    ) + ''','
	    + CHAR(10) + '''' + (
	        SELECT EmailSubject
	        FROM   @Tab
	    ) 
	    + ''','
	    + CHAR(10) + '''' + (
	        SELECT EmailBody
	        FROM   @Tab
	    ) + ''','
	    + CHAR(10) + '''' + CAST(
	        (
	            SELECT ProcessDate
	            FROM   @Tab
	        ) AS VARCHAR
	    ) + ''','
	    + CHAR(10) + 'null,'
	    + CHAR(10) + '''1'','
	    + CHAR(10) + ''''','
	    + CHAR(10) + '''0'','
		+ CHAR(10) + '''' + @EmailIDReference + ''''
	    + CHAR(10) + ' from __' + @NamaTable
	    
	PRINT @SQL
	EXEC (@SQL)
	--insert table detail untuk tiap master
	DECLARE @UnikFieldTablePrimary VARCHAR(250),
	        @PK_EmailTemplateSchedulerDetail_ID BIGINT,
			@EmailID VARCHAR(250)
	
	DECLARE my_cursor CURSOR FAST_FORWARD READ_ONLY 
	FOR
	    SELECT UnikFieldTablePrimary,
	           PK_EmailTemplateSchedulerDetail_ID,
			   EmailID
	    FROM   EmailTemplateSchedulerDetail
	    WHERE  FK_EmailTEmplateScheduler_ID = (
	               SELECT PK_EmailTemplateScheduler_ID
	               FROM   @Tab
	           )
	
	OPEN my_cursor
	
	FETCH FROM my_cursor INTO @UnikFieldTablePrimary,@PK_EmailTemplateSchedulerDetail_ID,@EmailID
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
	    DECLARE @QueryData2        VARCHAR(MAX),
	            @NamaTable2        VARCHAR(250),
	            @Replacer VARCHAR(MAX),
	            @FieldReplacer     VARCHAR(MAX)
	    
	    DECLARE my_cursor2 CURSOR FAST_FORWARD READ_ONLY 
	    FOR
	        SELECT eta.NamaTable,
	               eta.QueryData
	        FROM   EmailTemplateAdditional eta
	        WHERE  eta.FK_EmailTableType_ID = 2
	               AND eta.FK_EmailTemplate_ID = (
	                       SELECT PK_EmailTemplate_ID
	                       FROM   @Tab
	                   )
	    OPEN my_cursor2
	    
	    FETCH FROM my_cursor2 
	    INTO @NamaTable2,@QueryData2
	    
	    WHILE @@FETCH_STATUS = 0
	    BEGIN
	        PRINT @QueryData2
			
	        SET @QueryData2 = REPLACE(@QueryData2, '@ID', @UnikFieldTablePrimary)
	        EXEC usp_CreateTableEmailPrimary @NamaTable2,
	             @QueryData2
	        FETCH FROM my_cursor2 INTO @NamaTable2,@QueryData2
	    END
	    
	    CLOSE my_cursor2
	    DEALLOCATE my_cursor2 
	    
		-- Attachment
		INSERT INTO [dbo].[EmailTemplateSchedulerDetailAttachment] ([FK_EmailTEmplateSchedulerDetail_ID], [FK_EmailAttachmentType_ID], [NamaReport], [ParameterReport], [IsiFile], [FileName], EmailRenderAsName, FileExtension, MimeType) 
        Select EmailTemplateSchedulerDetail.PK_EmailTemplateSchedulerDetail_ID, EmailTemplateAttachment.FK_EmailAttachmentType_ID, EmailTemplateAttachment.NamaReport, EmailTemplateAttachment.ParameterReport, EmailTemplateAttachment.IsiFile, EmailTemplateAttachment.NamaFile, EmailRenderAs.EmailRenderAsName, EmailRenderAs.FileExtension, EmailRenderAs.MimeType
        From EmailTemplateSchedulerDetail INNER Join
        EmailTemplateScheduler On EmailTemplateSchedulerDetail.FK_EmailTEmplateScheduler_ID = EmailTemplateScheduler.PK_EmailTemplateScheduler_ID INNER Join 
        EmailTemplateAttachment On EmailTemplateScheduler.PK_EmailTemplate_ID = EmailTemplateAttachment.FK_EmailTemplate_ID 
        INNER JOIN EmailRenderAs on EmailTemplateAttachment.FK_EmailRenderAs_Id=EmailRenderAs.PK_EmailRenderAs_ID
		Where EmailTemplateSchedulerDetail.PK_EmailTemplateSchedulerDetail_ID = @PK_EmailTemplateSchedulerDetail_ID

	    DECLARE my_cursor3 CURSOR FAST_FORWARD READ_ONLY 
	    FOR
	        SELECT etd.Replacer,
	               SUBSTRING(etd.FieldReplacer, 1, 1) + '__' + SUBSTRING(etd.FieldReplacer, 2, LEN(etd.FieldReplacer) -1) AS 
	               FieldReplacer
	        FROM   EmailTemplateDetail etd
	        WHERE  etd.FK_EmailTemplate_ID = (
	                   SELECT PK_EmailTemplate_ID
	                   FROM   @Tab
	               )
	    
	    OPEN my_cursor3
	    
	    FETCH FROM my_cursor3 INTO @Replacer,@FieldReplacer
	    --PRINT 'JALAN1'
	    
	    WHILE @@FETCH_STATUS = 0
	    BEGIN
	        DECLARE @PK_Email AS INT
	        
	        SELECT @PK_Email = PK_EmailTemplate_ID
	        FROM   @Tab
	        
	        --PRINT 'JALAN2a'
	        --PRINT @PK_EmailTemplateSchedulerDetail_ID
	        --PRINT @Replacer
	        --PRINT @FieldReplacer
	        --PRINT @PK_Email
	        
	        SET @Replacer = '''' + @Replacer + ''''
	        
	        EXEC usp_replaceEmailSchedulerDetail @PK_EmailTemplateSchedulerDetail_ID,
	             @Replacer,
	             @FieldReplacer,
	             @PK_Email
	        
	        --PRINT 'JALAN2b'
	        
	        FETCH FROM my_cursor3 INTO @Replacer,@FieldReplacer
	    END
	    
	    CLOSE my_cursor3
	    DEALLOCATE my_cursor3
	    
		-- Replace $EmailID$ With EmailID Field
		EXEC usp_replaceEmailSchedulerDetail @PK_EmailTemplateSchedulerDetail_ID,'$EmailID$',@EmailID, @PK_EmailTemplate_ID
		-- Update EmailIDReference with EmailID because of System Initiate
		UPDATE EmailTemplateSchedulerDetail 
		SET EmailIDReference=EmailID 
		WHERE PK_EmailTemplateSchedulerDetail_ID = @PK_EmailTemplateSchedulerDetail_ID 
		AND (EmailIDReference IS NULL OR EmailIDReference='')

		FETCH FROM my_cursor INTO @UnikFieldTablePrimary,@PK_EmailTemplateSchedulerDetail_ID,@EmailID
	END
	
	CLOSE my_cursor
	
	DEALLOCATE my_cursor
	
	DECLARE @PK_EmailTemplateScheduler_ID INT
	SELECT @PK_EmailTemplateScheduler_ID = PK_EmailTemplateScheduler_ID
	FROM   @Tab
	
	EXEC usp_updateEmailStatusScheduler @PK_EmailTemplateScheduler_ID
END

GO
