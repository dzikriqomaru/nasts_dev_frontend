/****** Object:  StoredProcedure [dbo].[usp_CountStringDecrypt]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_CountStringDecrypt]
(
@decryptedPass VARCHAR(MAX)
)
AS
BEGIN
RETURN LEN(@decryptedPass)
END
GO
