/****** Object:  StoredProcedure [dbo].[usp_CleanRecordsImmediate]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_CleanRecordsImmediate] 
(
	@DataDate DATETIME
)
	-- Add the parameters for the stored procedure here
AS
BEGIN
DECLARE	@bulan varchar(2),
	@tahun varchar(4)

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;	

	DECLARE @SQL VARCHAR(8000),
	 @TableName VARCHAR(255),
	 @FieldName VARCHAR(255),
	 @TextToReplace VARCHAR(255),
	 @ReplaceWith VARCHAR(255)

	 DECLARE my_cursor CURSOR FAST_FORWARD READ_ONLY 
	 FOR
	 SELECT [TableName]
	 ,[FieldName]
	 ,[TextToReplace]
	 ,[ReplaceWith]
	 FROM [CleansingDataDictionary]
	 WHERE NeedConfirmation = 0
	    
	 OPEN my_cursor
	    
	 FETCH FROM my_cursor INTO @TableName,@FieldName,@TextToReplace,@ReplaceWith

	 WHILE @@FETCH_STATUS = 0
	 BEGIN
	 DECLARE @ModuleName VARCHAR(255),
	 @ModuleFieldName VARCHAR(255)

	 SELECT @ModuleName = ModuleName FROM Module WHERE PK_Module_ID = @TableName
	 SET @ModuleFieldName = @FieldName

	 SET @SQL = 'UPDATE ' + @ModuleName
	 + ' SET ' + @ModuleFieldName + ' = REPLACE(' + @ModuleFieldName + ',''' + @TextToReplace + ''',''' + @ReplaceWith + ''')'
	 + ' WHERE Bulan= ' + @bulan + ' AND Tahun= ' + @tahun
	 --PRINT @SQL
	 EXEC (@SQL)

	 FETCH FROM my_cursor INTO @TableName,@FieldName,@TextToReplace,@ReplaceWith
	 END
	 
	 CLOSE my_cursor
	 DEALLOCATE my_cursor
END


GO
