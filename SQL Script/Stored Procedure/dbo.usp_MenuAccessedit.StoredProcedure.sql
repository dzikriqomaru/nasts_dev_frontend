/****** Object:  StoredProcedure [dbo].[usp_MenuAccessedit]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_MenuAccessedit]
/***********************************************************
* Procedure description:
* Date:   11/9/2019 
* Author: nawadata
*
* Changes
* Date			Modified By			Comments
************************************************************
* 31 Mar 2023	Humam				Add IconName
************************************************************/
(
    @groupmenuid                INT,
    @tblMGroupMenuAccess        AS tblMGroupMenuAccess READONLY,
    @tblMGroupMenuSettting      AS tblMGroupMenuSettting READONLY,
    @createdby                  VARCHAR(50),
    @Approveby                  VARCHAR(50),
    @FK_AuditTrailStatus_ID     INT,
    @FK_ModuleAction_ID         INT,
    @ModuleLabel                VARCHAR(50),
    @PK_ModuleApproval_ID       BIGINT
)
AS
BEGIN
	
	DELETE FROM MGroupMenuAccess WHERE FK_GroupMenu_ID=@groupmenuid
	DELETE FROM MGroupMenuSettting WHERE FK_MGroupMenu_ID=@groupmenuid
	INSERT INTO MGroupMenuAccess
	  (
	    -- PK_MGroupMenuAcess_ID -- this column value is auto-generated
	    FK_GroupMenu_ID,
	    FK_Module_ID,
	    bAdd,
	    bEdit,
	    bDelete,
	    bActivation,
	    bView,
	    bApproval,
	    bUpload,
	    bDetail
	  )
	SELECT FK_GroupMenu_ID,
	       FK_Module_ID,
	       bAdd,
	       bEdit,
	       bDelete,
	       bActivation,
	       bView,
	       bApproval,
	       bUpload,
	       bDetail
	FROM   @tblMGroupMenuAccess
	
	INSERT INTO MGroupMenuSettting
	  (
	    -- PK_MGroupMenuSettting_ID -- this column value is auto-generated
	    FK_MGroupMenu_ID,
	    mMenuID,
	    mMenuLabel,
	    mMenuParentID,
	    mMenuURL,
	    FK_Module_ID,
	    FK_Action_ID,
		IconName,
	    urutan
	  )
	SELECT @groupmenuid AS FK_MGroupMenu_ID, 
			temp.mMenuID, 
			temp.mMenuLabel, 
			temp.mMenuParentID, 
			temp.mMenuURL, 
			temp.FK_Module_ID,
			temp.FK_Action_ID, 
			temp.IconName,
			temp.urutan
	FROM MenuTemplate temp
	INNER JOIN MGroupMenuAccess acc
	ON acc.FK_GroupMenu_ID = @groupmenuid  AND temp.FK_Module_ID = acc.FK_Module_ID
	WHERE acc.FK_GroupMenu_ID = @groupmenuid
		AND (
			temp.FK_Action_ID = 0
			OR temp.FK_Action_ID IS NULL 
			OR (temp.FK_Action_ID = 5 AND acc.bView = 1) 
			OR (temp.FK_Action_ID = 6 AND acc.bApproval = 1)
			OR (temp.FK_Action_ID = 7 AND acc.bUpload = 1)
		)
	
	DECLARE @pkAudittrailheader BIGINT
	INSERT INTO AuditTrailHeader
	  (
	    -- PK_AuditTrail_ID -- this column value is auto-generated
	    CreatedDate,
	    CreatedBy,
	    ApproveBy,
	    ModuleLabel,
	    FK_ModuleAction_ID,
	    FK_AuditTrailStatus_ID,
	    PK_ModuleApproval_ID
	  )
	VALUES
	  (
	    GETDATE(),
	    @createdby,
	    @Approveby,
	    @ModuleLabel,
	    @FK_ModuleAction_ID,
	    @FK_AuditTrailStatus_ID,
	    @PK_ModuleApproval_ID
	  )
	
	SET @pkAudittrailheader = SCOPE_IDENTITY()
	
	INSERT INTO AuditTrailDetail
	  (
	    -- PK_AuditTrailDetail_id -- this column value is auto-generated
	    FK_AuditTrailHeader_ID,
	    FieldName,
	    OldValue,
	    NewValue
	  )
	SELECT @pkAudittrailheader     FK_AuditTrailHeader_ID,
	       ca.fieldname,
	       ''                      OldValue,
	       ca.NewValue
	FROM   (
	           SELECT *
	           FROM   MGroupMenuAccess AS mma
	           WHERE  mma.FK_GroupMenu_ID = @groupmenuid
	       )xx
	       CROSS APPLY (
	    VALUES 
	    (
	        'PK_MGroupMenuAcess_ID',
	        TRY_CONVERT(VARCHAR(8000), PK_MGroupMenuAcess_ID)
	    ),
	    (
	        'FK_GroupMenu_ID',
	        TRY_CONVERT(VARCHAR(8000), FK_GroupMenu_ID)
	    ),
	    ('FK_Module_ID', TRY_CONVERT(VARCHAR(8000), FK_Module_ID)),
	    ('bAdd', TRY_CONVERT(VARCHAR(8000), bAdd)),
	    ('bEdit', TRY_CONVERT(VARCHAR(8000), bEdit)),
	    ('bDelete', TRY_CONVERT(VARCHAR(8000), bDelete)),
	    ('bActivation', TRY_CONVERT(VARCHAR(8000), bActivation)),
	    ('bView', TRY_CONVERT(VARCHAR(8000), bView)),
	    ('bApproval', TRY_CONVERT(VARCHAR(8000), bApproval)),
	    ('bUpload', TRY_CONVERT(VARCHAR(8000), bUpload)),
	    ('bDetail', TRY_CONVERT(VARCHAR(8000), bDetail))
	)ca(Fieldname, NewValue)
	UNION
	ALL
	SELECT @pkAudittrailheader     FK_AuditTrailHeader_ID,
	       ca.fieldname,
	       ''                      OldValue,
	       ca.NewValue
	FROM   (
	           SELECT *
	           FROM   MGroupMenuSettting AS mma
	           WHERE  mma.FK_MGroupMenu_ID = @groupmenuid
	       )xx
	       CROSS APPLY (
	    VALUES 
	    (
	        'PK_MGroupMenuSettting_ID',
	        TRY_CONVERT(VARCHAR(8000), PK_MGroupMenuSettting_ID)
	    ),
	    (
	        'FK_MGroupMenu_ID',
	        TRY_CONVERT(VARCHAR(8000), FK_MGroupMenu_ID)
	    ),
	    ('mMenuID', TRY_CONVERT(VARCHAR(8000), mMenuID)),
	    ('mMenuLabel', TRY_CONVERT(VARCHAR(8000), mMenuLabel)),
	    ('mMenuParentID', TRY_CONVERT(VARCHAR(8000), mMenuParentID)),
	    ('mMenuURL', TRY_CONVERT(VARCHAR(8000), mMenuURL)),
	    ('FK_Module_ID', TRY_CONVERT(VARCHAR(8000), FK_Module_ID)),
	    ('FK_Action_ID', TRY_CONVERT(VARCHAR(8000), FK_Action_ID)),
		('IconName', TRY_CONVERT(VARCHAR(8000), IconName)),
	    ('urutan', TRY_CONVERT(VARCHAR(8000), urutan))
	)ca(Fieldname, NewValue)

	SELECT @pkAudittrailheader AS PK_AuditTrail_ID
END
GO
