/****** Object:  StoredProcedure [dbo].[Usp_Console_InsertEODLog]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Usp_Console_InsertEODLog] (@datProcessDate DATETIME, @strUserId VARCHAR(50),@datadate datetime)
as
BEGIN
	
	
	--declare @datprocessdate datetime, @struserid varchar(50),@datadate datetime
	--set @datprocessdate=getdate()
	--set @struserid='sysadmin'
	--set @datadate='2016-06-30'
	DECLARE @PK_EODScheduler_ID bigint, @EODSchedulerName varchar(255),
        @EODSchedulerDescription varchar(8000), @EODPeriod int,
        @FK_MsEODPeriod int, @StartDate datetime, @Active bit
     
	
	DECLARE my_cursor CURSOR FAST_FORWARD READ_ONLY FOR
	SELECT e.PK_EODScheduler_ID, e.EODSchedulerName, e.EODSchedulerDescription,
	       e.EODPeriod, e.FK_MsEODPeriod, e.StartDate, e.[Active]
	FROM dbo.EODScheduler e
		LEFT JOIN EODSchedulerLog   es
	ON e.PK_EODScheduler_ID = es.FK_EODSchedulerID
	AND es.ProcessDate = dbo.Ufn_GetLastEODScheduler(e.PK_EODScheduler_ID, @datProcessDate)
	WHERE es.PK_EODSchedulerLog_ID  IS NULL
	AND DATEDIFF(ss, e.StartDate, @datProcessDate) >= 0
	AND ISNULL(e.[Active], 0) = 1
	AND e.HasPeriodikScheduler=1
	
	
	OPEN my_cursor
	
	FETCH FROM my_cursor INTO @PK_EODScheduler_ID, @EODSchedulerName,
	                          @EODSchedulerDescription, @EODPeriod,
	                          @FK_MsEODPeriod, @StartDate, @Active
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
		
		DECLARE @PK_EODSchedulerLog_ID BIGINT
		print (@PK_EODScheduler_ID)
		INSERT INTO EODSchedulerLog
		(
			-- PK_EODSchedulerLog_ID -- this column value is auto-generated
			FK_EODSchedulerID,
			ExecuteBy,
			ProcessDate,
			DataDate,
			StartDate,
			Enddate,
			ErrorMessage,
			FK_MsEODStatus_ID
		)
		VALUES
		(
			@PK_EODScheduler_ID,
			@strUserId,
			dbo.Ufn_GetLastEODScheduler(@PK_EODScheduler_ID, @datProcessDate),
			@datadate,
			null,			
			null,
			'',
			1 --onqueue
		)
		set @PK_EODSchedulerLog_ID=SCOPE_IDENTITY()
		
		--insert eodtasklog	
		--SELECT * FROM EODTaskLog el
		
		DECLARE @PK_EODTask_ID bigint, @EODTaskName varchar(50),@FK_EODSCheduler_ID bigint ,
        @EODTaskDescription varchar(250), @ActiveTask bit
		
		DECLARE my_cursor1 CURSOR FAST_FORWARD READ_ONLY FOR
		SELECT PK_EODTask_ID, EODTaskName, EODTaskDescription, [Active],ed.FK_EODSCheduler_ID
		FROM dbo.EODTask e
		INNER JOIN EODSchedulerDetail ed ON e.PK_EODTask_ID=ed.FK_EODTask_ID
		WHERE ed.FK_EODSCheduler_ID=@PK_EODScheduler_ID
		ORDER BY ed.OrderNo
		
		OPEN my_cursor1
		
		FETCH FROM my_cursor1 INTO @PK_EODTask_ID, @EODTaskName,
		                          @EODTaskDescription, @ActiveTask,@FK_EODSCheduler_ID
		
		WHILE @@FETCH_STATUS = 0
		BEGIN
			
			DECLARE @PK_EODTaskLog_ID BIGINT
			INSERT INTO EODTaskLog
		(
			-- PK_EODTaskLog_ID -- this column value is auto-generated
			EODSchedulerLogID,
			FK_EODTaskID,
			ExecuteBy,			
			StartDate,
			Enddate,
			ErrorMessage,
			FK_MsEODStatus_ID
		)
		VALUES
		(
			@PK_EODSchedulerLog_ID,
			@PK_EODTask_ID,
			@strUserId,			
			null,
			null,
			'',
			1
		)
		
			set @PK_EODTaskLog_ID=SCOPE_IDENTITY()
			
		
		DECLARE @PK_EODTaskDetail_ID bigint, @FK_EODTask_ID bigint,
        @FK_EODTaskDetailType_ID int, @OrderNo int, @SSISName varchar(250),
        @SSISFIle varbinary(max), @StoreProcedureName varchar(250),
        @Keterangan varchar(8000), @IsUseParameterProcessDate bit
		
		DECLARE my_cursor2 CURSOR FAST_FORWARD READ_ONLY FOR
		SELECT PK_EODTaskDetail_ID, FK_EODTask_ID, FK_EODTaskDetailType_ID,
		       OrderNo, SSISName, SSISFIle, StoreProcedureName, Keterangan,
		       IsUseParameterProcessDate
		FROM dbo.EODTaskDetail ed WHERE ed.FK_EODTask_ID=@PK_EODTask_ID ORDER BY ed.OrderNo
		
		OPEN my_cursor2
		
		FETCH FROM my_cursor2 INTO @PK_EODTaskDetail_ID, @FK_EODTask_ID,
		                          @FK_EODTaskDetailType_ID, @OrderNo, @SSISName,
		                          @SSISFIle, @StoreProcedureName, @Keterangan,
		                          @IsUseParameterProcessDate
		
		WHILE @@FETCH_STATUS = 0
		BEGIN
			
			INSERT INTO EODTaskDetailLog
			(
				-- PK_EODTaskDetailLog_ID -- this column value is auto-generated
				EODSchedulerLogID,
				EODTaskLogID,
				FK_EODTAskDetail_ID,
				ExecuteBy,
				StartDate,
				Enddate,
				ErrorMessage,
				FK_MsEODStatus_ID
			)
			VALUES
			(
				@PK_EODSchedulerLog_ID,
				@PK_EODTaskLog_ID,
				@PK_EODTaskDetail_ID,
				@strUserId,
				null,
				null,
				'',
				1
			)
		
			FETCH FROM my_cursor2 INTO @PK_EODTaskDetail_ID, @FK_EODTask_ID,
			                          @FK_EODTaskDetailType_ID, @OrderNo,
			                          @SSISName, @SSISFIle, @StoreProcedureName,
			                          @Keterangan, @IsUseParameterProcessDate
		END
		
		CLOSE my_cursor2
		DEALLOCATE my_cursor2
		
		
				
			
			
		
			FETCH FROM my_cursor1 INTO @PK_EODTask_ID, @EODTaskName,
			                          @EODTaskDescription, @Active,@FK_EODSCheduler_ID
		END
		
		CLOSE my_cursor1
		DEALLOCATE my_cursor1
		
		
		
		FETCH FROM my_cursor INTO @PK_EODScheduler_ID, @EODSchedulerName,
		                          @EODSchedulerDescription, @EODPeriod,
		                          @FK_MsEODPeriod, @StartDate, @Active
		                       
	END
	
	CLOSE my_cursor
	DEALLOCATE my_cursor
	
	
	
	
END
GO
