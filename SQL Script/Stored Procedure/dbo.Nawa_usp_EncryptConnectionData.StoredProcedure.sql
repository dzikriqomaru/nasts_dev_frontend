/****** Object:  StoredProcedure [dbo].[Nawa_usp_EncryptConnectionData]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Nawa_usp_EncryptConnectionData]  
/*********************  
* Procedure description:  
* Date:   9/2/2017   
* Author: Hendra  
*  
* Changes  
* Date  Modified By   Comments  
********************  
*  
********************/  
(  

 @ConnecitonName VARCHAR(8000)   
)  
AS  
BEGIN  

DECLARE @Password VARCHAR(8000)
SELECT @Password =PasswordValue FROM 	NawadataConnectionString WHERE ConnecitonName=@ConnecitonName


OPEN SYMMETRIC KEY ConfigSymKey DECRYPTION BY CERTIFICATE ConfigCert
 
	UPDATE NawadataConnectionString
	SET Passwordbinary =  EncryptByKey(Key_GUID('ConfigSymKey'), ISNULL( PasswordValue,'') ),
	PasswordValue = '0x'+ CONVERT(VARCHAR(MAX),EncryptByKey(Key_GUID('ConfigSymKey'), ISNULL( PasswordValue,'') ),2)
	WHERE ConnecitonName=@ConnecitonName
	

END

GO
