/****** Object:  StoredProcedure [dbo].[usp_GetPasswordExpiredNotification]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_GetPasswordExpiredNotification]
@UserID varchar(50)
AS

select 
'Your password will expired in ' +cast(ExpiredPeriod - DATEDIFF(DAY,LastChangePassword,GETDATE()) as varchar(10))+' days'
from mUser A
CROSS APPLY (SELECT SettingValue AS ExpiredPeriod FROM SystemParameter where PK_SystemParameter_ID =1000)b
where ( DATEDIFF(DAY,LastChangePassword,GETDATE()) in (ExpiredPeriod-30,ExpiredPeriod-15) or ExpiredPeriod - DATEDIFF(DAY,LastChangePassword,GETDATE()) <=7)
	AND A.UserID =@UserID
	and (select SettingValue from SystemParameter where PK_SystemParameter_ID = 3) = 2
GO
