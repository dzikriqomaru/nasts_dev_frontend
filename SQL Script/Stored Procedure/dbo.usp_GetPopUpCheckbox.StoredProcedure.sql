/****** Object:  StoredProcedure [dbo].[usp_GetPopUpCheckbox]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_GetPopUpCheckbox] 
/***********************************************************
* Function description:
* Date:   7/18/2019  
* Author: nawadata
*
* Changes
* Date		Modified By			Comments
************************************************************
*
************************************************************/
(
	@tablename VARCHAR(500),@Displayfieldname VARCHAR(500),@KeyFieldName VARCHAR(500),@valuefield VARCHAR(MAX),@delimiter VARCHAR(10)
) 
as
BEGIN
	
	
	DECLARE @sql varchar(max)
SET @sql = ' ' + char(10)
         + 'SELECT ISNULL( stuff((  ' + char(10)
         + 'SELECT '''+@delimiter+''' + ' + char(10)
         + '	 '+ @tablename +'.'+@Displayfieldname+' ' + char(10)
         + 'FROM ' + char(10)
         + '	dbo.[Split]( ' + char(10)
         + ''''+ @valuefield +''','''+@delimiter+''') AS s ' + char(10)
         + 'INNER JOIN '+@tablename +' ON '+ @tablename +'.'+ @KeyFieldName +'=s.val ' + char(10)
         + ' FOR XML PATH('''') ' + char(10)
         + '    ), 1, 1, ''''),'''') AS Displayfield;'
         PRINT @sql
EXEC(@sql)

	
END
GO
