/****** Object:  StoredProcedure [dbo].[usp_UpdateCleansingRules]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   PROCEDURE [dbo].[usp_UpdateCleansingRules] 
(
	@DataDate DATETIME
)
AS  
BEGIN
	DECLARE @SQLQuery AS VARCHAR(MAX) = '', 
		@SQLUpdate AS VARCHAR(MAX) = '',
		@TableName AS VARCHAR(100),
		@FieldName AS VARCHAR(200),
		@FieldValue AS VARCHAR(MAX),
		@FilterExpr AS VARCHAR(MAX),
	    @Unique          VARCHAR(255),
		@ModuleID AS BIGINT,
		@ModuleLabel AS VARCHAR(MAX),
		@CleansingRules AS VARCHAR(MAX),
		@FK_FieldType_ID AS BIGINT

	DECLARE @UpdateCursor AS CURSOR;
 
	SET @UpdateCursor = CURSOR FOR
		SELECT M.ModuleName, CDR.FieldName, CDR.FieldValue, ISNULL(CDR.FilterExpression, 'WHERE 1=1'), 
			mfk.FieldName, m.PK_Module_ID, m.ModuleLabel, CDR.Keterangan, mf.FK_FieldType_ID
		FROM CleansingDataRules CDR
		INNER JOIN Module M ON M.PK_Module_ID = CDR.TableName
		INNER JOIN ModuleField mf ON m.PK_Module_ID = mf.FK_Module_ID
		AND cdr.FieldName = mf.FieldName
	    LEFT JOIN ModuleField AS mfk
	        ON  mfk.FK_Module_ID = m.PK_Module_ID
				AND mfk.IsPrimaryKey = 1
		WHERE 1=1 ORDER BY CDR.NoUrut
 
	OPEN @UpdateCursor;
	FETCH NEXT FROM @UpdateCursor INTO @TableName, @FieldName, @FieldValue, @FilterExpr, @Unique, @ModuleID, 
		@ModuleLabel, @CleansingRules, @FK_FieldType_ID;
 
	WHILE @@FETCH_STATUS = 0
	BEGIN
		PRINT (@TableName)
		DECLARE @jmlFieldMatch int 
		SELECT @jmlFieldMatch =COUNT(1) FROM ModuleField mf
		INNER JOIN Module m ON m.PK_Module_ID = mf.FK_Module_ID 
		WHERE m.ModuleName = @TableName
		AND mf.FieldName IN ('Report_Date', 'Kode_Cabang_BI')

		SET @SQLUpdate = 'UPDATE ' + @TableName + ' ' +
					'SET ' + @FieldName + ' = ' + @FieldValue + ' ' +
					@FilterExpr + CHAR(10)

		IF @jmlFieldMatch = 2
		BEGIN
			If ISNULL(@FilterExpr, '') <> ''
			BEGIN
				SET @SQLUpdate = @SQLUpdate + ' AND Report_Date='''+ CONVERT(VARCHAR, @DataDate, 112) + ''''
			END
			ELSE
			BEGIN
				SET @SQLUpdate = @SQLUpdate + ' WHERE Report_Date='''+ CONVERT(VARCHAR, @DataDate, 112) + ''''
			END
		END

		DECLARE @StrOriginalValue AS VARCHAR(MAX), @StrChangeValue AS VARCHAR(MAX)
		IF @FK_FieldType_ID = '10'
		BEGIN
			SET @StrOriginalValue = 'CONVERT(VARCHAR, f.' + @FieldName + ', 111)'
			SET @StrChangeValue = 'CONVERT(VARCHAR, ' + @FieldValue + ', 111)'
		END
		ELSE
		BEGIN
			SET @StrOriginalValue = 'f.' + @FieldName
			SET @StrChangeValue = @FieldValue
		END

		DECLARE @SQLReport AS VARCHAR(MAX)
	    SET @SQLReport = 'INSERT INTO CleansingReport'
	        + CHAR(10) + '('
	        + CHAR(10) + CASE WHEN @jmlFieldMatch = 2 THEN 'Report_Date, Kode_Cabang_BI,' ELSE '' END
	        + CHAR(10) + '	SegmentData, CleansingRules, KeyField, KeyFieldValue, NamaField, OriginalValue, CleanedValue, [Status],'
	        + CHAR(10) + '	[Active], CreatedBy, LastUpdateBy, ApprovedBy, CreatedDate, LastUpdateDate, ApprovedDate,'
	        + CHAR(10) + '	UniqueField, UniqueValue, ModuleID, ModuleName,'
	        + CHAR(10) + '	Clean, [Keep], FieldName'
	        + CHAR(10) + ')'
	        + CHAR(10) + 'SELECT ' + CASE WHEN @jmlFieldMatch = 2 THEN 'f.Report_Date, f.Kode_Cabang_BI,' ELSE '' END + ' ''' + @ModuleLabel + ''', ''' +  
			+ CHAR(10) + REPLACE(@CleansingRules, '''', '''''') + ''', ''' +  @Unique
			+ ''', ' + 'f.' + @Unique + ', ''' + @FieldName 
	        + ''', ' + @StrOriginalValue + ', ' + @StrChangeValue
			+ ', ''CLEANED'','
	        + CHAR(10) + '''1'', ''System'', '
			+ CHAR(10) + '''System''' + ', NULL, GETDATE(), '
			+ CHAR(10) + 'GETDATE()' + ', NULL, '
	        + CHAR(10) + '''' + @Unique + ''', ' + REPLACE(@Unique,'cln.',@TableName+'.') + ', ''' + CONVERT(VARCHAR, @ModuleID) + ''', ''' + @TableName 
	        + ''','
	        + CHAR(10) + '1' + ', 0, '''+@FieldName+''''
	        + CHAR(10) + 'FROM ' + @TableName + ' AS f'
			+ CHAR(10) + @FilterExpr
	        --+ CHAR(10) + 'WHERE f.Report_Date = ''' + CONVERT(VARCHAR, @DataDate, 112) + ''' '
			--+ CHAR(10) + 'AND f.Kode_Cabang_BI = ''' + @KodeCabang + ''''

		IF @jmlFieldMatch = 2
		BEGIN
			If ISNULL(@FilterExpr, '') <> ''
			BEGIN
				SET @SQLReport = @SQLReport + ' AND Report_Date='''+ CONVERT(VARCHAR, @DataDate, 112) + ''''
			END
			ELSE
			BEGIN
				SET @SQLReport = @SQLReport + ' WHERE Report_Date='''+ CONVERT(VARCHAR, @DataDate, 112) + ''''
			END
		END

		PRINT(@SQLReport)
		EXEC(@SQLReport)

		PRINT(@SQLUpdate)
		EXEC(@SQLUpdate)

		FETCH NEXT FROM @UpdateCursor INTO @TableName, @FieldName, @FieldValue, @FilterExpr, @Unique, @ModuleID, 
			@ModuleLabel, @CleansingRules, @FK_FieldType_ID
	END


	CLOSE @UpdateCursor;
	DEALLOCATE @UpdateCursor;
END


GO
