CREATE OR ALTER PROCEDURE [dbo].[usp_ValidateWarningTableUpload]
/* =============================================
-- Author:		Dandi Juliandi
-- Create date: 19 April 2024
-- Description:	Validate Access on Upload
-- =============================================
* sp ini digunakan di:
1. usp_ValidateUploadFormModule

* Changes
* Date		Modified By			Comments
************************************************************
* 
************************************************************/
    @FieldName VARCHAR(250) = '',
    @FK_Module_ID BIGINT = 0,
	@strTableUploadName VARCHAR(250),
	@FieldLabel VARCHAR(250),
	@userid VARCHAR(250),
	@IsPrimaryKey BIT = 0,
	@Required BIT = 0
AS
BEGIN
	DECLARE @bAdd bit = NULL,
			@bEdit bit = NULL,
			@bDelete bit = NULL,
			@sql VARCHAR(MAX) = '',
			@fkMGroupMenuID BIGINT = 0;

	
	/*Get Access Field*/
	SELECT 
	@bAdd = bAdd, 
	@bEdit = bEdit, 
	@bDelete = bDelete 
	FROM MGroupMenuAccessField mga
	join muser mus on mga.FK_MGroupMenu_ID = mus.FK_MGroupMenu_ID and mus.UserID = @userid
	WHERE mga.FK_Module_ID = @FK_Module_ID 
	AND mga.ModuleField = @FieldName

	SET @bAdd = ISNULL(@bAdd, 1)
	SET @bEdit = ISNULL(@bEdit, 1)
	SET @bDelete = ISNULL(@bEdit, 1)

	/*Cek Column Nawa_Warning*/
	IF NOT EXISTS (
		SELECT 1 
		FROM sys.columns c 
		JOIN sys.objects o on c.object_id = o.object_ID 
		and c.Name ='nawa_Warning' 
		and o.name = @strTableUploadName
	)
    BEGIN
		SET @sql = ' ALTER TABLE ' + @strTableUploadName 
			+ ' ADD nawa_Warning VARCHAR(MAX) ';
		EXEC (@sql); 
	END

	/*Cek Field Exclude Primary Key*/
	IF @IsPrimaryKey <> 1
	BEGIN
		/*Update warning set field to null if doesn't have access for insert*/
		IF @bAdd = 0
		BEGIN
			IF @Required = 1
			BEGIN
				SET @sql = ' UPDATE ' + @strTableUploadName 
							+' SET Nawa_Warning =  ISNULL(Nawa_Warning,'''') + ''Line ''+ convert(Varchar(20),nawa_recordnumber) +'' : ' 
							+ @userid + ' does not have access to insert '
							+ @FieldLabel + ''' + CHAR(13) + CHAR(10) '
							+ ' , KeteranganError = KeteranganError + ''Line ''+ convert(Varchar(20),nawa_recordnumber) + '' : '+@FieldLabel+' is required, but you dont have access to input '
							+ @FieldLabel + ''' + CHAR(13) + CHAR(10) '
							+' WHERE nawa_userid=''' + @userid +
							'''  and nawa_Action = ''Insert'' ';
				EXEC (@sql);
			END
			ELSE
			BEGIN
				SET @sql = ' UPDATE ' + @strTableUploadName 
							+' SET Nawa_Warning =  ISNULL(Nawa_Warning,'''') + ''Line ''+ convert(Varchar(20),nawa_recordnumber) +'' : ' 
							+ @userid + ' does not have access to insert '
							+ @FieldLabel + ''' + CHAR(13) + CHAR(10) '
							+' WHERE nawa_userid=''' + @userid +
							'''  and nawa_Action = ''Insert'' ';
				EXEC (@sql);
			END
		END
		
		/*Update warning and set field to null if doesn't have access for update*/
		IF @bEdit = 0
		BEGIN
			SET @sql = ' UPDATE ' + @strTableUploadName 
						+' SET Nawa_Warning =  ISNULL(Nawa_Warning,'''') + ''Line ''+ convert(Varchar(20),nawa_recordnumber) +'' : ' 
						+ @userid + ' does not have access to update '
						+ @FieldLabel + ''' + CHAR(13) + CHAR(10) '
						+' WHERE nawa_userid=''' + @userid +
						'''  and nawa_Action = ''Update'' ';
			EXEC (@sql);
		END
	END
END