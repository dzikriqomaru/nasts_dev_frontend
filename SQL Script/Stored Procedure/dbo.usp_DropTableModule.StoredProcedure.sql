/****** Object:  StoredProcedure [dbo].[usp_DropTableModule]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_DropTableModule]
/***********************************************************
* Procedure description:
* Date:   10/26/2015 
* Author: Hendra
*
* Changes
* Date		Modified By			Comments
************************************************************
*
************************************************************/
(
	@PkModuleID INT
)
AS
BEGIN
	DECLARE @PK_Module_ID int, @ModuleName varchar(250), @ModuleLabel varchar(250),
			@ModuleDescription varchar(500), @IsUseDesigner bit, @IsUseApproval bit,
			@IsSupportAdd bit, @IsSupportEdit bit, @IsSupportDelete bit,
			@IsSupportActivation bit, @IsSupportView bit, @UrlAdd varchar(550),
			@UrlEdit varchar(550), @UrlDelete varchar(550),
			@UrlActivation varchar(550), @UrlView varchar(550),
			@UrlApproval varchar(250), @UrlApprovalDetail varchar(250),
			@IsUseStoreProcedureValidation bit, @Active bit, @CreatedBy varchar(50),
			@LastUpdateBy varchar(50), @ApprovedBy varchar(50),
			@CreatedDate datetime, @LastUpdateDate datetime, @ApprovedDate datetime

	SELECT
		@PK_Module_ID = PK_Module_ID, @ModuleName = ModuleName,
		@ModuleLabel = ModuleLabel, @ModuleDescription = ModuleDescription,
		@IsUseDesigner = IsUseDesigner, @IsUseApproval = IsUseApproval,
		@IsSupportAdd = IsSupportAdd, @IsSupportEdit = IsSupportEdit,
		@IsSupportDelete = IsSupportDelete,
		@IsSupportActivation = IsSupportActivation, @IsSupportView = IsSupportView,
		@UrlAdd = UrlAdd, @UrlEdit = UrlEdit, @UrlDelete = UrlDelete,
		@UrlActivation = UrlActivation, @UrlView = UrlView,
		@UrlApproval = UrlApproval, @UrlApprovalDetail = UrlApprovalDetail,
		@IsUseStoreProcedureValidation = IsUseStoreProcedureValidation,
		@Active = [Active], @CreatedBy = CreatedBy, @LastUpdateBy = LastUpdateBy,
		@ApprovedBy = ApprovedBy, @CreatedDate = CreatedDate,
		@LastUpdateDate = LastUpdateDate, @ApprovedDate = ApprovedDate
	FROM dbo.module	
	WHERE PK_Module_ID=@PkModuleID

	IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES t WHERE t.TABLE_NAME = @ModuleName)
	EXEC ('DROP TABLE ' + @ModuleName)

	DECLARE @modulenameUpload VARCHAR(50)
	SET @modulenameUpload = @ModuleName + '_Upload'
	IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES t WHERE t.TABLE_NAME = @modulenameUpload)
	EXEC ('DROP TABLE ' + @modulenameUpload)

	DECLARE @ModuleNameUploadApproval VARCHAR(350)
	SET @ModuleNameUploadApproval = @ModuleName + '_Upload_Approval'
	IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES t WHERE t.TABLE_NAME = @ModuleNameUploadApproval)
	EXEC ('DROP TABLE ' + @ModuleNameUploadApproval)
END
GO
