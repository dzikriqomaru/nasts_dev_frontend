/****** Object:  StoredProcedure [dbo].[Usp_GenerateLog]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Usp_GenerateLog]
	@PK_EODTaskDetailLog_ID	bigint
AS
	INSERT INTO LogConsoleService
	VALUES(GETDATE(), 'Process ID ' + CAST(@PK_EODTaskDetailLog_ID AS VARCHAR) + ' Running', 'Info', 'Description')
GO
