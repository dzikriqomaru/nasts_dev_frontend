/****** Object:  StoredProcedure [dbo].[usp_ValidationAdditionSettingUser]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ValidationAdditionSettingUser]
	@UserID VARCHAR(50),  
	@GroupID INT,  
	@RoleID INT  
AS  
DECLARE @hasApproval INT

DECLARE @msg VARCHAR(MAX)

SELECT @hasApproval = COUNT(1) FROM (
	SELECT JSON_VALUE(cast(ModuleField AS nvarchar(max)), '$[0].UserID') AS UserID,
	JSON_VALUE(cast(ModuleField AS nvarchar(max)), '$[0].FK_MRole_ID') AS FK_MRole_ID,
	JSON_VALUE(cast(ModuleField AS nvarchar(max)), '$[0].FK_MGroupMenu_ID') AS FK_MGroupMenu_ID
	FROM ModuleApproval
	WHERE FK_Module_ID = 13378
) X 
WHERE X.UserID = @UserID AND X.FK_MGroupMenu_ID = @GroupID AND X.FK_MRole_ID = @RoleID
IF @hasApproval > 0
	BEGIN RAISERROR ('Role and Group Menu already exist in approval for %s', 18, -1, @UserID)
	RETURN
END


DECLARE @hasData INT

SELECT @hasData = COUNT(1) FROM (
	SELECT UserID, FK_MRole_ID, FK_MGroupMenu_ID
	FROM NDSAdditionalSettingUser
) X 
WHERE X.UserID = @UserID AND X.FK_MGroupMenu_ID = @GroupID AND X.FK_MRole_ID = @RoleID
IF @hasData > 0
	BEGIN RAISERROR ('Role and Group Menu already exist for %s', 18, -1, @UserID)
	RETURN
END
GO
