/****** Object:  StoredProcedure [dbo].[Nawa_usp_GetConnectionStringByPK]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE  PROCEDURE [dbo].[Nawa_usp_GetConnectionStringByPK]
/***********************************************************
* Procedure description:
* Date:   9/2/2017 
* Author: Hendra
*
* Changes
* Date		Modified By			Comments
************************************************************
*
************************************************************/
(
	@pkid INT 
)
AS
BEGIN
	
	
	
	DECLARE @PK_NawaDataConnectionString_ID int, @ConnecitonName varchar(8000),
        @ConnectionValue varchar(8000), @PasswordValue varchar(8000),
        @Active bit, @CreatedBy varchar(50), @LastUpdateBy varchar(50),
        @ApprovedBy varchar(50), @CreatedDate datetime,
        @LastUpdateDate datetime, @ApprovedDate datetime,
        @Alternateby varchar(50), @Passwordbinary varbinary(max)
	
	SELECT
		@PK_NawaDataConnectionString_ID = PK_NawaDataConnectionString_ID,
		@ConnecitonName = ConnecitonName, @ConnectionValue = ConnectionValue,
		@PasswordValue = PasswordValue, @Active = [Active],
		@CreatedBy = CreatedBy, @LastUpdateBy = LastUpdateBy,
		@ApprovedBy = ApprovedBy, @CreatedDate = CreatedDate,
		@LastUpdateDate = LastUpdateDate, @ApprovedDate = ApprovedDate,
		@Alternateby = Alternateby, @Passwordbinary = Passwordbinary
	FROM NawadataConnectionString
	WHERE PK_NawaDataConnectionString_ID=@pkid
	
	DECLARE @result VARCHAR(8000)
    OPEN SYMMETRIC KEY ConfigSymKey DECRYPTION BY CERTIFICATE ConfigCert 
	SET @result = decryptByKey(@Passwordbinary)
	
	
	
	
	SELECT REPLACE( @ConnectionValue,'@Password',@result) AS ConnectionString
	
	
	
	
	
	
	
END
GO
