/****** Object:  StoredProcedure [dbo].[usp_SaveUploadApprovalModuleDetail_Correction]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_SaveUploadApprovalModuleDetail_Correction]    
/***********************************************************    
* Procedure description:    
* Date:   31/01/2022
* Author: Fauzan
*    
* Changes    
* Date  Modified By   Comments    
************************************************************    
*    
************************************************************/    
(    
	@modeID INT, @userID VARCHAR(50), @moduleDetailID INT, @moduleApprovalID BIGINT, @PK_ModuleDetailApproval_ID INT
)    
AS    
BEGIN     
	DECLARE @sqlmajorversion INT  
    SELECT @sqlmajorversion= CONVERT(INT, SERVERPROPERTY('ProductMajorVersion'))

	--Getting ModuleDetail's Data
	DECLARE @PK_ModuleDetail_ID int, @FK_Module_ID int, @ModuleDetailName varchar(250),
		@ModuleDetailLabel varchar(250), @ModuleDetailDescription varchar(500), @Active bit,
		@CreatedBy varchar(50), @LastUpdateBy varchar(50), @ApprovedBy varchar(50), 
		@CreatedDate datetime, @LastUpdateDate datetime, @ApprovedDate datetime
	SELECT    
		@PK_ModuleDetail_ID = PK_ModuleDetail_ID,
		@FK_Module_ID = FK_Module_ID,
		@ModuleDetailName = ModuleDetailName,
		@ModuleDetailLabel = ModuleDetailLabel,
		@ModuleDetailDescription = ModuleDetailDescription,
		@Active = [Active],
		@CreatedBy = CreatedBy,
		@LastUpdateBy = LastUpdateBy,
		@ApprovedBy = ApprovedBy,
		@CreatedDate = CreatedDate,
		@LastUpdateDate = LastUpdateDate,
		@ApprovedDate = ApprovedDate
	FROM dbo.ModuleDetail
	WHERE PK_ModuleDetail_ID = @moduleDetailID

	--Check SysParam 53 for Upload Table's Name
	DECLARE @bIsCreateTableUploadForEachUser AS BIT = 0
	DECLARE @strTableUploadName AS VARCHAR(500) = ''
	SELECT @bIsCreateTableUploadForEachUser = sp.SettingValue
	FROM   SystemParameter AS sp
	WHERE  sp.PK_SystemParameter_ID = 53

	IF @bIsCreateTableUploadForEachUser = 1
	BEGIN
	    SET @strTableUploadName = @ModuleDetailName + '_Upload_data_' + dbo.StripTableName(@userID)
	END
	ELSE
	BEGIN
	    SET @strTableUploadName = @ModuleDetailName + '_Upload'
	END

	--Update ModuleDetailApproval
	UPDATE ModuleDetailApproval 
	SET ModuleDetailName = @ModuleDetailName,
		ModuleDetailKey = @modeID,
		ModuleDetailField = '', 
		ModuleDetailFieldBefore = '',
		FK_ModuleApproval_ID = @moduleApprovalID,
		PK_ModuleAction_ID = 7,
		[Active] = 1,
		CreatedBy = @userID,
		CreatedDate = GETDATE(),
		FK_ModuleDetail_ID = @PK_ModuleDetail_ID
	WHERE PK_ModuleDetailApproval_ID = @PK_ModuleDetailApproval_ID

	--Check The Existence Of Upload Approval Table
	IF OBJECT_ID(@strTableUploadName + '_Approval', 'U') IS NULL
	BEGIN
		DECLARE @sqlCreateTable VARCHAR(MAX)
		SET @sqlCreateTable = 'SELECT CAST(' + CAST(@PK_ModuleDetailApproval_ID AS varchar) + ' AS BIGINT) FK_ModuleDetailApproval_ID, * INTO ' + @strTableUploadName + '_Approval FROM ' + @strTableUploadName + ' WHERE 1 = 2;' + CHAR(10)
			+ '	ALTER TABLE ' + @strTableUploadName + '_Approval ADD CONSTRAINT PK_' + @strTableUploadName + '_Approval_ID PRIMARY KEY (PK_upload_ID);' + CHAR(10)
			+ '	CREATE INDEX Idx' + @strTableUploadName + '_Approval_id  ON ' + @strTableUploadName + '_Approval (FK_ModuleDetailApproval_ID);' + CHAR(10)
			+ '	CREATE INDEX Idx' + @strTableUploadName + '_Approval_userid  ON ' + @strTableUploadName + '_Approval (nawa_userid);' + CHAR(10)
			+ '	CREATE INDEX Idx' + @strTableUploadName + '_Approval_userid_action  ON ' + @strTableUploadName + '_Approval (nawa_userid, nawa_Action);' + CHAR(10)
		EXEC(@sqlCreateTable)
	END
	ELSE	
	BEGIN
		DECLARE @sqlAlterTable VARCHAR(MAX) = ''	
		DECLARE @ApprovalColumnName	nvarchar(128),	
				@ApprovalDataType	nvarchar(128),	
				@ApprovalCharacterMaximumLength	int,	
				@ApprovalNumericPrecision	tinyint,	
				@ApprovalNumericScale	int,	
				@UploadColumnName	nvarchar(128),	
				@UploadDataType	nvarchar(128),	
				@UploadCharacterMaximumLength	int,	
				@UploadNumericPrecision	tinyint,	
				@UploadNumericScale	int

		DECLARE c CURSOR FOR
			SELECT ApprovalTable.COLUMN_NAME ApprovalColumnName, ApprovalTable.DATA_TYPE ApprovalDataType,	
				ApprovalTable.CHARACTER_MAXIMUM_LENGTH ApprovalCharacterMaximumLength,	
				ApprovalTable.NUMERIC_PRECISION ApprovalNumericPrecision,	
				ApprovalTable.NUMERIC_SCALE ApprovalNumericScale,	
				UploadTable.COLUMN_NAME UploadColumnName, UploadTable.DATA_TYPE UploadDataType,	
				UploadTable.CHARACTER_MAXIMUM_LENGTH UploadCharacterMaximumLength,	
				UploadTable.NUMERIC_PRECISION UploadNumericPrecision,	
				UploadTable.NUMERIC_SCALE UploadNumericScale	
			FROM INFORMATION_SCHEMA.[COLUMNS] ApprovalTable	
			JOIN INFORMATION_SCHEMA.[COLUMNS] UploadTable	
				ON UploadTable.TABLE_NAME = @strTableUploadName	
				AND ApprovalTable.COLUMN_NAME = UploadTable.COLUMN_NAME	
			WHERE ApprovalTable.TABLE_NAME = @strTableUploadName + '_Approval'	
				AND ApprovalTable.COLUMN_NAME <> 'FK_ModuleDetailApproval_ID'	
				AND (	
					ApprovalTable.DATA_TYPE <> UploadTable.DATA_TYPE	
					OR ApprovalTable.CHARACTER_MAXIMUM_LENGTH <> UploadTable.CHARACTER_MAXIMUM_LENGTH	
				)	
			UNION	
			SELECT ApprovalTable.COLUMN_NAME ApprovalColumnName, ApprovalTable.DATA_TYPE ApprovalDataType,	
				   ApprovalTable.CHARACTER_MAXIMUM_LENGTH ApprovalCharacterMaximumLength,	
				   ApprovalTable.NUMERIC_PRECISION ApprovalNumericPrecision,	
				   ApprovalTable.NUMERIC_SCALE ApprovalNumericScale,	
				   UploadTable.COLUMN_NAME UploadColumnName, UploadTable.DATA_TYPE UploadDataType,	
				   UploadTable.CHARACTER_MAXIMUM_LENGTH UploadCharacterMaximumLength,	
				   UploadTable.NUMERIC_PRECISION UploadNumericPrecision,	
				   UploadTable.NUMERIC_SCALE UploadNumericScale	
			FROM INFORMATION_SCHEMA.[COLUMNS] ApprovalTable	
			LEFT JOIN INFORMATION_SCHEMA.[COLUMNS] UploadTable	
				ON UploadTable.TABLE_NAME = @strTableUploadName	
				AND ApprovalTable.COLUMN_NAME = UploadTable.COLUMN_NAME	
			WHERE ApprovalTable.TABLE_NAME = @strTableUploadName + '_Approval'	
				  AND ApprovalTable.COLUMN_NAME <> 'FK_ModuleDetailApproval_ID'	
				  AND UploadTable.COLUMN_NAME IS NULL	
			UNION
			SELECT ApprovalTable.COLUMN_NAME ApprovalColumnName, ApprovalTable.DATA_TYPE ApprovalDataType,	
				   ApprovalTable.CHARACTER_MAXIMUM_LENGTH ApprovalCharacterMaximumLength,	
				   ApprovalTable.NUMERIC_PRECISION ApprovalNumericPrecision,	
				   ApprovalTable.NUMERIC_SCALE ApprovalNumericScale,	
				   UploadTable.COLUMN_NAME UploadColumnName, UploadTable.DATA_TYPE UploadDataType,	
				   UploadTable.CHARACTER_MAXIMUM_LENGTH UploadCharacterMaximumLength,	
				   UploadTable.NUMERIC_PRECISION UploadNumericPrecision,	
				   UploadTable.NUMERIC_SCALE UploadNumericScale	
			FROM INFORMATION_SCHEMA.[COLUMNS] UploadTable	
			LEFT JOIN INFORMATION_SCHEMA.[COLUMNS] ApprovalTable	
				ON ApprovalTable.TABLE_NAME = @strTableUploadName + '_Approval'	
				AND ApprovalTable.COLUMN_NAME = UploadTable.COLUMN_NAME	
				AND ApprovalTable.COLUMN_NAME <> 'FK_ModuleDetailApproval_ID'	
			WHERE UploadTable.TABLE_NAME = @strTableUploadName	
				  AND ApprovalTable.COLUMN_NAME IS NULL	
		OPEN c	
		FETCH NEXT FROM c INTO @ApprovalColumnName, @ApprovalDataType, @ApprovalCharacterMaximumLength, @ApprovalNumericPrecision, @ApprovalNumericScale,	
						   @UploadColumnName, @UploadDataType, @UploadCharacterMaximumLength, @UploadNumericPrecision, @UploadNumericScale	
		WHILE @@FETCH_STATUS = 0	
		BEGIN
			IF @UploadColumnName IS NULL	
			BEGIN	
				SET @sqlAlterTable = 'ALTER TABLE ' + @strTableUploadName + '_Approval' + CHAR(10) +	
									 'DROP COLUMN ' + @ApprovalColumnName	
			END	
			ELSE IF @ApprovalColumnName IS NULL	
			BEGIN	
				SET @sqlAlterTable = 'ALTER TABLE ' + @strTableUploadName + '_Approval' + CHAR(10) +	
					'ADD ' + @UploadColumnName + ' ' + @UploadDataType +	
					CASE WHEN @UploadDataType IN ('varchar', 'varbinary') THEN '(' + CASE WHEN @UploadCharacterMaximumLength < 0 THEN 'MAX' ELSE CAST(@UploadCharacterMaximumLength AS VARCHAR) END + ')' ELSE '' END +	
					' NULL'	
			END	
			ELSE	
			BEGIN	
				SET @sqlAlterTable = 'ALTER TABLE ' + @strTableUploadName + '_Approval' + CHAR(10) +	
					'ALTER COLUMN ' + @UploadColumnName + ' ' + @UploadDataType +	
					CASE WHEN @UploadDataType IN ('varchar', 'varbinary') THEN '(' + CASE WHEN @UploadCharacterMaximumLength < 0 THEN 'MAX' ELSE CAST(@UploadCharacterMaximumLength AS VARCHAR) END + ')' ELSE '' END +	
					' NULL'	
			END	
			EXEC(@sqlAlterTable)	
			FETCH NEXT FROM c INTO @ApprovalColumnName, @ApprovalDataType, @ApprovalCharacterMaximumLength, @ApprovalNumericPrecision, @ApprovalNumericScale, @UploadColumnName, @UploadDataType, @UploadCharacterMaximumLength, @UploadNumericPrecision, @UploadNumericScale	
		END	
	CLOSE c	
	DEALLOCATE c	
END

	--Get ModuleDetailField & Field Query
	DECLARE @queryfield VARCHAR(MAX)='', @querycleanfield VARCHAR(MAX) = ''
	DECLARE @PK_ModuleDetailField_ID bigint, @FK_ModuleDetail_ID int, @FieldName varchar(250),
        @FieldLabel varchar(250), @Sequence int, @Required bit,      
        @IsPrimaryKey bit, @IsUnik bit, @IsShowInView bit, @IsShowInForm bit,      
        @DefaultValue varchar(max), @FK_FieldType_ID int, @SizeField int,      
        @FK_ExtType_ID int, @TabelReferenceName varchar(250),      
        @TabelReferenceNameAlias varchar(250),      
        @TableReferenceFieldKey varchar(250),      
        @TableReferenceFieldDisplayName varchar(250),      
        @TableReferenceFilter varchar(550), @IsUseRegexValidation bit,      
        @TableReferenceAdditonalJoin varchar(1000), @BCasCade bit,      
        @FieldNameParent varchar(250), @FilterCascade varchar(1000)

	DECLARE my_cursor CURSOR FAST_FORWARD READ_ONLY FOR
		SELECT PK_ModuleDetailField_ID, FK_ModuleDetail_ID, FieldName, FieldLabel,      
				Sequence, [Required], IsPrimaryKey, IsUnik, IsShowInView,      
				IsShowInForm, DefaultValue, FK_FieldType_ID, SizeField,      
				FK_ExtType_ID, TabelReferenceName, TabelReferenceNameAlias,      
				TableReferenceFieldKey, TableReferenceFieldDisplayName,      
				TableReferenceFilter, IsUseRegexValidation,      
				TableReferenceAdditonalJoin, BCasCade, FieldNameParent,      
				FilterCascade      
		FROM ModuleDetailField WHERE FK_ModuleDetail_ID = @moduleDetailID
	OPEN my_cursor
	FETCH FROM my_cursor INTO @PK_ModuleDetailField_ID, @FK_ModuleDetail_ID,
                             @FieldName, @FieldLabel, @Sequence,
                             @Required, @IsPrimaryKey, @IsUnik,
                             @IsShowInView, @IsShowInForm, @DefaultValue,
                             @FK_FieldType_ID, @SizeField, @FK_ExtType_ID,
                             @TabelReferenceName,
                             @TabelReferenceNameAlias,
                             @TableReferenceFieldKey,
                             @TableReferenceFieldDisplayName,
                             @TableReferenceFilter, @IsUseRegexValidation,
                             @TableReferenceAdditonalJoin, @BCasCade,
                             @FieldNameParent, @FilterCascade
	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @querycleanfield = @querycleanfield + @FieldName + ', '     
		
		IF @FK_FieldType_ID = 9
			SET @queryfield = @queryfield + ' ('''+@FieldLabel+''', '+@FieldName+'),'
		IF  (@FK_FieldType_ID  IN  ( 1,2,3,4,5,6,7,8,12,13))
			SET @queryfield = @queryfield + ' ('''+@FieldLabel+''', TRY_CONVERT(VARCHAR(8000), '+@FieldName+')),'
		IF (@FK_FieldType_ID = 10)
			SET @queryfield = @queryfield + ' ('''+@FieldLabel+''', TRY_CONVERT(VARCHAR(8000), '+@FieldName+')),'
		IF (@FK_FieldType_ID = 11)
			SET @queryfield = @queryfield +' ('''+@FieldLabel +''', try_convert(varchar(8000), SUBSTRING( convert(varchar(8000),'+ @FieldName +'), PATINDEX(''%(%'', convert(varchar(8000),'+@FieldName+'))+1, CASE WHEN PATINDEX(''%)%'',convert(varchar(8000),'+@FieldName+'))=0 THEN LEN('+@FieldName+') else PATINDEX(''%)%'',convert(varchar(8000),'+@FieldName+'))-2 END ))),'

		FETCH FROM my_cursor INTO @PK_ModuleDetailField_ID, @FK_ModuleDetail_ID,
								 @FieldName, @FieldLabel, @Sequence,
								 @Required, @IsPrimaryKey, @IsUnik,
								 @IsShowInView, @IsShowInForm, @DefaultValue,
								 @FK_FieldType_ID, @SizeField, @FK_ExtType_ID,
								 @TabelReferenceName,
								 @TabelReferenceNameAlias,
								 @TableReferenceFieldKey,
								 @TableReferenceFieldDisplayName,
								 @TableReferenceFilter, @IsUseRegexValidation,
								 @TableReferenceAdditonalJoin, @BCasCade,
								 @FieldNameParent, @FilterCascade      
	END
	CLOSE my_cursor
	DEALLOCATE my_cursor

	IF LEN(@queryfield) > 0 
		SET @queryfield = SUBSTRING(@queryfield,1,LEN(@queryfield)-1)       
	IF LEN(@querycleanfield) > 0 
		SET @querycleanfield = SUBSTRING(@querycleanfield,1,LEN(@querycleanfield)-1)

	--Get ModuleFieldDefault
	DECLARE @PK_ModuleField_ID_Default     BIGINT,
	    @FieldName_Default             VARCHAR(250),
	    @FieldLabel_Default            VARCHAR(250),
	    @Sequence_Default              INT,
	    @Required_Default              BIT,
	    @IsPrimaryKey_Default          BIT,
	    @IsUnik_Default                BIT,
	    @FK_FieldType_ID_Default       INT,
	    @SizeField_Default             INT,
	    @FK_ExtType_ID_Default         INT,
		@fieldDefault               VARCHAR(MAX) = '',
		@fieldactive                VARCHAR(100) = ''
	
	DECLARE my_cursor CURSOR FAST_FORWARD READ_ONLY FOR
		SELECT PK_ModuleField_ID,
			FieldName,
	        FieldLabel,
	        Sequence,
	        [Required],
	        IsPrimaryKey,
	        IsUnik,
	        FK_FieldType_ID,
	        SizeField,
	        FK_ExtType_ID
		FROM   dbo.ModuleFieldDefault
	OPEN my_cursor 
	FETCH FROM my_cursor INTO @PK_ModuleField_ID_Default, @FieldName_Default, @FieldLabel_Default, 
		@Sequence_Default, @Required_Default, @IsPrimaryKey_Default, @IsUnik_Default, 
		@FK_FieldType_ID_Default, @SizeField_Default, @FK_ExtType_ID_Default    
	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF @FK_FieldType_ID_Default = 13
		BEGIN
			SET @fieldDefault = @fieldDefault + @FieldName_Default + ',' + CHAR(10) + CHAR(13)    
			SET @fieldactive = 'isnull(' + @FieldName_Default + ',1),' + CHAR(10) + CHAR(13)
		END 	    
	    
		FETCH FROM my_cursor INTO @PK_ModuleField_ID_Default, @FieldName_Default, 
		@FieldLabel_Default, @Sequence_Default, @Required_Default, @IsPrimaryKey_Default, 
		@IsUnik_Default, @FK_FieldType_ID_Default, @SizeField_Default, @FK_ExtType_ID_Default
	END 
	CLOSE my_cursor 
	DEALLOCATE my_cursor

	IF LEN(@fieldDefault) > 0
		SET @fieldDefault = SUBSTRING(@fieldDefault, 1, LEN(@fieldDefault) -3)   
	IF LEN(@fieldactive) > 0
		SET @fieldactive = SUBSTRING(@fieldactive, 1, LEN(@fieldactive) -3)

	--Insert Into Upload Approval Table
	DECLARE @sqlUploadApproval varchar(max)
	SET @sqlUploadApproval = ' ' + char(10)
		 + ' DELETE FROM ' + @strTableUploadName + '_Approval WHERE FK_ModuleDetailApproval_ID = ' + CONVERT(VARCHAR(100),@PK_ModuleDetailApproval_ID) + char(10)

         + 'INSERT INTO ' + @strTableUploadName + '_Approval(FK_ModuleDetailApproval_ID, nawa_userid, nawa_recordnumber, KeteranganError, nawa_Action, ' + @querycleanfield + CASE WHEN @fieldDefault <> '' THEN ', ' + @fieldDefault ELSE '' END + ') ' + char(10)
         + ' SELECT ' + CAST(@PK_ModuleDetailApproval_ID AS varchar) + ', nawa_userid, nawa_recordnumber, KeteranganError, nawa_Action, ' + @querycleanfield + CASE WHEN @fieldactive <> '' THEN ', ' + @fieldactive ELSE '' END + ' ' + char(10)     
         + ' FROM ' + @strTableUploadName + ' A ' + char(10)      
         + ' WHERE A.nawa_userid = '''+@userid+''' ' + char(10)      
         + '	AND A.KeteranganError = '''' ' + char(10)      
         + ' ORDER BY A.PK_upload_ID ' + char(10)
	PRINT(@sqlUploadApproval)
	EXEC(@sqlUploadApproval)

	--Final Query
	DECLARE @sqlFinal varchar(max)
	SET @sqlFinal = ' DELETE FROM ' + @strTableUploadName +' where nawa_userid=''' + @userID + '''' + char(10)
	EXEC(@sqlFinal)
END


GO
