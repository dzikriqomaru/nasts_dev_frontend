/****** Object:  StoredProcedure [dbo].[usp_Pagingdata2012]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_Pagingdata2012] (
    @Tables          VARCHAR(MAX),
    @fields          VARCHAR(MAX),
    @WhereClause     VARCHAR(8000),
    @OrderBy         VARCHAR(2000),
    @PageIndex       INT,
    @PageSize        INT
)
AS
BEGIN
		
	DECLARE @PageLowerBound INT  	
	DECLARE @PageUpperBound INT  	
	
	-- Set the page bounds  
	
	SET @PageLowerBound = @PageSize * @PageIndex  
	SET @PageUpperBound = @PageLowerBound + @PageSize  
	
	IF (@OrderBy IS NULL OR LEN(@OrderBy) < 1)
	BEGIN
	    -- default order by to first column  
	    
	    SET @OrderBy = ' 1 '
	END 
	
	IF (CHARINDEX('*', @OrderBy) > 0)
	BEGIN
	    --SELECT TOP 1 @OrderBy = c.COLUMN_NAME
	    --FROM   INFORMATION_SCHEMA.[COLUMNS] c
	    --WHERE  c.TABLE_NAME = @Tables
	    SET @OrderBy=' 1 '
	END
	
	
	-- SQL Server 2005 Paging  
	
	DECLARE @SQL AS VARCHAR(MAX) 
	
	
	
	SET @SQL = ' SELECT  ' + @fields 
	
	SET @SQL = @SQL + ' FROM ' + @Tables
	
	IF LEN(@WhereClause) > 0
	BEGIN
	    SET @SQL = @SQL + ' WHERE ' + @WhereClause
	END  
	
	
	SET @SQL = @SQL + ' ORDER BY ' + @OrderBy
	
	--SET @SQL = @SQL + ' OFFSET '+ CONVERT(VARCHAR(50), (@PageIndex ) *@PageSize )+ ' ROWS '
	
	SET @SQL = @SQL + ' OFFSET ' + CONVERT(VARCHAR(50), (@PageIndex)) + ' ROWS '
	
	SET @SQL = @SQL + ' FETCH NEXT ' + CONVERT(VARCHAR(50), @PageSize) +
	    ' ROWS ONLY'
	
	--PRINT @SQL
	EXEC (@SQL) 
	
	
	
	
	
	-- get row count  
	
	SET @SQL = 'SELECT COUNT(*) as TotalRowCount'  
	
	SET @SQL = @SQL + ' from ' + @Tables  
	
	IF LEN(@WhereClause) > 0
	BEGIN
	    SET @SQL = @SQL + ' WHERE ' + @WhereClause
	END  
	
	
	EXEC (@SQL)
END
GO
