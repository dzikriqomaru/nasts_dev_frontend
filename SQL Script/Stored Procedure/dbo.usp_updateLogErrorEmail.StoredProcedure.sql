/****** Object:  StoredProcedure [dbo].[usp_updateLogErrorEmail]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[usp_updateLogErrorEmail]
/***********************************************************
* Procedure description:
* Date:   24/02/2016 
* Author: Hendra
*
* Changes
* Date		Modified By			Comments
************************************************************
*
************************************************************/
(
	@PK_EmailTemplateSchedulerDetail_ID BIGINT,@errorMessage VARCHAR(MAX)
)
AS
BEGIN
	
	UPDATE EmailTemplateSchedulerDetail SET ErrorMessage = @errorMessage ,FK_EmailStatus_ID = 5
	WHERE PK_EmailTemplateSchedulerDetail_ID=@PK_EmailTemplateSchedulerDetail_ID
	
	
	
	UPDATE emailTemplateScheduler
	SET
	    FK_emailStatus_ID = 5 WHERE PK_emailTemplateScheduler_ID IN (
	    SELECT ssd.FK_emailTEmplateScheduler_ID
	      FROM emailTemplateSchedulerDetail ssd WHERE ssd.PK_emailTemplateSchedulerDetail_ID=@PK_EmailTemplateSchedulerDetail_ID	
	    )

	
END
GO
