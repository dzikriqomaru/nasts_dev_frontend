/****** Object:  StoredProcedure [dbo].[usp_SaveSettingPersonal]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_SaveSettingPersonal]
/***********************************************************
* Procedure description:
* Date:   24/07/2016 
* Author: Hendra
*
* Changes
* Date		Modified By			Comments
************************************************************
*
************************************************************/
(
	@userid VARCHAR(100),
	@ReportDate DATETIME,
	@kodecabang VARCHAR(100)
)
AS
BEGIN
	
	DECLARE @jml INT
	SELECT @jml=COUNT(1) FROM SettingPersonal ss WHERE ss.UserID=@userid
	PRINT @jml
	IF @jml>0 
	BEGIN
		--update
		UPDATE SettingPersonal
		SET ReportDate = @ReportDate,
		 KodeCabang = @kodecabang,
		 [Active] = 1,
		 LastUpdateBy = @userid,
		 LastUpdateDate = GETDATE()
		FROM SettingPersonal ss
		WHERE ss.UserID=@userid
		
	END 
	ELSE
		BEGIN
			INSERT INTO SettingPersonal
			(
				-- PK_Setting_ID -- this column value is auto-generated
				UserID,
				ReportDate,
				[Active],
				CreatedBy,
				LastUpdateBy,
				ApprovedBy,
				CreatedDate,
				LastUpdateDate,
				ApprovedDate,
				KodeCabang
			)
			VALUES
			(
				@userid,
				@ReportDate,
				1,
				@userid,
				@userid,
				@userid,
				GETDATE(),
				GETDATE(),
				GETDATE(),
				@kodecabang
			)
		END
	
END


GO
