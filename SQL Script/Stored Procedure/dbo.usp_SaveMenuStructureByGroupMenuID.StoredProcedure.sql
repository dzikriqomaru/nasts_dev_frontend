/****** Object:  StoredProcedure [dbo].[usp_SaveMenuStructureByGroupMenuID]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_SaveMenuStructureByGroupMenuID]
/***********************************************************
* Procedure description:
* Date:   11/9/2019 
* Author: nawadata
*
* Changes
* Date		Modified By			Comments
************************************************************
*
************************************************************/
(
	@groupmenuid INT,@strukturmenu AS VARCHAR(MAX)
	
)
AS
BEGIN
	UPDATE MGroupMenu
	SET
	    DataMenu = @strukturmenu WHERE PK_MGroupMenu_ID=@groupmenuid
	
END
GO
