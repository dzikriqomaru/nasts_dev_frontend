/****** Object:  StoredProcedure [dbo].[usp_DropTableApprovalDesigner]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_DropTableApprovalDesigner]
/***********************************************************
* Procedure description:
* Date:   23/08/2021 
* Author: Fauzan
*
* Changes
* Date		Modified By			Comments

************************************************************
*
************************************************************/
(
	@PkApprovalDesignerID INT
)
AS
BEGIN
DECLARE @ApprovalDesignerName varchar(250)

SELECT	@ApprovalDesignerName = ApprovalDesignerName
FROM	dbo.ApprovalDesigner
WHERE	PK_ApprovalDesigner_ID = @PkApprovalDesignerID

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES t WHERE t.TABLE_NAME = @ApprovalDesignerName)
EXEC ('DROP TABLE ' + @ApprovalDesignerName)

DECLARE @ApprovalDesignernameUpload VARCHAR(50)
SET @ApprovalDesignernameUpload = @ApprovalDesignerName + '_Upload' 

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES t WHERE t.TABLE_NAME = @ApprovalDesignernameUpload)
EXEC ('DROP TABLE ' + @ApprovalDesignernameUpload)

END
GO
