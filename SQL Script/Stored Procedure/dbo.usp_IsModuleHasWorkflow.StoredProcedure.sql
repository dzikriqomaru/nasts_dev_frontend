/****** Object:  StoredProcedure [dbo].[usp_IsModuleHasWorkflow]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_IsModuleHasWorkflow]
/***********************************************************
* Procedure description:
* Date:   5/9/2019 
* Author: nawadata
*
* Changes
* Date		Modified By			Comments
************************************************************
*
************************************************************/
(
	@pkmodule AS int
)
AS
BEGIN
	
	DECLARE @jml int
SELECT @jml=COUNT(1) FROM Module_TR_WorkFlow AS mtwf WHERE mtwf.FK_ModuleID=@pkmodule AND mtwf.[Active]=1

IF @jml>=1 
BEGIN
	SELECT 1 AS result
	
END
ELSE
	BEGIN
		SELECT 0 AS result
	END

	
END
GO
