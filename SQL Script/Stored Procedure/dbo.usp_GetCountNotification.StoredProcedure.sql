/****** Object:  StoredProcedure [dbo].[usp_GetCountNotification]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_GetCountNotification]
/***********************************************************
* Procedure description:
* Date:   7/26/2017 
* Author: Hendra
*
* Changes
* Date		Modified By			Comments
************************************************************
*
************************************************************/
(
	@userID VARCHAR(100)
)
AS
BEGIN
	
	SELECT COUNT(1) FROM [Notification] n
WHERE n.ReceiverNotification=@userID AND n.IsRead=0

END
GO
