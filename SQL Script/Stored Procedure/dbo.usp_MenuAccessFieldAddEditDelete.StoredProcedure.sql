/****** Object:  StoredProcedure [dbo].[usp_MenuAccessFieldAddEditDelete]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_MenuAccessFieldAddEditDelete]    
(
	@GroupMenuID				INT, 
	@tblMGroupMenuAccessField	AS tblMGroupMenuAccessField READONLY,
    @CreatedBy                  VARCHAR(50),
    @ApprovedBy                 VARCHAR(50),
    @FK_AuditTrailStatus_ID     INT,
    @FK_ModuleAction_ID         INT,
    @ModuleLabel                VARCHAR(50),
    @PK_ModuleApproval_ID       BIGINT
)
AS    
BEGIN    
	MERGE MGroupMenuAccessField t 
		USING @tblMGroupMenuAccessField s
	ON (s.PK_MGroupMenuAccessField_ID = t.PK_MGroupMenuAccessField_ID)
	WHEN MATCHED
		THEN UPDATE SET 
			t.bAdd = s.bAdd,
			t.bEdit = s.bEdit,
			t.bDelete = s.bDelete,
			t.bActivation = s.bActivation,
			t.bView = s.bView,
			t.bApproval = s.bApproval,
			t.bUpload = s.bUpload,
			t.bDetail = s.bDetail
	WHEN NOT MATCHED BY TARGET 
		THEN INSERT (FK_MGroupMenu_ID, FK_Module_ID, ModuleField, bAdd, bEdit, bDelete, bActivation, bView, bApproval, bUpload, bDetail)
			 VALUES (s.FK_MGroupMenu_ID, s.FK_Module_ID, s.ModuleField, s.bAdd, s.bEdit, s.bDelete, s.bActivation, s.bView, s.bApproval, s.bUpload, s.bDetail)
	WHEN NOT MATCHED BY SOURCE AND t.FK_MGroupMenu_ID = @GroupMenuID
		THEN DELETE;

	--Audit Trail
	DECLARE @PKAuditTrailHeader BIGINT
	INSERT INTO AuditTrailHeader
	  (
	    CreatedDate,
	    CreatedBy,
	    ApproveBy,
	    ModuleLabel,
	    FK_ModuleAction_ID,
	    FK_AuditTrailStatus_ID,
	    PK_ModuleApproval_ID
	  )
	VALUES
	  (
	    GETDATE(),
	    @CreatedBy,
	    @ApprovedBy,
	    @ModuleLabel,
	    @FK_ModuleAction_ID,
	    @FK_AuditTrailStatus_ID,
	    @PK_ModuleApproval_ID
	  )

	SET @PKAuditTrailHeader = SCOPE_IDENTITY()

	Declare @ModuleActionName Varchar(50) = ''
	SELECT TOP 1 @ModuleActionName = ModuleActionName From ModuleAction Where PK_ModuleAction_ID = @FK_ModuleAction_ID

	INSERT INTO AuditTrailLog    
	   (    
		CreatedDate,    
		CreatedBy,  
		LastUpdateDate,
		LastUpdateBy,
		ApprovedDate,
		ApprovedBy,
		LogName,    
		LogTime,    
		LogDescription,    
		LogCode  ,
		LogSeverityCode
	   )    
     VALUES    
	   (    
	   GETDATE(),    
		@CreatedBy,  
	   GETDATE(),    
		@CreatedBy,  
	   GETDATE(),    
		@ApprovedBy,    
		'Menu Access - ' + @ModuleActionName,    
		GETDATE(),    
		'Affected to Database - CreatedBy: '+@CreatedBy+ '- ApprovalBy: ' + @ApprovedBy,      
		NEWID()   ,
		0
	   )    

	INSERT INTO AuditTrailDetail
	(
		FK_AuditTrailHeader_ID,
		FieldName,
		OldValue,
		NewValue
	)
	SELECT 
		@pkAudittrailheader AS FK_AuditTrailHeader_ID,
	    ca.fieldname,
	    '' AS OldValue,
	    ca.NewValue
	FROM
	(
		SELECT *
		FROM   MGroupMenuAccessField AS mma
		WHERE  mma.FK_MGroupMenu_ID = @GroupMenuID
	) xx
	CROSS APPLY 
	(
		VALUES 
	    ('PK_MGroupMenuAccessField_ID', TRY_CONVERT(VARCHAR(8000), PK_MGroupMenuAccessField_ID)),
	    ('FK_MGroupMenu_ID', TRY_CONVERT(VARCHAR(8000), FK_MGroupMenu_ID)),
	    ('FK_Module_ID', TRY_CONVERT(VARCHAR(8000), FK_Module_ID)),
		('ModuleField', TRY_CONVERT(VARCHAR(8000), ModuleField)),
	    ('bAdd', TRY_CONVERT(VARCHAR(8000), bAdd)),
	    ('bEdit', TRY_CONVERT(VARCHAR(8000), bEdit)),
	    ('bDelete', TRY_CONVERT(VARCHAR(8000), bDelete)),
	    ('bActivation', TRY_CONVERT(VARCHAR(8000), bActivation)),
	    ('bView', TRY_CONVERT(VARCHAR(8000), bView)),
	    ('bApproval', TRY_CONVERT(VARCHAR(8000), bApproval)),
	    ('bUpload', TRY_CONVERT(VARCHAR(8000), bUpload)),
	    ('bDetail', TRY_CONVERT(VARCHAR(8000), bDetail))
	) ca (Fieldname, NewValue)
END
GO
