/****** Object:  StoredProcedure [dbo].[usp_createuserAndGroup]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_createuserAndGroup]
/***********************************************************
* Procedure description:
* Date:   2/5/2018 
* Author: Hendra
*
* Changes
* Date		Modified By			Comments
************************************************************
*
************************************************************/
(
	@userid VARCHAR(50),@groupmenuname VARCHAR(250)
)
AS
BEGIN
	
	DECLARE @pkuserid INT,@pkgroupmenuid INT
	DECLARE @jmluser int

	set @groupmenuname=LTRIM(RTRIM(@groupmenuname))

SELECT @jmluser =COUNT(1) 	
	  FROM muser WHERE UserID=@userid
	  
	  IF @jmluser=0 
	  BEGIN
	  	--belum ada ,buat user baru
	  	DECLARE @jmlgroupmenu INT
	  	DECLARE @pkgruopmenuid int
	  	SELECT @jmlgroupmenu =COUNT(1) FROM MGroupMenu mm WHERE mm.GroupMenuName=@groupmenuname
	  	IF @jmlgroupmenu=0
	  	BEGIN
	  		 INSERT INTO MGroupMenu
	  		 (
	  		 	-- PK_MGroupMenu_ID -- this column value is auto-generated
	  		 	GroupMenuName,
	  		 	GroupMenuDesciption,
	  		 	[Active],
	  		 	CreatedBy,
	  		 	LastUpdateBy,
	  		 	ApprovedBy,
	  		 	CreatedDate,
	  		 	LastUpdateDate,
	  		 	ApprovedDate,
	  		 	Alternateby
	  		 )
	  		 VALUES
	  		 (
	  		 	@groupmenuname,
	  		 	@groupmenuname,
	  		 	1,
	  		 	'sysadmin',
	  		 	'sysadmin',
	  		 	'sysadmin',
	  		    GETDATE(),
	  		 	GETDATE(),
	  		 	GETDATE(),
	  		 	''
	  		 )
	  		 SET @pkgroupmenuid=SCOPE_IDENTITY()
	  	END 
	  	ELSE
	  		BEGIN
	  				SELECT @pkgroupmenuid =mm.PK_MGroupMenu_ID
	  				  FROM MGroupMenu mm WHERE mm.GroupMenuName=@groupmenuname
	  		END
	  	INSERT INTO MUser
	  	(
	  		-- PK_MUser_ID -- this column value is auto-generated
	  		UserID,
	  		UserName,
	  		FK_MRole_ID,
	  		FK_MGroupMenu_ID,
	  		UserPasword,
	  		UserPasswordSalt,
	  		UserEmailAddress,
	  		CreatedBy,
	  		LastUpdateBy,
	  		ApprovedBy,
	  		CreatedDate,
	  		LastUpdateDate,
	  		ApprovedDate,
	  		IPAddress,
	  		[Active],
	  		InUsed,
	  		IsDisabled,
	  		LastLogin,
	  		LastChangePassword,
	  		Alternateby
	  	)
	  	VALUES
	  	(
	  		@userid,
	  		@userid,
	            49,
	  		@pkgroupmenuid,
	  		'',
	  		'',
	  		'',
	  		'sysadmin',
	  		'sysadmin',
	  		'sysadmin',
	  		GETDATE(),
	  		GETDATE(),
	  		GETDATE(),
	  		'',
	  		1,
	  		0,
	  		0,
	  		GETDATE(),
	  		GETDATE(),
	  		''
	  	)
	  	
	  END
	  ELSE
	  	BEGIN
	  		--user ada
	  		--update group menunya
	  		
	  			DECLARE @jmlgroupmenu1 INT
	     	
	   	SELECT @jmlgroupmenu1 =COUNT(1) FROM MGroupMenu mm WHERE mm.GroupMenuName=@groupmenuname
	  	IF @jmlgroupmenu1=0
	  	BEGIN
	  		 INSERT INTO MGroupMenu
	  		 (
	  		 	-- PK_MGroupMenu_ID -- this column value is auto-generated
	  		 	GroupMenuName,
	  		 	GroupMenuDesciption,
	  		 	[Active],
	  		 	CreatedBy,
	  		 	LastUpdateBy,
	  		 	ApprovedBy,
	  		 	CreatedDate,
	  		 	LastUpdateDate,
	  		 	ApprovedDate,
	  		 	Alternateby
	  		 )
	  		 VALUES
	  		 (
	  		 	@groupmenuname,
	  		 	@groupmenuname,
	  		 	1,
	  		 	'sysadmin',
	  		 	'sysadmin',
	  		 	'sysadmin',
	  		    GETDATE(),
	  		 	GETDATE(),
	  		 	GETDATE(),
	  		 	''
	  		 )
	  		 SET @pkgroupmenuid=SCOPE_IDENTITY()
	  		 
	  		 UPDATE MUSER SET FK_MGroupMenu_ID = @pkgroupmenuid WHERE UserID=@userid
	  	END 
	  	ELSE
	  		BEGIN
	  				SELECT @pkgroupmenuid =mm.PK_MGroupMenu_ID
	  				  FROM MGroupMenu mm WHERE mm.GroupMenuName=@groupmenuname
	  				  
	  				  
	  				   UPDATE MUSER SET FK_MGroupMenu_ID = @pkgroupmenuid WHERE UserID=@userid
	  		END
	  		
	  	END
	  

	  
	
	
END
GO
