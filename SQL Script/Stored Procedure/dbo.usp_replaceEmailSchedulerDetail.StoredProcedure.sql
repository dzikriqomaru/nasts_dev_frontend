/****** Object:  StoredProcedure [dbo].[usp_replaceEmailSchedulerDetail]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_replaceEmailSchedulerDetail]
/***********************************************************
* Procedure description:
* Date:   24/02/2016 
* Author: Hendra
*
* Changes
* Date		Modified By			Comments
************************************************************
*
************************************************************/
(
	@PK_EmailTemplateSchedulerDetail_ID BIGINT,@Replacer VARCHAR(550),@FieldReplacer VARCHAR(550),@pk_emailitemplate_id int
)
AS
BEGIN
	IF @Replacer='$EmailID$' 
	BEGIN
		DECLARE @sql3 varchar(max)
	
		SET @sql3 = ' ' + char(10)
				 + '	UPDATE EmailTemplateSchedulerDetail ' + char(10)
				 + '	SET EmailTemplateSchedulerDetail.EmailTo = REPLACE(EmailTemplateSchedulerDetail.EmailTo,'''+ @Replacer+''' , EmailTemplateSchedulerDetail.EmailID) ' + char(10)
				 + '	WHERE EmailTemplateSchedulerDetail.PK_EmailTemplateSchedulerDetail_ID= '+ convert(varchar(20),@PK_EmailTemplateSchedulerDetail_ID) +' ' + char(10)
				 + '	'
		EXEC(@sql3)

		SET @sql3 = ' ' + char(10)
				 + '	UPDATE EmailTemplateSchedulerDetail ' + char(10)
				 + '	SET EmailTemplateSchedulerDetail.EmailCC = REPLACE(EmailTemplateSchedulerDetail.EmailCC,'''+ @Replacer+''' , EmailTemplateSchedulerDetail.EmailID) ' + char(10)
				 + '	WHERE EmailTemplateSchedulerDetail.PK_EmailTemplateSchedulerDetail_ID= '+ convert(varchar(20),@PK_EmailTemplateSchedulerDetail_ID) +' ' + char(10)
				 + '	'
		EXEC(@sql3)

		SET @sql3 = ' ' + char(10)
				 + '	UPDATE EmailTemplateSchedulerDetail ' + char(10)
				 + '	SET EmailTemplateSchedulerDetail.EmailBCC = REPLACE(EmailTemplateSchedulerDetail.EmailBCC,'''+ @Replacer+''' , EmailTemplateSchedulerDetail.EmailID) ' + char(10)
				 + '	WHERE EmailTemplateSchedulerDetail.PK_EmailTemplateSchedulerDetail_ID= '+ convert(varchar(20),@PK_EmailTemplateSchedulerDetail_ID) +' ' + char(10)
				 + '	'
		EXEC(@sql3)

		SET @sql3 = ' ' + char(10)
				 + '	UPDATE EmailTemplateSchedulerDetail ' + char(10)
				 + '	SET EmailTemplateSchedulerDetail.EmailSubject = REPLACE(EmailTemplateSchedulerDetail.EmailSubject,'''+ @Replacer+''' , EmailTemplateSchedulerDetail.EmailID) ' + char(10)
				 + '	WHERE EmailTemplateSchedulerDetail.PK_EmailTemplateSchedulerDetail_ID= '+ convert(varchar(20),@PK_EmailTemplateSchedulerDetail_ID) +' ' + char(10)
				 + '	'
		EXEC(@sql3)

		SET @sql3 = ' ' + char(10)
				 + '	UPDATE EmailTemplateSchedulerDetail ' + char(10)
				 + '	SET EmailTemplateSchedulerDetail.Emailbody = REPLACE(EmailTemplateSchedulerDetail.Emailbody,'''+ @Replacer+''' , EmailTemplateSchedulerDetail.EmailID) ' + char(10)
				 + '	WHERE EmailTemplateSchedulerDetail.PK_EmailTemplateSchedulerDetail_ID= '+ convert(varchar(20),@PK_EmailTemplateSchedulerDetail_ID) +' ' + char(10)
				 + '	'
		EXEC(@sql3)
		
		SET @sql3 = ' ' + char(10)
				 + '	UPDATE EmailTemplateSchedulerDetailAttachment ' + char(10)
				 + '	SET EmailTemplateSchedulerDetailAttachment.NamaReport = REPLACE(EmailTemplateSchedulerDetailAttachment.NamaReport,'''+ @Replacer +''', EmailTemplateSchedulerDetail.EmailID) ' + char(10)
				 + '	FROM EmailTemplateSchedulerDetailAttachment ' + char(10)
				 + '	INNER JOIN EmailTemplateSchedulerDetail ON EmailTemplateSchedulerDetailAttachment.FK_EmailTEmplateSchedulerDetail_ID=EmailTemplateSchedulerDetail.PK_EmailTEmplateSchedulerDetail_ID ' + char(10)
				 + '	WHERE EmailTemplateSchedulerDetail.PK_EmailTemplateSchedulerDetail_ID= '+ CONVERT(VARCHAR(20), @PK_EmailTemplateSchedulerDetail_ID) +' '+ char(10)
		EXEC(@sql3)

		SET @sql3 = ' ' + char(10)
				 + '	UPDATE EmailTemplateSchedulerDetailAttachment ' + char(10)
				 + '	SET EmailTemplateSchedulerDetailAttachment.ParameterReport = REPLACE(EmailTemplateSchedulerDetailAttachment.ParameterReport,'''+ @Replacer +''', EmailTemplateSchedulerDetail.EmailID) ' + char(10)
				 + '	FROM EmailTemplateSchedulerDetailAttachment ' + char(10)
				 + '	INNER JOIN EmailTemplateSchedulerDetail ON EmailTemplateSchedulerDetailAttachment.FK_EmailTEmplateSchedulerDetail_ID=EmailTemplateSchedulerDetail.PK_EmailTEmplateSchedulerDetail_ID ' + char(10)
				 + '	WHERE EmailTemplateSchedulerDetail.PK_EmailTemplateSchedulerDetail_ID= '+ CONVERT(VARCHAR(20), @PK_EmailTemplateSchedulerDetail_ID) +' '+ char(10)
		EXEC(@sql3)

		SET @sql3 = ' ' + char(10)
				 + '	UPDATE EmailTemplateSchedulerDetailAttachment ' + char(10)
				 + '	SET EmailTemplateSchedulerDetailAttachment.FileName = REPLACE(EmailTemplateSchedulerDetailAttachment.FileName,'''+ @Replacer +''', EmailTemplateSchedulerDetail.EmailID) ' + char(10)
				 + '	FROM EmailTemplateSchedulerDetailAttachment ' + char(10)
				 + '	INNER JOIN EmailTemplateSchedulerDetail ON EmailTemplateSchedulerDetailAttachment.FK_EmailTEmplateSchedulerDetail_ID=EmailTemplateSchedulerDetail.PK_EmailTEmplateSchedulerDetail_ID ' + char(10)
				 + '	WHERE EmailTemplateSchedulerDetail.PK_EmailTemplateSchedulerDetail_ID= '+ CONVERT(VARCHAR(20), @PK_EmailTemplateSchedulerDetail_ID) +' '+ char(10)
		EXEC(@sql3)

	END
	ELSE
	BEGIN
		DECLARE  @NamaTableprimary varchar(250),        
			@FieldUnikPrimaryTable varchar(250)
	
		SELECT
			@NamaTableprimary = '[__'+NamaTable+']',		
			@FieldUnikPrimaryTable = '['+FieldUnikPrimaryTable+']'
		FROM dbo.EmailTemplateAdditional eta
		WHERE eta.FK_EmailTemplate_ID=@pk_emailitemplate_id
		AND eta.FK_EmailTableType_ID=1 
	
	
	
		DECLARE @tablename VARCHAR(500)
	
	
		SELECT @tablename=SUBSTRING(@FieldReplacer,0 , CHARINDEX(  '].[',@FieldReplacer,1)+1)
	
	
		IF @tablename=@NamaTableprimary
		BEGIN
			--UPDATE EmailTemplateSchedulerDetail
			--SET EmailTo = REPLACE(emailto,@Replacer,@FieldReplacer)
			--FROM EmailTemplateSchedulerDetail
			--INNER JOIN @tablename ON EmailTemplateSchedulerDetail.UnikFieldTablePrimary=@tablename.@FieldUnikPrimaryTable  
			--WHERE EmailTemplateSchedulerDetail.PK_EmailTemplateSchedulerDetail_ID=@PK_EmailTemplateSchedulerDetail_ID
	
			DECLARE @sql varchar(max)
			SET @sql = '	UPDATE EmailTemplateSchedulerDetail ' + char(10)
					 + '	SET EmailTemplateSchedulerDetail.EmailTo = REPLACE(EmailTemplateSchedulerDetail.emailto,'+ @Replacer +',isnull('+ @FieldReplacer+','''') ) ' + char(10)
					 + '	FROM EmailTemplateSchedulerDetail ' + char(10)
					 + '	INNER JOIN '+ @tablename +' ON EmailTemplateSchedulerDetail.UnikFieldTablePrimary= '+ @tablename +'.'+ @FieldUnikPrimaryTable +'   ' + char(10)
					 + '	WHERE EmailTemplateSchedulerDetail.PK_EmailTemplateSchedulerDetail_ID= '+ CONVERT(VARCHAR(20), @PK_EmailTemplateSchedulerDetail_ID) +' '+ char(10)
                  
			EXEC(@sql)



			SET @sql = '	UPDATE EmailTemplateSchedulerDetail ' + char(10)
					 + '	SET EmailTemplateSchedulerDetail.EmailCC = REPLACE(EmailTemplateSchedulerDetail.EmailCC,'+ @Replacer +',isnull('+ @FieldReplacer+','''') ) ' + char(10)
					 + '	FROM EmailTemplateSchedulerDetail ' + char(10)
					 + '	INNER JOIN '+ @tablename +' ON EmailTemplateSchedulerDetail.UnikFieldTablePrimary= '+ @tablename +'.'+ @FieldUnikPrimaryTable +'   ' + char(10)
					 + '	WHERE EmailTemplateSchedulerDetail.PK_EmailTemplateSchedulerDetail_ID= '+ CONVERT(VARCHAR(20), @PK_EmailTemplateSchedulerDetail_ID) +' '+ char(10)
                  
			EXEC(@sql)



			SET @sql = '	UPDATE EmailTemplateSchedulerDetail ' + char(10)
					 + '	SET EmailTemplateSchedulerDetail.EmailBCC = REPLACE(EmailTemplateSchedulerDetail.EmailBCC,'+ @Replacer +',isnull('+ @FieldReplacer+','''') ) ' + char(10)
					 + '	FROM EmailTemplateSchedulerDetail ' + char(10)
					 + '	INNER JOIN '+ @tablename +' ON EmailTemplateSchedulerDetail.UnikFieldTablePrimary= '+ @tablename +'.'+ @FieldUnikPrimaryTable +'   ' + char(10)
					 + '	WHERE EmailTemplateSchedulerDetail.PK_EmailTemplateSchedulerDetail_ID= '+ CONVERT(VARCHAR(20), @PK_EmailTemplateSchedulerDetail_ID) +' '+ char(10)
                  
			EXEC(@sql)
	


			SET @sql = '	UPDATE EmailTemplateSchedulerDetail ' + char(10)
					 + '	SET EmailTemplateSchedulerDetail.EmailSubject = REPLACE(EmailTemplateSchedulerDetail.EmailSubject,'+ @Replacer +',isnull('+ @FieldReplacer+','''') ) ' + char(10)
					 + '	FROM EmailTemplateSchedulerDetail ' + char(10)
					 + '	INNER JOIN '+ @tablename +' ON EmailTemplateSchedulerDetail.UnikFieldTablePrimary= '+ @tablename +'.'+ @FieldUnikPrimaryTable +'   ' + char(10)
					 + '	WHERE EmailTemplateSchedulerDetail.PK_EmailTemplateSchedulerDetail_ID= '+ CONVERT(VARCHAR(20), @PK_EmailTemplateSchedulerDetail_ID) +' '+ char(10)
                  
			EXEC(@sql)
	


	
			SET @sql = '	UPDATE EmailTemplateSchedulerDetail ' + char(10)
					 + '	SET EmailTemplateSchedulerDetail.EmailBody = REPLACE(EmailTemplateSchedulerDetail.EmailBody,'+ @Replacer +',isnull('+ @FieldReplacer+','''') ) ' + char(10)
					 + '	FROM EmailTemplateSchedulerDetail ' + char(10)
					 + '	INNER JOIN '+ @tablename +' ON EmailTemplateSchedulerDetail.UnikFieldTablePrimary= '+ @tablename +'.'+ @FieldUnikPrimaryTable +'   ' + char(10)
					 + '	WHERE EmailTemplateSchedulerDetail.PK_EmailTemplateSchedulerDetail_ID= '+ CONVERT(VARCHAR(20), @PK_EmailTemplateSchedulerDetail_ID) +' '+ char(10)
                  
			EXEC(@sql)

			SET @sql = '	UPDATE EmailTemplateSchedulerDetail ' + char(10)
					 + '	SET EmailTemplateSchedulerDetail.AttachmentPassword = REPLACE(EmailTemplateSchedulerDetail.AttachmentPassword,'+ @Replacer +',isnull('+ @FieldReplacer+','''') ) ' + char(10)
					 + '	FROM EmailTemplateSchedulerDetail ' + char(10)
					 + '	INNER JOIN '+ @tablename +' ON EmailTemplateSchedulerDetail.UnikFieldTablePrimary= '+ @tablename +'.'+ @FieldUnikPrimaryTable +'   ' + char(10)
					 + '	WHERE EmailTemplateSchedulerDetail.PK_EmailTemplateSchedulerDetail_ID= '+ CONVERT(VARCHAR(20), @PK_EmailTemplateSchedulerDetail_ID) +' '+ char(10)
                  
			EXEC(@sql)

			-- Replace Attachment			
			SET @sql = '	UPDATE EmailTemplateSchedulerDetailAttachment ' + char(10)
					 + '	SET EmailTemplateSchedulerDetailAttachment.NamaReport = REPLACE(EmailTemplateSchedulerDetailAttachment.NamaReport,'+ @Replacer +',isnull('+ @FieldReplacer+','''') ) ' + char(10)
					 + '	FROM EmailTemplateSchedulerDetailAttachment ' + char(10)
					 + '	INNER JOIN EmailTemplateSchedulerDetail ON EmailTemplateSchedulerDetailAttachment.FK_EmailTEmplateSchedulerDetail_ID=EmailTemplateSchedulerDetail.PK_EmailTEmplateSchedulerDetail_ID ' + char(10)
					 + '	INNER JOIN '+ @tablename +' ON EmailTemplateSchedulerDetail.UnikFieldTablePrimary= '+ @tablename +'.'+ @FieldUnikPrimaryTable +'   ' + char(10)
					 + '	WHERE EmailTemplateSchedulerDetail.PK_EmailTemplateSchedulerDetail_ID= '+ CONVERT(VARCHAR(20), @PK_EmailTemplateSchedulerDetail_ID) +' '+ char(10)
                  
			EXEC(@sql)

			SET @sql = '	UPDATE EmailTemplateSchedulerDetailAttachment ' + char(10)
					 + '	SET EmailTemplateSchedulerDetailAttachment.ParameterReport = REPLACE(EmailTemplateSchedulerDetailAttachment.ParameterReport,'+ @Replacer +',isnull('+ @FieldReplacer+','''') ) ' + char(10)
					 + '	FROM EmailTemplateSchedulerDetailAttachment ' + char(10)
					 + '	INNER JOIN EmailTemplateSchedulerDetail ON EmailTemplateSchedulerDetailAttachment.FK_EmailTEmplateSchedulerDetail_ID=EmailTemplateSchedulerDetail.PK_EmailTEmplateSchedulerDetail_ID ' + char(10)
					 + '	INNER JOIN '+ @tablename +' ON EmailTemplateSchedulerDetail.UnikFieldTablePrimary= '+ @tablename +'.'+ @FieldUnikPrimaryTable +'   ' + char(10)
					 + '	WHERE EmailTemplateSchedulerDetail.PK_EmailTemplateSchedulerDetail_ID= '+ CONVERT(VARCHAR(20), @PK_EmailTemplateSchedulerDetail_ID) +' '+ char(10)
                  
			EXEC(@sql)

			SET @sql = '	UPDATE EmailTemplateSchedulerDetailAttachment ' + char(10)
					 + '	SET EmailTemplateSchedulerDetailAttachment.FileName = REPLACE(EmailTemplateSchedulerDetailAttachment.FileName,'+ @Replacer +',isnull('+ @FieldReplacer+','''') ) ' + char(10)
					 + '	FROM EmailTemplateSchedulerDetailAttachment ' + char(10)
					 + '	INNER JOIN EmailTemplateSchedulerDetail ON EmailTemplateSchedulerDetailAttachment.FK_EmailTEmplateSchedulerDetail_ID=EmailTemplateSchedulerDetail.PK_EmailTEmplateSchedulerDetail_ID ' + char(10)
					 + '	INNER JOIN '+ @tablename +' ON EmailTemplateSchedulerDetail.UnikFieldTablePrimary= '+ @tablename +'.'+ @FieldUnikPrimaryTable +'   ' + char(10)
					 + '	WHERE EmailTemplateSchedulerDetail.PK_EmailTemplateSchedulerDetail_ID= '+ CONVERT(VARCHAR(20), @PK_EmailTemplateSchedulerDetail_ID) +' '+ char(10)
                  
			EXEC(@sql)
		
		END
		ELSE
		BEGIN
			--	UPDATE EmailTemplateSchedulerDetail
			--	SET EmailTo = REPLACE(emailto,@Replacer,@nilaireplace)
			--	FROM EmailTemplateSchedulerDetail	  
			--	WHERE EmailTemplateSchedulerDetail.PK_EmailTemplateSchedulerDetail_ID=@PK_EmailTemplateSchedulerDetail_ID
			DECLARE @sql1 varchar(max)
	
			SET @sql1 = ' ' + char(10)
					 + 'DECLARE @nilaireplace VARCHAR(MAX)	=''''		 ' + char(10)
					 + ' SELECT @nilaireplace=@nilaireplace + isnull('+  @FieldReplacer  +','''') FROM '+ @tablename +' ' + char(10)
					 + '	UPDATE EmailTemplateSchedulerDetail ' + char(10)
					 + '	SET EmailTemplateSchedulerDetail.EmailTo = REPLACE(EmailTemplateSchedulerDetail.EmailTo,'+ @Replacer+' ,  @nilaireplace) ' + char(10)
					 + '	FROM EmailTemplateSchedulerDetail	   ' + char(10)
					 + '	WHERE EmailTemplateSchedulerDetail.PK_EmailTemplateSchedulerDetail_ID= '+ convert(varchar(20),@PK_EmailTemplateSchedulerDetail_ID) +' ' + char(10)
					 + '	'

			EXEC(@sql1)


			SET @sql1 = ' ' + char(10)
					 + 'DECLARE @nilaireplace VARCHAR(MAX)	=''''		 ' + char(10)
					 + ' SELECT @nilaireplace=@nilaireplace + isnull('+  @FieldReplacer  +','''') FROM '+ @tablename +' ' + char(10)
					 + '	UPDATE EmailTemplateSchedulerDetail ' + char(10)
					 + '	SET EmailTemplateSchedulerDetail.EmailCC = REPLACE(EmailTemplateSchedulerDetail.EmailCC,'+ @Replacer+' ,  @nilaireplace) ' + char(10)
					 + '	FROM EmailTemplateSchedulerDetail	   ' + char(10)
					 + '	WHERE EmailTemplateSchedulerDetail.PK_EmailTemplateSchedulerDetail_ID= '+ convert(varchar(20),@PK_EmailTemplateSchedulerDetail_ID) +' ' + char(10)
					 + '	'

			EXEC(@sql1)


			SET @sql1 = ' ' + char(10)
					 + 'DECLARE @nilaireplace VARCHAR(MAX)	=''''		 ' + char(10)
					 + ' SELECT @nilaireplace=@nilaireplace + isnull('+  @FieldReplacer  +','''') FROM '+ @tablename +' ' + char(10)
					 + '	UPDATE EmailTemplateSchedulerDetail ' + char(10)
					 + '	SET EmailTemplateSchedulerDetail.EmailBCC = REPLACE(EmailTemplateSchedulerDetail.EmailBCC,'+ @Replacer+' ,  @nilaireplace) ' + char(10)
					 + '	FROM EmailTemplateSchedulerDetail	   ' + char(10)
					 + '	WHERE EmailTemplateSchedulerDetail.PK_EmailTemplateSchedulerDetail_ID= '+ convert(varchar(20),@PK_EmailTemplateSchedulerDetail_ID) +' ' + char(10)
					 + '	'

			EXEC(@sql1)



			SET @sql1 = ' ' + char(10)
					 + 'DECLARE @nilaireplace VARCHAR(MAX)	=''''		 ' + char(10)
					 + ' SELECT @nilaireplace=@nilaireplace + isnull('+  @FieldReplacer  +','''') FROM '+ @tablename +' ' + char(10)
					 + '	UPDATE EmailTemplateSchedulerDetail ' + char(10)
					 + '	SET EmailTemplateSchedulerDetail.EmailSubject = REPLACE(EmailTemplateSchedulerDetail.EmailSubject,'+ @Replacer+' ,  @nilaireplace) ' + char(10)
					 + '	FROM EmailTemplateSchedulerDetail	   ' + char(10)
					 + '	WHERE EmailTemplateSchedulerDetail.PK_EmailTemplateSchedulerDetail_ID= '+ convert(varchar(20),@PK_EmailTemplateSchedulerDetail_ID) +' ' + char(10)
					 + '	'

			EXEC(@sql1)
			SET @sql1 = ' ' + char(10)
					 + 'DECLARE @nilaireplace VARCHAR(MAX)	=''''		 ' + char(10)
					 + ' SELECT @nilaireplace=@nilaireplace + isnull('+  @FieldReplacer  +','''') FROM '+ @tablename +' ' + char(10)
					 + '	UPDATE EmailTemplateSchedulerDetail ' + char(10)
					 + '	SET EmailTemplateSchedulerDetail.Emailbody = REPLACE(EmailTemplateSchedulerDetail.Emailbody,'+ @Replacer+' ,  @nilaireplace) ' + char(10)
					 + '	FROM EmailTemplateSchedulerDetail	   ' + char(10)
					 + '	WHERE EmailTemplateSchedulerDetail.PK_EmailTemplateSchedulerDetail_ID= '+ convert(varchar(20),@PK_EmailTemplateSchedulerDetail_ID) +' ' + char(10)
					 + '	'

			EXEC(@sql1)
			SET @sql1 = ' ' + char(10)
					 + 'DECLARE @nilaireplace VARCHAR(MAX)	=''''		 ' + char(10)
					 + ' SELECT @nilaireplace=@nilaireplace + isnull('+  @FieldReplacer  +','''') FROM '+ @tablename +' ' + char(10)
					 + '	UPDATE EmailTemplateSchedulerDetail ' + char(10)
					 + '	SET EmailTemplateSchedulerDetail.AttachmentPassword = REPLACE(EmailTemplateSchedulerDetail.AttachmentPassword,'+ @Replacer+' ,  @nilaireplace) ' + char(10)
					 + '	FROM EmailTemplateSchedulerDetail	   ' + char(10)
					 + '	WHERE EmailTemplateSchedulerDetail.PK_EmailTemplateSchedulerDetail_ID= '+ convert(varchar(20),@PK_EmailTemplateSchedulerDetail_ID) +' ' + char(10)
					 + '	'

			EXEC(@sql1)

			SET @sql1 = ' ' + char(10)
					 + 'DECLARE @nilaireplace VARCHAR(MAX)	=''''		 ' + char(10)
					 + ' SELECT @nilaireplace=@nilaireplace + isnull('+  @FieldReplacer  +','''') FROM '+ @tablename +' ' + char(10)
					 + '	UPDATE EmailTemplateSchedulerDetailAttachment ' + char(10)
					 + '	SET EmailTemplateSchedulerDetailAttachment.NamaReport = REPLACE(EmailTemplateSchedulerDetailAttachment.NamaReport,'+ @Replacer +', @nilaireplace) ' + char(10)
					 + '	FROM EmailTemplateSchedulerDetailAttachment ' + char(10)
					 + '	INNER JOIN EmailTemplateSchedulerDetail ON EmailTemplateSchedulerDetailAttachment.FK_EmailTEmplateSchedulerDetail_ID=EmailTemplateSchedulerDetail.PK_EmailTEmplateSchedulerDetail_ID ' + char(10)
					 + '	WHERE EmailTemplateSchedulerDetail.PK_EmailTemplateSchedulerDetail_ID= '+ CONVERT(VARCHAR(20), @PK_EmailTemplateSchedulerDetail_ID) +' '+ char(10)
                  
			EXEC(@sql1)

			SET @sql1 = ' ' + char(10)
					 + 'DECLARE @nilaireplace VARCHAR(MAX)	=''''		 ' + char(10)
					 + ' SELECT @nilaireplace=@nilaireplace + isnull('+  @FieldReplacer  +','''') FROM '+ @tablename +' ' + char(10)
					 + '	UPDATE EmailTemplateSchedulerDetailAttachment ' + char(10)
					 + '	SET EmailTemplateSchedulerDetailAttachment.ParameterReport = REPLACE(EmailTemplateSchedulerDetailAttachment.ParameterReport,'+ @Replacer +', @nilaireplace) ' + char(10)
					 + '	FROM EmailTemplateSchedulerDetailAttachment ' + char(10)
					 + '	INNER JOIN EmailTemplateSchedulerDetail ON EmailTemplateSchedulerDetailAttachment.FK_EmailTEmplateSchedulerDetail_ID=EmailTemplateSchedulerDetail.PK_EmailTEmplateSchedulerDetail_ID ' + char(10)
					 + '	WHERE EmailTemplateSchedulerDetail.PK_EmailTemplateSchedulerDetail_ID= '+ CONVERT(VARCHAR(20), @PK_EmailTemplateSchedulerDetail_ID) +' '+ char(10)
                  
			EXEC(@sql1)

			SET @sql1 = ' ' + char(10)
					 + 'DECLARE @nilaireplace VARCHAR(MAX)	=''''		 ' + char(10)
					 + ' SELECT @nilaireplace=@nilaireplace + isnull('+  @FieldReplacer  +','''') FROM '+ @tablename +' ' + char(10)
					 + '	UPDATE EmailTemplateSchedulerDetailAttachment ' + char(10)
					 + '	SET EmailTemplateSchedulerDetailAttachment.FileName = REPLACE(EmailTemplateSchedulerDetailAttachment.FileName,'+ @Replacer +', @nilaireplace) ' + char(10)
					 + '	FROM EmailTemplateSchedulerDetailAttachment ' + char(10)
					 + '	INNER JOIN EmailTemplateSchedulerDetail ON EmailTemplateSchedulerDetailAttachment.FK_EmailTEmplateSchedulerDetail_ID=EmailTemplateSchedulerDetail.PK_EmailTEmplateSchedulerDetail_ID ' + char(10)
					 + '	WHERE EmailTemplateSchedulerDetail.PK_EmailTemplateSchedulerDetail_ID= '+ CONVERT(VARCHAR(20), @PK_EmailTemplateSchedulerDetail_ID) +' '+ char(10)
                  
			EXEC(@sql1)
		END
	
	END

	--tambahan replacer yang ada imagenya
	update EmailTemplateSchedulerDetail
	set EmailBody=replace(EmailBody,'$ID$',EmailID)
	where EmailBody like '%<img%' and EmailBody like '%src=%'
	and EmailBody like '%$ID$%' and PK_EmailTemplateSchedulerDetail_ID=@PK_EmailTemplateSchedulerDetail_ID
END
GO
