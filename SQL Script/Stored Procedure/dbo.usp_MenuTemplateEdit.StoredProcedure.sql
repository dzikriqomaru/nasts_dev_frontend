/****** Object:  StoredProcedure [dbo].[usp_MenuTemplateEdit]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_MenuTemplateEdit]
/***********************************************************
* Procedure description:
* Date:   11/18/2019 
* Author: nawadata
*
* Changes
* Date		Modified By			Comments
************************************************************
* 20221005	Davin J				Add Audit Trail Log 
* 20230331  Humam				Add IconName
************************************************************/
(
	@tblMenuTemplate AS tblMenuTemplate READONLY,
	@menuTemplateJSON AS VARCHAR(MAX),
	@createdBy AS VARCHAR(50),
	@approvedBy AS VARCHAR(50)
)
AS
BEGIN
SELECT * FROM @tblMenuTemplate;

	DELETE FROM MenuTemplate
	INSERT INTO MenuTemplate
	(
		mMenuID,
		mMenuLabel,
		mMenuParentID,
		mMenuURL,
		FK_Module_ID,
		FK_Action_ID,
		urutan,
		IconName
	)
	SELECT 
		mMenuID,
		mMenuLabel,
		mMenuParentID,
		mMenuURL,
		CASE WHEN FK_Module_ID > 0 THEN FK_Module_ID END,
		CASE WHEN FK_Action_ID > 0 THEN FK_Action_ID END,
		urutan,
		CASE WHEN IconName != '' THEN IconName END
	FROM @tblMenuTemplate
	
	DECLARE @pkAudittrailheader BIGINT
	INSERT INTO AuditTrailHeader
	(
		-- PK_AuditTrail_ID -- this column value is auto-generated
		CreatedDate,
		CreatedBy,
		ApproveBy,
		ModuleLabel,
		FK_ModuleAction_ID,
		FK_AuditTrailStatus_ID
	)
	VALUES
	(
		GETDATE(),
		@createdBy,
		@approvedBy,
		'Menu Template',
		2,
		1
	)
	
	SET @pkAudittrailheader = SCOPE_IDENTITY()
	
	 INSERT INTO AuditTrailLog    
   (    
    CreatedDate,    
    CreatedBy,  
	LastUpdateDate,
	LastUpdateBy,
    LogName,    
    LogTime,    
    LogDescription,    
    LogCode   ,
	LogSeverityCode
   )    
       
   VALUES    
   (    
   GETDATE(),    
    @createdBy,  
   GETDATE(),    
    @createdBy,    
    'Menu Template - Update',    
    GETDATE(),    
    'Affected to Database - CreatedBy: '+@createdBy+ '- ApprovalBy: ' + @approvedBy,      
    NEWID()   ,
	0
   )    

	INSERT INTO AuditTrailDetail
	(
		-- PK_AuditTrailDetail_id -- this column value is auto-generated
		FK_AuditTrailHeader_ID,
		FieldName,
		OldValue,
		NewValue
	)
	SELECT @pkAudittrailheader     FK_AuditTrailHeader_ID,
	       ca.fieldname,
	       ''                      OldValue,
	       ca.NewValue
	FROM   (
			SELECT * FROM menutemplate
	       )xx
	       CROSS APPLY (
			VALUES 
				('mMenuID', try_convert(varchar(8000),mMenuID)),
				('mMenuLabel', try_convert(varchar(8000),mMenuLabel)),
				('mMenuParentID', try_convert(varchar(8000),mMenuParentID)),
				('mMenuURL', try_convert(varchar(8000),mMenuURL)),
				('FK_Module_ID', try_convert(varchar(8000),FK_Module_ID)),
				('FK_Action_ID', try_convert(varchar(8000),FK_Action_ID)),
				('urutan', try_convert(varchar(8000),urutan)),
				('IconName', try_convert(varchar(8000),IconName))
		   ) ca(Fieldname, NewValue)
	  
	IF EXISTS(SELECT * FROM MenuTemplateJSON WHERE Active = 1)
	BEGIN
		UPDATE MenuTemplateJSON
		SET MenuTemplateJSON = @menuTemplateJSON
		WHERE Active = 1
	END
	ELSE
	BEGIN
		INSERT INTO MenuTemplateJSON VALUES(@menuTemplateJSON, 1, @createdBy, @createdBy, @approvedBy, GETDATE(), GETDATE(), GETDATE(), '')
	END
END
GO
