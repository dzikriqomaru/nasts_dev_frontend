/****** Object:  StoredProcedure [dbo].[usp_DeleteApplicationLog]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[usp_DeleteApplicationLog]
as
declare  @retentionPeriod int 
		,@DeletedDate datetime

set @retentionPeriod =  (select value from ApplicationParameter where name ='Retention Periode 3')
set @DeletedDate = DATEADD(day,-1*@retentionPeriod,cast(GETDATE() as date))


delete [dbo].[ELMAH_Error] where TimeUtc < @DeletedDate
delete EODTaskLog 
	   from EODTaskLog a
	   inner join EODTaskDetailLog b on a.PK_EODTaskLog_ID = b.EODTaskLogID
	   where b.StartDate < @DeletedDate
delete EODSchedulerLog
	   from EODSchedulerLog a
	   inner join EODTaskDetailLog b on a.PK_EODSchedulerLog_ID = b.EODSchedulerLogID
	   where b.StartDate < @DeletedDate
delete EODTaskDetailLog where StartDate < @DeletedDate
delete TextFileTemporaryTable where StartDate < @DeletedDate




DECLARE  @Query varchar(MAX)
		,@ColumnToUpdate VARCHAR(8000) 
		,@ColumnToInsert VARCHAR(8000) 
		,@ArchieveDatabase varchar(100)
set @ArchieveDatabase = (select value from ApplicationParameter where name ='Archieve Database')+'.'


-- 1 delete GeneratedFileList
SELECT 
	@ColumnToUpdate = COALESCE(@ColumnToUpdate + ', '+char(13)+char(9), '') + 'T.' + a.Name +' = S.'+a.Name
   ,@ColumnToInsert = COALESCE(@ColumnToInsert + ',  ', '') + '' + a.Name
from sys.all_columns a
inner join sys.tables b on b.object_id = a.object_id
where b.name = 'GeneratedFileList' and is_identity = 0
SET @Query ='
set identity_insert '+@ArchieveDatabase+'dbo.GeneratedFileList ON

MERGE '+@ArchieveDatabase+'dbo.GeneratedFileList T
USING (
	SELECT * FROM dbo.GeneratedFileList 
	where StartDate < '''+convert(varchar,@DeletedDate,120)+'''

) S ON S.PK_GeneratedFileList_ID = T.PK_GeneratedFileList_ID
WHEN MATCHED 
	THEN 
	UPDATE SET 
	'+@ColumnToUpdate+'
WHEN NOT MATCHED THEN
	INSERT(  PK_GeneratedFileList_ID,  '+@ColumnToInsert+')
	VALUES(S.PK_GeneratedFileList_ID,S.'+REPLACE(@ColumnToInsert,',  ',',S.')+');

DELETE dbo.GeneratedFileList 
WHERE StartDate < '''+convert(varchar,@DeletedDate,120)+'''
set identity_insert '+@ArchieveDatabase+'dbo.GeneratedFileList OFF
'
PRINT(@Query)
EXEC(@Query)


-- Audit Trail Header
SET @ColumnToUpdate = null
SET @ColumnToInsert = null
SELECT 
	@ColumnToUpdate = COALESCE(@ColumnToUpdate + ', '+char(13)+char(9), '') + 'T.' + a.Name +' = S.'+a.Name
   ,@ColumnToInsert = COALESCE(@ColumnToInsert + ',  ', '') + '' + a.Name
from sys.all_columns a
inner join sys.tables b on b.object_id = a.object_id
where b.name = 'AuditTrailHeader' and is_identity = 0

SET @Query ='
set identity_insert '+@ArchieveDatabase+'dbo.AuditTrailHeader ON

MERGE '+@ArchieveDatabase+'dbo.AuditTrailHeader T
USING (
	SELECT * FROM dbo.AuditTrailHeader 
	where CreatedDate < '''+convert(varchar,@DeletedDate,120)+'''

) S ON S.PK_AuditTrail_ID = T.PK_AuditTrail_ID
WHEN MATCHED 
	THEN 
	UPDATE SET 
	'+@ColumnToUpdate+'
WHEN NOT MATCHED THEN
	INSERT(  PK_AuditTrail_ID,  '+@ColumnToInsert+')
	VALUES(S.PK_AuditTrail_ID,S.'+REPLACE(@ColumnToInsert,',  ',',S.')+');

set identity_insert '+@ArchieveDatabase+'dbo.AuditTrailHeader OFF
'
PRINT(@Query)
EXEC(@Query)

-- Audit Trail Detail
SET @ColumnToUpdate = null
SET @ColumnToInsert = null
SELECT 
	@ColumnToUpdate = COALESCE(@ColumnToUpdate + ', '+char(13)+char(9), '') + 'T.' + a.Name +' = S.'+a.Name
   ,@ColumnToInsert = COALESCE(@ColumnToInsert + ',  ', '') + '' + a.Name
from sys.all_columns a
inner join sys.tables b on b.object_id = a.object_id
where b.name = 'AuditTrailDetail' and is_identity = 0

SET @Query ='
set identity_insert '+@ArchieveDatabase+'dbo.AuditTrailDetail ON

MERGE '+@ArchieveDatabase+'dbo.AuditTrailDetail T
USING (
	SELECT a.* FROM dbo.AuditTrailDetail a
	inner join dbo.AuditTrailHeader b on a.FK_AuditTrailHeader_ID = b.PK_AuditTrail_ID
	where CreatedDate < '''+convert(varchar,@DeletedDate,120)+'''

) S ON S.PK_AuditTrailDetail_id = T.PK_AuditTrailDetail_id
WHEN MATCHED 
	THEN 
	UPDATE SET 
	'+@ColumnToUpdate+'
WHEN NOT MATCHED THEN
	INSERT(  PK_AuditTrailDetail_id,  '+@ColumnToInsert+')
	VALUES(S.PK_AuditTrailDetail_id,S.'+REPLACE(@ColumnToInsert,',  ',',S.')+');
DELETE dbo.AuditTrailHeader 
WHERE CreatedDate < '''+convert(varchar,@DeletedDate,120)+'''
set identity_insert '+@ArchieveDatabase+'dbo.AuditTrailDetail OFF
'
PRINT(@Query)
EXEC(@Query)

-- AuditTrail User Access
SET @ColumnToUpdate = null
SET @ColumnToInsert = null
SELECT 
	@ColumnToUpdate = COALESCE(@ColumnToUpdate + ', '+char(13)+char(9), '') + 'T.' + a.Name +' = S.'+a.Name
   ,@ColumnToInsert = COALESCE(@ColumnToInsert + ',  ', '') + '' + a.Name
from sys.all_columns a
inner join sys.tables b on b.object_id = a.object_id
where b.name = 'AuditTrail_UserAccess' and is_identity = 0

SET @Query ='
set identity_insert '+@ArchieveDatabase+'dbo.AuditTrail_UserAccess ON

MERGE '+@ArchieveDatabase+'dbo.AuditTrail_UserAccess T
USING (
	SELECT * FROM dbo.AuditTrail_UserAccess 
	where ActionDate < '''+convert(varchar,@DeletedDate,120)+'''

) S ON S.PK_UserAccessID = T.PK_UserAccessID
WHEN MATCHED 
	THEN 
	UPDATE SET 
	'+@ColumnToUpdate+'
WHEN NOT MATCHED THEN
	INSERT(  PK_UserAccessID,  '+@ColumnToInsert+')
	VALUES(S.PK_UserAccessID,S.'+REPLACE(@ColumnToInsert,',  ',',S.')+');

DELETE dbo.AuditTrail_UserAccess 
WHERE ActionDate < '''+convert(varchar,@DeletedDate,120)+'''
set identity_insert '+@ArchieveDatabase+'dbo.AuditTrail_UserAccess OFF
'
PRINT(@Query)
EXEC(@Query)

-- AuditTrail User Login
SET @ColumnToUpdate = null
SET @ColumnToInsert = null
SELECT 
	@ColumnToUpdate = COALESCE(@ColumnToUpdate + ', '+char(13)+char(9), '') + 'T.' + a.Name +' = S.'+a.Name
   ,@ColumnToInsert = COALESCE(@ColumnToInsert + ',  ', '') + '' + a.Name
from sys.all_columns a
inner join sys.tables b on b.object_id = a.object_id
where b.name = 'AuditTrail_UserLogin' and is_identity = 0

SET @Query ='
set identity_insert '+@ArchieveDatabase+'dbo.AuditTrail_UserLogin ON

MERGE '+@ArchieveDatabase+'dbo.AuditTrail_UserLogin T
USING (
	SELECT * FROM dbo.AuditTrail_UserLogin 
	where UserLoginActionDate < '''+convert(varchar,@DeletedDate,120)+'''

) S ON S.UserLoginID = T.UserLoginID
WHEN MATCHED 
	THEN 
	UPDATE SET 
	'+@ColumnToUpdate+'
WHEN NOT MATCHED THEN
	INSERT(  UserLoginID,  '+@ColumnToInsert+')
	VALUES(S.UserLoginID,S.'+REPLACE(@ColumnToInsert,',  ',',S.')+');

DELETE dbo.AuditTrail_UserLogin 
WHERE UserLoginActionDate < '''+convert(varchar,@DeletedDate,120)+'''
set identity_insert '+@ArchieveDatabase+'dbo.AuditTrail_UserLogin OFF
'
PRINT(@Query)
EXEC(@Query)
GO
