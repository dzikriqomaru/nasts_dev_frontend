/****** Object:  StoredProcedure [dbo].[Nawa_usp_EncryptRemoteConnectionData]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Nawa_usp_EncryptRemoteConnectionData]  
/*********************  
* Procedure description:  
* Date:   9/2/2017   
* Author: Hendra  
*  
* Changes  
* Date  Modified By   Comments  
********************  
*  
********************/ 





 
(  


 @PK_Conn_ID BIGINT   
)  
AS  
BEGIN  

DECLARE @Password VARCHAR(MAX)
SELECT @Password =password FROM remoteConnection WHERE @PK_Conn_ID= PK_Conn_ID
DECLARE @decryptedPass VARBINARY(MAX)
DECLARE @result VARCHAR(MAX)

	IF (TRY_CONVERT(VARBINARY(MAX), @Password, 1) IS NULL)
	BEGIN
	OPEN SYMMETRIC KEY ConfigSymKey DECRYPTION BY CERTIFICATE ConfigCert
	UPDATE remoteConnection
	SET passwordSalt = Key_GUID('ConfigSymKey')
	WHERE @PK_Conn_ID=PK_Conn_ID

	UPDATE remoteConnection
	SET password =  '0x'+ CONVERT(VARCHAR(MAX),EncryptByKey(CAST(passwordSalt AS uniqueidentifier),ISNULL( @Password,'') ),2)
	
	WHERE
	@PK_Conn_ID=PK_Conn_ID
	END
END
GO
