/****** Object:  StoredProcedure [dbo].[usp_GetPopUpCombo]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_GetPopUpCombo]
/***********************************************************
* Function description:
* Date:   7/18/2019  
* Author: nawadata
*
* Changes
* Date		Modified By			Comments
************************************************************
*
************************************************************/
(
	@tablename VARCHAR(500),@Displayfieldname VARCHAR(500),@KeyFieldName VARCHAR(500),@valuefield VARCHAR(MAX)
) 
as
BEGIN
	
	
	DECLARE @sql varchar(max)
SET @sql = 'SELECT ['+@Displayfieldname +'] AS DisplayName FROM ['+@tablename+'] WHERE ['+@KeyFieldName+']='''+@valuefield+''''
EXEC(@sql)




	
END
GO
