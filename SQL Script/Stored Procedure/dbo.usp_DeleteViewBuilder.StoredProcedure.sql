/****** Object:  StoredProcedure [dbo].[usp_DeleteViewBuilder]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Davin Julian
-- Create date: 3 Juli 2023
-- Description:	Delete view builder after delete master view builder
-- =============================================
CREATE   PROCEDURE [dbo].[usp_DeleteViewBuilder] 
	@FK_MViewBuilder_ID BIGINT
AS
BEGIN
	Delete ViewBuilder
	where FK_MViewBuilder_ID = @FK_MViewBuilder_ID
END
GO
