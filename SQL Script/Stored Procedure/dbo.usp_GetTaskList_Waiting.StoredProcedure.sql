/****** Object:  StoredProcedure [dbo].[usp_GetTaskList_Waiting]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_GetTaskList_Waiting]  
/***********************************************************  
* Procedure description  
* Date   10/7/2015  
* Author Hendra  
*  
* Changes  
* Date  Modified By   Comments  
************************************************************  
*  
************************************************************/  
(  
    @userid          VARCHAR(50),  
    @groupmenuid     INT,  
    @actionid        INT,  
    @orderby         VARCHAR(4000),  
    @whereclause     VARCHAR(4000),  
    @PageIndex       INT,  
    @PageSize        INT  
)  
AS  
BEGIN  
 DECLARE @roleid VARCHAR(20)  
 DECLARE @pkuserid INT  
   
 SELECT @roleid = m.FK_MRole_ID,@pkuserid =m.PK_MUser_ID      
 FROM   MUser m      
 WHERE  m.UserID = @userid      
   
 DECLARE @sql VARCHAR(8000)  
 SET @sql = ' ' + CHAR(10)  
     + ' WITH PageIndex AS ( '  
     +  
     'SELECT xx.jumlah as jumlah,ab.ModuleLabel AS ModuleName,  ''/Parameter/WaitingApproval.aspx''mMenuURL,yy.FK_Module_ID as ModuleID FROM ( '  
     + CHAR(10)  
     + 'SELECT COUNT(1) AS jumlah,ma.ModuleName NamaModule FROM ModuleApproval ma ' +  
     CHAR(10)  
     + ' INNER JOIN MUser m ON m.UserID=ma.CreatedBy ' + CHAR(10)  
     + 'WHERE ma.CreatedBy =''' + @userid + '''' + CHAR(10) +  
     ' AND m.FK_MRole_ID= ' + @roleid  
     + 'GROUP BY ma.ModuleName ' + CHAR(10)  
     + ')xx ' + CHAR(10)  
     + 'INNER JOIN ( ' + CHAR(10)  
     + ' ' + CHAR(10)  
     +  
     'SELECT distinct m.Modulename as namamodule,''/Parameter/WaitingApproval.aspx'' mMenuURL,mms.FK_Module_ID '  
     + CHAR(10)  
     + '  FROM MGroupMenuSettting mms ' + CHAR(10)  
     + 'INNER JOIN Module m ON mms.FK_Module_ID=m.PK_Module_ID ' + CHAR(10)  
     + 'WHERE mms.FK_MGroupMenu_ID=' + CONVERT(VARCHAR(50), @groupmenuid) +  
     ' AND mms.FK_Action_ID=' + CONVERT(VARCHAR(50), @actionid)  
     + CHAR(10)  
     

     + ')yy ON xx.NamaModule=yy.namamodule ' + CHAR(10)  
   +' INNER JOIN Module ab ON ab.ModuleName=xx.NamaModule '


 SET @sql = @sql + ') select * From pageindex'  
 IF LEN(@whereclause) > 0  
     SET @sql = @sql + ' where ' + @whereclause  
 SET @sql = @sql + ' ORDER BY  ' + @orderby + CHAR(10)  
 PRINT @sql  
 EXEC (@sql)  
END
GO
