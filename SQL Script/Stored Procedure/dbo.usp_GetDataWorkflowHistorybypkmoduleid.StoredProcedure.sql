/****** Object:  StoredProcedure [dbo].[usp_GetDataWorkflowHistorybypkmoduleid]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  CREATE PROCEDURE [dbo].[usp_GetDataWorkflowHistorybypkmoduleid] 
  /***********************************************************
  * Procedure description:
  * Date:   5/6/2019 
  * Author: nawadata
  *
  * Changes
  * Date		Modified By			Comments
  ************************************************************
  *
  ************************************************************/
  (
  	@pkmoduleapprovalid BIGINT
  )
  AS
  BEGIN
 
 

SELECT mfh.PK_MWorkflow_History_ID, mfh.FK_ModuleApproval_ID, mfh.FK_Module_ID,
       mfh.FK_Unik_ID, mfh.FK_MUserId, mfh.FK_MRoleId, mfh.intLevel, mfh.RoleName,
       mfh.UserName, mfh.UserNameExecute, mfh.ResponseDate,
       mfh.FK_MWorkflow_ApprovalStatus_ID, mfh.Notes, mas.ApprovalStatusName ,mfh.CreatedDate
  FROM MWorkFlow_History AS mfh 
left JOIN MWorkflow_ApprovalStatus AS mas ON mfh.FK_MWorkflow_ApprovalStatus_ID=mas.PK_MWorkflow_ApprovalStatus_ID
WHERE mfh.FK_ModuleApproval_ID =@pkmoduleapprovalid 
ORDER BY mfh.PK_MWorkflow_History_ID desc

 
 
  END
GO
