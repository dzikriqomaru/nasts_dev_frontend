/****** Object:  StoredProcedure [dbo].[usp_DeleteModuleDetailByModuleID]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_DeleteModuleDetailByModuleID]
/***********************************************************
* Procedure description:
* Date:   19/09/2022
* Author: Fauzan
*
* Changes
* Date		Modified By			Comments
************************************************************
*
************************************************************/
(@PK_Module_ID INT)
AS
BEGIN
	DECLARE @PK_ModuleDetail_ID int, @ModuleDetailName varchar(250)

	DECLARE my_cursor CURSOR FAST_FORWARD READ_ONLY FOR
	SELECT PK_ModuleDetail_ID, ModuleDetailName
	FROM dbo.ModuleDetail
	WHERE FK_Module_ID = @PK_Module_ID

	OPEN my_cursor
	FETCH FROM my_cursor INTO @PK_ModuleDetail_ID, @ModuleDetailName
	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES t WHERE t.TABLE_NAME = @ModuleDetailName)
		EXEC ('DROP TABLE ' + @ModuleDetailName)

		DECLARE @ModuleDetailNameUpload VARCHAR(300)
		SET @ModuleDetailNameUpload = @ModuleDetailName + '_Upload'
		IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES t WHERE t.TABLE_NAME = @ModuleDetailNameUpload)
		EXEC ('DROP TABLE ' + @ModuleDetailNameUpload)

		DECLARE @ModuleDetailNameUploadApproval VARCHAR(350)
		SET @ModuleDetailNameUpload = @ModuleDetailName + '_Upload_Approval'
		IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES t WHERE t.TABLE_NAME = @ModuleDetailNameUploadApproval)
		EXEC ('DROP TABLE ' + @ModuleDetailNameUploadApproval)

		DELETE rg FROM ModuleDetailFieldRegex rg 
	    WHERE EXISTS(SELECT 1 FROM ModuleDetailField mf WHERE mf.PK_ModuleDetailField_ID = rg.FK_ModuleDetailField_ID 
	    AND mf.FK_ModuleDetail_ID = @PK_ModuleDetail_ID);

		DELETE FROM ModuleDetailField WHERE FK_ModuleDetail_ID = @PK_ModuleDetail_ID;
		
		DELETE FROM ModuleDetail WHERE PK_ModuleDetail_ID = @PK_ModuleDetail_ID;

		FETCH FROM my_cursor INTO @PK_ModuleDetail_ID, @ModuleDetailName
	END
	CLOSE my_cursor
	DEALLOCATE my_cursor
END

GO
