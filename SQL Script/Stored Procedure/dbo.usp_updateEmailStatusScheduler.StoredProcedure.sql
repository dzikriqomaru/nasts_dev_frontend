/****** Object:  StoredProcedure [dbo].[usp_updateEmailStatusScheduler]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[usp_updateEmailStatusScheduler]
/***********************************************************
* Procedure description:
* Date:   24/02/2016 
* Author: Hendra
*
* Changes
* Date		Modified By			Comments
************************************************************
*
************************************************************/
(
	@PK_EmailTemplateScheduler_ID bigint
)
AS
BEGIN
	
	UPDATE EmailTemplateScheduler
	SET
	    FK_EmailStatus_ID = 2 WHERE PK_EmailTemplateScheduler_ID=@PK_EmailTemplateScheduler_ID
	    
	    
   UPDATE EmailTemplateSchedulerDetail
   SET
       FK_EmailStatus_ID = 2 WHERE FK_EmailTEmplateScheduler_ID=@PK_EmailTemplateScheduler_ID	    
	
END
GO
