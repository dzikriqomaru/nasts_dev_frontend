/****** Object:  StoredProcedure [dbo].[usp_DBNotificationSize]    Script Date: 12/12/2023 11:28:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_DBNotificationSize]
as

select
case  when size*8/1000 > cast(b.SettingValue as float)/1000
then 'Your database exceed DB notification size ('+cast(size*8/1000 as varchar(20))+'/'+cast(cast(b.SettingValue as float)/1000  as varchar(20)) +' MB)'
end as notification
from sys.sysfiles a
cross apply (select * from SystemParameter where PK_SystemParameter_ID = 35) b
where fileid = 1 and size*8/1000 >  cast(b.SettingValue as float)/1000
GO
