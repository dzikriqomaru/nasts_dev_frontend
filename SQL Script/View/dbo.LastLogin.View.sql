/****** Object:  View [dbo].[LastLogin]    Script Date: 12/12/2023 11:22:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[LastLogin] 

AS	 

select UserID
, UserName, UserEmailAddress,LastLogin,datediff (day,lastlogin,getdate()) LastLoginInDays
 from MUser  
 where datediff (day,lastlogin,getdate()) > (select SettingValue from SystemParameter where PK_SystemParameter_ID = 1003)
GO
