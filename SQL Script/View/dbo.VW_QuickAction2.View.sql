/****** Object:  View [dbo].[VW_QuickAction2]    Script Date: 12/12/2023 11:22:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VW_QuickAction2]
AS
SELECT FK_MUser_ID, MenuName, MenuURL, FK_Module_ID, ActionType, COUNT(1) TotalAccess
FROM UserAccessHistory
GROUP BY FK_MUser_ID, MenuName, MenuURL, FK_Module_ID, ActionType
GO
