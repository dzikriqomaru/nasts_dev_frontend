/****** Object:  View [dbo].[VW_MUser]    Script Date: 12/12/2023 11:22:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VW_MUser]
AS
SELECT
    PK_MUser_ID, 
  UserID, 
  UserName, 
  RoleName, 
  GroupMenuName, 
  UserEmailAddress, 
  MUser.Active,
  MUser.LastLogin
FROM dbo.MUser
JOIN dbo.MRole
     ON FK_MRole_ID = PK_MRole_ID
JOIN dbo.MGroupMenu
     ON FK_MGroupMenu_ID = PK_MGroupMenu_ID
GO
