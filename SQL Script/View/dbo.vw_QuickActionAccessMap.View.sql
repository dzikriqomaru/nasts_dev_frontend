/****** Object:  View [dbo].[vw_QuickActionAccessMap]    Script Date: 12/12/2023 11:22:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_QuickActionAccessMap] AS 
SELECT PK_UserAccessHistory_ID, CASE ActionType
            WHEN 1 THEN bAdd
            WHEN 2 THEN bEdit
			WHEN 3 THEN bDelete
			WHEN 4 THEN bActivation
			WHEN 5 THEN bView
			WHEN 6 THEN bApproval
			WHEN 7 THEN bUpload
			WHEN 8 THEN bDetail
            ELSE bView
        END val
FROM vw_QuickAction
GO
