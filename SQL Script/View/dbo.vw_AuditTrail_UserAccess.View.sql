/****** Object:  View [dbo].[vw_AuditTrail_UserAccess]    Script Date: 12/12/2023 11:22:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vw_AuditTrail_UserAccess] as 
select a.PK_UserAccessID, a.Userid, a.ActionDate, a.[Action], c.GroupMenuName 
from AuditTrail_UserAccess a
inner join MUser b on b.UserID = a.Userid
inner join MGroupMenu c on b.FK_MGroupMenu_ID = c.PK_MGroupMenu_ID
GO
