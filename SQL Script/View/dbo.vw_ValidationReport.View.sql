/****** Object:  View [dbo].[vw_ValidationReport]    Script Date: 12/12/2023 11:22:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_ValidationReport]
AS
SELECT        dbo.ValidationReport.TanggalData, dbo.ValidationReport.SegmentData, dbo.ValidationReport.NamaField, dbo.ValidationReport.ValidationMessage, dbo.ValidationReport.KeyFieldValue, dbo.ValidationReport.KeyField, 
                          dbo.ValidationReport.ModuleURL, dbo.ValidationReport.RecordID, dbo.ValidationReport.ModuleID, dbo.ValidationType.ValidationType, dbo.ValidationReport.ID
FROM            dbo.ValidationType INNER JOIN
                         dbo.ValidationReport ON dbo.ValidationType.PK_ValidationType_ID = dbo.ValidationReport.FK_ValidationType
GO
