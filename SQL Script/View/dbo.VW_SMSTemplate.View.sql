/****** Object:  View [dbo].[VW_SMSTemplate]    Script Date: 12/12/2023 11:22:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VW_SMSTemplate]  
AS  
SELECT a.PK_SMSTemplate_ID,
       a.SMSTemplateName,
       b.MonitoringDurationName,
       a.StartDate,
       a.StartTime,
       a.ExcludeHoliday,
       a.SMSTo,
       a.SMSSubject,
       a.SMSBody,
       a.Active
FROM SMSTemplate a  
INNER JOIN MonitoringDuration b  
 ON a.FK_Monitoringduration_ID = b.PK_MonitoringDuration_Id  
  
  
  
  
GO
