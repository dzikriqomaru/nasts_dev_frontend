/****** Object:  View [dbo].[VW_TableProperties]    Script Date: 12/12/2023 11:22:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VW_TableProperties]
AS
SELECT A.name, 'public ' + CASE B.xtype
						WHEN 56 THEN 'int'
						WHEN 167 THEN 'string'
						WHEN 104 THEN 'bool'
						WHEN 61 THEN 'DateTime'
						WHEN 127 THEN 'Int64'
				   END + ' ' + B.name + ' { get; set; }' Properties
FROM sysobjects A
	 JOIN syscolumns B
	 ON A.id = B.id
WHERE A.xtype = 'U'
GO
