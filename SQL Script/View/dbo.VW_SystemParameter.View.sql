/****** Object:  View [dbo].[VW_SystemParameter]    Script Date: 12/12/2023 11:22:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
  
CREATE VIEW [dbo].[VW_SystemParameter] AS SELECT dbo.SystemParameter.PK_SystemParameter_ID, dbo.SystemParameterGroup.SystemParameterGroup, dbo.SystemParameter.SettingName, dbo.SystemParameter.Active, dbo.SystemParameter.fk_MFieldType_ID, CASE WHEN fk_MFieldType_ID = 14 THEN EncriptionKey ELSE 
ISNULL(SystemParameterPickList.ApplicationAuthentication,   
             CASE WHEN IsEncript = 1 THEN '*************' ELSE SettingValue END) END AS SettingValue  
FROM   dbo.SystemParameter INNER JOIN  
             dbo.SystemParameterGroup ON dbo.SystemParameterGroup.PK_SystemParameterGroup_ID = dbo.SystemParameter.FK_SystemParameterGroup_ID LEFT OUTER JOIN  
             dbo.SystemParameterPickList ON dbo.SystemParameter.fk_MFieldType_ID = 11 AND dbo.SystemParameter.SettingName = dbo.SystemParameterPickList.SettingName AND   
             dbo.SystemParameter.SettingValue = CAST(dbo.SystemParameterPickList.Pk_ApplicationAuthentication_ID AS VARCHAR)   
  
GO
