/****** Object:  View [dbo].[Vw_validationReport_List]    Script Date: 12/12/2023 11:22:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_validationReport_List]
AS
SELECT 
mf.FieldName, vr.ID, vr.SegmentData, vr.NamaField, vr.ValidationMessage, vr.KeyFieldValue, vr.KeyField, vr.ModuleURL, vr.RecordID, 
vr.ModuleID, vr.FK_ValidationType, vr.Edited, vr.ErrorType, vr.FieldValue, vr.TanggalData, vr.KodeCabang
FROM validationReport vr
INNER JOIN ModuleField mf
ON vr.ModuleID = mf.FK_Module_ID
AND vr.NamaField = mf.FieldLabel

GO
