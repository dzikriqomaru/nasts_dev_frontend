/****** Object:  View [dbo].[vw_QuickAction]    Script Date: 12/12/2023 11:22:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_QuickAction] AS
SELECT PK_UserAccessHistory_ID, FK_MUser_ID, FK_MGroupMenu_ID, MenuName, MenuURL, UserAccessHistory.FK_Module_ID ,ActionType,bView,bAdd,bEdit,bDelete,bActivation,bApproval,bDetail,bUpload,
COUNT(1) AS [Amount] FROM UserAccessHistory JOIN MUser ON UserAccessHistory.FK_MUser_ID=MUser.PK_MUser_ID
JOIN MGroupMenuAccess ON (MUser.FK_MGroupMenu_ID=MGroupMenuAccess.FK_GroupMenu_ID AND UserAccessHistory.FK_Module_ID=MGroupMenuAccess.FK_Module_ID)
GROUP BY PK_UserAccessHistory_ID,MenuName, FK_MUser_ID, FK_MGroupMenu_ID,MenuURL, UserAccessHistory.FK_Module_ID, ActionType,bView,bAdd,bEdit,bDelete,bActivation,bApproval,bDetail,bUpload
GO
