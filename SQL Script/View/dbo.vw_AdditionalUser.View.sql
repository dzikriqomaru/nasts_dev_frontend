/****** Object:  View [dbo].[vw_AdditionalUser]    Script Date: 12/12/2023 11:22:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_AdditionalUser]   
AS    
SELECT X.UserID,
       PK_MGroupMenu_ID,
       PK_MRole_ID,
       CONCAT(PK_MGroupMenu_ID, '-', GroupMenuName) AS [Group Menu],
       CONCAT(PK_MRole_ID, '-', RoleName) [Role Name]
FROM
(
    SELECT UserID,
           FK_MGroupMenu_ID,
           FK_MRole_ID
    FROM MUser
    WHERE Active = 1
    UNION ALL
    SELECT UserID,
           FK_MGroupMenu_ID,
           FK_MRole_ID
    FROM NDSAdditionalSettingUser
    WHERE Active = 1
) X
    INNER JOIN MGroupMenu
        ON X.FK_MGroupMenu_ID = PK_MGroupMenu_ID
    INNER JOIN MRole
        ON X.FK_MRole_ID = PK_MRole_ID;
GO
