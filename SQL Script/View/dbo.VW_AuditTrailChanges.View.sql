/****** Object:  View [dbo].[VW_AuditTrailChanges]    Script Date: 12/12/2023 11:22:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VW_AuditTrailChanges]
AS
SELECT PK_AuditTrail_ID, AuditTrailHeader.CreatedDate, AuditTrailHeader.CreatedBy, AuditTrailHeader.ApproveBy,
	   ModuleLabel, ModuleActionName, AuditTrailStatusName
FROM AuditTrailHeader
	 JOIN ModuleAction
	 ON FK_ModuleAction_ID = PK_ModuleAction_ID
	 JOIN AuditTrailStatus
	 ON FK_AuditTrailStatus_ID = Pk_AuditTrailStatus_ID
GO
