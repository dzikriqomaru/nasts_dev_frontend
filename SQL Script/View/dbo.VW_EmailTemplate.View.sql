/****** Object:  View [dbo].[VW_EmailTemplate]    Script Date: 12/12/2023 11:22:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[VW_EmailTemplate]  
AS  
SELECT	PK_EmailTemplate_ID,  
		EmailTemplateName,  
		MonitoringDurationName,  
		StartDate,  
		StartTime,  
		ExcludeHoliday,  
		EmailTo,  
		EmailCC,  
		EmailBCC,  
		EmailSubject,  
		EmailBody,
		ImportanceDescription,
		SensitivityDescription,
		DeliveryReceipt,
		ReadReceipt, 
		a.Active  
FROM EmailTemplate a  
INNER JOIN MonitoringDuration b  
	ON a.FK_Monitoringduration_ID = b.PK_MonitoringDuration_Id
INNER JOIN EmailTemplateImportance c
	ON a.FK_EmailTemplateImportance_ID = c.PK_EmailTemplateImportance_ID
INNER JOIN EmailTemplateSensitivity d
	ON a.FK_EmailTemplateSensitivity_ID = d.PK_EmailTemplateSensitivity_ID
GO
