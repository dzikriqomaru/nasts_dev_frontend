/****** Object:  View [dbo].[Vw_SettingPersonal]    Script Date: 12/12/2023 11:22:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_SettingPersonal]
AS

SELECT        PK_SettingPersonal_ID, UserID, ReportDate, KdCabang = KodeCabang
FROM            SettingPersonal
GO
