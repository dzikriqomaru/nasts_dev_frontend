/****** Object:  View [dbo].[Vw_ValidationReportSummary]    Script Date: 12/12/2023 11:22:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

  
CREATE View [dbo].[Vw_ValidationReportSummary]  
as  
  
Select SegmentData, NamaField, ValidationMessage, Count(1) as Jumlah  
from ValidationReport  with (nolock)
Group by SegmentData, NamaField, ValidationMessage  
  
GO
