/****** Object:  View [dbo].[VW_ChartSetting]    Script Date: 12/12/2023 11:22:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[VW_ChartSetting]  
AS  
SELECT PK_Chart_ID,  
       Title,  
       TypeName 
FROM ChartSetting a  
INNER JOIN ChartType b  
 ON a.FK_Type_ID = b.PK_Type_ID   
GO
