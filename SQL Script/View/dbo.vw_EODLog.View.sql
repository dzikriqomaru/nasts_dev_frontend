/****** Object:  View [dbo].[vw_EODLog]    Script Date: 12/12/2023 11:22:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

	
CREATE VIEW [dbo].[vw_EODLog]          
AS        
          
/*SELECT edl.PK_EODTaskDetailLog_ID as PK_EODSchedulerLog_ID,el2.PK_EODTaskLog_ID,el.PK_EODSchedulerLog_ID as PK_EODTaskDetailLog_ID,          
e.EODSchedulerName,e2.EODTaskName,          
edt.
EODTaskDetailType,CASE WHEN edt.PK_EODTaskDetailType_ID= 2 THEN ed.StoreProcedureName ELSE ed.SSISName END AS NAME          
,edl.ExecuteBy,el.DataDate,el.ProcessDate,edl.StartDate,edl.Enddate, el.ErrorMessage + ' '+ edl.ErrorMessage AS ErrorMessage,me.Ms
EODStatusName          
,edl.ExecuteBy,el.DataDate,el.ProcessDate,edl.StartDate,edl.Enddate,  edl.ErrorMessage AS ErrorMessage,me.MsEODStatusName          
          
FROM EODSchedulerLog el          
INNER JOIN EODTaskLog el2 ON el.PK_EODSchedulerLog_ID=
el2.EODSchedulerLogID          
INNER JOIN EODTaskDetailLog edl ON edl.EODTaskLogID=el2.PK_EODTaskLog_ID          
INNER JOIN EODTaskDetail ed ON ed.PK_EODTaskDetail_ID=edl.FK_EODTAskDetail_ID           
INNER JOIN EODTaskDetailType edt ON edt.PK_EODTaskD
etailType_ID=ed.FK_EODTaskDetailType_ID          
INNER JOIN EODScheduler e ON e.PK_EODScheduler_ID=el.FK_EODSchedulerID          
INNER JOIN EODTask e2 ON e2.PK_EODTask_ID=el2.FK_EODTaskID          
INNER JOIN MsEODStatus me ON me.PK_MsEODStatus_ID=edl.F
K_MsEODStatus_ID*/   
    
    
SELECT el.PK_EODSchedulerLog_ID,    
       el2.PK_EODTaskLog_ID,    
       edl.PK_EODTaskDetailLog_ID,    
       e.EODSchedulerName,    
       e2.EODTaskName,    
       edt.EODTaskDetailType,    
       CASE    
      
     WHEN edt.PK_EODTaskDetailType_ID = 2 THEN    
               ed.StoreProcedureName    
           ELSE    
               ed.SSISName    
       END AS NAME,    
       edl.ExecuteBy,    
       el.DataDate,    
       el.ProcessDate,    
       edl.
StartDate,    
       edl.Enddate,    
	   CASE xx.PK_MsEODStatus_ID 
			WHEN 1 THEN '' -- On Queue
			WHEN 2 THEN '' -- In Progress
			WHEN 3 THEN '' -- Process Success
			ELSE CASE me.PK_MsEODStatus_ID 
				WHEN 3 THEN '' -- Task Success
				WHEN 5 THEN
 el.ErrorMessage -- Task Cancelled
				WHEN 6 THEN el.ErrorMessage -- Task Cancelled
				ELSE el.ErrorMessage + ': ' + edl.ErrorMessage 
			END
		END AS ErrorMessage,
       --el.ErrorMessage + ' ' + edl.ErrorMessage AS ErrorMessage,    
       me.MsEODStatusName AS [TaskStatus],    
    el.FK_MsEODStatus_ID,    
    xx.MsEODStatusName AS [ProcessStatus],    
    el.FK_EODSchedulerID    
FROM EODSchedulerLog el    
    INNER JOIN EODTaskLog el2    
        ON el.PK_EODSchedulerLog_ID = el2.EODSchedulerLogID    
    INNER JOIN EODTaskDetailLog edl    
        ON edl.EODTaskLogID = el2.PK_EODTaskLog_ID    
    INNER JOIN EODTaskDetail ed    
        ON ed.PK_EODTaskDetail_ID = edl.FK_EODTAskDetail_ID    
    INNER JOIN EODTaskDetailType edt    
        ON edt.PK_EODTaskDetailType_ID = ed.FK_EODTaskDetailType_ID    
    INNER JOIN EODScheduler e    
        ON e.PK_EODScheduler_ID = el.FK_EODSchedulerID    
    INNER JOIN EODTask e2    
        ON e2.PK_EODTask_ID = el2.FK_EODTaskID    
    INNER JOIN MsEODStatus me    
      
  ON me.PK_MsEODStatus_ID = edl.FK_MsEODStatus_ID    
    INNER JOIN MsEODStatus xx    
        ON xx.PK_MsEODStatus_ID = el.FK_MsEODStatus_ID
GO
