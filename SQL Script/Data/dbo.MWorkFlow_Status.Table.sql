SET IDENTITY_INSERT [dbo].[MWorkFlow_Status] ON 

INSERT [dbo].[MWorkFlow_Status] ([PK_MWorkFlow_Status_ID], [WorkFlowStatusName], [Active], [CreatedBy], [LastUpdateBy], [ApprovedBy], [CreatedDate], [LastUpdateDate], [ApprovedDate], [Alternateby]) VALUES (1, N'Draft', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MWorkFlow_Status] ([PK_MWorkFlow_Status_ID], [WorkFlowStatusName], [Active], [CreatedBy], [LastUpdateBy], [ApprovedBy], [CreatedDate], [LastUpdateDate], [ApprovedDate], [Alternateby]) VALUES (2, N'Submit', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MWorkFlow_Status] ([PK_MWorkFlow_Status_ID], [WorkFlowStatusName], [Active], [CreatedBy], [LastUpdateBy], [ApprovedBy], [CreatedDate], [LastUpdateDate], [ApprovedDate], [Alternateby]) VALUES (3, N'InProgress', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MWorkFlow_Status] ([PK_MWorkFlow_Status_ID], [WorkFlowStatusName], [Active], [CreatedBy], [LastUpdateBy], [ApprovedBy], [CreatedDate], [LastUpdateDate], [ApprovedDate], [Alternateby]) VALUES (4, N'Approved', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MWorkFlow_Status] ([PK_MWorkFlow_Status_ID], [WorkFlowStatusName], [Active], [CreatedBy], [LastUpdateBy], [ApprovedBy], [CreatedDate], [LastUpdateDate], [ApprovedDate], [Alternateby]) VALUES (5, N'Rejected', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MWorkFlow_Status] ([PK_MWorkFlow_Status_ID], [WorkFlowStatusName], [Active], [CreatedBy], [LastUpdateBy], [ApprovedBy], [CreatedDate], [LastUpdateDate], [ApprovedDate], [Alternateby]) VALUES (6, N'Revise', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[MWorkFlow_Status] OFF
GO
