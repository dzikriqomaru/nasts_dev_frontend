INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (0, 'False', 'Fallback Login with SHA1');
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (1, 'True', 'Fallback Login with SHA1');
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (0, N'Aria', N'Application Theme')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (1, N'Crisp', N'Application Theme')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (1, N'LDAP', N'Aplication Authentication')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (1, N'Period', N'Data Purging Method')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (2, N'Record Count', N'Data Purging Method')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (2, N'Form Authentication', N'Aplication Authentication')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (2, N'CrispTouch', N'Application Theme')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (3, N'Default', N'Application Theme')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (4, N'Gray', N'Application Theme')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (5, N'Neptune', N'Application Theme')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (6, N'NeptuneTouch', N'Application Theme')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (7, N'None', N'Application Theme')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (1, N'Right', N'Button Position')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (2, N'Left', N'Button Position')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (1, N'True', N'Allow Multiple Login')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (0, N'False', N'Allow Multiple Login')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (0, N'False', N'Enable Notification')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (1, N'True', N'Enable Notification')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (0, N'False', N'Validate Can not save if no changes')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (1, N'True', N'Validate Can not save if no changes')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (1, N'True', N'Enable OTP')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (0, N'False', N'Enable OTP')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (1, N'True', N'Password Combination Lower And Upper Case')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (0, N'False', N'Password Combination Lower And Upper Case')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (0, N'False', N'SAML Get Certificate Is Directory')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (1, N'True', N'SAML Get Certificate Is Directory')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (0, N'False', N'SAML IsEnabled Logout')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (1, N'True', N'SAML IsEnabled Logout')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (1, N'True', N'Show IsDraft In Parameter View')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (0, N'False', N'Show IsDraft In Parameter View')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (1, N'id', N'Language')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (2, N'en', N'Language')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (0, N'False', N'EnableSignUp')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (1, N'True', N'EnableSignUp')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (0, N'False', N'EnableForgotPassword')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (1, N'True', N'EnableForgotPassword')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (0, N'False', N'Enable Facebook Login Mobile')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (0, N'False', N'Enable Google Login Mobile')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (0, N'False', N'EnableFacebookLogin')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (0, N'False', N'EnableGoogleLogin')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (0, N'False', N'Enable Captcha Mobile')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (0, N'False', N'Enable Captcha Web')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (0, N'False', N'Show CreatedBy In Parameter View')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (0, N'False', N'Show LastUpdateBy In Parameter View')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (0, N'False', N'Show ApprovedBy In Parameter View')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (0, N'False', N'Show CreatedDate In Parameter View')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (0, N'False', N'Show LastUpdateDate In Parameter View')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (0, N'False', N'Show ApprovedDate In Parameter View')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (0, N'False', N'Show User Profile Setting')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (0, N'False', N'Show Password Setting')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (0, N'False', N'Show Role and Group Setting')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (1, N'True', N'Enable Facebook Login Mobile')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (1, N'True', N'Enable Google Login Mobile')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (1, N'True', N'EnableFacebookLogin')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (1, N'True', N'EnableGoogleLogin')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (1, N'True', N'Enable Captcha Mobile')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (1, N'True', N'Enable Captcha Web')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (1, N'True', N'Show CreatedBy In Parameter View')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (1, N'True', N'Show LastUpdateBy In Parameter View')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (1, N'True', N'Show ApprovedBy In Parameter View')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (1, N'True', N'Show CreatedDate In Parameter View')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (1, N'True', N'Show LastUpdateDate In Parameter View')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (1, N'True', N'Show ApprovedDate In Parameter View')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (1, N'True', N'Show User Profile Setting')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (1, N'True', N'Show Password Setting')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (1, N'True', N'Show Role and Group Setting')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (1, N'True', N'Show Active In Parameter View')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (0, N'False', N'Show Active In Parameter View')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (3, N'SSO', N'Aplication Authentication')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (4, N'Scorpion', N'Aplication Authentication')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (1, N'True', N'Hide Module Key')
INSERT [dbo].[SystemParameterPickList] ([Pk_ApplicationAuthentication_ID], [ApplicationAuthentication], [SettingName]) VALUES (2, N'False', N'Hide Module Key')
GO
