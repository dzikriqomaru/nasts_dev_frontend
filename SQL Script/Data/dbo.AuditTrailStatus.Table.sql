SET IDENTITY_INSERT [dbo].[AuditTrailStatus] ON 

INSERT [dbo].[AuditTrailStatus] ([Pk_AuditTrailStatus_ID], [AuditTrailStatusName]) VALUES (1, N'Affected To Database')
INSERT [dbo].[AuditTrailStatus] ([Pk_AuditTrailStatus_ID], [AuditTrailStatusName]) VALUES (2, N'Rejected')
INSERT [dbo].[AuditTrailStatus] ([Pk_AuditTrailStatus_ID], [AuditTrailStatusName]) VALUES (3, N'Waiting To Approval')
SET IDENTITY_INSERT [dbo].[AuditTrailStatus] OFF
GO
