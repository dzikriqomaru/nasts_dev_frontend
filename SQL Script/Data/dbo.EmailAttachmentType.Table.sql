SET IDENTITY_INSERT [dbo].[EmailAttachmentType] ON 

INSERT [dbo].[EmailAttachmentType] ([PK_EmailAttachmentType_ID], [EmailAttachmentType]) VALUES (1, N'File')
INSERT [dbo].[EmailAttachmentType] ([PK_EmailAttachmentType_ID], [EmailAttachmentType]) VALUES (2, N'Reporting Service')
INSERT [dbo].[EmailAttachmentType] ([PK_EmailAttachmentType_ID], [EmailAttachmentType]) VALUES (3, N'Cloud File')
INSERT [dbo].[EmailAttachmentType] ([PK_EmailAttachmentType_ID], [EmailAttachmentType]) VALUES (4, N'Report Designer')
SET IDENTITY_INSERT [dbo].[EmailAttachmentType] OFF
GO
