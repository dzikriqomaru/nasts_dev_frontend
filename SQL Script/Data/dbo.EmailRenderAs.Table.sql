INSERT [dbo].[EmailRenderAs] ([PK_EmailRenderAs_ID], [EmailRenderAsName], [FileExtension], [MimeType]) VALUES (1, N'PDF', N'.pdf', N'application/pdf')
INSERT [dbo].[EmailRenderAs] ([PK_EmailRenderAs_ID], [EmailRenderAsName], [FileExtension], [MimeType]) VALUES (2, N'EXCEL', N'.xls', N'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
INSERT [dbo].[EmailRenderAs] ([PK_EmailRenderAs_ID], [EmailRenderAsName], [FileExtension], [MimeType]) VALUES (3, N'WORD', N'.docx', N'application/vnd.openxmlformats-officedocument.wordprocessingml.document')
INSERT [dbo].[EmailRenderAs] ([PK_EmailRenderAs_ID], [EmailRenderAsName], [FileExtension], [MimeType]) VALUES (4, N'CSV', N'.csv', N'text/csv')
INSERT [dbo].[EmailRenderAs] ([PK_EmailRenderAs_ID], [EmailRenderAsName], [FileExtension], [MimeType]) VALUES (5, N'XML', N'.xml', N'text/xml')
INSERT [dbo].[EmailRenderAs] ([PK_EmailRenderAs_ID], [EmailRenderAsName], [FileExtension], [MimeType]) VALUES (6, N'IMAGE', N'.tiff', N'image/tiff')
GO
