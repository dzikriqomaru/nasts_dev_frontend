INSERT [dbo].[MsEODPeriod] ([PK_MsEODPeriod_Id], [MsEODPeriodName]) VALUES (1, N'Days(s)')
INSERT [dbo].[MsEODPeriod] ([PK_MsEODPeriod_Id], [MsEODPeriodName]) VALUES (2, N'Week(s)')
INSERT [dbo].[MsEODPeriod] ([PK_MsEODPeriod_Id], [MsEODPeriodName]) VALUES (3, N'Month(s)')
INSERT [dbo].[MsEODPeriod] ([PK_MsEODPeriod_Id], [MsEODPeriodName]) VALUES (4, N'Year(s)')
INSERT [dbo].[MsEODPeriod] ([PK_MsEODPeriod_Id], [MsEODPeriodName]) VALUES (5, N'Hour(s)')
INSERT [dbo].[MsEODPeriod] ([PK_MsEODPeriod_Id], [MsEODPeriodName]) VALUES (6, N'Minute(s)')
GO
