SET IDENTITY_INSERT [dbo].[SystemDictionaryDetail] ON 

INSERT [dbo].[SystemDictionaryDetail] ([PK_ID], [Keywords], [ParamName]) VALUES (1, N'module-add', N'name = "Module"')
INSERT [dbo].[SystemDictionaryDetail] ([PK_ID], [Keywords], [ParamName]) VALUES (2, N'module-edit', N'name = "Module"')
INSERT [dbo].[SystemDictionaryDetail] ([PK_ID], [Keywords], [ParamName]) VALUES (3, N'module-delete', N'name = "Module"')
INSERT [dbo].[SystemDictionaryDetail] ([PK_ID], [Keywords], [ParamName]) VALUES (4, N'module-activation', N'name = "Module"')
INSERT [dbo].[SystemDictionaryDetail] ([PK_ID], [Keywords], [ParamName]) VALUES (5, N'module-detail', N'name = "Module"')
INSERT [dbo].[SystemDictionaryDetail] ([PK_ID], [Keywords], [ParamName]) VALUES (6, N'module-approval', N'name = "Module"')
INSERT [dbo].[SystemDictionaryDetail] ([PK_ID], [Keywords], [ParamName]) VALUES (7, N'module-approval-detail', N'name = "Module"')
INSERT [dbo].[SystemDictionaryDetail] ([PK_ID], [Keywords], [ParamName]) VALUES (9, N'name-adjective', N'name = "default"')
INSERT [dbo].[SystemDictionaryDetail] ([PK_ID], [Keywords], [ParamName]) VALUES (10, N'module-upload', N'name="Module"')
INSERT [dbo].[SystemDictionaryDetail] ([PK_ID], [Keywords], [ParamName]) VALUES (11, N'infopanel-success-module', N'name = "Module"')
INSERT [dbo].[SystemDictionaryDetail] ([PK_ID], [Keywords], [ParamName]) VALUES (12, N'menu-access-title', N'name="User"')
INSERT [dbo].[SystemDictionaryDetail] ([PK_ID], [Keywords], [ParamName]) VALUES (13, N'search-value', N'value="Module"')
INSERT [dbo].[SystemDictionaryDetail] ([PK_ID], [Keywords], [ParamName]) VALUES (14, N'menu-access-title', N'name="User"')
INSERT [dbo].[SystemDictionaryDetail] ([PK_ID], [Keywords], [ParamName]) VALUES (15, N'search-value', N'value="Module"')
INSERT [dbo].[SystemDictionaryDetail] ([PK_ID], [Keywords], [ParamName]) VALUES (16, N'activate-deactivate-field-access', N'name="Module"')
INSERT [dbo].[SystemDictionaryDetail] ([PK_ID], [Keywords], [ParamName]) VALUES (17, N'for-name', N'name="Module"')
INSERT [dbo].[SystemDictionaryDetail] ([PK_ID], [Keywords], [ParamName]) VALUES (18, N'edit-name', N'name="Module"')
INSERT [dbo].[SystemDictionaryDetail] ([PK_ID], [Keywords], [ParamName]) VALUES (19, N'module-view', N'name = "Module"')
INSERT [dbo].[SystemDictionaryDetail] ([PK_ID], [Keywords], [ParamName]) VALUES (22, N'otp-email', N'name = "Module"')
INSERT [dbo].[SystemDictionaryDetail] ([PK_ID], [Keywords], [ParamName]) VALUES (25, N'otp-resend-cooldown', N'name = "Module"')
SET IDENTITY_INSERT [dbo].[SystemDictionaryDetail] OFF
GO
