SET IDENTITY_INSERT [dbo].[ModuleFieldRegex] ON 

INSERT [dbo].[ModuleFieldRegex] ([PK_ModuleFieldRegex], [FK_ModuleField_ID], [Regex]) VALUES (101347, 585805, N'^[0-9]+$')
INSERT [dbo].[ModuleFieldRegex] ([PK_ModuleFieldRegex], [FK_ModuleField_ID], [Regex]) VALUES (101348, 585809, N'^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$')
INSERT [dbo].[ModuleFieldRegex] ([PK_ModuleFieldRegex], [FK_ModuleField_ID], [Regex]) VALUES (101349, 0, N'/^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/')
INSERT [dbo].[ModuleFieldRegex] ([PK_ModuleFieldRegex], [FK_ModuleField_ID], [Regex]) VALUES (101412, 596435, N'^\d$')
INSERT [dbo].[ModuleFieldRegex] ([PK_ModuleFieldRegex], [FK_ModuleField_ID], [Regex]) VALUES (101413, 596435, N'0[1-9]|1[0-2]')
INSERT [dbo].[ModuleFieldRegex] ([PK_ModuleFieldRegex], [FK_ModuleField_ID], [Regex]) VALUES (101414, 596436, N'^\d+$')
INSERT [dbo].[ModuleFieldRegex] ([PK_ModuleFieldRegex], [FK_ModuleField_ID], [Regex]) VALUES (101415, 0, N'^\d$')
INSERT [dbo].[ModuleFieldRegex] ([PK_ModuleFieldRegex], [FK_ModuleField_ID], [Regex]) VALUES (101416, 0, N'^\d$')
INSERT [dbo].[ModuleFieldRegex] ([PK_ModuleFieldRegex], [FK_ModuleField_ID], [Regex]) VALUES (111404, 0, N'^[0-9]*$')
INSERT [dbo].[ModuleFieldRegex] ([PK_ModuleFieldRegex], [FK_ModuleField_ID], [Regex]) VALUES (111405, 0, N'^[0-9]*$')
INSERT [dbo].[ModuleFieldRegex] ([PK_ModuleFieldRegex], [FK_ModuleField_ID], [Regex]) VALUES (111409, 606424, N'^[0-9]*$')
INSERT [dbo].[ModuleFieldRegex] ([PK_ModuleFieldRegex], [FK_ModuleField_ID], [Regex]) VALUES (111410, 606424, N'^\d$')
INSERT [dbo].[ModuleFieldRegex] ([PK_ModuleFieldRegex], [FK_ModuleField_ID], [Regex]) VALUES (111411, 0, N'^[0-9]*$')
INSERT [dbo].[ModuleFieldRegex] ([PK_ModuleFieldRegex], [FK_ModuleField_ID], [Regex]) VALUES (111412, 0, N'^[0-9]*$')
INSERT [dbo].[ModuleFieldRegex] ([PK_ModuleFieldRegex], [FK_ModuleField_ID], [Regex]) VALUES (131623, 663100, N'^[0-9]+$')
INSERT [dbo].[ModuleFieldRegex] ([PK_ModuleFieldRegex], [FK_ModuleField_ID], [Regex]) VALUES (342135, 1249697, N'sss')
INSERT [dbo].[ModuleFieldRegex] ([PK_ModuleFieldRegex], [FK_ModuleField_ID], [Regex]) VALUES (342136, 1249698, N'sdd')
INSERT [dbo].[ModuleFieldRegex] ([PK_ModuleFieldRegex], [FK_ModuleField_ID], [Regex]) VALUES (342137, 1249698, N'sdf')
INSERT [dbo].[ModuleFieldRegex] ([PK_ModuleFieldRegex], [FK_ModuleField_ID], [Regex]) VALUES (342138, 1249699, N'ssda')
INSERT [dbo].[ModuleFieldRegex] ([PK_ModuleFieldRegex], [FK_ModuleField_ID], [Regex]) VALUES (342139, 1249700, N'sss')
INSERT [dbo].[ModuleFieldRegex] ([PK_ModuleFieldRegex], [FK_ModuleField_ID], [Regex]) VALUES (342140, 1249701, N'sdsdssd')
INSERT [dbo].[ModuleFieldRegex] ([PK_ModuleFieldRegex], [FK_ModuleField_ID], [Regex]) VALUES (342141, 1249702, N'ssss')
INSERT [dbo].[ModuleFieldRegex] ([PK_ModuleFieldRegex], [FK_ModuleField_ID], [Regex]) VALUES (342142, 1249703, N'sdd')
INSERT [dbo].[ModuleFieldRegex] ([PK_ModuleFieldRegex], [FK_ModuleField_ID], [Regex]) VALUES (342143, 1249704, N'//dffdsf')
INSERT [dbo].[ModuleFieldRegex] ([PK_ModuleFieldRegex], [FK_ModuleField_ID], [Regex]) VALUES (342144, 1249705, N'bfbfbfb')
INSERT [dbo].[ModuleFieldRegex] ([PK_ModuleFieldRegex], [FK_ModuleField_ID], [Regex]) VALUES (342145, 1249705, N'bfbfbf')
INSERT [dbo].[ModuleFieldRegex] ([PK_ModuleFieldRegex], [FK_ModuleField_ID], [Regex]) VALUES (342146, 1249706, N'sddsa')
INSERT [dbo].[ModuleFieldRegex] ([PK_ModuleFieldRegex], [FK_ModuleField_ID], [Regex]) VALUES (342147, 1249707, N'sdd')
INSERT [dbo].[ModuleFieldRegex] ([PK_ModuleFieldRegex], [FK_ModuleField_ID], [Regex]) VALUES (342148, 1249708, N'//dffdsf')
INSERT [dbo].[ModuleFieldRegex] ([PK_ModuleFieldRegex], [FK_ModuleField_ID], [Regex]) VALUES (342149, 1249709, N'ssss')
INSERT [dbo].[ModuleFieldRegex] ([PK_ModuleFieldRegex], [FK_ModuleField_ID], [Regex]) VALUES (342164, 1313747, N'*&^%$#@()')
INSERT [dbo].[ModuleFieldRegex] ([PK_ModuleFieldRegex], [FK_ModuleField_ID], [Regex]) VALUES (342184, 1459725, N'[a-z]')
SET IDENTITY_INSERT [dbo].[ModuleFieldRegex] OFF
GO
