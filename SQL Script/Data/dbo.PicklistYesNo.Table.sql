SET IDENTITY_INSERT [dbo].[PicklistYesNo] ON 

INSERT [dbo].[PicklistYesNo] ([PK_PicklistYesNo_ID], [PicklistValue], [PicklistDescription], [Active], [CreatedBy], [LastUpdateBy], [ApprovedBy], [CreatedDate], [LastUpdateDate], [ApprovedDate], [Alternateby]) VALUES (0, N'No', N'No', 1, N'system', NULL, NULL, CAST(N'2018-01-20T00:00:00.000' AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[PicklistYesNo] ([PK_PicklistYesNo_ID], [PicklistValue], [PicklistDescription], [Active], [CreatedBy], [LastUpdateBy], [ApprovedBy], [CreatedDate], [LastUpdateDate], [ApprovedDate], [Alternateby]) VALUES (1, N'Yes', N'Yes', 1, N'system', NULL, NULL, CAST(N'2018-01-20T00:00:00.000' AS DateTime), NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[PicklistYesNo] OFF
GO
