SET IDENTITY_INSERT [dbo].[MBreach_SLA_Action] ON 

INSERT [dbo].[MBreach_SLA_Action] ([PK_BreachSLAAction], [Breach_SLA_Action], [Active], [CreatedBy], [LastUpdateBy], [ApprovedBy], [CreatedDate], [LastUpdateDate], [ApprovedDate], [Alternateby]) VALUES (1, N'Auto Reversed', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MBreach_SLA_Action] ([PK_BreachSLAAction], [Breach_SLA_Action], [Active], [CreatedBy], [LastUpdateBy], [ApprovedBy], [CreatedDate], [LastUpdateDate], [ApprovedDate], [Alternateby]) VALUES (2, N'Auto Approved', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MBreach_SLA_Action] ([PK_BreachSLAAction], [Breach_SLA_Action], [Active], [CreatedBy], [LastUpdateBy], [ApprovedBy], [CreatedDate], [LastUpdateDate], [ApprovedDate], [Alternateby]) VALUES (3, N'Auto Rejected', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[MBreach_SLA_Action] OFF
GO
