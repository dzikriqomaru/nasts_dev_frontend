SET IDENTITY_INSERT [dbo].[ChartType] ON 

INSERT [dbo].[ChartType] ([PK_Type_ID], [TypeName], [Active], [CreatedBy], [LastUpdateBy], [ApprovedBy], [CreatedDate], [LastUpdateDate], [ApprovedDate], [Alternateby]) VALUES (1, N'Line', NULL, NULL, N'sysadmin', NULL, NULL, CAST(N'2022-10-20T15:11:48.863' AS DateTime), NULL, N'')
INSERT [dbo].[ChartType] ([PK_Type_ID], [TypeName], [Active], [CreatedBy], [LastUpdateBy], [ApprovedBy], [CreatedDate], [LastUpdateDate], [ApprovedDate], [Alternateby]) VALUES (2, N'Bar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ChartType] ([PK_Type_ID], [TypeName], [Active], [CreatedBy], [LastUpdateBy], [ApprovedBy], [CreatedDate], [LastUpdateDate], [ApprovedDate], [Alternateby]) VALUES (3, N'Pie', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ChartType] ([PK_Type_ID], [TypeName], [Active], [CreatedBy], [LastUpdateBy], [ApprovedBy], [CreatedDate], [LastUpdateDate], [ApprovedDate], [Alternateby]) VALUES (4, N'Radar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ChartType] ([PK_Type_ID], [TypeName], [Active], [CreatedBy], [LastUpdateBy], [ApprovedBy], [CreatedDate], [LastUpdateDate], [ApprovedDate], [Alternateby]) VALUES (5, N'Bubble', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ChartType] ([PK_Type_ID], [TypeName], [Active], [CreatedBy], [LastUpdateBy], [ApprovedBy], [CreatedDate], [LastUpdateDate], [ApprovedDate], [Alternateby]) VALUES (6, N'Polar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ChartType] ([PK_Type_ID], [TypeName], [Active], [CreatedBy], [LastUpdateBy], [ApprovedBy], [CreatedDate], [LastUpdateDate], [ApprovedDate], [Alternateby]) VALUES (7, N'Table', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ChartType] ([PK_Type_ID], [TypeName], [Active], [CreatedBy], [LastUpdateBy], [ApprovedBy], [CreatedDate], [LastUpdateDate], [ApprovedDate], [Alternateby]) VALUES (9, N'Shortcut Horizontal', 1, N'sysadmin', N'sysadmin', NULL, CAST(N'2022-07-04T10:28:45.790' AS DateTime), CAST(N'2022-07-04T10:28:45.790' AS DateTime), NULL, NULL)
INSERT [dbo].[ChartType] ([PK_Type_ID], [TypeName], [Active], [CreatedBy], [LastUpdateBy], [ApprovedBy], [CreatedDate], [LastUpdateDate], [ApprovedDate], [Alternateby]) VALUES (10, N'Shortcut Vertical', 1, N'sysadmin', N'sysadmin', NULL, CAST(N'2022-07-04T10:28:54.863' AS DateTime), CAST(N'2022-07-04T10:28:54.863' AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[ChartType] OFF
GO
