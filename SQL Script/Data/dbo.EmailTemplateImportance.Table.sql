INSERT [dbo].[EmailTemplateImportance] ([PK_EmailTemplateImportance_ID], [ImportanceDescription]) VALUES (1, N'Low')
INSERT [dbo].[EmailTemplateImportance] ([PK_EmailTemplateImportance_ID], [ImportanceDescription]) VALUES (2, N'Normal')
INSERT [dbo].[EmailTemplateImportance] ([PK_EmailTemplateImportance_ID], [ImportanceDescription]) VALUES (3, N'High')
GO
