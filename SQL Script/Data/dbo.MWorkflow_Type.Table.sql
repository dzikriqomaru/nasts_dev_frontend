SET IDENTITY_INSERT [dbo].[MWorkflow_Type] ON 

INSERT [dbo].[MWorkflow_Type] ([PK_WorkflowType], [WorkflowType], [Active], [CreatedBy], [LastUpdateBy], [ApprovedBy], [CreatedDate], [LastUpdateDate], [ApprovedDate], [Alternateby]) VALUES (1, N'Data Workflow', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MWorkflow_Type] ([PK_WorkflowType], [WorkflowType], [Active], [CreatedBy], [LastUpdateBy], [ApprovedBy], [CreatedDate], [LastUpdateDate], [ApprovedDate], [Alternateby]) VALUES (2, N'Document Workflow', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[MWorkflow_Type] OFF
GO
