SET IDENTITY_INSERT [dbo].[MWorkFlowUserType] ON 

INSERT [dbo].[MWorkFlowUserType] ([PK_WFUserType], [UserType], [Active], [CreatedBy], [LastUpdateBy], [ApprovedBy], [CreatedDate], [LastUpdateDate], [ApprovedDate], [Alternateby]) VALUES (1, N'UserID', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MWorkFlowUserType] ([PK_WFUserType], [UserType], [Active], [CreatedBy], [LastUpdateBy], [ApprovedBy], [CreatedDate], [LastUpdateDate], [ApprovedDate], [Alternateby]) VALUES (2, N'Previous User Upline', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MWorkFlowUserType] ([PK_WFUserType], [UserType], [Active], [CreatedBy], [LastUpdateBy], [ApprovedBy], [CreatedDate], [LastUpdateDate], [ApprovedDate], [Alternateby]) VALUES (4, N'Role User', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[MWorkFlowUserType] OFF
GO
