SET IDENTITY_INSERT [dbo].[MExtType] ON 

INSERT [dbo].[MExtType] ([PK_ExtType_ID], [ExtTypeName], [ExtTypeDesc]) VALUES (1, N'DateField', N'')
INSERT [dbo].[MExtType] ([PK_ExtType_ID], [ExtTypeName], [ExtTypeDesc]) VALUES (2, N'DropDownField', N'')
INSERT [dbo].[MExtType] ([PK_ExtType_ID], [ExtTypeName], [ExtTypeDesc]) VALUES (4, N'NumberField', N'')
INSERT [dbo].[MExtType] ([PK_ExtType_ID], [ExtTypeName], [ExtTypeDesc]) VALUES (5, N'TextField', N'')
INSERT [dbo].[MExtType] ([PK_ExtType_ID], [ExtTypeName], [ExtTypeDesc]) VALUES (6, N'Radio', N'')
INSERT [dbo].[MExtType] ([PK_ExtType_ID], [ExtTypeName], [ExtTypeDesc]) VALUES (7, N'DisplayField', N'')
INSERT [dbo].[MExtType] ([PK_ExtType_ID], [ExtTypeName], [ExtTypeDesc]) VALUES (8, N'File Upload', N'')
INSERT [dbo].[MExtType] ([PK_ExtType_ID], [ExtTypeName], [ExtTypeDesc]) VALUES (9, N'PasswordField', N'')
INSERT [dbo].[MExtType] ([PK_ExtType_ID], [ExtTypeName], [ExtTypeDesc]) VALUES (12, N'TimeField', N'')
INSERT [dbo].[MExtType] ([PK_ExtType_ID], [ExtTypeName], [ExtTypeDesc]) VALUES (14, N'PopUp Combobox', N'')
INSERT [dbo].[MExtType] ([PK_ExtType_ID], [ExtTypeName], [ExtTypeDesc]) VALUES (15, N'PopUp MultiCheckBox', N'')
INSERT [dbo].[MExtType] ([PK_ExtType_ID], [ExtTypeName], [ExtTypeDesc]) VALUES (16, N'RichText', N'')
INSERT [dbo].[MExtType] ([PK_ExtType_ID], [ExtTypeName], [ExtTypeDesc]) VALUES (17, N'QueryField', N'')
INSERT [dbo].[MExtType] ([PK_ExtType_ID], [ExtTypeName], [ExtTypeDesc]) VALUES (18, N'Location', N'')
INSERT [dbo].[MExtType] ([PK_ExtType_ID], [ExtTypeName], [ExtTypeDesc]) VALUES (19, N'DescField', N'')
INSERT [dbo].[MExtType] ([PK_ExtType_ID], [ExtTypeName], [ExtTypeDesc]) VALUES (20, N'ToggleField', N'')
INSERT [dbo].[MExtType] ([PK_ExtType_ID], [ExtTypeName], [ExtTypeDesc]) VALUES (21, N'CheckboxField', N'')
SET IDENTITY_INSERT [dbo].[MExtType] OFF
GO
