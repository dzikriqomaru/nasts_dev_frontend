INSERT [dbo].[MsEODStatus] ([PK_MsEODStatus_ID], [MsEODStatusName]) VALUES (1, N'OnQueue')
INSERT [dbo].[MsEODStatus] ([PK_MsEODStatus_ID], [MsEODStatusName]) VALUES (2, N'In Progress')
INSERT [dbo].[MsEODStatus] ([PK_MsEODStatus_ID], [MsEODStatusName]) VALUES (3, N'Success')
INSERT [dbo].[MsEODStatus] ([PK_MsEODStatus_ID], [MsEODStatusName]) VALUES (4, N'Error')
INSERT [dbo].[MsEODStatus] ([PK_MsEODStatus_ID], [MsEODStatusName]) VALUES (5, N'Cancelled')
INSERT [dbo].[MsEODStatus] ([PK_MsEODStatus_ID], [MsEODStatusName]) VALUES (6, N'Cancelled')
GO
