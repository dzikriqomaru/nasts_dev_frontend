SET IDENTITY_INSERT [dbo].[connectionType] ON 

INSERT [dbo].[connectionType] ([pk_connType_id], [connType_name], [Active], [CreatedBy], [LastUpdateBy], [ApprovedBy], [CreatedDate], [LastUpdateDate], [ApprovedDate], [Alternateby]) VALUES (1, N'SQL', 1, N'sysadmin', N'sysadmin', NULL, CAST(N'2022-08-04T10:36:52.120' AS DateTime), CAST(N'2022-08-04T10:36:52.120' AS DateTime), NULL, NULL)
INSERT [dbo].[connectionType] ([pk_connType_id], [connType_name], [Active], [CreatedBy], [LastUpdateBy], [ApprovedBy], [CreatedDate], [LastUpdateDate], [ApprovedDate], [Alternateby]) VALUES (2, N'PostgreSQL', 1, N'sysadmin', N'sysadmin', NULL, CAST(N'2022-08-04T10:37:08.967' AS DateTime), CAST(N'2022-08-04T10:37:08.967' AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[connectionType] OFF
GO
