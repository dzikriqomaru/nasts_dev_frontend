INSERT [dbo].[EmailStatus] ([PK_EmailStatus_ID], [EmailStatusName]) VALUES (1, N'Preparation')
INSERT [dbo].[EmailStatus] ([PK_EmailStatus_ID], [EmailStatusName]) VALUES (2, N'OnQueue')
INSERT [dbo].[EmailStatus] ([PK_EmailStatus_ID], [EmailStatusName]) VALUES (3, N'InProcess')
INSERT [dbo].[EmailStatus] ([PK_EmailStatus_ID], [EmailStatusName]) VALUES (4, N'SucessSend')
INSERT [dbo].[EmailStatus] ([PK_EmailStatus_ID], [EmailStatusName]) VALUES (5, N'FailSend')
INSERT [dbo].[EmailStatus] ([PK_EmailStatus_ID], [EmailStatusName]) VALUES (6, N'Bounced')
INSERT [dbo].[EmailStatus] ([PK_EmailStatus_ID], [EmailStatusName]) VALUES (7, N'Read')
GO
