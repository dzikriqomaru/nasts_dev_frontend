SET IDENTITY_INSERT [dbo].[SystemParameterGroup] ON 

INSERT [dbo].[SystemParameterGroup] ([PK_SystemParameterGroup_ID], [SystemParameterGroup]) VALUES (1, N'Application Parameter')
INSERT [dbo].[SystemParameterGroup] ([PK_SystemParameterGroup_ID], [SystemParameterGroup]) VALUES (2, N'Login Parameter')
INSERT [dbo].[SystemParameterGroup] ([PK_SystemParameterGroup_ID], [SystemParameterGroup]) VALUES (3, N'Email Parameter')
INSERT [dbo].[SystemParameterGroup] ([PK_SystemParameterGroup_ID], [SystemParameterGroup]) VALUES (4, N'Common Parameter')
INSERT [dbo].[SystemParameterGroup] ([PK_SystemParameterGroup_ID], [SystemParameterGroup]) VALUES (5, N'Message Parameter')
INSERT [dbo].[SystemParameterGroup] ([PK_SystemParameterGroup_ID], [SystemParameterGroup]) VALUES (6, N'FTP Parameter')
INSERT [dbo].[SystemParameterGroup] ([PK_SystemParameterGroup_ID], [SystemParameterGroup]) VALUES (7, N'SISMONTAVAR PARAMETER')
INSERT [dbo].[SystemParameterGroup] ([PK_SystemParameterGroup_ID], [SystemParameterGroup]) VALUES (8, N'Scorpion Paramater')
INSERT [dbo].[SystemParameterGroup] ([PK_SystemParameterGroup_ID], [SystemParameterGroup]) VALUES (9, N'DevExpress Paramater')
SET IDENTITY_INSERT [dbo].[SystemParameterGroup] OFF
GO
