INSERT [dbo].[EmailTemplateSensitivity] ([PK_EmailTemplateSensitivity_ID], [SensitivityDescription]) VALUES (1, N'Normal')
INSERT [dbo].[EmailTemplateSensitivity] ([PK_EmailTemplateSensitivity_ID], [SensitivityDescription]) VALUES (2, N'Personal')
INSERT [dbo].[EmailTemplateSensitivity] ([PK_EmailTemplateSensitivity_ID], [SensitivityDescription]) VALUES (3, N'Private')
INSERT [dbo].[EmailTemplateSensitivity] ([PK_EmailTemplateSensitivity_ID], [SensitivityDescription]) VALUES (4, N'Confidential')
GO
