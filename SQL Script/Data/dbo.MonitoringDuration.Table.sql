SET IDENTITY_INSERT [dbo].[MonitoringDuration] ON 

INSERT [dbo].[MonitoringDuration] ([PK_MonitoringDuration_Id], [MonitoringDurationName]) VALUES (1, N'None')
INSERT [dbo].[MonitoringDuration] ([PK_MonitoringDuration_Id], [MonitoringDurationName]) VALUES (2, N'Day(s)')
INSERT [dbo].[MonitoringDuration] ([PK_MonitoringDuration_Id], [MonitoringDurationName]) VALUES (3, N'Week(s)')
INSERT [dbo].[MonitoringDuration] ([PK_MonitoringDuration_Id], [MonitoringDurationName]) VALUES (4, N'Month(s)')
SET IDENTITY_INSERT [dbo].[MonitoringDuration] OFF
GO
