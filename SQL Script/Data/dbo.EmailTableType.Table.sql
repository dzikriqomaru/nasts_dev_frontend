INSERT [dbo].[EmailTableType] ([PK_EmailTableType_ID], [EmailTableTypeName]) VALUES (1, N'Primary Table')
INSERT [dbo].[EmailTableType] ([PK_EmailTableType_ID], [EmailTableTypeName]) VALUES (2, N'Additional Table')
INSERT [dbo].[EmailTableType] ([PK_EmailTableType_ID], [EmailTableTypeName]) VALUES (3, N'Attachment Table')
GO
