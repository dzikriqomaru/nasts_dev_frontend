SET IDENTITY_INSERT [dbo].[SSRSExportType] ON 

INSERT [dbo].[SSRSExportType] ([pk_id], [Label], [RenderAs], [Extension], [Active], [CreatedBy], [LastUpdateBy], [ApprovedBy], [CreatedDate], [LastUpdateDate], [ApprovedDate], [Alternateby]) VALUES (1, N'Excel', N'ExcelOpenXML', N'.xlsx', 1, N'sysadmin', N'sysadmin', NULL, CAST(N'2022-03-31T10:16:05.093' AS DateTime), CAST(N'2022-03-31T10:16:05.093' AS DateTime), NULL, NULL)
INSERT [dbo].[SSRSExportType] ([pk_id], [Label], [RenderAs], [Extension], [Active], [CreatedBy], [LastUpdateBy], [ApprovedBy], [CreatedDate], [LastUpdateDate], [ApprovedDate], [Alternateby]) VALUES (2, N'PDF', N'PDF', N'.pdf', 1, N'sysadmin', N'sysadmin', NULL, CAST(N'2022-03-31T10:53:39.370' AS DateTime), CAST(N'2022-03-31T10:53:39.370' AS DateTime), NULL, NULL)
INSERT [dbo].[SSRSExportType] ([pk_id], [Label], [RenderAs], [Extension], [Active], [CreatedBy], [LastUpdateBy], [ApprovedBy], [CreatedDate], [LastUpdateDate], [ApprovedDate], [Alternateby]) VALUES (3, N'Word Open XML', N'WORDOPENXML', N'.docx', 1, N'sysadmin', N'sysadmin', NULL, CAST(N'2022-03-31T14:18:26.667' AS DateTime), CAST(N'2022-03-31T14:18:26.667' AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[SSRSExportType] OFF
GO
