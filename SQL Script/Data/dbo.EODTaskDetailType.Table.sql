INSERT [dbo].[EODTaskDetailType] ([PK_EODTaskDetailType_ID], [EODTaskDetailType]) VALUES (1, N'SSIS')
INSERT [dbo].[EODTaskDetailType] ([PK_EODTaskDetailType_ID], [EODTaskDetailType]) VALUES (2, N'Store Procedure')
INSERT [dbo].[EODTaskDetailType] ([PK_EODTaskDetailType_ID], [EODTaskDetailType]) VALUES (3, N'SSIS in SQL Agent')
INSERT [dbo].[EODTaskDetailType] ([PK_EODTaskDetailType_ID], [EODTaskDetailType]) VALUES (4, N'API')
INSERT [dbo].[EODTaskDetailType] ([PK_EODTaskDetailType_ID], [EODTaskDetailType]) VALUES (5, N'Data Fusion')
INSERT [dbo].[EODTaskDetailType] ([PK_EODTaskDetailType_ID], [EODTaskDetailType]) VALUES (6, N'SSIS ISPAC')
GO
